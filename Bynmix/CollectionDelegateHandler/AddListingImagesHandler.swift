import UIKit

class AddListingImagesHandler: NSObject,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate {
    
    var imageListingCell = "CreateListingImageCell"
    var addImageListingCell = "CreateListingFooterImageCell"
    var addImage = [AddListingImage]()
    var collectionView:UICollectionView?
    let imagePickerManager = ImagePickerManager()
    var oncoverImageClick:(() -> Void)!
    var onRemoveImageClick:(() -> Void)!
    var imageListingCollectionViewCell : ImageListingCollectionViewCell?
    var addImageDelegate:AddImageProtocol?
    var buttonRemove:Bool? = false
    var TYPE:Int?
    
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        if self.addImage[indexPath.row].userImage != UIImage(named: AppConstants.Images.CREATE_ADD_PHOTO_ICON){
            return true
        }else{
            return false
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        if self.addImage[destinationIndexPath.row].userImage == UIImage(named: AppConstants.Images.CREATE_ADD_PHOTO_ICON){
            self.collectionView!.moveItem(at: destinationIndexPath, to: sourceIndexPath)
        }else{
            let item = self.addImage.remove(at: sourceIndexPath.row)
            self.addImage.insert(item, at: destinationIndexPath.row)
        }
        //self.collectionView?.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.collectionView = collectionView
    
        if self.addImage.count > 4 {
            return 4
        }
        return self.addImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let data: AddListingImage?
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: imageListingCell, for: indexPath) as! ImageListingCollectionViewCell
        self.imageListingCollectionViewCell = cell
        
        data = self.addImage[indexPath.item] as AddListingImage
        
        if data?.type == 0{
            cell.userImageView.image = UIImage(named: AppConstants.Images.CREATE_ADD_PHOTO_ICON)
            cell.removeButton.isHidden = true
            
            cell.userImageView.tag = indexPath.row
            let cellTap = UITapGestureRecognizer(target: self, action: #selector(onFooterAction))
            cell.addGestureRecognizer(cellTap)
            
        }else{
            
            cell.removeButton.isHidden = false
            cell.removeButton.tag = indexPath.row
            cell.removeButton.addTarget(self, action: #selector(onRemoveButtonAction), for: .touchUpInside)
            if self.TYPE == AppConstants.TYPE_EDIT_LISTING {
                let image = UIImage(named: AppConstants.Images.PLACEHOLDER_IMAGE)
                let userImage = data?.imageUrl
                if let imageUrl = userImage{
                    cell.userImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
                } else {
                    let image = data?.userImage
                    cell.userImageView.image =  image
                }
            } else {
                let userImage = data?.userImage
                cell.userImageView.image = userImage
            }
            let cellTap = UITapGestureRecognizer(target: self, action: #selector(emptyClick))
            cell.addGestureRecognizer(cellTap)
        }
        
        cell.backgroundColor = UIColor.cE8E8E8
        cell.userImageView.alpha = 1
        
        if cell.userImageView.image == UIImage(named: AppConstants.Images.CREATE_ADD_PHOTO_ICON){
            cell.userImageView.contentMode = .scaleAspectFit
        }else{
            cell.userImageView.contentMode = .scaleToFill
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

        if self.addImage.count < 2{
            let totalCellWidth = 179 * collectionView.numberOfItems(inSection: 0)
            let totalSpacingWidth = 10 * (collectionView.numberOfItems(inSection: 0) - 1)

            let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
            let rightInset = leftInset
            print(totalCellWidth,totalSpacingWidth,leftInset,rightInset)
            print(collectionView.layer.frame.size.width,collectionView.layer.frame.size.height)
            return UIEdgeInsets(top: 0, left: leftInset , bottom: 0, right: rightInset )
        }else{
            return UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        }
        
    }
    
    @objc func onFooterAction(sender:UITapGestureRecognizer){
        oncoverImageClick()
    }
    
    @objc func emptyClick(sender:UITapGestureRecognizer){
    }
    
    @objc func onRemoveButtonAction(sender:UIButton){
        let index = sender.tag
        addImage.remove(at: index)
        onRemoveImageClick()
        collectionView?.reloadData()
    }
    
}
