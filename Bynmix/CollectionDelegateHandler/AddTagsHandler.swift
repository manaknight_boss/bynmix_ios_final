import UIKit
import Toast_Swift

class AddTagsHandler: NSObject,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITextFieldDelegate {
    
    var enterTagCell = "addTagsCell"
    var addTagCell = "enterTagsCell"
    var dataObject = [AddTags]()
    var collectionView:UICollectionView?
    var enterTagCollectionViewCell: EnterTagCollectionViewCell?
    var onTagsClickDelegate:onTagsClickProtocol?
    var TYPE = AppConstants.ON_TAGS_CLICK_TYPE
    var tagsContainsValueDelegate:TagsContainsValueProtocol?

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if dataObject[indexPath.item].type == 1 {
            
            return CGSize(width: 104, height: 36)
        }else if dataObject[indexPath.item].type == 2{
            let label = UILabel(frame: CGRect.zero)
            label.font = UIFont.fontBold(size: 15)
            label.textColor = UIColor.cBB189C
            label.text = dataObject[indexPath.item].listingTags?.tagName
            label.sizeToFit()
            let button = UIButton(frame: CGRect.zero)
            button.sizeToFit()
            return CGSize(width: label.frame.width + button.frame.width, height: 30)
        }else {
            let label = UILabel(frame: CGRect.zero)
            label.font = UIFont.fontBold(size: 15)
            label.textColor = UIColor.cBB189C
            label.text = dataObject[indexPath.item].tag
            label.sizeToFit()
            let button = UIButton(frame: CGRect.zero)
            button.sizeToFit()
            return CGSize(width: label.frame.width + button.frame.width, height: 30)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.collectionView = collectionView
        return self.dataObject.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = self.dataObject[indexPath.item] as AddTags
        
        if data.type == 1{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: enterTagCell, for: indexPath) as! EnterTagCollectionViewCell
            
            if dataObject.count > 1{
                self.tagsContainsValueDelegate?.tagsContainsValue(type: AppConstants.TYPE_ON_TAG_HAS_VALUE)
            }else{
                self.tagsContainsValueDelegate?.tagsContainsValue(type: AppConstants.TYPE_NO_TAGS)
            }
            
            let yourViewBorder = CAShapeLayer()
            yourViewBorder.strokeColor = UIColor.cB1B1B1.cgColor
            yourViewBorder.lineDashPattern = [2, 2]
            yourViewBorder.fillColor = UIColor.clear.cgColor
            yourViewBorder.lineWidth = 2
            yourViewBorder.path = UIBezierPath(roundedRect: cell.headerView.bounds, cornerRadius: 8).cgPath
            if (cell.headerView.layer.sublayers?.count)! < 2{
                cell.headerView.layer.addSublayer(yourViewBorder)
            }
            
            let numberOfPostsViewSelector : Selector = #selector(self.onHeaderClick)
            let viewPostsViewGesture = UITapGestureRecognizer(target: self, action: numberOfPostsViewSelector)
            cell.addTagLabel.addGestureRecognizer(viewPostsViewGesture)
            cell.headerView.isHidden = false
            cell.dataView.isHidden = true
            cell.addTagTextField.delegate = self
            cell.addTagTextField.returnKeyType = UIReturnKeyType.done
            
            self.TYPE = 2
            enterTagCollectionViewCell = cell
            return cell
            
        }else if data.type == 2{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: addTagCell, for: indexPath) as! AddTagsCollectionViewCell
            
            let dataAdd = data.listingTags?.tagName
            
            if dataAdd != nil {
                cell.isHidden = false
                cell.tagNameLabel.text = dataAdd
                cell.tag = indexPath.row
                let cellTap = UITapGestureRecognizer(target: self, action: #selector(deleteTagData))
                cell.addGestureRecognizer(cellTap)
            }else {
                cell.isHidden = true
            }
            
            return cell
            
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: addTagCell, for: indexPath) as! AddTagsCollectionViewCell
            
            let dataAdd = data.tag
            
            if dataAdd != nil {
                cell.isHidden = false
                cell.tagNameLabel.text = dataAdd
                cell.tag = indexPath.row
                let cellTap = UITapGestureRecognizer(target: self, action: #selector(deleteTagData))
                cell.addGestureRecognizer(cellTap)
            }else {
                cell.isHidden = true
            }
            
            self.onTagsClickDelegate?.onTagsClick(type: 3)
            
            return cell
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text == "" {
            print("Invalid Tag")
        } else {
            dataObject.append(AddTags(tag: textField.text!, type:0))
            textField.text = ""
            if dataObject.count > 10 {
                dataObject.remove(at: 0)
                collectionView?.reloadData()
            }
        }
        collectionView?.reloadData()
        if dataObject.count > 1{
        self.tagsContainsValueDelegate?.tagsContainsValue(type: AppConstants.TYPE_ON_TAG_HAS_VALUE)
        }else{
            self.tagsContainsValueDelegate?.tagsContainsValue(type: AppConstants.TYPE_NO_TAGS)
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 20
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        enterTagCollectionViewCell?.headerView.isHidden = false
        enterTagCollectionViewCell?.dataView.isHidden = true
        enterTagCollectionViewCell?.addTagTextField.text = ""
    }
    
    @objc  func onHeaderClick(sender: UITapGestureRecognizer) {
        enterTagCollectionViewCell?.headerView.isHidden = true
        enterTagCollectionViewCell?.dataView.isHidden = false
        enterTagCollectionViewCell?.addTagTextField.becomeFirstResponder()
        self.TYPE = 1
        self.onTagsClickDelegate?.onTagsClick(type: 1)
    }
    
    @objc func deleteTagData(sender:UITapGestureRecognizer) {
        let index = sender.view!.tag
        var isHeaderRequired = false
        if dataObject.count == 10 && dataObject[0].type != 1{
            isHeaderRequired = true
        }
        dataObject.remove(at: index)
        if isHeaderRequired  {
            dataObject.insert(AddTags(tag:"", type:1), at: 0)
        }
        collectionView?.reloadData()
        if dataObject.count > 1{
            self.tagsContainsValueDelegate?.tagsContainsValue(type: AppConstants.TYPE_ON_TAG_HAS_VALUE)
        }else{
            self.tagsContainsValueDelegate?.tagsContainsValue(type: AppConstants.TYPE_NO_TAGS)
        }
    }
    
}
