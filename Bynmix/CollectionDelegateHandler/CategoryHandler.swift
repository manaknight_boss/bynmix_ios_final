import UIKit

class CategoryHandler: NSObject,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    let categoryCell = "categoryCell"
    var categoryType = [CategoryType]()
    var onCategoryClick: ((CategoryType) -> Void)!
    let baseViewController = BaseViewController()
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 7
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           let screenSize = UIScreen.main.bounds
           let screenWidth = screenSize.width * 0.95
           let height: CGFloat = screenWidth / 3
           let width: CGFloat = screenWidth / 3
           return CGSize(width: width, height: height)
       }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  categoryType.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        
        cell!.layer.borderWidth = 4
        cell!.layer.borderColor = UIColor.cBB189C.cgColor
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: categoryCell, for: indexPath) as! CategoryCollectionViewCell
        let category = self.categoryType[indexPath.item] as CategoryType
        
        
        cell.categoryView.layer.masksToBounds = true
        
        if let imageUrl = category.displayImage{
            cell.categoryImageView.kf.setImage(with: URL(string: imageUrl)!)
        }
        cell.shadowView.layer.shadowColor = UIColor.black.cgColor
        cell.shadowView.alpha = CGFloat(0.26)
        cell.shadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        cell.shadowView.layer.shadowOpacity = 1
        
        cell.categoryNameLabel.text = category.categoryTypeName?.uppercased()
        
        cell.tag = indexPath.row
        let cellTap = UITapGestureRecognizer(target: self, action: #selector(oncategoryTap))
        cell.addGestureRecognizer(cellTap)
        if category.isSelected! {
            cell.categoryView.layer.borderWidth = 4
            cell.categoryView.layer.borderColor = UIColor.cBB189C.cgColor
        } else {
            cell.categoryView.layer.borderWidth = 2
            cell.categoryView.layer.borderColor = UIColor.c464646.cgColor
        }
        
        return cell
    }
    
    @objc func oncategoryTap(sender:UITapGestureRecognizer){
        let index = sender.view!.tag
        onCategoryClick(categoryType[index])
    }
    
}
