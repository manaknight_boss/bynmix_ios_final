import UIKit

class ColorsHandler: NSObject,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    let colorsCell = "colorsCell"
    var colorsList = [Colors]()
    var colorsCollectionViewCell : ColorsCollectionViewCell?
    var collectionView:UICollectionView?
    var onMoreThanTwoColorsClick:(() -> Void)!
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width * 0.95
        let height: CGFloat = screenWidth / 3
        let width: CGFloat = screenWidth / 3
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)

        cell!.layer.borderWidth = 4
        cell!.layer.borderColor = UIColor.cBB189C.cgColor

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.collectionView = collectionView
        return colorsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: colorsCell, for: indexPath) as! ColorsCollectionViewCell
        let colors = colorsList[indexPath.item] as Colors
        
        cell.colorsView.layer.borderWidth = 2.0
        cell.colorsView.layer.borderColor = UIColor.c464646.cgColor
        cell.colorsView.layer.masksToBounds = true
        
        cell.shadowView.layer.shadowColor = UIColor.black.cgColor
        cell.shadowView.alpha = CGFloat(0.26)
        cell.shadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        cell.shadowView.layer.shadowOpacity = 1
        
        cell.colorShadowView.layer.shadowColor = UIColor.black.cgColor
        cell.colorShadowView.alpha = CGFloat(0.26)
        cell.colorShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        cell.colorShadowView.layer.shadowOpacity = 1
        cell.colorShadowView.layer.cornerRadius = cell.colorShadowView.frame.size.width/2
        
        cell.colorFillView.layer.borderWidth = 2.0
        cell.colorFillView.layer.masksToBounds = false
        cell.colorFillView.layer.borderColor = UIColor.white.cgColor
        cell.colorFillView.clipsToBounds = true
        cell.colorFillView.layer.cornerRadius = cell.colorFillView.frame.size.width/2
        
        cell.colorNameLabel.text = colors.colorName
        cell.colorFillView.backgroundColor = UIColor(hex: colors.hexCode!)
        
        cell.colorsView.tag = indexPath.row
        let colorTap = UITapGestureRecognizer(target: self, action: #selector(onColorTap))
        cell.colorsView.addGestureRecognizer(colorTap)
        
        if colors.isSelected! {
            cell.colorsView.layer.borderWidth = 4
            cell.colorsView.layer.borderColor = UIColor.cBB189C.cgColor
        } else {
            cell.colorsView.layer.borderWidth = 2
            cell.colorsView.layer.borderColor = UIColor.c464646.cgColor
        }
        
        return cell
    }
    
    @objc func onColorTap(sender:UITapGestureRecognizer){
        let index = sender.view!.tag
        if getSelectedColors().count == 2 && !colorsList[index].isSelected! {
            onMoreThanTwoColorsClick()
        } else {
            colorsList[index].isSelected = !colorsList[index].isSelected!
            let position = IndexPath(row: index, section: 0)
            UIView.performWithoutAnimation {
                self.collectionView?.reloadItems(at: [position])
            }
        }
    }
    
    func getSelectedColors() -> [Colors] {
        var selectedColors : [Colors] = []
        for i in 0..<colorsList.count {
            if colorsList[i].isSelected! {
                selectedColors.append(colorsList[i])
            }
        }
        return selectedColors
    }
    
}
