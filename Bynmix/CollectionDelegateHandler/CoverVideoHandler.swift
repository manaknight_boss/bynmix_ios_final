import UIKit

class CoverVideoHandler: NSObject,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout  {
    
    let coverVideoCell = "galleryCell"
    var items = [CoverVideos]()
    var time = [String]()
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 95, height: 93)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: coverVideoCell,
                                                      for: indexPath) as! CoverVideoCollectionViewCell
        
        let item = items[indexPath.row]
        
        cell.videoImageView.image = item.videoImage
        cell.videoImageView.contentMode = .scaleToFill
        cell.timeLabel.text = item.time
        
        return cell
    }
    
}
