
import UIKit

class FeedListingsHandler: NSObject,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource {
    
    static var feed = [Feed]()
    static var browse = [Feed]()
    static var myLikes = [Feed]()
    var feedListingsCell = "feedListingsCell"
    var itemClickHandler: ((Feed) -> Void)!
    var onItemImageViewClick:((Feed) -> Void)!
    var onHeartButtonClick:((Feed) -> Void)!
    var scrollDelegate: FeedScrollProtocol?
    var collectionView:UICollectionView?
    var loadMoreDelegate: LoadMoreProtocol?
    var isLoadMoreRequired = true
    var listingFooterCell = "listingFooterCell"
    var listingFooterCollectionViewCell = ListingFooterCollectionViewCell()
    var onOtherUserClick:((Feed) -> Void)!
    var TYPE:Int?
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if (kind == UICollectionView.elementKindSectionFooter) {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: listingFooterCell, for: indexPath) as! ListingFooterCollectionViewCell
           listingFooterCollectionViewCell = footerView
            if isLoadMoreRequired {
                footerView.footerActivityIndicator.isHidden = false
            } else {
                footerView.footerActivityIndicator = nil
            }
            return footerView
        }
        fatalError()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
       
        let width = collectionView.frame.size.width
        let cellWidth = (width * 0.50) - 7.5
        return CGSize(width: cellWidth , height: 80 + cellWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.collectionView = collectionView
        if self.TYPE == AppConstants.TYPE_MY_LIKES{
            if FeedListingsHandler.myLikes.count > 0{
                self.collectionView?.alpha = 1
                return FeedListingsHandler.myLikes.count
            }else{
                self.collectionView?.alpha = 0
                return 0
            }
        }else if self.TYPE == AppConstants.TYPE_FEED_LISTING{
            if FeedListingsHandler.feed.count > 0{
                self.collectionView?.alpha = 1
                return FeedListingsHandler.feed.count
            }else{
                self.collectionView?.alpha = 0
                return 0
            }
        }else{
            if FeedListingsHandler.browse.count > 0{
                self.collectionView?.alpha = 1
                return FeedListingsHandler.browse.count
            }else{
                self.collectionView?.alpha = 0
                return 0
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: feedListingsCell, for: indexPath) as! FeedListingsCollectionViewCell
        
        var myLikes: Feed?
        var feed: Feed?
        var browse: Feed?
        
        if self.TYPE == AppConstants.TYPE_MY_LIKES{
            myLikes = FeedListingsHandler.myLikes[indexPath.item] as Feed
            self.handleTypes(cell:cell,feed:myLikes!,indexPath:indexPath)
        }else if self.TYPE == AppConstants.TYPE_FEED_LISTING{
            feed = FeedListingsHandler.feed[indexPath.item] as Feed
            self.handleTypes(cell:cell,feed:feed!,indexPath:indexPath)
        }else if self.TYPE == AppConstants.TYPE_BROWSE_LISTING{
            browse = FeedListingsHandler.browse[indexPath.item] as Feed
            self.handleTypes(cell:cell,feed:browse!,indexPath:indexPath)
        }
        
        return cell
    }
    
    func handleTypes(cell:FeedListingsCollectionViewCell,feed:Feed,indexPath:IndexPath){
        cell.topView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        cell.topView.backgroundColor = UIColor.cD53A95
        let type = feed.type
        let image = UIImage(named: AppConstants.Images.DEFAULT_USER)
        let listingImage =  UIImage(named: AppConstants.Images.ITEM_PLACEHOLDER)!
        
        let text:String = AppConstants.Strings.SIZE + (feed.size ?? "")
        let textRegular:String = AppConstants.Strings.SIZE
        let range = (text as NSString).range(of: textRegular)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 11), range: range)
        
        let priceText:String = AppConstants.Strings.PRICE + AppConstants.Strings.DOLLAR_SIGN + String(Int(feed.price ?? 0))
        let textPriceRegular:String = AppConstants.Strings.PRICE
        let priceRange = (priceText as NSString).range(of: textPriceRegular)
        let attributedPriceString = NSMutableAttributedString(string: priceText)
        attributedPriceString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 11), range: priceRange)
        
        cell.titleLabel.text = feed.title
        
        cell.itemImageView.layer.zPosition = -1
        if let imageUrl = feed.photoUrl{
            cell.itemImageView.kf.setImage(with: URL(string: imageUrl),placeholder: listingImage)
        }else{
            cell.itemImageView.image = listingImage
        }
        
        if type == AppConstants.LISTING {
            cell.titleLabel.numberOfLines = 1
            cell.sizeLabel.attributedText = attributedString
            cell.priceLabel.attributedText = attributedPriceString
        }
        
        let likesCount =  feed.likesCount
        cell.likesShadowView.layer.shadowColor = UIColor.black.cgColor
        cell.likesShadowView.alpha = CGFloat(0.26)
        cell.likesShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        cell.likesShadowView.layer.shadowOpacity = 1
        
        cell.likesView.tag = indexPath.row
        
        let likesTap = UITapGestureRecognizer(target: self, action: #selector(likesClick))
        cell.likesView.addGestureRecognizer(likesTap)
        
        cell.middleView.tag = indexPath.row
        let itemImageTap = UITapGestureRecognizer(target: self, action: #selector(itemImageViewClick))
        cell.middleView.addGestureRecognizer(itemImageTap)
        
        cell.heartButton.tag = indexPath.row
        cell.heartButton.addTarget(self, action: #selector(heartClick), for: .touchDown)
        
        if likesCount == 0 {
            cell.likesView.isHidden = true
            cell.likesShadowView.isHidden = true
            
        }else if likesCount == 1{
            cell.likesView.isHidden = false
            cell.likesShadowView.isHidden = false
            cell.likesLabel.text = "\(likesCount ?? 0)"
            cell.likesStringLabel.text = AppConstants.Strings.LIKE
        }else{
            cell.likesView.isHidden = false
            cell.likesShadowView.isHidden = false
            cell.likesLabel.text = "\(likesCount ?? 0)"
            cell.likesStringLabel.text = AppConstants.Strings.LIKES
        }
        
        cell.likesLabel.text = "\(feed.likesCount ?? 0)"
        
        cell.userImageView.layer.borderWidth = 2.0
        cell.userImageView.layer.masksToBounds = false
        cell.userImageView.layer.borderColor = UIColor.white.cgColor
        cell.userImageView.clipsToBounds = true
        cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.width/2
        
        cell.userShadowView.layer.cornerRadius = cell.userShadowView.frame.size.width/2
        cell.userShadowView.layer.shadowColor = UIColor.black.cgColor
        cell.userShadowView.alpha = CGFloat(0.26)
        cell.userShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        cell.userShadowView.layer.shadowOpacity = 1
        cell.userShadowView.layer.zPosition = -1
        cell.userImageView.contentMode = .scaleAspectFill
        
        if let imageUrl = feed.userPhotoUrl{
            cell.userImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        } else {
            cell.userImageView.image =  image
        }
        
        cell.usernameLabel.text = feed.username
        cell.postTimeLabel.text = feed.createdDateFormatted
        
        if feed.userLiked == false {
            cell.heartButton.setImage(UIImage(named: AppConstants.Images.FEED_HEART), for: .normal)
        } else {
            cell.heartButton.setImage(UIImage(named: AppConstants.Images.FEED_HEART_SELECTED), for: .normal)
        }
        
        cell.userImageView.tag = indexPath.row
        let userImageTap = UITapGestureRecognizer(target: self, action: #selector(onUserImageViewClick))
        cell.userImageView.addGestureRecognizer(userImageTap)
        
        cell.userShadowView.tag = indexPath.row
        let userShadowImageTap = UITapGestureRecognizer(target: self, action: #selector(onUserImageViewClick))
        cell.userShadowView.addGestureRecognizer(userShadowImageTap)
        
        cell.usernameLabel.tag = indexPath.row
        let userNameTap = UITapGestureRecognizer(target: self, action: #selector (onUserNameClick))
        cell.usernameLabel.addGestureRecognizer(userNameTap)
        
        cell.usernameView.tag = indexPath.row
        let userNameViewTap = UITapGestureRecognizer(target: self, action: #selector (onUserNameClick))
        cell.usernameView.addGestureRecognizer(userNameViewTap)
        
        if feed.canUserLikeItem!{
            cell.heartButton.isHidden = false
        }else{
            cell.heartButton.isHidden = true
        }
    }
    
    @objc func onUserImageViewClick(sender:UITapGestureRecognizer){
        let index = sender.view!.tag
        self.usernameAndUserImageClick(index:index)
    }
    
    @objc func onUserNameClick(sender:UITapGestureRecognizer){
        let index = sender.view!.tag
        self.usernameAndUserImageClick(index:index)
    }
    
    func usernameAndUserImageClick(index:Int){
        if self.TYPE == AppConstants.TYPE_MY_LIKES{
            onOtherUserClick(FeedListingsHandler.myLikes[index])
        }else if self.TYPE == AppConstants.TYPE_FEED_LISTING{
            onOtherUserClick(FeedListingsHandler.feed[index])
        }else if self.TYPE == AppConstants.TYPE_BROWSE_LISTING{
            onOtherUserClick(FeedListingsHandler.browse[index])
        }
    }
    
    @objc func heartClick(sender: UIButton){
        let index = sender.tag
        
        var likesCount :Int? = 0
        var userLiked:Bool? = false
    
        if self.TYPE == AppConstants.TYPE_MY_LIKES{
            likesCount =  FeedListingsHandler.myLikes[index].likesCount
            userLiked = FeedListingsHandler.myLikes[index].userLiked
        }else if self.TYPE == AppConstants.TYPE_FEED_LISTING{
            likesCount =  FeedListingsHandler.feed[index].likesCount
            userLiked = FeedListingsHandler.feed[index].userLiked
        }else if self.TYPE == AppConstants.TYPE_BROWSE_LISTING{
            likesCount =  FeedListingsHandler.browse[index].likesCount
            userLiked = FeedListingsHandler.browse[index].userLiked
        }
        
        userLiked = !userLiked!
        if userLiked == false {
            likesCount! -= 1
        }else{
            likesCount! += 1
        }
        
        if self.TYPE == AppConstants.TYPE_MY_LIKES{
            FeedListingsHandler.myLikes[index].likesCount = likesCount
            FeedListingsHandler.myLikes[index].userLiked = userLiked
            onHeartButtonClick(FeedListingsHandler.myLikes[index])
        }else if self.TYPE == AppConstants.TYPE_FEED_LISTING{
            FeedListingsHandler.feed[index].likesCount = likesCount
            FeedListingsHandler.feed[index].userLiked = userLiked
            onHeartButtonClick(FeedListingsHandler.feed[index])
        }else if self.TYPE == AppConstants.TYPE_BROWSE_LISTING{
            FeedListingsHandler.browse[index].likesCount = likesCount
            FeedListingsHandler.browse[index].userLiked = userLiked
            onHeartButtonClick(FeedListingsHandler.browse[index])
        }
        
        let position = IndexPath(row: index, section: 0)
        UIView.performWithoutAnimation {
            self.collectionView?.reloadItems(at: [position])
        }
        
    }
    
    @objc func likesClick(sender: UITapGestureRecognizer){
        let index = sender.view!.tag
        if self.TYPE == AppConstants.TYPE_MY_LIKES{
            itemClickHandler(FeedListingsHandler.myLikes[index])
        }else if self.TYPE == AppConstants.TYPE_FEED_LISTING{
            itemClickHandler(FeedListingsHandler.feed[index])
        }else if self.TYPE == AppConstants.TYPE_BROWSE_LISTING{
            itemClickHandler(FeedListingsHandler.browse[index])
        }
    }
    
    @objc func itemImageViewClick(sender: UITapGestureRecognizer){
        let index = sender.view!.tag
        if self.TYPE == AppConstants.TYPE_MY_LIKES{
            onItemImageViewClick(FeedListingsHandler.myLikes[index])
        }else if self.TYPE == AppConstants.TYPE_FEED_LISTING{
            onItemImageViewClick(FeedListingsHandler.feed[index])
        }else if self.TYPE == AppConstants.TYPE_BROWSE_LISTING{
            onItemImageViewClick(FeedListingsHandler.browse[index])
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom >= height {
            scrollDelegate?.onScroll(scrollValue: scrollView.contentOffset.y)
        }
        if self.listingFooterCollectionViewCell.footerActivityIndicator != nil {
            if distanceFromBottom < height && contentYoffset > 0{
                loadMoreDelegate?.onLoadMore()
            }
        }
    }
  
}
