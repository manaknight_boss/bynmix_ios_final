import UIKit

class FeedsHandler: NSObject,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
    var followCell = "follow_cell"
    var users = [User]()
    let followFooterCell = "follow_footer_cell"
    var onOtherUserClick:((User) -> Void)!
    var onMoreUserClick:(() -> Void)!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.users.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: followCell, for: indexPath) as! FollowCollectionViewCell
        
        let users = self.users[indexPath.row] as User
        
        let image = UIImage(named: AppConstants.Images.DEFAULT_USER)
        
        cell.userImageView.layer.borderWidth = 2.0
        cell.userImageView.layer.masksToBounds = false
        cell.userImageView.layer.borderColor = UIColor.white.cgColor
        cell.userImageView.clipsToBounds = true
        cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.width/2
        
        cell.shadowView.layer.cornerRadius = cell.shadowView.frame.size.width/2
        cell.shadowView.layer.shadowColor = UIColor.white.cgColor
        cell.shadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        cell.shadowView.layer.shadowOpacity = 1
        
        if let imageUrl = users.photoUrl{
            cell.userImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        } else {
            cell.userImageView.image =  image
        }
        
        cell.usernameLabel.text = users.username
        if (cell.usernameLabel.text?.count)! >= 8 {
            let index = cell.usernameLabel.text?.index((cell.usernameLabel.text?.startIndex)!, offsetBy: 8)
            let newStr = String((cell.usernameLabel.text?[..<index!])!)
            cell.usernameLabel.text = newStr
            cell.usernameLabel.text = cell.usernameLabel.text?.appending("..")
        }
        
        cell.userImageView.tag = indexPath.row
        let userImageTap = UITapGestureRecognizer(target: self, action: #selector(onUserImageViewClick))
        cell.userImageView.addGestureRecognizer(userImageTap)
        
        cell.usernameLabel.tag = indexPath.row
        let userNameTap = UITapGestureRecognizer(target: self, action: #selector (onUserNameClick))
        cell.usernameLabel.addGestureRecognizer(userNameTap)
        
        return cell
    }
    
    @objc func onUserImageViewClick(sender:UITapGestureRecognizer){
        let index = sender.view!.tag
        onOtherUserClick(users[index])
    }
    
    @objc func onUserNameClick(sender:UITapGestureRecognizer){
        let index = sender.view!.tag
        onOtherUserClick(users[index])
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if (kind == UICollectionView.elementKindSectionFooter) {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: followFooterCell, for: indexPath) as! MoreButtonCollectionReusableView
            let moreUsersTap = UITapGestureRecognizer(target: self, action: #selector (onMoreUsersClick))
            footerView.addButton.addGestureRecognizer(moreUsersTap)
            footerView.addMoreLabel.addGestureRecognizer(moreUsersTap)
            footerView.addMoreView.addGestureRecognizer(moreUsersTap)
            
            return footerView
        }
        
        fatalError()
    }
    
    @objc func onMoreUsersClick(sender:UITapGestureRecognizer){
        onMoreUserClick()
    }
    
}
