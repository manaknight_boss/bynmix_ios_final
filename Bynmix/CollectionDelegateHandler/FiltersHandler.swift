import UIKit

class FiltersHandler: NSObject,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var filterCell = "filterCell"
    var filter = [SelectedFilter]()
    var collectionView:UICollectionView?
    var onRemoveButtonClick:((SelectedFilter,Int) -> Void)!
    var scrollEndedInFilterDelegate:ScrollEndedInFilterProtocol?
    var TYPE:Int?
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let label = UILabel(frame: CGRect.zero)
        label.font = UIFont.fontMedium(size: 15)
        label.textColor = UIColor.white
        label.text = filter[indexPath.item].name
        label.sizeToFit()
        
        let width:Double = Double(34 + label.frame.width)
        
        AppConstants.filterCellWidth.append(width)
        return CGSize(width: 24 + label.frame.width , height: 34)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.collectionView = collectionView
        return filter.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: filterCell, for: indexPath) as! FilterCollectionViewCell
        let filters = filter[indexPath.row] as SelectedFilter
        
        if self.TYPE == AppConstants.TYPE_LISTING_FILTER {
            if filters.id == 0 {
                if (filters.type == AppConstants.CONDITION_FILTER) {
                    cell.filterNameLabel.text = (filters.name?.uppercased() ?? "") + " - Condition"
                } else if (filters.type == AppConstants.SIZE_FILTER) {
                    cell.filterNameLabel.text = (filters.name?.uppercased() ?? "") + " - Size"
                } else if (filters.type == AppConstants.COLOR_FILTER) {
                    cell.filterNameLabel.text = (filters.name?.uppercased() ?? "") + " - Color"
                } else if (filters.type == AppConstants.CATEGORY_FILTER) {
                    cell.filterNameLabel.text = (filters.name?.uppercased() ?? "") + " - Category"
                } else if (filters.type == AppConstants.PRICE_FILTER) {
                    cell.filterNameLabel.text = (filters.name?.uppercased() ?? "") + " - Price"
                } else if (filters.type == AppConstants.BRAND_FILTER) {
                    cell.filterNameLabel.text = (filters.name?.uppercased() ?? "") + " - Brand"
                }
            } else {
                if (filters.subType == 0 && filters.subTypeName != nil) {
                    cell.filterNameLabel.text = ((filters.subTypeName ?? "") + " - " + (filters.name ?? ""))
                } else {
                    if (filters.subTypeName == nil) {
                        cell.filterNameLabel.text = (filters.name ?? "")
                    } else {
                        cell.filterNameLabel.text = (filters.subTypeName ?? "")
                    }
                }

            }
        } else {
            if (filters.id == 0 && filters.subType == 0) {
                if (filters.type == AppConstants.USER_STYLE_FILTER) {
                    cell.filterNameLabel.text = (filters.name?.uppercased() ?? "") + " - Style"
                } else if (filters.type == AppConstants.USER_BODY_TYPE_FILTER) {
                    cell.filterNameLabel.text = (filters.name?.uppercased() ?? "") + " - Body Type"
                } else if (filters.type == AppConstants.USER_BRAND_FILTER) {
                    cell.filterNameLabel.text = (filters.name?.uppercased() ?? "") + " - Brand"
                } else if (filters.type == AppConstants.HEIGHT_FILTER) {
                    cell.filterNameLabel.text = (filters.name?.uppercased() ?? "") + " - Height"
                }
            } else {
                cell.filterNameLabel.text = (filters.name ?? "")
            }
        }
        
        cell.tag = indexPath.row
        let tap = UITapGestureRecognizer(target: self, action: #selector(onFilterClickAction))
        cell.addGestureRecognizer(tap)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row < self.filter.count - 1{
            print("lastItem")
        }
    }
    
    @objc func onFilterClickAction(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        self.onRemoveButtonClick(filter[index],index)
        filter.remove(at: index)
        collectionView?.reloadData()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffsetX = scrollView.contentOffset.x
        if collectionView?.contentSize.width ?? 0 > UIScreen.main.bounds.width{
            if contentOffsetX >= (scrollView.contentSize.width - scrollView.bounds.width) - 20 /* Needed offset */ {
                self.scrollEndedInFilterDelegate?.ScrollEndedInFilterProtocol(type: AppConstants.TYPE_RIGHT_BUTTON)
            }else if contentOffsetX == 0{
                self.scrollEndedInFilterDelegate?.ScrollEndedInFilterProtocol(type: AppConstants.TYPE_LEFT_BUTTON)
            }else{
                self.scrollEndedInFilterDelegate?.ScrollEndedInFilterProtocol(type: AppConstants.TYPE_BOTH_BUTTON)
            }
        }
    }
    
}
