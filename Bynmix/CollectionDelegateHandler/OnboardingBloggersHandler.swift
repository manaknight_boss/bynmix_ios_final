import UIKit

class OnboardingBloggersHandler: NSObject,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,CollectionViewWaterfallLayoutDelegate {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: NSIndexPath) -> CGSize {
        
        let screenSize = UIScreen.main.bounds
        
        let screenWidth = screenSize.width * 0.40
        
        let label = UILabel(frame: CGRect.zero)
        label.font = UIFont.fontBold(size: 15)
        label.textColor = UIColor.cBB189C
        label.text = bloggers[indexPath.item].title
        label.sizeToFit()
        
        var screenHeight:CGFloat = 0
        
        if bloggers[indexPath.item].title == nil{
            screenHeight = 185
        }else{
            screenHeight = 200 + 15
        }
        
        let height: CGFloat = screenHeight
        let width: CGFloat = screenWidth
        
        return CGSize(width: width, height: height)
    }
    
    var bloggers = [User]()
    let bloggersCell = "onboardingBloggersCell"
    var bloggerClick:((User) -> Void)!
    var collectionView:UICollectionView?
    var loadMoreDelegate: LoadMoreProtocol?
    var isLoadMoreRequired = true
    var bloggerFooterCell = "bloggerFooter"
    var bloggerFooterCollectionViewCell = BloggersFooterCollectionReusableView()
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        return CGSize(width: screenWidth, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout: UICollectionViewLayout, heightForFooterInSection section: Int) -> Float {
        return 50
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if (kind == "CollectionViewWaterfallElementKindSectionFooter") {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind:  UICollectionView.elementKindSectionFooter, withReuseIdentifier: bloggerFooterCell, for: indexPath) as! BloggersFooterCollectionReusableView
            bloggerFooterCollectionViewCell = footerView
            if isLoadMoreRequired {
                footerView.footerActivityIndicator.isHidden = false
            } else {
                //footerView.footerActivityIndicator = nil
                footerView.footerActivityIndicator.isHidden = true
            }
            return footerView
        }
        fatalError()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.collectionView = collectionView
        return bloggers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: bloggersCell, for: indexPath) as! OnboardingBloggersCollectionViewCell
        
        let blogger = bloggers[indexPath.row] as User
        
        let placeholder = UIImage(named: AppConstants.Images.ITEM_PLACEHOLDER)
        if let imageURL = blogger.photoUrl{
            cell.userImageView.contentMode = .scaleToFill
            cell.userImageView.kf.setImage(with: URL(string: imageURL),placeholder: placeholder)
        }
        cell.usernameLabel.text = blogger.username
        
        if blogger.title != nil{
            cell.titleView.isHidden = false
            cell.userDescLabel.text = blogger.title
            if (blogger.title?.count ?? 0) > 30{
                cell.titleViewHeight.constant = 39
            }else{
                cell.titleViewHeight.constant = 30
            }
        }else{
            cell.titleView.isHidden = true
            cell.titleViewHeight.constant = 0
        }
        
        cell.shadowView.layer.shadowColor = UIColor.black.cgColor
        cell.shadowView.alpha = CGFloat(0.40)
        cell.shadowView.layer.shadowOffset = CGSize(width: 0,height: 2)
        cell.shadowView.layer.shadowOpacity = 1
        //cell.shadowView.layer.shadowRadius = 4
//        cell.shadowView.layer.shadowPath = UIBezierPath(rect: cell.shadowView.bounds).cgPath
//        cell.shadowView.layer.shouldRasterize = true
//        cell.shadowView.layer.rasterizationScale = UIScreen.main.scale
        
        cell.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMinXMaxYCorner]
        cell.layer.cornerRadius = 5
        cell.followView.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMinXMaxYCorner]
        cell.followView.layer.cornerRadius = 5
        cell.followButton.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMinXMaxYCorner]
        cell.followButton.layer.cornerRadius = 5
        cell.mainView.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMinXMaxYCorner]
        cell.mainView.layer.cornerRadius = 5
        
        if blogger.isUserFollowing ?? false{
            cell.followButton.backgroundColor = UIColor.cCBCBCB
            cell.followButton.setTitle("FOLLOWING", for: .normal)
        }else{
            cell.followButton.backgroundColor = UIColor.cBB189C
            cell.followButton.setTitle("FOLLOW", for: .normal)
        }
        
        cell.followButton.tag = indexPath.row
        cell.followButton.addTarget(self, action: #selector(followButtonAction), for: .touchUpInside)
    
        return cell
    }
    
    @objc func followButtonAction(_ sender:UIButton){
        let index = sender.tag
        bloggers[index].isUserFollowing = !(bloggers[index].isUserFollowing!)
        bloggerClick(bloggers[index])
        let position = IndexPath(row: index, section: 0)
        UIView.performWithoutAnimation {
            self.collectionView?.reloadItems(at: [position])
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset

        if self.bloggerFooterCollectionViewCell.footerActivityIndicator != nil {
            if distanceFromBottom < height && contentYoffset > 0{
                loadMoreDelegate?.onLoadMore()
            }
        }
    }
    

}
