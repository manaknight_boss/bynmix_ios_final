import UIKit

class OnboardingSelectedBrandsHandler: NSObject,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    let brandsCell = "selectedBrandsCell"
    var brands = [Brand]()
    var collectionView :UICollectionView?
    var brandsClick:((Brand,Int) -> Void)!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.collectionView = collectionView
        return brands.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: brandsCell,
                                                      for: indexPath) as! SelectedBrandsCollectionViewCell
        
        let brand = brands[indexPath.row] as Brand
        
        if brand.brandName == nil{
            cell.brandNameLabel.text = brand.brand
        }else{
            cell.brandNameLabel.text = brand.brandName
        }
        
        cell.tag = indexPath.item
        let cellTap = UITapGestureRecognizer(target: self, action: #selector(brandsTapAction))
        cell.addGestureRecognizer(cellTap)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let label = UILabel(frame: CGRect.zero)
        label.font = UIFont.fontMedium(size: 15)
        label.textColor = UIColor.white
        if brands[indexPath.item].brandName == nil{
            label.text = brands[indexPath.item].brand
        }else{
            label.text = brands[indexPath.item].brandName
        }
        label.sizeToFit()
        let image = UIImageView(frame: CGRect.zero)
        image.image = UIImage(named: AppConstants.Images.HEART_SELECTED)
        image.sizeToFit()
        
        return CGSize(width: 30 + label.frame.width + image.frame.width, height: 40)
    }
    
    @objc func brandsTapAction(sender:UITapGestureRecognizer){
        let index = sender.view!.tag
        brandsClick(brands[index],index)        
    }
    

}
