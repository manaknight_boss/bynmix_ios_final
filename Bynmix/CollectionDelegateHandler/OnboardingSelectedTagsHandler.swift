import UIKit

class OnboardingSelectedTagsHandler: NSObject,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    let brandsCell = "selectedBrandsCell"
    var tags = [Tags]()
    var collectionView :UICollectionView?
    var tagsClick:((Tags,Int) -> Void)!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.collectionView = collectionView
        return tags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: brandsCell,
                                                      for: indexPath) as! SelectedBrandsCollectionViewCell
        
        let tag = tags[indexPath.row] as Tags
        
        if tag.descriptorName == nil{
            cell.brandNameLabel.text = tag.descriptor
        }else{
            cell.brandNameLabel.text = tag.descriptorName
        }
        
        cell.tag = indexPath.item
        let cellTap = UITapGestureRecognizer(target: self, action: #selector(brandsTapAction))
        cell.addGestureRecognizer(cellTap)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let label = UILabel(frame: CGRect.zero)
        label.font = UIFont.fontMedium(size: 15)
        label.textColor = UIColor.white
        if tags[indexPath.item].descriptorName == nil{
            label.text = tags[indexPath.item].descriptor
        }else{
            label.text = tags[indexPath.item].descriptorName
        }
        label.sizeToFit()
        let image = UIImageView(frame: CGRect.zero)
        image.image = UIImage(named: AppConstants.Images.HEART_SELECTED)
        image.sizeToFit()
        
        return CGSize(width: 30 + label.frame.width + image.frame.width, height: 40)
    }
    
    @objc func brandsTapAction(sender:UITapGestureRecognizer){
        let index = sender.view!.tag
        self.tagsClick(tags[index],index)
    }
}
