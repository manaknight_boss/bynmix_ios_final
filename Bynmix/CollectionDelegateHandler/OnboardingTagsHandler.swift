import UIKit

class OnboardingTagsHandler: NSObject,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var tagsCell = "tags_cell"
    var tags = [Tags]()
    var loadMoreDelegate: LoadMoreProtocol?
    var brandsFooterCollectionViewCell = OnBoardingBrandsCollectionViewCell()
    var isLoadMoreRequired = true
    var tagsFooterCell = "TagsFooterCell"
    var tagsClick:((Tags) -> Void)!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tags.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: tagsCell,
                                                      for: indexPath) as! TagsCollectionViewCell
        
        let tag = tags[indexPath.row] as Tags
        cell.brandLabel.text = tag.descriptorName
        
        cell.tag = indexPath.row
        let cellTap = UITapGestureRecognizer(target: self, action: #selector(brandsTapAction))
        cell.addGestureRecognizer(cellTap)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let label = UILabel(frame: CGRect.zero)
        label.font = UIFont.fontBold(size: 15)
        label.textColor = UIColor.cBB189C
        label.text = tags[indexPath.item].descriptorName
        label.sizeToFit()
        let image = UIImageView(frame: CGRect.zero)
        image.image = UIImage(named: AppConstants.Images.HEART)
        image.sizeToFit()
        return CGSize(width: label.frame.width + 40 + image.frame.width, height: image.frame.height + 16)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        return CGSize(width: screenWidth, height: 50)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYOffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYOffset
        
        if self.brandsFooterCollectionViewCell.footerActivityIndicator != nil {
            if distanceFromBottom < height && contentYOffset > 0{
                loadMoreDelegate?.onLoadMore()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if (kind == UICollectionView.elementKindSectionFooter) {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: tagsFooterCell, for: indexPath) as! OnBoardingBrandsCollectionViewCell
            brandsFooterCollectionViewCell = footerView
            if isLoadMoreRequired {
                footerView.footerActivityIndicator.isHidden = false
                
            } else {
                footerView.footerActivityIndicator.isHidden = true
            }
            
            return footerView
        }
        fatalError()
    }
    
    @objc func brandsTapAction(sender:UITapGestureRecognizer){
        let index = sender.view!.tag
        tagsClick(tags[index])
    }

    
}
