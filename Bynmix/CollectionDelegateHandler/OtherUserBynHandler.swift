import UIKit

class OtherUserBynHandler: NSObject,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    var feed = [Feed]()
    var otherUserBynCell = "myBynCell"
    var itemClickHandler: ((Feed) -> Void)!
    var onItemImageViewClick:((Feed) -> Void)!
    var onHeartButtonClick:((Feed) -> Void)!
    var scrollDelegate: FeedScrollProtocol?
    var collectionView:UICollectionView?
    var loadMoreDelegate: LoadMoreProtocol?
    var isLoadMoreRequired = true
    var listingFooterCell = "listingFooterCell"
    var listingFooterCollectionViewCell = ListingFooterCollectionViewCell()
    var canRefresh = true
    var refreshOtherUserProfileDelegate:RefreshOtherUserProfileProtocol?
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        return refreshControl
    }()
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if (kind == UICollectionView.elementKindSectionFooter) {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: listingFooterCell, for: indexPath) as! ListingFooterCollectionViewCell
            listingFooterCollectionViewCell = footerView
            if isLoadMoreRequired {
                footerView.footerActivityIndicator.isHidden = false
                
            } else {
                footerView.footerActivityIndicator.isHidden = true
            }
            
            return footerView
        }
        
        fatalError()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.size.width
        let cellWidth = (width * 0.50) - 7.5
        return CGSize(width: cellWidth , height: 80 + cellWidth)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.collectionView = collectionView
        return feed.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: otherUserBynCell, for: indexPath) as! OtherUserBynCollectionViewCell
        
        let feed = self.feed[indexPath.item] as Feed
        
        cell.topView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        cell.topView.backgroundColor = UIColor.cD53A95
        let type = feed.type
        var image = UIImage(named: AppConstants.Images.DEFAULT_USER)
        if type == AppConstants.LISTING {
            image =  UIImage(named: AppConstants.Images.ITEM_PLACEHOLDER)!
        }
        
        let text:String = AppConstants.Strings.SIZE + (feed.size ?? "")
        let textRegular:String = AppConstants.Strings.SIZE
        let range = (text as NSString).range(of: textRegular)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 10), range: range)
        
        let priceText:String = AppConstants.Strings.PRICE + AppConstants.Strings.DOLLAR_SIGN + String(Int(feed.price ?? 0))
        let textPriceRegular:String = AppConstants.Strings.PRICE
        let priceRange = (priceText as NSString).range(of: textPriceRegular)
        let attributedPriceString = NSMutableAttributedString(string: priceText)
        attributedPriceString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 10), range: priceRange)
        
        
        let brandText:String = AppConstants.Strings.BRAND + feed.brand!
        let textBrandRegular:String = AppConstants.Strings.BRAND
        let brandRange = (brandText as NSString).range(of: textBrandRegular)
        let attributedBrandString = NSMutableAttributedString(string: brandText)
        attributedBrandString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 10), range: brandRange)
        
        cell.titleLabel.text = feed.title
        cell.dateLabel.text = feed.createdDateFormatted
        
        cell.itemImageView.layer.zPosition = -1
        if let imageUrl = feed.photoUrl{
            cell.itemImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        }
        
        cell.titleLabel.numberOfLines = 1
        cell.sizeLabel.attributedText = attributedString
        cell.priceLabel.attributedText = attributedPriceString
        cell.brandLabel.attributedText = attributedBrandString
        
        let likesCount =  feed.likesCount
        cell.likesShadowView.layer.shadowColor = UIColor.black.cgColor
        cell.likesShadowView.alpha = CGFloat(0.26)
        cell.likesShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        cell.likesShadowView.layer.shadowOpacity = 1
        
        cell.likesView.tag = indexPath.row
        
        let likesTap = UITapGestureRecognizer(target: self, action: #selector(likesClick))
        cell.likesView.addGestureRecognizer(likesTap)
        
        cell.middleView.tag = indexPath.row
        let itemImageTap = UITapGestureRecognizer(target: self, action: #selector(itemImageViewClick))
        cell.middleView.addGestureRecognizer(itemImageTap)
        
        cell.heartButton.tag = indexPath.row
        cell.heartButton.addTarget(self, action: #selector(heartClick), for: .touchDown)
        
        if likesCount == 0 {
            cell.likesView.isHidden = true
            cell.likesShadowView.isHidden = true
            
        }else if likesCount == 1{
            cell.likesView.isHidden = false
            cell.likesShadowView.isHidden = false
            cell.likesLabel.text = "\(likesCount ?? 0)"
            cell.likesStringLabel.text = AppConstants.Strings.LIKE
        }else{
            cell.likesView.isHidden = false
            cell.likesShadowView.isHidden = false
            cell.likesLabel.text = "\(likesCount ?? 0)"
            cell.likesStringLabel.text = AppConstants.Strings.LIKES
        }
        
        cell.likesLabel.text = "\(feed.likesCount ?? 0)"
        
        if feed.userLiked == false {
            cell.heartButton.setImage(UIImage(named: AppConstants.Images.FEED_HEART), for: .normal)
        } else {
            cell.heartButton.setImage(UIImage(named: AppConstants.Images.FEED_HEART_SELECTED), for: .normal)
        }
        let feedStatus = feed.status!.lowercased()
        switch feedStatus {
            case AppConstants.SOLD:
                cell.itemStatusImageView.image = UIImage(named: AppConstants.Images.ITEM_SOLD_ICON)
                cell.itemStatusImageView.isHidden = false
                break;
            case AppConstants.ON_HOLD:
                cell.itemStatusImageView.image = UIImage(named: AppConstants.Images.ITEM_ON_HOLD_ICON)
                cell.itemStatusImageView.isHidden = false
                break;
            case AppConstants.NOT_FOR_SALE:
                cell.itemStatusImageView.image = UIImage(named: AppConstants.Images.ITEM_NOT_FOR_SALE_ICON)
                cell.itemStatusImageView.isHidden = false
                break;
            default:
                cell.itemStatusImageView.isHidden = true
        }
        
        return cell
    }
    
    @objc func heartClick(sender: UIButton){
        let index = sender.tag
        var likesCount =  feed[index].likesCount
        var userLiked = feed[index].userLiked
        userLiked = !userLiked!
        if userLiked == false {
            likesCount! -= 1
        }else{
            likesCount! += 1
        }
        feed[index].likesCount = likesCount
        feed[index].userLiked = userLiked
        onHeartButtonClick(feed[index])
        let position = IndexPath(row: index, section: 0)
        UIView.performWithoutAnimation {
            self.collectionView?.reloadItems(at: [position])
        }
        
    }
    
    @objc func likesClick(sender: UITapGestureRecognizer){
        let index = sender.view!.tag
        itemClickHandler(feed[index])
    }
    
    @objc func itemImageViewClick(sender: UITapGestureRecognizer){
        let index = sender.view!.tag
        onItemImageViewClick(feed[index])
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom >= height {
            scrollDelegate?.onScroll(scrollValue: scrollView.contentOffset.y)
        }
        if self.listingFooterCollectionViewCell.footerActivityIndicator != nil {
            if distanceFromBottom < height && contentYoffset > 0{
                loadMoreDelegate?.onLoadMore()
            }
        }
        
        if scrollView.contentOffset.y < -55 { //change 100 to whatever you want
            
            if canRefresh && !(self.collectionView?.refreshControl?.isRefreshing ?? false) {
                
                self.canRefresh = false
                self.refreshControl.beginRefreshing()
                
                self.refreshOtherUserProfileDelegate?.refreshOtherUserProfile()
            }
        }else if scrollView.contentOffset.y <= -40 {
            
            self.canRefresh = true
        }
        
    }
}
