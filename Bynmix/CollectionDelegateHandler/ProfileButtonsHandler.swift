import Foundation
import UIKit

class ProfileButtonsHandler : NSObject, UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
    
    var profileCell = "profile_cell"
    var profileOptions = AppConstants.StaticLists.profile
    
    var itemClickHandler: ((Int) -> Void)!
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        itemClickHandler(indexPath.item)
    }
    
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width * 0.95
        let screenHeight = screenSize.width * 0.87
        let height: CGFloat = screenHeight / 3
        let width: CGFloat = screenWidth / 3
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.profileOptions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: profileCell, for: indexPath as IndexPath) as! ProfileCollectionViewCell
        let image = UIImage(named: profileOptions[indexPath.item])
        cell.profileImageView.image = image
        cell.profileImageView.contentMode = .scaleAspectFit
        
        return cell
    }
}
