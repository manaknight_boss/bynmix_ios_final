import UIKit

class SalePurchaseHandler: NSObject,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var isLoadMoreRequired = true
    var listingFooterCollectionViewCell = ListingFooterCollectionViewCell()
    var scrollDelegate: FeedScrollProtocol?
    var loadMoreDelegate: LoadMoreProtocol?
    var salePurchaseCell = "SalePurchaseCell"
    var saleDetail = [SalesDetail]()
    var purchaseDetail = [PurchaseDetail]()
    var TYPE:Int?
    var listingFooterCell = "salePurchaseFooterCell"
    var collectionView:UICollectionView?
    var onSaleDetailClick:((SalesDetail,Int) -> Void)!
    var onPurchaseDetailClick:((PurchaseDetail,Int) -> Void)!
    var onRelistItemSaleClick:((SalesDetail) -> Void)!
    var onFeedbackPurchaseClick:((PurchaseDetail) -> Void)!
    var onFeedbackSaleClick:((SalesDetail) -> Void)!
    var onButtonTwoSaleClick:((SalesDetail) -> Void)!
    var onButtonTwoPurchaseClick:((PurchaseDetail) -> Void)!
    var onReportAnIssuePurchaseClick:((PurchaseDetail) -> Void)!
    var onReportAnIssueSaleClick:((SalesDetail) -> Void)!
    var onCancelOrderSaleClick:((SalesDetail) -> Void)!
    var onCancelOrderPurchaseClick:((PurchaseDetail) -> Void)!
    var onDisputeSaleClick:((SalesDetail) -> Void)!
    var onDisputePurchaseClick:((PurchaseDetail) -> Void)!
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if (kind == UICollectionView.elementKindSectionFooter) {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: listingFooterCell, for: indexPath) as! ListingFooterCollectionViewCell
            listingFooterCollectionViewCell = footerView
            if isLoadMoreRequired {
                footerView.footerActivityIndicator.isHidden = false
                
            } else {
                footerView.footerActivityIndicator.isHidden = true
            }
            
            return footerView
        }
        fatalError()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = collectionView.frame.size.width
        var height:CGFloat = 0
        if TYPE != AppConstants.TYPE_PURCHASE {
            height = 30
        }
        let cellWidth = (width * 0.50) - 7.5
        return CGSize(width: cellWidth , height: 315 + height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.collectionView = collectionView
        if TYPE == AppConstants.TYPE_PURCHASE{
            return purchaseDetail.count
        }else{
            return saleDetail.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: salePurchaseCell, for: indexPath) as! SalePurchaseCollectionViewCell
        var purchase:PurchaseDetail?
        var sale:SalesDetail?
        
        var statusText:String = ""
        var subStatusText:String = ""
        
        cell.topView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        cell.topView.backgroundColor = UIColor.cD53A95
        let image =  UIImage(named: AppConstants.Images.ITEM_PLACEHOLDER)!
        cell.relistItemButton.tag = indexPath.row
        cell.relistItemButton.addTarget(self, action: #selector(onRelistItemClickPage), for: .touchUpInside)
        
        cell.confirmDeliveryView.isHidden = true
        
        if TYPE == AppConstants.TYPE_PURCHASE{
            purchase = purchaseDetail[indexPath.item] as PurchaseDetail
            cell.itemImageView.tag = indexPath.row
            let tap = UITapGestureRecognizer(target: self, action: #selector(onPurchaseDetailClickPage))
            cell.itemImageView.addGestureRecognizer(tap)
            cell.itemImageView.isUserInteractionEnabled = true
            
            cell.leaveFeedBackButton.tag = indexPath.row
            cell.leaveFeedBackButton.addTarget(self, action: #selector(onFeedbackPurchaseClickPage), for: .touchUpInside)
            
            cell.reportAnIssueButton.tag = indexPath.row
            cell.reportAnIssueButton.addTarget(self, action: #selector(onReportAnIssuePurchaseClickAction), for: .touchUpInside)
            
            cell.cancelOrderButton.tag = indexPath.row
            cell.cancelOrderButton.addTarget(self, action: #selector(onCancelOrderPurchaseClickAction), for: .touchUpInside)
            
            cell.disputeDetailButton.tag = indexPath.row
            cell.disputeDetailButton.addTarget(self, action: #selector(onDisputePurchaseClickAction), for: .touchUpInside)
            
            cell.buttonTwoView.tag = indexPath.row
            let buttonTwoTap = UITapGestureRecognizer(target: self, action: #selector(onButtonTwoPurchaseClickDialog))
            cell.buttonTwoView.addGestureRecognizer(buttonTwoTap)
            
            let price:String = String(Int(purchase!.purchasePrice!.rounded()))
            
            let text:String = AppConstants.Strings.PRICE + AppConstants.Strings.DOLLAR_SIGN + price
            let textColor:String = AppConstants.Strings.DOLLAR_SIGN + price
            let range = (text as NSString).range(of: textColor)
            let attributedString = NSMutableAttributedString(string: text)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 12), range: range)
            cell.priceLabel.attributedText = attributedString
            
            cell.titleLabel.text = purchase!.listingTitle
            cell.dateLabel.text = AppConstants.PURCHASED + purchase!.purchaseDateFormatted!
            cell.relistItemButton.isHidden = true
            statusText = purchase!.purchaseStatus!.uppercased()
            subStatusText = purchase!.purchaseSubStatus ?? "".uppercased()
            
            if let imageUrl = purchase!.listingImageUrl{
                cell.itemImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
            }
            if (purchase!.canOpenDispute! || purchase!.hasOpenDispute! || purchase!.hasViewableDispute!){
                cell.reportAnIssueButton.backgroundColor = UIColor.black
                cell.reportAnIssueButton.isEnabled = true
            }else{
                cell.reportAnIssueButton.backgroundColor = UIColor.cCBCBCB
                cell.reportAnIssueButton.isEnabled = false
            }
            if purchase!.canLeaveFeedback!{
                cell.leaveFeedBackButton.backgroundColor = UIColor.cBB189C
                cell.leaveFeedBackButton.isEnabled = true
            }else{
                cell.leaveFeedBackButton.backgroundColor = UIColor.cCBCBCB
                cell.leaveFeedBackButton.isEnabled = false
            }
            
        }else{
            sale = saleDetail[indexPath.item] as SalesDetail
            cell.itemImageView.tag = indexPath.item
            let tap = UITapGestureRecognizer(target: self, action: #selector(onSaleDetailClickPage))
            cell.itemImageView.addGestureRecognizer(tap)
            cell.itemImageView.isUserInteractionEnabled = true
            
            cell.leaveFeedBackButton.tag = indexPath.row
            cell.leaveFeedBackButton.addTarget(self, action: #selector(onFeedbackSaleClickPage), for: .touchUpInside)
            
            cell.buttonTwoView.tag = indexPath.row
            let buttonTwoTap = UITapGestureRecognizer(target: self, action: #selector(onButtonTwoSaleClickDialog))
            cell.buttonTwoView.addGestureRecognizer(buttonTwoTap)
            
            cell.reportAnIssueButton.tag = indexPath.row
            cell.reportAnIssueButton.addTarget(self, action: #selector(onReportAnIssueSaleClickAction), for: .touchUpInside)
            
            cell.cancelOrderButton.tag = indexPath.row
            cell.cancelOrderButton.addTarget(self, action: #selector(onCancelOrderSaleClickAction), for: .touchUpInside)
            
            cell.disputeDetailButton.tag = indexPath.row
            cell.disputeDetailButton.addTarget(self, action: #selector(onDisputeSaleClickAction), for: .touchUpInside)
            
            let price:String = String(Int(sale!.salePrice!.rounded()))
            
            let text:String = AppConstants.Strings.PRICE + AppConstants.Strings.DOLLAR_SIGN + price
            let textColor:String = AppConstants.Strings.DOLLAR_SIGN + price
            let range = (text as NSString).range(of: textColor)
            let attributedString = NSMutableAttributedString(string: text)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 12), range: range)
            cell.priceLabel.attributedText = attributedString
            
            cell.titleLabel.text = sale!.listingTitle
            cell.dateLabel.text = AppConstants.SOLD_ON + sale!.saleDateFormatted!
            statusText = sale!.saleStatus!.uppercased()
            subStatusText = sale!.saleSubStatus ?? "".uppercased()
            
            if let imageUrl = sale!.listingImageUrl{
                cell.itemImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
            }
            
            if (sale!.canOpenDispute! || sale!.hasOpenDispute! || sale!.hasViewableDispute!){
                cell.reportAnIssueButton.backgroundColor = UIColor.black
                cell.reportAnIssueButton.isEnabled = true
            }else{
                cell.reportAnIssueButton.backgroundColor = UIColor.cCBCBCB
                cell.reportAnIssueButton.isEnabled = false
            }
            
            if sale!.canLeaveFeedback!{
                cell.leaveFeedBackButton.backgroundColor = UIColor.cBB189C
                cell.leaveFeedBackButton.isEnabled = true
            }else{
                cell.leaveFeedBackButton.backgroundColor = UIColor.cCBCBCB
                cell.leaveFeedBackButton.isEnabled = false
            }
            
        }
        
        if TYPE == AppConstants.TYPE_PURCHASE{
            let imageUrl = purchase?.iconImageUrlShort ?? ""
            if purchase?.iconImageUrlShort != ""{
                let image = AppConstants.IMAGES_URL + imageUrl + AppConstants.IMAGES_EXTENSION
                cell.itemStatusImageView.kf.setImage(with: URL(string: image)!)
            }
        }else{
            let imageUrl = sale?.iconImageUrlShort ?? ""
            let image = AppConstants.IMAGES_URL + imageUrl + AppConstants.IMAGES_EXTENSION
            cell.itemStatusImageView.kf.setImage(with: URL(string: image)!)
        }
        
        if TYPE == AppConstants.TYPE_PURCHASE{
            
            if (purchase!.hasSubStatus!) {
                cell.withoutSubHeadingView.isHidden = true
                cell.withSubStatusView.isHidden = false
                cell.withSubStatusItemStatusLabel.text = statusText
                cell.withSubStatusItemSubStatusLabel.text = subStatusText
                cell.withSubStatusView.backgroundColor = UIColor.white
                cell.returnDeliveredImageView.isHidden = true
            } else {
                cell.itemStatusLabel.text = statusText
                cell.withSubStatusView.isHidden = true
                cell.withoutSubHeadingView.isHidden = false
                cell.withoutSubHeadingView.backgroundColor = UIColor.cDC4293
                cell.withoutSubHeadingInnerView.backgroundColor = UIColor.cDC4293
            }
            
            if (purchase!.hasOpenDispute!) {
                cell.disputeDetailButton.isHidden = false
                cell.reportAnIssueButton.isHidden = true
            } else {
                if(purchase!.hasViewableDispute!) {
                    cell.disputeDetailButton.isHidden = false
                    cell.reportAnIssueButton.isHidden = true
                } else {
                    cell.reportAnIssueButton.isHidden = false
                    cell.disputeDetailButton.isHidden = true
                }
            }
            
            if (purchase!.pointsStatusId! > 0) {
                cell.pointsImageView.isHidden = false
                cell.pointsLabel.isHidden = false
                if (purchase!.pointsStatusId! == 1) {
                    cell.pointsImageView.image = UIImage(named:AppConstants.Images.POINTS_BLUE_ICON)
                    cell.pointsLabel.textColor = UIColor.white
                    cell.pointsLabel.text = String(purchase!.points ?? 0) + AppConstants.Strings.POINTS
                } else if (purchase!.pointsStatusId! == 2) {
                    cell.pointsImageView.image = UIImage(named:AppConstants.Images.POINTS_GREEN_ICON)
                    cell.pointsLabel.textColor = UIColor.white
                    cell.pointsLabel.text = String(purchase!.points ?? 0) + AppConstants.Strings.POINTS
                } else if (purchase!.pointsStatusId! == 3) {
                    cell.pointsImageView.image = UIImage(named:AppConstants.Images.POINTS_WHITE_ICON)
                    cell.pointsLabel.textColor = UIColor.c212121
                    cell.pointsLabel.text = String(purchase!.points ?? 0) + AppConstants.Strings.POINTS
                } else if (purchase!.pointsStatusId! == 4) {
                    cell.pointsImageView.image = UIImage(named:AppConstants.Images.POINTS_WHITE_ICON)
                    cell.pointsLabel.textColor = UIColor.c212121
                    cell.pointsLabel.text = String(purchase!.points ?? 0) + AppConstants.Strings.POINTS
                }
            } else {
                cell.pointsImageView.isHidden = true
                cell.pointsLabel.isHidden = true
            }
            if (purchase!.canCancelPurchase!) {
                cell.cancelOrderButton.isHidden = false
                cell.leaveFeedBackButton.isHidden = true
                cell.ratingView.isHidden = true
            } else {
                cell.cancelOrderButton.isHidden = true
                cell.leaveFeedBackButton.isHidden = false
                cell.ratingView.isHidden = true
                if (purchase!.rating ?? 0 > 0) {
                    cell.cancelOrderButton.isHidden = true
                    cell.leaveFeedBackButton.isHidden = true
                    cell.cancelOrderButton.isOpaque = true
                    cell.leaveFeedBackButton.isOpaque = true
                    cell.ratingView.isHidden = false
                    
                    let rating = purchase!.rating!
                    if rating == 1{
                        cell.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starTwoImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        cell.starThreeImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        cell.starFourImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        cell.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    }else if rating == 2{
                        cell.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starThreeImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        cell.starFourImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        cell.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    }else if rating == 3{
                        cell.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starThreeImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starFourImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        cell.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    }else if rating == 4{
                        cell.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starThreeImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starFourImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    }else{
                        cell.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starThreeImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starFourImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starFiveImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    }
                    
                } else {
                    cell.cancelOrderButton.isHidden = true
                    cell.leaveFeedBackButton.isHidden = false
                    cell.cancelOrderButton.isOpaque = true
                    cell.ratingView.isOpaque = true
                    cell.ratingView.isHidden = true
                }
            }
        }else{
            if (sale!.hasSubStatus!) {
                cell.withoutSubHeadingView.isHidden = true
                
                cell.withSubStatusItemStatusLabel.text = statusText
                cell.withSubStatusItemSubStatusLabel.text = subStatusText
                
                if sale?.saleStatusId == AppConstants.RETURN_ORDER_DELIVERED{
                    
                    cell.confirmDeliveryView.isHidden = false
                    cell.withSubStatusView.isHidden = true
                    
                    cell.confirmDeliveryView.backgroundColor = UIColor.cDC4293
                    cell.confirmDeliveryStatusLabel.textColor = UIColor.white
                    cell.confirmDeliverySubStatusLabel.textColor = UIColor.white
                    cell.confirmDeliveryImageView.isHidden = false
                    cell.confirmDeliveryStatusLabel.text = statusText
                    cell.confirmDeliverySubStatusLabel.text = subStatusText
                    
                }else{
                    cell.confirmDeliveryView.isHidden = true
                    cell.withSubStatusView.isHidden = false
                    cell.withSubStatusView.backgroundColor = UIColor.white
                    cell.withSubStatusItemStatusLabel.textColor = UIColor.c232323
                    cell.withSubStatusItemSubStatusLabel.textColor = UIColor.c232323
                    cell.returnDeliveredImageView.isHidden = true
                }
    
            } else {
                cell.itemStatusLabel.text = statusText
                cell.withSubStatusView.isHidden = true
                cell.withoutSubHeadingView.isHidden = false
                cell.withoutSubHeadingView.backgroundColor = UIColor.cDC4293
                cell.withoutSubHeadingInnerView.backgroundColor = UIColor.cDC4293
            }
            
            if (sale!.hasOpenDispute!) {
                cell.disputeDetailButton.isHidden = false
                cell.reportAnIssueButton.isHidden = true
            } else {
                if(sale!.hasViewableDispute!) {
                    cell.disputeDetailButton.isHidden = false
                    cell.reportAnIssueButton.isHidden = true
                } else {
                    cell.reportAnIssueButton.isHidden = false
                    cell.disputeDetailButton.isHidden = true
                }
            }
            
            
            
            if (sale!.pointsStatusId! > 0) {
                cell.pointsImageView.isHidden = false
                cell.pointsLabel.isHidden = false
                if (sale!.pointsStatusId! == 1) {
                    cell.pointsImageView.image = UIImage(named:AppConstants.Images.POINTS_BLUE_ICON)
                    cell.pointsLabel.textColor = UIColor.white
                    cell.pointsLabel.text = String(sale!.points ?? 0) + AppConstants.Strings.POINTS
                } else if (sale!.pointsStatusId! == 2) {
                    cell.pointsImageView.image = UIImage(named:AppConstants.Images.POINTS_GREEN_ICON)
                    cell.pointsLabel.textColor = UIColor.white
                    cell.pointsLabel.text = String(sale!.points ?? 0) + AppConstants.Strings.POINTS
                } else if (sale!.pointsStatusId! == 3) {
                    cell.pointsImageView.image = UIImage(named:AppConstants.Images.POINTS_WHITE_ICON)
                    cell.pointsLabel.textColor = UIColor.c212121
                    cell.pointsLabel.text = String(sale!.points ?? 0) + AppConstants.Strings.POINTS
                } else if (sale!.pointsStatusId! == 4) {
                    cell.pointsImageView.image = UIImage(named:AppConstants.Images.POINTS_WHITE_ICON)
                    cell.pointsLabel.textColor = UIColor.c212121
                    cell.pointsLabel.text = String(sale!.points ?? 0) + AppConstants.Strings.POINTS
                }
            } else {
                cell.pointsImageView.isHidden = true
                cell.pointsLabel.isHidden = true
            }
            
            if (sale!.canCancelSale!) {
                cell.cancelOrderButton.isHidden = false
                cell.leaveFeedBackButton.isHidden = true
                cell.ratingView.isHidden = true
            } else {
                cell.cancelOrderButton.isHidden = true
                cell.leaveFeedBackButton.isHidden = false
                cell.ratingView.isHidden = true
                
                if (sale!.rating ?? 0 > 0) {
                    cell.cancelOrderButton.isHidden = true
                    cell.leaveFeedBackButton.isHidden = true
                    cell.ratingView.isHidden = false
                    let rating = sale!.rating!
                    
                    if rating == 1{
                        cell.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starTwoImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        cell.starThreeImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        cell.starFourImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        cell.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    }else if rating == 2{
                        cell.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starThreeImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        cell.starFourImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        cell.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    }else if rating == 3{
                        cell.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starThreeImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starFourImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        cell.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    }else if rating == 4{
                        cell.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starThreeImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starFourImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    }else{
                        cell.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starThreeImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starFourImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        cell.starFiveImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    }
                    
                    
                } else {
                    cell.cancelOrderButton.isHidden = true
                    cell.leaveFeedBackButton.isHidden = false
                    cell.cancelOrderButton.isOpaque = true
                    cell.ratingView.isOpaque = true
                    cell.ratingView.isHidden = true
                }
                
            }
        }
        
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYOffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYOffset
        if distanceFromBottom >= height {
            scrollDelegate?.onScroll(scrollValue: scrollView.contentOffset.y)
        }
        if self.listingFooterCollectionViewCell.footerActivityIndicator != nil {
            if distanceFromBottom < height && contentYOffset > 0{
                loadMoreDelegate?.onLoadMore()
            }
        }
    }
    
    @objc func onButtonTwoSaleClickDialog(sender:UITapGestureRecognizer){
        let index = sender.view!.tag
        onButtonTwoSaleClick(saleDetail[index])
    }
    
    @objc func onButtonTwoPurchaseClickDialog(sender:UITapGestureRecognizer){
        let index = sender.view!.tag
        onButtonTwoPurchaseClick(purchaseDetail[index])
    }
    
    @objc func onFeedbackPurchaseClickPage(sender:UIButton){
        let index = sender.tag
        onFeedbackPurchaseClick(purchaseDetail[index])
    }
    
    @objc func onFeedbackSaleClickPage(sender:UIButton){
        let index = sender.tag
        onFeedbackSaleClick(saleDetail[index])
    }
    
    @objc func onPurchaseDetailClickPage(sender:UITapGestureRecognizer){
        let index = sender.view!.tag
        onPurchaseDetailClick(purchaseDetail[index],index)
    }
    
    @objc func onSaleDetailClickPage(sender:UITapGestureRecognizer){
        let index = sender.view!.tag
        onSaleDetailClick(saleDetail[index],index)
    }
    
    @objc func onRelistItemClickPage(sender:UIButton){
        let index = sender.tag
        onRelistItemSaleClick(saleDetail[index])
    }
    @objc func onReportAnIssuePurchaseClickAction(sender:UIButton){
        let index = sender.tag
        onReportAnIssuePurchaseClick(purchaseDetail[index])
    }
    
    @objc func onReportAnIssueSaleClickAction(sender:UIButton){
        let index = sender.tag
        onReportAnIssueSaleClick(saleDetail[index])
    }
    
    @objc func onCancelOrderPurchaseClickAction(sender:UIButton){
        let index = sender.tag
        onCancelOrderPurchaseClick(purchaseDetail[index])
    }
    
    @objc func onCancelOrderSaleClickAction(sender:UIButton){
        let index = sender.tag
        onCancelOrderSaleClick(saleDetail[index])
    }
    
    @objc func onDisputePurchaseClickAction(sender:UIButton){
        let index = sender.tag
        onDisputePurchaseClick(purchaseDetail[index])
    }
    
    @objc func onDisputeSaleClickAction(sender:UIButton){
        let index = sender.tag
        onDisputeSaleClick(saleDetail[index])
    }
    
}
