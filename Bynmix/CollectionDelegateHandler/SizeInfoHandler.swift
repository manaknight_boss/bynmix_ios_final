import UIKit

class SizeInfoHandler: NSObject,UICollectionViewDelegate,UICollectionViewDataSource {
    
    var sizeInfoData:[Sizes] = []
    var onSizesClick: ((Int) -> Void)!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sizeInfoData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell :SizeCellCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "dataCollectionCellId", for: indexPath) as! SizeCellCollectionViewCell
        cell.sizeBtn.setTitle(self.sizeInfoData[indexPath.row].sizeName, for: .normal)
        cell.sizeBtn.tag = self.sizeInfoData[indexPath.row].sizeId!
        
        if sizeInfoData[indexPath.row].isSelected! {
            cell.layer.borderWidth = 4
            cell.layer.borderColor = UIColor.cBB189C.cgColor
            cell.layer.cornerRadius = 2
        } else {
            cell.layer.borderWidth = 2.3
            cell.layer.borderColor = UIColor.c464646.cgColor
            cell.layer.cornerRadius = 2
        }
        
        let cellTap = UITapGestureRecognizer(target: self, action: #selector(onSizesTap))
        cell.sizeBtn.addGestureRecognizer(cellTap)
        
        return cell
    }
    
    @objc func onSizesTap(sender:UITapGestureRecognizer){
        let id = sender.view!.tag
        onSizesClick(id)
    }
    
}
