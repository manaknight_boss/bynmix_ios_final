import UIKit

class SliderHeaderHandler: NSObject,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource  ,UICollectionViewDelegate {
    
    var images:[SliderImages] = []
    var pageControl:UIPageControl?
    var commentsCell = "imageCell"
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        var width:CGFloat = 0
        width  = screenWidth/CGFloat(images.count)
        var extraSpace : CGFloat = 2.0
        if (images.count == 1){
            extraSpace = 0.0
        }
        return CGSize(width: width - extraSpace, height: 10)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: commentsCell, for: indexPath as IndexPath) as! ImageHeaderCollectionViewCell
        
        if pageControl!.currentPage == indexPath.item {
            cell.imageHeaderView.backgroundColor = UIColor.cBB189C
        } else {
            cell.imageHeaderView.backgroundColor = UIColor.cB8B8B8
        }
        return cell
    }
    
}
