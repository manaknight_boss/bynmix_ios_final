import Foundation
import UIKit

class SocialIconsHandler : NSObject, UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
    
    var socialCell = "social_cell"
    var socialOptions = [SocialLink]()
    
    var itemClickHandler: ((SocialLink) -> Void)!
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        itemClickHandler(socialOptions[indexPath.item])
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.socialOptions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: socialCell, for: indexPath as IndexPath) as! SocialIconCollectionViewCell
        var image: UIImage
        if socialOptions[indexPath.item].type == AppConstants.Social.FACEBOOK {
            image = UIImage(named: AppConstants.Images.FACEBOOK_ICON)!
        }
        else if socialOptions[indexPath.item].type == AppConstants.Social.YOUTUBE {
            image = UIImage(named: AppConstants.Images.SOCIAL_LINK_CIRCLE_YOUTUBE_ICON)!
        }
        else if socialOptions[indexPath.item].type == AppConstants.Social.TIKTOK {
            image = UIImage(named: AppConstants.Images.SOCIAL_LINK_CIRCLE_TIKTOK_ICON)!
        }
        else if socialOptions[indexPath.item].type == AppConstants.Social.PINTEREST{
            image = UIImage(named: AppConstants.Images.PINTEREST_ICON)!
        }
        else {
            image = UIImage(named: AppConstants.Images.INSTAGRAM_ICON)!
        }
        
        cell.socialImageView.image = image
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let totalCellWidth = 30 * collectionView.numberOfItems(inSection: 0)
        let totalSpacingWidth = 10 * (collectionView.numberOfItems(inSection: 0) - 1)
        
        let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
        
    }
}
