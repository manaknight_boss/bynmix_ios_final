import UIKit

class UserBynHandler: NSObject,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource{
    var feed = [Feed]()
    var userBynCell = "UserMyBynCell"
    var itemClickHandler: ((Feed) -> Void)!
    var onItemImageViewClick:((Feed) -> Void)!
    var loadMoreDelegate: LoadMoreProtocol?
    var scrollDelegate: FeedScrollProtocol?
    var isLoadMoreRequired = true
    var listingFooterCell = "bynListingFooterCell"
    var listingFooterCollectionViewCell = ListingFooterCollectionViewCell()
    var onEnableDisableButtonClick: ((Feed) -> Void)!
    var onDeleteButtonClick:((Feed) -> Void)!
    var collectionView:UICollectionView?
    var onEditButtonClick:((Feed) -> Void)!
    
    func updateStatus(id: Int) {
        for i in 0..<self.feed.count {
            if self.feed[i].id == id {
                self.feed[i].isActive = !feed[i].isActive!
            }
        }
    }
    
    func deleteStatus(id: Int) {
        var index = -1
        for i in 0..<self.feed.count {
            if self.feed[i].id == id {
                index = i
                break
            }
        }
        self.feed.remove(at: index)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if (kind == UICollectionView.elementKindSectionFooter) {
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: listingFooterCell, for: indexPath) as! ListingFooterCollectionViewCell
           listingFooterCollectionViewCell = footerView
            if isLoadMoreRequired {
                footerView.footerActivityIndicator.isHidden = false
            } else {
                footerView.footerActivityIndicator.isHidden = true
            }
            return footerView
        }
        fatalError()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width = collectionView.frame.size.width
        let cellWidth = (width * 0.50) - 7.5
        let cellHeight = (width * 0.60)
        return CGSize(width: cellWidth , height: cellHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.collectionView = collectionView
        return feed.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: userBynCell, for: indexPath) as! UserBynCollectionViewCell
        
        let feed = self.feed[indexPath.item] as Feed
        
        cell.topView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        cell.topView.backgroundColor = UIColor.cD53A95
        let type = feed.type
        var image = UIImage(named: AppConstants.Images.DEFAULT_USER)
        if type == AppConstants.LISTING {
            image =  UIImage(named: AppConstants.Images.ITEM_PLACEHOLDER)!
        }
        
        let text:String = AppConstants.Strings.SIZE + (feed.size ?? "")
        let textRegular:String = AppConstants.Strings.SIZE
        let range = (text as NSString).range(of: textRegular)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 10), range: range)
        
        let priceText:String = AppConstants.Strings.PRICE + AppConstants.Strings.DOLLAR_SIGN + String(Int(feed.price ?? 0))
        let textPriceRegular:String = AppConstants.Strings.PRICE
        let priceRange = (priceText as NSString).range(of: textPriceRegular)
        let attributedPriceString = NSMutableAttributedString(string: priceText)
        attributedPriceString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 10), range: priceRange)
        
        
        let brandText:String = AppConstants.Strings.BRAND + feed.brand!
        let textBrandRegular:String = AppConstants.Strings.BRAND
        let brandRange = (brandText as NSString).range(of: textBrandRegular)
        let attributedBrandString = NSMutableAttributedString(string: brandText)
        attributedBrandString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 10), range: brandRange)
        
        cell.titleLabel.text = feed.title
        cell.dateLabel.text = feed.createdDateFormatted
        
        cell.itemImageView.layer.zPosition = -1
        if let imageUrl = feed.photoUrl{
            if imageUrl != "" {
                cell.itemImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
            } else {
                cell.itemImageView.image = image
            }
        }
        
        cell.titleLabel.numberOfLines = 1
        
        cell.middleView.tag = indexPath.row
        let itemImageTap = UITapGestureRecognizer(target: self, action: #selector(itemImageViewClick))
        cell.middleView.addGestureRecognizer(itemImageTap)
        
        let feedStatus = feed.status!.lowercased()
        
        switch feedStatus {
        case AppConstants.SOLD:
            cell.itemStatusImageView.image = UIImage(named: AppConstants.Images.ITEM_SOLD_ICON)
            cell.itemStatusImageView.isHidden = false
            cell.buttonsView.isHidden = true
            cell.itemStatusImageView.isHidden = false
            cell.deleteButtonView.isHidden = false
            break;
        case AppConstants.ON_HOLD:
            cell.itemStatusImageView.image = UIImage(named: AppConstants.Images.ITEM_ON_HOLD_ICON)
            cell.itemStatusImageView.isHidden = false
            cell.buttonsView.isHidden = false
            cell.itemStatusImageView.isHidden = false
            cell.deleteButtonView.isHidden = true
            break;
        case AppConstants.NOT_FOR_SALE:
            cell.itemStatusImageView.image = UIImage(named: AppConstants.Images.ITEM_NOT_FOR_SALE_ICON)
            cell.itemStatusImageView.isHidden = false
            cell.buttonsView.isHidden = false
            cell.itemStatusImageView.isHidden = false
            cell.deleteButtonView.isHidden = true
            break;
        default:
            cell.itemStatusImageView.isHidden = true
            cell.deleteButtonView.isHidden = true
            cell.buttonsView.isHidden = false
            cell.itemStatusImageView.isHidden = true
        }
        
        if feed.isActive! {
            cell.enableDisableButton.setTitle(AppConstants.DISABLE, for: .normal)
            cell.enableDisableButton.backgroundColor = UIColor.cC80000.withAlphaComponent(0.80)
        }else{
            cell.enableDisableButton.setTitle(AppConstants.ENABLE, for: .normal)
            cell.enableDisableButton.backgroundColor = UIColor.cE17500.withAlphaComponent(0.80)
        }
        
        cell.smallDeleteButton.tag = indexPath.row
        cell.smallDeleteButton.addTarget(self, action: #selector(deleteButtonClick), for: .touchUpInside)
        
        cell.deleteButton.tag = indexPath.row
        cell.deleteButton.addTarget(self, action: #selector(deleteButtonClick), for: .touchUpInside)
        
        cell.enableDisableButton.tag = indexPath.row
        cell.enableDisableButton.addTarget(self, action: #selector(enableDisableButtonClick), for: .touchUpInside)
        
        cell.editButton.tag = indexPath.row
        cell.editButton.addTarget(self, action: #selector(editButtonClick), for: .touchUpInside)
        
        return cell
    }
    
    @objc func itemImageViewClick(sender: UITapGestureRecognizer){
         let index = sender.view!.tag
        onItemImageViewClick(feed[index])
    }
    
    @objc func editButtonClick(sender: UIButton){
        let index = sender.tag
        onEditButtonClick(feed[index])
    }
    
    @objc func deleteButtonClick(sender: UIButton){
        let index = sender.tag
        onDeleteButtonClick(feed[index])
    }
    
    @objc func enableDisableButtonClick(sender: UIButton){
        let index = sender.tag
        onEnableDisableButtonClick(feed[index])
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom >= height {
            scrollDelegate?.onScroll(scrollValue: scrollView.contentOffset.y)
        }
        if self.listingFooterCollectionViewCell.footerActivityIndicator != nil {
            if distanceFromBottom < height && contentYoffset > 0{
                loadMoreDelegate?.onLoadMore()
            }
        }
    }
    
}
