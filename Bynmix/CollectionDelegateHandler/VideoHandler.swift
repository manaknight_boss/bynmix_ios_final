import UIKit
import AVKit

class VideoHandler: NSObject,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    var imageDictionary = [String: AnyObject]()
    var videoDictionary = [String: AnyObject]()
    let coverVideoCell = "galleryCell"
    let cameraCell = "cameraCell"
    var onCameraClick:(() -> Void)!
    var videoFormatDelegate:VideoFormatProtocol?
    var videoAssetDelegate:VideoAssetProtocol?
    var videoURLDelegate:VideoURLProtocol?
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.imageDictionary.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let key = "\(indexPath.row)"
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: coverVideoCell,for: indexPath) as! CoverVideoCollectionViewCell
        
        cell.videoImageView.image = self.imageDictionary[key] as? UIImage
        let singleVideo = self.videoDictionary[key] as? [String: AnyObject]
        if key == "0"{
            cell.videoImageView.contentMode = .center
        }else{
            cell.videoImageView.contentMode = .scaleAspectFill
        }
        
        cell.timeLabel.text = singleVideo?["VideoDuration"] as? String
        cell.videoURLLabel.text = singleVideo?["VideoPath"]  as? String
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width * 0.995
        let width: CGFloat = screenWidth / 4
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let key = "\(indexPath.row)"
        
        let singleVideo = self.videoDictionary[key] as? [String: AnyObject]
        let asset = singleVideo?["VideoPath"]
        let videoUrl = singleVideo?["VideoURL"]
        if asset != nil{
            DispatchQueue.main.async {
                if let avAsset = asset {
                    self.videoAssetDelegate?.videoAsset(asset: avAsset as! AVAsset,duration:singleVideo?["VideoDuration"] as? String ?? "", index: indexPath.row)
                    if !(videoUrl is NSNull){
                        self.videoURLDelegate?.videoURL(url: videoUrl as! URL,index: indexPath.row)
                    }else{
                        self.videoFormatDelegate?.videoFormat(index: indexPath.row)
                    }
                }
            }
        }else{
            self.videoFormatDelegate?.videoFormat(index: indexPath.row)
        }
        
    }
    
//    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: self.cameraCell, for: indexPath) as! CameraCollectionReusableView
//
//        headerView.cameraImageView.contentMode = .scaleAspectFit
//
//        let cameraTap = UITapGestureRecognizer(target: self, action: #selector (onCameraViewClick))
//        headerView.addGestureRecognizer(cameraTap)
//
//        return headerView
//    }
    
//    @objc func onCameraViewClick(sender:UITapGestureRecognizer){
//        onCameraClick()
//    }
    
}
