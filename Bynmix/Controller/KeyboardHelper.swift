import UIKit

class KeyboardHelper {

    let ANIMATION_DURATION_OF_KEYBOARD = 0.3
    var keyboardHeight: CGFloat?
    var activeTextField: UITextField?
    var defaultTopConstraintValue: CGFloat = 0
    
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

}
