import UIKit
import XLPagerTabStrip

class BasicInfoViewController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension BasicInfoViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: "Item 1")
    }
}
