import UIKit

class FollowFollowingViewController: BaseViewController,UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var tableViewLayout: UITableView!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    
    var user = [User]()
    let userCell:String = "follow_following_cell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        screenActivityIndicator.stopAnimating()
        getfollowers()
        getfollowing()
    }
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return user.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: userCell, for: indexPath) as! followFollowingTableViewCell
        
        let users = user[indexPath.row] as User
        
        let image = UIImage(named: AppConstants.Images.DEFAULT_USER)
        cell.usernameLabel.text = users.username
        cell.titleLabel.text = users.title
        cell.styleLabel.text = users.userBrandsFormatted
        cell.brandsLabel.text = users.userBrandsFormatted
        cell.userImageView.layer.borderWidth = 2.0
        cell.userImageView.layer.masksToBounds = false
        cell.userImageView.layer.borderColor = UIColor.white.cgColor
        cell.userImageView.clipsToBounds = true
        cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.width/2
        
        if let imageUrl = users.photoUrl{
            cell.userImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        } else {
            cell.userImageView.image =  image
        }
        
        
        
        return cell
    }
    
    func getfollowers() {
        ProfileViewModel().getFollowers(accessToken: getAccessToken(), { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<User> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let userdata = data.data
                    if let users = userdata?.items {
                        self.user = users
                        self.screenActivityIndicator.isHidden = true
                        self.tableViewLayout.isHidden = false
                        self.tableViewLayout.reloadData()
                    }
                }
            } else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getfollowing() {
        ProfileViewModel().getFollowing(accessToken: getAccessToken(), { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<User> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let userdata = data.data
                    if let users = userdata?.items {
                        self.user = users
                        self.screenActivityIndicator.isHidden = true
                        self.tableViewLayout.isHidden = false
                        self.tableViewLayout.reloadData()
                    }
                }
            } else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
}
