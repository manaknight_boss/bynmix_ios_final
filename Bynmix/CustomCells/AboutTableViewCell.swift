import UIKit

class AboutTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionTwoLabel: UILabel!
    @IBOutlet weak var descriptionThreeLabel: UILabel!
    @IBOutlet weak var descriptionFourLabel: UILabel!
    @IBOutlet weak var descriptionFiveLabel: UILabel!
}
