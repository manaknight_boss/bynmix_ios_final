import UIKit

class AddTagsCollectionViewCell: UICollectionViewCell {
    @IBOutlet var tagNameLabel: UILabel!
    @IBOutlet var closeButton: UIButton!
}
