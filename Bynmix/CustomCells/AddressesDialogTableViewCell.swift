import UIKit

class AddressesDialogTableViewCell: UITableViewCell {

    @IBOutlet var defaultShippingAddressButton: UIButton!
    
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var addressOneLabel: UILabel!
    
    @IBOutlet var addressTwoLabel: UILabel!
    
    @IBOutlet var addressThreeLabel: UILabel!
    
   

}
