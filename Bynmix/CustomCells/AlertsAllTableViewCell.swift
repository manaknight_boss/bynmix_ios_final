import UIKit
import ActiveLabel

class AlertsAllTableViewCell: UITableViewCell {

    @IBOutlet var userImageView: UIImageView!
    @IBOutlet var userShadowView: UIView!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var statusImageView: UIImageView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var addBorderView: UIView!
    
}
