import UIKit

class BalanceHistoryTableViewCell: UITableViewCell {

    @IBOutlet var statusImageView: UIImageView!
    @IBOutlet var typeLabel: UILabel!
    @IBOutlet var amountLabel: UILabel!
    @IBOutlet var formatedDisplayLabel: UILabel!
    @IBOutlet var failedCanceledLabel: UILabel!
    @IBOutlet var confirmationCodeLabel: UILabel!
    @IBOutlet var failedCanceledView: UIView!
    
}
