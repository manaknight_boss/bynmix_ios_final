import UIKit

class BloggersCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var likesImage: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
}
