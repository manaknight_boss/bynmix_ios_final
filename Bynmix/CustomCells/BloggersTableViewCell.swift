import UIKit

class BloggersTableViewCell: UITableViewCell {

    @IBOutlet weak var userPhotoImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var userTitleLabel: UILabel!
    @IBOutlet weak var userStyleLabel: UILabel!
    @IBOutlet weak var userBrandsLabel: UILabel!
    @IBOutlet weak var userSelectButton: UIButton! 
}
