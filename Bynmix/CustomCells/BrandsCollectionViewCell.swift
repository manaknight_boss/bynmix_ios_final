import UIKit

class BrandsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var heartImage: UIImageView!
    @IBOutlet weak var brandLabel: UILabel!
    
}
