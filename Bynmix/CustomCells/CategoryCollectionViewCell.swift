import UIKit

class CategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet var categoryNameLabel: UILabel!
  
    @IBOutlet var categoryImageView: UIImageView!
    
    @IBOutlet var categoryView: UIView!
  
    @IBOutlet var shadowView: UIView!
    
}
