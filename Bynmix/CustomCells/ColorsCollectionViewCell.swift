import UIKit

class ColorsCollectionViewCell: UICollectionViewCell {
    @IBOutlet var colorNameLabel: UILabel!
    
    @IBOutlet var colorFillView: UIView!
    
    @IBOutlet var colorsView: UIView!
    
    @IBOutlet var shadowView: UIView!
    
    @IBOutlet var colorShadowView: UIView!
    
    
}
