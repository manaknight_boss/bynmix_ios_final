import UIKit
import ActiveLabel

class CommentsTableViewCell: UITableViewCell {

    @IBOutlet var userImageView: UIImageView!
    @IBOutlet var userShadowView: UIView!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var replyButton: UIButton!
    @IBOutlet var hilightedView: UIView!
    @IBOutlet var InnerView: UIView!
    @IBOutlet var commentLabel: ActiveLabel!
    
}
