import UIKit

class CoverVideoCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var videoURLLabel: UILabel!
    
}
