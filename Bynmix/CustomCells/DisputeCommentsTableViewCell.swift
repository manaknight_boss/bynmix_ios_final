import UIKit

class DisputeCommentsTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var cancelDisputeView: UIView!
    @IBOutlet weak var chargeDisputeCommentLabel: UILabel!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var buyerNameView: UIView!
    @IBOutlet weak var sellerNameView: UIView!
    @IBOutlet weak var triangleLeftImageView: UIImageView!
    @IBOutlet weak var triangleRightImageView: UIImageView!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet var buyerWidthConstraint: NSLayoutConstraint!
    @IBOutlet var buyerHeightConstraint: NSLayoutConstraint!
    @IBOutlet var sellerWidthConstraint: NSLayoutConstraint!
    @IBOutlet var sellerHeightConstraint: NSLayoutConstraint!
    @IBOutlet var sellerNameLabel: UILabel!
    @IBOutlet var buyerNameLabel: UILabel!
    @IBOutlet weak var disputeImagesView: UIView!
    @IBOutlet weak var disputeImageOne: UIImageView!
    @IBOutlet weak var disputeImageTwo: UIImageView!
    @IBOutlet weak var disputeImageThree: UIImageView!
    @IBOutlet var commentLabelTrailingConstraint: NSLayoutConstraint!
    @IBOutlet var imagesTrailingConstraint: NSLayoutConstraint!
    
    
}
