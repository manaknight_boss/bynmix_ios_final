import UIKit

class EnterTagCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var headerView: UIView!
    @IBOutlet var addTagLabel: UILabel!
    @IBOutlet var addTagTextField: UITextField!
    @IBOutlet var dataView: UIView!
    
}
