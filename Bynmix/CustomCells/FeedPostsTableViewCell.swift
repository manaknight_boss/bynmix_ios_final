import UIKit

class FeedPostsTableViewCell: UITableViewCell {

    @IBOutlet var topView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var itemImageView: UIImageView!
    @IBOutlet var likesView: UIView!
    @IBOutlet var likesShadowView: UIView!
    @IBOutlet var likesLabel: UILabel!
    @IBOutlet var userShadowView: UIView!
    @IBOutlet var userImageView: UIImageView!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var postTimeLabel: UILabel!
    @IBOutlet var heartButton: UIButton!
    @IBOutlet var shareButton: UIButton!
    @IBOutlet var middleView: UIView!
    @IBOutlet var likesStringLabel: UILabel!
    @IBOutlet var videoImageView: UIImageView!
    @IBOutlet weak var dotsButton: UIButton!
    

}
