import UIKit

class FilterSelectCategoryTableViewCell: UITableViewCell {

    @IBOutlet var categoryNameButton: UIButton!
    @IBOutlet var arrowImageView: UIImageView!
    
}
