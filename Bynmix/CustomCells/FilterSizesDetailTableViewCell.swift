import UIKit

class FilterSizesDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    var collectionViewOffset: CGFloat {
        set {
            collectionView.contentOffset.y = newValue
        }
        get {
            return collectionView.contentOffset.y
        }
    }
}
