import UIKit

class FollowCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var userImageView: UIImageView!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var shadowView: UIView!
}
