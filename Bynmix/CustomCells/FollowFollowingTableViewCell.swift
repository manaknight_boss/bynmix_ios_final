import UIKit

class FollowFollowingTableViewCell: UITableViewCell {

    @IBOutlet var userImageView: UIImageView!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var styleLabel: UILabel!
    @IBOutlet var brandsLabel: UILabel!
    @IBOutlet var followFolllowingImageView: UIImageView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var userShadowView: UIView!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var topView:UIView!
    
}
