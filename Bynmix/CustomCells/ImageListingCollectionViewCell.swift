import UIKit

class ImageListingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var userImageView: UIImageView!
    @IBOutlet var removeButton: UIButton!
    
}
