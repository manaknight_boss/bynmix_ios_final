import UIKit

class InAppNotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var notificationImageView: UIImageView!
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var notificationBackgroundView: UIView!
    @IBOutlet var closeButtonWidth: NSLayoutConstraint!
    
}
