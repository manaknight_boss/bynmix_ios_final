import UIKit

class MentionsTableViewCell: UITableViewCell {
    
    var mentionItem : MentionItem? {
        didSet{
            userNameLabel.text = mentionItem?.username
        }
    }

    @IBOutlet var userNameLabel:UILabel!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet var fullNameLabel:UILabel!
}
