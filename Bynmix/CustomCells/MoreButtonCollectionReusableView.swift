import UIKit

class MoreButtonCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet var addButton: UIImageView!
    
    @IBOutlet var addMoreLabel: UILabel!

    @IBOutlet var addMoreView: UIView!
    
}
