import UIKit

class MyWalletDefaultPaymentTableViewCell: UITableViewCell {

    @IBOutlet var brandImageView: UIImageView!
    @IBOutlet var brandNameWithCardNumberLabel: UILabel!
    @IBOutlet var cardHolderNameLabel: UILabel!
    @IBOutlet var expirationDateLabel: UILabel!
    @IBOutlet var editButton: UIButton!
    @IBOutlet var deleteButton: UIButton!
    
}
