import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet var notificationNameLabel: UILabel!
    @IBOutlet var notificationDescLabel: UILabel!
    @IBOutlet var notificationSwitch: UISwitch!
    
}
