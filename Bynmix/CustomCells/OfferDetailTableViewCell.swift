import UIKit

class OfferDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var offeredLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var rightDeclinedView: UIView!
    @IBOutlet weak var rightDeclinedLabel: UILabel!
    
    @IBOutlet weak var leftShadowView: UIView!
    @IBOutlet weak var leftItemImageView: UIImageView!
    @IBOutlet weak var leftUsernameLabel: UILabel!
    @IBOutlet weak var leftOfferedLabel: UILabel!
    @IBOutlet weak var leftPriceLabel: UILabel!
    @IBOutlet weak var leftDateLabel: UILabel!
    @IBOutlet weak var leftDeclinedView: UIView!
    @IBOutlet weak var leftDeclinedLabel: UILabel!
    @IBOutlet weak var leftHeaderView: UIView!
    @IBOutlet weak var leftCancelButton: UIButton!
    
    @IBOutlet weak var rightMainView: UIView!
    @IBOutlet weak var leftMainView: UIView!
    
}
