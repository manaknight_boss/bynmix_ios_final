import UIKit

class OnboardingBloggersCollectionViewCell: UICollectionViewCell {

    @IBOutlet var shadowView: UIView!
    @IBOutlet var userImageView: UIImageView!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var userDescLabel: UILabel!
    @IBOutlet var mainView: UIView!
    @IBOutlet var followView: UIView!
    @IBOutlet var followButton: UIButton!
    @IBOutlet var titleView: UIView!
    @IBOutlet var topView: UIView!
    @IBOutlet var titleViewHeight: NSLayoutConstraint!
    
}
