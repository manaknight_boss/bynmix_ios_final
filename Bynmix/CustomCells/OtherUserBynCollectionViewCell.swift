import UIKit

class OtherUserBynCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var topView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var itemImageView: UIImageView!
    @IBOutlet var likesView: UIView!
    @IBOutlet var likesShadowView: UIView!
    @IBOutlet var likesLabel: UILabel!
    @IBOutlet var heartButton: UIButton!
    @IBOutlet var middleView: UIView!
    @IBOutlet var likesStringLabel: UILabel!
    @IBOutlet var sizeLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var brandLabel: UILabel!
    @IBOutlet var itemStatusImageView: UIImageView!
}
