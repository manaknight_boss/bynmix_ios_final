import UIKit
import ExpandableLabel

class OtherUserPostTableViewCell: UITableViewCell {
  
    @IBOutlet var topView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var itemImageView: UIImageView!
    @IBOutlet var likesView: UIView!
    @IBOutlet var likesShadowView: UIView!
    @IBOutlet var likesLabel: UILabel!
    @IBOutlet var heartButton: UIButton!
    @IBOutlet var shareButton: UIButton!
    @IBOutlet var middleView: UIView!
    @IBOutlet var likesStringLabel: UILabel!
    @IBOutlet var dotsButton: UIButton!
    @IBOutlet var videoImageView: UIImageView!
    @IBOutlet var titleDescLabel: ExpandableLabel!
    @IBOutlet var titleDescView: UIView!
 
    override func prepareForReuse() {
        super.prepareForReuse()
        //titleDescLabel.numberOfLines = 3
        //titleDescLabel.collapsed = true
    }
}
