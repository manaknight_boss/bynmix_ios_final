import UIKit

class ReplaceCardTableViewCell: UITableViewCell {

    @IBOutlet var brandImageView: UIImageView!
    @IBOutlet var brandNameWithCardNumberLabel: UILabel!
    @IBOutlet var cardHolderNameLabel: UILabel!
    @IBOutlet var expirationDateLabel: UILabel!
    @IBOutlet var SelectButton: UIButton!

}
