import UIKit

class ReturnAddressTableViewCell: UITableViewCell {

    @IBOutlet var nameLabel: UILabel!
  
    @IBOutlet var addressOneLabel: UILabel!
    
    @IBOutlet var addressTwoLabel: UILabel!
    
    @IBOutlet var editButton: UIButton!
    
    @IBOutlet var preferredShippingLabel: UILabel!
    
    @IBOutlet var addressThreeLabel: UILabel!
    
    @IBOutlet var deleteButton:UIButton!
    
    @IBOutlet var addressesView: UIView!
    
    @IBOutlet var buttonsView: UIView!
    
    @IBOutlet var addNewAddressButton: UIButton!
    
    @IBOutlet var selectExistingAddress: UIButton!
    
    @IBOutlet var newAddressView: UIView!
    
    @IBOutlet var footerView: UIView!
    
    @IBOutlet weak var addNewFullButton: UIButton!
    
}
