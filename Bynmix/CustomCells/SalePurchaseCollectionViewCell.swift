import UIKit

class SalePurchaseCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var topView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var itemImageView: UIImageView!
    @IBOutlet var pointsImageView: UIImageView!
    @IBOutlet var pointsLabel: UILabel!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var cancelOrderButton: UIButton!
    @IBOutlet var leaveFeedBackButton: UIButton!
    @IBOutlet var ratingView: UIView!
    @IBOutlet var withoutSubHeadingView: UIView!
    @IBOutlet var withoutSubHeadingInnerView:UIView!
    @IBOutlet var itemStatusImageView: UIImageView!
    @IBOutlet var itemStatusLabel: UILabel!
    @IBOutlet var withSubStatusView: UIView!
    @IBOutlet var withSubStatusItemStatusLabel: UILabel!
    @IBOutlet var withSubStatusItemSubStatusLabel: UILabel!
    @IBOutlet var reportAnIssueButton: UIButton!
    @IBOutlet var disputeDetailButton: UIButton!
    @IBOutlet var relistItemButton: UIButton!
    @IBOutlet var buttonOneView: UIView!
    
    @IBOutlet var starOneImageView: UIImageView!
    @IBOutlet var starTwoImageView: UIImageView!
    @IBOutlet var starThreeImageView: UIImageView!
    @IBOutlet var starFourImageView: UIImageView!
    @IBOutlet var starFiveImageView: UIImageView!
    @IBOutlet var buttonTwoView: UIView!
    @IBOutlet weak var returnDeliveredImageView: UIImageView!
    
    @IBOutlet weak var confirmDeliveryView: UIView!
    @IBOutlet weak var confirmDeliveryImageView: UIImageView!
    @IBOutlet weak var confirmDeliveryStatusLabel: UILabel!
    @IBOutlet weak var confirmDeliverySubStatusLabel: UILabel!
    
}
