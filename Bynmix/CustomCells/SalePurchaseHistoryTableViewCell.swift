import UIKit

class SalePurchaseHistoryTableViewCell: UITableViewCell {
    
    @IBOutlet var postDateLabel: UILabel!
    @IBOutlet var topView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var soldDateLabel: UILabel!
    @IBOutlet var menuButton: UIButton!
    @IBOutlet var userImageView: UIImageView!
    @IBOutlet var originalPriceLabel: UILabel!
    @IBOutlet var promotionPriceLabel: UILabel!
    @IBOutlet var shippingPriceLabel: UILabel!
    @IBOutlet var pointsEarnedLabel: UILabel!
    @IBOutlet var promotionPointsLabel: UILabel!
    @IBOutlet var totalPriceLabel: UILabel!
    @IBOutlet var postDateView: UIView!
    @IBOutlet var shadowImageView: UIView!
    @IBOutlet var detailView: UIView!
    

}
