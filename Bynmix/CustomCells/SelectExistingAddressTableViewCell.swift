import UIKit

class SelectExistingAddressTableViewCell: UITableViewCell {

    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var addressOneLabel: UILabel!
    
    @IBOutlet var addressTwoLabel: UILabel!
    
    @IBOutlet var selectButton: UIButton!
    
    @IBOutlet var addressThreeLabel: UILabel!
    
    @IBOutlet var addressesView: UIView!
    
    @IBOutlet var footerView: UIView!
    
    @IBOutlet var footerViewHeight: NSLayoutConstraint!
    
}
