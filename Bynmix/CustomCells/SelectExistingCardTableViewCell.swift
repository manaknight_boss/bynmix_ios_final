import UIKit

class SelectExistingCardTableViewCell: UITableViewCell {

    @IBOutlet var brandImageView: UIImageView!
    @IBOutlet var brandNameWithCardNumberLabel: UILabel!
    @IBOutlet var cardHolderNameLabel: UILabel!
    @IBOutlet var expirationDateLabel: UILabel!
    @IBOutlet var SelectButton: UIButton!
    @IBOutlet var mainView: UIView!

}
