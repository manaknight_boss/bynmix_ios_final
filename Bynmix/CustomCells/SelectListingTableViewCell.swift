import UIKit

class SelectListingTableViewCell: UITableViewCell {

    @IBOutlet weak var listingImageView: UIImageView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var listingLabel: UILabel!

}
