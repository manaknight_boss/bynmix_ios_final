import UIKit

class SelectShippingMethodTableViewCell: UITableViewCell {

    @IBOutlet var rateLabel: UILabel!
    @IBOutlet var editImageView: UIImageView!
    @IBOutlet var mediumLabel: UILabel!
    @IBOutlet var deliveryLabel: UILabel!
    @IBOutlet var topView: UIView!
    @IBOutlet var bottomView: UIView!
    
}
