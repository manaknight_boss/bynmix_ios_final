import UIKit

class SelectShippingRateTableViewCell: UITableViewCell {

    @IBOutlet var shadowView: UIView!
    @IBOutlet var borderView: UIView!
    @IBOutlet var innerView: UIView!
    @IBOutlet var rateLabel: UILabel!
    @IBOutlet var classLabel: UILabel!
    @IBOutlet var weightLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    
}
