import UIKit

class SelectTagTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var tagView: UIView!
    @IBOutlet weak var dropDownView: UIView!
    @IBOutlet weak var dropDownButton: UIButton!
    @IBOutlet weak var dropDownContainerView: UIView!
    @IBOutlet weak var titleView: UIView!
    @IBOutlet weak var titletextField: UITextField!
    @IBOutlet weak var linkView: UIView!
    @IBOutlet weak var linkTextField: UITextField!
    @IBOutlet weak var listingButton: UIButton!
    @IBOutlet weak var selectTagTypeLabel: UILabel!
    @IBOutlet weak var titleErrorButton: UIButton!
    @IBOutlet weak var linkErrorButton: UIButton!
    
}
