import UIKit

class SelectedBrandsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var brandNameLabel:UILabel!
    @IBOutlet var heartImageView:UIImageView!
    
}
