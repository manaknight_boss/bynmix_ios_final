import UIKit

class SellerPolicyTableViewCell: UITableViewCell {

 
    @IBOutlet var titleLabel: UILabel!
    
    @IBOutlet var descriptionLabel: UILabel!
    
    @IBOutlet var topView: UIView!
}
