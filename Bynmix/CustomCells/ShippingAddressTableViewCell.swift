import UIKit

class ShippingAddressTableViewCell: UITableViewCell {

    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var addressOneLabel: UILabel!
    
    @IBOutlet var addressTwoLabel: UILabel!
    
    @IBOutlet var editButton: UIButton!
  
   @IBOutlet var preferredShippingLabel: UILabel!
}
