import UIKit

class ShippingTableViewCell: UITableViewCell {

    @IBOutlet var rateLabel: UILabel!
    @IBOutlet var classLabel: UILabel!
    @IBOutlet var weightLabel: UILabel!
    @IBOutlet var shadowView: UIView!
    @IBOutlet var shippingView: UIView!
    
}
