import UIKit

class SizeCellTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dataCollectionView: UICollectionView!
    var sizeInfoData:[Sizes] = []

    @IBOutlet weak var dataCollectionViewHeightConstraint: NSLayoutConstraint!
    func setCollectionViewDataSourceDelegate<D: UICollectionViewDataSource & UICollectionViewDelegate>(_ dataSourceDelegate: D, forRow row: Int) {
        
        dataCollectionView.delegate = dataSourceDelegate
        dataCollectionView.dataSource = dataSourceDelegate
        dataCollectionView.reloadData()
    }

}
