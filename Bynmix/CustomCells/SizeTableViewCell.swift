import UIKit

class SizeTableViewCell: UITableViewCell {
    
    @IBOutlet var tabNameLabel: UILabel!
    
    @IBOutlet var sizeTypeTableView: UITableView!
    
    @IBOutlet var sizeTypeTableViewHeight: NSLayoutConstraint!
}


