import UIKit

class SizeTypeTableViewCell: UITableViewCell {

    
    @IBOutlet var sizeCollectionView: UICollectionView!
    
    @IBOutlet var sizeCollectionViewHeight: NSLayoutConstraint!
}
