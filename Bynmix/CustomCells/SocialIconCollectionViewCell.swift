import UIKit

class SocialIconCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var socialImageView: UIImageView!
    
}
