import UIKit

class TagsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var heartImage: UIImageView!
    @IBOutlet var brandLabel: UILabel!
    
}
