import UIKit

class UserBynCollectionViewCell: UICollectionViewCell {
    @IBOutlet var topView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var itemImageView: UIImageView!
    @IBOutlet var middleView: UIView!
    @IBOutlet var itemStatusImageView: UIImageView!
    @IBOutlet var buttonsView: UIView!
    @IBOutlet var editButton: UIButton!
    @IBOutlet var enableDisableButton: UIButton!
    @IBOutlet var smallDeleteButton: UIButton!
    @IBOutlet var deleteButtonView: UIView!
    @IBOutlet var deleteButton: UIButton!
    
}
