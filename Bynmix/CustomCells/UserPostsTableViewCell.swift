import UIKit

class UserPostsTableViewCell: UITableViewCell {

    @IBOutlet var topView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var itemImageView: UIImageView!
    @IBOutlet var middleView: UIView!
    @IBOutlet var videoImageView: UIImageView!
    @IBOutlet var editButton: UIButton!
    @IBOutlet var enableDisableButton: UIButton!
    @IBOutlet var deleteButton: UIButton!
    
}
