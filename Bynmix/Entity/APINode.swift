import Foundation
import Alamofire

struct APINode {
    var requestMethod: Int
    var api: String
    var dict: Data?
    var onResponse: (AFDataResponse<Any>) -> Void
    var onError: (ApiError) -> Void
}
