import Foundation

struct About:Decodable {
    var title:String?
    var sections:[About]?
    var order:Int?
    var subSections:[About]?
    var description:String?
    
}
