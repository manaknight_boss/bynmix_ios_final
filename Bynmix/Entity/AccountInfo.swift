import Foundation

struct AccountInfo:Codable {
    var id: Int?
    var bankNickName: String?
    var lastFourNumbers: String?
    var routingNumber: String?
    var accountNumber: String?
    var nameOnBankAccount: String?
    var withdrawAmount: Double?
    var amountInCents: Double?
    var amount: Double?
    var arrivalDate: String?
    var arrivalDateFormatted: String?
    var bankAccountNickName: String?
    var confirmationCode: String?
    var lastFour: String?
    var status: String?
    var statusId: Int?
    var createdDate: String?
    init(bankNickName:String,routingNumber:String,accountNumber:String,nameOnBankAccount:String,withdrawAmount:Double) {
        self.bankNickName = bankNickName
        self.routingNumber = routingNumber
        self.accountNumber = accountNumber
        self.nameOnBankAccount = nameOnBankAccount
        self.withdrawAmount = withdrawAmount
    }
    
    init(withdrawAmount:Double) {
        self.withdrawAmount = withdrawAmount
    }
    
}
