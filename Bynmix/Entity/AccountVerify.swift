import Foundation

struct AccountVerify:Codable {
    
    var ssn: String?
    var phoneNumber: String?
    var frontDocumentToken: String?
    var backDocumentToken: String?
    var documentType: Int?
    var documentSide:Int?
    
}
