import Foundation
import UIKit

struct AddListingImage {
    var userImage:UIImage?
    var type:Int?
    var imageUrl: String?
    var imageId: Int?
    
    init(userImage: UIImage, type: Int) {
        self.userImage = userImage
        self.type = type
        self.imageId = 0
    }
    
    init(imageUrl: String, imageId: Int, type: Int) {
        self.imageUrl = imageUrl
        self.type = type
        self.imageId = imageId
    }
}
