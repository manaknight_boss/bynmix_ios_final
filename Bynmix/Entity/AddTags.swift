import Foundation

struct AddTags:Codable {
    var tag:String?
    var type:Int?
    var listingTags:ListingTags?
    
    init(type:Int) {
        self.type = type
    }
    
    init(tag:String,type:Int) {
        self.tag = tag
        self.type = type
    }
    
    init(listingTags:ListingTags,type:Int) {
        self.listingTags = listingTags
        self.type = type
    }
    
}
