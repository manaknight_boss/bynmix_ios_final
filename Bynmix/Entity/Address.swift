import Foundation

struct Address:Codable {
    var addressId: Int?
    var addressOne: String?
    var addressTwo: String?
    var city: String?
    var fullName: String?
    var isDeleted: Bool?
    var isReturnAddress: Bool?
    var isShippingAddress: Bool?
    var isVerified: Bool?
    var state: String?
    var stateAbbreviation: String?
    var zipCode: String?
    var userId:Int?
    var username: String?
    var userImage:String?
    var country:String?
    var shippingStatus:String?
    var deliveryDateFormatted:String?
    var shippingStatusDetails:String?
    var isDefaultAddress:Bool?
    var needsVerification:Bool?
    var stateId:Int?
    var addressLineOne:String?
    var addressLineTwo:String?
    var isDefaultReturnAddress:Bool?
    var returnStatus: String?
    var returnStatusDetails: String?
    var deliveryDate: String?
    
    init(fullName:String,addressOne:String,addressTwo:String,city:String,state:String,zipCode:String,country:String,isShippingAddress: Bool,isReturnAddress: Bool,needsVerification:Bool) {
        self.fullName = fullName
        self.addressOne = addressOne
        self.addressTwo = addressTwo
        self.city = city
        self.state = state
        self.zipCode = zipCode
        self.country = country
        self.isShippingAddress = isShippingAddress
        self.isReturnAddress = isReturnAddress
        self.needsVerification = needsVerification
    }
    
    init(fullName:String,addressOne:String,addressTwo:String,city:String,state:String,zipCode:String) {
        self.fullName = fullName
        self.addressOne = addressOne
        self.addressTwo = addressTwo
        self.city = city
        self.state = state
        self.zipCode = zipCode
    }
    
    init(isReturnAddress: Bool) {
        self.isReturnAddress = isReturnAddress
    }
    
    
}
