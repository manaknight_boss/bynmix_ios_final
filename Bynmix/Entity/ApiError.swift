
import Foundation

struct ApiError : Decodable {
    public var domain : String?
    public var message : String?
    public var statusCode: Int?
    
    init(statusCode: Int, message: String) {
        self.statusCode = statusCode
        self.message = message
    }
}
