import Foundation

struct BrandRequest : Encodable {
    var brandIds : [Int]?
    var userBrands : [Int]?
    
    init(brandIds : [Int]) {
        self.brandIds = brandIds
    }
    
    init(userBrands : [Int]) {
        self.userBrands = userBrands
    }
}
