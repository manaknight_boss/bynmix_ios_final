import Foundation

class CreatePostRequest:Encodable{
    var title:String?
    var shortDescription:String?
    var link:String?
    var postTypeId:Int?
    var tags:[String]?
    var postDots:[Dots]?
    
    init(title:String,postType:Int,link:String,shortDescription:String,tags:[String],postDots:[Dots]) {
        self.title = title
        self.postTypeId = postType
        self.link = link
        self.shortDescription = shortDescription
        self.tags = tags
        self.postDots = postDots
    }
    
    init(title:String,postType:Int,link:String,shortDescription:String,tags:[String]) {
        self.title = title
        self.postTypeId = postType
        self.link = link
        self.shortDescription = shortDescription
        self.tags = tags
    }
    
}
