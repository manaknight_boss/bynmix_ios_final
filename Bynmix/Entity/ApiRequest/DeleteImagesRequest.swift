import Foundation
struct DeleteImagesRequest: Encodable {
    var imageIds: [Int]
}
