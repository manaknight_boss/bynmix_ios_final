import Foundation

struct DeviceIdRequest:Encodable {
    var deviceId:String?
    var deviceType:String?
}
