import Foundation

struct ForgotPasswordRequest: Encodable {
    var email: String?
}
