import Foundation

struct NotificationSettingRequest : Codable {
    var settings: [NotificationSettings]
    
    init(settings: [NotificationSettings]) {
        self.settings = settings
    }
}
