import Foundation

struct ResetPasswordRequest: Encodable {
    var email: String?
    var password:String?
}
