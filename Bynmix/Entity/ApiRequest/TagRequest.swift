import Foundation

struct TagRequest : Encodable {
    var descriptorIds : [Int]?
    var userDescriptors : [Int]?
    
    init(descriptorIds : [Int]) {
        self.descriptorIds = descriptorIds
    }
    
    init(userDescriptors : [Int]) {
        self.userDescriptors  = userDescriptors
    }
}
