import Foundation

struct VerifyCodeRequest: Encodable {
    var email: String?
    var code: Int?
    
}
