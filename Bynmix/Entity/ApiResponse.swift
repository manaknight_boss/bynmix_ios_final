import Foundation

struct ApiResponse<T: Decodable> : Decodable {
    var apiVersion: Int
    var data: T?
    var errors: [ApiError]
    var isError: Bool
    var statusCode : Int
}
