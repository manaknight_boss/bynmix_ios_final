import Foundation

struct Availability:Codable {
    var availabilityId: Int?
    var availabilityName: String?
}
