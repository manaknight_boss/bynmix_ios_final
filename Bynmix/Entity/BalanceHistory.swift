import Foundation

struct BalanceHistory:Codable {
    
    var amountInCents: Double?
    var amount:Int?
    var arrivalDate: String?
    var arrivalDateFormatted: String?
    var bankAccountNickName: String?
    var confirmationCode: String?
    var lastFour: String?
    var status: String?
    var statusId: Int?
    var type: String?
    var typeId: Int?
    var createdDate: String?
    var formattedDisplayText:String?
    var items:[BalanceHistory]?
    var totalItems:Int?
    var totalPages: Int?
    var itemsPerPage: Int?
    var pageNumber: Int?
    var sortDirection: String?
    var sortField: String?
    var failureMessage:String?
        
}
