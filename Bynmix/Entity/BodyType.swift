import Foundation

struct BodyType: Decodable {
    
    var bodyTypeId: Int?
    var bodyTypeName: String?
    var isSelected:Bool?
    
    init(bodyTypeId:Int,bodyTypeName:String) {
        self.bodyTypeId = bodyTypeId
        self.bodyTypeName = bodyTypeName
    }
    
}
