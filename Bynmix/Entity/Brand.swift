import Foundation

struct Brand : Codable {
    var brandId : Int?
    var brandName : String?
    var brand : String?
    var isSelected : Bool?
    var items:[Brand]?
    var totalItems: Int?
    var totalPages: Int?
    var itemsPerPage: Int?
    var pageNumber: Int?
    
    init(brandId:Int,brandName:String) {
        self.brandId = brandId
        self.brandName = brandName
    }
    
    init(brandId:Int,brandName:String,isSelected:Bool) {
        self.brandId = brandId
        self.brandName = brandName
        self.isSelected = isSelected
    }
    
}
