import Foundation

struct BuyerAndSellerInfo:Codable {
    var buyerId:Int?
    var buyerUsername:String?
    var buyerImageUrl:String?
    var sellerId:Int?
    var sellerUsername:String?
    var sellerImageUrl:String?
    var loggedInUserFollowingSeller:Bool?
    var loggedInUserFollowingBuyer:Bool?
}
