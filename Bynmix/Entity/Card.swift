import Foundation

struct Card:Codable {
    var cardId: Int?
    var stripeCardId: String?
    var stripeCustomerId: String?
    var expirationMonth: Int?
    var expirationYear: Int?
    var isDefault: Bool?
    var nameOnCard: String?
    var lastFour: String?
    var brand: String?
    var brandImageUrlShort: String?
    var brandImageUrl: String?
    var addressId: Int?
    var fullName: String?
    var addressLineOne: String?
    var addressLineTwo:String?
    var city: String?
    var state: String?
    var zipcode: String?
    var country: String?
    var isDefaultShippingAddress: Bool?
    var isDefaultReturnAddress: Bool?
    var userId: Int?
    var username: String?
    var userImage: String?
    var sourceToken: String?
    var settingAddressId: Int?
    var isNewAddress: Bool?
    var name:String?
    var addressCountry:String?
    var addressLine1:String?
    var addressLine2:String?
    var addressCity:String?
    var addressState:String?
    var addressZipcode:String?
    var needsVerification:Bool?
    init(sourceToken:String,isNewAddress:Bool,name:String,addressCountry:String,addressLine1:String,addressLine2:String,addressCity:String,addressState:String,addressZipcode:String,isDefault:Bool, needsVerification:Bool) {
        self.sourceToken = sourceToken
        self.isNewAddress = isNewAddress
        self.name = name
        self.addressCountry = addressCountry
        self.addressLine1 = addressLine1
        self.addressLine2 = addressLine2
        self.addressCity = addressCity
        self.addressState = addressState
        self.addressZipcode = addressZipcode
        self.isDefault = isDefault
        self.needsVerification = needsVerification
    }
    
    init(sourceToken:String,expirationYear:Int,expirationMonth:Int,nameOnCard:String,isNewAddress:Bool,name:String,addressCountry:String,addressLine1:String,addressLine2:String,addressCity:String,addressState:String,addressZipcode:String,isDefault:Bool) {
        self.sourceToken = sourceToken
        self.expirationYear = expirationYear
        self.expirationMonth = expirationMonth
        self.nameOnCard = nameOnCard
        self.isNewAddress = isNewAddress
        self.name = name
        self.addressCountry = addressCountry
        self.addressLine1 = addressLine1
        self.addressLine2 = addressLine2
        self.addressCity = addressCity
        self.addressState = addressState
        self.addressZipcode = addressZipcode
        self.isDefault = isDefault
    }
    
    init(sourceToken:String,settingAddressId:Int,isNewAddress:Bool,isDefault:Bool) {
        self.sourceToken = sourceToken
        self.settingAddressId = settingAddressId
        self.isNewAddress = isNewAddress
        self.isDefault = isDefault
    }
    
    init(sourceToken:String,expirationYear:Int,expirationMonth:Int,nameOnCard:String,settingAddressId:Int,isNewAddress:Bool,isDefault:Bool) {
        self.sourceToken = sourceToken
        self.expirationYear = expirationYear
        self.expirationMonth = expirationMonth
        self.nameOnCard = nameOnCard
        self.settingAddressId = settingAddressId
        self.isNewAddress = isNewAddress
        self.isDefault = isDefault
    }
    
    init(isDefault:Bool) {
        self.isDefault = isDefault
    }
    
    init(sourceToken:String,settingAddressId:Int,isNewAddress:Bool,name:String,addressCountry:String,addressLine1:String,addressLine2:String,addressCity:String,addressState:String,addressZipcode:String,isDefault:Bool, needsVerification:Bool) {
           self.sourceToken = sourceToken
           self.settingAddressId = settingAddressId
           self.isNewAddress = isNewAddress
           self.name = name
           self.addressCountry = addressCountry
           self.addressLine1 = addressLine1
           self.addressLine2 = addressLine2
           self.addressCity = addressCity
           self.addressState = addressState
           self.addressZipcode = addressZipcode
           self.isDefault = isDefault
           self.needsVerification = needsVerification
       }
    
}
