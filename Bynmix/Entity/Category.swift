import Foundation

struct Category:Codable {
    var categoryId: Int?
    var categoryName: String?
    var isSelected: Bool?
}
