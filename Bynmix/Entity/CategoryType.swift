import Foundation

struct CategoryType:Codable {
    var categoryTypeId: Int?
    var categoryTypeName: String?
    var displayImage: String?
    var categories:[Category]?
    var isSelected: Bool?
    var imageType:Int?
}
