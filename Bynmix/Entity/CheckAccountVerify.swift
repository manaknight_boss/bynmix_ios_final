import Foundation

struct CheckAccountVerify:Codable {
    
    var isVerified: Bool?
    var isPending: Bool?
    var disabledReasons:[DisabledReasons]?
    
}
