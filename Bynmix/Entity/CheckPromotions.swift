import Foundation

struct CheckPromotions:Codable {
    var listingId: Int?
    var offerDetails: OfferDetails?
    var shippingDetails: ShippingDetails?
    var pointsDetails: PointsDetail?
    var salesTax: Double?
    var discount: Double?
    var totalFinalAmount: Double?
    var promotionInfoDescription: String?
    var shippingType:String?
    var shippingTypeId : Int?
    var shipRates:[ShipRates]?
}
