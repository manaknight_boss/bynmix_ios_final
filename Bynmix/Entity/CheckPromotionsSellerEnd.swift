import Foundation

struct CheckPromotionsSellerEnd:Codable{
    
    var listingId: Int?
    var offerAmount: Int?
    var shippingPrice: Double?
    var commission: Double?
    var totalFinalProfit: Double?
    var pointsAwarded: Int?
    var currentPromotions:[Promotions]?
    var shippingType:String?
    var shippingTypeId : Int?
    var shipRates:[ShipRates]?
    
}
