import Foundation

struct Colors:Codable{
    var colorId:Int?
    var colorName:String?
    var hexCode:String?
    var isSelected: Bool?
}
