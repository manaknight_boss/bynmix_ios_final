import Foundation

struct Comments:Codable {
    var listingCommentId:Int?
    var listingId: Int?
    var createdDateFormatted: String?
    var comment: String?
    var userId: Int?
    var userImage:String?
    var username: String?
    var direction: String?
    var isBuyerComment: Bool?
    var additionalComment:String?
    var isChargeDisputeEvidenceComment:Bool?
    var replyToUserId:Int?
    var userMentions:[MentionItem]?
    var userMentionsJSON:String?
    var items:[Comments]?
    var totalPages:Int?
    var pageNumber:Int?
    var images:[String]?
    
}
