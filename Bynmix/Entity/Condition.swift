import Foundation

struct Condition:Codable {
    var conditionId: Int?
    var conditionName: String?
    var isSelected:Bool?
}
