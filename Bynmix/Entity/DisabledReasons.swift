import Foundation

struct DisabledReasons:Codable {
    
    var code: String?
    var reason: String?
    var requiredField: String?
    
}
