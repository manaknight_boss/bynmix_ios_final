import Foundation

struct Dispute:Codable{
    var issueTypeId:Int?
    var purchaseId:Int?
    var comments:String?
    var images:[String]?
    var reportedIssueTypeId:Int?
    var entityId:Int?
    var disputeTypeId:Int?
}
