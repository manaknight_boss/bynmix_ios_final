import Foundation

struct DisputeDetails:Codable{
    var id :Int?
    var comments:String?
    var expirationDate:String?
    var expirationDateFormatted:String?
    var createdDateFormatted:String?
    var isResolved:Bool?
    var disputeTypeId:Int?
    var disputeType:String?
    var disputeStatusId:Int?
    var disputeStatus:String?
    var resolvedByUserId:Int?
    var hasEvidence:Bool?
    var isInProgress:Bool?
    var canCancelDispute:Bool?
    var canLeaveComment:Bool?
    var images:[String]?
    var purchaseId:Int?
    var saleId:Int?
    var listingTitle:String?
    var buyerId:Int?
    var buyerUsername:String?
    var buyerImageUrl:String?
    var sellerId:Int?
    var sellerUsername:String?
    var additionalComments:[Comments]?
    var canEscalateDispute:Bool?
    var escalateRequested:Bool?
    var resolutionDescription:String?
    var canRefundBuyer:Bool?
    var disputeEscalated:Bool?
    
}
