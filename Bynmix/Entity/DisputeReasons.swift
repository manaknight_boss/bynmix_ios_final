import Foundation

struct DisputeReasons:Codable {
    var id: Int?
    var disputeTypeId: Int?
    var disputeTypeName: String?
}
