import Foundation

struct DocumentType:Codable {
    
    var id: Int?
    var documentType: String?
    
}
