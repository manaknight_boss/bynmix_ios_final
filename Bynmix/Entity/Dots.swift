import Foundation

struct Dots:Codable{
    var postId: Int?
    var postDotId: Int?
    var postDotTypeId:Int?
    var postDotType: String?
    var hexColor: String?
    var xCoordinate: Double?
    var yCoordinate: Double?
    var title: String?
    var link: String?
    var listingId: Int?
    var listingTitle: String?
    var id :Int?
    var isDeleted:Bool?
    var items:[Dots]?
    var type: Int?
    static var dots = [Dots]()
    static var removedDots = [Dots]()
    static let DOT1: Int = 1
    static let DOT2: Int = 2
    static let DOT3: Int = 3
    static let DOT4: Int = 4
    static let DOT5: Int = 5
    
    init(id:Int,color:String,isDeleted:Bool,x:Double,y:Double) {
        self.id = id
        self.hexColor = color
        self.isDeleted = isDeleted
        self.xCoordinate = x
        self.yCoordinate = y
    }
    
    init(type:Int, x:Double,y:Double) {
        self.type = type
        self.xCoordinate = x
        self.yCoordinate = y
    }
    
    init() {
        self.id = 0
    }
    
}
