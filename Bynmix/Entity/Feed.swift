import Foundation

struct Feed:Codable {
    var type:String?
    var createdDateFormatted:String?
    var title:String?
    var photoUrl:String?
    var userPhotoUrl:String?
    var userLiked:Bool?
    var price:Double?
    var size:String?
    var username:String?
    var likesCount:Int?
    var id:Int?
    var description:String?
    var link:String?
    var hasPostDots:Bool?
    var postType:String?
    var items:[Feed]?
    var totalPages:Int?
    var brand:String?
    var status:String?
    var userId:Int?
    var isActive:Bool?
    var expandableLabelCollapsed:Bool?
    var postTags:[String]?
    var postDotsCount:Int?
    var postImageId:Int?
    var canUserLikeItem:Bool?
    var canUserReportAnIssue:Bool?
    var postDots:[Dots]?
    var containDots:Bool?
    var originalPrice:Double?
    var startPosition: Int?
    var endPosition: Int?
    var postTypeId:Int?
    
    init(userId:Int,startPosition:Int,endPosition:Int) {
        self.userId = userId
        self.startPosition = startPosition
        self.endPosition = endPosition
    }
    
}
