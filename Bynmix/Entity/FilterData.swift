import Foundation

struct FilterData:Codable {
    var searchText:String?
    var conditionIds:[Int]?
    var availabilityIds:[Int]?
    var sizeIds:[Int]?
    var categoryIds:[Int]?
    var categoryTypeIds:[Int]?
    var colorIds:[Int]?
    var customMinPrice:Int?
    var customMaxPrice:Int?
    var brandIds:[Int]?
}
