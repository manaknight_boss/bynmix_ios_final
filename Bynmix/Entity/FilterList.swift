import Foundation

struct FilterList:Codable {
    var filterName:String?
    var isSelected:Bool?
    var isItemSelected:Bool?
    
    init(filterName:String,isSelected:Bool) {
        self.filterName = filterName
        self.isSelected = isSelected
    }
    
    
}
