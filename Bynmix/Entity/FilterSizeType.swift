import Foundation

struct FilterSizeType:Codable {
    var sizeTypeId:Int?
    var sizeTypeName:String?
    var sizes:[FilterSizes]?
    var isSelected:Bool?
    
}
