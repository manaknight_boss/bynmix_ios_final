import Foundation
import UIKit

struct GetListingImages:Codable {
    var type:Int?
    var listingImageId: Int?
    var displayOrder: Int?
    var imageUrl: String?
    //var userImage:UIImage?
    
}
