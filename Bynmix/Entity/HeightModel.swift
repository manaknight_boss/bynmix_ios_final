import Foundation

class HeightModel{
    var customMinHeightFeet:Int = 3
    var customMinHeightInches:Int = 0
    var customMaxHeightFeet:Int = 8
    var customMaxHeightInches:Int = 0
    var isHeightShowAll:Bool?
    var maxPinValue:Float = 100
    var minPinValue:Float = 0

}
