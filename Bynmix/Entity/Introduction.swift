import Foundation

struct Introduction: Decodable{
    
    var title:String?
    var position:Int?
    var imageUrl:String?
    var points:[IntroPoints]?
    
}

