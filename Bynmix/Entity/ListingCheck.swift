import Foundation

struct ListingCheck: Codable {
    var isError: Bool?
    var errorMessage: String?
    var addressMissing: Bool?
    var paymentMissing: Bool?
    var isNewSeller: Bool?
}
