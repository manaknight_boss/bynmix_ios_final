import Foundation

struct ListingDetail:Codable{
    var id:Int?
    var createdDateFormatted:String?
    var description:String?
    var isActive:Bool?
    var isBuyItNowOnly: Bool?
    var isDeleted: Bool?
    var isPurchased: Bool?
    var isLoggedInUserListing: Bool?
    var likesCount: Int?
    var lowestAllowableOffer: Int?
    var price: Int?
    var title: String?
    var availability: Availability?
    var brand:Brand?
    var category: Category?
    var categoryType: CategoryType?
    var condition: Condition?
    var colors: [Colors]?
    var comments: [Comments]?
    var listingTags: [ListingTags]?
    var size: Sizes?
    var sizeType:SizeType?
    var shippingInfo: ShippingInfo?
    var shippingWeight: Double?
    var status: Status?
    var userId: Int?
    var username: String?
    var userPhotoUrl: String?
    var userCredits:Int?
    var userLiked: Bool?
    var userLoggedIn: Bool?
    var doesLoggedInUserHaveActiveBid: Bool?
    var canUserReportAnIssue: Bool?
    var sliderImages:[SliderImages]?
    var originalPrice:Int?
    var isMultipleQuantities:Bool?
    var sizeTypeId:Int?
    var sizeId:Int?
    var brandId:Int?
    var newBrand:String?
    var isPersonalBrand:Bool?
    var categoryTypeId:Int?
    var categoryId:Int?
    var conditionId:Int?
    var colorIds:[Int]?
    var availabilityId:Int?
    var tags:[String]?
    var shippingRateId:Int?
    var canUserLikeListing:Bool?
    var statusId: Int?
    var offerId:Int?
    var buyerName:String?
    var videoUrl:String?
    
    var shippingWeightPounds:Int?
    var shippingWeightOunces:Int?
    var shippingLength:Double?
    var shippingHeight:Double?
    var shippingWidth:Double?
    var shippingDepth:Double?
    var shippingType:Int?
    var shippingDescription:String?
    
    init(title:String,description:String,tags:[String],conditionId:Int,originalPrice:Int,listingPrice:Int,lowestAllowableOffer:Int,categoryId:Int,categoryTypeId:Int,sizeId:Int,colorIds:[Int],brandId:Int,shippingWeight:Double,shippingRateId:Int, sizeTypeId:Int, availabilityId:Int, shippingInfo: ShippingInfo) {
        self.title = title
        self.description = description
        self.tags = tags
        self.conditionId = conditionId
        self.originalPrice = originalPrice
        self.price = listingPrice
        self.lowestAllowableOffer = lowestAllowableOffer
        self.categoryId = categoryId
        self.categoryTypeId = categoryTypeId
        self.sizeId = sizeId
        self.colorIds = colorIds
        self.brandId = brandId
        self.shippingWeight  = shippingWeight
        self.shippingRateId = shippingRateId
        self.sizeTypeId = sizeTypeId
        self.availabilityId = availabilityId
        self.shippingInfo = shippingInfo
    }
    
    init(title:String,description:String,tags:[String],conditionId:Int,originalPrice:Int,listingPrice:Int,categoryId:Int,categoryTypeId:Int,sizeId:Int,colorIds:[Int],brandId:Int,shippingWeight:Double,shippingRateId:Int, sizeTypeId:Int, availabilityId:Int, shippingInfo: ShippingInfo) {
        self.title = title
        self.description = description
        self.tags = tags
        self.conditionId = conditionId
        self.originalPrice = originalPrice
        self.price = listingPrice
        self.categoryId = categoryId
        self.categoryTypeId = categoryTypeId
        self.sizeId = sizeId
        self.colorIds = colorIds
        self.brandId = brandId
        self.shippingWeight  = shippingWeight
        self.shippingRateId = shippingRateId
        self.sizeTypeId = sizeTypeId
        self.availabilityId = availabilityId
        self.shippingInfo = shippingInfo
    }
    
    init(title:String,description:String,tags:[String],originalPrice:Int,listingPrice:Int,lowestAllowableOffer:Int, statusId:Int, isBuyNowOnly: Bool,shippingType: Int) {
        self.title = title
        self.description = description
        self.tags = tags
        self.originalPrice = originalPrice
        self.price = listingPrice
        self.lowestAllowableOffer = lowestAllowableOffer
        self.statusId = statusId
        self.isBuyItNowOnly = isBuyNowOnly
        self.shippingType = shippingType
    }
    
    init(title:String,description:String,tags:[String],originalPrice:Int,listingPrice:Int,lowestAllowableOffer:Int, statusId:Int, isBuyNowOnly: Bool) {
        self.title = title
        self.description = description
        self.tags = tags
        self.originalPrice = originalPrice
        self.price = listingPrice
        self.lowestAllowableOffer = lowestAllowableOffer
        self.statusId = statusId
        self.isBuyItNowOnly = isBuyNowOnly
    }
    
    init(title:String,description:String,tags:[String],originalPrice:Int,listingPrice:Int, statusId:Int, isBuyNowOnly: Bool,shippingType: Int) {
        self.title = title
        self.description = description
        self.tags = tags
        self.originalPrice = originalPrice
        self.price = listingPrice
        self.statusId = statusId
        self.isBuyItNowOnly = isBuyNowOnly
        self.shippingType = shippingType
    }
    
    init(title:String,description:String,tags:[String],originalPrice:Int,listingPrice:Int, statusId:Int, isBuyNowOnly: Bool) {
        self.title = title
        self.description = description
        self.tags = tags
        self.originalPrice = originalPrice
        self.price = listingPrice
        self.statusId = statusId
        self.isBuyItNowOnly = isBuyNowOnly
    }
    
    init(conditionId:Int,categoryId:Int,categoryTypeId:Int,sizeId:Int,colorIds:[Int],brandId:Int, sizeTypeId:Int) {
        self.conditionId = conditionId
        self.categoryId = categoryId
        self.categoryTypeId = categoryTypeId
        self.sizeId = sizeId
        self.colorIds = colorIds
        self.brandId = brandId
        self.sizeTypeId = sizeTypeId
    }
    
    init(conditionId:Int,categoryId:Int,categoryTypeId:Int,sizeId:Int,colorIds:[Int],brandId:Int, sizeTypeId:Int,lowestAllowableOffer:Int,description:String) {
        self.conditionId = conditionId
        self.categoryId = categoryId
        self.categoryTypeId = categoryTypeId
        self.sizeId = sizeId
        self.colorIds = colorIds
        self.brandId = brandId
        self.sizeTypeId = sizeTypeId
        self.lowestAllowableOffer = lowestAllowableOffer
        self.description = description
    }
    
    init(shippingWeight:Double,shippingRateId:Int, shippingInfo: ShippingInfo) {
        self.shippingWeight  = shippingWeight
        self.shippingRateId = shippingRateId
        self.shippingInfo = shippingInfo
    }
    
    init(shippingWeight:Double,shippingRateId:Int, shippingInfo: ShippingInfo,description:String) {
        self.shippingWeight  = shippingWeight
        self.shippingRateId = shippingRateId
        self.shippingInfo = shippingInfo
        self.description = description
    }
    
    init(title:String,description:String,tags:[String],conditionId:Int,originalPrice:Int,listingPrice:Int,categoryId:Int,categoryTypeId:Int,sizeId:Int,colorIds:[Int],brandId:Int,shippingRateId:Int, sizeTypeId:Int, availabilityId:Int,shippingWeightPounds:Int,shippingWeightOunces:Int,shippingLength:Double,shippingHeight:Double,shippingDepth:Double,shippingType:Int) {
        self.title = title
        self.description = description
        self.tags = tags
        self.conditionId = conditionId
        self.originalPrice = originalPrice
        self.price = listingPrice
        self.categoryId = categoryId
        self.categoryTypeId = categoryTypeId
        self.sizeId = sizeId
        self.colorIds = colorIds
        self.brandId = brandId
        self.shippingRateId = shippingRateId
        self.sizeTypeId = sizeTypeId
        self.availabilityId = availabilityId
        self.shippingWeightPounds = shippingWeightPounds
        self.shippingWeightOunces = shippingWeightOunces
        self.shippingLength = shippingLength
        self.shippingHeight = shippingHeight
        self.shippingDepth = shippingDepth
        self.shippingType = shippingType
    }
    
    init(title:String,description:String,tags:[String],conditionId:Int,originalPrice:Int,listingPrice:Int,categoryId:Int,categoryTypeId:Int,sizeId:Int,colorIds:[Int],brandId:Int, sizeTypeId:Int, availabilityId:Int,shippingWeightPounds:Int,shippingWeightOunces:Int,shippingLength:Double,shippingHeight:Double,shippingDepth:Double,shippingType:Int) {
        self.title = title
        self.description = description
        self.tags = tags
        self.conditionId = conditionId
        self.originalPrice = originalPrice
        self.price = listingPrice
        self.categoryId = categoryId
        self.categoryTypeId = categoryTypeId
        self.sizeId = sizeId
        self.colorIds = colorIds
        self.brandId = brandId
        self.sizeTypeId = sizeTypeId
        self.availabilityId = availabilityId
        self.shippingWeightPounds = shippingWeightPounds
        self.shippingWeightOunces = shippingWeightOunces
        self.shippingLength = shippingLength
        self.shippingHeight = shippingHeight
        self.shippingDepth = shippingDepth
        self.shippingType = shippingType
    }
    
    init(title:String,description:String,tags:[String],conditionId:Int,originalPrice:Int,listingPrice:Int,lowestAllowableOffer:Int,categoryId:Int,categoryTypeId:Int,sizeId:Int,colorIds:[Int],brandId:Int,shippingRateId:Int, sizeTypeId:Int, availabilityId:Int, shippingWeightPounds:Int,shippingWeightOunces:Int,shippingLength:Double,shippingHeight:Double,shippingDepth:Double,shippingType:Int) {
        self.title = title
        self.description = description
        self.tags = tags
        self.conditionId = conditionId
        self.originalPrice = originalPrice
        self.price = listingPrice
        self.lowestAllowableOffer = lowestAllowableOffer
        self.categoryId = categoryId
        self.categoryTypeId = categoryTypeId
        self.sizeId = sizeId
        self.colorIds = colorIds
        self.brandId = brandId
        self.shippingRateId = shippingRateId
        self.sizeTypeId = sizeTypeId
        self.availabilityId = availabilityId
        self.shippingWeightPounds = shippingWeightPounds
        self.shippingWeightOunces = shippingWeightOunces
        self.shippingLength = shippingLength
        self.shippingHeight = shippingHeight
        self.shippingDepth = shippingDepth
        self.shippingType = shippingType
    }
    
    init(title:String,description:String,tags:[String],conditionId:Int,originalPrice:Int,listingPrice:Int,lowestAllowableOffer:Int,categoryId:Int,categoryTypeId:Int,sizeId:Int,colorIds:[Int],brandId:Int, sizeTypeId:Int, availabilityId:Int, shippingWeightPounds:Int,shippingWeightOunces:Int,shippingLength:Double,shippingHeight:Double,shippingDepth:Double,shippingType:Int) {
        self.title = title
        self.description = description
        self.tags = tags
        self.conditionId = conditionId
        self.originalPrice = originalPrice
        self.price = listingPrice
        self.lowestAllowableOffer = lowestAllowableOffer
        self.categoryId = categoryId
        self.categoryTypeId = categoryTypeId
        self.sizeId = sizeId
        self.colorIds = colorIds
        self.brandId = brandId
        self.sizeTypeId = sizeTypeId
        self.availabilityId = availabilityId
        self.shippingWeightPounds = shippingWeightPounds
        self.shippingWeightOunces = shippingWeightOunces
        self.shippingLength = shippingLength
        self.shippingHeight = shippingHeight
        self.shippingDepth = shippingDepth
        self.shippingType = shippingType
    }
    
    init(shippingRateId:Int,description:String,shippingWeightPounds:Int,shippingWeightOunces:Int,shippingLength:Double,shippingHeight:Double,shippingDepth:Double,shippingType:Int) {
        self.shippingRateId = shippingRateId
        self.description = description
        self.shippingWeightPounds = shippingWeightPounds
        self.shippingWeightOunces = shippingWeightOunces
        self.shippingLength = shippingLength
        self.shippingHeight = shippingHeight
        self.shippingDepth = shippingDepth
        self.shippingType = shippingType
    }
    
    init(description:String,shippingWeightPounds:Int,shippingWeightOunces:Int,shippingLength:Double,shippingHeight:Double,shippingDepth:Double,shippingType:Int) {
        self.description = description
        self.shippingWeightPounds = shippingWeightPounds
        self.shippingWeightOunces = shippingWeightOunces
        self.shippingLength = shippingLength
        self.shippingHeight = shippingHeight
        self.shippingDepth = shippingDepth
        self.shippingType = shippingType
    }
    
    init(shippingWeightPounds:Int,shippingWeightOunces:Int,shippingLength:Double,shippingHeight:Double,shippingDepth:Double,shippingType:Int) {
        self.shippingWeightPounds = shippingWeightPounds
        self.shippingWeightOunces = shippingWeightOunces
        self.shippingLength = shippingLength
        self.shippingHeight = shippingHeight
        self.shippingDepth = shippingDepth
        self.shippingType = shippingType
    }
    
    init(shippingRateId:Int,shippingWeightPounds:Int,shippingWeightOunces:Int,shippingLength:Double,shippingHeight:Double,shippingDepth:Double,shippingType:Int) {
        self.shippingRateId = shippingRateId
        self.shippingWeightPounds = shippingWeightPounds
        self.shippingWeightOunces = shippingWeightOunces
        self.shippingLength = shippingLength
        self.shippingHeight = shippingHeight
        self.shippingDepth = shippingDepth
        self.shippingType = shippingType
    }
    
}
