import Foundation

struct ListingStatuses:Codable {
    
    var listingStatusId: Int?
    var status: Int?
    var statusName: String?
    var isVisibleToUser: Bool?
    
}
