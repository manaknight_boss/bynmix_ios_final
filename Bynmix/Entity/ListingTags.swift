import Foundation

struct ListingTags:Codable {
    var tagId: Int?
    var tagName: String?
}
