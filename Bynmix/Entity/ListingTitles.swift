import Foundation

struct ListingTitles:Codable {
    
    var id:Int?
    var title:String?
    var imageUrl:String?
    var items:[ListingTitles]?
    var pageNumber:Int?
    var totalPages:Int?
    
}
