import Foundation

public struct MentionItem:Codable {
    
    var username: String?
    var userId: Int?
    var firstName:String?
    var lastName:String?
    var fullName:String?
    var imageUrl:String?
    var startPosition: Int?
    var endPosition: Int?
    var items:[MentionItem]?
    
    public init(username: String,userId: Int){
        self.username = username
        self.userId = userId
    }
    
}
