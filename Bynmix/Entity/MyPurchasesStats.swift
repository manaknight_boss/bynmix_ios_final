import Foundation

struct MyPurchasesStats:Codable {
   
    var purchaseTotalId:Int?
    var totalItems:Int?
    var totalPendingPurchasePoints:Int?
    var totalPurchasePoints:Int?
    var totalPurchases:Double?
    var purchasesNeedingFeedback:Int?
    var totalSpentThisMonth:Double?
    var buyerId:Int?
    var buyerUsername:String?
    var buyerImageUrl:String?
    var totalPurchaseItems:Int?
    var totalBuyerFeedback:Int?
    var totalBuyerFeedbackScore:Double?
    var creditTotal:Int?
}
