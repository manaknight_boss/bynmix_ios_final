import Foundation

struct MySalesStats:Codable {
    
    var saleTotalId:Int?
    var totalItems:Int?
    var totalPendingItems:Int?
    var totalPendingSalePoints:Int?
    var totalPendingSales:Double?
    var totalSalePoints:Int?
    var totalSales:Double?
    var sellerId:Int?
    var sellerUsername:String?
    var sellerImageUrl:String?
    var totalSellerFeedback:Int?
    var totalSellerFeedbackScore:Double?
    var totalPendingSaleItems:Int?
    var totalSaleItems:Int?
    var totalSellerScore:Double?
    var totalAvailableForWithdrawal:Int?
}
