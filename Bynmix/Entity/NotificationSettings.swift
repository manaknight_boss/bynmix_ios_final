import Foundation

struct NotificationSettings:Codable{
    var settingNotificationId:Int?
    var settingType:SettingType?
    var settingTypeId:Int?
    var isEnabled:Bool?
    var order:Int?
    var userId:Int?
    
    init(settingNotificationId:Int,isEnabled:Bool) {
        self.settingNotificationId = settingNotificationId
        self.isEnabled = isEnabled
    }
}

