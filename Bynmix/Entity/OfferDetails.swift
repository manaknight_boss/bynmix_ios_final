import Foundation

struct OfferDetails:Codable {
    var id: Int?
    var offerAmount: Double?
    var hasCurrentPromotion: Bool?
    var promotionDescription: String?
}
