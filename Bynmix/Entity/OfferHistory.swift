import Foundation

struct OfferHistory:Codable {
    
    var latestOfferId: Int?
    var isLatestOfferExpired: Bool?
    var isLatestOfferBySeller: Bool?
    var latestOfferType: Int?
    var latestOfferTypeName: String?
    var latestOfferDateFormatted: String?
    var isBuyerViewing: Bool?
    var isOfferAllowed: Bool?
    var isPurchased: Bool?
    var isBuyerAndCanOffer: Bool?
    var isSellerAndCanCounterOffer: Bool?
    var isOfferCancelable: Bool?
    var listingId: Int?
    var listingTitle: String?
    var listingImage: String?
    var isListingBuyItNowOnly: Bool?
    var listingStatusId: Int?
    var buyerId: Int?
    var buyerUsername: String?
    var buyerUserImage: String?
    var sellerId: Int?
    var sellerUsername: String?
    var sellerUserImage: String?
    var offerHistories:[SubOfferHistory]?
    
}
