import Foundation

struct OffersCheck:Codable {
    
    var offerId: Int?
    var offer: Int?
    var shippingAddressId: Int?
    var cardId: Int?
    var counterOffer:Int?
    var ReturnAddressId:Int?
    
}
