
import Foundation

struct PageResponse {
    var totalItems: Int?
    var totalPages: Int?
    var itemsPerPage: Int?
    var pageNumber: Int?
    var sortDirection: String?
    var sortField: String?
}
