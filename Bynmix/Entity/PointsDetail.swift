import Foundation

struct PointsDetail:Codable {
    var id: Int?
    var pointsAwarded: Double?
    var hasCurrentPromotion: Bool?
    var promotionDescription: String?
}
