import Foundation
struct PostImage: Decodable {
    var postImageId: Int?
    var displayOrder: Int?
    var imageUrl: String?
}
