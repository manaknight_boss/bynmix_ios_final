import Foundation

struct PostType:Codable {
    
    var id: Int?
    var name: String?
    var postTypeId: Int?
    
}
