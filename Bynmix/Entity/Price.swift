import Foundation

struct Price:Codable {
    var id:Int?
    var price:String?
    var isPriceSelected:Bool?
}
