import Foundation

struct Promotions:Codable {
    
    var promotionId: Int?
    var promotionStartDate: String?
    var promotionStartDateFormatted: String?
    var promotionEndDate: String?
    var promotionEndDateFormatted: String?
    var promotionTypeId: Int?
    var promotionTypeEnumId: Int?
    var promotionType: String?
    var iconUrlShort: String?
    var iconUrl: String?
    var imageType: Int?
    var bannerColor : String?
    var bannerText: String?
    var displayImage:String?
    var displayImageShort:String?
    
    
}
