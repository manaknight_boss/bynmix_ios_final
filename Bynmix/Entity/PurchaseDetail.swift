import Foundation

struct PurchaseDetail:Codable  {
    
    var purchaseId:Int?
    var purchaseDate:String?
    var purchaseDateFormatted:String?
    var purchaseCharged:Double?
    var purchaseDiscountAmount:Double?
    var purchasePercentDiscount:Int?
    var purchasePrice:Double?
    var userShippingAddressId:Int?
    var sellerId:Int?
    var sellerUsername:String?
    var sellerImageUrl:String?
    var purchaseStatusId:Int?
    var purchaseStatus:String?
    var purchaseShippingPrice:Double?
    var purchaseTotalPrice:Double?
    var purchaseOriginalBeforeDiscountPrice:Double?
    var purchaseOriginalAfterDiscountPrice:Double?
    var canCancelPurchase:Bool?
    var purchaseSubStatus:String?
    var paidWith:Card?
    var discount:Double?
    var purchaseMonth:String?
    var purchaseYear:String?
    var items:[PurchaseDetail]?
    var totalPages: Int?
    
    var rating:Int?
    var feedback:String?
    var ribbonColor:String?
    var isComplete:Bool?
    var points:Int?
    var pointsStatusId:Int?
    var pointsStatus:String?
    var shippingPrice:Double?
    var listingId:Int?
    var listingTitle:String?
    var listingImageUrl:String?
    var userId:Int?
    var username:String?
    var userImageUrl:String?
    var buyerAndSellerInfo:BuyerAndSellerInfo?
    var shippedTo:Address?
    var promotions:[Int]?
    var promotionsFormatted:[String]?
    var canPrintShippingLabel:Bool?
    var canLeaveFeedback:Bool?
    var canOpenDispute:Bool?
    var hasOpenDispute:Bool?
    var hasSubStatus:Bool?
    var hasViewableDispute:Bool?
    var disputeId:Int?
    var indexOfItem:Int?
    var salesTax:Double?
    var purchasedWithPromotions:[Promotions]?
    var canPrintReturnLabel:Bool?
    var returnRequested:Int?
    var returnOrderShipped:Int?
    var returnOrderInTransit:Int?
    var returnOrderDelivered:Int?
    var returnLabelPrinted:Int?
    var returnedTo:Address?
    var iconImageUrlShort:String?
    var iconImageUrl:String?
    var updatedDateFormatted:String?
    
    init(purchaseId:Int,rating:Int,feedback:String) {
        self.purchaseId = purchaseId
        self.rating = rating
        self.feedback = feedback
    }
}
