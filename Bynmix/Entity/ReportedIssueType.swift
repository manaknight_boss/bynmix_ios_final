import Foundation

struct ReportedIssueType:Codable {
    var reportedIssueTypeId: Int?
    var issueType: Int?
    var name: String?
}
