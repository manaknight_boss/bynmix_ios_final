import Foundation

struct Reviews:Codable {
    var createdDateFormatted:String?
    var items:[Reviews]?
    var purchaseFeedbackId:Int?
    var createdDate:String?
    var feedback:String?
    var rating:Double?
    var listingId:Int?
    var listingTitle:String?
    var listingPhotoUrl:String?
    var buyerId:Int?
    var buyerUsername:String?
    var buyerPhotoUrl:String?
    var sellerId:Int?
    var sellerUsername:String?
    var sellerPhotoUrl:String?
    
    var totalItems: Int?
    var totalPages: Int?
    var itemsPerPage: Int?
    var pageNumber: Int?
    var sortDirection: String?
    var sortField: String?
}
