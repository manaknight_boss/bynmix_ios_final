import Foundation

struct SaleCancelReason:Codable{
    var saleCancelReasonId:Int?
    var saleCancelReasonType:Int?
    var cancelReason:String?
    var comments:String?
    var isVisibleToUser: Bool?
}
