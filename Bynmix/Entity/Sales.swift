import Foundation

struct Sales:Codable {
    
    var saleTotalId: Int?
    var totalPendingSaleItems: Int?
    var totalPendingSalePoints: Int?
    var totalPendingSales:Double?
    var totalSaleItems: Int?
    var totalSalePoints: Int?
    var totalSales: Int?
    var totalAvailableForWithdrawal: Int?
    var totalSellerFeedback: Int?
    var totalSellerScore: Double?
    var totalSellerFeedbackScore:Double?
    var sellerId: Int?
    var sellerUsername: String?
    var sellerImageUrl: String?
    var hasPayoutInProgress:Bool?
    var payoutProgressMessage:String?
}
