import Foundation

struct SalesDetail:Codable{
    
    var saleId:Int?
    var saleDate:String?
    var saleDateFormatted:String?
    var saleOriginalPrice:Double?
    var saleEarned:Int?
    var salePrice:Double?
    var saleProfit:Double?
    var saleStatusId:Int?
    var saleStatus:String?
    var userReturnAddressId:Int?
    var buyerId:Int?
    var buyerUsername:String?
    var buyerImageUrl:String?
    var saleTotalPrice:Double?
    var saleShippingPrice:Double?
    var saleBynmixCommission:Double?
    var saleMonth:String?
    var saleYear:String?
    var canCancelSale:Bool?
    var saleSubStatus:String?
    var saleTotalDisplayPrice:Double?
    var items:[SalesDetail]?
    var totalPages: Int?
    
    var rating:Int?
    var feedback:String?
    var ribbonColor:String?
    var isComplete:Bool?
    var points:Int?
    var pointsStatusId:Int?
    var pointsStatus:String?
    var shippingPrice:Double?
    var listingId:Int?
    var listingTitle:String?
    var listingImageUrl:String?
    var userId:Int?
    var username:String?
    var userImageUrl:String?
    var buyerAndSellerInfo:BuyerAndSellerInfo?
    var shippedTo:Address?
    var promotions:[Int]?
    var promotionsFormatted:[String]?
    var canPrintShippingLabel:Bool?
    var canLeaveFeedback:Bool?
    var canOpenDispute:Bool?
    var hasOpenDispute:Bool?
    var hasSubStatus:Bool?
    var hasViewableDispute:Bool?
    var disputeId:Int?
    var indexOfItem:Int?
    var purchasedWithPromotions:[Promotions]?
    var returnRequested:Int?
    var returnOrderShipped:Int?
    var returnOrderInTransit:Int?
    var returnOrderDelivered:Int?
    var returnLabelPrinted:Int?
    var returnedTo:Address?
    var iconImageUrlShort:String?
    var iconImageUrl:String?
    var updatedDateFormatted:String?
    
    init(saleId:Int,rating:Int,feedback:String) {
        self.saleId = saleId
        self.rating = rating
        self.feedback = feedback
    }
    
}
