import Foundation

struct SelectedFilter:Codable {
    var id:Int?
    var name:String?
    var type:Int?
    var subType:Int?
    var subTypeName:String?
}
