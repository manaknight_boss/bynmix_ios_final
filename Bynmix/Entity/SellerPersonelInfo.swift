import Foundation

struct SellerPersonelInfo:Codable {
    var dobDay:Int?
    var dobMonth:Int?
    var dobYear:Int?
    var addressId:Int?
    var lastFourSocial:String?
    
    init(dobDay:Int,dobMonth:Int,dobYear:Int,addressId:Int,lastFourSocial:String) {
        self.dobDay = dobDay
        self.dobMonth = dobMonth
        self.dobYear = dobYear
        self.addressId = addressId
        self.lastFourSocial = lastFourSocial
    }
}
