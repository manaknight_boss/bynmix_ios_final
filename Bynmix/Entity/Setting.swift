import Foundation
import UIKit

struct Setting:Codable {
    var notificationId: Int?
    var notificationDate: String?
    var notificationDateFormatted: String?
    var notificationTypeId: Int?
    var notificationType: String?
    var initiatorUserId: Int?
    var iconUrlShort: String?
    var iconUrl: String?
    var isUnread: Bool?
    var listingId: Int?
    var listingImage: String?
    var listingTitle: String?
    var listingShippingPrice:Double?
    var offerActive:Bool?
    var sellerId: Int?
    var createdDate: String?
    var userId: Int?
    var username: String?
    var userImage: String?
    var isActiveOffer: Bool?
    var isLoggedInUserOffer: Bool?
    var formattedText: String?
    var items:[Setting]?
    var totalItems: Int?
    var totalPages: Int?
    var itemsPerPage:Int?
    var pageNumber: Int?
    var sortDirection: String?
    var sortField: String?
    var promotionTypeId: Int?
    var promotionType: String?
    var isActive:Bool?
    var postLink:String?
    var postId:Int?
    var isSellerOffer:Bool?
    var offerId:Int?
    var buyerUsername:String?
    var offerPrice:Double?
    var offerDateFormatted:String?
    var notificationIds:[Int]?
    var notificationRedirectTypeId: Int?
    var notificationRedirectType: String?
    var notificationRedirectId: Int?
    var idType: String?
    
    init(notificationIds:[Int]){
        self.notificationIds = notificationIds
    }
    
}
