import Foundation

struct ShipRates:Codable {
    
    var bestRate :Bool?
    var cheapestRate :Bool?
    var durationDescription: String?
    var provider : String?
    var rate : Double?
    var serviceLevel :String?
    var shippingRateObjectId :String?
    var weightRangeDescription : String?
    
    var buyerId: Int?
    var addressId: Int?
    
    var id: Int?
    var objectId: String?
    var amount: Double?
    var currency: String?
    var amountLocal: Double?
    var currencyLocal: String?
    var serviceLevelName: String?
    var serviceLevelToken: String?
    var duration: String?
    var zone: Int?
    var cheapestValue: Bool?
    var bestValue: Bool?
    
    init(buyerId:Int,addressId:Int) {
        self.buyerId = buyerId
        self.addressId = addressId
    }
    
    init(shippingRateObjectId:String) {
        self.shippingRateObjectId = shippingRateObjectId
    }
}
