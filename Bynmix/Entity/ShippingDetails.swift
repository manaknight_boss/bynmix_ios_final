import Foundation

struct ShippingDetails:Codable {
    var id: Int?
    var shippingPrice: Double?
    var hasCurrentPromotion: Bool?
    var promotionDescription: String?
}
