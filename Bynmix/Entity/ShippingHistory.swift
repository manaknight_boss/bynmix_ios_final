import Foundation

struct ShippingHistory:Codable {
    var  trackingLogId:Int?
    var  carrier: String?
    var  eta: String?
    var  trackingNumber:String?
    var  locationCity:String?
    var  locationCountry:String?
    var  locationState:String?
    var  locationZip:String?
    var  originalETA:String?
    var  status:String?
    var  statusCreated:String?
    var  statusDate:String?
    var  statusDetails:String?
    var  displayStatus:String?
}
