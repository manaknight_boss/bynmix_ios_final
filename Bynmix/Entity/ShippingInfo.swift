import Foundation

struct ShippingInfo:Codable {
    var shippingRateId :Int?
    var rate:Double?
    var shippingType: ShippingType?
    var maxHeightDimension: Double?
    var maxLengthDimension: Double?
    var maxWidthDimension: Double?
    var shippingWeightMax:Double?
    var shippingWeightMin: Double?
    var isSelected:Bool?
    var shippingWeightOunceMax:Double?
    var shippingWeightOunceMin:Double?
    
}
