import Foundation

struct ShippingRate:Codable {
    var shippingRate: Double?
    var listingWeight: Double?
    var listingShippingType:String?
    var listingShippingTypeId : Int?
    var listingWeightOunces:Int?
    var listingWeightPounds :Int?
    var shipRatesForFreeShipping:[ShipRates]?
    var shippingType:String?
    
}
