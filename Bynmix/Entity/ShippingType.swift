import Foundation

struct ShippingType:Codable {
    var shippingTypeId: Int?
    var carrier: String?
    var type: String?
    var description: String?
}
