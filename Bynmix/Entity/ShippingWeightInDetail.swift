import Foundation

struct ShippingWeightInDetail {
    var pounds:Double?
    var ounces:Double?
    var length:Double?
    var width:Double?
    var height:Double?
    
}
