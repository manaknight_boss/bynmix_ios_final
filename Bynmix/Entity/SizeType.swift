import Foundation

struct SizeType:Codable {
    var sizeTypeId:Int?
    var sizeTypeName:String?
    var sizes: [Sizes]?
    var displayGroups: [String]?
    var isSelected:Bool?
}
