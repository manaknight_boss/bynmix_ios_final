import Foundation

struct Sizes:Codable {
    var sizeId: Int?
    var sizeName: String?
    var fit: String?
    var displayGroup: String?
    var isSelected: Bool?
    
    init(sizeId:Int,sizeName:String){
        self.sizeId = sizeId
        self.sizeName = sizeName
    }
}
