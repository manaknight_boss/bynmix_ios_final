import Foundation

struct SliderImages:Codable {
    var listingImageId:Int?
    var displayOrder:Int?
    var imageUrl:String?
    var videoUrl:String?
}
