import Foundation

struct Styles:Codable {
    var descriptorId: Int?
    var descriptorName: String?
    var isSelected:Bool?
}
