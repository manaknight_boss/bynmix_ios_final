import Foundation

struct SubOfferHistory:Codable{
    
    static let TYPE_OFFER:Int = 1
    static let TYPE_ACCEPTED:Int = 3
    static let TYPE_DECLINED:Int = 2
    
    var offerDate:String?
    var offerDateFormatted:String?
    var offerPrice:Int?
    var offerExpirationDate:String?
    var offerOrder:Int?
    var previousOfferId:Int?
    var isCounterOffer:Bool?
    var isSellerCounterOffer:Bool?
    var isBuyerOffer:Bool?
    var cardId:Int?
    var addressId:Int?
    var sellerReturnAddressId:Int?
    var offerId:Int?
    var offerType:Int?
    var offerTypeName:String?
    var buyerId:Int?
    var buyerUsername:String?
    var buyerUserImage:String?
    var sellerId:Int?
    var sellerUsername:String?
    var sellerUserImage:String?
    var listingId:Int?
    var listingTitle:String?
    var listingImage:String?
    var isListingBuyItNowOnly:Bool?
    var createdDate:String?
    var isActiveOffer:Bool?
    var declinedReason:String?
    var isDeclinedBySeller:Bool?
    var declinedPrice:Double?
    var declinedDateFormatted:String?
    
    var isAcceptedByBuyer:Bool?
    var acceptedPrice:Double?
    var acceptedDateFormatted:String?
    var isSellerOffer:Bool?
    var isOfferCancelable:Bool?
    var isUnread:Bool?
    var items:[SubOfferHistory]?
    var totalPages:Int?
    var initiatorUserId:Int?
    var notificationRedirectTypeId: Int?
    var notificationRedirectType: String?
    var notificationRedirectId: Int?
    var idType: String?
    var shippingRateObjectId:String?
    
    init(offerPrice:Int,addressId:Int,listingId:Int,listingTitle:String,offerId:Int,sellerUsername:String,buyerUsername:String,cardId:Int){
        
        self.offerPrice = offerPrice
        self.addressId = addressId
        self.listingId = listingId
        self.listingTitle = listingTitle
        self.offerId = offerId
        self.sellerUsername = sellerUsername
        self.buyerUsername = buyerUsername
        self.cardId = cardId
        
    }
    
    init(offerPrice:Int,addressId:Int,listingId:Int,listingTitle:String,offerId:Int,sellerUsername:String,buyerUsername:String){
        
        self.offerPrice = offerPrice
        self.addressId = addressId
        self.listingId = listingId
        self.listingTitle = listingTitle
        self.offerId = offerId
        self.sellerUsername = sellerUsername
        self.buyerUsername = buyerUsername
        
    }
    
    init(offerPrice:Int,addressId:Int,listingId:Int,listingTitle:String,offerId:Int,sellerUsername:String,buyerUsername:String,shippingRateObjectId:String){
        
        self.offerPrice = offerPrice
        self.addressId = addressId
        self.listingId = listingId
        self.listingTitle = listingTitle
        self.offerId = offerId
        self.sellerUsername = sellerUsername
        self.buyerUsername = buyerUsername
        self.shippingRateObjectId = shippingRateObjectId
    }
    
    init(offerPrice:Int,addressId:Int,listingId:Int,listingTitle:String,offerId:Int,sellerUsername:String,buyerUsername:String,cardId:Int,shippingRateObjectId:String){
        
        self.offerPrice = offerPrice
        self.addressId = addressId
        self.listingId = listingId
        self.listingTitle = listingTitle
        self.offerId = offerId
        self.sellerUsername = sellerUsername
        self.buyerUsername = buyerUsername
        self.cardId = cardId
        self.shippingRateObjectId = shippingRateObjectId
        
    }
    
}
