import Foundation

struct SuggestedAddress:Codable {
    var originalAddress: Address?
    var suggestedAddress:Address?
}
