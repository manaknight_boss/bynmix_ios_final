import Foundation

struct Tags : Decodable {
    var descriptorId : Int?
    var descriptorName : String?
    var descriptor : String?
    var isSelected : Bool?
    var items:[Tags]?
    var totalItems: Int?
    var totalPages: Int?
    var itemsPerPage: Int?
    var pageNumber: Int?
    
}
