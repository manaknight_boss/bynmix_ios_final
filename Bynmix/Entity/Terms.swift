import Foundation

struct Terms: Decodable {
    var order : Int?
    var title : String?
    var description : String?
    var points : [String]?
    var subSections : [Terms]?
    var sections:[Terms]?
    
    init(order: Int, title: String, description: String) {
        self.order = order
        self.title = title
        self.description = description
    }
}


