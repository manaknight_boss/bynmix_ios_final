import Foundation

struct TrackSearchOfUser:Codable {
    
    var searchBarLocation: Int?
    var searchString: String?
    var searchFilters: String?
    
}
