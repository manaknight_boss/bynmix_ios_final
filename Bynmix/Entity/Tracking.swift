import Foundation

struct Tracking:Codable {
    var trackingNumber:String?
    var listingTitle:String?
    var listingId:Int?
    var shippingHistory:[ShippingHistory]?
    var returnHistory:[ShippingHistory]?
}
