import Foundation

struct User: Codable {
    var userId: Int?
    var username: String?
    var photoUrl: String?
    var createdDate: String?
    var dateJoined: String?
    var firstname: String?
    var lastname: String?
    var userBrands: [String]?
    var userBrandsFormatted: String?
    var userDescriptors: [String]?
    var userDescriptorsFormatted: String?
    var title: String?
    var items: [User]?
    var isSelected : Bool?
    var lastOnboardingScreenCompleted: Int?
    var onboardingComplete: Bool?
    var followersCount: Int?
    var followingCount: Int?
    var state: String?
    var stateAbbreviated:String?
    var shortBio:String?
    var city: String?
    var heightFeet: Int?
    var heightInches:Int?
    var bodyTypeId: Int?
    var gender:Int?
    var blogLink:String?
    var facebookLink:String?
    var instagramLink:String?
    var twitterLink:String?
    var pinterestLink:String?
    var googlePlusLink:String?
    var shortTitle : String?
    var isLoggedInUserFollowing :Bool?
    var backgroundPhoto:String?
    var viewControllerType:Bool?
    var isUserSameAsLoggedInUser:Bool?
    var totalPages: Int?
    var rating:Double?
    var isUserFollowing:Bool?
    var youtubeLink: String?
    var tikTokLink: String?
    var instagramUsername:String?
    var youtubeUsername:String?
    var facebookUsername:String?
    
    init(shortTitle: String,gender:  Int,bodyTypeId: Int,heightFeet: Int,heightInches: Int,onboardingComplete: Bool) {
        self.shortTitle = shortTitle
        self.gender = gender
        self.bodyTypeId = bodyTypeId
        self.heightFeet = heightFeet
        self.heightInches = heightInches
        self.onboardingComplete = onboardingComplete
    }
    
    init(shortTitle: String,bodyTypeId: Int,heightFeet: Int,heightInches: Int,onboardingComplete: Bool) {
        self.shortTitle = shortTitle
        self.bodyTypeId = bodyTypeId
        self.heightFeet = heightFeet
        self.heightInches = heightInches
        self.onboardingComplete = onboardingComplete
    }
    
    init(facebookUsername:String,facebookLink:String) {
        self.facebookUsername = facebookUsername
        self.facebookLink = facebookLink
    }

    
}
