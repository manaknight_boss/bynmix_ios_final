import Foundation

struct UserAuth : Codable {
    var username : String?
    var password : String?
    var grantType : String?
    var client_id : String?
    var client_secret : String?
    var access_token : String?
    var error : String?
    var error_description : String?
    var firstname : String?
    var lastname : String?
    var email : String?
    var isMobileRegistration : Bool?
    var appDeviceId : String?
    var appDeviceType : String?
    var provider: String?
    var message: String?
    var refresh_token: String?
    var eventType:Int?
    
    init(username: String, password: String) {
        self.username = username
        self.password = password
        self.grantType = "password"
        self.client_secret = AppConstants.CLIENT_SECRET
        self.client_id = AppConstants.CLIENT_ID
    }
    
    init(firstname: String, lastname: String, username: String, email: String, password: String) {
        self.init(username: username, password: password)
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.isMobileRegistration = true
        self.appDeviceId = AppDefaults.getDeviceId()
        self.appDeviceType = "ios"
    }
    
    init(firstname: String, lastname: String, email: String, accessToken: String, provider: String) {
        self.firstname = firstname
        self.lastname = lastname
        self.email = email
        self.isMobileRegistration = true
        self.appDeviceId = AppDefaults.getDeviceId()
        self.appDeviceType = "ios"
        self.provider = provider
        self.client_id = AppConstants.CLIENT_ID
        self.client_secret = AppConstants.CLIENT_SECRET
        self.access_token = accessToken
    }
    
    init(eventType:Int) {
        self.eventType = eventType
    }
    
}
