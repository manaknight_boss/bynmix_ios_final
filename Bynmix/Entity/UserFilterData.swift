import Foundation

struct UserFilterData:Codable {
    var searchText:String?
    var userId:String?
    var styleIds:[Int]?
    var brandIds:[Int]?
    var bodyTypeIds:[Int]?
    var customMinHeightFeet:String?
    var customMinHeightInches:String?
    var customMaxHeightFeet:String?
    var customMaxHeightInches:String?
    
}
