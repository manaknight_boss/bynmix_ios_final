import Foundation

struct states: Decodable {
    var stateId: Int?
    var abbreviation: String?
    var name: String?
}
