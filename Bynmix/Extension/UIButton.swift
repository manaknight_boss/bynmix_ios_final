import Foundation
import UIKit

extension UIButton {
    
     func hasImage(named imageName: String, for state: UIControl.State) -> Bool {
        guard let buttonImage = image(for: state), let namedImage = UIImage(named: imageName) else {
            return false
        }
        
        return buttonImage.pngData() == namedImage.pngData()
    }
}
