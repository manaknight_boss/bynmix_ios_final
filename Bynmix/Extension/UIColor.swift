import UIKit

extension UIColor {
    
    
    class var c848484 : UIColor{
        return UIColor(hex: "848484")
    }

    class var c202020: UIColor {
        return UIColor(hex: "202020")
    }
    
    class var cBB189C: UIColor {
        return UIColor(hex: "BB189C")
    }
    
    class var cBDBDBD: UIColor {
        return UIColor(hex: "bdbdbd")
    }
    
    class var cE3E3E3: UIColor {
        return UIColor(hex: "e3e3e3")
    }
    
    class var cE8E8E8: UIColor {
        return UIColor(hex: "e8e8e8")
    }
    
    class var cCDCDCD: UIColor {
        return UIColor(hex: "cdcdcd")
    }
    
    class var cDCDCDC: UIColor {
        return UIColor(hex: "dcdcdc")
    }
    
    class var cFB9D52: UIColor {
        return UIColor(hex: "fb9d52")
    }
    
    class var c2A2A2A: UIColor {
        return UIColor(hex: "2a2a2a")
    }
    
    class var c32afff: UIColor {
        return UIColor(hex: "32afff")
    }
    
    class var cD53A95: UIColor {
        return UIColor(hex: "d53a95")
    }
    
    class var cF3608C: UIColor {
        return UIColor(hex: "f3608c")
    }
    class var cFF6DE3: UIColor {
        return UIColor(hex: "ff6de3")
    }
    
    class var c414141: UIColor {
        return UIColor(hex: "414141")
    }
    //dialog higlight color
    class var cEA9DDC: UIColor {
        return UIColor(hex: "ea9ddc")
    }
    
    class var cC80000: UIColor {
        return UIColor(hex: "c80000")
    }
    
    class var cE17500: UIColor {
        return UIColor(hex: "e17500")
    }
    
    class var c696969: UIColor {
        return UIColor(hex: "696969")
    }
    
    class var cB8B8B8: UIColor {
        return UIColor(hex: "b8b8b8")
    }
    
    class var c545454: UIColor {
        return UIColor(hex: "545454")
    }
    
    class var cFF80EE: UIColor {
        return UIColor(hex: "ff80ee")
    }
    class var cB70000: UIColor {
        return UIColor(hex: "b70000")
    }
    class var cDC4293: UIColor {
        return UIColor(hex: "dc4293")
    }
    
    class var c151515: UIColor {
        return UIColor(hex: "151515")
    }
    
    class var c3B3B3B: UIColor {
        return UIColor(hex: "3b3b3b")
    }
    
    class var cB1B1B1: UIColor {
        return UIColor(hex: "b1b1b1")
    }
    
    class var cF5A623: UIColor {
        return UIColor(hex: "f5a623")
    }
    
    class var c333333: UIColor {
        return UIColor(hex: "333333")
    }
    
    class var c343434: UIColor {
        return UIColor(hex: "343434")
    }
    
    class var c464646: UIColor {
        return UIColor(hex: "464646")
    }
    
    class var c272727: UIColor {
        return UIColor(hex: "272727")
    }
    
    class var c2E2E2E: UIColor {
        return UIColor(hex: "2e2e2e")
    }
    
    class var c424242: UIColor {
        return UIColor(hex: "424242")
    }
    
    class var c84A666: UIColor {
        return UIColor(hex: "84a666")
    }
    
    class var c6B8652: UIColor {
        return UIColor(hex: "6b8652")
    }
    
    class var cE26E00: UIColor {
        return UIColor(hex: "e26e00")
    }
    
    class var cB05600: UIColor {
        return UIColor(hex: "b05600")
    }
    
    class var c0089B3: UIColor {
        return UIColor(hex: "0089b3")
    }
    
    class var c006483: UIColor {
        return UIColor(hex: "006483")
    }
    
    class var cBCBCBC: UIColor {
        return UIColor(hex: "bcbcbc")
    }
    
    class var c434343: UIColor {
        return UIColor(hex: "434343")
    }
    class var c212121: UIColor {
        return UIColor(hex: "212121")
    }
    class var c007EE2: UIColor {
        return UIColor(hex: "007ee2")
    }
    class var c0AA90E: UIColor {
        return UIColor(hex: "0aa90e")
    }
    class var cA0A0A0: UIColor {
        return UIColor(hex: "A0A0A0")
    }
    
    class var cCBCBCB: UIColor {
        return UIColor(hex: "cbcbcb")
    }
    
    class var cFF50A9: UIColor {
        return UIColor(hex: "ff50a9")
    }
    
    class var c858DE5: UIColor {
        return UIColor(hex: "858de5")
    }
    
    class var c85E590: UIColor {
        return UIColor(hex: "85e590")
    }
    
    class var cE58585: UIColor {
        return UIColor(hex: "e58585")
    }
    
    class var c22CFFF: UIColor {
        return UIColor(hex: "22cfff")
    }
    
    class var cFDD12A: UIColor {
        return UIColor(hex: "fdd12a")
    }
    
    class var cDDDDDD: UIColor {
        return UIColor(hex: "dddddd")
    }
    
    class var cA6A6A6: UIColor {
        return UIColor(hex: "A6A6A6")
    }
    
    class var c3C3C3C: UIColor {
        return UIColor(hex: "3c3c3c")
    }
    
    class var cFFD1D1: UIColor {
        return UIColor(hex: "ffd1d1")
    }
    
    class var cDF6CE: UIColor {
        return UIColor(hex: "cdf6ce")
    }
    
    class var FFDAF8: UIColor {
        return UIColor(hex: "ffdaf8")
    }
    
    class var D0E8FF: UIColor {
        return UIColor(hex: "d0e8ff")
    }
    
    class var cF87DE8: UIColor {
        return UIColor(hex: "f87de8")
    }
    
    class var c313131: UIColor {
        return UIColor(hex: "313131")
    }
    
    class var c323232: UIColor {
        return UIColor(hex: "323232")
    }
    
    class var c4C4C4C: UIColor {
        return UIColor(hex: "4c4c4c")
    }
    
    class var c0001C2: UIColor {
        return UIColor(hex: "0001c2")
    }
    
    class var c00A200: UIColor {
        return UIColor(hex: "00A200")
    }
    
    class var c1F1F1F: UIColor {
        return UIColor(hex: "1f1f1f")
    }
    
    class var c232323: UIColor {
        return UIColor(hex: "232323")
    }
    
    class var c00A02A: UIColor {
        return UIColor(hex: "00a02a")
    }
    
    class var c00BCF0: UIColor {
        return UIColor(hex: "00bcf0")
    }
    
    class var cEA489E: UIColor {
        return UIColor(hex: "ea489e")
    }
    
    class var cFF74C2: UIColor {
        return UIColor(hex: "ff74c2")
    }
    
    class var cB5B5B5: UIColor {
        return UIColor(hex: "b5b5b5")
    }
    
    class var cFF7AC0: UIColor {
        return UIColor(hex: "ff7ac0")
    }
    
    class var cFF77C1: UIColor {
        return UIColor(hex: "ff77c1")
    }
    
    class var cFF70E7: UIColor {
        return UIColor(hex: "ff70e7")
    }
    
    class var cFF8BEA: UIColor {
        return UIColor(hex: "ff8bea")
    }
    
    class var cFF89C7: UIColor {
        return UIColor(hex: "ff89c7")
    }
    
    class var cEFEFEF: UIColor {
        return UIColor(hex: "efefef")
    }
    
    class var cAA27C1: UIColor {
        return UIColor(hex: "aa27c1")
    }
    
    class var cFA4E49: UIColor {
        return UIColor(hex: "fa4e49")
    }
    
    class var cADADAD: UIColor {
        return UIColor(hex: "adadad")
    }
    
    class var cD3D3D3: UIColor {
        return UIColor(hex: "d3d3d3")
    }
    
    class var cC7C7C7: UIColor {
        return UIColor(hex: "c7c7c7")
    }
    
    class var c80FFFF: UIColor {
        return UIColor(hex: "80ffff")
    }
    
    class var cFF86DD: UIColor {
        return UIColor(hex: "ff86dd")
    }
    
    class var c979797: UIColor {
        return UIColor(hex: "979797")
    }
    
    class var cAC0000: UIColor {
        return UIColor(hex: "ac0000")
    }
    
    class var cFFD8D8: UIColor {
        return UIColor(hex: "ffd8d8")
    }
    
    class var c383838: UIColor {
        return UIColor(hex: "383838")
    }
    
    class var cFFE9FB:UIColor{
        return UIColor(hex: "ffe9fb")
    }
    
    class var cE2E2E2:UIColor{
        return UIColor(hex: "e2e2e2")
    }
    
    class var c797979:UIColor{
        return UIColor(hex: "797979")
    }
    
    class var cAA0000:UIColor{
        return UIColor(hex: "aa0000")
    }
    
    convenience init(hex: String) {
        var red:   CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue:  CGFloat = 0.0
        var alpha: CGFloat = 1.0
        var hex:   String = hex
        
        if hex.hasPrefix("#") {
            let index = hex.index(hex.startIndex, offsetBy: 1)
            hex = String(hex[index...])
        }
        
        let scanner = Scanner(string: hex)
        var hexValue: CUnsignedLongLong = 0
        if scanner.scanHexInt64(&hexValue) {
            switch (hex.count) {
            case 3:
                red   = CGFloat((hexValue & 0xF00) >> 8)       / 15.0
                green = CGFloat((hexValue & 0x0F0) >> 4)       / 15.0
                blue  = CGFloat(hexValue & 0x00F)              / 15.0
            case 4:
                red   = CGFloat((hexValue & 0xF000) >> 12)     / 15.0
                green = CGFloat((hexValue & 0x0F00) >> 8)      / 15.0
                blue  = CGFloat((hexValue & 0x00F0) >> 4)      / 15.0
                alpha = CGFloat(hexValue & 0x000F)             / 15.0
            case 6:
                red   = CGFloat((hexValue & 0xFF0000) >> 16)   / 255.0
                green = CGFloat((hexValue & 0x00FF00) >> 8)    / 255.0
                blue  = CGFloat(hexValue & 0x0000FF)           / 255.0
            case 8:
                red   = CGFloat((hexValue & 0xFF000000) >> 24) / 255.0
                green = CGFloat((hexValue & 0x00FF0000) >> 16) / 255.0
                blue  = CGFloat((hexValue & 0x0000FF00) >> 8)  / 255.0
                alpha = CGFloat(hexValue & 0x000000FF)         / 255.0
            default:
                print("Invalid RGB string, number of characters after '#' should be either 3, 4, 6 or 8", terminator: "")
            }
        } else {
            print("Scan hex error")
        }
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
}
