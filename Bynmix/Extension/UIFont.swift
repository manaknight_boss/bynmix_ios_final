import Foundation
import UIKit

extension UIFont {
    
    static func fontRegular(size: CGFloat) -> UIFont {
        return UIFont(name: "Raleway-Regular", size: size)!
    }
    
    static func fontSemiBold(size: CGFloat) -> UIFont {
        return UIFont(name: "Raleway-SemiBold", size: size)!
    }
    
    static func fontMedium(size: CGFloat) -> UIFont {
        return UIFont(name: "Raleway-Medium", size: size)!
    }
    
    static func fontBold(size: CGFloat) -> UIFont {
        return UIFont(name: "Raleway-Bold", size: size)!
    }
    
    static func fontBoldItalic(size: CGFloat) -> UIFont {
        return UIFont(name: "Raleway-SemiBoldItalic", size: size)!
    }
    
    static func fontMediumItalic(size: CGFloat) -> UIFont {
        return UIFont(name: "Raleway-MediumItalic", size: size)!
    }
    
    static func fontExtraBold(size: CGFloat) -> UIFont {
        return UIFont(name: "Raleway-ExtraBold", size: size)!
    }
    
    static func fontMontserratHairline(size: CGFloat) -> UIFont {
        //        var descriptor = UIFontDescriptor(name: "Montserrat-Hairline", size: size)
        //        descriptor = descriptor.addingAttributes([UIFontDescriptor.AttributeName.traits : [UIFontDescriptor.TraitKey.weight : UIFont.Weight.bold]])
        //        let font = UIFont(descriptor: descriptor, size: size)
        let font = UIFont(name: "Montserrat-Hairline", size: size)!.bold
        return font
    }
    
    var bold: UIFont { return withWeight(.bold) }
    var semibold: UIFont { return withWeight(.semibold) }
    
    private func withWeight(_ weight: UIFont.Weight) -> UIFont {
        var attributes = fontDescriptor.fontAttributes
        var traits = (attributes[.traits] as? [UIFontDescriptor.TraitKey: Any]) ?? [:]
        
        traits[.weight] = weight
        
        attributes[.name] = nil
        attributes[.traits] = traits
        attributes[.family] = familyName
        
        let descriptor = UIFontDescriptor(fontAttributes: attributes)
        
        return UIFont(descriptor: descriptor, size: pointSize)
    }
    
}
