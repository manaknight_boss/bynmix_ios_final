import Foundation
import UIKit

extension UINavigationController{
    public func removePreviousController(total: Int){
        let totalViewControllers = self.viewControllers.count
        self.viewControllers.removeSubrange(totalViewControllers-total..<totalViewControllers - 1)
    }}
