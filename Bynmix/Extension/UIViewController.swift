import Foundation
import UIKit

extension UIViewController{
    
    func heightForCommentsTableView(labelText:String, labelWidth:CGFloat) -> CGFloat{
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: labelWidth, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byCharWrapping
        label.text = labelText
        
        label.sizeToFit()
        
        return (label.frame.height )
    }
    
}
