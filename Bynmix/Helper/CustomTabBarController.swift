import UIKit

class CustomTabBarController: UITabBarController,UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        tabBarItemsUI()
        AppConstants.INDEX_OF_CURRENT_SELECTED_TAB = 0
        AppConstants.CURRENT_TAB = 0
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)

        coordinator.animate(alongsideTransition: { [weak self] context in
            self?.tabBarItemsUI()
        })
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        print("Selected item")
        let indexOfCurrentTab = tabBar.items?.firstIndex(of: item)
        AppConstants.INDEX_OF_CURRENT_SELECTED_TAB = indexOfCurrentTab ?? 0
        
        if tabBar.selectedItem == tabBar.items![2]{
            tabBar.items?[2].image = UIImage(named: "create_tab_selected")?.withRenderingMode(.alwaysOriginal)
            tabBar.items?[0].selectedImage = UIImage(named: "feed_tab")?.withRenderingMode(.alwaysOriginal)
            tabBar.items?[1].selectedImage = UIImage(named: "browse_tab")?.withRenderingMode(.alwaysOriginal)
            tabBar.items?[3].selectedImage = UIImage(named: "alerts_tab")?.withRenderingMode(.alwaysOriginal)
            tabBar.items?[4].selectedImage = UIImage(named: "profile_tab")?.withRenderingMode(.alwaysOriginal)
        }else{
            let indexOfTab = tabBar.items?.firstIndex(of: item)
            print("pressed tabBar: \(String(describing: indexOfTab))")
            tabBar.items?[0].image = UIImage(named: "feed_tab")?.withRenderingMode(.alwaysOriginal)
            tabBar.items?[1].image = UIImage(named: "browse_tab")?.withRenderingMode(.alwaysOriginal)
            tabBar.items?[2].image = UIImage(named: "create_tab")?.withRenderingMode(.alwaysOriginal)
            tabBar.items?[3].image = UIImage(named: "alerts_tab")?.withRenderingMode(.alwaysOriginal)
            tabBar.items?[4].image = UIImage(named: "profile_tab")?.withRenderingMode(.alwaysOriginal)
            
            tabBar.items?[0].selectedImage = UIImage(named: "feed_tab_selected")?.withRenderingMode(.alwaysOriginal)
            tabBar.items?[1].selectedImage = UIImage(named: "browse_tab_selected")?.withRenderingMode(.alwaysOriginal)
            tabBar.items?[2].selectedImage = UIImage(named: "create_tab_selected")?.withRenderingMode(.alwaysOriginal)
            tabBar.items?[3].selectedImage = UIImage(named: "alerts_tab_selected")?.withRenderingMode(.alwaysOriginal)
            tabBar.items?[4].selectedImage = UIImage(named: "profile_tab_selected")?.withRenderingMode(.alwaysOriginal)
            
        }
        
        clearPreSelectedDots()
    }
    
    func clearPreSelectedDots(){
        Dots.dots.removeAll()
        Dots.removedDots.removeAll()
    }

    fileprivate func tabBarItemsUI() {
        let modelName = UIDevice.modelName
        let items: [UITabBarItem] = self.tabBar.items!
        for i in 0..<items.count {
            if modelName == "iPhone 7" || modelName == "iPhone 7 Plus" || modelName == "iPhone 8" || modelName == "iPhone 8 Plus"{
                items[i].imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
                self.title = nil
            }else{
                items[i].imageInsets = UIEdgeInsets(top: 3, left: 0, bottom: -6, right: 0)
                self.title = nil
            }
        }
        
    }

}
