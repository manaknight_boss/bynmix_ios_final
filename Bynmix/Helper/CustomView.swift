import UIKit
import AudioToolbox

class CustomView: UIView {
    
    var lastLocation = CGPoint(x: 0, y: 0)
    var touchEnded = false
    var viewOutOfFrame = false
    var limitDots:Int?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // Initialization code
        let panRecognizer = UIPanGestureRecognizer(target:self, action:#selector(detectPan))
        self.gestureRecognizers = [panRecognizer]
//        var type = -1
        for i in Dots.DOT1...Dots.DOT5 {
            var isFound = false
            for dot in Dots.dots{
                if dot.type == i {
                    isFound = true
                    break
                }
            }
            if !isFound {
                if i == Dots.DOT1{
                    self.backgroundColor = UIColor.cE58585
                }else if i == Dots.DOT2{
                    self.backgroundColor = UIColor.c85E590
                }else if i == Dots.DOT3{
                    self.backgroundColor = UIColor.c858DE5
                }else if i == Dots.DOT4{
                    self.backgroundColor = UIColor.c22CFFF
                }else if i == Dots.DOT5{
                    self.backgroundColor = UIColor.cFDD12A
                }
                break
            }
        }
        
        panRecognizer.delaysTouchesEnded = false
       limitDots = AppDefaults.getTags()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func detectPan(_ recognizer:UIPanGestureRecognizer) {
        recognizer.cancelsTouchesInView = false
        let translation  = recognizer.translation(in: self.superview)
        self.center = CGPoint(x: lastLocation.x + translation.x, y: lastLocation.y + translation.y)
        
        let dotColor = self.backgroundColor
        var checkDot:Int?
        if dotColor == UIColor.cE58585{
            checkDot = Dots.DOT1
        }else if dotColor == UIColor.c85E590{
            checkDot = Dots.DOT2
        }else if dotColor == UIColor.c858DE5{
            checkDot = Dots.DOT3
        }else if dotColor == UIColor.c22CFFF{
            checkDot = Dots.DOT4
        }else if dotColor == UIColor.cFDD12A{
            checkDot = Dots.DOT5
        }
        
        
        for i in 0..<Dots.dots.count{
            var dot = Dots.dots[i]
            if dot.type == checkDot {
                dot.xCoordinate = Double(lastLocation.x) + Double(translation.x)
                dot.yCoordinate = Double(lastLocation.y) + Double(translation.y)
                Dots.dots[i] = dot
                break
            }
        }
        
        if (!self.superview!.bounds.contains(self.frame))
        {
            //view is completely out of bounds of its super view.
            viewOutOfFrame = true
        }else{
            viewOutOfFrame = false
        }
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        removeTag()
    }
    
    func removeTag(){
        var checkDot:Int?
        if viewOutOfFrame {
            if self.backgroundColor == UIColor.cE58585{
                checkDot = Dots.DOT1
            }else if self.backgroundColor == UIColor.c85E590{
                checkDot = Dots.DOT2
            }else if self.backgroundColor == UIColor.c858DE5{
                checkDot = Dots.DOT3
            }else if self.backgroundColor == UIColor.c22CFFF{
                checkDot = Dots.DOT4
            }else if self.backgroundColor == UIColor.cFDD12A{
                checkDot = Dots.DOT5
            }
            print(Dots.dots)
            for i in 0..<Dots.dots.count {
                let dot = Dots.dots[i]
                if dot.type == checkDot {
                    if dot.postDotId != nil && dot.postDotId != 0 {
                        Dots.removedDots.append(Dots.dots[i])
                    }
                    Dots.dots.remove(at: i)
                    break
                }
            }
            self.removeFromSuperview()
            limitDots = AppDefaults.getTags()
            limitDots = limitDots! - 1
            AppDefaults.setTags(tagValue: limitDots!)
            vibrate()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        // Promote the touched view
        self.superview?.bringSubviewToFront(self)
        
        // Remember original location
        lastLocation = self.center
        
    }
    
    func vibrate() {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    
}
