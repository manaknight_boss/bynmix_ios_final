import Foundation

class ValidationHelper {
    
    
    public static func isValidEmail(email:String) -> Bool {
        if email.isEmpty {
            return false
        }
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with:email)
    }
    
    public static func isValidPassword(password:String) -> Bool {
        if password.isEmpty {
            return false
        }
        if password.count < 6 {
            return false
        }
        return true
    }
    public static func isEmptyAny(field:String) -> Bool {
        if field.isEmpty {
            return false
        }
        return true
    }
    
     public static func youTubeURLmatches(link: String) -> Bool {
        let youtubeRegex = "(http(s)?:\\/\\/)?(www\\.|m\\.)?youtu(be\\.com|\\.be)(\\/watch\\?([&=a-z]{0,})(v=[\\d\\w]{1,}).+|\\/[\\d\\w]{1,})"
        
        let youtubeCheckResult = NSPredicate(format: "SELF MATCHES %@", youtubeRegex)
        return youtubeCheckResult.evaluate(with: link)
    }
    
    public static func isValidURL(link: String) -> Bool {
           let urlRegex = "(?i)https?://(?:www\\.)?\\S+(?:/|\\b)"
           
           let checkResult = NSPredicate(format: "SELF MATCHES %@", urlRegex)
           return checkResult.evaluate(with: link)
       }
   
}

