import Foundation

protocol AccountUnlinkProtocol:class {
    func accountUnlink(type:Int)
}
