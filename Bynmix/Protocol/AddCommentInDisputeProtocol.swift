import Foundation
import UIKit

protocol AddCommentInDisputeProtocol:class {
    func addCommentInDispute(dispute:DisputeDetails,comment:String,images:[UIImage])
}
