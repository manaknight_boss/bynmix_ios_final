import Foundation

protocol AddImageProtocol {
    func addImage(type:Int)
}
