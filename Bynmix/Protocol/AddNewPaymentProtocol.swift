import Foundation

protocol AddNewPaymentProtocol:class {
    func addNewPayment(type:Int , card:Card)
}
