import Foundation

protocol AddPaymentSuggestedAddressProtocol:class {
    func addPaymentSuggestedAddress(token:String,address:Address,fullName:String,type:Int)
}

