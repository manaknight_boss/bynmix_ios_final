import Foundation

protocol AddPaymentWithExistingAddressSuggestedAddressProtocol:class{
    func  addPaymentWithExistingAddressSuggestedAddress(token:String,type:Int)
}
