import Foundation

protocol BuyNowReturnProtocol:class {
    func buyNowReturn()
}
