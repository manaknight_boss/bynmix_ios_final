import Foundation

protocol CancelDisputeProtocol:class {
    func cancelDispute(disputeDetails:DisputeDetails)
}
