import Foundation

protocol CancelDisputeRefreshProtocol:class {
    func cancelDisputeRefresh()
}
