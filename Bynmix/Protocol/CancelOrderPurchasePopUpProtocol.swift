import Foundation

protocol CancelOrderPurchasePopUpProtocol:class {
    func cancelOrderPurchasePopUp(purchase:PurchaseDetail)
}
