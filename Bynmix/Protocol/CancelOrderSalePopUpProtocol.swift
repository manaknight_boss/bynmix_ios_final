import Foundation

protocol CancelOrderSalePopUpProtocol:class {
    func cancelOrderSalePopUp(sale:SalesDetail)
}
