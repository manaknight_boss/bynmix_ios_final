import Foundation

protocol CancelOrderSaleProtocol:class {
    func cancelOrderSale(listingTitle:String)
}
