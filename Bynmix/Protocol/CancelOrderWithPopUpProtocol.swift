import Foundation

protocol CancelOrderWithPopUpProtocol:class {
    func cancelOrderWithPopUp(type:Int)
}
