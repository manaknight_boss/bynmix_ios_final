import Foundation

protocol CancelPurchaseProtocol:class {
    func cancelPurchase(purchase:PurchaseDetail)
}
