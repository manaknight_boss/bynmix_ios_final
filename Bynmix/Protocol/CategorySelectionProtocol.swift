import Foundation

protocol CategorySelectionProtocol: class {
    func onCategoryClick(categoryType: CategoryType, selectedCategory: Category)
}
