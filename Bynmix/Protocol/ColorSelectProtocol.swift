import Foundation

protocol ColorSelectProtocol {
    func colorSelect(selectedColors: [Colors])
}
