import Foundation

protocol ConfirmDeliveryProtocol:class {
    func confirmDelivery(type:Int)
}
