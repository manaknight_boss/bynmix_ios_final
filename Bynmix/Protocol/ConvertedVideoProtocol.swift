import Foundation
import Photos

protocol ConvertedVideoProtocol:class {
    func convertedVideo(url:String,avAsset:AVAsset,asset:PHAsset,avUrlAsset:URL)
}
