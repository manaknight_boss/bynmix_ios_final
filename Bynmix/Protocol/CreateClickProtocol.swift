import Foundation

protocol CreateClickProtocol: class {
    func onCreateClick(type:Int)
}
