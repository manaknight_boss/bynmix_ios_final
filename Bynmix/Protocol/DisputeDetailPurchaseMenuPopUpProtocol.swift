import Foundation

protocol DisputeDetailPurchaseMenuPopUpProtocol:class {
    func disputeDetailPurchaseMenuPop(purchase:PurchaseDetail)
}
