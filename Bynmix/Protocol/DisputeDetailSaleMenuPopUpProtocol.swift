import Foundation

protocol DisputeDetailSaleMenuPopUpProtocol:class {
    func disputeDetailSaleMenuPopUp(sale:SalesDetail)
}
