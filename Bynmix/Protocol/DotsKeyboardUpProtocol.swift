import Foundation

protocol DotsKeyboardUpProtocol:class {
    func dotsKeyboardUp(type:Int)
}
