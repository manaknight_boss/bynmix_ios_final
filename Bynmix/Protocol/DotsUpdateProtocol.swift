import Foundation
import UIKit

protocol  DotsUpdateProtocol:class {
    func onDotsUpdate(dots:[Dots], removedDots: [Dots])
}
