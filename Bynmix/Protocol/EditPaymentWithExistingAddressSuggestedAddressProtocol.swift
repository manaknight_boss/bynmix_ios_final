import Foundation

protocol EditPaymentWithExistingAddressSuggestedAddressProtocol:class {
    func editPaymentWithExistingAddressSuggestedAddress(cardId:Int,nameOnCard:String,expirationMonth:Int,expirationYear:Int)
}
