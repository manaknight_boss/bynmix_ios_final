import Foundation

protocol EscalateDisputeProtocol:class {
    func escalateDispute(type:Int,disputeDetails:DisputeDetails)
}
