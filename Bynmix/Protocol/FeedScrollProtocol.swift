import Foundation
import UIKit
protocol FeedScrollProtocol {
    func onScroll(scrollValue: CGFloat)
}
