import Foundation

protocol FilterSelectedProtocol:class {
    func isFilterSelected(type:Int, id:Int , subType:Int , isParentType:Bool) -> Bool
}
