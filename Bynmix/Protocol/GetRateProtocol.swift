import Foundation

protocol GetRateProtocol:class {
    func getRate(shippingInfo:ShippingInfo)
}
