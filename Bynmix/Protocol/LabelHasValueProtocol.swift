import Foundation

protocol LabelHasValueProtocol:class {
    func labelHasValue()
}
