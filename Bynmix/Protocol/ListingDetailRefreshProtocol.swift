import Foundation

protocol ListingDetailRefreshProtocol:class {
    func listingDetailRefresh()
}
