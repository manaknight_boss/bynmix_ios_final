import Foundation

protocol ListingHasImageProtocol:class {
    func listingHasImage(type:Int)
}
