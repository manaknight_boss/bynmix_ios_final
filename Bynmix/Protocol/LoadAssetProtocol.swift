import Foundation
import Photos

protocol LoadAssetProtocol:class {
    func loadAssetMethod(asset:AVAsset)
}
