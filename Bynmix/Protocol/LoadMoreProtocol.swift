import Foundation

protocol LoadMoreProtocol : class {
    func onLoadMore()
}
