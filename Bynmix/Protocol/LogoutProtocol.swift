import Foundation

protocol LogoutProtocol:class {
    func logout()
}
