import Foundation

protocol MakeAnOfferProtocol:class {
    func makeAnOffer(listingDetail:ListingDetail,type:Int,offeredPrice:Int)
}
