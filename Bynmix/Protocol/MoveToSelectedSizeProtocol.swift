import Foundation

protocol MoveToSelectedSizeProtocol:class {
    func moveToSelectedSize(sizeType:[SizeType],selectedSize:Sizes)
}
