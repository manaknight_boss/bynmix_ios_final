import Foundation

protocol NotificationsUpdatedProtocol:class {
    func notificationsUpdated()
}
