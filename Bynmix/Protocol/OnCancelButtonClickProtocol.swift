import Foundation

protocol OnCancelButtonClickProtocol:class {
    func onCancelButtonClick(subOfferHistory:SubOfferHistory,index:Int)
}
