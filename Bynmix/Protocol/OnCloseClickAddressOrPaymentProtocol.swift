import Foundation

protocol OnCloseClickAddressOrPaymentProtocol:class {
    func onCloseClickAddressOrPayment()
}
