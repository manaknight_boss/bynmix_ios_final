import Foundation
import UIKit

protocol OnCreateGIFClickProtocol:class {
    func onCreateGIFClick(image:UIImage)
}
