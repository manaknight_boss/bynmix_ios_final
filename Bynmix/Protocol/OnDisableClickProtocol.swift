import Foundation

protocol OnDisableClickProtocol: class {
    func onDisableClick(feed:Feed)
}
