import Foundation

protocol OnFeedbackSubmitClickProtocol:class {
    func onFeedbackSubmitClick(type:Int, id:Int, rating:Int, comment:String)
}
