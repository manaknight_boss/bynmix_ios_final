
import Foundation
import UIKit
protocol OnImageChangeProtocol: class {
    func onImageChange(image: UIImage)
}
