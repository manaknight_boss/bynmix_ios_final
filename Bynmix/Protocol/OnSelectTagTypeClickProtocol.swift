import Foundation
import UIKit

protocol  OnSelectTagTypeClickProtocol:class {
    func onSelectTagTypeClick(buttonSender:UIButton,tapGestureSender:UITapGestureRecognizer)
}
