import Foundation

protocol PageRedirectToPurchaseListProtocol:class {
    func pageRedirectToPurchaseList(purchase:PurchaseDetail)
}
