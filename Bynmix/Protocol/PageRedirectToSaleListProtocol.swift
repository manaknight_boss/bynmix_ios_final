import Foundation

protocol PageRedirectToSaleListProtocol:class {
    func pageRedirectToSaleList(sale:SalesDetail)
}
