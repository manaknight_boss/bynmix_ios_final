import Foundation

protocol PageRedirectToSaleProtocol:class {
    func pageRedirectToSale()
}
