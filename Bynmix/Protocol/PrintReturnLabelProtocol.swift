import Foundation

protocol PrintReturnLabelProtocol:class {
    func printReturnLabel(type:Int,id:Int)
}
