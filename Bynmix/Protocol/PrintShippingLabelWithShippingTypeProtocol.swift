import Foundation

protocol PrintShippingLabelWithShippingTypeProtocol:class {
    func printShippingLabelWithShippingType(type:Int,id:Int)
}
