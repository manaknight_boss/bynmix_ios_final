import Foundation

protocol RefreshOtherUserProfileProtocol:class {
    func refreshOtherUserProfile()
}
