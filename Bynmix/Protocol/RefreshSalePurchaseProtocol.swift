import Foundation

protocol RefreshSalePurchaseProtocol:class {
    func refreshSalePurchase()
}
