import Foundation

protocol RefundBuyerWithoutReturnProtocol:class {
    func refundBuyerWithoutReturn(type:Int,disputeDetails:DisputeDetails)
}
