import Foundation

protocol ReplaceCardClickProtocol:class {
    func replaceCardClick(oldCard:Card,newCard:Card)
}
