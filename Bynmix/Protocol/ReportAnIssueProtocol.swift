import Foundation

protocol ReportAnIssueProtocol:class {
    func reportAnIssue()
}
