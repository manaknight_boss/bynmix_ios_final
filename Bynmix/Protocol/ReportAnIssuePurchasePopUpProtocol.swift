import Foundation

protocol ReportAnIssuePurchasePopUpProtocol:class {
    func reportAnIssuePurchasePopUp(purchase:PurchaseDetail)
}
