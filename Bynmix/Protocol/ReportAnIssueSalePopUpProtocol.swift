import Foundation

protocol ReportAnIssueSalePopUpProtocol:class {
    func reportAnIssueSalePopUp(sale:SalesDetail)
}
