import Foundation

protocol ReturnDeliveredProtocol:class {
    func returnDelivered()
}
