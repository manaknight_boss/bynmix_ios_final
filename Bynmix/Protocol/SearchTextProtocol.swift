import Foundation

protocol SearchTextProtocol:class {
    func searchText(type:Int,searchText:String)
}
