import Foundation

protocol SelectListingProtocol: class {
    func selectListing(selectListing:ListingTitles,index:Int,type:Int)
}
