import Foundation

protocol SelectShipRateProtocol:class {
    func selectShipRate(shipRate:ShipRates,saleId:Int)
}
