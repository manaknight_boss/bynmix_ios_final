import Foundation

protocol SellerSideBackToOptionsProtocol:class {
    func sellerSideBackToOptions(type:Int,disputeDetails:DisputeDetails)
}
