import Foundation

protocol SellerSideEscalateDisputeProtocol:class {
    func sellerSideEscalateDispute(type:Int,disputeDetails:DisputeDetails)
}
