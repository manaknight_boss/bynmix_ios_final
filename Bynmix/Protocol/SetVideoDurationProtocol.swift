import Foundation

protocol SetVideoDurationProtocol:class {
    func setVideoDurationMethod(totalTime:Int)
}
