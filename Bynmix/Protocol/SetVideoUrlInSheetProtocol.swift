import Foundation

protocol SetVideoUrlInSheetProtocol:class {
    func setVideoUrlInSheetMethod(url:URL)
}
