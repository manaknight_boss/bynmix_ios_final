import Foundation

protocol ShippingRateProtocol:class {
    func shippingRate(type:Int)
}
