import Foundation

protocol ShowEmptyStateInListingProtocol:class {
    func showEmptyStateInListing(type:Int)
}
