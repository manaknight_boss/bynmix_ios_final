import Foundation

protocol SizeSelectProtocol:class {
    func sizeSelect(size:Sizes)
}
