import Foundation

protocol StartSellingProtocol:class {
    func startSelling(type:Int)
}
