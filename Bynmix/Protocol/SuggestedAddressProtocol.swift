import Foundation

protocol SuggestedAddressProtocol:class {
    func suggestedAddress(address:Address,fullName:String,type:Int)
}
