import Foundation

protocol TagsContainsValueProtocol:class {
    func tagsContainsValue(type:Int)
}
