import Foundation

protocol  UpdateAddressProtocol:class {
    func updateAddress(type:Int,isDefault:Bool,address:Address)
}
