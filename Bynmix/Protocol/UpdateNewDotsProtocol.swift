import Foundation

protocol  UpdateNewDotsProtocol:class {
    func onNewDotsUpdate(dots:[Dots])
}
