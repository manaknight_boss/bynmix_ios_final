import Foundation

protocol UpdateSuggestedAddressProtocol:class {
    func updateSuggestedAddress(address:Address,fullName:String,type:Int,addressId:Int)
}
