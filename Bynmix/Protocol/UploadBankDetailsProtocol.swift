import Foundation

protocol UploadBankDetailsProtocol:class {
    func uploadBankDetails(type:Int,accountINfo:AccountInfo)
}
