import Foundation

protocol UserFilterApplyProtocol:class {
    func userFilterApply(selectedFilter:[SelectedFilter], heightModal: HeightModel)
}
