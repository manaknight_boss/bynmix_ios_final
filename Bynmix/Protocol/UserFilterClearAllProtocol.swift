import Foundation

protocol UserFilterClearAllProtocol:class {
    func userFilterClearAll()
}
