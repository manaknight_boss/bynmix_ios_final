import Foundation
import AVKit

protocol VideoAssetProtocol:class {
    func videoAsset(asset:AVAsset,duration:String,index:Int)
}
