import Foundation

protocol WithdrawClickProtocol:class {
    func withdrawClick(type:Int,amount:String,selectedBankAccount:Int)
}
