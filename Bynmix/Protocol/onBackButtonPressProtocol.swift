import Foundation

protocol onBackButtonPressProtocol:class {
    func onBackButtonPress(type:Int)
}
