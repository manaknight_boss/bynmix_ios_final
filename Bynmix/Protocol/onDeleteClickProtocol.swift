import Foundation

protocol OnDeleteClickProtocol: class {
    func onDeleteClick(id: Int)
}
