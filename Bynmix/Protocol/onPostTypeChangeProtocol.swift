import Foundation

protocol onPostTypeChangeProtocol:class {
    func onPostTypeChange(type:Int)
}
