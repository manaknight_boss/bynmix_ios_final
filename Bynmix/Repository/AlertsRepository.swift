import Foundation
import Alamofire

class AlertsRepository: BaseRepository {

    public func getAlertsAll(accessToken: String,page:Int ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/notifications?page=\(page)&sortDirection=DESC&ImageType=2&pageSize=25"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getMyOffers(accessToken: String,page:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/notifications/offers/my-offers?page=\(page)&sortDirection=DESC&imageType=2&pageSize=25"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getReceivedOffers(accessToken: String,page:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/notifications/offers/received-offers?page=\(page)&sortDirection=DESC&imageType=2&pageSize=25"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func offerAccepted(accessToken: String,listingId:Int,offerId:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(listingId)/actions/offer/\(offerId)/accept"
        apiCallPOST(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func offerDeclined(accessToken: String,listingId:Int,offerId:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(listingId)/actions/offer/\(offerId)/decline"
        apiCallPOST(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getMyOffersDetail(accessToken: String,listingId:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(listingId)/actions/offer-history"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getReceivedOffersDetail(accessToken: String,listingId:Int,initiatorUserId:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(listingId)/actions/offer-history/\(initiatorUserId)"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func cancelOffer(accessToken: String,listingId:Int,offerId:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(listingId)/actions/offer/\(offerId)/cancel"
        apiCallPUT(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getBuyerBids(accessToken: String,id:Int,page:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(id)/actions/buyer-bids?page=\(page)&sortDirection=DESC&imageType=2&pageSize=25"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func submitOffer(accessToken: String,id:Int,data:SubOfferHistory,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(id)/actions/buyer-offer"
        do {
            try? apiCallPOST(api: api, accessToken: accessToken, dict: JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func counterOffer(accessToken: String,listingId:Int,offerId:Int,data:SubOfferHistory,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(listingId)/actions/seller-offer/\(offerId)"
        do {
            try? apiCallPOST(api: api, accessToken: accessToken, dict: JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func updateAlertsReadNotifications(accessToken: String,data:Setting,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/notifications"
        do {
        try? apiCallPUT(api: api, accessToken: accessToken, dict: JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        }catch {
            
        }
    }
    
}
