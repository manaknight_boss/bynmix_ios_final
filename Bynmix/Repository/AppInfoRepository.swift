import Foundation
import Alamofire

class AppInfoRepository : BaseRepository {
    
    public func getTerms(_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        apiCallGET(api: "api/info/TOS", accessToken: "", dict: nil, response, error)
    }
    
    public func getPrivacyPolicy(_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        apiCallGET(api: "api/info/privacy-policy", accessToken: "", dict: nil, response, error)
    }
    
}
