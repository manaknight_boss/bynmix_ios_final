import Foundation
import Alamofire

class AuthRepository : BaseRepository {
    public func login(data: UserAuth, _ response: @escaping (UserAuth) -> Void, _ error: @escaping (ApiError) -> Void) {
        apiCallForUserAuth(api: "token", dict: data, response, error)
    }
    
    public func loginWithSocial(data: UserAuth, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        do {
            try? apiCallPOST(api: "api/accounts/oauth/login", accessToken: "", dict: JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func register(data: UserAuth, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        do {
            try? apiCallPOST(api: "api/accounts/register", accessToken: "", dict: JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
        
    }
    
    public func updateDeviceId(accessToken: String,data:DeviceIdRequest ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        do {
            try? apiCallPUT(api: "api/users/app-device/" , accessToken: accessToken, dict: JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func trackUsers(accessToken: String,data: UserAuth, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        do {
            try? apiCallPOST(api: "api/analytics/track/users/user-logins", accessToken: accessToken, dict: JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
        
    }
    
}
