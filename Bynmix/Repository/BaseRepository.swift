import Foundation
import Alamofire
import TTGSnackbar

class BaseRepository {
    
    public static let baseURL = "https://bynmixtest.azurewebsites.net/"
    let REQUEST_POST = 0
    let REQUEST_PUT = 1
    let REQUEST_GET = -1
    let REQUEST_DELETE = 2
    static var isRefreshAPICalled = false
    static var refreshAPIQueue = [APINode]()
    
    func apiCallGET(api:String, accessToken: String, dict: Data?, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        let url : URL = URL(string: BaseRepository.baseURL + api)!
        log("Calling an api using get method: \(url.absoluteString)")
        if Utils.isConnectedToNetwork() {
            
            let headers: HTTPHeaders = [
                "Authorization": "Bearer "+accessToken,
                "Accept": "application/json"
            ]
            
            let manager = Alamofire.Session.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            
            manager.request(url,headers: headers).responseJSON { (response) -> Void in
                self.log("Response === \(response.description)")
                if response.response?.statusCode == 401 {
                    self.tokenExpire(requestMethod: self.REQUEST_GET, api: api, dict: dict, onResponse, onError)
                } else {
                    switch response.result {
                    case .success:
                        do{
                            onResponse(response)
                            throw MyError.runtimeError(response.error?.localizedDescription ?? "")
                        }catch{
                            print("Whoops, couldn't fetch resource: \(error)")
                        }
                        break
                    case .failure(let error):
                        if let data = response.data {
                            print("Print Server Error: " + String(data: data, encoding: String.Encoding.utf8)!)
                        }
                        if error._code == NSURLErrorTimedOut {
                            print("REQUEST TIME OUT")
                        }
                        break
                    }
                }
            }
        } else {
            onError(ApiError(statusCode: AppConstants.NO_INTERNET, message: AppConstants.Strings.NO_INTERNET_MESSAGE))
        }
        
    }
    
    func tokenExpire(requestMethod: Int, api: String, dict: Data?, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        
        AppDefaults.setTokenExpired(isTokenExpired: true)
        
        if(AppDefaults.getRefreshToken() == "") {
            AppDefaults.setAccessToken(token: "")
            let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
            appDel.window!.rootViewController?.dismiss(animated: false, completion: nil)
        }
        if !BaseRepository.isRefreshAPICalled {
            BaseRepository.isRefreshAPICalled = true
            apiCallForRefreshToken(requestMethod: requestMethod, api: api, dict: dict, onResponse, onError)
        } else {
            BaseRepository.refreshAPIQueue.append(APINode(requestMethod: requestMethod, api: api, dict: dict, onResponse: onResponse, onError: onError))
        }
    }
    
    func apiCallPOST(api:String, accessToken: String,dict: Data?, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        apiCall(requestMethod: REQUEST_POST, api: api, accessToken: accessToken, dict: dict, onResponse, onError)
    }
    
    func apiCallDELETE(api:String, accessToken: String,dict: Data?, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        apiCall(requestMethod: REQUEST_DELETE, api: api, accessToken: accessToken, dict: dict, onResponse, onError)
    }
    
    
    func apiCallPUT(api:String, accessToken: String,dict: Data?, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        apiCall(requestMethod: REQUEST_PUT, api: api, accessToken: accessToken, dict: dict, onResponse, onError)
    }
    
    func uploadImagePOST(api:String, accessToken: String,image: UIImage, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        uploadImage(requestMethod: REQUEST_POST, api: api, accessToken: accessToken, image: image, onResponse, onError)
    }
    
    func uploadImagesPOST(api:String, accessToken: String,images: [UIImage], _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        uploadImages(requestMethod: REQUEST_POST, api: api, accessToken: accessToken, images: images, onResponse, onError)
    }
    
    func uploadImagePUT(api:String, accessToken: String,image: UIImage, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        uploadImage(requestMethod: REQUEST_PUT, api: api, accessToken: accessToken, image: image, onResponse, onError)
    }
    
    func uploadVideo(videoUrl:URL,api:String,accessToken: String, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void){
        
        let url : URL = URL(string: BaseRepository.baseURL + api)!
        log("Calling an api : \(url.absoluteString)")
        
        let timestamp = NSDate().timeIntervalSince1970 // just for some random name.
        
        AF.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(videoUrl, withName: "image", fileName: "\(timestamp).mp4", mimeType: "\(timestamp)/mp4")
        }, to: url , method: HTTPMethod.post,headers: ["Authorization": "Bearer "+accessToken]).responseJSON { (response) in
            switch response.result {
            case .success(_):
                self.log("Response === \(response.description)")
                if response.response?.statusCode == 401 {
                    
                } else {
                    onResponse(response)
                }
                break
            case .failure(let encodingError):
                print(encodingError)
                break
            }
        }
    }
    
    func uploadImages(requestMethod:Int,api:String, accessToken: String,images: [UIImage], _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        
        var imageData: [Data] = []
        
        for i in 0..<images.count {
            imageData.append(images[i].jpegData(compressionQuality: 0.7)!)
        }
        
        let url : URL = URL(string: BaseRepository.baseURL + api)!
        log("Calling an api : \(url.absoluteString)")
        
        var request:HTTPMethod
        if requestMethod == REQUEST_POST {
            request = HTTPMethod.post
        } else {
            request = HTTPMethod.put
        }
        
        AF.upload(multipartFormData: { multipartFormData in
            for i in 0..<imageData.count {
                multipartFormData.append(imageData[i], withName: "image", fileName: "image.jpg", mimeType: "image/jpeg")
            }
            
        }, to: url,usingThreshold: 1,method: request,headers: ["Authorization": "Bearer "+accessToken])
        .uploadProgress(queue: .main, closure: { (progress) in
            print("Upload Progress: \(progress.fractionCompleted)")
        })
        .responseJSON (completionHandler: { data in
            switch data.result {
            case .success(_):
                self.log("Response === \(data.description)")
                if data.response?.statusCode == 401 {
                    
                } else {
                    onResponse(data)
                }
                break
            case .failure(let encodingError):
                print(encodingError)
                break
            }
        })
    }
    
    func uploadImagesWithData(api:String, accessToken: String,image: UIImage,data:AccountVerify ,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        
        guard let imageData = image.jpegData(compressionQuality: 0.7) else {
            print("Could not get JPEG representation of UIImage")
            return
        }
        
        //let parameters = ["documentType":data.documentType,"documentSide":data.documentSide]
        
        let url : URL = URL(string: BaseRepository.baseURL + api)!
        log("Calling an api : \(url.absoluteString)")
        
        let request = HTTPMethod.put
        
        AF.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData, withName: "image", fileName: "image.jpg", mimeType: "image/jpeg")
//            for (key, value) in parameters {
//                //multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
//            }
            multipartFormData.append("\(data.documentType ?? 0)".data(using: .utf8)!, withName: "documentType")
            multipartFormData.append("\(data.documentSide ?? 0)".data(using: .utf8)!, withName: "documentSide")
            
        }, to: url,usingThreshold: 1,method: request,headers: ["Authorization": "Bearer "+accessToken])
        .uploadProgress(queue: .main, closure: { (progress) in
            print("Upload Progress: \(progress.fractionCompleted)")
        })
        .responseJSON (completionHandler: { data in
            switch data.result {
            case .success(_):
                self.log("Response === \(data.description)")
                if data.response?.statusCode == 401 {
                    
                } else {
                    onResponse(data)
                }
                break
            case .failure(let encodingError):
                print(encodingError)
                break
            }
        })
    }
    
    func uploadImage(requestMethod:Int,api:String, accessToken: String,image: UIImage, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        guard let imageData = image.jpegData(compressionQuality: 0.7) else {
            print("Could not get JPEG representation of UIImage")
            return
        }
        
        let url : URL = URL(string: BaseRepository.baseURL + api)!
        log("Calling an api : \(url.absoluteString)")
        
        var request:HTTPMethod
        if requestMethod == REQUEST_POST {
            request = HTTPMethod.post
        } else {
            request = HTTPMethod.put
        }
        
        AF.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData, withName: "image", fileName: "image.jpg", mimeType: "image/jpeg")
            
        }, to: url,usingThreshold: 1,method: request,headers: ["Authorization": "Bearer "+accessToken])
        .uploadProgress(queue: .main, closure: { (progress) in
            print("Upload Progress: \(progress.fractionCompleted)")
        })
        .responseJSON (completionHandler: { data in
            switch data.result {
            case .success(_):
                self.log("Response === \(data.description)")
                if data.response?.statusCode == 401 {
                    
                } else {
                    onResponse(data)
                }
                break
            case .failure(let encodingError):
                print(encodingError)
                break
            }
        })
    }
    
    func apiCall(requestMethod: Int, api: String, accessToken: String,dict: Data?, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        let url : URL = URL(string: BaseRepository.baseURL + api)!
        log("Calling an api : \(url.absoluteString)")
        var request = URLRequest(url: url)
        if requestMethod == REQUEST_POST {
            request.httpMethod = HTTPMethod.post.rawValue
        } else if requestMethod == REQUEST_PUT {
            request.httpMethod = HTTPMethod.put.rawValue
        }else{
            request.httpMethod = HTTPMethod.delete.rawValue
        }
        if let bodyData = dict{
            let jsonString = String(data: bodyData, encoding: .utf8)!
            log("Request Body : \(jsonString)")
        } 
        request.setValue("application/json; charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = dict
        request.addValue("Bearer "+accessToken, forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        if Utils.isConnectedToNetwork() {
            
            let manager = Alamofire.Session.default
            manager.session.configuration.timeoutIntervalForRequest = 120
            manager.request(request).responseJSON { (response) -> Void in
                self.log("Response === \(response.description)")
                if response.response?.statusCode == 401 {
                    self.tokenExpire(requestMethod: requestMethod, api: api, dict: dict, onResponse, onError)
                } else {
                    switch response.result {
                    case .success:
                        do{
                            onResponse(response)
                            throw MyError.runtimeError(response.error?.localizedDescription ?? "")
                        }catch{
                            print("Whoops, couldn't fetch resource: \(error)")
                        }
                        break
                    case .failure(let error):
                        if let data = response.data {
                            print("Print Server Error: " + String(data: data, encoding: String.Encoding.utf8)!)
                        }
                        if error._code == NSURLErrorTimedOut {
                            print("REQUEST TIME OUT")
                        }
                        break
                    }
                    
                }
            }
        } else {
            onError(ApiError(statusCode: AppConstants.NO_INTERNET, message: AppConstants.Strings.NO_INTERNET_MESSAGE))
        }
    }
    
    func apiCallForUserAuth(api:String, dict: UserAuth, _ onResponse: @escaping (UserAuth) -> Void, _ onError: @escaping (ApiError) -> Void) {
        let url : URL = URL(string: BaseRepository.baseURL + api)!
        log("Calling an api using post method: \(url.absoluteString)")
        let parameters: [String: Any] = [
            "username" : dict.username!,
            "password" : dict.password!,
            "grant_type" : dict.grantType!,
            "client_id" : dict.client_id!,
            "client_secret" : dict.client_secret!
        ]
        if Utils.isConnectedToNetwork() {
            AF.request(url, method: .post, parameters: parameters).responseJSON { (response) -> Void in
                self.log("Response === \(response.description)")
                do {
                    let response = try JSONDecoder().decode(UserAuth.self, from: response.data!)
                    onResponse(response)
                } catch {
                    onError(ApiError(statusCode: AppConstants.API_DOWN, message: AppConstants.Strings.SOMETHING_WENT_WRONG))
                }
            }
        } else {
            onError(ApiError(statusCode: AppConstants.NO_INTERNET, message: AppConstants.Strings.NO_INTERNET_MESSAGE))
        }
    }
    
    func apiCallForRefreshToken(requestMethod: Int, api: String, dict: Data?, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        let url : URL = URL(string: BaseRepository.baseURL + "token")!
        log("Calling an api using post method: \(url.absoluteString)")
        let parameters: [String: Any] = [
            "grant_type" : "refresh_token",
            "client_id" : AppConstants.CLIENT_ID,
            "refresh_token" : AppDefaults.getRefreshToken()
        ]
        if Utils.isConnectedToNetwork() {
            AF.request(url, method: .post, parameters: parameters).responseJSON { (response) -> Void in
                self.log("Response === \(response.description)")
                BaseRepository.isRefreshAPICalled = false
                if(response.response?.statusCode == 200) {
                    let response = try! JSONDecoder().decode(UserAuth.self, from: response.data!)
                    AppDefaults.setAccessToken(token: response.access_token!)
                    AppDefaults.setRefreshToken(token: response.refresh_token!)
                    if(requestMethod == self.REQUEST_GET) {
                        self.apiCallGET(api: api, accessToken: response.access_token!, dict: dict, onResponse, onError)
                    } else {
                        self.apiCall(requestMethod: requestMethod,api: api, accessToken: response.access_token!, dict: dict, onResponse, onError)
                    }
                    self.callAllPendingAPI()
                } else {
                    AppDefaults.setAccessToken(token: "")
                    AppDefaults.setRefreshToken(token: "")
                    AppDefaults.setToken(token: "")
                    AppDefaults.setPromotionId(promotionId: 0)
                    self.logout()
                }
            }
        } else {
            BaseRepository.isRefreshAPICalled = true
            onError(ApiError(statusCode: AppConstants.NO_INTERNET, message: AppConstants.Strings.NO_INTERNET_MESSAGE))
        }
    }
    
    func callAllPendingAPI() {
        for i in 0..<BaseRepository.refreshAPIQueue.count {
            let node: APINode = BaseRepository.refreshAPIQueue[i]
            if(node.requestMethod == self.REQUEST_GET) {
                self.apiCallGET(api: node.api, accessToken: AppDefaults.getAccessToken(), dict: node.dict, node.onResponse, node.onError)
            } else {
                self.apiCall(requestMethod: node.requestMethod,api: node.api, accessToken: AppDefaults.getAccessToken(), dict: node.dict, node.onResponse, node.onError)
            }
            
        }
        BaseRepository.refreshAPIQueue.removeAll()
    }
    
    func logout(){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.LOGIN, bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: AppConstants.Controllers.LOGIN_NAVIGATION_CONTROLLER) as! LoginNavigationViewController
        if !(UIApplication.shared.keyWindow?.rootViewController is LoginNavigationViewController) {
            UIApplication.shared.keyWindow?.rootViewController = viewController
        }
    }
    
    private func log(_ msg: String) {
        print("\nBase ===> \(msg)")
    }
    
    enum MyError: Error {
        case runtimeError(String)
    }
    
    func showSnackBar(message: String) {
        let snackbar = TTGSnackbar(message: message, duration: .short)
        snackbar.show()
    }
    
}
