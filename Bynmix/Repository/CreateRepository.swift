import Foundation
import Alamofire

class CreateRepository:BaseRepository{
    
    public func createPost(accessToken: String,data:CreatePostRequest,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/posts"
        do {
            try? apiCallPOST(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func uploadPostImage(accessToken: String,id:Int,image:UIImage,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/posts/\(id)/images"
        uploadImagePOST(api: api, accessToken: accessToken, image:image, response, error)
    }
    
    public func updatePost(accessToken: String,data:CreatePostRequest,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/posts/\(id)"
        do {
            try? apiCallPUT(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func deleteImage(accessToken: String, data:DeleteImagesRequest, id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/posts/\(id)/images"
        do {
            try? apiCallDELETE(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func getImageIdPost(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/posts/\(id)/images"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getDotsInfo(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/posts/\(id)"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getSellerPolicy(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/seller-policy"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getSizeTabName(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/filters/size-fits"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getSizes(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/filters/sizes"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getSizesWithFilterView(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/filters/sizes?filterView=true"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getCategory(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/filters/categories?imageType=2"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getColors(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/filters/colors"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getBrands(accessToken: String,search:String,page:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        var api = ""
        if search == ""{
             api = "api/listings/filters/brands?page=\(page)&pageSize=100"
        }else{
            let encoded = search.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
            api = "api/listings/filters/brands?page=\(page)&startLetter=\(encoded!)&pageSize=100"
        }
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getBrandsForFilters(accessToken: String,search:String,page:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        var api = ""
        if search == ""{
            api = "api/listings/filters/brands?page=\(page)&pageSize=100"
        }else{
            let encoded = search.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
            api = "api/listings/filters/brands?page=\(page)&startLetter=" + encoded! + "&pageSize=100"
        }
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func createListing(accessToken: String,data:ListingDetail,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings"
        try? apiCallPOST(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
    }
    
    public func uploadListingImages(accessToken: String,id:Int,images:[UIImage],_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(id)/images"
        uploadImagesPOST(api: api, accessToken: accessToken, images:images, response, error)
    }
    
    public func getShippingSlab(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/shipping/parcels/shipping-rates"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getAbout(_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/info/about"
        apiCallGET(api: api, accessToken: "", dict:nil, response, error)
    }
    
    public func checkSellerStatus(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/check"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func sendSellerDetail(accessToken: String,data:SellerPersonelInfo,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/transactions/account"
        do {
            try? apiCallPOST(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func getListingTitles(accessToken: String,searchQuery: String? = nil,page:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        var api = "api/listings/title"
        if let query = searchQuery {
            let encoded = query.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
            api += "?search=" + encoded! 
        }else{
            api += "?page=\(page)"
        }
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func deleteDots(accessToken: String, id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/posts/\(id)/dots"
        do {
            apiCallDELETE(api: api, accessToken: accessToken, dict: nil, response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func getCondition(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
           let api = "api/listings/filters/conditions"
           apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
       }
    
    public func getSalesTax(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/sales-tax"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func trackListings(accessToken: String,listingId:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/analytics/track/listings/\(listingId)/create"
        apiCallPOST(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func trackPosts(accessToken: String,postId:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/analytics/track/posts/\(postId)/create"
        apiCallPOST(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getPostType(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/posts/post-types"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func uploadListingVideo(accessToken: String,id:Int,videoURL:URL,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(id)/videos"
        uploadVideo(videoUrl: videoURL, api: api, accessToken: accessToken, response, error)
    }
    
    public func deleteListingVideo(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(id)/videos"
        apiCallDELETE(api: api, accessToken: accessToken, dict: nil, response, error)
    }
    
    public func updateImagesOrder(accessToken: String,data:ListingImagesOrder,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(id)/images/rearrange"
        do {
            try? apiCallPUT(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
}
