import Foundation
import Alamofire

class EditProfileRepository : BaseRepository {
    
    public func getBodyType(accessToken: String, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        apiCallGET(api: "/api/users/bodyTypes", accessToken: accessToken, dict: nil, response, error)
    }
    
    public func getStates(accessToken: String, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        apiCallGET(api: "/api/settings/states", accessToken: accessToken, dict: nil, response, error)
    }
    
}
