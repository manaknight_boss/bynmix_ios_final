import Foundation
import Alamofire

class FeedRepository: BaseRepository {
    
    public func getFeedAll(accessToken: String, data:FilterData? ,page:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        var api = ""
        let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
        if selectedIndex == 0{
            api = "api/feed/all?page=\(page)&sortDirection=DESC&pageSize=25"
        }else if selectedIndex == 1{
            api = "api/browse/all?page=\(page)&sortDirection=DESC&pageSize=26"
        }
        try? apiCallPUT(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
    }
    
    public func getFeedPosts(accessToken: String,data:FilterData? ,page:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        var api = ""
        let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
        if selectedIndex == 0{
            api = "api/feed/posts?page=\(page)&sortDirection=DESC&pageSize=26"
        }else if selectedIndex == 1{
            api = "api/browse/posts?page=\(page)&sortDirection=DESC&pageSize=26"
        }
        try? apiCallPUT(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
    }
    
    public func getFeedListings(accessToken: String,data:FilterData? ,page:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        var api = ""
        let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
        if selectedIndex == 0{
            api = "api/feed/listings?page=\(page)&sortDirection=DESC&pageSize=26"
        }else if selectedIndex == 1{
            api = "api/browse/listings?page=\(page)&sortDirection=DESC&pageSize=26"
        }
        try? apiCallPUT(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
    }
    
    public func getLikeUsers(accessToken: String,page:Int,id:Int, likesType:String, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        
        var api = "api/"+likesType
        
        if id != 0 {
            api = api + "/" + "\(id)" + "/like/all?page=" + "\(page)"
        }
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func updateLikeStatus(accessToken: String,id:Int, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        
        var api = "api/listings"
        
        if id != 0 {
            api = api + "/" + "\(id)" + "/like"
        }
        apiCallPUT(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func updatePostLikeStatus(accessToken: String,id:Int, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        
        var api = "api/posts"
        
        if id != 0 {
            api = api + "/" + "\(id)" + "/like"
        }
        apiCallPUT(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getFeedUsers(accessToken: String, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        apiCallGET(api: "api/users/best-match?pageSize=25", accessToken: accessToken, dict: nil, response, error)
    }
    
    public func getFollowFollowingUsers(accessToken: String, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        apiCallGET(api: "api/users/follow/followings", accessToken: accessToken, dict: nil, response, error)
    }
    
    public func getListingDetail(accessToken: String,id:Int, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(id)"
        apiCallGET(api: api, accessToken: accessToken, dict: nil, response, error)
    }
    
    public func getPostDetail(accessToken: String,id:Int, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/posts/\(id)"
        apiCallGET(api: api, accessToken: accessToken, dict: nil, response, error)
    }
    
    public func getListingDetailImages(accessToken: String,id:Int, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(id)/images"
        apiCallGET(api: api, accessToken: accessToken, dict: nil, response, error)
    }
    
    public func getAllUsers(accessToken: String,page:Int,data:UserFilterData? ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/users/search?page=\(page)"
        if data != nil{
            do {
               try? apiCallPUT(api: api, accessToken: accessToken, dict: JSONEncoder().encode(data), response, error) 
                throw MyError.runtimeError("\(String(describing: error))")
            } catch {
                
            }
            
        }else{
            apiCallPUT(api: api, accessToken: accessToken, dict: nil, response, error)
        }
        
    }
    
    public func getDots(accessToken: String ,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/posts/\(id)/dots"
        apiCallGET(api: api, accessToken: accessToken, dict: nil, response, error)
    }
    
    public func getDefaultCard(accessToken: String ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/transactions/card/default?imageType=2"
        apiCallGET(api: api, accessToken: accessToken, dict: nil, response, error)
    }
    
    public func getDefaultAddress(accessToken: String ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/settings/address/default-shipping"
        apiCallGET(api: api, accessToken: accessToken, dict: nil, response, error)
    }
    
    public func getDefaultReturnAddress(accessToken: String ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/settings/address/default-return"
        apiCallGET(api: api, accessToken: accessToken, dict: nil, response, error)
    }
    
    public func purchaseListing(accessToken: String,data:SubOfferHistory,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(id)/actions/quick-buy"
        do {
           try? apiCallPOST(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func addComment(accessToken: String,id:Int,data:Comments,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(id)/comment"
        do {
           try? apiCallPOST(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func getIssues(accessToken: String ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/reported-issues/issue-types"
        apiCallGET(api: api, accessToken: accessToken, dict: nil, response, error)
    }
    
    public func reportAnIssue(accessToken: String,data:Dispute,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/reported-issues/report"
        try? apiCallPOST(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
    }
    
    public func getPromotions(accessToken: String ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/promotions?imageType=2"
        apiCallGET(api: api, accessToken: accessToken, dict: nil, response, error)
    }
    
    public func getPromotionsBuyerEnd(accessToken: String ,listingId:Int,data:OffersCheck,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(listingId)/actions/pre-offer/buyer"
        try? apiCallPUT(api: api, accessToken: accessToken, dict: JSONEncoder().encode(data), response, error)
    }
    
    public func getPromotionsSellerEnd(accessToken: String ,listingId:Int,data:OffersCheck,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(listingId)/actions/pre-counter-offer/seller"
        try? apiCallPUT(api: api, accessToken: accessToken, dict: JSONEncoder().encode(data), response, error)
    }
    
    public func getUsersList(accessToken: String,search:String,page:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        var api = ""
        if search.trimmingCharacters(in: .whitespacesAndNewlines) == ""{
           api = "api/users/username/search?sortDirection=asc&page=\(page)"
        }else{
            let encoded = search.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
            api = "api/users/username/search?sortDirection=asc&search=\(encoded!)&page=\(page)"
        }
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getListingComments(accessToken: String ,listingId:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(listingId)/comments"
        apiCallGET(api: api, accessToken: accessToken, dict: nil, response, error)
    }
    
    public func getShippingRates(accessToken: String ,listingId:Int,data:ShipRates,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/shipping/parcels/listing/\(listingId)/shipping-rate"
        try? apiCallPUT(api: api, accessToken: accessToken, dict: JSONEncoder().encode(data), response, error)
    }

}
