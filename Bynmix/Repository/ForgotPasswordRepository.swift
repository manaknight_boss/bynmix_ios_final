import Foundation
import Alamofire

class ForgotPasswordRepository : BaseRepository {
    
    public func sendCode(accessToken: String,data: ForgotPasswordRequest, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/accounts/password/send-code"
        do {
           try? apiCallPOST(api: api, accessToken: accessToken, dict: JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func verifyCode(accessToken: String,data: VerifyCodeRequest, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/accounts/password/verify-code"
        do {
           try? apiCallPUT(api: api, accessToken: accessToken, dict: JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func resetPassword(accessToken: String,data: ResetPasswordRequest, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/accounts/password/reset"
        do {
           try? apiCallPUT(api: api, accessToken: accessToken, dict: JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
}
