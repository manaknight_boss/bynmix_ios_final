import Foundation
import Alamofire

class OnBoardingRepository : BaseRepository {
    
    public func getBrands(accessToken: String,page:Int,searchQuery: String? = nil, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        var api = ""
        if searchQuery != nil && searchQuery != "" {
            let encoded = searchQuery?.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
            api = "api/onboarding/brands?search=" + encoded! + "&page=\(page)&pageSize=26"
        }else{
            api = "api/onboarding/brands?page=\(page)&pageSize=26"
        }
        apiCallGET(api: api, accessToken: accessToken, dict: nil, response, error)
    }
    
    public func getTags(accessToken: String,page:Int,searchQuery: String? = nil, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        var api = ""
        if searchQuery != nil && searchQuery != "" {
            let encoded = searchQuery?.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
            api = "api/onboarding/descriptors?search=" + encoded! + "&page=\(page)&pageSize=28"
        }else{
            api = "api/onboarding/descriptors?page=\(page)&pageSize=28"
        }
        apiCallGET(api: api, accessToken: accessToken, dict: nil, response, error)
    }
    
    public func getBloggers(accessToken: String,page:Int,searchQuery: String? = nil, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        var api = ""
        if searchQuery != nil && searchQuery != "" {
            let encoded = searchQuery?.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
            api = "api/onboarding/bloggers?search=" + encoded! + "&page=\(page)&pageSize=28"
        }else{
            api = "api/onboarding/bloggers?page=\(page)&pageSize=28"
        }
        apiCallGET(api: api, accessToken: accessToken, dict: nil, response, error)
    }
    
    public func saveBrands(accessToken: String,data: BrandRequest, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        do {
           try? apiCallPOST(api: "api/onboarding/brands", accessToken: accessToken, dict: JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func saveTags(accessToken: String,data: TagRequest, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        do {
           try? apiCallPOST(api: "api/onboarding/descriptors", accessToken: accessToken, dict: JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    
    public func saveBloggers(accessToken: String,data: BloggerRequest, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        do {
           try? apiCallPOST(api: "api/onboarding/bloggers", accessToken: accessToken, dict: JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func introduction(accessToken: String, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        apiCallGET(api: "api/onboarding/introduction", accessToken: accessToken, dict: nil, response, error)
    }
   
}
