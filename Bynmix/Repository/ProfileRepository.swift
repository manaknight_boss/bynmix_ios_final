import Foundation
import Alamofire

class ProfileRepository : BaseRepository {
    
    public func getUser(accessToken: String,userId: Int, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        var api = "/api/users"
        if userId != 0 {
            api = api + "/" + "\(userId)"
        }
        apiCallGET(api: api, accessToken: accessToken, dict: nil, response, error)
    }
    
    public func updateProfileInfo(accessToken: String,data: User, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        do {
            try? apiCallPUT(api: "api/users", accessToken: accessToken, dict: JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func getfollowers(accessToken: String, page:Int , _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        apiCallGET(api: "api/users/follow/followers?page=" + "\(page)", accessToken: accessToken, dict: nil, response, error)
    }
    
    public func getfollowing(accessToken: String, page: Int, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        apiCallGET(api: "api/users/follow/followings?page=" + "\(page)", accessToken: accessToken, dict: nil, response, error)
    }
    
    public func getOtherUserFollowers(accessToken: String, userId:Int, page:Int , _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        apiCallGET(api: "api/users/follow/" + "\(userId)" + "/followers?page=" + "\(page)", accessToken: accessToken, dict: nil, response, error)
    }
    
    public func getOtherUserFollowing(accessToken: String, userId:Int ,page: Int, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        apiCallGET(api: "api/users/follow/" + "\(userId)" + "/followings?page=" + "\(page)", accessToken: accessToken, dict: nil, response, error)
    }
    
    public func updateFollowStatus(accessToken: String,userId: Int, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        apiCallPUT(api: "api/users/follow/" + "\(userId)", accessToken: accessToken, dict: nil, response, error)
    }
    
    public func updateUserPhoto(accessToken: String,id:Int, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        
        var api = "api/users"
        
        if id != 0 {
            api = api + "/" + "\(id)" + "/images"
        }
        apiCallPUT(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func updateUserCoverPhoto(accessToken: String,id:Int, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        
        var api = "api/users"
        
        if id != 0 {
            api = api + "/" + "\(id)" + "/images/background"
        }
        apiCallPUT(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func uploadCoverImage(accessToken: String,userId: Int, image: UIImage, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let url = "/api/users/" + "\(userId)" + "/images/background"
        uploadImagePUT(api: url, accessToken: accessToken, image: image, response, error)
    }
    
    public func uploadProfileImage(accessToken: String,userId: Int, image: UIImage, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let url = "/api/users/" + "\(userId)" + "/images"
        uploadImagePUT(api: url, accessToken: accessToken, image: image, response, error)
    }
    
    public func getLikesListing(accessToken: String, sort: String ,page:Int ,data:FilterData?,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/users/listing-likes?page=" + "\(page)" + "&sortDirection=DESC"
        try? apiCallPUT(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
    }
    
    public func getLikesPost(accessToken: String ,page:Int ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/users/post-likes?page=" + "\(page)" + "&sortDirection=DESC"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getNotifications(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/settings/user-notifications"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func updateNotifications(accessToken: String,data: NotificationSettingRequest ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/settings/user-notifications"
        do {
            try? apiCallPUT(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func getOtherUserByn(accessToken: String,page:Int,id:Int, data:FilterData?,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        
        var api = "api/users/"
        
        if id != 0 {
            api = api + "\(id)" + "/listings?page=" + "\(page)" + "&sortDirection=DESC&pageSize=26"
        }
        try? apiCallPUT(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
    }
    
    public func getOtherUserPost(accessToken: String,page:Int,id:Int, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        
        var api = "api/users/"
        
        if id != 0 {
            api = api + "\(id)" + "/posts?page=" + "\(page)" + "&sortDirection=DESC"
        }
        apiCallPUT(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getUserPost(accessToken: String,page:Int, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        
        let api = "api/users/posts?page=\(page)&sortDirection=DESC&pageSize=26"
        
        apiCallPUT(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getUserByn(accessToken: String,page:Int,data:FilterData? ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        
        let api = "api/users/listings?page=\(page)&sortDirection=DESC&pageSize=26"
        
        try? apiCallPUT(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
    }
    
    public func enableDisableByn(accessToken: String,id:Int,page:Int ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        
        let api = "api/listings/\(id)/active?page=\(page)"
        
        apiCallPUT(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    public func deleteByn(accessToken: String,id:Int ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        
        let api = "api/listings/\(id)"
        
        apiCallDELETE(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func enableDisablePosts(accessToken: String,id:Int,page:Int ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        
        let api = "api/posts/\(id)/active?page=\(page)"
        
        apiCallPUT(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    public func deletePosts(accessToken: String,id:Int ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        
        let api = "api/posts/\(id)"
        
        apiCallDELETE(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getAddresses(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/settings/address"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func addAddresses(accessToken: String,data:Address,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/settings/address"
        do {
            try? apiCallPOST(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func updateAddresses(accessToken: String,id:Int,data:Address,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/settings/address/\(id)"
        do {
            try? apiCallPUT(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func deleteAddresses(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/settings/address/\(id)"
        apiCallDELETE(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getMyWalletSales(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/users/totals/sales"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getMyWalletCard(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/transactions/card?imageType=2"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getBalanceHistory(accessToken: String,page:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/transactions/payout/history?page=\(page)"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func addPayment(accessToken: String,data:Card,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/transactions/card"
        do {
            try? apiCallPOST(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func getCardInfo(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/transactions/card/\(id)"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func editPayment(accessToken: String,id:Int,data:Card,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/transactions/card/\(id)"
        do {
            try? apiCallPUT(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func getAccountInfo(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/transactions/payout/bank-accounts"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func withdrawMoneyFromAccount(accessToken: String,id:Int,data:AccountInfo,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/transactions/payout/\(id)"
        do {
            try? apiCallPOST(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func uploadBankDetails(accessToken: String,data:AccountInfo,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/transactions/payout"
        do {
            try? apiCallPOST(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func checkIfOffersAccepted(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/transactions/card/\(id)/check"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func deleteCard(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/transactions/card/\(id)"
        apiCallDELETE(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func replaceCard(accessToken: String,oldCardId:Int,newCardId:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/transactions/card/\(oldCardId)/replace/\(newCardId)"
        apiCallDELETE(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getSales(accessToken: String,page:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/sales?page=\(page)&sortDirection=DESC&pageSize=26"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getPurchase(accessToken: String,page:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/purchases?page=\(page)&sortDirection=DESC&pageSize=26"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getPurchaseStats(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/users/totals/purchases"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getSaleStats(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/users/totals/sales"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getSalesDetail(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/sales/\(id)"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getPurchasesDetail(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/purchases/\(id)?imageType=2"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func relistItem(accessToken: String,listingId:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(listingId)/relist"
        apiCallPOST(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func updateSalePurchaseLikeStatus(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/users/follow/\(id)"
        apiCallPUT(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func buyerFeedback(accessToken: String,id:Int,data:PurchaseDetail,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/purchases/\(id)/buyer-feedback"
        do {
            try? apiCallPOST(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func sellerFeedback(accessToken: String,id:Int,data:SalesDetail,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/sales/\(id)/seller-feedback"
        try? apiCallPOST(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
    }
    
    public func getTrackInformation(accessToken: String,id:Int,canReturnInfoForReturnLabel:Bool,type:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        var api = ""
        if type == AppConstants.TYPE_PURCHASE{
            if canReturnInfoForReturnLabel{
                api = "api/purchases/\(id)/tracking/return"
            }else{
                api = "api/purchases/\(id)/tracking"
            }
        }else{
            if canReturnInfoForReturnLabel{
                api = "api/sales/\(id)/tracking/return"
            }else{
                api = "api/sales/\(id)/tracking"
            }
        }
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func emailShippingLabel(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/sales/\(id)/shipping-label"
        apiCallPOST(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func emailShippingLabelFreeShipping(accessToken: String,id:Int,data: ShipRates,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/sales/\(id)/shipping-label"
        try? apiCallPOST(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
    }
    
    public func shippingRate(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(id)/shipping-rate"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getListingImages(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(id)/images"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getListingStatuses(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/filters/statuses"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getListingDetail(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(id)"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func updateEditListing(accessToken: String,id:Int,data:ListingDetail,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(id)"
        do {
            try? apiCallPUT(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
        
    }
    
    public func updateEditListingImages(accessToken: String,id:Int,images: [UIImage],_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(id)/images"
        uploadImagesPOST(api: api, accessToken:accessToken, images: images, response, error)
    }
    
    public func deleteEditListingImages(accessToken: String,id:Int,data:DeleteImagesRequest,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/listings/\(id)/images"
        do {
            try? apiCallDELETE(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
            throw MyError.runtimeError("\(String(describing: error))")
        } catch {
            
        }
    }
    
    public func getStyles(accessToken: String,searchQuery: String? = nil,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        var api = ""
        if searchQuery != "" &&  searchQuery != nil{
            let encoded = searchQuery?.addingPercentEncoding(withAllowedCharacters: .alphanumerics)
            api = "api/users/styles?search=" + encoded!
        }else{
            api = "api/users/styles"
        }
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getReportIssueType(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/reported-issues/issue-types"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func checkReportAnIssue(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/disputes/\(id)/check"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getSaleCancelReason(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/sales/cancel-reasons"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getDisputes(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/disputes/dispute-types"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func uploadReportAnIssueImages(accessToken: String,id:Int,images:[UIImage],_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/disputes/\(id)/images"
        uploadImagesPOST(api: api, accessToken:accessToken, images: images, response, error)
    }
    
    public func reportAnIssue(accessToken: String,data:Dispute,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/disputes"
        try? apiCallPOST(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
    }
    public func cancelOrderSale(accessToken: String,id:Int,data:SaleCancelReason,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/sales/\(id)/cancel"
        try? apiCallPOST(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
    }
    
    public func cancelPurchase(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/purchases/\(id)/cancel"
        apiCallPOST(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getDisputeDetails(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/disputes/\(id)/details"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func cancelDispute(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/disputes/\(id)/cancel"
        apiCallPUT(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func fetchBuyerReviews(accessToken: String,id:Int,page:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/users/feedback/\(id)/as-buyer?page=\(page)&sortDirection=DESC"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func fetchSellerReviews(accessToken: String,id:Int,page:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/users/feedback/\(id)/as-seller?page=\(page)&sortDirection=DESC"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func addDisputeCommment(accessToken: String,id:Int,data:Comments,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/disputes/\(id)/comment"
        try? apiCallPOST(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
    }
    
    public func uploadCommentImages(accessToken: String,disputeId:Int,commentId:Int,images:[UIImage],_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/disputes/\(disputeId)/comment/\(commentId)/images"
        uploadImagesPOST(api: api, accessToken:accessToken, images: images, response, error)
    }
    
    public func escalateDispute(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/disputes/\(id)/resolve/escalate"
        apiCallPOST(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func refundBuyerWithoutReturn(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/disputes/\(id)/resolve/refund-no-return"
        apiCallPOST(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func returnLabel(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/purchases/\(id)/return-label"
        apiCallPOST(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func confirmDeliveryStatus(accessToken: String,saleId: Int, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/sales/\(saleId)/return/confirm-delivery"
        apiCallPUT(api: api, accessToken: accessToken, dict: nil, response, error)
    }
    
    public func trackSearch(accessToken: String,data:TrackSearchOfUser,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/analytics/track/search/search-bar"
        try? apiCallPOST(api: api, accessToken: accessToken, dict:JSONEncoder().encode(data), response, error)
    }
    
    public func logoutUser(accessToken: String, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/accounts/logout"
        apiCallPUT(api: api, accessToken: accessToken, dict: nil, response, error)
    }
    
    public func uploadVerifyAccount(accessToken: String,images:[UIImage],_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/transactions/account/file/upload"
        uploadImagesPOST(api: api, accessToken:accessToken, images: images, response, error)
    }
    
    public func updateUserInfo(accessToken: String,data:AccountVerify, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/transactions/account"
        try? apiCallPUT(api: api, accessToken: accessToken, dict: JSONEncoder().encode(data), response, error)
    }
    
    public func checkIfAccountVerified(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/transactions/account/verify"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getDocumentType(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/transactions/account/document-types"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func uploadImageWithDocument(accessToken: String,image:UIImage,data:AccountVerify,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/transactions/account/file/upload"
        uploadImagesWithData(api: api, accessToken: accessToken, image: image, data: data, response, error)
    }
    
    public func getSelectedBrands(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/users/info/brands"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func getSelectedDescriptors(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        let api = "api/users/info/descriptors"
        apiCallGET(api: api, accessToken: accessToken, dict:nil, response, error)
    }
    
    public func saveEditedBrands(accessToken: String,data: User, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        try? apiCallPUT(api: "api/users", accessToken: accessToken, dict: JSONEncoder().encode(data), response, error)
    }
    
    public func saveEditedTags(accessToken: String,data: User, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        try? apiCallPUT(api: "api/users", accessToken: accessToken, dict: JSONEncoder().encode(data), response, error)
    }

}
