import UIKit
import XLPagerTabStrip

class AlertsViewController: ButtonBarPagerTabStripViewController {
    
    @IBOutlet var promotionTableView: UITableView!
    @IBOutlet var promotionHeightTableView: NSLayoutConstraint!
    
    var promotionsHandler = PromotionsHandler()
    
    override func viewDidLoad() {
        loadDesign()
        super.viewDidLoad()
        promotionTableView.delegate = promotionsHandler
        promotionTableView.dataSource = promotionsHandler
        getPromotionsClick()
        promotionTableView.rowHeight = UITableView.automaticDimension
        promotionTableView.estimatedRowHeight = 60
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        if AppConstants.TYPE_IS_ADDED_NEW_OFFER{
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.moveToViewController(at: 1, animated: true)
            }
            self.offerCompletionDialog(username: AppConstants.SELLER_NAME)
        }
        if AppConstants.isPromotionsAllListLoaded{
            getPromotions()
        }
    }
    
    func getPromotionsClick(){
        promotionsHandler.onCancelButtonClick = { promotions,index in
            self.promotionsHandler.promotions.remove(at: index)
            self.promotionTableView.reloadData()
            let promotionId = promotions.promotionId!
            
            AppDefaults.setPromotionId(promotionId: promotionId)
            print(AppDefaults.getPromotionIds())
            self.resizePromotionsTableView(promotions: self.promotionsHandler.promotions)
        }
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let tabAllAlertsViewController = UIStoryboard(name: AppConstants.Storyboard.ALERTS_ALL_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.ALERTS_ALL_SCREEN) as! AlertsAllViewController
        
        let tabOffersAlertsViewController = UIStoryboard(name: AppConstants.Storyboard.ALERTS_OFFERS_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.ALERTS_OFFERS_SCREEN) as! AlertsOffersViewController
        
        return [tabAllAlertsViewController,tabOffersAlertsViewController]
    }
    
    func loadDesign() {
        self.settings.style.buttonBarBackgroundColor = UIColor.white
        self.settings.style.buttonBarItemBackgroundColor = UIColor.white
        self.settings.style.selectedBarBackgroundColor = UIColor.cFB9D52
        self.settings.style.buttonBarItemFont = UIFont.fontSemiBold(size: 12)
        self.settings.style.selectedBarHeight = 2.0
        self.settings.style.buttonBarMinimumLineSpacing = 0
        self.settings.style.buttonBarItemTitleColor = UIColor.black
        self.settings.style.buttonBarItemsShouldFillAvailableWidth = true
        self.settings.style.buttonBarLeftContentInset = 10
        self.settings.style.buttonBarRightContentInset = 10
        
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            
            oldCell?.label.textColor = UIColor.c2A2A2A
            oldCell?.label.font = UIFont.fontSemiBold(size: 12)
            newCell?.label.textColor = UIColor.cBB189C
            newCell?.label.font = UIFont.fontSemiBold(size: 12)
            
        }
    }
    
    func offerCompletionDialog(username:String){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.LISTING_BUYED_DIALOG_STORYBOARD, bundle: nil)
        let offerComleteViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.LISTING_BUYED_DIALOG) as! ListingBuyedDialogViewController
        offerComleteViewController.modalPresentationStyle = .overFullScreen
        offerComleteViewController.username = username
        offerComleteViewController.TYPE = AppConstants.TYPE_BUY_NOW
        self.navigationController?.present(offerComleteViewController, animated: false, completion: nil)
    }
    
    func getPromotions() {
        let promotions = AppConstants.PROMOTIONS_ALL_LIST
        var promotionsNew : [Promotions] = []
        if AppDefaults.getPromotionIds().count > 0{
            print("promotions:",AppDefaults.getPromotionIds())
            for i in 0..<promotions.count{
                if  !AppDefaults.getPromotionIds().contains(promotions[i].promotionId!){
                    promotionsNew.append(promotions[i])
                }
            }
            self.promotionsHandler.promotions = promotionsNew
        }else{
            self.promotionsHandler.promotions = promotions
        }
        self.promotionTableView.reloadData()
        self.promotionTableView.isHidden = false
        self.resizePromotionsTableView(promotions: promotions)
    }

    func resizePromotionsTableView(promotions:[Promotions]){
        if promotions.count == 0{
            promotionHeightTableView.constant = 0
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                var tableViewHeight: CGFloat {
                    self.promotionTableView.layoutIfNeeded()
                    return self.promotionTableView.contentSize.height
                }
                self.promotionHeightTableView?.constant = tableViewHeight
            }
        }
    }
    
}
