import UIKit
import XLPagerTabStrip
import Alamofire

class AlertsAllViewController: BaseViewController,LoadMoreProtocol {
    func onLoadMore() {
        if !isApiCalled {
            isApiCalled = true
            self.getAllFeed()
        }
    }
    
    @IBOutlet var alertsTableView: UITableView!
    @IBOutlet var screenActivityIndicator:UIActivityIndicatorView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var emptyStateView: UIView!
    
    let alertsViewModel = AlertsViewModel()
    var currentPage = 1
    var isApiCalled = false
    var refreshControl : UIRefreshControl?
    var alertsAllHandler = AlertsAllHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        alertsTableView.delegate = alertsAllHandler
        alertsTableView.dataSource = alertsAllHandler
        alertsAllHandler.loadMoreDelegate = self
        alertsTableView.contentInset = UIEdgeInsets(top: 50, left: 0, bottom: 0, right: 0)
        addRefreshControl()
        getClick()
        UIView.animate(withDuration: 0.2) {
            self.alertsAllTableView = self.alertsTableView
            self.view.layoutIfNeeded()
        }
        //getAllFeed()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        shadowView.layer.cornerRadius = 2
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.alpha = CGFloat(0.26)
        shadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        shadowView.layer.shadowOpacity = 1
        shadowView.layer.zPosition = -1
        if (self.alertsAllHandler.alerts.count) <= 0{
            self.getAllFeed()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if (self.alertsAllHandler.alerts.count) > 0{
            for i in 0..<self.alertsAllHandler.alerts.count{
                if self.alertsAllHandler.alerts[i].isUnread!{
                    self.alertsAllHandler.alerts[i].isUnread! = !self.alertsAllHandler.alerts[i].isUnread!
                }
            }
            alertsTableView.reloadData()
        }
    }
    
    func getClick(){
//        alertsAllHandler.onUserImageClickHandler = { alerts in
//
//            if alerts.notificationTypeId == AppConstants.POST_LIKED{
//                if alerts.postLink != ""{
//
//                    let storyboard = UIStoryboard(name: AppConstants.Storyboard.POST_DETAIL_BOARD, bundle: nil)
//                    let postDetailViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.POST_DETAIL_SCREEN) as! PostDetailViewController
//                    postDetailViewController.id = alerts.postId
//                    self.navigationController?.pushViewController(postDetailViewController, animated: true)
//
//                }
//
//            }else{
//                if (alerts.notificationTypeId == AppConstants.COMMENT_RECEIVED || alerts.notificationTypeId == AppConstants.USER_FOLLOWING){
//
//                    if alerts.initiatorUserId == AppDefaults.getUserId(){
//                        self.openUserProfile()
//                    }else{
//                        self.openProfile(id:alerts.initiatorUserId!,username:alerts.username!)
//                    }
//
//                }else{
//                    let storyboard = UIStoryboard(name: AppConstants.Storyboard.LISTING_DETAIL_BOARD, bundle: nil)
//
//                    let listingDetailViewController = storyboard.instantiateViewController(withIdentifier:AppConstants.Controllers.Listing_Detail_Screen) as! ListDetailViewController
//                    listingDetailViewController.itemId = alerts.listingId
//                    self.navigationController?.pushViewController(listingDetailViewController, animated: true)
//                }
//            }
//
//        }
        
        //alertsAllHandler.onUserImagePromotionClickHandler = {
//            self.showMessageDialog(message: AppConstants.Strings.LISTING_IS_REQD)
        //}
        
//        alertsAllHandler.onFormattedTextClicked = { setting in
//            if setting.initiatorUserId == AppDefaults.getUserId(){
//                self.openUserProfile()
//            }else{
//                self.openProfile(id:setting.initiatorUserId!,username:setting.username!)
//            }
//        }
        
        alertsAllHandler.onNotificationClick = { setting in
            let notificationRedirectTypeId = setting.notificationRedirectTypeId ?? 0
            let notificationRedirectType = setting.notificationRedirectType ?? ""
            let notificationRedirectId = setting.notificationRedirectId ?? 0
            let idType = setting.idType ?? ""
            let initiatorUserId = setting.initiatorUserId ?? 0
            
            self.notificationRedirect(notificationRedirectTypeId: notificationRedirectTypeId, notificationRedirectType: notificationRedirectType, notificationRedirectId: notificationRedirectId, idType: idType, initiatorUserId: initiatorUserId)
        }
        
    }
    
    func getAllFeed() {
        alertsViewModel.getAlertsAll(accessToken: getAccessToken(),page:currentPage, { (response) in
            self.handleResponse(response: response)
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            self.refreshControl?.endRefreshing()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func handleResponse(response: AFDataResponse<Any>) {
        self.isApiCalled = false
        self.refreshControl?.endRefreshing()
        if response.response?.statusCode == 200 {
            let data: ApiResponse<Setting> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            } else {
                
                let userdata = data.data
                if (userdata?.items?.count)! > 0{
                    
                    if let users = userdata?.items {
                        if self.currentPage == 1 {
                            self.alertsAllHandler.alerts = users
                        } else {
                            self.alertsAllHandler.alerts.append(contentsOf: users)
                        }
                        if userdata?.totalPages == currentPage {
                            self.isApiCalled = true
                            self.alertsAllHandler.isLoadMoreRequired = false
                        }
                        DispatchQueue.main.async {
                            self.alertsTableView.reloadData()
                            self.screenActivityIndicator.stopAnimating()
                            self.alertsTableView.isHidden = false
                        }
                        var ids : [Int] = []
                        for i in 0..<users.count{
                            if users[i].isUnread!{
                                ids.append(users[i].notificationId!)
                            }
                        }
                        if ids.count > 0{
                            self.decreaseCount(ids: ids)
                        }
                    }
                }else{
                    self.alertsAllHandler.alerts.removeAll()
                    self.alertsTableView.reloadData()
                    self.emptyStateView.isHidden = false
                    self.shadowView.isHidden = false
                    self.screenActivityIndicator.stopAnimating()
                }
                self.currentPage = self.currentPage + 1
            }
        
        } else {
            self.screenActivityIndicator.stopAnimating()
        }
    }
    
    func decreaseCount(ids:[Int]) {
        let setting = Setting(notificationIds: ids)
        alertsViewModel.updateAlertsReadNotifications(accessToken: getAccessToken(), data:setting, { (response) in
            if response.response?.statusCode == 200 || response.response?.statusCode == 201{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    self.alertsTableView.reloadData()
                }
            }else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print(error)
                }
            }
            
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            self.refreshControl?.endRefreshing()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    
    func addRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        alertsTableView.addSubview(refreshControl!)
    }
    
    @objc func refreshList(){
        currentPage = 1
        isApiCalled = false
        self.alertsAllHandler.isLoadMoreRequired = true
        getAllFeed()
    }
    
}

extension AlertsAllViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: AppConstants.ALL)
    }
    
}
