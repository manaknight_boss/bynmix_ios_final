import UIKit
import XLPagerTabStrip

class AlertsOffersViewController: ButtonBarPagerTabStripViewController {
    
    @IBOutlet weak var myOffersButton: UIButton!
    @IBOutlet weak var receivedOffersButton: UIButton!
    @IBOutlet weak var screenActivityIndicator: UIActivityIndicatorView!
    
    var TYPE:Int = 0
    
    override func viewDidLoad() {
        loadDesign()
        super.viewDidLoad()
        setSelectedMyOffersButtonUI()
        setReceivedOffersButtonUI()
    }
    
    func loadDesign() {
        self.settings.style.buttonBarBackgroundColor = UIColor.cE8E8E8
        self.settings.style.buttonBarItemBackgroundColor = UIColor.cE8E8E8
        self.settings.style.selectedBarBackgroundColor = UIColor.cE8E8E8
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if AppConstants.TYPE_IS_ADDED_NEW_OFFER{
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                self.moveToViewController(at: 0, animated: false)
                self.setSelectedMyOffersButtonUI()
                self.setReceivedOffersButtonUI()
                AppConstants.TYPE_IS_ADDED_NEW_OFFER = false
            }
        }
    }
    
    @IBAction func myOffersButtonAction(_ sender: UIButton) {
        setSelectedMyOffersButtonUI()
        setReceivedOffersButtonUI()
        self.moveToViewController(at: 0, animated: false)
    }
    
    func setSelectedMyOffersButtonUI(){
        myOffersButton.setTitleColor(UIColor.white, for: .normal)
        myOffersButton.backgroundColor = UIColor.c3C3C3C
    }
    
    func setMyOffersButtonUI(){
        myOffersButton.setTitleColor(UIColor.c3C3C3C, for: .normal)
        myOffersButton.backgroundColor = UIColor.white
    }
    
    func setSelectedReceivedOffersButtonUI(){
        receivedOffersButton.setTitleColor(UIColor.white, for: .normal)
        receivedOffersButton.backgroundColor = UIColor.c3C3C3C
    }
    
    func setReceivedOffersButtonUI(){
        receivedOffersButton.setTitleColor(UIColor.c3C3C3C, for: .normal)
        receivedOffersButton.backgroundColor = UIColor.white
    }
    
    @IBAction func receivedOffersButtonAction(_ sender: UIButton) {
        setSelectedReceivedOffersButtonUI()
        setMyOffersButtonUI()
        self.moveToViewController(at: 1, animated: false)
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let tabMyOffersViewController = UIStoryboard(name: AppConstants.Storyboard.OFFERS_STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.OFFERS_SCREEN) as! OffersViewController
        tabMyOffersViewController.TYPE = AppConstants.TYPE_MY_OFFERS
        
        let tabReceivedOffersViewController = UIStoryboard(name: AppConstants.Storyboard.OFFERS_STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.OFFERS_SCREEN) as! OffersViewController
        tabReceivedOffersViewController.TYPE = AppConstants.TYPE_RECEIVED_OFFERS
        
        return [tabMyOffersViewController,tabReceivedOffersViewController]
    }
    
    override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool) {
        super.updateIndicator(for: viewController, fromIndex: fromIndex, toIndex: toIndex, withProgressPercentage: progressPercentage, indexWasChanged: indexWasChanged)
        
        if indexWasChanged {
            if progressPercentage != 1.0 {
                if currentIndex == 0{
                    setSelectedMyOffersButtonUI()
                    setReceivedOffersButtonUI()
                }else if currentIndex == 1 {
                    setSelectedReceivedOffersButtonUI()
                    setMyOffersButtonUI()
                }
            }
        }
    }
    
}

extension AlertsOffersViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: AppConstants.OFFERS)
    }
}
