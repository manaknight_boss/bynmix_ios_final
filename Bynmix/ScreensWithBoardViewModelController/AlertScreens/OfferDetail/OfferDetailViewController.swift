import UIKit
import Alamofire

class OfferDetailViewController: BaseViewController,OnCancelButtonClickProtocol,MakeAnOfferProtocol {
    
    func makeAnOffer(listingDetail: ListingDetail, type: Int,offeredPrice:Int) {
        self.dismiss(animated: false, completion: nil)
        self.buyNowListingScreen(listingDetail: listingDetail, type: type, offeredPrice: offeredPrice)
        
    }
    
    func onCancelButtonClick(subOfferHistory: SubOfferHistory,index:Int) {
        let listingId = subOfferHistory.listingId!
        let offerId = subOfferHistory.offerId!
        self.cancelOffer(listingId: listingId,offerId: offerId,index:index)
    }
    
    @IBOutlet weak var itemShadowView: UIView!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var buyNowLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var currentPriceTextLabel: UILabel!
    @IBOutlet weak var currentPriceLabel: UILabel!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var makeOfferButton: UIButton!
    @IBOutlet weak var offerHistoryTextLabel: UILabel!
    @IBOutlet weak var noOfferHistoryView: UIView!
    @IBOutlet weak var noOfferHistoryCurrentPriceLabel: UILabel!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var headerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var tableViewFooter: UIView!
    
    var TYPE:Int?
    var setting:Setting?
    var subOffersHistory:SubOfferHistory?
    
    let alertsViewModel = AlertsViewModel()
    let feedViewModel = FeedViewModel()
    
    let offerDetailHandler = OfferDetailHandler()
    var listingId:Int?
    var listingTitle:String?
    var listingDetail:ListingDetail?
    var subOfferHistory:[SubOfferHistory]?
    var offerHistory:OfferHistory?
    var isBuyer:Bool? = false
    var initiatorUserId:Int? = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = offerDetailHandler
        tableView.dataSource = offerDetailHandler
        
        offerDetailHandler.onCancelButtonClick = { subOfferHistory,index in
            self.cancelOfferDialog(subOfferHistory: subOfferHistory,index: index)
        }
        
        offerDetailHandler.onLeftViewClick = { subOfferHistory in
            if subOfferHistory.sellerId == AppDefaults.getUserId(){
                self.openUserProfile()
            }else{
                self.openProfile(id:subOfferHistory.sellerId!,username:subOfferHistory.sellerUsername!)
            }
        }
        
        offerDetailHandler.onRightViewClick = { subOfferHistory in
            if subOfferHistory.buyerId == AppDefaults.getUserId(){
                self.openUserProfile()
            }else{
                self.openProfile(id:subOfferHistory.buyerId!,username:subOfferHistory.buyerUsername!)
            }
        }
        
        onTypeChange()
        setImage()
        let backBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.BACK_BUTTON), style: .plain, target: self, action: #selector(backBttonClick))
        self.navigationItem.leftBarButtonItem  = backBarButton
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 75
    }
    
    @objc func backBttonClick(){
        self.navigationController?.popViewController(animated: false)
    }
    
    func cancelOfferDialog(subOfferHistory:SubOfferHistory,index:Int){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.LISTING_LOST_DIALOG_BOARD, bundle: nil)
        let cancelOfferViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.LISTING_LOST_DIALOG) as! ListingLostDialogViewController
        cancelOfferViewController.TYPE = AppConstants.TYPE_CANCEL_OFFER
        cancelOfferViewController.subOfferHistory = subOfferHistory
        cancelOfferViewController.index = index
        cancelOfferViewController.onCancelButtonClickDelegate = self
        self.navigationController!.present(cancelOfferViewController, animated: false, completion: nil)
    }
    
    func onTypeChange(){
        if TYPE == AppConstants.TYPE_MY_OFFERS{
            self.noOfferHistoryView.isHidden = true
            self.offerHistoryTextLabel.isHidden = false
            self.currentPriceTextLabel.isHidden = false
            self.currentPriceLabel.isHidden = false
            navigationItem.title = AppConstants.Strings.OFFER_TITLE
            let listingId = setting?.listingId!
            self.getListing(id: listingId!)
            self.headerViewHeight.constant = 149.5
        }else if TYPE == AppConstants.TYPE_RECEIVED_OFFERS{
            self.noOfferHistoryView.isHidden = true
            self.offerHistoryTextLabel.isHidden = false
            self.currentPriceTextLabel.isHidden = false
            self.currentPriceLabel.isHidden = false
            navigationItem.title = AppConstants.Strings.OFFER_TITLE
            let listingId = setting?.listingId!
            self.getListing(id: listingId!)
            self.headerViewHeight.constant = 149.5
        }else if TYPE == AppConstants.TYPE_MAKE_A_NEW_OFFER{
            navigationItem.title = AppConstants.Strings.OFFER_TITLE
            let listingId = self.listingId!
            self.getListing(id: listingId)
        }else if TYPE == AppConstants.TYPE_COUNTER_OFFER{
            navigationItem.title = AppConstants.Strings.OFFER_TITLE
            var listingId = setting?.listingId!
            if listingId == nil{
                listingId = self.subOffersHistory?.listingId!
            }
            self.getListing(id: listingId!)
        }else{
            navigationItem.title = AppConstants.Strings.OFFER_TITLE
            let listingId = self.listingId!
            self.getListing(id: listingId)
        }
        
    }
    
    func setData(offerHistory:OfferHistory){
        self.offerHistory = offerHistory
        let listingTitle = "\"" + offerHistory.listingTitle! + "\""
        setText(text: listingTitle, listingTitle: offerHistory.listingTitle!)
        if self.TYPE == AppConstants.TYPE_MY_OFFERS{
            //self.buyNowLabel.text = AppConstants.Strings.MAKE_AN_OFFER_FOR
        }else if TYPE == AppConstants.TYPE_MAKE_A_NEW_OFFER{
            //self.buyNowLabel.text = AppConstants.Strings.MAKE_AN_OFFER_FOR
        }else if TYPE == AppConstants.TYPE_COUNTER_OFFER{
            //self.buyNowLabel.text = AppConstants.Strings.COUNTER_OFFER_FOR
            self.noOfferHistoryView.isHidden = true
            self.headerViewHeight.constant = 149.5
        }else{
            //self.buyNowLabel.text = AppConstants.Strings.COUNTER_OFFER_FOR
        }
        
        let image = UIImage(named:AppConstants.Images.DEFAULT_OFFER_PLACEHOLDER)
        
        if let imageUrl = offerHistory.listingImage{
            self.itemImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        } else {
            self.itemImageView.image =  image
        }
        
        var listingId = self.setting?.listingId!
        if listingId == nil{
            listingId = offerHistory.listingId!
        }
        
        if (self.listingDetail?.id == AppDefaults.getUserId()) {
            isBuyer = false
        } else {
            isBuyer = true
        }
        
        if (self.subOfferHistory?.count ?? 0) > 0{
            let lastOffer = self.subOfferHistory?.last
            
            if lastOffer?.isActiveOffer ?? false{
                if isBuyer ?? false{
                    if(lastOffer?.isBuyerOffer ?? false){
                        self.makeANewOffer()
                    }else {
                        self.counterOffer()
                    }
                }else{
                    if(lastOffer?.isSellerCounterOffer ?? false){
                        self.makeANewOffer()
                    }else {
                        self.counterOffer()
                    }
                }
            }else{
                self.makeANewOffer()
            }
        }else{
            self.buyNowLabel.text = AppConstants.Strings.MAKE_AN_OFFER_FOR
            self.makeOfferButton.setTitle(AppConstants.Strings.MAKE_AN_OFFER, for: .normal)
        }
        
        getListingDetailImagesInOffer(id: listingId!, offerHistory: offerHistory)
    }
    
    func makeANewOffer(){
        self.buyNowLabel.text = AppConstants.Strings.MAKE_A_NEW_OFFER_FOR
        self.makeOfferButton.setTitle(AppConstants.Strings.MAKE_A_NEW_OFFER, for: .normal)
    }
    
    func counterOffer(){
        self.buyNowLabel.text = AppConstants.Strings.COUNTER_OFFER_FOR
        self.makeOfferButton.setTitle(AppConstants.Strings.COUNTER_OFFER, for: .normal)
    }
    
    func setImage(){
        self.itemImageView.layer.borderWidth = 2.0
        self.itemImageView.layer.masksToBounds = false
        self.itemImageView.layer.borderColor = UIColor.white.cgColor
        self.itemImageView.clipsToBounds = true
        self.itemImageView.layer.cornerRadius = 8
        
        self.itemShadowView.layer.cornerRadius = 6
        self.itemShadowView.layer.shadowColor = UIColor.black.cgColor
        self.itemShadowView.alpha = CGFloat(0.50)
        self.itemShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        self.itemShadowView.layer.shadowOpacity = 1
    }
    
    func setMakeAnOfferData(){
        let listingTitle = "\"" + self.listingTitle! + "\""
        setText(text: listingTitle, listingTitle: self.listingTitle!)
        self.buyNowLabel.text = AppConstants.Strings.MAKE_AN_OFFER_FOR
        self.makeOfferButton.setTitle(AppConstants.Strings.MAKE_AN_OFFER, for: .normal)
    }
    
    func setText(text:String ,listingTitle:String){
        let textRegular:String = "\(listingTitle )"
        let range = (text as NSString).range(of: textRegular)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cBB189C, range: range)
        titleLabel.attributedText = attributedString
    }
    
    func getMyOffersDetail(listingId:Int) {
        alertsViewModel.getMyOffersDetail(accessToken: getAccessToken(), listingId: listingId, { (response) in
            if response.response?.statusCode == 200 || response.response?.statusCode == 201{
                let data: ApiResponse<OfferHistory> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    let offerHistory = data.data ?? nil
                    
                    if self.TYPE == AppConstants.TYPE_MAKE_OFFER{
                        self.subOfferHistory = (offerHistory?.offerHistories!)
                        self.offerDetailHandler.isOfferCancelable = (offerHistory?.isOfferCancelable!)!
                        self.offerDetailHandler.subOfferHistory = (offerHistory?.offerHistories!)!
                        self.setData(offerHistory: offerHistory!)
                    }else{
                        
                        if (offerHistory?.offerHistories ?? nil) !=  nil{
                            self.subOfferHistory = (offerHistory?.offerHistories!)
                            self.noOfferHistoryView.isHidden = true
                            self.offerHistoryTextLabel.isHidden = false
                            self.currentPriceTextLabel.isHidden = false
                            self.currentPriceLabel.isHidden = false
                            self.offerDetailHandler.isOfferCancelable = (offerHistory?.isOfferCancelable!)!
                            self.offerDetailHandler.subOfferHistory = (offerHistory?.offerHistories!)!
                            self.setData(offerHistory: offerHistory!)
                            self.headerViewHeight.constant = 149.5
                        }else{
                            self.noOfferHistoryView.isHidden = false
                            self.offerHistoryTextLabel.isHidden = true
                            self.currentPriceTextLabel.isHidden = true
                            self.currentPriceLabel.isHidden = true
                            self.setMakeAnOfferData()
                            if self.TYPE == AppConstants.TYPE_COUNTER_OFFER{
                                var listingId = self.setting?.listingId!
                                if listingId == nil{
                                    listingId = self.subOffersHistory?.listingId!
                                }
                                self.getListingDetailImages(id: listingId!)
                            }else{
                                self.getListingDetailImages(id:self.listingId!)
                            }
                            
                            self.headerViewHeight.constant = 154
                            
                        }
                    }
                }
            }else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print(error)
                }
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func cancelOffer(listingId:Int,offerId:Int,index:Int) {
        self.view.makeToastActivity(.center)
        alertsViewModel.cancelOffer(accessToken: getAccessToken(), listingId: listingId, offerId: offerId, { (response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 200 || response.response?.statusCode == 201{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    self.offerDetailHandler.isOfferCancelable = false
                    self.offerDetailHandler.offerDetailTableViewCell?.cancelButton.isHidden = true
                    self.tableView.reloadData()
                    self.resizeTableView()
                }
            }else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print(error)
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getReceivedOffersDetail(listingId:Int,initiatorUserId:Int) {
        alertsViewModel.getReceivedOffersDetail(accessToken: getAccessToken(), listingId: listingId, initiatorUserId: initiatorUserId, { (response) in
            if response.response?.statusCode == 200 || response.response?.statusCode == 201{
                let data: ApiResponse<OfferHistory> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    let offerHistory = data.data!
                    
                    self.subOfferHistory = (offerHistory.offerHistories!)
                    self.offerDetailHandler.isOfferCancelable = (offerHistory.isOfferCancelable!)
                    self.offerDetailHandler.subOfferHistory = offerHistory.offerHistories!
                    self.setData(offerHistory: offerHistory)
                }
            }else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print( error)
                }
            }
            
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getListing(id:Int){
        feedViewModel.getListingDetail(accessToken: getAccessToken(), id: id,{(response) in
            self.handleResponse(response:response)
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func handleResponse(response:AFDataResponse<Any>){
        if response.response?.statusCode == 200 {
            let data: ApiResponse<ListingDetail> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            } else {
                if let list = data.data{
                    self.currentPriceLabel.text = AppConstants.DOLLAR  + String(list.price!)
                    self.noOfferHistoryCurrentPriceLabel.text = AppConstants.DOLLAR  + String(list.price!)
                    self.listingTitle = list.title
                    
                    self.listingDetail = list
                    let listingId = (list.id ?? 0)
                    
                    var initiatorUserId = (setting?.initiatorUserId ?? 0)
                    
                    if list.userId == AppDefaults.getUserId(){
                        if initiatorUserId == 0 {
                            initiatorUserId = (subOffersHistory?.buyerId ?? 0)
                        }
                        if initiatorUserId == 0{
                            self.getReceivedOffersDetail(listingId: listingId,initiatorUserId: self.initiatorUserId ?? 0)
                        }else{
                            self.getReceivedOffersDetail(listingId: listingId,initiatorUserId: initiatorUserId)
                        }
                    }else{
                        self.getMyOffersDetail(listingId: listingId)
                    }
                    
                }
            }
        }else{
            let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                print( error)
            }
        }
    }
    
    func getListingDetailImages(id:Int){
        feedViewModel.getListingDetailImages(accessToken: getAccessToken(), id: id,{(response) in
            self.handleResponseImages(response:response)
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func handleResponseImages(response:AFDataResponse<Any>){
        if response.response?.statusCode == 200 {
            let data: ApiResponse<[SliderImages]> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            } else {
                let sliderImages = data.data
                self.listingDetail?.sliderImages = sliderImages
                let image = UIImage(named:AppConstants.Images.DEFAULT_OFFER_PLACEHOLDER)
                
                if sliderImages!.count > 0{
                    let listingImage = sliderImages![0].imageUrl
                    if let imageUrl = listingImage{
                        self.itemImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
                    } else {
                        self.itemImageView.image =  image
                    }
                }else{
                    self.itemImageView.image =  image
                }
                
                self.headerView.isHidden = false
                self.footerView.isHidden = false
                self.tableView.isHidden = true
                self.tableViewHeight.constant = 0
                self.screenActivityIndicator.stopAnimating()
                self.view.hideToastActivity()
            }
        }
    }
    
    func getListingDetailImagesInOffer(id:Int,offerHistory:OfferHistory){
        feedViewModel.getListingDetailImages(accessToken: getAccessToken(), id: id,{(response) in
            self.handleResponseImagesInOffer(response:response, offerHistory: offerHistory)
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func handleResponseImagesInOffer(response:AFDataResponse<Any>,offerHistory:OfferHistory){
        if response.response?.statusCode == 200 {
            let data: ApiResponse<[SliderImages]> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            } else {
                let sliderImages = data.data
                self.listingDetail?.sliderImages = sliderImages
                let image = UIImage(named:AppConstants.Images.DEFAULT_OFFER_PLACEHOLDER)
                
                let listingImage = sliderImages![0].imageUrl
                
                if let imageUrl = listingImage{
                    self.itemImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
                } else {
                    self.itemImageView.image =  image
                }
                if offerHistory.isOfferAllowed ?? false{
                    self.footerView.isHidden = false
                }else{
                    self.footerView.isHidden = true
                    self.tableViewFooter.frame = CGRect(x: 0, y: 0, width: self.tableViewFooter.frame.size.width, height: 6)
                }
                
                self.tableView.reloadData()
                self.resizeTableView()
                
                self.headerView.isHidden = false
                self.tableView.isHidden = false
                if offerHistory.offerHistories!.count > 2{
                    let seconds = 1.0
                    DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
                        self.scrollView.scrollToBottom(animated: true)
                    }
                }
                self.screenActivityIndicator.stopAnimating()
                self.view.hideToastActivity()
            }
        }
    }
    
    func resizeTableView(){
        if self.subOfferHistory!.count == 0{
            tableViewHeight.constant = 0
        }else{
      
            var tableViewHeight: CGFloat {
                self.tableView.layoutIfNeeded()
                return self.tableView.contentSize.height
            }
            self.tableViewHeight?.constant = tableViewHeight
        }
    }
    
    @IBAction func makeOfferButtonAction(_ sender: UIButton) {
        makeAnOfferDialog()
    }
    
    func makeAnOfferDialog(){
        let storybvoard = UIStoryboard(name: AppConstants.Storyboard.MAKE_AN_OFFER_DIALOG_STORYBOARD, bundle: nil)
        let makeOfferDialogViewController = storybvoard.instantiateViewController(withIdentifier: AppConstants.DialogController.MAKE_AN_OFFER_DIALOG) as! MakeAnOfferDialogViewController
        makeOfferDialogViewController.modalPresentationStyle = .overFullScreen
        makeOfferDialogViewController.listingTitle = self.listingTitle
        makeOfferDialogViewController.listingDetail = self.listingDetail
        makeOfferDialogViewController.TYPE = self.TYPE
        makeOfferDialogViewController.isBuyer = self.isBuyer
        makeOfferDialogViewController.makeAnOfferDelegate = self
        if (self.subOfferHistory?.count) ?? 0 > 0{
            makeOfferDialogViewController.subOfferHistory = self.subOfferHistory
        }
        self.navigationController!.present(makeOfferDialogViewController, animated: false, completion: nil)
    }
    
    func buyNowListingScreen(listingDetail: ListingDetail, type: Int,offeredPrice:Int){
        var listing = listingDetail
        if self.subOffersHistory == nil{
            listing.offerId = self.setting?.offerId!
        }else{
            listing.offerId = self.subOffersHistory?.offerId!
        }
        if self.subOffersHistory?.buyerUsername! == nil{
            listing.buyerName = self.offerHistory?.buyerUsername!
        }else{
            listing.buyerName = self.subOffersHistory?.buyerUsername!
        }
        
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.BUY_NOW_LISTING_STORYBOARD, bundle: nil)
        let  buyNowViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.BUY_NOW_LISTING_SCREEN) as! BuyNowListingViewController
        buyNowViewController.listingDetail = listing
        buyNowViewController.TYPE = type
        buyNowViewController.offeredPrice = offeredPrice
        if self.subOfferHistory != nil{
            buyNowViewController.subOfferHistory = self.subOfferHistory
        }
        navigationController?.pushViewController(buyNowViewController, animated: false)
    }
    
    
}
