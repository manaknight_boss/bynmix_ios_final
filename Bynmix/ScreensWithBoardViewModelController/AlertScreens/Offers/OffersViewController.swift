import UIKit
import Alamofire
import XLPagerTabStrip

class OffersViewController: BaseViewController,LoadMoreProtocol {
    
    func onLoadMore() {
        if !isApiCalled {
            isApiCalled = true
            if self.TYPE == AppConstants.TYPE_MY_OFFERS{
                getMyOffers()
            }else if self.TYPE == AppConstants.TYPE_RECEIVED_OFFERS{
                getReceivedOffers()
            }else{
                getBuyerBids(id: self.listingId!)
            }
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var screenActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var noOfferView: UIView!
    @IBOutlet weak var noOffersShadowView: UIView!
    @IBOutlet weak var noOffersMessageLabel: UILabel!
    
    var offersHandler = OffersHandler()
    var currentPage = 1
    var isApiCalled = false
    var refreshControl : UIRefreshControl?
    let alertsViewModel = AlertsViewModel()
    var TYPE:Int?
    var listingId:Int?
    var userOfferHandler = UserOffersHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addRefreshControl()
        itemsClick()
        UIView.animate(withDuration: 0.2) {
            self.alertsOffersTableView = self.tableView
            self.view.layoutIfNeeded()
        }
        offersHandler.loadMoreDelegate = self
        userOfferHandler.loadMoreDelegate = self
    }
    
    func checkType(){
        if self.TYPE == AppConstants.TYPE_MY_OFFERS{
            tableView.delegate = offersHandler
            tableView.dataSource = offersHandler
            getMyOffers()
        }else if self.TYPE == AppConstants.TYPE_RECEIVED_OFFERS{
            tableView.delegate = offersHandler
            tableView.dataSource = offersHandler
            getReceivedOffers()
        }else{
            tableView.delegate = userOfferHandler
            tableView.dataSource = userOfferHandler
            self.topConstraint.constant = 12
            navigationItem.title = AppConstants.Strings.LISTING_OFFERS_TITLE
            self.getBuyerBids(id: self.listingId!)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        noOffersShadowView.layer.cornerRadius = 2
        noOffersShadowView.layer.shadowColor = UIColor.black.cgColor
        noOffersShadowView.alpha = CGFloat(0.26)
        noOffersShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        noOffersShadowView.layer.shadowOpacity = 1
        noOffersShadowView.layer.zPosition = -1
        currentPage = 1
        checkType()
    }
    
    func itemsClick(){
        offersHandler.onViewButtonClick = { setting in
            self.onViewButtonClick(type:self.TYPE!,setting:setting)
        }
        
        offersHandler.onCounterButtonClick = { setting in
            self.onCounterOfferButtonClick(type: self.TYPE!,setting:setting)
        }
        
        offersHandler.onAcceptButtonClick = { setting in
            let listingId = setting.listingId!
            let offerId = setting.offerId!
            let sellerId = setting.sellerId!
            self.offerAccept(listingId: listingId,offerId: offerId,sellerId: sellerId)
        }
        
        offersHandler.onDeclineButtonClick = { setting in
            let listingId = setting.listingId!
            let offerId = setting.offerId!
            self.offerDeclined(listingId: listingId,offerId: offerId)
        }
        
        offersHandler.onUserImageViewClick = { setting in
//            if setting.notificationTypeId == AppConstants.POST_LIKED{
//                if setting.postLink != ""{
//
//                    let storyboard = UIStoryboard(name: AppConstants.Storyboard.POST_DETAIL_BOARD, bundle: nil)
//                    let postDetailViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.POST_DETAIL_SCREEN) as! PostDetailViewController
//                    postDetailViewController.id = setting.postId
//                    self.navigationController?.pushViewController(postDetailViewController, animated: true)
//
//                }
//
//            }else{
//                if (setting.notificationTypeId == AppConstants.COMMENT_RECEIVED || setting.notificationTypeId == AppConstants.USER_FOLLOWING){
//
//                    if setting.initiatorUserId == AppDefaults.getUserId(){
//                        self.openUserProfile()
//                    }else{
//                        self.openProfile(id:setting.initiatorUserId!,username:setting.username!)
//                    }
//
//                }else{
//                    let storyboard = UIStoryboard(name: AppConstants.Storyboard.LISTING_DETAIL_BOARD, bundle: nil)
//
//                    let listingDetailViewController = storyboard.instantiateViewController(withIdentifier:AppConstants.Controllers.Listing_Detail_Screen) as! ListDetailViewController
//                    listingDetailViewController.itemId = setting.listingId
//                    self.navigationController?.pushViewController(listingDetailViewController, animated: true)
//                }
//            }
            
            self.redirectToNotificationSetting(setting:setting)
        }
        
        offersHandler.onFormattedTextClick = { setting in
//            if setting.initiatorUserId == AppDefaults.getUserId(){
//                self.openUserProfile()
//            }else{
//                self.openProfile(id:setting.initiatorUserId!,username:setting.username!)
//            }
            self.redirectToNotificationSetting(setting:setting)
        }
        
        userOfferHandler.onViewButtonClick = { subOfferHistory in
            self.onViewButtonClickUserOffer(type:self.TYPE!,subOfferHistory:subOfferHistory)
        }
        
        userOfferHandler.onCounterButtonClick = { subOfferHistory in
            self.onCounterOfferButtonClickUserOffer(type: self.TYPE!,subOfferHistory:subOfferHistory)
        }
        
        userOfferHandler.onAcceptButtonClick = { subOfferHistory in
            let listingId = subOfferHistory.listingId!
            let offerId = subOfferHistory.offerId!
            let sellerId = subOfferHistory.sellerId!
            self.offerAccept(listingId: listingId,offerId: offerId,sellerId: sellerId)
        }
        
        userOfferHandler.onDeclineButtonClick = { subOfferHistory in
            let listingId = subOfferHistory.listingId!
            let offerId = subOfferHistory.offerId!
            self.offerDeclined(listingId: listingId,offerId: offerId)
        }
        
        userOfferHandler.onUserImageViewClick = { subOfferHistory in
//            let listingId = subOfferHistory.listingId!
//            self.listingDetailScreen(id: listingId)
            self.redirectToNotificationSubOfferHistory(subOfferHistoy:subOfferHistory)
        }
        
        userOfferHandler.onFormattedTextClick = { subOfferHistory in
            //            if subOfferHistory.initia == AppDefaults.getUserId(){
            //                self.tabBarController?.selectedIndex = 4
            //            }else{
            //                self.openProfile(id:subOfferHistory.userId!,username:subOfferHistory.username!)
            //            }
            self.redirectToNotificationSubOfferHistory(subOfferHistoy:subOfferHistory)
        }
        
    }
    
    func redirectToNotificationSetting(setting:Setting){
        let notificationRedirectTypeId = setting.notificationRedirectTypeId ?? 0
        let notificationRedirectType = setting.notificationRedirectType ?? ""
        let notificationRedirectId = setting.notificationRedirectId ?? 0
        let idType = setting.idType ?? ""
        let initiatorUserId = setting.initiatorUserId ?? 0
        
        self.notificationRedirect(notificationRedirectTypeId: notificationRedirectTypeId, notificationRedirectType: notificationRedirectType, notificationRedirectId: notificationRedirectId, idType: idType, initiatorUserId: initiatorUserId)
    }
    
    func redirectToNotificationSubOfferHistory(subOfferHistoy:SubOfferHistory){
        let notificationRedirectTypeId = subOfferHistoy.notificationRedirectTypeId ?? 0
        let notificationRedirectType = subOfferHistoy.notificationRedirectType ?? ""
        let notificationRedirectId = subOfferHistoy.notificationRedirectId ?? 0
        let idType = subOfferHistoy.idType ?? ""
        let initiatorUserId = subOfferHistoy.initiatorUserId ?? 0
        
        self.notificationRedirect(notificationRedirectTypeId: notificationRedirectTypeId, notificationRedirectType: notificationRedirectType, notificationRedirectId: notificationRedirectId, idType: idType, initiatorUserId: initiatorUserId)
    }
    
    func onViewButtonClick(type:Int,setting:Setting){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.OFFER_DETAIL_STORYBOARD, bundle: nil)
        let  offerDetailViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.OFFERS_DETAIL_SCREEN) as! OfferDetailViewController
        offerDetailViewController.TYPE = type
        offerDetailViewController.setting = setting
        navigationController?.pushViewController(offerDetailViewController, animated: true)
    }
    
    func onViewButtonClickUserOffer(type:Int,subOfferHistory:SubOfferHistory){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.OFFER_DETAIL_STORYBOARD, bundle: nil)
        let  offerDetailViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.OFFERS_DETAIL_SCREEN) as! OfferDetailViewController
        offerDetailViewController.TYPE = type
        offerDetailViewController.subOffersHistory = subOfferHistory
        navigationController?.pushViewController(offerDetailViewController, animated: true)
    }
    
    func onCounterOfferButtonClick(type:Int,setting:Setting){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.OFFER_DETAIL_STORYBOARD, bundle: nil)
        let  offerDetailViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.OFFERS_DETAIL_SCREEN) as! OfferDetailViewController
        offerDetailViewController.TYPE = type
        offerDetailViewController.setting = setting
        navigationController?.pushViewController(offerDetailViewController, animated: true)
    }
    
    func onCounterOfferButtonClickUserOffer(type:Int,subOfferHistory:SubOfferHistory){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.OFFER_DETAIL_STORYBOARD, bundle: nil)
        let  offerDetailViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.OFFERS_DETAIL_SCREEN) as! OfferDetailViewController
        offerDetailViewController.TYPE = type
        offerDetailViewController.subOffersHistory = subOfferHistory
        navigationController?.pushViewController(offerDetailViewController, animated: true)
    }
    
    func offerAccept(listingId:Int,offerId:Int,sellerId:Int) {
        self.view.makeToastActivity(.center)
        self.view.isUserInteractionEnabled = false
        alertsViewModel.offerAccepted(accessToken: getAccessToken(), listingId: listingId, offerId: offerId, { (response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 200 || response.response?.statusCode == 201{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    if sellerId == AppDefaults.getUserId(){
                        self.openMySaleAndMyPurchasesScreen(type:AppConstants.TYPE_SALE)
                    }else{
                        self.openMySaleAndMyPurchasesScreen(type:AppConstants.TYPE_PURCHASE)
                    }
                }
                self.view.isUserInteractionEnabled = true
            }else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print(error)
                }
                self.view.isUserInteractionEnabled = true
            }
            
        } , { (error) in
            self.view.hideToastActivity()
            self.view.isUserInteractionEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func offerDeclined(listingId:Int,offerId:Int) {
        self.view.makeToastActivity(.center)
        self.view.isUserInteractionEnabled = false
        alertsViewModel.offerDeclined(accessToken: getAccessToken(), listingId: listingId, offerId: offerId, { (response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 200 || response.response?.statusCode == 201{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    self.currentPage = 1
                    if self.TYPE == AppConstants.TYPE_MY_OFFERS{
                        self.getMyOffers()
                    }else if self.TYPE == AppConstants.TYPE_RECEIVED_OFFERS{
                        self.getReceivedOffers()
                    }else{
                        self.getBuyerBids(id: self.listingId!)
                    }
                }
                self.view.isUserInteractionEnabled = true
            }else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print(error)
                }
                self.view.isUserInteractionEnabled = true
            }
            
        } , { (error) in
            self.view.hideToastActivity()
            self.view.isUserInteractionEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getMyOffers() {
        alertsViewModel.getMyOffers(accessToken: getAccessToken(),page:currentPage, { (response) in
            self.handleResponse(response: response)
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            self.refreshControl?.endRefreshing()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getReceivedOffers() {
        alertsViewModel.getReceivedOffers(accessToken: getAccessToken(),page:currentPage, { (response) in
            self.handleResponse(response: response)
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            self.refreshControl?.endRefreshing()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getBuyerBids(id:Int) {
        alertsViewModel.getBuyerBids(accessToken: getAccessToken(), id: id,page:currentPage, { (response) in
            self.handleResponseBuyerBids(response: response)
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            self.refreshControl?.endRefreshing()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func handleResponse(response: AFDataResponse<Any>) {
        self.isApiCalled = false
        self.refreshControl?.endRefreshing()
        if response.response?.statusCode == 200 {
            let data: ApiResponse<Setting> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            } else {
                self.offersHandler.TYPE = self.TYPE
            
                let settingData = data.data
                
                if (settingData?.items!.count)! > 0{
                    
                    if let setting = settingData?.items {
                        if self.currentPage == 1 {
                            self.offersHandler.setting = setting
                        } else {
                            self.offersHandler.setting.append(contentsOf: setting)
                        }
                        if settingData?.totalPages == currentPage {
                            self.isApiCalled = true
                            self.offersHandler.isLoadMoreRequired = false
                        }
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                            self.screenActivityIndicator.stopAnimating()
                            self.tableView.isHidden = false
                        }
                        
                    }
                    
                }else{
                    if self.TYPE == AppConstants.TYPE_MY_OFFERS{
                        self.noOffersMessageLabel.text = AppConstants.Strings.NO_ACTIVE_OFFERS
                    }else{
                        self.noOffersMessageLabel.text = AppConstants.Strings.NO_RECEIVED_OFFERS
                    }
                    self.offersHandler.setting.removeAll()
                    self.tableView.reloadData()
                    self.tableView.isHidden = true
                    self.noOfferView.isHidden = false
                    self.noOffersShadowView.isHidden = false
                    self.screenActivityIndicator.stopAnimating()
                }
                
            }
            self.currentPage = self.currentPage + 1
        } else {
            
        }
    }
    
    
    func handleResponseBuyerBids(response: AFDataResponse<Any>) {
        self.isApiCalled = false
        self.refreshControl?.endRefreshing()
        if response.response?.statusCode == 200 {
            let data: ApiResponse<SubOfferHistory> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            } else {
                self.userOfferHandler.TYPE = self.TYPE
                let settingData = data.data
                
                if (settingData?.items?.count)! > 0{
                    
                    if let setting = settingData?.items {
                        if self.currentPage == 1 {
                            self.userOfferHandler.subOfferHistory = setting
                        } else {
                            self.userOfferHandler.subOfferHistory.append(contentsOf: setting)
                        }
                        if settingData?.totalPages == currentPage {
                            self.isApiCalled = true
                            self.userOfferHandler.isLoadMoreRequired = false
                        }
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                            self.screenActivityIndicator.stopAnimating()
                            self.tableView.isHidden = false
                        }
                        
                    }
                    
                }else{
                    self.userOfferHandler.subOfferHistory.removeAll()
                    self.tableView.reloadData()
                    self.tableView.isHidden = true
                    self.noOffersMessageLabel.text = AppConstants.Strings.NO_OFFERS_ON_LISTING
                    self.noOfferView.isHidden = false
                    self.noOffersShadowView.isHidden = false
                    self.screenActivityIndicator.stopAnimating()
                }
                
            }
            self.currentPage = self.currentPage + 1
        } else {
            
        }
    }
    
    func addRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        tableView.addSubview(refreshControl!)
    }
    
    @objc func refreshList(){
        currentPage = 1
        isApiCalled = false
        self.offersHandler.isLoadMoreRequired = true
        if self.TYPE == AppConstants.TYPE_MY_OFFERS{
            self.getMyOffers()
        }else if self.TYPE == AppConstants.TYPE_RECEIVED_OFFERS{
            self.getReceivedOffers()
        }else{
            self.getBuyerBids(id: self.listingId!)
        }
    }
    
}

extension OffersViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        var title = ""
        if self.TYPE == AppConstants.TYPE_MY_OFFERS{
            title = AppConstants.Strings.MY_OFFERS
        }else{
            title = AppConstants.Strings.RECEIVED_OFFERS
        }
        return IndicatorInfo(title: title)
    }
}
