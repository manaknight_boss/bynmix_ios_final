import UIKit
import GoogleSignIn
import TTGSnackbar
import FacebookCore
import FacebookLogin
import Toast_Swift
import FBSDKLoginKit
import AMPopTip
import GTMSessionFetcher

class BaseViewController: UIViewController, UITextFieldDelegate, GIDSignInDelegate,UITextViewDelegate, UITabBarControllerDelegate, CreateClickProtocol,onBackButtonPressProtocol,UIAdaptivePresentationControllerDelegate, ListingDetailRefreshProtocol {
    
    func listingDetailRefresh() {
        NotificationCenter.default.post(name: Notification.Name("listingDetailRefresh"), object: nil)
    }
    
    func onBackButtonPress(type: Int) {
        if type == AppConstants.TYPE_OK_BUTTON{
            
            let currentViewController = getVisibleViewController(CreateListingViewController())
            
            if currentViewController == CreateListingViewController(){
                self.tabBarController?.selectedIndex = 0
            }else{
                self.navigationController?.popViewController(animated: false)
            }
            AppConstants.LISTING_LOST = false
        }else{
            //self.tabBarController?.selectedIndex = AppConstants.SELECTED_TAB
            print("Selected Tab:",AppConstants.SELECTED_TAB,"type:",type)
            AppConstants.LISTING_LOST = false
        }
    }
    
    func onCreateClick(type: Int) {
        if type == AppConstants.CREATE_TYPE_POST {
            if let nav = self.tabBarController?.viewControllers?[2] as? CreateNavigationViewController {
                nav.viewControllerType = AppConstants.CREATE_TYPE_POST
                nav.popToRootViewController(animated: false)
            }
            self.tabBarController?.selectedIndex = 2
        } else if type == AppConstants.CLEAR_STACK_OF_CREATE_SCREEN_TYPE {
            AppConstants.IS_JUST_CREATED = AppConstants.CREATE_TYPE_POST
            self.tabBarController?.selectedIndex = 4
            if let nav = self.tabBarController?.viewControllers?[4] as? ProfileNavigationViewController {
                nav.popToRootViewController(animated: false)
            }
        } else if type == AppConstants.CLEAR_STACK_OF_CREATE_LISTING_TYPE {
            AppConstants.IS_JUST_CREATED = AppConstants.CREATE_TYPE_LISTING
            self.tabBarController?.selectedIndex = 4
            if let nav = self.tabBarController?.viewControllers?[4] as? ProfileNavigationViewController {
                nav.popToRootViewController(animated: false)
            }
        } else if type == AppConstants.CREATE_TYPE_LISTING {
            if let nav = self.tabBarController?.viewControllers?[2] as? CreateNavigationViewController {
                nav.popToRootViewController(animated: false)
                nav.viewControllerType = AppConstants.CREATE_TYPE_LISTING
            }
            self.tabBarController?.selectedIndex = 2
        } else if type == AppConstants.CREATE_TYPE_NONE {
            // todo fix here
            print("old tab: ",self.selectedTab)
            self.tabBarController?.selectedIndex = self.selectedTab
        }else{
            print("else block")
        }
    }
    
    let ANIMATION_DURATION_OF_KEYBOARD = 0.3
    var keyboardHeight: CGFloat?
    var activeTextField: UITextField?
    var activeTextView: UITextView?
    var initialTopConstraintValue: CGFloat = 0
    var topConstraintForKeyboard: NSLayoutConstraint?
    var popTip: PopTip?
    var alertViewModel = AlertsViewModel()
    var isAlertsApiCalled = true
    var feedAllTableView:UITableView?
    var feedListingCollectionView:UICollectionView?
    var feedPoststableView:UITableView?
    var alertsAllTableView:UITableView?
    var alertsOffersTableView:UITableView?
    var selectedTab = 0
    var getkeyboardHeightInNumeric: CGFloat?
    var trackSearchDictionary : [String: [String]] = [:]
    var videoUrls:[URL] = []
    
    static var mSelectedFeedFilterList:[SelectedFilter] = []
    static var mSelectedBrowseFilterList:[SelectedFilter] = []
    static var mSelectedUserBynFilterList:[SelectedFilter] = []
    static var mSelectedMyLikesFilterList:[SelectedFilter] = []
    static var mSelectedOtherUserProfileFilterList:[SelectedFilter] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        changeStatusBarBackground()
        let button = UIBarButtonItem(title: " ", style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.backBarButtonItem = button
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNeedsStatusBarAppearanceUpdate()
        NotificationCenter.default.addObserver(self, selector: #selector(getkeyboardHeight(notification:)), name: UIWindow.keyboardWillShowNotification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.delegate = self
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        self.dismiss(animated: false, completion: nil)
        let selectedIndex = tabBarController.viewControllers!.firstIndex(of: viewController)!
        AppConstants.SELECTED_TAB = selectedIndex
        self.selectedTab = tabBarController.selectedIndex
        if viewController is CreateNavigationViewController || selectedIndex == 2{
            
            let createViewController = tabBarController.storyboard?.instantiateViewController(withIdentifier: AppConstants.Controllers.CREATE_SCREEN) as! CreateViewController
            createViewController.modalPresentationStyle = .overCurrentContext
            createViewController.createClickDelegate = self
            tabBarController.present(createViewController, animated: false,completion: nil)
            return false
        }
        
        if AppConstants.LISTING_LOST {
            
            let storyBoard = UIStoryboard(name: AppConstants.Storyboard.LISTING_LOST_DIALOG_BOARD, bundle: nil)
            let listingLostViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.LISTING_LOST_DIALOG) as! ListingLostDialogViewController
            listingLostViewController.onBackButtonPressDelegate = self
            listingLostViewController.TYPE = AppConstants.TYPE_LISTING_LOST_ON_TAB_SELECT
            tabBarController.present(listingLostViewController, animated: false)
            return false
        }
        
        
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 0 || tabBarIndex == 1{
            self.setFeedScrollToTop()
        }else if tabBarIndex == 3{
            //self.alertsAllTableView?.setContentOffset(CGPoint(x: 0, y: -40), animated: true)
            //self.alertsOffersTableView?.setContentOffset(CGPoint.zero, animated: true)
        }else{
            
        }
        
        return true
    }
    
    func setFeedScrollToTop(){
        self.feedAllTableView?.setContentOffset(CGPoint(x: 0, y: -40), animated: true)
        self.feedListingCollectionView?.setContentOffset(CGPoint(x: 0, y: -40), animated: true)
        self.feedPoststableView?.setContentOffset(CGPoint(x: 0, y: -40), animated: true)
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        AppConstants.CURRENT_TAB = tabBarController.selectedIndex
        guard let navigation = viewController as? UINavigationController else {
            return
        }
        
        navigation.popToRootViewController(animated: false)
        
        let tabBarIndex = tabBarController.selectedIndex
        if tabBarIndex == 3 {
            if let tabItems = tabBarController.tabBar.items {
                // In this case we want to modify the badge number of the third tab:
                let tabItem = tabItems[3]
                tabItem.badgeValue = nil
            }
        }
    }
    
    @objc func goBack()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    func keyboardRequired(topConstraint: NSLayoutConstraint) {
        topConstraintForKeyboard = topConstraint
        initialTopConstraintValue = topConstraintForKeyboard!.constant
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShowNotification(notification:)), name: UIWindow.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHideNotification(notification:)), name: UIWindow.keyboardWillHideNotification, object: nil)
    }
    
    func keyboardRequiredForTextView(topConstraint: NSLayoutConstraint) {
        topConstraintForKeyboard = topConstraint
        initialTopConstraintValue = topConstraintForKeyboard!.constant
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShowNotificationForTextView(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHideNotificationForTextView(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    @objc func keyboardWillShowNotification(notification: Notification) {
        if let userInfo = notification.userInfo as? Dictionary <String, AnyObject> {
            let frame = userInfo[UIResponder.keyboardFrameEndUserInfoKey]
            let keyboardRect = frame?.cgRectValue
            if let keyboardHeight = keyboardRect?.height {
                self.keyboardHeight = keyboardHeight
                self.upTheTextField()
            }
        }
    }
    
    @objc func getkeyboardHeight(notification: Notification) {
        if let userInfo = notification.userInfo as? Dictionary <String, AnyObject> {
            let frame = userInfo[UIResponder.keyboardFrameEndUserInfoKey]
            let keyboardRect = frame?.cgRectValue
            if let keyboardHeight = keyboardRect?.height {
                self.getkeyboardHeightInNumeric = keyboardHeight
            }
        }
    }
    
    @objc func keyboardWillShowNotificationForTextView(notification: Notification) {
        if let userInfo = notification.userInfo as? Dictionary <String, AnyObject> {
            let frame = userInfo[UIResponder.keyboardFrameEndUserInfoKey]
            let keyboardRect = frame?.cgRectValue
            if let keyboardHeight = keyboardRect?.height {
                self.keyboardHeight = keyboardHeight
                self.upTheTextView()
            }
        }
    }
    
    func upTheTextField() {
        if let keyboardVisibleHeight = keyboardHeight {
            if let activeTextField = self.activeTextField {
                let textFieldMaxY = self.view.convert(activeTextField.frame, from: activeTextField.superview).maxY + initialTopConstraintValue
                let keyboardMinY = self.view.frame.height - keyboardVisibleHeight
                if(textFieldMaxY > keyboardMinY) {
                    topConstraintForKeyboard!.constant = initialTopConstraintValue - (textFieldMaxY - keyboardMinY)
                }
            }
            
            UIView.animate(withDuration: self.ANIMATION_DURATION_OF_KEYBOARD) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func upTheTextView() {
        if let keyboardVisibleHeight = keyboardHeight {
            if let activeTextView = self.activeTextView {
                let textViewMaxY = self.view.convert(activeTextView.frame, from: activeTextView.superview).maxY + initialTopConstraintValue
                let keyboardMinY = self.view.frame.height - keyboardVisibleHeight
                if(textViewMaxY > keyboardMinY) {
                    topConstraintForKeyboard!.constant = initialTopConstraintValue - (textViewMaxY - keyboardMinY)
                }
            }
            
            UIView.animate(withDuration: self.ANIMATION_DURATION_OF_KEYBOARD) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func resetTheTextField() {
        self.topConstraintForKeyboard!.constant = initialTopConstraintValue
    }
    
    func resetTheTextView() {
        self.topConstraintForKeyboard!.constant = initialTopConstraintValue
    }
    
    @objc func keyboardWillHideNotification(notification: Notification) {
        self.resetTheTextField()
    }
    
    @objc func keyboardWillHideNotificationForTextView(notification: Notification) {
        self.resetTheTextView()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        hideToolTip()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
        self.upTheTextField()
        return true
    }
    
    func  changeStatusBarBackground() {
        let statusBarView = UIView(frame: UIApplication.shared.statusBarFrame)
        statusBarView.backgroundColor = UIColor.black
        statusBarView.tintColor = UIColor.white
        view.addSubview(statusBarView)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent;
    }
    
    func showErrorTooltip(errorMessage: String, view: UIView) {
        popTip = PopTip()
        popTip?.arrowRadius = 5
        popTip?.font = UIFont.fontRegular(size: 13)
        popTip?.bubbleColor = UIColor.black
        popTip?.entranceAnimation = .none
        popTip?.exitAnimation = .none
        popTip?.bubbleOffset = 5
        popTip?.arrowSize = CGSize(width: 5, height: 5)
        popTip?.arrowOffset = 5
        popTip?.show(text: errorMessage, direction: .up, maxWidth: 250, in: view, from: view.frame)
    }
    
    func hideToolTip() {
        if let popTip = self.popTip{
            if !popTip.isHidden{
                popTip.hide()
            }
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            let idToken = user.authentication.idToken
            let givenName = user.profile.givenName
            let familyName = user.profile.familyName
            let email = user.profile.email
            
            loginWithSocialAccount(firstName: givenName!, lastName: familyName!, email: email!, accessToken: idToken!, provider: "Google")
        }else if error.localizedDescription == AppConstants.Strings.USER_CANCELED_GOOGLE_LOGIN{
            print("\(error.localizedDescription)")
        }else {
            print("\(error.localizedDescription)")
            self.showMessageDialog(message: AppConstants.Strings.FACEBOOK_GOOGLE_ERROR)
        }
    }
    
    func loginWithSocialAccount(firstName: String, lastName: String, email: String, accessToken: String, provider: String) {
        let userAuth = UserAuth(firstname: firstName, lastname: lastName, email: email, accessToken: accessToken, provider: provider)
        self.view.makeToastActivity(.center)
        AuthViewModel().loginWithSocial(request: userAuth,{ (response) in
            let data: UserAuth = Utils.convertResponseToData(data: response.data!)
            if response.response?.statusCode == 200 {
                if let token = data.access_token {
                    self.setTemporaryToken(accessToken: token)
                    ProfileViewModel().getUser(accessToken: token, { (response) in
                        self.view.hideToastActivity()
                        if response.response?.statusCode == 200 {
                            let data: ApiResponse<User> = Utils.convertResponseToData(data: response.data!)
                            if data.isError {
                                let error = Utils.getErrorMessage(errors: data.errors)
                                self.showMessageDialog(message: error)
                            } else {
                                let user = data.data!
                                
                                let username = user.username ?? ""
                                AppDefaults.setUsername(username: username)
                                var eventType = UserAuth(eventType: AppConstants.TYPE_EVENT_UNKNOWN)
                                if provider == "Google"{
                                    eventType = UserAuth(eventType: AppConstants.TYPE_EVENT_GOOGLE_LOGIN)
                                }else{
                                    eventType = UserAuth(eventType: AppConstants.TYPE_EVENT_FACEBOOK_LOGIN)
                                }
                                self.trackUsersInBase(token: token, data: eventType)
                                
                                if user.lastOnboardingScreenCompleted == nil {
                                    self.checkForOnBoardingStage(onBoardingComplete: false, lastOnboardingScreenCompleted: 0)
                                }else{
                                    self.checkForOnBoardingStage(onBoardingComplete: user.onboardingComplete!, lastOnboardingScreenCompleted: user.lastOnboardingScreenCompleted!)
                                }
                            }
                        }
                    } , { (error) in
                        self.view.hideAllToasts()
                        if error.statusCode == AppConstants.NO_INTERNET {
                            self.showInternetError()
                        }
                        
                    })
                }
            } else {
                if let error = data.message {
                    self.view.hideAllToasts()
                    print(error)
                    self.showMessageDialog(message: AppConstants.Strings.FACEBOOK_GOOGLE_ERROR)
                }
            }
        }, { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.view.hideAllToasts()
                self.showInternetError()
            }
        })
    }
    
    func initGoogleLogin() {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance()?.presentingViewController = self
        GIDSignIn.sharedInstance().signIn()
        
//        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
    }
    
     func loginButtonClicked(type:Int?) {
        let fbLoginManager : LoginManager = LoginManager()
        fbLoginManager.logIn(permissions: ["public_profile","email"], from: self) { (result, error) -> Void in
            if (error == nil){
                let fbloginresult : LoginManagerLoginResult = result!
                if (result?.isCancelled)!{
                    self.view.makeToast( AppConstants.Strings.FACEBOOK_ERROR)
                    return
                }
                if(fbloginresult.grantedPermissions.contains("email")) {
                    self.getFBUserData(type:type ?? 0)
                }
            }
        }
    }
    
    func getFBUserData(type:Int) {
        var firstName = ""
        var lastName = ""
        var email = ""
        var fbID = ""
        var name = ""
        if((AccessToken.current) != nil){
            GraphRequest(graphPath: "me", parameters: ["fields": "id, name, first_name, last_name , email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    guard let userDict = result as? [String:Any] else {
                        return
                    }
                    print("userDict:",userDict)
                    if let firstFamilyName = userDict["first_name"] as? String {
                        firstName = firstFamilyName
                    }
                    if let lastFamilyName = userDict["last_name"] as? String {
                        lastName = lastFamilyName
                    }
                    if let fullEmail = userDict["email"] as? String {
                        email = fullEmail
                    }
                    if let fbId = userDict["id"] as? String {
                        fbID = fbId
                    }
                    if let fbName = userDict["name"] as? String {
                        name = fbName
                    }
                    let token = AccessToken.current?.tokenString ?? ""
                    print(token,firstName,lastName,fbID)
                    print("email:",email)
                    
                    let fbLink = "https://www.facebook.com/profile.php?\(fbID)"
                    let user = User(facebookUsername: name, facebookLink: fbLink)
                    self.fbSaveDetails(user:user)
                    if type == 0{
                        self.loginWithSocialAccount(firstName: firstName, lastName: lastName, email: email, accessToken: token, provider: "Facebook")
                    }else{
                        NotificationCenter.default.post(name: Notification.Name(AppConstants.Notifications.SOCIAL_LINK_FB_BAR_BUTTON),object: nil,userInfo:[AppConstants.Notifications.FB_LINK_CHANGED: 1])
                    }
                 }
            })
        }
    }
    
    func fbSaveDetails(user:User){
        ProfileViewModel().updateProfileInfo(accessToken: getAccessToken(), data: user,{(response) in
            if response.response?.statusCode == 200 {
                self.navigationController?.popViewController(animated: false)
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func showInternetError() {
        let snackbar = TTGSnackbar(message: AppConstants.Strings.NO_INTERNET_MESSAGE, duration: .short)
        snackbar.show()
    }
    
    func showSnackBar(message: String) {
        let snackbar = TTGSnackbar(message: message, duration: .short)
        snackbar.show()
    }
    
    func showMessageDialog(message: String) {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.MESSAGE_DIALOG_BOARD, bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.MESSAGE_DIALOG) as! MessageDialogViewController
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        myAlert.message = message
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func showFailDialog(message: String) {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.FAILS_MESSAGE_DIALOG_BOARD, bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.FAIL_MESSAGE_DIALOG) as! FailesMessageViewController
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        myAlert.message = message
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func showCreatePostMessageDialog(text: String,textColor:String) {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.MESSAGE_DIALOG_BOARD, bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.MESSAGE_DIALOG) as! MessageDialogViewController
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        myAlert.text = text
        myAlert.textColor = textColor
        
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func showDropDown(tag: Int, title: String, dataSourse: UIPickerViewDataSource, delegate: UIPickerViewDelegate, _ onClick: @escaping (Int) -> Void) {
        let vc = UIViewController()
        vc.preferredContentSize = CGSize(width: self.view.frame.width - 16, height: self.view.frame.width / 2)
        vc.view.backgroundColor = UIColor.white
        let pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width - 16, height: self.view.frame.width / 2))
        pickerView.tag = tag
        pickerView.delegate = dataSourse as? UIPickerViewDelegate
        pickerView.dataSource = delegate as? UIPickerViewDataSource
        vc.view.addSubview(pickerView)
        let dropDownAlert = UIAlertController(title: title, message: "", preferredStyle: UIAlertController.Style.actionSheet)
        dropDownAlert.view.subviews.first?.subviews.first?.subviews.first?.backgroundColor = UIColor.white
        dropDownAlert.setValue(vc, forKey: "contentViewController")
        dropDownAlert.addAction(UIAlertAction(title: AppConstants.Strings.DONE, style: .default, handler: {
            (alert: UIAlertAction!) in
            onClick(pickerView.selectedRow(inComponent: 0))
        }))
        dropDownAlert.addAction(UIAlertAction(title: AppConstants.CANCEL, style: .destructive, handler: nil))
        self.present(dropDownAlert, animated: true)
    }
    
    public func setTemporaryToken(accessToken: String) {
        AppDefaults.setToken(token: accessToken)
    }
    
    public func getTemporaryToken() -> String {
        return AppDefaults.getToken()
    }
    
    public func setAccessToken(accessToken: String) {
        AppDefaults.setAccessToken(token: accessToken)
    }
    
    public func getAccessToken() -> String {
        return AppDefaults.getAccessToken()
    }
    
    public func pushViewController(storyboardName: String, viewControllerIdentifier: String) {
        let storyBoard : UIStoryboard = UIStoryboard(name: storyboardName, bundle:nil)
        let ViewController = storyBoard.instantiateViewController(withIdentifier: viewControllerIdentifier)
        self.navigationController?.pushViewController(ViewController, animated:true)
        
    }
    
    public func openProfile(id:Int,username:String) {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.OTHER_USER_PROFILE_BOARD, bundle: nil)
        let otherUserViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.OTHER_USER_PROFILE) as! OtherUserProfileViewController
        otherUserViewController.otherUserId = id
        otherUserViewController.USERNAME = username
        self.navigationController?.pushViewController(otherUserViewController, animated: true)
        
    }
    
    public func presentViewController(storyboardName: String, viewControllerIdentifier: String) {
        let storyBoard : UIStoryboard = UIStoryboard(name: storyboardName, bundle:nil)
        let ViewController = storyBoard.instantiateViewController(withIdentifier: viewControllerIdentifier)
        if #available(iOS 13.0, *) {
            ViewController.isModalInPresentation = true
        } else {
            // Fallback on earlier versions
        }
        self.present(ViewController, animated:false, completion:nil)
    }
    
    func heightForLabel(text:String, width:CGFloat) -> CGFloat {
        
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byCharWrapping
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
    
    func checkForOnBoardingStage(onBoardingComplete: Bool, lastOnboardingScreenCompleted: Int) {
        if onBoardingComplete {
            self.setAccessToken(accessToken: self.getTemporaryToken())
            self.tabsController()
        } else {
            if lastOnboardingScreenCompleted == AppConstants.OnBoardingStages.SELECT_BRANDS {
                self.presentViewControllerForOnBoarding(storyboardName: AppConstants.Storyboard.WELCOME, viewControllerIdentifier: AppConstants.Controllers.WELCOME_SCREEN)
            } else if lastOnboardingScreenCompleted == AppConstants.OnBoardingStages.SELECT_TAGS {
                self.presentViewControllerForOnBoarding(storyboardName: AppConstants.Storyboard.SELECT_TAGS, viewControllerIdentifier: AppConstants.Controllers.SELECT_TAGS)
            } else if lastOnboardingScreenCompleted == AppConstants.OnBoardingStages.SELECT_BLOGGERS {
                self.presentViewControllerForOnBoarding(storyboardName: AppConstants.Storyboard.BLOGGERS, viewControllerIdentifier: AppConstants.Controllers.SELECT_BLOGGERS)
            } else {
                self.presentViewControllerForOnBoarding(storyboardName: AppConstants.Storyboard.PROFILE_INFO, viewControllerIdentifier: AppConstants.Controllers.PROFILE_INFO)
            }
            
        }
    }
    
    func tabsController(){
        let mainStoryBoard = UIStoryboard(name: AppConstants.Storyboard.HOME, bundle: nil)
        let rootViewController = mainStoryBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.MAIN_TAB)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.window?.rootViewController = rootViewController
    }
    
    func registerScreen(){
        self.pushViewController(storyboardName: AppConstants.Storyboard.REGISTER, viewControllerIdentifier: AppConstants.Controllers.REGISTER_SCREEN)
    }
    
    func privacyPolicyScreen(){
        self.pushViewController(storyboardName: AppConstants.Storyboard.PRIVACY_POLICY, viewControllerIdentifier: AppConstants.Controllers.PRIVACY_POLICY)
    }
    
    func termsOfServiceScreen(){
        self.pushViewController(storyboardName: AppConstants.Storyboard.TERMS_OF_SERVICE, viewControllerIdentifier: AppConstants.Controllers.TERMS_SCREEN)
    }
    
    func aboutUsScreen(){
        self.pushViewController(storyboardName: AppConstants.Storyboard.ABOUT_US, viewControllerIdentifier: AppConstants.Controllers.ABOUT_SCREEN)
    }
    
    func createAccountScreen(){
        self.pushViewController(storyboardName: AppConstants.Storyboard.CREATE_ACCOUNT, viewControllerIdentifier: AppConstants.Controllers.CREATE_ACCOUNT)
    }
    
    func forgotPasswordScreen(){
        self.pushViewController(storyboardName:  AppConstants.Storyboard.FORGOT_PASSWORD, viewControllerIdentifier: AppConstants.Controllers.FORGOT_PASSWORD_EMAIL)
    }
    
    func selectBrandsScreen(){
        self.presentViewControllerForOnBoarding(storyboardName: AppConstants.Storyboard.SELECT_BRANDS, viewControllerIdentifier: AppConstants.Controllers.SELECT_BRANDS)
    }
    
    func selectTagsScreen(){
        self.presentViewControllerForOnBoarding(storyboardName: AppConstants.Storyboard.SELECT_TAGS, viewControllerIdentifier: AppConstants.Controllers.SELECT_TAGS)
    }
    
    func bloggersScreen(){
        self.presentViewControllerForOnBoarding(storyboardName: AppConstants.Storyboard.BLOGGERS, viewControllerIdentifier: AppConstants.Controllers.SELECT_BLOGGERS)
    }
    
    func profileInfoScreen(){
        self.presentViewControllerForOnBoarding(storyboardName: AppConstants.Storyboard.PROFILE_INFO, viewControllerIdentifier: AppConstants.Controllers.PROFILE_INFO)
    }
    
    func logoutScreenBase(){
        let mainStoryboard: UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.LOGIN, bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: AppConstants.Controllers.LOGIN_NAVIGATION_CONTROLLER) as! LoginNavigationViewController
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    func presentationControllerShouldDismiss(_ presentationController: UIPresentationController) -> Bool {
        return false
    }
    
    func tagsDemoDialog(){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.TAGS_INFO_DIALOG_BOARD, bundle: nil)
        let tagsInfoViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.TAGS_INFO_SCREEN) as! TagsInfoViewController
        tagsInfoViewController.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(tagsInfoViewController, animated: false)
    }
    
    func editListingScreen(listingDetail:ListingDetail){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.EDIT_LISTING_STORYBOARD, bundle: nil)
        let editListingScreen = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.EDIT_LISTING_SCREEN) as! EditListingViewController
        editListingScreen.listingDetail = listingDetail
        editListingScreen.listingDetailRefreshDelegate = self
        self.navigationController?.pushViewController(editListingScreen, animated: false)
    }
    
    func listingDetailScreen(id:Int){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.LISTING_DETAIL_BOARD, bundle: nil)
        let listingDetailScreen = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.Listing_Detail_Screen) as! ListDetailViewController
        listingDetailScreen.itemId = id
        self.navigationController?.pushViewController(listingDetailScreen, animated: false)
    }
    
    func getListingDetail(id:Int) {
        self.listingDetailScreen(id:id)
    }
    
    func getListingDetailEdit(id:Int) {
        self.view.makeToastActivity(.center)
        ProfileViewModel().getListingDetail(accessToken: AppDefaults.getAccessToken(), id: id, { (response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<ListingDetail> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let listingData = data.data!
                    self.editListingScreen(listingDetail: listingData)
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print(error)
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func userBynViewController() {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.USER_BYN_BOARD, bundle: nil)
        let userBynViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.USER_BYN_SCREEN) as! UserBynViewController
        userBynViewController.listNeedsRefresh = true
        self.navigationController?.pushViewController(userBynViewController, animated: false)
    }
    
    func openMySaleAndMyPurchasesScreen(type:Int) {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.INVENTORY_TYPE_BOARD, bundle: nil)
        let  inventoryTypeViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.INVENTORY_TYPE_SCREEN) as! InventoryTypeViewController
        inventoryTypeViewController.TYPE = type
        navigationController?.pushViewController(inventoryTypeViewController, animated: true)
    }
    
    func getAllAlerts() {
        alertViewModel.getAlertsAll(accessToken: getAccessToken(), page: 1, { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Setting> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    var isUnread:Int = 0
                    let alertsdata = data.data
                    if let alerts = alertsdata?.items {
                        for i in 0..<alerts.count{
                            if alerts[i].isUnread!{
                                isUnread += 1
                            }
                        }
                        
                        if let tabItems = self.tabBarController?.tabBar.items {
                            // In this case we want to modify the badge number of the third tab:
                            let tabItem = tabItems[3]
                            if isUnread > 0{
                                tabItem.badgeValue = String(isUnread)
                            }
                        }
                    }
                    
                }
            } else {
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
        self.isAlertsApiCalled = false
    }
    
    func searchUsersScreen(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.FOLLOW_FOLLOWING_BOARD, bundle: nil)
        let followFollowingViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.FOLLOWERS_FOLLOWING) as! FollowersFollowingViewController
        followFollowingViewController.TYPE = AppConstants.TYPE_SEARCH_USERS
        self.navigationController?.pushViewController(followFollowingViewController, animated: false)
    }
    
    func reportAnIssueSuccessMessageDialog(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.SUCCESS_MESSAGE_DIALOG_BOARD, bundle: nil)
        let successViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.SUCCESS_MESSAGE_DIALOG) as! SuccessMessageViewController
        successViewController.modalPresentationStyle = .overFullScreen
        successViewController.TYPE = AppConstants.TYPE_REPORT_AN_ISSUE
        self.navigationController?.present(successViewController, animated: false, completion: nil)
    }
    
    func searchButtonClickedForFeedAll(){
        AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_FEED_ALL = false
    }
    
    func searchButtonClickedForFeedListing(){
        AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_FEED_LISTING = false
    }
    
    func searchButtonClickedForFeedPost(){
        AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_FEED_POST = false
    }
    
    func searchButtonClickedForBrowseAll(){
        AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_BROWSE_ALL = false
    }
    
    func searchButtonClickedForBrowseListing(){
        AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_BROWSE_LISTING = false
    }
    
    func searchButtonClickedForBrowsePost(){
        AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_BROWSE_POST = false
    }
    
    func getAllPromotions() {
        FeedViewModel().getPromotions(accessToken: getAccessToken(), { (response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[Promotions]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let promotions = data.data!
                    AppConstants.PROMOTIONS_ALL_LIST = promotions
                    AppConstants.isPromotionsAllListLoaded = true
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print(error)
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func trackSearch(trackSearchOfUser:TrackSearchOfUser) {
        ProfileViewModel().trackSearch(accessToken: getAccessToken(), data: trackSearchOfUser, { (response) in
            if response.response?.statusCode == 200 || response.response?.statusCode == 201{
                print("track search")
                self.trackSearchDictionary.removeAll()
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print(error)
                }
                self.view.hideToastActivity()
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func convertIntoJSONString(arrayObject: [String: [String]]) -> String? {
        do {
            let jsonData: Data = try JSONSerialization.data(withJSONObject: arrayObject, options: [])
            if  let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue) {
                return jsonString as String
            }
        } catch let error as NSError {
            print("Array convertIntoJSON - \(error.description)")
        }
        return nil
    }
    
    func trackUsersInBase(token: String, data: UserAuth) {
        let userAuth = data
        AuthViewModel().trackUsers(accessToken: token,data:userAuth,{ (response) in
            if response.response?.statusCode == 200 || response.response?.statusCode == 201{
                print("login")
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print(error)
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    public func presentViewControllerForOnBoarding(storyboardName: String, viewControllerIdentifier: String) {
        let storyBoard : UIStoryboard = UIStoryboard(name: storyboardName, bundle:nil)
        let ViewController = storyBoard.instantiateViewController(withIdentifier: viewControllerIdentifier)
        ViewController.modalPresentationStyle = .overFullScreen
        if #available(iOS 13.0, *) {
            ViewController.isModalInPresentation = true
        } else {
            // Fallback on earlier versions
        }
        self.present(ViewController, animated:false, completion:nil)
    }
    
    func openUserProfile(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.PROFILE_BOARD, bundle: nil)
        let profileViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.PROFILE) as! ProfileViewController
        self.navigationController?.pushViewController(profileViewController, animated: false)
    }
    
    func sellerPolicyScreen(){
        let sellerPolicyController = UIStoryboard(name: AppConstants.Storyboard.SELLER_POLICY_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.SELLER_POLICY_SCREEN) as! SellerPolicyViewController
        sellerPolicyController.modalPresentationStyle = .overFullScreen
        self.present(sellerPolicyController, animated: false, completion: nil)
    }
    
    func inAppWebBrowserScreen(link:String?,title:String?){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.IN_APP_WEBVIEW_BOARD, bundle: nil)
        let videoViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.VIDEO_SCREEN) as! VideoViewController
        videoViewController.modalPresentationStyle = .overFullScreen
        videoViewController.videoLink = link
        videoViewController.linkTitle = title
        navigationController?.present(videoViewController, animated: false,completion: nil)
    }
    
    func postDetailScreen(id:Int){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.POST_DETAIL_BOARD, bundle: nil)
        let postDetailViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.POST_DETAIL_SCREEN) as! PostDetailViewController
        postDetailViewController.id = id
        self.navigationController?.pushViewController(postDetailViewController, animated: true)
    }
    
    func disputeDetailScreen(id:Int,type:Int){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.DISPUTE_STORYBOARD, bundle: nil)
        let disputeViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.DISPUTE_DETAILS_SCREEN) as! DisputeDetailsViewController
        disputeViewController.disputeId = id
        disputeViewController.TYPE = type
        self.navigationController?.pushViewController(disputeViewController, animated: false)
    }
    
    func offerHistoryScreen(id:Int,initiatorUserId:Int){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.OFFER_DETAIL_STORYBOARD, bundle: nil)
        let  offerDetailViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.OFFERS_DETAIL_SCREEN) as! OfferDetailViewController
        offerDetailViewController.listingId = id
        offerDetailViewController.initiatorUserId = initiatorUserId
        navigationController?.pushViewController(offerDetailViewController, animated: true)
    }
    
    func salePurchaseDetailScreen(id:Int,type:Int){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.SALE_PURCHASE_DETAIL_BOARD, bundle:nil)
        let detailViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.SALE_PURCHASE_DETAIL_SCREEN) as! SalePurchaseDetailViewController
        detailViewController.id = id
        detailViewController.TYPE = type
        self.navigationController?.pushViewController(detailViewController, animated:false)
    }
    
    func notificationRedirect(notificationRedirectTypeId:Int,notificationRedirectType:String,notificationRedirectId:Int,idType:String,initiatorUserId:Int){
        
        switch notificationRedirectTypeId {
        case AppConstants.NOTIFICATION_REDIRECT_TYPE_NONE:
            print("type_none")
        case AppConstants.NOTIFICATION_REDIRECT_TYPE_NEWS_UPDATE:
            print("news_update")
        case AppConstants.NOTIFICATION_REDIRECT_TYPE_USER_PROFILE:
            self.redirectUser(notificationRedirectId: notificationRedirectId)
        case AppConstants.NOTIFICATION_REDIRECT_TYPE_LISTING:
            self.redirectToListingDetail(notificationRedirectId: notificationRedirectId)
        case AppConstants.NOTIFICATION_REDIRECT_TYPE_POST:
            self.redirectToPostDetail(notificationRedirectId: notificationRedirectId)
        case AppConstants.NOTIFICATION_REDIRECT_TYPE_DISPUTE:
            self.redirectToDispute(notificationRedirectId:notificationRedirectId,type:0)
        case AppConstants.NOTIFICATION_REDIRECT_TYPE_SALE_DETAIL:
            self.redirectToSaleDetail(notificationRedirectId:notificationRedirectId,type:AppConstants.TYPE_SALE)
        case AppConstants.NOTIFICATION_REDIRECT_TYPE_PURCHASE_DETAIL:
            self.redirectToSaleDetail(notificationRedirectId:notificationRedirectId,type:AppConstants.TYPE_PURCHASE)
        case AppConstants.NOTIFICATION_REDIRECT_TYPE_OFFER_HISTORY:
            self.redirectToOfferHistory(notificationRedirectId: notificationRedirectId, initiatorUserId: initiatorUserId)
        default:
            print("none")
        }
        
    }
    
    func redirectUser(notificationRedirectId:Int){
        if notificationRedirectId == AppDefaults.getUserId(){
            self.openUserProfile()
        }else{
            self.openProfile(id: notificationRedirectId,username:"")
        }
    }
    
    func redirectToListingDetail(notificationRedirectId:Int){
        self.getListingDetail(id: notificationRedirectId)
    }
    
    func redirectToPostDetail(notificationRedirectId:Int){
        self.postDetailScreen(id: notificationRedirectId)
    }
    
    func redirectToDispute(notificationRedirectId:Int,type:Int){
        self.disputeDetailScreen(id:notificationRedirectId,type:type)
    }
    
    func redirectToSaleDetail(notificationRedirectId:Int,type:Int){
        self.salePurchaseDetailScreen(id:notificationRedirectId,type:type)
    }
    
    func redirectToPurchaseDetail(notificationRedirectId:Int,type:Int){
        self.salePurchaseDetailScreen(id:notificationRedirectId,type:type)
    }

    func redirectToOfferHistory(notificationRedirectId:Int,initiatorUserId:Int){
        self.offerHistoryScreen(id: notificationRedirectId,initiatorUserId:initiatorUserId)
    }
    
    func deleteCachedVideos() throws {
        for url in self.videoUrls{
            try FileManager.default.removeFileIfNecessary(at: url)
        }
    }
    
    func getVisibleViewController(_ rootViewController: UIViewController?) -> UIViewController? {

        var rootVC = rootViewController
        if rootVC == nil {
            rootVC = UIApplication.shared.keyWindow?.rootViewController
        }

        if rootVC?.presentedViewController == nil {
            return rootVC
        }

        if let presented = rootVC?.presentedViewController {
            if presented.isKind(of: UINavigationController.self) {
                let navigationController = presented as! UINavigationController
                return navigationController.viewControllers.last!
            }

            if presented.isKind(of: UITabBarController.self) {
                let tabBarController = presented as! UITabBarController
                return tabBarController.selectedViewController!
            }

            return getVisibleViewController(presented)
        }
        return nil
    }
    
}
