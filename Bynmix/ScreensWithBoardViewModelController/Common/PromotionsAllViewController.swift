import UIKit

class PromotionsAllViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var feedViewModel = FeedViewModel()
    var promotionsHandler = PromotionsHandler()
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = promotionsHandler
        tableView.dataSource = promotionsHandler
        getPromotions()
        tableView.estimatedRowHeight = 42
        tableView.rowHeight = UITableView.automaticDimension
    }
    
    func getPromotions() {
        feedViewModel.getPromotions(accessToken: AppDefaults.getAccessToken(), { (response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[Promotions]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let promotions = data.data!
                    self.promotionsHandler.promotions = promotions
                    self.tableView.reloadData()
                    self.tableView.isHidden = false
                    self.resizePromotionsTableView(promotions:promotions)
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func resizePromotionsTableView(promotions:[Promotions]){
        if promotions.count == 0{
            tableViewHeight.constant = 0
        }else{
            
            UIView.animate(withDuration: 0, animations: {
                self.tableView.layoutIfNeeded()
            }) { (complete) in
                var heightOfTableView: CGFloat = 0.0
                let cells = self.tableView.visibleCells
                for cell in cells {
                    heightOfTableView += cell.frame.height
                }
                self.tableViewHeight.constant = heightOfTableView
            }
        }
    }
    
    
}
