import UIKit

class AddPostTagsViewController: BaseViewController {
    
    @IBOutlet var coverImageView: UIImageView!
    @IBOutlet var whatIsThisLabel: UILabel!
    
    var image:UIImage?
    var onImageChangeDelegate: OnImageChangeProtocol?
    var type:Int?
    var TYPE:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.CREATE_POST_TITLE
        coverImageView.layer.borderWidth = 1
        coverImageView.layer.borderColor = UIColor.c3B3B3B.cgColor
        if image != nil {
            coverImageView.image = image
            coverImageView.contentMode = .scaleToFill
        }
        setTextForAboutTagsLabel()
    }
    
    @IBAction func onTagsInfoAction(_ sender: UITapGestureRecognizer) {
        tagsDemoDialog()
    }
    
    @IBAction func addProductTagsAction(_ sender: UIButton) {
        addTagsScreen()
    }
    
    func addTagsScreen(){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.ADD_POST_TAGS_STORYBOARD, bundle: nil)
        let addPostTagsViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.ADD_TAGS_SCREEN) as! AddTagsViewController
        addPostTagsViewController.TYPE = self.TYPE
        addPostTagsViewController.image = self.image
        self.navigationController?.pushViewController(addPostTagsViewController, animated: false)
    }
    
    @IBAction func onPostDetailsAction(_ sender: UIButton) {
        if image != nil {
            if TYPE == AppConstants.TYPE_EDIT_TAGS{
                openEditPostScreen()
            }else if self.TYPE == AppConstants.TYPE_ADD_TAGS_RESELECT_IMAGE{
                self.navigationController?.popViewController(animated: false)
            }else{
                openCreatePostScreen()
            }
        }
    }
    
    func openCreatePostScreen(){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.CREATE_POST_BOARD, bundle: nil)
        let createPostViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.CREATE_POST_INFO_SCREEN) as! CreatePostViewController
        
        createPostViewController.image = self.image
        self.navigationController?.pushViewController(createPostViewController, animated: false)
    }
    
    func openEditPostScreen(){
        onImageChangeDelegate?.onImageChange(image: self.image!)
        self.navigationController?.popViewController(animated: false)
    }
    
    func setTextForAboutTagsLabel(){
        let text = AppConstants.Strings.WHAT_THIS_TEXT + " "
        let textAttributed = AppConstants.Strings.WHAT_THIS_TEXT
        let range = (text as NSString).range(of: textAttributed)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 12), range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cBB189C, range: range)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range)
        whatIsThisLabel.attributedText = attributedString
    }
    
}
