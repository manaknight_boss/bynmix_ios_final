import UIKit

class CreateNavigationViewController: UINavigationController {

    var createViewModel = CreateViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let modelName = UIDevice.modelName
        if modelName == "iPhone 7" || modelName == "iPhone 7 Plus" || modelName == "iPhone 8" || modelName == "iPhone 8 Plus"{
            self.tabBarItem.imageInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            self.title = nil
        }else{
            self.tabBarItem.imageInsets = UIEdgeInsets(top: 3, left: 0, bottom: -6, right: 0)
            self.title = nil
        }
    }
    
    var viewControllerType:Int? {
        didSet {
            if viewControllerType != -1 {
                let mainStoryBoard = UIStoryboard(name: AppConstants.Storyboard.CREATE_POST_ADD_COVER_BOARD, bundle: nil)
                let createPostViewController = mainStoryBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.CREATE_POST_SCREEN) as! CreatePostAddCoverViewController

                if viewControllerType == AppConstants.CREATE_TYPE_POST {
                    self.setViewControllers([createPostViewController], animated: false)
                } else {
                    checkSellerStatus()
                }
                viewControllerType = -1
            }
        }
    }
    
    func listingScreen(){
        let mainStoryBoard = UIStoryboard(name: AppConstants.Storyboard.CREATE_LISTING_BOARD, bundle: nil)
        let createListingViewController = mainStoryBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.CREATE_LISTING_SCREEN) as! CreateListingViewController
        self.pushViewController(createListingViewController, animated: false)
    }
    
    private func checkSellerStatus() {
//        let placeholderViewController = UIViewController()
//        placeholderViewController.view.backgroundColor = UIColor.white
//        self.setViewControllers([placeholderViewController], animated: false)
        self.view.makeToastActivity(.center)
        createViewModel.checkSellerStatus(accessToken: getAccessToken(), {(response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<ListingCheck> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    BaseViewController().showMessageDialog(message: error)
                } else {
                    let checkStatus = data.data!
                    
                    if (checkStatus.isNewSeller!) {
                        let storyboard = UIStoryboard(name: AppConstants.Storyboard.SELLER_WELCOME_BOARD, bundle: nil)
                        let  sellerWelcomeViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.SELLER_WELCOME_SCREEN) as! SellerWelcomeViewController
                        AppDefaults.setSellerSetup(isSetupDone: false)
                        self.pushViewController(sellerWelcomeViewController, animated: false)
                    } else {
                            self.listingScreen()
                    }
                }
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    BaseViewController().showMessageDialog(message: error)
                    self.view.makeToast(error)
                }
                self.view.hideToastActivity()
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                BaseViewController().showInternetError()
            }
        })
    }
    
    public func getAccessToken() -> String {
        return AppDefaults.getAccessToken()
    }
    
}
