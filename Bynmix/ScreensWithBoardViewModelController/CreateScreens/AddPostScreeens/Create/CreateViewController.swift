import UIKit

class CreateViewController: BaseViewController {

    @IBOutlet var outerView: UIView!
    var createClickDelegate: CreateClickProtocol?
    override func viewDidLoad() {
        super.viewDidLoad()
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
    }
    
    @objc func tapDetected() {
        self.dismiss(animated: false, completion: nil)
        createClickDelegate!.onCreateClick(type: AppConstants.CREATE_TYPE_NONE)
    }
    
    @IBAction func CreatePostAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
        createClickDelegate!.onCreateClick(type: AppConstants.CREATE_TYPE_POST)
    }
    
    
    @IBAction func createListingAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
        createClickDelegate!.onCreateClick(type: AppConstants.CREATE_TYPE_LISTING)
    }
    
}
