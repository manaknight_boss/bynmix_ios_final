import UIKit

class CreatePostViewController: BaseViewController,onPostTypeChangeProtocol,onTagsClickProtocol {
    func onTagsClick(type: Int) {
        if type == 1{
            UIView.animate(withDuration: 0.5) {
                self.topConstraint.constant = -250
                self.view.layoutIfNeeded()
            }
            
        }else if type == 3{
            UIView.animate(withDuration: 0.5) {
                self.topConstraint.constant = 15
                self.view.layoutIfNeeded()
            }
            
//            let bottomOffset = CGPoint(x: 0, y: self.mainScrollView.contentSize.height - self.mainScrollView.bounds.size.height)
//            self.mainScrollView.setContentOffset(bottomOffset, animated: true)
        }
    }
    
    func onPostTypeChange(type: Int) {
        if type == AppConstants.TYPE_VIDEO_POST{
            self.postTypeLabel.text = AppConstants.BLOG_POST_TEXT
            createButtonAction()
        }else if type == AppConstants.TYPE_BLOG_POST{
            self.postTypeLabel.text = AppConstants.VIDEO_POST_TEXT
            createButtonAction()
        }
    }
    
    @IBOutlet var titleTextField: UITextField!
    @IBOutlet var postTypeLabel: UILabel!
    @IBOutlet var postLinkTextField: UITextField!
    @IBOutlet var shortDescTextView: UITextView!
    @IBOutlet var collectionViewLayout: UICollectionView!
    @IBOutlet var createButton: UIButton!
    @IBOutlet var topConstraint: NSLayoutConstraint!
    @IBOutlet var buttonActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var savingPostView: UIView!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    
    var type_post = 0
    var selectedPostType = 0
    var postTypeSelect = [PostType]()
    let addTagsHandler = AddTagsHandler()
    var image:UIImage?
    let createViewModel = CreateViewModel()
    var TYPE:Int?
    var dots:[Dots]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonActivityIndicator.isHidden = true
        navigationItem.title = AppConstants.CREATE_A_POST_TITLE
        //postTypeSelect = AppConstants.StaticLists.postType
        shortDescTextView.text = AppConstants.Strings.SHORT_DESCRIPTION_POST_TEXT
        shortDescTextView.textColor = UIColor.cCDCDCD
        shortDescTextView.font = UIFont.fontRegular(size: 18)
        collectionViewLayout.delegate = addTagsHandler
        collectionViewLayout.dataSource = addTagsHandler
        addTagsHandler.dataObject.append(AddTags(tag:"", type:1))
        collectionViewLayout!.collectionViewLayout = FlowLayoutHelper()
        hideKeyboardWhenTappedAround()
        addTagsHandler.onTagsClickDelegate = self
        initalTopConstraintValueOnTap()
        getPostType()
    }
    
    func initalTopConstraintValueOnTap() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(topConstraintInitialValue))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func topConstraintInitialValue() {
        view.endEditing(true)
        UIView.animate(withDuration: 0.5) {
            self.topConstraint.constant = 15
            self.view.layoutIfNeeded()
        }
        
        self.addTagsHandler.enterTagCollectionViewCell?.headerView.isHidden = false
        self.addTagsHandler.enterTagCollectionViewCell?.dataView.isHidden = true
        self.addTagsHandler.enterTagCollectionViewCell?.addTagTextField.text = ""
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 150
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case titleTextField:
            postLinkTextField.becomeFirstResponder()
        case postLinkTextField:
            shortDescTextView.becomeFirstResponder()
        case shortDescTextView:
            shortDescTextView.resignFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if shortDescTextView.text == AppConstants.Strings.SHORT_DESCRIPTION_POST_TEXT {
            shortDescTextView.text = nil
            shortDescTextView.textColor = UIColor.black
            shortDescTextView.font = UIFont.fontRegular(size: 18)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if shortDescTextView.text.isEmpty {
            shortDescTextView.text = AppConstants.Strings.SHORT_DESCRIPTION_POST_TEXT
            shortDescTextView.textColor = UIColor.cCDCDCD
            shortDescTextView.font = UIFont.fontRegular(size: 18)
        }
    }
    
    func getPostType(){
        createViewModel.getPostType(accessToken: getAccessToken(), { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[PostType]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let postType = data.data!
                    self.postTypeSelect = postType
                    
                    self.showScreenAndStopLoader()
                }
            }else{
                self.stopLoader()
                print("error")
            }
        } , { (error) in
            self.stopLoader()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func showScreenAndStopLoader(){
        self.screenActivityIndicator.stopAnimating()
        self.mainScrollView.isHidden = false
    }
    
    func stopLoader(){
        self.screenActivityIndicator.stopAnimating()
    }
    
    @IBAction func postTypeAction(_ sender: UITapGestureRecognizer) {
        showDropDown(tag: type_post, title: AppConstants.Strings.SELECT_A_POST_TYPE, dataSourse: self, delegate: self) { (row) in
            self.postTypeLabel.textColor = UIColor.black
            self.postTypeLabel.text = self.postTypeSelect[row].name
            self.selectedPostType = self.postTypeSelect[row].postTypeId ?? 0
        }
    }
    
    @IBAction func createButtonAction(_ sender: UIButton) {
        
        createButtonAction()
    }
    
    func createButtonAction(){
        let titleTextField = self.titleTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let postTypeLabel = self.postTypeLabel.text
        let postLink = self.postLinkTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let shortDesc = self.shortDescTextView.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let addtagsCount = self.addTagsHandler.dataObject.count
        
        if titleTextField == ""{
            let text:String = AppConstants.Strings.POST_TITLE_REQUIRED
            let textColor:String = AppConstants.Strings.POST_TITLE
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        if postTypeLabel == AppConstants.Strings.SELECT_A_POST_TYPE{
            let text:String = AppConstants.Strings.POST_TYPE_REQUIRED
            let textColor:String = AppConstants.Strings.POST_TYPE
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        if postLink == ""{
            let text:String = AppConstants.Strings.POST_LINK_REQUIRED
            let textColor:String = AppConstants.Strings.POST_LINK
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        if  !((postLink?.contains(AppConstants.HTTPS))! || ((postLink?.contains(AppConstants.WWW_TEXT))!) || (postLink?.contains(AppConstants.HTTP))!){
            let text:String = AppConstants.Strings.POST_LINK_START_WITH
            let textColor:String = AppConstants.Strings.POST_LINK
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        
        if shortDesc == "" || shortDesc == AppConstants.Strings.SHORT_DESCRIPTION_POST_TEXT{
            let text:String = AppConstants.Strings.SHORT_DESC_REQUIRED
            let textColor:String = AppConstants.Strings.SHORT_DESCRIPTION_TEXT
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        if addtagsCount == 1{
            let text:String = AppConstants.Strings.TAG_REQUIRED
            let textColor:String = AppConstants.Strings.TAG
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        createPost()
    }
    
    func createPost() {
        let title = titleTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        var postLink = postLinkTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let shortDesc = shortDescTextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
        let tagsArray = self.addTagsHandler.dataObject
        var tags:[String] = []
        let isYoutubeLink = ValidationHelper.youTubeURLmatches(link: postLink!)
        
        for i in 1..<tagsArray.count{
            tags.append(tagsArray[i].tag!)
            
            let itemsArray = tags
            let searchToSearch = tagsArray[i].tag!

            let filteredStrings = itemsArray.filter({(item: String) -> Bool in

                let stringMatch = item.lowercased().range(of: searchToSearch.lowercased())
                return stringMatch != nil ? true : false
            })
            print("filteredStrings:",filteredStrings)

            if (filteredStrings as NSArray).count > 0
            {
                //Record found
                //MARK:- You can also print the result and can do any kind of work with them
            }
            else
            {
                //Record Not found
            }
            
        }
        
        if !(postLink?.contains("http"))!{
            postLink = AppConstants.HTTP + postLink!
        }
        
        if self.selectedPostType == AppConstants.TYPE_BLOG_POST && isYoutubeLink{
            
            let storyBoard = UIStoryboard(name: AppConstants.Storyboard.POST_CHECK_DIALOG_BOARD, bundle: nil)
            let postCheckViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.POST_CHECK_DIALOG) as! PostCheckDialogViewController
            postCheckViewController.delegate = self
            postCheckViewController.TYPE = AppConstants.TYPE_BLOG_POST
            self.navigationController?.present(postCheckViewController, animated: false)
            
        }else if self.selectedPostType == AppConstants.TYPE_VIDEO_POST && !isYoutubeLink{
            
            let storyBoard = UIStoryboard(name: AppConstants.Storyboard.POST_CHECK_DIALOG_BOARD, bundle: nil)
            let postCheckViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.POST_CHECK_DIALOG) as! PostCheckDialogViewController
            postCheckViewController.delegate = self
            postCheckViewController.TYPE = AppConstants.TYPE_VIDEO_POST
            self.navigationController?.present(postCheckViewController, animated: false)
            
        }else{
            
            if self.dots == nil{
                self.dots = []
            }
            
            let createPostData = CreatePostRequest(title: title!, postType: self.selectedPostType, link: postLink!, shortDescription: shortDesc, tags: tags, postDots: self.dots!)
            self.buttonActivityIndicator.isHidden = false
            self.buttonActivityIndicator.startAnimating()
            self.createButton.isEnabled = false
            self.savingPostView.isHidden = false
            createViewModel.createPost(accessToken: getAccessToken(), data: createPostData, { (response) in
                if response.response?.statusCode == 201 {
                    let data: ApiResponse<Int> = Utils.convertResponseToData(data: response.data!)
                    let postId:Int = data.data!
                    Dots.dots.removeAll()
                    AppDefaults.setTags(tagValue: 0)
                    self.uploadImage(id:postId)
                }
            } , { (error) in
                if error.statusCode == AppConstants.NO_INTERNET {
                    self.showInternetError()
                }
            })
        }
        
    }
    
    func uploadImage(id:Int){
        createViewModel.uploadPostImage(accessToken: getAccessToken(), id: id,image:image!, { (response) in
            self.buttonActivityIndicator.isHidden = true
            self.buttonActivityIndicator.stopAnimating()
            self.createButton.isEnabled = true
            self.savingPostView.isHidden = true
            if response.response?.statusCode == 201 {
                let data:  ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.trackPost(postId:id)
                    self.onCreateClick(type: AppConstants.CLEAR_STACK_OF_CREATE_SCREEN_TYPE)
                }
            }else{
                let data:  ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.buttonActivityIndicator.isHidden = true
                self.buttonActivityIndicator.stopAnimating()
                self.createButton.isEnabled = true
                self.savingPostView.isHidden = true
            }
        } , { (error) in
            self.buttonActivityIndicator.isHidden = true
            self.buttonActivityIndicator.stopAnimating()
            self.createButton.isEnabled = true
            self.savingPostView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            } 
        })
    }
    
    func trackPost(postId:Int){
        self.createViewModel.trackPosts(accessToken: self.getAccessToken(), postId: postId, {(response) in
            
            if response.response?.statusCode == 201 || response.response?.statusCode == 200{
                print("tracked post")
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.buttonActivityIndicator.isHidden = true
                self.buttonActivityIndicator.stopAnimating()
                self.createButton.isEnabled = true
                self.savingPostView.isHidden = true
            }
        } , { (error) in
            self.buttonActivityIndicator.isHidden = true
            self.buttonActivityIndicator.stopAnimating()
            self.createButton.isEnabled = true
            self.savingPostView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
}

extension CreatePostViewController:UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return postTypeSelect.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return postTypeSelect[row].name
    }
}
