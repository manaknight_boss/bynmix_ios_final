import UIKit
import Mantis

class CreatePostAddCoverViewController: BaseViewController {
    
    let imagePickerManager = ImagePickerManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.CREATE_POST_TITLE
//        let backBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.BACK_BUTTON), style: .plain, target: self, action: #selector(onBackButtonPressedAction))
//        self.navigationItem.leftBarButtonItem  = backBarButton
    }
    
//    @objc func onBackButtonPressedAction(){
//        if self.navigationController?.viewControllers.count ?? 0 > 0{
//            self.navigationController?.popViewController(animated: false)
//        }else{
//            self.tabBarController?.selectedIndex = 0
//        }
//    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationItem.hidesBackButton = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.navigationItem.hidesBackButton = false
    }
    
    func openAddPostTagsScreen(image:UIImage){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.ADD_POST_TAGS_BOARD, bundle: nil)
        let addPostTagsViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.ADD_POST_TAGS_SCREEN) as! AddPostTagsViewController
        addPostTagsViewController.TYPE = AppConstants.TYPE_ADD_TAGS
        addPostTagsViewController.image = image
        self.navigationController?.pushViewController(addPostTagsViewController, animated: false)
    }
    
    @IBAction func onCoverPhotoAction(_ sender: UITapGestureRecognizer) {
        //getImage()
        pickImage()
    }
    
    func getImage(){
        imagePickerManager.pickImage(self,cameraTitle: AppConstants.CAMERA,galleryTitle: AppConstants.GALLERY){ image in
            self.openAddPostTagsScreen(image:image)
        }
        
    }
    
}

extension CreatePostAddCoverViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropViewControllerDelegate {
    
    func cropViewControllerDidCrop(_ cropViewController: CropViewController, cropped: UIImage, transformation: Transformation) {
        //set image
        self.openAddPostTagsScreen(image:cropped)
        cropViewController.dismiss(animated: false)
    }
    
    func cropViewControllerDidCancel(_ cropViewController: CropViewController, original: UIImage) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    @objc public func pickImage() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let defaultAction = UIAlertAction(title: AppConstants.GALLERY, style: .default) { (action) in
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        let camera = UIAlertAction(title: AppConstants.CAMERA, style: .default) { (action) in
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        
        let cancel = UIAlertAction(title: AppConstants.CANCEL, style: .destructive)
        
        alertController.addAction(defaultAction)
        alertController.addAction(camera)
        alertController.addAction(cancel)
        alertController.modalPresentationStyle = .popover
        
        present(alertController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage) else { return }

        let config = Mantis.Config()
        
        let cropViewController = Mantis.cropViewController(image: image, config: config)
        cropViewController.modalPresentationStyle = .fullScreen
        cropViewController.delegate = self
        cropViewController.config.showRotationDial = false
        cropViewController.config.cropToolbarConfig.toolbarButtonOptions = .reset
        cropViewController.config.cropVisualEffectType = .light
        cropViewController.config.presetFixedRatioType = .alwaysUsingOnePresetFixedRatio(ratio: 16.0 / 16.0)
    
        picker.dismiss(animated: true, completion: {
            self.present(cropViewController, animated: true,completion: nil)
        })
        
    }
    
}

