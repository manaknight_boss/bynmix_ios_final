import UIKit

class CreatePostNavigationViewController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let createStoryBoard = UIStoryboard(name: AppConstants.Storyboard.CREATE_BOARD, bundle: nil)
        let createViewController = createStoryBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.CREATE_SCREEN) as! CreateViewController
        createViewController.modalPresentationStyle = .overCurrentContext
        self.present(createViewController, animated: false,completion: nil)
        
        let createPostAddCoverStoryBoard = UIStoryboard(name: AppConstants.Storyboard.CREATE_POST_ADD_COVER_BOARD, bundle: nil)
        let createPostViewController = createPostAddCoverStoryBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.CREATE_POST_SCREEN) as! CreatePostAddCoverViewController
        createPostViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        createPostViewController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(createPostViewController, animated: false,completion: nil)
    }

}
