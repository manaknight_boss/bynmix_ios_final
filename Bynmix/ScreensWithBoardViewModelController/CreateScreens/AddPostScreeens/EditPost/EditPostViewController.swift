import UIKit

class EditPostViewController:  BaseViewController,PostImageCloseClickProtocol,onTagsClickProtocol,onPostTypeChangeProtocol, OnImageChangeProtocol , DotsUpdateProtocol {
    
    func onDotsUpdate(dots: [Dots], removedDots: [Dots]) {
        self.dotsList = dots
        let dotsCount = dots.count
        self.dotsButton.setTitle(AppConstants.Strings.EDIT_POST_TAGS + " " + AppConstants.BRACKET_ON + "\(dotsCount )" + AppConstants.BRACKET_OFF, for: .normal)
        if removedDots.count > 0 {
            self.removedDots = removedDots
            for i in 0..<self.removedDots.count {
                self.removedDots[i].isDeleted = true
            }
        }
    }
    func onImageChange(image: UIImage) {
        self.image = image
        userCoverImageView.image = self.image
    }
    
    
    func onPostTypeChange(type: Int) {
        if type == AppConstants.TYPE_VIDEO_POST{
            self.postTypeLabel.text = AppConstants.BLOG_POST_TEXT
            createButtonAction()
        }else if type == AppConstants.TYPE_BLOG_POST{
            self.postTypeLabel.text = AppConstants.VIDEO_POST_TEXT
            createButtonAction()
        }
    }
    
    func onTagsClick(type: Int) {
        if type == 1{
            UIView.animate(withDuration: 0.5) {
                self.topConstraint.constant = -500
                self.view.layoutIfNeeded()
            }
            self.isScreenonLoad = false
        }else if type == 3{
            
            let seconds = 0.4
            DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
                
                UIView.animate(withDuration: 0.4) {
                    self.topConstraint.constant = 15
                    self.view.layoutIfNeeded()
                }
                
                if self.isScreenonLoad{
                    let topOffset = CGPoint(x: 0, y: -self.screenScrollView.contentInset.top)
                    self.screenScrollView.setContentOffset(topOffset, animated: false)
                }else{
                    let bottomOffset = CGPoint(x: 0, y: self.screenScrollView.contentSize.height - self.screenScrollView.bounds.size.height)
                    self.screenScrollView.setContentOffset(bottomOffset, animated: false)
                }
                
            }
        }
    }
    
    func postImageCloseClick() {
        self.isCurrentPhotoRemoved = true
        editPhotoButton.isHidden = true
        userCoverImageView.image = UIImage(named: AppConstants.Images.DOTS_IMAGE_VIEW)
        dotsButton.isHidden = true
    }
    
    @IBOutlet var titleTextField: UITextField!
    @IBOutlet var postTypeLabel: UILabel!
    @IBOutlet var postLinkTextField: UITextField!
    @IBOutlet var shortDescTextView: UITextView!
    @IBOutlet var collectionViewLayout: UICollectionView!
    @IBOutlet var createButton: UIButton!
    @IBOutlet var topConstraint: NSLayoutConstraint!
    @IBOutlet var userCoverImageView: UIImageView!
    @IBOutlet var dotsButton: UIButton!
    @IBOutlet var editPhotoButton: UIButton!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var screenScrollView: UIScrollView!
    @IBOutlet var savingPostView: UIView!
    @IBOutlet var keyboardHeightLayoutConstraint: NSLayoutConstraint!
    
    var type_post = 0
    var selectedPostType = 0
    var postTypeSelect = [PostType]()
    let addTagsHandler = AddTagsHandler()
    var image:UIImage?
    let createViewModel = CreateViewModel()
    var feed:Feed?
    let imagePickerManager = ImagePickerManager()
    var isImageDeleted:Bool = false
    var dotsList:[Dots]?
    var editPostImage:String?
    var dotsInfo:Feed?
    var TYPE:Int?
    var removedDots: [Dots] = []
    var TYPE_IMAGE_SELECT :Int?
    var isScreenonLoad = true
    var isCurrentPhotoRemoved:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.EDIT_POST_TITLE
        //postTypeSelect = AppConstants.StaticLists.postType
        shortDescTextView.text = AppConstants.Strings.SHORT_DESCRIPTION_POST_TEXT
        shortDescTextView.textColor = UIColor.cCDCDCD
        shortDescTextView.font = UIFont.fontRegular(size: 18)
        collectionViewLayout.delegate = addTagsHandler
        collectionViewLayout.dataSource = addTagsHandler
        addTagsHandler.dataObject.append(AddTags(tag:"", type:1))
        collectionViewLayout!.collectionViewLayout = FlowLayoutHelper()
        hideKeyboardWhenTappedAround()
        addTagsHandler.onTagsClickDelegate = self
        initalTopConstraintValueOnTap()
        getData()
        keyboardRequired(topConstraint: topConstraint)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if self.TYPE == AppConstants.TYPE_ADD_TAGS_EDIT_SCREEN{
            let dotsCount = dotsList?.count
            self.dotsButton.setTitle(AppConstants.Strings.EDIT_POST_TAGS + " " + AppConstants.BRACKET_ON + "\(dotsCount ?? 0)" + AppConstants.BRACKET_OFF, for: .normal)
        }else if self.TYPE_IMAGE_SELECT == AppConstants.TYPE_ADD_TAGS_RESELECT_IMAGE{
            if self.image == nil{
                let coverSelectImage = UIImage(named: AppConstants.Images.DOTS_IMAGE_VIEW)
                if let imageUrl = feed!.photoUrl{
                    userCoverImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: coverSelectImage)
                    self.editPostImage = imageUrl
                }
                self.editPhotoButton.isHidden = true
            }else{
                self.userCoverImageView.image  = self.image
                self.editPhotoButton.isHidden = false
            }
            
            if dotsList?.count == 0{
                dotsButton.setTitle(AppConstants.Strings.ADD_POST_PRODUCT_TAGS, for: .normal)
            }else{
                let dotsCount = dotsList?.count
                self.dotsButton.setTitle(AppConstants.Strings.EDIT_POST_TAGS + " " + AppConstants.BRACKET_ON + "\(dotsCount ?? 0)" + AppConstants.BRACKET_OFF, for: .normal)
            }
            self.dotsButton.isHidden = false
        }
    }
    
    override func willMove(toParent parent: UIViewController?){
        super.willMove(toParent: parent)
        if parent == nil{
            Dots.dots.removeAll()
            Dots.removedDots.removeAll()
        }
    }
    
    func initalTopConstraintValueOnTap() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(topConstraintInitialValue))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func topConstraintInitialValue() {
        view.endEditing(true)
        UIView.animate(withDuration: 0.5) {
            self.topConstraint.constant = 15
            self.view.layoutIfNeeded()
        }
        self.addTagsHandler.enterTagCollectionViewCell?.headerView.isHidden = false
        self.addTagsHandler.enterTagCollectionViewCell?.dataView.isHidden = true
        self.addTagsHandler.enterTagCollectionViewCell?.addTagTextField.text = ""
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 150
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case titleTextField:
            postLinkTextField.becomeFirstResponder()
        case postLinkTextField:
            shortDescTextView.becomeFirstResponder()
        case shortDescTextView:
            shortDescTextView.resignFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if shortDescTextView.text == AppConstants.Strings.SHORT_DESCRIPTION_POST_TEXT {
            shortDescTextView.text = nil
            shortDescTextView.textColor = UIColor.black
            shortDescTextView.font = UIFont.fontRegular(size: 18)
        }
        UIView.animate(withDuration: 0.5) {
            self.topConstraint.constant = -300
            self.view.layoutIfNeeded()
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if shortDescTextView.text.isEmpty {
            shortDescTextView.text = AppConstants.Strings.SHORT_DESCRIPTION_POST_TEXT
            shortDescTextView.textColor = UIColor.cCDCDCD
            shortDescTextView.font = UIFont.fontRegular(size: 18)
        }
        UIView.animate(withDuration: 0.5) {
            self.topConstraint.constant = 15
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func postTypeAction(_ sender: UITapGestureRecognizer) {
        showDropDown(tag: type_post, title: AppConstants.Strings.SELECT_A_POST_TYPE, dataSourse: self, delegate: self) { (row) in
            self.postTypeLabel.textColor = UIColor.black
            self.postTypeLabel.text = self.postTypeSelect[row].name
            self.selectedPostType = self.postTypeSelect[row].postTypeId ?? 0
        }
    }
    
    @IBAction func updateButtonAction(_ sender: UIButton) {
        createButtonAction()
    }
    
    @IBAction func closeImageButtonAction(_ sender: UIButton) {
        isImageDeleted = true
        
        if Dots.dots.count > 0 || self.dotsList != nil {
            self.removedDots = self.dotsList!
            
            for i in 0..<self.removedDots.count {
                self.removedDots[i].isDeleted = true
            }
            
        }
        if feed?.hasPostDots == true{
            let storyBoard = UIStoryboard(name: AppConstants.Storyboard.DELETE_IMAGE_DIALOG_BOARD, bundle: nil)
            let addPostTagsViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.DELETE_IMAGE_DIALOG) as! DeleteImageDialogViewController
            addPostTagsViewController.postImageCloseClickDelegate = self
            self.navigationController?.present(addPostTagsViewController, animated: false)
            
        } else{
            postImageCloseClick()
            
        }
        
    }
    
    @IBAction func coverPhotoAction(_ sender: UITapGestureRecognizer) {
        if userCoverImageView.image == UIImage(named: AppConstants.Images.DOTS_IMAGE_VIEW){
            self.TYPE_IMAGE_SELECT = AppConstants.TYPE_ADD_TAGS_RESELECT_IMAGE
            getImage()
        }
    }
    
    func getData(){
        let tagsCount = feed?.postTags?.count
        titleTextField.text = feed?.title
        postLinkTextField.text = feed?.link
        
        shortDescTextView.text = feed?.description
        shortDescTextView.textColor = UIColor.black
        shortDescTextView.font = UIFont.fontRegular(size: 18)
        
        postTypeLabel.textColor = UIColor.black
        postTypeLabel.font = UIFont.fontRegular(size: 18)
        postTypeLabel.text = feed?.postType
        self.selectedPostType = feed?.postTypeId ?? 0
        
        if tagsCount != 0{
            for i in 0..<tagsCount!{
                self.addTagsHandler.dataObject.append(AddTags(tag:(feed?.postTags![i])!,type:0))
            }
            
        }
        let coverSelectImage = UIImage(named: AppConstants.Images.DOTS_IMAGE_VIEW)
        editPhotoButton.isHidden = false
        if let imageUrl = feed!.photoUrl{
            userCoverImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: coverSelectImage)
            self.editPostImage = imageUrl
        }
        
        if feed?.hasPostDots == true{
            getDotsInfo()
        }else{
            self.getPostType(hasDots: false)
        }
        
    }
    
//    func getDataEditPost(){
//        let tagsCount = feed?.postTags?.count
//        titleTextField.text = feed?.title
//        postLinkTextField.text = feed?.link
//        
//        shortDescTextView.text = feed?.description
//        shortDescTextView.textColor = UIColor.black
//        shortDescTextView.font = UIFont.fontRegular(size: 18)
//        
//        postTypeLabel.textColor = UIColor.black
//        postTypeLabel.font = UIFont.fontRegular(size: 18)
//        postTypeLabel.text = feed?.postType
//        self.selectedPostType = feed?.postTypeId ?? 0
//        
//        if tagsCount != 0{
//            for i in 0..<tagsCount!{
//                self.addTagsHandler.dataObject.append(AddTags(tag:(feed?.postTags![i])!,type:0))
//            }
//        }
//        
//        if feed?.hasPostDots == true{
//            getDotsInfo()
//        } else{
//            self.getPostType(hasDots: false)
//            screenActivityIndicator.isHidden = true
//            screenScrollView.isHidden = false
//            dotsButton.setTitle(AppConstants.Strings.ADD_POST_PRODUCT_TAGS, for: .normal)
//        }
//        
//    }
    
    func createButtonAction(){
        let titleTextField = self.titleTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let postTypeLabel = self.postTypeLabel.text
        let postLink = self.postLinkTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let shortDesc = self.shortDescTextView.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let addtagsCount = self.addTagsHandler.dataObject.count
        
        if userCoverImageView.image == UIImage(named: AppConstants.Images.DOTS_IMAGE_VIEW) {
            let text:String = AppConstants.Strings.POST_PHOTO_REQUIRED
            let textColor:String = AppConstants.Strings.POST_PHOTO
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        if titleTextField == ""{
            let text:String = AppConstants.Strings.POST_TITLE_REQUIRED
            let textColor:String = AppConstants.Strings.POST_TITLE
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        if postTypeLabel == AppConstants.Strings.SELECT_A_POST_TYPE{
            let text:String = AppConstants.Strings.POST_TYPE_REQUIRED
            let textColor:String = AppConstants.Strings.POST_TYPE
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        if postLink == ""{
            let text:String = AppConstants.Strings.POST_LINK_REQUIRED
            let textColor:String = AppConstants.Strings.POST_LINK
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        if  !((postLink?.contains(AppConstants.HTTPS))! || ((postLink?.contains(AppConstants.WWW_TEXT))!) || (postLink?.contains(AppConstants.HTTP))!){
            let text:String = AppConstants.Strings.POST_LINK_START_WITH
            let textColor:String = AppConstants.Strings.POST_LINK
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        
        if shortDesc == "" || shortDesc == AppConstants.Strings.SHORT_DESCRIPTION_POST_TEXT{
            let text:String = AppConstants.Strings.SHORT_DESC_REQUIRED
            let textColor:String = AppConstants.Strings.SHORT_DESCRIPTION_TEXT
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        if addtagsCount == 1{
            let text:String = AppConstants.Strings.TAG_REQUIRED
            let textColor:String = AppConstants.Strings.TAG
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        
        updatePost()
    }
    
    func updatePost() {
        let title = titleTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        var postLink = postLinkTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let shortDesc = shortDescTextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
        let tagsArray = self.addTagsHandler.dataObject
        var tags:[String] = []
        let isYoutubeLink = ValidationHelper.youTubeURLmatches(link: postLink!)
        
        for i in 1..<tagsArray.count{
            tags.append(tagsArray[i].tag!)
            
            let itemsArray = tags
            let searchToSearch = tagsArray[i].tag!
            
            let filteredStrings = itemsArray.filter({(item: String) -> Bool in
                
                let stringMatch = item.lowercased().range(of: searchToSearch.lowercased())
                return stringMatch != nil ? true : false
            })
            print("filteredStrings:",filteredStrings)
            
            
            if (filteredStrings as NSArray).count > 0
            {
                //Record found
                //MARK:- You can also print the result and can do any kind of work with them
            }
            else
            {
                //Record Not found
            }
            
        }
        
        if !(postLink?.contains("http"))!{
            postLink = AppConstants.HTTP + postLink!
        }
        
        if self.selectedPostType == AppConstants.TYPE_BLOG_POST && isYoutubeLink{
            
            let storyBoard = UIStoryboard(name: AppConstants.Storyboard.POST_CHECK_DIALOG_BOARD, bundle: nil)
            let postCheckViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.POST_CHECK_DIALOG) as! PostCheckDialogViewController
            postCheckViewController.delegate = self
            postCheckViewController.TYPE = AppConstants.TYPE_BLOG_POST
            self.navigationController?.present(postCheckViewController, animated: false)
            
        }else if self.selectedPostType == AppConstants.TYPE_VIDEO_POST && !isYoutubeLink{
            
            let storyBoard = UIStoryboard(name: AppConstants.Storyboard.POST_CHECK_DIALOG_BOARD, bundle: nil)
            let postCheckViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.POST_CHECK_DIALOG) as! PostCheckDialogViewController
            postCheckViewController.delegate = self
            postCheckViewController.TYPE = AppConstants.TYPE_VIDEO_POST
            self.navigationController?.present(postCheckViewController, animated: false)
            
        }else{
            let createPostData = CreatePostRequest(title: (title)!, postType: self.selectedPostType, link: (postLink)!, shortDescription: (shortDesc), tags: tags)
            
            var dotsUpdated = false
            
            if dotsList != nil && dotsList!.count > 0 {
                createPostData.postDots = dotsList
                createPostData.postDots?.append(contentsOf: self.removedDots)
                dotsUpdated = true
            }
            print(createPostData)
            self.savingPostView.isHidden = false
            createViewModel.updatePost(accessToken: getAccessToken(), data: createPostData, id: (feed?.id)!, { (response) in
                if response.response?.statusCode == 200 {
                    self.removedDots.removeAll()
                    self.dotsList?.removeAll()
                    Dots.dots.removeAll()
                    if self.isImageDeleted == true{
                        self.getImageId()
                        if !dotsUpdated {
                            self.deleteDots()
                        }
                    } else {
                        AppConstants.IS_JUST_CREATED = AppConstants.CREATE_TYPE_POST
                        self.navigationController?.popToRootViewController(animated: false)
                    }
                }else{
                    let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            } , { (error) in
                if error.statusCode == AppConstants.NO_INTERNET {
                    self.showInternetError()
                }
            })
        }
    }
    
    func getDotsInfo(){
        createViewModel.getDotsInfo(accessToken: getAccessToken(), id: (feed?.id)!, { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Feed> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let feedData = data.data
                    self.dotsInfo = feedData
                    self.dotsList = feedData?.postDots
                    let dotsCount = feedData?.postDotsCount
                    self.dotsButton.setTitle(AppConstants.Strings.EDIT_POST_TAGS + " " + AppConstants.BRACKET_ON + "\(dotsCount ?? 0)" + AppConstants.BRACKET_OFF, for: .normal)
                    self.getPostType(hasDots: true)
                    
                }
            }else{
                self.stopLoader()
                print("error")
            }
        } , { (error) in
            self.stopLoader()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getPostType(hasDots:Bool){
        createViewModel.getPostType(accessToken: getAccessToken(), { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[PostType]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let postType = data.data!
                    self.postTypeSelect = postType
                    
                    if !hasDots{
                        self.dotsButton.setTitle(AppConstants.Strings.ADD_POST_PRODUCT_TAGS, for: .normal)
                    }
                    self.showScreenAndStopLoader()
                }
            }else{
                self.stopLoader()
                print("error")
            }
        } , { (error) in
            self.stopLoader()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func showScreenAndStopLoader(){
        self.screenActivityIndicator.stopAnimating()
        self.screenScrollView.isHidden = false
    }
    
    func stopLoader(){
        self.screenActivityIndicator.stopAnimating()
    }
    
    func deleteDots(){
        createViewModel.deleteDots(accessToken: getAccessToken(), id:feed!.id!, { (response) in
            if response.response?.statusCode == 200  || response.response?.statusCode == 201{
                let data: ApiResponse<Feed> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getImageId(){
        createViewModel.getImageIdPost(accessToken: getAccessToken(), id: (feed?.id)!, { (response) in
            if response.response?.statusCode == 200 {
                
                let data: ApiResponse<[PostImage]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let postImageData: [PostImage] = data.data!
                    self.deleteUserImage(id:postImageData[0].postImageId!)
                }
                
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func deleteUserImage(id:Int){
        let imageIds = [id]
        let request = DeleteImagesRequest(imageIds: imageIds)
        createViewModel.deleteImage(accessToken: getAccessToken(), data: request, id: (feed?.id)!, { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Feed> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.uploadImage(id: (self.feed?.id)!)
                }
                
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func uploadImage(id:Int){
        self.savingPostView.isHidden = false
        createViewModel.uploadPostImage(accessToken: getAccessToken(), id: id,image:image!, { (response) in
            self.savingPostView.isHidden = true
            if response.response?.statusCode == 201 {
                AppConstants.IS_JUST_CREATED = AppConstants.CREATE_TYPE_POST
                self.navigationController?.popToRootViewController(animated: false)
            }
        } , { (error) in
            self.savingPostView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getImage(){
        self.dotsList = []
        imagePickerManager.pickImage(self,cameraTitle: AppConstants.CAMERA,galleryTitle: AppConstants.GALLERY){ image in
            self.image = image
            let storyBoard = UIStoryboard(name: AppConstants.Storyboard.ADD_POST_TAGS_BOARD, bundle: nil)
            let addPostTagsViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.ADD_POST_TAGS_SCREEN) as! AddPostTagsViewController
            addPostTagsViewController.image = image
            addPostTagsViewController.TYPE = AppConstants.TYPE_ADD_TAGS_RESELECT_IMAGE
            addPostTagsViewController.onImageChangeDelegate = self
            self.navigationController?.pushViewController(addPostTagsViewController, animated: false)
            self.editPhotoButton.isHidden = false
        }
        
    }
    
    @IBAction func dotsButtonAction(_ sender: UIButton) {
        if self.dotsList?.count ?? 0 < 1 {
            addTagsScreen()
        }else{
            selectTagTypeScreen()
        }
    }
    
    func addTagsScreen(){
        var photoUrl = ""
        var photoUIImage:UIImage? = image
        
        if photoUIImage != nil{
            photoUIImage = image
        }else{
            photoUrl = self.editPostImage!
        }
        
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.ADD_POST_TAGS_STORYBOARD, bundle: nil)
        let addPostTagsViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.ADD_TAGS_SCREEN) as! AddTagsViewController
        if photoUIImage != nil{
            addPostTagsViewController.image = photoUIImage
        }else{
            addPostTagsViewController.editPostImage = photoUrl
        }
        addPostTagsViewController.TYPE = AppConstants.TYPE_ADD_TAGS_EDIT_SCREEN
        self.navigationController?.pushViewController(addPostTagsViewController, animated: false)
    }
    
    func selectTagTypeScreen(){
        var photoUrl = ""
        if self.dotsInfo == nil{
            photoUrl = (self.feed?.photoUrl!)!
        }else{
            photoUrl = (self.dotsInfo?.photoUrl!)!
        }
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.SELECT_TAG_TYPE_STORYBOARD, bundle: nil)
        let selectTagsTypeViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.SELECT_TAG_TYPE_SCREEN) as! SelectTagTypeViewController
        selectTagsTypeViewController.editDotsList = self.dotsList
        selectTagsTypeViewController.editPostImage = photoUrl
        selectTagsTypeViewController.TYPE = AppConstants.TYPE_EDIT_TAGS
        selectTagsTypeViewController.dotsUpdateDelegate = self
        self.navigationController?.pushViewController(selectTagsTypeViewController, animated: false)
    }
    
}

extension EditPostViewController:UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return postTypeSelect.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return postTypeSelect[row].name
    }
}
