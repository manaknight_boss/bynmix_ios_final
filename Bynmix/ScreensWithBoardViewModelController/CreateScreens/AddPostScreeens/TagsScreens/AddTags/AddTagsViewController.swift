import UIKit

class AddTagsViewController: BaseViewController,UIGestureRecognizerDelegate {
    
    @IBOutlet weak var selectedImageView: UIImageView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet var whatIsThisLabel: UILabel!
    
    var image:UIImage?
    let maxViews = 1
    var limitDots = 0
    var TYPE:Int?
    var editDotsList:[Dots]?
    var editPostImage:String?
    var dots: [Dots] = []
    var customViews: [CustomView] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedImageView.layer.borderWidth = 1
        selectedImageView.layer.borderColor = UIColor.c3B3B3B.cgColor
        selectedImageView.contentMode = .scaleAspectFill
        
        navigationItem.title = AppConstants.Strings.CREATE_A_POST_TITLE
        let selectTagsBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.SELECT_ICON), style: .plain, target: self, action: #selector(onSelectTagsBarButtonItem))
        self.navigationItem.rightBarButtonItem  = selectTagsBarButton
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        selectedImageView.addGestureRecognizer(tapGestureRecognizer)
        AppDefaults.setTags(tagValue: 0)

        let backBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.BACK_BUTTON), style: .plain, target: self, action: #selector(onBackBarButtonItem))
        self.navigationItem.leftBarButtonItem  = backBarButton
        
        if image != nil{
            selectedImageView.image = image
        }else{
            onselectTypeShowScreen()
        }
        setTextForAboutTagsLabel()
    }
    
    func setTextForAboutTagsLabel(){
        let text = AppConstants.Strings.WHAT_THIS_TEXT + " "
        let textAttributed = AppConstants.Strings.WHAT_THIS_TEXT
        let range = (text as NSString).range(of: textAttributed)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 12), range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cBB189C, range: range)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range)
        whatIsThisLabel.attributedText = attributedString
    }
    
    @objc func onBackBarButtonItem(){
        Dots.dots.removeAll()
        AppDefaults.setTags(tagValue: 0)
        self.navigationController?.popViewController(animated: false)
    }
    
    func onselectTypeShowScreen(){
        let coverSelectImage = UIImage(named: AppConstants.Images.DOTS_IMAGE_VIEW)
        self.selectedImageView.kf.setImage(with: URL(string: editPostImage!)!,placeholder: coverSelectImage)
    }
    
    @IBAction func TagsDemoTap(_ sender: UITapGestureRecognizer) {
        tagsDemoDialog()
    }
    
    func tagsColorsCheck(color:Int) -> String{
        var check = ""
        if color == 0 {
            check = AppConstants.DOT_COLOR_RED
        }else if color == 1{
            check = AppConstants.DOT_COLOR_GREEN
        }else if color == 2{
            check = AppConstants.DOT_COLOR_PURPLE
        }else if color == 3{
            check = AppConstants.DOT_COLOR_BLUE
        }else if color == 4{
            check = AppConstants.DOT_COLOR_YELLOW
        }
        return check
    }

    func checkTagsCount(){
        if Dots.dots.count > 0 {
            
            for i in 0..<Dots.dots.count {
                let dot = Dots.dots[i]
                var color = ""
                if dot.type == Dots.DOT1{
                  color = AppConstants.DOT_COLOR_RED
                }else if dot.type == Dots.DOT2{
                  color = AppConstants.DOT_COLOR_GREEN
                }else if dot.type == Dots.DOT3{
                  color = AppConstants.DOT_COLOR_PURPLE
                }else if dot.type == Dots.DOT4{
                  color = AppConstants.DOT_COLOR_BLUE
                }else if dot.type == Dots.DOT5{
                  color = AppConstants.DOT_COLOR_YELLOW
                }
                let imageWidth = Double((self.selectedImageView?.frame.size.width)!)
                let imageHeight = Double((self.selectedImageView?.frame.size.height)!)
                let dotX = dot.xCoordinate!
                let dotY = dot.yCoordinate!
                let newDot = Dots(id: 0, color: color, isDeleted: false, x: (dotX / imageWidth), y: (dotY / imageHeight))
                dots.append(newDot)
            }
            
            selectTagTypeScreen(dots: self.dots)
        }else{
            self.view.makeToast(AppConstants.Strings.PLEASE_ADD_TAGS_FIRST)
        }
    }
    
    @IBAction func nextButtonAction(_ sender: UIButton) {
        checkTagsCount()
    }
    
    @objc func onSelectTagsBarButtonItem(){
        checkTagsCount()
    }
    
    func selectTagTypeScreen(dots:[Dots]){
        self.dots.removeAll()
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.SELECT_TAG_TYPE_STORYBOARD, bundle: nil)
        let selectTagTypeViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.SELECT_TAG_TYPE_SCREEN) as! SelectTagTypeViewController
        selectTagTypeViewController.TYPE = self.TYPE
        selectTagTypeViewController.dots = dots
        if self.TYPE == AppConstants.TYPE_ADD_TAGS{
           selectTagTypeViewController.addDotsImage = self.image
        }else if self.TYPE == AppConstants.TYPE_ADD_TAGS_EDIT_SCREEN{
           selectTagTypeViewController.editPostImage = self.editPostImage
        }else if self.TYPE == AppConstants.TYPE_ADD_TAGS_RESELECT_IMAGE{
           selectTagTypeViewController.addDotsImage = self.image
        }
        self.navigationController?.pushViewController(selectTagTypeViewController, animated: false)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){
        let touchPoint = tapGestureRecognizer.location(in: self.selectedImageView)
        let dotWidth: CGFloat = CGFloat(AppConstants.DOT_SIZE)
        let dotsView = CustomView(frame: CGRect(x: touchPoint.x - (dotWidth/2), y: touchPoint.y - (dotWidth/2), width: dotWidth, height: dotWidth))
        dotsView.layer.cornerRadius = dotWidth / 2
        dotsView.isMultipleTouchEnabled = true
        limitDots=AppDefaults.getTags()
        if limitDots < 5{
            for _ in 0 ..< maxViews {
            
                let dotColor = dotsView.backgroundColor
                var checkDot:Int?
                if dotColor == UIColor.cE58585{
                    checkDot = Dots.DOT1
                }else if dotColor == UIColor.c85E590{
                    checkDot = Dots.DOT2
                }else if dotColor == UIColor.c858DE5{
                    checkDot = Dots.DOT3
                }else if dotColor == UIColor.c22CFFF{
                    checkDot = Dots.DOT4
                }else if dotColor == UIColor.cFDD12A{
                    checkDot = Dots.DOT5
                }
                
                Dots.dots.append(Dots(type: checkDot!, x: Double(touchPoint.x), y: Double(touchPoint.y)))
                self.selectedImageView.addSubview(dotsView)
                customViews.append(dotsView)
                limitDots += 1
                AppDefaults.setTags(tagValue: limitDots)
            }
        }
    }
    
}
