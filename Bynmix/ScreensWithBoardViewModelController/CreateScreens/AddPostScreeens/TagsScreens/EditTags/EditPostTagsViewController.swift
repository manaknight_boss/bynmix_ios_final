import UIKit
import Kingfisher
import Foundation

class EditPostTagsViewController: BaseViewController {
    
    @IBOutlet weak var postImageView: UIImageView!
    
    var editPostImage:String?
    var editDotsList:[Dots]?
    var TYPE:Int?
    let maxViews = 1
    var limitDots = 0
    var customViews: [CustomView] = []
    var deletedDots:[Dots]?
    var updateDotsDelegate: DotsUpdateProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.Strings.EDIT_POST_TAGS_TEXT
        let selectTagsBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.SELECT_ICON), style: .plain, target: self, action: #selector(onSelectTagsBarButtonItem))
        self.navigationItem.rightBarButtonItem  = selectTagsBarButton
        
        let image = UIImage(named: "placeholderImage")
        postImageView.autoresizingMask = UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.flexibleBottomMargin.rawValue | UIView.AutoresizingMask.flexibleHeight.rawValue | UIView.AutoresizingMask.flexibleRightMargin.rawValue | UIView.AutoresizingMask.flexibleLeftMargin.rawValue | UIView.AutoresizingMask.flexibleTopMargin.rawValue | UIView.AutoresizingMask.flexibleWidth.rawValue)
        postImageView.contentMode = UIView.ContentMode.scaleToFill
        if let imageUrl = editPostImage{
            self.postImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        } else {
            self.postImageView.image =  image
        }
        AppDefaults.setTags(tagValue: editDotsList!.count)
        createDots(dots: editDotsList!)
        TYPE = AppConstants.TYPE_EDIT_TAGS
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        tapGestureRecognizer.numberOfTapsRequired = 1
        postImageView.addGestureRecognizer(tapGestureRecognizer)
    
    }
    
    @objc func onSelectTagsBarButtonItem(){
        checkTagsCount()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if self.isMovingFromParent {
            Dots.dots.removeAll()
            Dots.removedDots.removeAll()
        }
    }
    
    func createDots(dots:[Dots]){
        
        let imageWidth = Double((self.postImageView.frame.width))
        let imageHeight = Double((self.postImageView.frame.height))
//        var checkDot:Int?
        
        for i in 0..<dots.count{
            var dot = dots[i]
            let x = dot.xCoordinate! * imageWidth
            let y = dot.yCoordinate! * imageHeight
            let requiredX = x - (AppConstants.DOT_SIZE)
            let requiredY = y - (AppConstants.DOT_SIZE/2)
            let dotsView = CustomView(frame: CGRect(x:requiredX, y:requiredY, width: AppConstants.DOT_SIZE, height: AppConstants.DOT_SIZE))
            dotsView.layer.cornerRadius = dotsView.frame.width / 2
            dotsView.isMultipleTouchEnabled = true
            if dot.hexColor == AppConstants.DOT_COLOR_RED{
                dotsView.backgroundColor = UIColor.cE58585
                dot.type = Dots.DOT1
            }else if dot.hexColor == AppConstants.DOT_COLOR_YELLOW{
                dotsView.backgroundColor = UIColor.cFDD12A
                dot.type = Dots.DOT5
            }else if dot.hexColor == AppConstants.DOT_COLOR_BLUE{
                dotsView.backgroundColor = UIColor.c22CFFF
                dot.type = Dots.DOT4
            }else if dot.hexColor == AppConstants.DOT_COLOR_GREEN{
                dotsView.backgroundColor = UIColor.c85E590
                dot.type = Dots.DOT2
            }else if dot.hexColor == AppConstants.DOT_COLOR_PURPLE{
                dotsView.backgroundColor = UIColor.c858DE5
                dot.type = Dots.DOT3
            }
            self.postImageView.addSubview(dotsView)
            Dots.dots.append(dot)
        }
        self.editDotsList?.removeAll()
    }
    
    func checkTagsCount(){
        if Dots.dots.count > 0 {
            
            for i in 0..<Dots.dots.count {
                var dot = Dots.dots[i]
                var color = ""
                
                if dot.type == Dots.DOT1{
                    color = AppConstants.DOT_COLOR_RED
                }else if dot.type == Dots.DOT2{
                    color = AppConstants.DOT_COLOR_GREEN
                }else if dot.type == Dots.DOT3{
                    color = AppConstants.DOT_COLOR_PURPLE
                }else if dot.type == Dots.DOT4{
                    color = AppConstants.DOT_COLOR_BLUE
                }else if dot.type == Dots.DOT5{
                    color = AppConstants.DOT_COLOR_YELLOW
                }
                let imageWidth = Double(self.postImageView.frame.size.width)
                let imageHeight = Double(self.postImageView.frame.size.height)
                let dotX = dot.xCoordinate!
                let dotY = dot.yCoordinate!
                let newDot: Dots
                if dot.postDotId == nil || dot.postDotId == 0 {
                    newDot = Dots(id: 0, color: color, isDeleted: false, x: (dotX / imageWidth), y: (dotY / imageHeight))
                } else {
                    if dot.xCoordinate! > 1.0 {
                        dot.xCoordinate = dot.xCoordinate! / imageWidth
                    }
                    if dot.yCoordinate! > 1.0 {
                        dot.yCoordinate = dot.yCoordinate! / imageHeight
                    }
                    newDot = dot
                }
                editDotsList?.append(newDot)
            }
            print(self.editDotsList!)
            selectTagTypeScreen(dots: self.editDotsList!)
        }else{
            self.view.makeToast(AppConstants.Strings.PLEASE_ADD_TAGS_FIRST)
        }
    }
    
    func selectTagTypeScreen(dots:[Dots]){
        updateDotsDelegate?.onDotsUpdate(dots: editDotsList!, removedDots: Dots.removedDots)
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){
        let touchPoint = tapGestureRecognizer.location(in: self.postImageView)
        let dotWidth: CGFloat = CGFloat(AppConstants.DOT_SIZE)
        let dotsView = CustomView(frame: CGRect(x: touchPoint.x - (dotWidth/2), y: touchPoint.y - (dotWidth/2), width: dotWidth, height: dotWidth))
        dotsView.layer.cornerRadius = dotWidth / 2
        dotsView.isMultipleTouchEnabled = true
        limitDots=AppDefaults.getTags()
        if limitDots < 5{
            for _ in 0 ..< maxViews {
                
                let dotColor = dotsView.backgroundColor
                var checkDot:Int?
                if dotColor == UIColor.cE58585{
                    checkDot = Dots.DOT1
                }else if dotColor == UIColor.c85E590{
                    checkDot = Dots.DOT2
                }else if dotColor == UIColor.c858DE5{
                    checkDot = Dots.DOT3
                }else if dotColor == UIColor.c22CFFF{
                    checkDot = Dots.DOT4
                }else if dotColor == UIColor.cFDD12A{
                    checkDot = Dots.DOT5
                }
                
                Dots.dots.append(Dots(type: checkDot!, x: Double(touchPoint.x), y: Double(touchPoint.y)))
                self.postImageView.addSubview(dotsView)
                customViews.append(dotsView)
                limitDots += 1
                AppDefaults.setTags(tagValue: limitDots)
            }
        }
    }
    
}

