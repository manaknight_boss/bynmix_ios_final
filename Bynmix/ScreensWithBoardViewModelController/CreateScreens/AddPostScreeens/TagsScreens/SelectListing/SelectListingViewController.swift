import UIKit
import Alamofire

class SelectListingViewController: BaseViewController, LoadMoreProtocol ,BuyNowReturnProtocol{
    
    func buyNowReturn() {
        self.navigationController?.popViewController(animated: false)
    }
    
    func onLoadMore() {
        if !isApiCalled {
            isApiCalled = true
            getListingTitles()
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var crossButton: UIButton!
    
    let selectListingHandler = SelectListingHandler()
    let createViewModel = CreateViewModel()
    var currentPage = 1
    var isApiCalled = false
    var onSelectListingTitlesClickDelegate:SelectListingProtocol?
    var indexOfSelectTagType:Int?
    var TYPE:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = selectListingHandler
        tableView.dataSource = selectListingHandler
        navigationItem.title = AppConstants.Strings.SELECT_LISTING_TITLE
        selectListingHandler.loadMoreDelegate = self
        getListingTitles()
        selectListingHandler.onListingClick = { listingTitles in
            self.onSelectListingTitlesClickDelegate?.selectListing(selectListing: listingTitles,index: self.indexOfSelectTagType!,type: self.TYPE!)
        }
        searchTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        crossButton.isHidden = true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text == ""{
            self.crossButton.isHidden = true
        }else{
            self.view.makeToastActivity(.center)
            self.crossButton.isHidden = false
            getListingTitles(forSuggestions: true)
        }
    }
    
    func getListingTitles(forSuggestions: Bool! = false) {
        if forSuggestions{
            currentPage = 1
        }
        createViewModel.getListingTitles(accessToken: getAccessToken(),searchQuery: forSuggestions ? searchTextField.text : nil,page:currentPage, { (response) in
            self.screenActivityIndicator.stopAnimating()
            self.view.hideToastActivity()
            self.handleResponse(response: response,forSuggestions: forSuggestions)
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func handleResponse(response: AFDataResponse<Any>,forSuggestions: Bool! = false) {
        self.isApiCalled = false
        if response.response?.statusCode == 200 {
            let data: ApiResponse<ListingTitles> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            } else {
                let listingData = data.data
                
                if listingData?.items?.count ?? 0 > 0{
                    if let listing = listingData?.items {
                        if self.currentPage == 1 {
                            self.selectListingHandler.listingTitles = listing
                        } else {
                            self.selectListingHandler.listingTitles.append(contentsOf: listing)
                        }
                        if listingData?.totalPages == currentPage {
                            self.isApiCalled = true
                            self.selectListingHandler.isLoadMoreRequired = false
                        }
                        self.tableView.reloadData()
                        self.screenActivityIndicator.stopAnimating()
                        self.searchView.isHidden = false
                        self.tableView.isHidden = false
                    }
                }else{
                    self.showMessageDialog(message: AppConstants.Strings.NO_LISTING_TEXT,_:1)
                }
                
            }
            if !forSuggestions {
                self.currentPage = self.currentPage + 1
            }
            
        } else {
            self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
        }
    }
    
    func showMessageDialog(message: String,_:Int) {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.MESSAGE_DIALOG_BOARD, bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.MESSAGE_DIALOG) as! MessageDialogViewController
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        myAlert.message = message
        myAlert.buyNowReturnDelegate = self
        self.present(myAlert, animated: true, completion: nil)
    }
    
    
    @IBAction func crossButtonAction(_ sender: UIButton) {
        self.searchTextField.text = ""
        self.view.makeToastActivity(.center)
        self.getListingTitles(forSuggestions: false)
    }
    
}
