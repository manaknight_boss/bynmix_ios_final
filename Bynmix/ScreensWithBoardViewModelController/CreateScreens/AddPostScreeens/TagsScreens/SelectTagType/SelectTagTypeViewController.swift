import UIKit

class SelectTagTypeViewController: BaseViewController,SelectListingProtocol , DotsUpdateProtocol {

    func onDotsUpdate(dots: [Dots], removedDots: [Dots]) {
    //    self.dotsList = dots
    //    let dotsCount = dots.count
    //    self.dotsButton.setTitle(AppConstants.Strings.EDIT_POST_TAGS + " " + AppConstants.BRACKET_ON + "\(dotsCount )" + AppConstants.BRACKET_OFF, for: .normal)
        if removedDots.count > 0 {
            self.removedDots = removedDots
        }
        
        if self.TYPE == AppConstants.TYPE_EDIT_TAGS{
            self.editDotsList = dots
        }else{
            self.dots = dots
        }
    
    selectType()
    checkIfComplete()
}
    
    func selectListing(selectListing: ListingTitles,index:Int,type:Int) {
        self.navigationController?.popViewController(animated: false)
        let title = selectListing.title
        if type == AppConstants.TYPE_ADD_TAGS{
            self.dots![index].listingId = selectListing.id
            self.dots![index].listingTitle = title
            self.selectTagsTypeHandler.tags = self.dots!
        }else if type == AppConstants.TYPE_EDIT_TAGS{
            self.editDotsList![index].listingId = selectListing.id
            self.editDotsList![index].listingTitle = title
            self.selectTagsTypeHandler.tags = self.editDotsList!
        }else{
            self.dots![index].listingId = selectListing.id
            self.dots![index].listingTitle = title
            self.selectTagsTypeHandler.tags = self.dots!
        }
        self.TYPE = type
        tableView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.checkIfComplete()
        }
        
    }
    
    func checkIfComplete() {
        isComplete = false
        var tempDots: [Dots]
        if TYPE == AppConstants.TYPE_EDIT_TAGS {
            tempDots = editDotsList!
        } else {
            tempDots = dots!
        }
        for i in 0..<tempDots.count {
            let dot = tempDots[i]
            if dot.postDotTypeId == AppConstants.TYPE_ID_ONLINE_RETAILER {
                if dot.title == nil || dot.title == "" || dot.link == nil || dot.link == "" {
                    return
                }
                if  !((dot.link?.contains(AppConstants.HTTPS))! || ((dot.link?.contains(AppConstants.WWW_TEXT))!) || (dot.link?.contains(AppConstants.HTTP))! || (dot.link?.contains(AppConstants.Strings.COM_TEXT))!){
                    return
                }
            } else if dot.postDotTypeId == AppConstants.TYPE_ID_MY_LISTING{
                if dot.listingId == nil {
                    return
                }
            } else {
                return
            }
        }
        isComplete = true
    }
    
    func enableButton(){
        doneButton.isEnabled = true
        doneButton.backgroundColor = UIColor.cBB189C
    }
    
    func disableButton(){
        doneButton.isEnabled = false
        doneButton.backgroundColor = UIColor.cCDCDCD
    }
    
    @IBOutlet weak var editDeleteTagsView: UIView!
    @IBOutlet weak var editDeleteTagsButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var editDeleteTagsHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var errorMessageView: UIView!
    @IBOutlet weak var errorTextLabel: UILabel!
    @IBOutlet weak var errorMessageInnerView: UIView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    var inputActive: UITextField!
    
    let selectTagsTypeHandler = SelectTagsTypeHandler()
    var TYPE:Int?
    var tagTypeSelect = [String]()
    var TYPE_TAG = 0
    var isTagSelected = false
    var dots:[Dots]?
    var editDotsList:[Dots]?
    var editPostImage:String?
    var selectedRow:Int = 0
    var index:Int = 0
    var addDotsImage:UIImage?
    var isComplete: Bool = false
    weak var dotsUpdateDelegate: DotsUpdateProtocol?
    var removedDots: [Dots] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = selectTagsTypeHandler
        tableView.dataSource = selectTagsTypeHandler
        selectType()
        onSelectTypeItemsClick()
        tagTypeSelect = AppConstants.StaticLists.tagType
        hideKeyboardWhenTappedAround()
        enableButton()
        let outerViewTapGesture = UITapGestureRecognizer(target: self, action:  #selector(outerViewErrorMessageTapAction))
        self.errorMessageView.addGestureRecognizer(outerViewTapGesture)
        let innerViewTapGesture = UITapGestureRecognizer(target: self, action:  #selector(outerViewErrorMessageTapAction))
        self.errorMessageInnerView.addGestureRecognizer(innerViewTapGesture)
        checkIfComplete()
    }
    
    deinit {
        self.removeKeyboardObserver()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       self.removeKeyboardObserver()
    }
    
    @objc func outerViewErrorMessageTapAction(sender : UITapGestureRecognizer) {
        self.errorMessageView.isHidden = true
    }
    
    override func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        inputActive = textField
        return true
    }
    
    func selectType(){
        if TYPE == AppConstants.TYPE_ADD_TAGS{
            self.navigationItem.title = AppConstants.Strings.CREATE_A_POST_TITLE
            self.editDeleteTagsHeightConstraint.constant = 0
            selectTagsTypeHandler.TYPE = AppConstants.TYPE_ADD_TAGS
            selectTagsTypeHandler.tags = self.dots!
        }else if TYPE == AppConstants.TYPE_EDIT_TAGS{
            self.editDeleteTagsHeightConstraint.constant = 75
            self.navigationItem.title = AppConstants.Strings.EDIT_POST_TITLE
            selectTagsTypeHandler.TYPE = AppConstants.TYPE_EDIT_TAGS
            selectTagsTypeHandler.tags = self.editDotsList!
        }else if TYPE == AppConstants.TYPE_ADD_TAGS_EDIT_SCREEN {
            self.navigationItem.title = AppConstants.Strings.EDIT_POST_TITLE
            self.editDeleteTagsHeightConstraint.constant = 0
            selectTagsTypeHandler.TYPE = AppConstants.TYPE_ADD_TAGS_EDIT_SCREEN
            selectTagsTypeHandler.tags = self.dots!
        }else if TYPE == AppConstants.TYPE_ADD_TAGS_RESELECT_IMAGE {
            self.navigationItem.title = AppConstants.Strings.EDIT_POST_TITLE
            self.editDeleteTagsHeightConstraint.constant = 0
            selectTagsTypeHandler.TYPE = AppConstants.TYPE_ADD_TAGS_RESELECT_IMAGE
            selectTagsTypeHandler.tags = self.dots!
        }
        tableView.reloadData()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let indexOfList = textField.tag
        self.index = indexOfList
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("index in textFieldDidEndEditing:",self.index)
        let index = IndexPath(row: self.index , section: 0)
        print("indexPath:",index)
        guard let cell = (self.tableView.cellForRow(at: index) as? SelectTagTypeTableViewCell) else{
            if self.TYPE == AppConstants.TYPE_EDIT_TAGS{
                self.editDotsList![self.index].link = textField.text ?? ""
                self.selectTagsTypeHandler.tags = self.editDotsList!
            }else{
                self.dots![self.index].link = textField.text ?? ""
                self.selectTagsTypeHandler.tags = self.dots!
            }
            self.tableView.reloadData()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
                self.checkIfComplete()
            }
            return
        }
        
        if textField == cell.linkTextField && textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
            if self.TYPE == AppConstants.TYPE_EDIT_TAGS{
                self.editDotsList![self.index].link = textField.text ?? ""
                self.selectTagsTypeHandler.tags = self.editDotsList!
            }else{
                self.dots![self.index].link = textField.text ?? ""
                self.selectTagsTypeHandler.tags = self.dots!
            }
        }else if textField == cell.titletextField && textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
            if self.TYPE == AppConstants.TYPE_EDIT_TAGS{
                self.editDotsList![self.index].title = textField.text ?? ""
                self.selectTagsTypeHandler.tags = self.editDotsList!
            }else{
                self.dots![self.index].title = textField.text ?? ""
                self.selectTagsTypeHandler.tags = self.dots!
            }
        }
        self.tableView.reloadData()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.checkIfComplete()
        }
    }

    func onSelectTypeItemsClick(){
        self.selectTagsTypeHandler.onSelectTagTypeClick = { index,dots in
            print("index:",index,"dots:",dots)
            self.onSelectTagTypeTapAction(index:index ,dots: dots)
        }
        
        self.selectTagsTypeHandler.onListingButtonClick = { index in
            self.listingTitlesScreen(index:index)
        }
        self.selectTagsTypeHandler.onLinkErrorButtonClick = { sender in
            self.showErrorTooltip(errorMessage: AppConstants.Strings.SOME_URL_NOT_VALID, view: sender)
        }
        
    }
    
    func listingTitlesScreen(index:Int){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.SELECT_LISTING_STORYBOARD, bundle:nil)
        let selectListingViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.SELECT_LISTING_SCREEN) as! SelectListingViewController
        selectListingViewController.TYPE = self.TYPE
        selectListingViewController.indexOfSelectTagType = index
        selectListingViewController.onSelectListingTitlesClickDelegate = self
        self.navigationController?.pushViewController(selectListingViewController, animated:false)
    }
    
    func onSelectTagTypeTapAction(index:Int ,dots: Dots){
        showDropDown(tag: TYPE_TAG, title: AppConstants.SELECT_TAG_TYPE, dataSourse: self, delegate: self, {(row) in
            let selectedLabel = self.tagTypeSelect[row]
            
            if(selectedLabel == AppConstants.Strings.ONLINE_RETAILER){
                if self.TYPE == AppConstants.TYPE_ADD_TAGS{
                    self.dots![index].postDotTypeId = AppConstants.TYPE_ID_ONLINE_RETAILER
                }else if self.TYPE == AppConstants.TYPE_EDIT_TAGS{
                    self.editDotsList![index].postDotTypeId = AppConstants.TYPE_ID_ONLINE_RETAILER
                }else{
                    self.dots![index].postDotTypeId = AppConstants.TYPE_ID_ONLINE_RETAILER
                }
                
            } else {
                
                if self.TYPE == AppConstants.TYPE_ADD_TAGS{
                    self.dots![index].postDotTypeId = AppConstants.TYPE_ID_MY_LISTING
                }else if self.TYPE == AppConstants.TYPE_EDIT_TAGS{
                    self.editDotsList![index].postDotTypeId = AppConstants.TYPE_ID_MY_LISTING
                }else{
                    self.dots![index].postDotTypeId = AppConstants.TYPE_ID_MY_LISTING
                }
                
            }
            if self.TYPE == AppConstants.TYPE_ADD_TAGS{
                self.selectTagsTypeHandler.tags = self.dots!
            }else if self.TYPE == AppConstants.TYPE_EDIT_TAGS{
                self.selectTagsTypeHandler.tags = self.editDotsList!
            }else{
                self.selectTagsTypeHandler.tags = self.dots!
            }
            
            self.tableView.reloadData()
        })
        
    }
    
    @IBAction func editDeleteTagsAction(_ sender: UIButton) {
        editTagsScreen()
    }
    
    func editTagsScreen(){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.EDIT_POST_TAGS_STORYBOARD, bundle:nil)
        let editDeleteTagsViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.EDIT_PRODUCT_TAGS_SCREEN) as! EditPostTagsViewController
        editDeleteTagsViewController.editDotsList = self.editDotsList!
        editDeleteTagsViewController.editPostImage = self.editPostImage
        editDeleteTagsViewController.updateDotsDelegate = self
        self.navigationController?.pushViewController(editDeleteTagsViewController, animated:false)
    }
    
    @IBAction func doneButtonAction(_ sender: UIButton) {
        if isComplete {

            if TYPE == AppConstants.TYPE_EDIT_TAGS {
                for i in 0..<self.editDotsList!.count{
                    if self.editDotsList![i].postDotTypeId == AppConstants.TYPE_ID_ONLINE_RETAILER {
                        if !(self.editDotsList![i].link?.contains("http"))!{
                            self.editDotsList![i].link = AppConstants.HTTP + self.editDotsList![i].link!
                        }else{
                            self.editDotsList![i].link = self.editDotsList![i].link!
                        }
                    }
                }
            } else {
                for i in 0..<self.dots!.count{
                    if self.dots![i].postDotTypeId == AppConstants.TYPE_ID_ONLINE_RETAILER {
                        if !(self.dots![i].link?.contains("http"))!{
                            self.dots![i].link = AppConstants.HTTP + self.dots![i].link!
                        }else{
                            self.dots![i].link = self.dots![i].link!
                        }
                    }
                }
            }
            
            
            if self.TYPE == AppConstants.TYPE_ADD_TAGS{
                createPostScreen()
            }else if self.TYPE == AppConstants.TYPE_EDIT_TAGS{
                dotsUpdateDelegate?.onDotsUpdate(dots: editDotsList!, removedDots: removedDots)
                self.navigationController?.popViewController(animated: false)
            }else{
                let editPostViewController = navigationController!.viewControllers[2] as! EditPostViewController
                editPostViewController.TYPE = self.TYPE
                editPostViewController.dotsList = self.dots
                if addDotsImage != nil{
                 editPostViewController.image = self.addDotsImage
                    print(addDotsImage!)
                }
                navigationController!.popToViewController(editPostViewController, animated: false)
            }
        }else{
            var tempDots: [Dots]
            if TYPE == AppConstants.TYPE_EDIT_TAGS {
                tempDots = editDotsList!
            } else {
                tempDots = dots!
            }
            print(tempDots)
            for i in 0..<tempDots.count {
                let dot = tempDots[i]
                if dot.postDotTypeId == AppConstants.TYPE_ID_ONLINE_RETAILER {
                    if dot.title?.trimmingCharacters(in: .whitespacesAndNewlines) == nil {
                        errorMessageView.isHidden = false
                        errorTextLabel.text = AppConstants.Strings.TITLE_MISSING
                        break
                    }else{
                        if dot.link?.trimmingCharacters(in: .whitespacesAndNewlines) == nil{
                            errorMessageView.isHidden = false
                            errorTextLabel.text = AppConstants.Strings.LINK_MISSING
                            break
                        }else{
                            if !((dot.link?.contains(AppConstants.HTTPS))! || ((dot.link?.contains(AppConstants.WWW_TEXT))!) || (dot.link?.contains(AppConstants.HTTP))! || (dot.link?.contains(AppConstants.Strings.COM_TEXT))!){
                               errorMessageView.isHidden = false
                               let text:String = "The link \(dot.link ?? "") is invalid"
                               let textPink:String = "\(dot.link ?? "")"
                               let range = (text as NSString).range(of: textPink)
                               let attributedString = NSMutableAttributedString(string: text)
                               attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cFF89C7, range: range)
                               errorTextLabel.attributedText = attributedString
                               break
                            }
                        }
                        
                    }
                        
                } else if dot.postDotTypeId == AppConstants.TYPE_ID_MY_LISTING{
                    if dot.listingId == nil {
                        errorMessageView.isHidden = false
                        errorTextLabel.text = AppConstants.Strings.LISTING_NOT_SELECTED
                        break
                    }
                } else {
                    errorMessageView.isHidden = false
                    errorTextLabel.text = AppConstants.Strings.SELECT_A_TAG_VALUE
                    break
                }
            }
            
        }
    }
    
    func createPostScreen(){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.CREATE_POST_BOARD, bundle:nil)
        let createPostViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.CREATE_POST_INFO_SCREEN) as! CreatePostViewController
        createPostViewController.TYPE = self.TYPE
        createPostViewController.dots = self.dots
        createPostViewController.image = self.addDotsImage
        self.navigationController?.pushViewController(createPostViewController, animated:false)
    }
    
    func editPostScreen(){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.EDIT_POST_BOARD, bundle:nil)
        let editPostViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.EDIT_POST_SCREEN) as! EditPostViewController
        editPostViewController.TYPE = self.TYPE
        editPostViewController.editPostImage = self.editPostImage
        editPostViewController.dotsList = self.dots
        self.navigationController?.pushViewController(editPostViewController, animated:false)
    }

    @objc func keyboardWillShow(notification: NSNotification) {
        let userInfo = notification.userInfo!
        if let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            // Get my height size
            let myheight = tableView.frame.height
            // Get the top Y point where the keyboard will finish on the view
            let keyboardEndPoint = myheight - keyboardFrame.height
            // Get the the bottom Y point of the textInput and transform it to the currentView coordinates.
            if let pointInTable = inputActive.superview?.convert(inputActive.frame.origin, to: tableView) {
                let textFieldBottomPoint = pointInTable.y + inputActive.frame.size.height + 20
                // Finally check if the keyboard will cover the textInput
                if keyboardEndPoint <= textFieldBottomPoint {
                    tableView.contentOffset.y = textFieldBottomPoint - keyboardEndPoint
                } else {
                    tableView.contentOffset.y = 0
                }
            }
        }
    }
    
    func removeKeyboardObserver(){
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        tableView.contentOffset.y = 0
        self.view.layoutIfNeeded()
    }
    
    func verifyUrl (urlString: String?) -> Bool {
        let urlRegEx = "^(https?://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$"
        let urlTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
        let result = urlTest.evaluate(with: urlString)
        return result
    }
    
    @IBAction func dialogCloseButtonAction(_ sender: UIButton) {
        self.errorMessageView.isHidden = true
    }
    
}

extension SelectTagTypeViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return tagTypeSelect.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(tagTypeSelect[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == TYPE_TAG {
            self.selectedRow = row + 1
        }
    }
    
}

