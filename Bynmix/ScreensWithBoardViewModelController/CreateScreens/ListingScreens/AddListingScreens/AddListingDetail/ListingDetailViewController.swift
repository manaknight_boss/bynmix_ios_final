import UIKit

class ListingDetailViewController: BaseViewController,onPostTypeChangeProtocol,ColorSelectProtocol, CategorySelectionProtocol,BrandsSelectProtocol,SizeSelectProtocol,TagsContainsValueProtocol {
    
    func tagsContainsValue(type: Int) {
        if type == AppConstants.TYPE_ON_TAG_HAS_VALUE{
            self.valueChanged = true
            AppConstants.LISTING_LOST = true
        }else{
            self.valueChanged = false
            AppConstants.LISTING_LOST = false
        }
    }
    
    func sizeSelect(size: Sizes) {
        self.valueChanged = true
        AppConstants.LISTING_LOST = true
        self.sizeButton.isHidden = true
        self.mainSizeView.isHidden = false
        self.sizelabel.text = size.sizeName
        self.selectedSize = size
    }
    
    func brandsSelect(brand: Brand) {
        self.valueChanged = true
        AppConstants.LISTING_LOST = true
        self.brandButton.isHidden = true
        self.mainBrandView.isHidden = false
        self.brandLabel.text = brand.brandName
        self.selectedBrand = brand
    }
    
    func colorSelect(selectedColors: [Colors]) {
        self.valueChanged = true
        AppConstants.LISTING_LOST = true
        self.colorsStackView.isHidden = false
        self.colorTwoView.isHidden = true
        self.colorsButton.isHidden = true
        self.selectedColors = selectedColors
        self.colorNameOneLabel.text = selectedColors[0].colorName
        self.visibleColorOneview.backgroundColor = UIColor(hex: selectedColors[0].hexCode!)
        if selectedColors.count == 2 {
            self.colorTwoView.isHidden = false
            self.colorNameTwoLabel.text = selectedColors[1].colorName
            self.visibleColorTwoview.backgroundColor = UIColor(hex: selectedColors[1].hexCode!)
        }
    }
    
    func onCategoryClick(categoryType: CategoryType, selectedCategory: Category) {
        self.valueChanged = true
        AppConstants.LISTING_LOST = true
        self.categoryButton.isHidden = true
        self.mainCategoryView.isHidden = false
        self.categoryLabel.text = categoryType.categoryTypeName
        self.categoryTypeLabel.text = selectedCategory.categoryName
        self.selectedCategory = categoryType
        self.selectedSubCategory = selectedCategory
        self.dismiss(animated: false, completion: nil)
        self.navigationController?.popViewController(animated: false)
    }
    
    func onPostTypeChange(type: Int) {
        if type == AppConstants.TYPE_DIRECT_PURCHASE{
            self.directPurchaseSwitch.isOn = true
            self.allowanceOfferView.isHidden = true
        }else{
            self.directPurchaseSwitch.isOn = false
        }
    }
    
    @IBOutlet var titleTextField: UITextField!
    @IBOutlet var descriptionTextView: UITextView!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var conditionSwitch: UISwitch!
    @IBOutlet var originalPriceTextField: UITextField!
    @IBOutlet var listingPriceTextField: UITextField!
    @IBOutlet var directPurchaseSwitch: UISwitch!
    @IBOutlet var allowanceOfferView: UIView!
    @IBOutlet var lowestAllowanceOfferTextField: UITextField!
    @IBOutlet var categoryButton: UIButton!
    @IBOutlet var categoryView: UIView!
    @IBOutlet var categoryTypeView: UIView!
    @IBOutlet var categoryLabel: UILabel!
    @IBOutlet var categoryTypeLabel: UILabel!
    @IBOutlet var sizeButton: UIButton!
    @IBOutlet var sizelabel: UILabel!
    @IBOutlet var sizeView: UIView!
    @IBOutlet var colorOneView: UIView!
    @IBOutlet var colorTwoView: UIView!
    @IBOutlet var visibleColorOneview: UIView!
    @IBOutlet var visibleColorTwoview: UIView!
    @IBOutlet var colorNameOneLabel: UILabel!
    @IBOutlet var colorNameTwoLabel: UILabel!
    @IBOutlet var colorsButton: UIButton!
    @IBOutlet var brandButton: UIButton!
    @IBOutlet var brandView: UIView!
    @IBOutlet var brandLabel: UILabel!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var screenScrollView: UIScrollView!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var mainCategoryView: UIView!
    @IBOutlet var mainSizeView: UIView!
    @IBOutlet var mainBrandView: UIView!
    @IBOutlet var colorsStackView: UIStackView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    var selectedCategory:CategoryType?
    var selectedSubCategory:Category?
    var selectedColors:[Colors]?
    var selectedBrand:Brand?
    var selectedImages:[AddListingImage]?
    var selectedSize:Sizes?
    let tagsHandler = AddTagsHandler()
    var valueChanged:Bool? = false
    var compressedVideoURL:URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.CREATE_LISTING_TITLE
        let sellerPolicyBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.SELLER_POLICY_ICON), style: .plain, target: self, action: #selector(onSellerPolicyBarButtonItem))
        self.navigationItem.rightBarButtonItem  = sellerPolicyBarButton
        let backBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.BACK_BUTTON), style: .plain, target: self, action: #selector(onBackBarButtonItem))
        self.navigationItem.leftBarButtonItem  = backBarButton
        screenActivityIndicator.stopAnimating()
        screenScrollView.isHidden = false
        collectionView.delegate = tagsHandler
        collectionView.dataSource = tagsHandler
        tagsHandler.dataObject.append(AddTags(tag:"", type:1))
        collectionView!.collectionViewLayout = FlowLayoutHelper()
        hideKeyboardWhenTappedAround()
        conditionSwitch.isOn = false
        directPurchaseSwitch.isOn = false
        titleTextField.delegate = self
        descriptionTextView.delegate = self
        originalPriceTextField.delegate = self
        listingPriceTextField.delegate = self
        lowestAllowanceOfferTextField.delegate = self
        titleTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        originalPriceTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        listingPriceTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        lowestAllowanceOfferTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        conditionSwitch.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
        directPurchaseSwitch.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
        tagsHandler.tagsContainsValueDelegate = self
        keyboardRequired(topConstraint: topConstraint)
    }

    @objc func switchChanged(mySwitch: UISwitch) {
        let conditionSwitch = self.conditionSwitch.isOn
        let directPurchaseSwitch = self.directPurchaseSwitch.isOn
        
        if directPurchaseSwitch || conditionSwitch{
            valueChanged = true
            AppConstants.LISTING_LOST = true
            
        }else{
            valueChanged = false
            AppConstants.LISTING_LOST = false
        }
        
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        let title = self.titleTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let originalPrice = self.originalPriceTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let listingPrice = self.listingPriceTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let lowestAllowableOffer = self.lowestAllowanceOfferTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let shortDesc = self.descriptionTextView.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        if title != "" || originalPrice != "" || listingPrice != "" || lowestAllowableOffer != "" || shortDesc != ""{
            valueChanged = true
            AppConstants.LISTING_LOST = true
        }else{
            valueChanged = false
            AppConstants.LISTING_LOST = false
        }
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField != self.titleTextField{
            
            guard let oldText = textField.text, let r = Range(range, in: oldText) else {
                return true
            }
            
            let newText = oldText.replacingCharacters(in: r, with: string)
            let isNumeric = newText.isEmpty || (Double(newText) != nil)
            let numberOfDots = newText.components(separatedBy: ".").count - 1
            
            let numberOfDecimalDigits: Int
            if let dotIndex = newText.firstIndex(of: ".") {
                numberOfDecimalDigits = newText.distance(from: dotIndex, to: newText.endIndex) - 1
            } else {
                numberOfDecimalDigits = 0
            }
            
            return isNumeric && numberOfDots <= 1 && numberOfDecimalDigits <= 2
        }else if textField == self.titleTextField{
            let maxLength = 150
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }else{
            return true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        conditionSwitch.layer.borderColor = UIColor.c696969.cgColor
        conditionSwitch.layer.borderWidth = 2
        conditionSwitch.layer.cornerRadius = 16
        directPurchaseSwitch.layer.borderColor = UIColor.c696969.cgColor
        directPurchaseSwitch.layer.borderWidth = 2
        directPurchaseSwitch.layer.cornerRadius = 16
        addBorderBlack(view:categoryView)
        addBorderBlack(view:categoryTypeView)
        addBorderBlack(view:sizeView)
        addBorderBlack(view:colorOneView)
        addBorderBlack(view:colorTwoView)
        addBorderBlack(view:brandView)
        colorRoundView(view:visibleColorOneview)
        colorRoundView(view:visibleColorTwoview)
        
    }
    
    @objc func onSellerPolicyBarButtonItem(){
        self.sellerPolicyScreen()
    }
    
    @objc func onBackBarButtonItem(){
        if  valueChanged!{
            let storyBoard = UIStoryboard(name: AppConstants.Storyboard.LISTING_LOST_DIALOG_BOARD, bundle: nil)
            let listingLostViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.LISTING_LOST_DIALOG) as! ListingLostDialogViewController
            listingLostViewController.onBackButtonPressDelegate = self
            listingLostViewController.TYPE = AppConstants.TYPE_OK_BUTTON
            self.navigationController?.present(listingLostViewController, animated: false)
        }else{
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case titleTextField:
            descriptionTextView.becomeFirstResponder()
        case descriptionTextView:
            originalPriceTextField.becomeFirstResponder()
        case originalPriceTextField:
            listingPriceTextField.becomeFirstResponder()
        case listingPriceTextField:
            lowestAllowanceOfferTextField.resignFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if descriptionTextView.text == AppConstants.Strings.SHORT_DESCRIPTION_ABOUT_ITEM_TEXT {
            descriptionTextView.text = nil
            descriptionTextView.textColor = UIColor.black
            descriptionTextView.font = UIFont.fontRegular(size: 18)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if descriptionTextView.text.isEmpty {
            descriptionTextView.text = AppConstants.Strings.SHORT_DESCRIPTION_ABOUT_ITEM_TEXT
            descriptionTextView.textColor = UIColor.cDCDCDC
            descriptionTextView.font = UIFont.fontRegular(size: 18)
        }
    }
    
    func addBorderBlack(view:UIView){
        view.layer.borderWidth = 1.0
        view.layer.borderColor = UIColor.c545454.cgColor
        view.layer.masksToBounds = true
    }
    
    func colorRoundView(view:UIView){
        view.layer.borderWidth = 0.5
        view.layer.masksToBounds = false
        view.layer.borderColor = UIColor.c2E2E2E.cgColor
        view.clipsToBounds = true
        view.layer.cornerRadius = view.frame.size.width/2
    }
    
    @IBAction func nextButtonAction(_ sender: UIButton) {
        nextButtonAction()
    }
    
    func nextButtonAction(){
        let title = self.titleTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let shortDesc = self.descriptionTextView.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let addtagsCount = self.tagsHandler.dataObject.count
        let originalPrice = self.originalPriceTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let listingPrice = self.listingPriceTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let lowestAllowableOffer = self.lowestAllowanceOfferTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if title == ""{
            let text:String = AppConstants.Strings.LISTING_TITLE_REQUIRED
            let textColor:String = AppConstants.Strings.LISTING_TITLE
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        if shortDesc == "" || shortDesc == AppConstants.Strings.SHORT_DESCRIPTION_ABOUT_ITEM_TEXT{
            let text:String = AppConstants.Strings.SHORT_DESC_REQUIRED
            let textColor:String = AppConstants.Strings.SHORT_DESCRIPTION_TEXT
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        if addtagsCount == 1{
            let text:String = AppConstants.Strings.LISTING_TAGS_REQUIRED
            let textColor:String = AppConstants.Strings.LISTING_TAGS
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        if originalPrice == ""{
            let text:String = AppConstants.Strings.ORIGINAL_PRICE_REQUIRED
            let textColor:String = AppConstants.Strings.ORIGINAL_PRICE
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }else if Int(originalPrice ?? "0") ?? 0 > 500000{
            let text:String = AppConstants.Strings.ORIGINAL_PRICE_LESS_THAN
            let textColor:String = AppConstants.Strings.ORIGINAL_PRICE
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        if listingPrice == ""{
            let text:String = AppConstants.Strings.LISTING_PRICE_REQUIRED
            let textColor:String = AppConstants.Strings.LISTING_PRICE
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }

        if listingPrice != "" {
            if Int(listingPrice ?? "0")! < 10{
                let text:String = AppConstants.Strings.LISTING_PRICE_TEN_DOLLAR_TEXT
                let textColor:String = AppConstants.Strings.LISTING_PRICE
                showCreatePostMessageDialog(text: text, textColor: textColor)
                return
            }else if Int(listingPrice ?? "0")! > 100000{
                let text:String = AppConstants.Strings.LISTING_PRICE_LESS_THAN
                let textColor:String = AppConstants.Strings.LISTING_PRICE
                showCreatePostMessageDialog(text: text, textColor: textColor)
                return
            }
        }
        
        if  lowestAllowableOffer != "" && Int(listingPrice ?? "0")! <= Int(lowestAllowableOffer ?? "1")!{
            let text:String = AppConstants.Strings.LOWEST_ALLOWABLE_OFFER
            let textColor:String = ""
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        
        if lowestAllowableOffer != "" && Int(lowestAllowableOffer ?? "1")! < 10{
            let text:String = AppConstants.Strings.LOWEST_OFFER_TEN_DOLLAR_TEXT
            let textColor:String = AppConstants.Strings.LOWEST_OFFER
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        
        if self.categoryButton.isHidden == false{
            let text:String = AppConstants.Strings.CATEGORY_REQUIRED
            let textColor:String = AppConstants.Strings.CATEGORY
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        if self.sizeButton.isHidden == false{
            let text:String = AppConstants.Strings.SIZE_REQUIRED
            let textColor:String = AppConstants.Strings.SIZE_TEXT
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        if self.colorsButton.isHidden == false{
            let text:String = AppConstants.Strings.COLOR_REQUIRED
            let textColor:String = AppConstants.Strings.COLORS
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        if self.brandButton.isHidden == false{
            let text:String = AppConstants.Strings.BRAND_REQUIRED
            let textColor:String = AppConstants.Strings.BRAND_TEXT
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        
        var conditionId:Int = 0
        
        if self.conditionSwitch.isOn {
            conditionId = 1
        }else{
            conditionId = 2
        }
        
//        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.LISTING_SHIPPING_BOARD, bundle:nil)
//        let viewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.SHIPPING_SCREEN) as! ListingShippingViewController
//        viewController.titleText = title
//        viewController.shortDescText = shortDesc
//        viewController.tagsArray = self.tagsHandler.dataObject
//
//
//        viewController.conditionId = conditionId
//        viewController.originalPrice = Int(self.originalPriceTextField.text ?? "")
//        viewController.listingPrice = Int(self.listingPriceTextField.text ?? "")
//        viewController.lowestAllowableOffer = Int(self.lowestAllowanceOfferTextField.text ?? "0")
//        viewController.categoryId = self.selectedCategory?.categoryTypeId
//        viewController.subCategoryId = self.selectedSubCategory?.categoryId
//        viewController.sizeId = self.selectedSize?.sizeId
//        viewController.colors = self.selectedColors
//        viewController.brandId = self.selectedBrand?.brandId
//        viewController.selectedImages = self.selectedImages
//        viewController.compressedVideoURL = self.compressedVideoURL
//        self.navigationController?.pushViewController(viewController, animated:false)
        
        calculateShippingScreen(shortDesc: shortDesc ?? "", conditionId: conditionId,title:title ?? "")
    }
    
    func calculateShippingScreen(shortDesc:String,conditionId:Int,title:String){
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.CALCULATE_SHIPPING_STORYBOARD, bundle:nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.CALCULATE_SHIPPING_SCREEN) as! CalculateShippingViewController
        viewController.TYPE = AppConstants.TYPE_ADD_LISTING
        viewController.titleText = title
        viewController.shortDescText = shortDesc
        viewController.tagsArray = self.tagsHandler.dataObject
        viewController.conditionId = conditionId
        viewController.originalPrice = Int(self.originalPriceTextField.text ?? "")
        viewController.listingPrice = Int(self.listingPriceTextField.text ?? "")
        viewController.lowestAllowableOffer = Int(self.lowestAllowanceOfferTextField.text ?? "0")
        viewController.categoryId = self.selectedCategory?.categoryTypeId
        viewController.subCategoryId = self.selectedSubCategory?.categoryId
        viewController.sizeId = self.selectedSize?.sizeId
        viewController.colors = self.selectedColors
        viewController.brandId = self.selectedBrand?.brandId
        viewController.selectedImages = self.selectedImages
        viewController.compressedVideoURL = self.compressedVideoURL
        self.navigationController?.pushViewController(viewController, animated:false)
    }
    
    @IBAction func categoryTapAction(_ sender: UITapGestureRecognizer) {
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.CATEGORY_BOARD, bundle:nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.CATEGORY_SCREEN) as! CategoryViewController
        viewController.categorySelectionDelegate = self
        viewController.categorySelected = self.selectedCategory
        viewController.subCategorySelected = self.selectedSubCategory
        self.navigationController?.pushViewController(viewController, animated:false)
    }
    
    @IBAction func sizeTapAction(_ sender: UITapGestureRecognizer) {
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.SIZE_TYPE_BOARD, bundle:nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.SIZES_TABS_SCREEN) as! SizeTypeViewController
        viewController.sizeSelectDelegate = self
        viewController.selectedSize = self.selectedSize
        self.navigationController?.pushViewController(viewController, animated:false)
    }
    
    @IBAction func colorOneTapAction(_ sender: UITapGestureRecognizer) {
        onColorClick()
    }
    
    @IBAction func colorTwoTapAction(_ sender: UITapGestureRecognizer) {
        onColorClick()
    }
    
    func onColorClick() {
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.COLORS_BOARD, bundle:nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.COLORS_SCREEN) as! ColorsViewController
        viewController.colorsSelectDelegate = self
        viewController.selectedColors  = self.selectedColors!
        self.navigationController?.pushViewController(viewController, animated:false)
    }
    
    @IBAction func brandTapAction(_ sender: UITapGestureRecognizer) {
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.BRANDS_BOARD, bundle:nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.BRANDS_SCREEN) as! BrandsViewController
        viewController.brandSelectDelegate = self
        viewController.selectedBrand = self.selectedBrand
        self.navigationController?.pushViewController(viewController, animated:false)
    }
    
    @IBAction func categoryButtonAction(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.CATEGORY_BOARD, bundle:nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.CATEGORY_SCREEN) as! CategoryViewController
        viewController.categorySelectionDelegate = self
        self.navigationController?.pushViewController(viewController, animated:false)
    }
    
    @IBAction func SizeButtonAction(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.SIZE_TYPE_BOARD, bundle:nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.SIZES_TABS_SCREEN) as! SizeTypeViewController
        viewController.sizeSelectDelegate = self
        self.navigationController?.pushViewController(viewController, animated:false)
    }
    
    @IBAction func colorsButtonAction(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.COLORS_BOARD, bundle:nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.COLORS_SCREEN) as! ColorsViewController
        viewController.colorsSelectDelegate = self
        self.navigationController?.pushViewController(viewController, animated:false)
    }
    
    @IBAction func brandButtonAction(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.BRANDS_BOARD, bundle:nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.BRANDS_SCREEN) as! BrandsViewController
        viewController.brandSelectDelegate = self
        self.navigationController?.pushViewController(viewController, animated:false)
    }
    
    @IBAction func onDirectPurchaseAction(_ sender: UISwitch) {
        
        if (self.directPurchaseSwitch?.isOn)!{
            let storyBoard = UIStoryboard(name: AppConstants.Storyboard.POST_CHECK_DIALOG_BOARD, bundle: nil)
            let postCheckViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.POST_CHECK_DIALOG) as! PostCheckDialogViewController
            postCheckViewController.delegate = self
            postCheckViewController.TYPE = AppConstants.TYPE_DIRECT_PURCHASE
            self.navigationController?.present(postCheckViewController, animated: false)
        }else{
            self.allowanceOfferView.isHidden = false
        }
        
    }
    
}
