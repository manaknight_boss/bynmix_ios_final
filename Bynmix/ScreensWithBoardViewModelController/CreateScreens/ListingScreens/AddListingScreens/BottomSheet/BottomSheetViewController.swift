import UIKit
import Photos

class BottomSheetViewController: UIViewController,CameraProtocol,VideoFormatProtocol,VideoAssetProtocol,VideoURLProtocol  {
    
    func videoFormat(index: Int) {
        if index == 0{
            self.cameraScreen()
        }else{
            self.view.makeToast("Video format or duration not correct.")
        }
    }
    
    func videoAsset(asset: AVAsset, duration: String, index: Int) {
        if index != 0{
            let durationInSec = duration.components(separatedBy: ":")
            if durationInSec.count > 1{
                let hour = Int(durationInSec[0]) ?? 0 * 60
                let seconds = Int(durationInSec[1]) ?? 0
                let total = hour + seconds
                //self.videoDuration = Float(total)
                self.setVideoDurationDelegate?.setVideoDurationMethod(totalTime: total)
            }
            self.loadAssetDelegate?.loadAssetMethod(asset: asset)
            //self.loadAsset(asset)
        }
    }
    
    func videoURL(url: URL, index: Int) {
        if index != 0{
            self.setVideoUrlInSheetDelegate?.setVideoUrlInSheetMethod(url: url)
        }
    }
    
    func CameraProtocol(url: URL) {
        self.dismiss(animated: false, completion: nil)
        let videoUrl = url.absoluteString
        let videoAsset: AVAsset = AVAsset(url: URL(string: videoUrl)!) as AVAsset
        self.setVideoUrlInSheetDelegate?.setVideoUrlInSheetMethod(url: url)
        //self.setVideoURL(url: url)
        //self.loadAsset(videoAsset)
        self.loadAssetDelegate?.loadAssetMethod(asset: videoAsset)
        
    }
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var imageDictionary = [String: AnyObject]()
    var videoDictionary = [String: AnyObject]()
    var videoHandler = VideoHandler()
    var loadAssetDelegate:LoadAssetProtocol?
    var setVideoDurationDelegate:SetVideoDurationProtocol?
    var setVideoUrlInSheetDelegate:SetVideoUrlInSheetProtocol?
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        let flowLayoutHelper = FlowLayoutHelper()
        flowLayoutHelper.horizontalSpacing = 0
        collectionView.collectionViewLayout = flowLayoutHelper
        collectionView.delegate = videoHandler
        collectionView.dataSource = videoHandler
        
        self.videoHandler.imageDictionary = self.imageDictionary
        self.videoHandler.videoDictionary = self.videoDictionary
        self.collectionView.reloadData()
        
        videoHandler.videoFormatDelegate =  self
        videoHandler.videoAssetDelegate = self
        videoHandler.videoURLDelegate = self
        
    }
    
    func cameraScreen(){
        let cameraViewController = CameraViewController()
        cameraViewController.modalPresentationStyle = .overFullScreen
        cameraViewController.camerasDelegate = self
        self.navigationController?.present(cameraViewController, animated: false, completion: nil)
    }
    
}
