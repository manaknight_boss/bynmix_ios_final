import UIKit

class BrandsViewController: BaseViewController,LoadMoreProtocol {
    
    func onLoadMore() {
        if !isApiCalled {
            isApiCalled = true
            self.getBrands()
        }
    }
    
    @IBOutlet var searchView: UIView!
    @IBOutlet var searchTextField: UITextField!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    
    let createViewModel = CreateViewModel()
    let brandsHandler = BrandsHandler()
    var allBrands : [Brand] = []
    var brandsList : [Brand] = []
    var selectedBrand:Brand?
    var brandSelectDelegate:BrandsSelectProtocol?
    var filteredBrands: [Brand] = []
    var currentPage = 1
    var isApiCalled = false
    var refreshControl : UIRefreshControl?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.Strings.SELECT_A_BRAND_TITLE
        let sellerPolicyBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.SELLER_POLICY_ICON), style: .plain, target: self, action: #selector(onSellerPolicyBarButtonItem))
        self.navigationItem.rightBarButtonItem  = sellerPolicyBarButton
        tableView.delegate = brandsHandler
        tableView.dataSource = brandsHandler
        getBrands()
        brandClick()
        searchTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        let backBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.BACK_BUTTON), style: .plain, target: self, action: #selector(onBackBarButtonItem))
        self.navigationItem.leftBarButtonItem  = backBarButton
        brandsHandler.loadMoreDelegate = self
        addRefreshControl()
    }
    
    @objc func onBackBarButtonItem(){
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
                //let text = textField.text
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reloadBrands), object: nil)
        self.perform(#selector(self.reloadBrands), with: nil, afterDelay: 0.6)
        //        filteredBrands.removeAll()
        //        for brand in allBrands {
        //            if (brand.brandName?.lowercased().starts(with: (text?.lowercased())!))! {
        //                filteredBrands.append(brand)
        //            }
        //        }
        //
        //        showBrands(brands: &filteredBrands)
        
    }
    
    @objc func reloadBrands() {
        self.view.makeToastActivity(.center)
        currentPage = 1
        self.getBrands(search: self.searchTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines))
    }
    
    //    func brandsIndexTiles(){
    //        brandsHandler.brandsSectionTitles.removeAll()
    //        brandsHandler.brandsDictionary.removeAll()
    //        for brand in brandsHandler.brands {
    //            let brandKey = String(brand.brandName!.prefix(1))
    //            if var brandValues = brandsHandler.brandsDictionary[brandKey] {
    //                brandValues.append(brand)
    //                brandsHandler.brandsDictionary[brandKey] = brandValues
    //            } else {
    //                brandsHandler.brandsDictionary[brandKey] = [brand]
    //            }
    //        }
    //
    //        brandsHandler.brandsSectionTitles = [String](brandsHandler.brandsDictionary.keys)
    //        brandsHandler.brandsSectionTitles = brandsHandler.brandsSectionTitles.sorted(by: { $0 < $1 })
    //
    //    }
    
    func brandClick(){
        brandsHandler.onBrandsClick = { brand in
            self.selectedBrand = brand
            self.brandSelectDelegate?.brandsSelect(brand: brand)
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    @objc func onSellerPolicyBarButtonItem(){
        self.sellerPolicyScreen()
    }
    
    func getBrands(search:String? = nil) {
        createViewModel.getBrands(accessToken: getAccessToken(), search: search ?? "", page: currentPage,{(response) in
            self.isApiCalled = false
            self.refreshControl?.endRefreshing()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Brand> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let brand = data.data!
                    
                    if brand.items!.count > 0{
                        if var brands = brand.items {
                            //self.allBrands = brands
                            
                            for i in 0..<brands.count{
                                if self.selectedBrand != nil && brands[i].brandId == self.selectedBrand?.brandId {
                                    brands[i].isSelected = true
                                } else {
                                    brands[i].isSelected = false
                                }
                                //self.brandsList.append(brand)
                            }
                            
                            if self.currentPage == 1 {
                                self.brandsHandler.brands = brands
                            } else {
                                self.brandsHandler.brands.append(contentsOf: brands)
                            }
                            if brand.totalPages == self.currentPage {
                                self.isApiCalled = true
                                self.brandsHandler.isLoadMoreRequired = false
                            }
                            self.showBrands(brands: brands)
                        }
                    }else{
                        self.brandsHandler.brands.removeAll()
                        self.tableView.reloadData()
                        self.view.hideToastActivity()
                        self.refreshControl?.endRefreshing()
                    }
                }
                self.currentPage = self.currentPage + 1
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.screenActivityIndicator.stopAnimating()
                self.view.hideToastActivity()
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            self.view.hideToastActivity()
            self.refreshControl?.endRefreshing()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
//    func showBrands( brands: inout [Brand]) {
//        self.brandsList.removeAll()
//        for i in 0..<brands.count{
//            var brand = brands[i]
//            if self.selectedBrand != nil && brands[i].brandId == self.selectedBrand?.brandId {
//                brand.isSelected = true
//            } else {
//                brand.isSelected = false
//            }
//            brand.brandName = brand.brandName!.uppercased()
//            self.brandsList.append(brand)
//        }
//
//        self.brandsHandler.brands = self.brandsList
//        //self.brandsIndexTiles()
//        self.tableView.reloadData()
//        self.screenActivityIndicator.stopAnimating()
//        self.searchView.isHidden = false
//        self.tableView.isHidden = false
//    }
    
    func showBrands( brands:[Brand]) {
    
        //self.brandsHandler.brands = self.brandsList
        self.tableView.reloadData()
        self.screenActivityIndicator.stopAnimating()
        self.searchView.isHidden = false
        self.tableView.isHidden = false
        self.view.hideToastActivity()
    }
    
    func addRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        tableView.addSubview(refreshControl!)
    }
    
    @objc func refreshList(){
        currentPage = 1
        isApiCalled = false
        self.brandsHandler.isLoadMoreRequired = true
        self.getBrands()
    }
    
}

