import UIKit
import XLPagerTabStrip

class CalculateShippingViewController: BaseViewController, ShippingRateProtocol, GetRateProtocol {
    
    func getRate(shippingInfo: ShippingInfo) {
        self.shippingInfo = shippingInfo
        self.shippingRateId = shippingInfo.shippingRateId ?? 0
        self.setFlatRateView(shippingInfo: shippingInfo)
        self.navigationController?.popViewController(animated: false)
    }
    
    func shippingRate(type:Int){
        
        let pounds = Double(poundsTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let ounces = Double(ouncesTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let weightTotal = pounds + (ounces * 0.0625)
        
        let length = Double(lengthTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let width = Double(widthTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let height = Double(heightTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        
        if type == AppConstants.TYPE_FREE_SHIPPING{
            self.freeShippingCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_SELECTED_PINK_ICON)
            self.calculateShippingCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_UNSELECTED_PINK_ICON)
            self.flatRateCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_UNSELECTED_PINK_ICON)
            freeShippingNoteView.isHidden = false
            calculateShippingNoteView.isHidden = true
            flatRateMainView.isHidden = true
            self.shippingType = AppConstants.SHIPPING_TYPE_FREE
            self.shippingInfo = nil
            self.checkFreeShippingAndCalculateShipping(pounds: pounds, ounces: ounces, weightTotal: weightTotal, length: length, width: width, height: height)
        }else if type == AppConstants.TYPE_CALCULATED_SHIPPING_RATE{
            self.calculateShippingCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_SELECTED_PINK_ICON)
            self.freeShippingCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_UNSELECTED_PINK_ICON)
            self.flatRateCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_UNSELECTED_PINK_ICON)
            freeShippingNoteView.isHidden = true
            calculateShippingNoteView.isHidden = false
            flatRateMainView.isHidden = true
            self.shippingType = AppConstants.SHIPPING_TYPE_CALCULATE_SHIPPING_IN_APP
            self.shippingInfo = nil
            self.checkFreeShippingAndCalculateShipping(pounds: pounds, ounces: ounces, weightTotal: weightTotal, length: length, width: width, height: height)
        }else{
            self.flatRateCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_SELECTED_PINK_ICON)
            self.calculateShippingCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_UNSELECTED_PINK_ICON)
            self.freeShippingCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_UNSELECTED_PINK_ICON)
            freeShippingNoteView.isHidden = true
            calculateShippingNoteView.isHidden = true
            flatRateMainView.isHidden = false
            self.shippingType = AppConstants.SHIPPING_TYPE_FLATE_RATE
            checkFlatRate(pounds: pounds, ounces: ounces, weightTotal: weightTotal, length: length, width: width, height: height)
        }
        shippingMainView.isHidden = true
        //self.isShippingMethodChanged = true
    }
    
    @IBOutlet var poundsTextField: UITextField!
    @IBOutlet var poundsView: UIView!
    @IBOutlet var ouncesView: UIView!
    @IBOutlet var ouncesTextField: UITextField!
    @IBOutlet var weightErrorMsgLabel: UILabel!
    
    @IBOutlet var lengthView: UIView!
    @IBOutlet var lengthTextField: UITextField!
    @IBOutlet var widthView: UIView!
    @IBOutlet var widthTextField: UITextField!
    @IBOutlet var heightView: UIView!
    @IBOutlet var heightTextField: UITextField!
    
    @IBOutlet var doneButton:UIButton!
    
    @IBOutlet weak var freeShippingCheckBoxImageView: UIImageView!
    @IBOutlet weak var calculateShippingCheckBoxImageView: UIImageView!
    @IBOutlet weak var flatRateCheckBoxImageView: UIImageView!
    @IBOutlet weak var freeShippingNoteLabel: UILabel!
    @IBOutlet weak var calculateShippingNoteLabel: UILabel!
    @IBOutlet weak var flatRateView: UIView!
    @IBOutlet weak var flatRateMainView: UIView!
    
    @IBOutlet weak var freeShippingNoteView: UIView!
    @IBOutlet weak var calculateShippingNoteView: UIView!
    
    @IBOutlet weak var flatRateErrorImageView: UIImageView!

    @IBOutlet weak var flatRateSelectedView: UIView!
    @IBOutlet weak var flatRateShadowView: UIView!
    @IBOutlet weak var flatRateInnerView: UIView!
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var classLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var deliveryTimeLabel: UILabel!
    
    @IBOutlet weak var shippingMainView: UIView!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var firstSeperatorView: UIView!
    
    @IBOutlet weak var flateRateLabel: UILabel!
    @IBOutlet weak var lengthErrorMsgLabel: UILabel!
    
    @IBOutlet weak var doneView: UIView!

    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var noteLabel: UILabel!

    var TYPE:Int?
    
    var selectedImages:[AddListingImage]?
    var titleText:String?
    var shortDescText:String?
    var tagsArray = [AddTags]()
    var conditionId: Int?
    var originalPrice:Int?
    var listingPrice:Int?
    var lowestAllowableOffer:Int?
    var categoryId:Int?
    var subCategoryId:Int?
    var sizeId:Int?
    var colors:[Colors]?
    var brandId:Int?
    let createViewModel = CreateViewModel()
    var shippingRateId:Int = 0

    var shippingInfo : ShippingInfo?
    var listingDetail:ListingDetail?
    let profileViewModel = ProfileViewModel()
    var shippingHandler = ShippingHandler()
    var compressedVideoURL:URL?
    
    var shippingType = 0
    var shippingWeightInDetail:ShippingWeightInDetail?
    var isShippingMethodChanged = false
    var listingDetailRefreshDelegate:ListingDetailRefreshProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        onTypeChange()
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        self.addShadowToView(shadowView:self.flatRateShadowView)
        let pounds = Double(poundsTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let ounces = Double(ouncesTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let weightTotal = pounds + (ounces * 0.0625)
        
        let length = Double(lengthTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let width = Double(widthTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let height = Double(heightTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        
        if shippingType == AppConstants.SHIPPING_TYPE_FREE || shippingType == AppConstants.SHIPPING_TYPE_CALCULATE_SHIPPING_IN_APP{
         
            self.checkFreeShippingAndCalculateShipping(pounds:pounds,ounces:ounces,weightTotal:weightTotal,length:length,width:width,height:height)
            
        }else if shippingType == AppConstants.SHIPPING_TYPE_FLATE_RATE{
            self.checkFlatRate(pounds:pounds,ounces:ounces,weightTotal:weightTotal,length:length,width:width,height:height)
            
        }else{
            doneButtonDisabled()
        }
        
    }
    
    func onTypeChange(){
        
        poundsTextField.addTarget(self, action: #selector(CalculateShippingViewController.textFieldDidChange(_:)), for: .editingChanged)
        ouncesTextField.addTarget(self, action: #selector(CalculateShippingViewController.textFieldDidChange(_:)), for: .editingChanged)
        lengthTextField.addTarget(self, action: #selector(CalculateShippingViewController.textFieldDidChange(_:)), for: .editingChanged)
        widthTextField.addTarget(self, action: #selector(CalculateShippingViewController.textFieldDidChange(_:)), for: .editingChanged)
        heightTextField.addTarget(self, action: #selector(CalculateShippingViewController.textFieldDidChange(_:)), for: .editingChanged)
        
        let sellerPolicyBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.SELLER_POLICY_ICON), style: .plain, target: self, action: #selector(onSellerPolicyBarButtonItem))
        self.navigationItem.rightBarButtonItem  = sellerPolicyBarButton
        
        freeShippingCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_UNSELECTED_PINK_ICON)
        calculateShippingCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_UNSELECTED_PINK_ICON)
        flatRateCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_UNSELECTED_PINK_ICON)
        
        freeShippingCheckBoxImageView.contentMode = .scaleAspectFit
        calculateShippingCheckBoxImageView.contentMode = .scaleAspectFit
        flatRateCheckBoxImageView.contentMode = .scaleAspectFit
        
        freeShippingNoteView.isHidden = true
        calculateShippingNoteView.isHidden = true
        flatRateMainView.isHidden = true
        weightErrorMsgLabel.isHidden = true
        flatRateErrorImageView.isHidden = true
        shippingMainView.isHidden = true
        firstSeperatorView.isHidden = false
        lengthErrorMsgLabel.isHidden = true
        setFreeShippingNoteBold()
        setCalculateShippingInAppNoteBold()
        setNoteLabelBold()
        
        if TYPE == AppConstants.TYPE_EDIT_LISTING{
            navigationItem.title = AppConstants.Strings.EDIT_LISTING_TITLE
            topConstraint.constant = 56
            doneButton.setTitle(AppConstants.UPDATE, for: .normal)
            doneButtonEnabled()
            setEditListingData()
        }else{
            navigationItem.title = AppConstants.CREATE_LISTING_TITLE
            topConstraint.constant = 15
            doneButtonDisabled()
            doneButton.setTitle(AppConstants.Strings.DONE, for: .normal)
        }
        
    }
    
    func setEditListingData(){
        poundsTextField.text = "\(self.listingDetail?.shippingWeightPounds ?? 0)"
        ouncesTextField.text = "\(self.listingDetail?.shippingWeightOunces ?? 0)"
        lengthTextField.text = "\(self.listingDetail?.shippingLength ?? 0)"
        widthTextField.text = "\(self.listingDetail?.shippingWidth ?? 0)"
        heightTextField.text = "\(self.listingDetail?.shippingHeight ?? 0)"
        if self.listingDetail?.shippingType == AppConstants.SHIPPING_TYPE_FREE{
            self.shippingType = AppConstants.SHIPPING_TYPE_FREE
            freeShippingCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_SELECTED_PINK_ICON)
            freeShippingNoteView.isHidden = false
        }else if self.listingDetail?.shippingType == AppConstants.SHIPPING_TYPE_CALCULATE_SHIPPING_IN_APP{
            self.shippingType = AppConstants.SHIPPING_TYPE_CALCULATE_SHIPPING_IN_APP
            calculateShippingCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_SELECTED_PINK_ICON)
            calculateShippingNoteView.isHidden = false
        }else{
            self.shippingType = AppConstants.SHIPPING_TYPE_FLATE_RATE
            flatRateCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_SELECTED_PINK_ICON)
            self.shippingInfo = self.listingDetail?.shippingInfo
            self.shippingRateId = self.listingDetail?.shippingInfo?.shippingRateId ?? 0
            self.addShadowToView(shadowView:self.flatRateShadowView)
            self.setFlatRateView(shippingInfo: (self.listingDetail?.shippingInfo)!)
        }
    }
    
    func setNoteLabelBold(){
        let text:String = "NOTE: Be sure to weigh your item and select the correct shipping rate to prevent postal carrier returns."
        let textExtraBold:String = "NOTE"
        let range = (text as NSString).range(of: textExtraBold)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontExtraBold(size: 12), range: range)
        noteLabel.attributedText = attributedString
    }
    
    func setFreeShippingNoteBold(){
        let text:String = "NOTE: By offering free shipping, Bynmix will calculate and charge you the best and cheapest priority shipping between the buyer’s shipping address and your shipping address."
        let textExtraBold:String = "NOTE"
        let range = (text as NSString).range(of: textExtraBold)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontExtraBold(size: 12), range: range)
        freeShippingNoteLabel.attributedText = attributedString
    }
    
    func setCalculateShippingInAppNoteBold(){
        let text:String = "NOTE: Enabling calculated shipping in app lets the buyer choose a shipping method of their choice. You will need to make sure that you ship the order by the selected shipping method."
        let textExtraBold:String = "NOTE"
        let range = (text as NSString).range(of: textExtraBold)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontExtraBold(size: 12), range: range)
        calculateShippingNoteLabel.attributedText = attributedString
    }
    
    @objc func onSellerPolicyBarButtonItem(){
        self.sellerPolicyScreen()
    }
    
    @IBAction func doneButtonAction(_ sender:UIButton){
        if self.TYPE == AppConstants.TYPE_EDIT_LISTING{
            self.updateButton()
        }else{
            self.uploadListing()
        }
    }
    
    @IBAction func shippingMainViewTapAction(_ sender: UITapGestureRecognizer) {
        
        let pounds = Double(poundsTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let ounces = Double(ouncesTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        
        let length = Double(lengthTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let width = Double(widthTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let height = Double(heightTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        
        self.shippingWeightInDetail = ShippingWeightInDetail(pounds: pounds, ounces: ounces, length: length, width: width, height: height)
        
        self.selectShippingRateScreen(shippingWeightInDetail: shippingWeightInDetail!)
    }
    
    func doneButtonEnabled(){
        self.doneButton.backgroundColor = UIColor.cBB189C
        self.doneButton.isEnabled = true
    }
    
    func doneButtonDisabled(){
        self.doneButton.backgroundColor = UIColor.cCBCBCB
        self.doneButton.isEnabled = false
    }
    
    func determineShippingRate(type:Int,listingTitle:String){
        print("type:",type,"listingTitle:",listingTitle)
        let storyboard = UIStoryboard(name:AppConstants.Storyboard.LISTING_LOST_DIALOG_BOARD, bundle: nil)
        let shippingRateViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.LISTING_LOST_DIALOG) as! ListingLostDialogViewController
        shippingRateViewController.modalPresentationStyle = .overFullScreen
        shippingRateViewController.TYPE = type
        shippingRateViewController.listingTitle = listingTitle
        shippingRateViewController.shippingRateDelegate = self
        self.navigationController?.present(shippingRateViewController, animated: false, completion: nil)
    }
    
    @IBAction func freeShippingTapAction(_ sender: UITapGestureRecognizer) {
        
        if self.TYPE == AppConstants.TYPE_EDIT_LISTING{
            if shippingType != AppConstants.SHIPPING_TYPE_FREE && !isShippingMethodChanged{
                // open dialog
                let title = self.listingDetail?.title ?? ""
                let type = AppConstants.TYPE_FREE_SHIPPING
                self.determineShippingRate(type:type,listingTitle:title)
            }else{
                self.typeCreateFreeShipping()
            }
        }else{
            self.typeCreateFreeShipping()
        }
        
    }
    
    func typeCreateFreeShipping(){
        calculateShippingCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_UNSELECTED_PINK_ICON)
        flatRateCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_UNSELECTED_PINK_ICON)
        
        calculateShippingNoteView.isHidden = true
        flatRateMainView.isHidden = true
        shippingMainView.isHidden = true
        self.shippingInfo = nil
        
        if freeShippingCheckBoxImageView.image == UIImage(named: AppConstants.Images.CHECKBOX_UNSELECTED_PINK_ICON){
            freeShippingCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_SELECTED_PINK_ICON)
            freeShippingNoteView.isHidden = false
            shippingType = AppConstants.SHIPPING_TYPE_FREE
        }else{
            freeShippingCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_UNSELECTED_PINK_ICON)
            freeShippingNoteView.isHidden = true
            shippingType = 0
            doneButtonDisabled()
        }
        let pounds = Double(poundsTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let ounces = Double(ouncesTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let weightTotal = pounds + (ounces * 0.0625)
        
        let length = Double(lengthTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let width = Double(widthTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let height = Double(heightTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        self.checkFreeShippingAndCalculateShipping(pounds:pounds,ounces:ounces,weightTotal:weightTotal,length:length,width:width,height:height)
    }
    
    @IBAction func calculateShippingTapAction(_ sender: UITapGestureRecognizer) {
        
        if self.TYPE == AppConstants.TYPE_EDIT_LISTING{
            if shippingType != AppConstants.SHIPPING_TYPE_CALCULATE_SHIPPING_IN_APP && !isShippingMethodChanged{
                // open dialog
                let title = self.listingDetail?.title ?? ""
                let type = AppConstants.TYPE_CALCULATED_SHIPPING_RATE
                self.determineShippingRate(type:type,listingTitle:title)
            }else{
                self.typeCreateCalculateRate()
            }
        }else{
            self.typeCreateCalculateRate()
        }
        
    }
    
    func typeCreateCalculateRate(){
        freeShippingCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_UNSELECTED_PINK_ICON)
        flatRateCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_UNSELECTED_PINK_ICON)
        
        freeShippingNoteView.isHidden = true
        flatRateMainView.isHidden = true
        shippingMainView.isHidden = true
        self.shippingInfo = nil
        
        if calculateShippingCheckBoxImageView.image == UIImage(named: AppConstants.Images.CHECKBOX_UNSELECTED_PINK_ICON){
            calculateShippingCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_SELECTED_PINK_ICON)
            calculateShippingNoteView.isHidden = false
            shippingType = AppConstants.SHIPPING_TYPE_CALCULATE_SHIPPING_IN_APP
        }else{
            calculateShippingCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_UNSELECTED_PINK_ICON)
            calculateShippingNoteView.isHidden = true
            shippingType = 0
            doneButtonDisabled()
        }
        
        let pounds = Double(poundsTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let ounces = Double(ouncesTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let weightTotal = pounds + (ounces * 0.0625)
        
        let length = Double(lengthTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let width = Double(widthTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let height = Double(heightTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        self.checkFreeShippingAndCalculateShipping(pounds:pounds,ounces:ounces,weightTotal:weightTotal,length:length,width:width,height:height)
    }
    
    @IBAction func flateRateTapAction(_ sender: UITapGestureRecognizer) {
        
        if self.TYPE == AppConstants.TYPE_EDIT_LISTING{
            if shippingType != AppConstants.SHIPPING_TYPE_FLATE_RATE && !isShippingMethodChanged{
                // open dialog
                let title = self.listingDetail?.title ?? ""
                let type = AppConstants.TYPE_FLAT_RATE
                self.determineShippingRate(type:type,listingTitle:title)
            }else{
                self.typeCreateFlatRate()
            }
        }else{
            self.typeCreateFlatRate()
        }
        
    }
    
    func typeCreateFlatRate(){
        freeShippingCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_UNSELECTED_PINK_ICON)
        calculateShippingCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_UNSELECTED_PINK_ICON)
        
        freeShippingNoteView.isHidden = true
        calculateShippingNoteView.isHidden = true
        
        if flatRateCheckBoxImageView.image == UIImage(named: AppConstants.Images.CHECKBOX_UNSELECTED_PINK_ICON){
            flatRateCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_SELECTED_PINK_ICON)
            flatRateMainView.isHidden = false
            shippingType = AppConstants.SHIPPING_TYPE_FLATE_RATE
        }else{
            flatRateCheckBoxImageView.image = UIImage(named: AppConstants.Images.CHECKBOX_UNSELECTED_PINK_ICON)
            flatRateMainView.isHidden = true
            shippingType = 0
            doneButtonDisabled()
        }
        let pounds = Double(poundsTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let ounces = Double(ouncesTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let weightTotal = pounds + (ounces * 0.0625 )
        
        let length = Double(lengthTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let width = Double(widthTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let height = Double(heightTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        
        self.checkFlatRate(pounds:pounds,ounces:ounces,weightTotal:weightTotal,length:length,width:width,height:height)
    }
    

    @IBAction func flateRateViewTapAction(_ sender: UITapGestureRecognizer) {
        let pounds = Double(poundsTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let ounces = Double(ouncesTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        
        let length = Double(lengthTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let width = Double(widthTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let height = Double(heightTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        
        self.shippingWeightInDetail = ShippingWeightInDetail(pounds: pounds, ounces: ounces, length: length, width: width, height: height)
        selectShippingRateScreen(shippingWeightInDetail:shippingWeightInDetail!)
    }
    
    func flatRateViewEnabled(){
        flatRateView.isUserInteractionEnabled = true
        flatRateView.backgroundColor = UIColor.cBB189C
        flateRateLabel.text = "SELECT SHIPPING RATE"
    }
    
    func flatRateViewDisabled(){
        flatRateView.isUserInteractionEnabled = false
        flatRateView.backgroundColor = UIColor.cCBCBCB
        flateRateLabel.text = "FILL IN WEIGHT & DIMENTIONS BEFORE SELECTING A FLAT SHIPPING RATE"
    }
    
    func weightExceeds(){
        poundsView.backgroundColor = UIColor.cFFD8D8
        poundsTextField.backgroundColor = UIColor.cFFD8D8
        poundsTextField.textColor = UIColor.cAC0000
        
        ouncesView.backgroundColor = UIColor.cFFD8D8
        ouncesTextField.backgroundColor = UIColor.cFFD8D8
        ouncesTextField.textColor = UIColor.cAC0000
        
        weightErrorMsgLabel.isHidden = false
        
    }
    
    func weightNormal(){
        poundsView.backgroundColor = UIColor.white
        poundsTextField.backgroundColor = UIColor.white
        poundsTextField.textColor = UIColor.c383838
        
        ouncesView.backgroundColor = UIColor.white
        ouncesTextField.backgroundColor = UIColor.white
        ouncesTextField.textColor = UIColor.white
        
        weightErrorMsgLabel.isHidden = true
        
    }
    
    func lengthNotCorrect(){
        lengthView.backgroundColor = UIColor.cFFD8D8
        lengthTextField.backgroundColor = UIColor.cFFD8D8
        lengthTextField.textColor = UIColor.cAC0000
        lengthErrorMsgLabel.isHidden = false
    }
    
    func lengthCorrect(){
        lengthView.backgroundColor = UIColor.white
        lengthTextField.backgroundColor = UIColor.white
        lengthTextField.textColor = UIColor.c383838
        lengthErrorMsgLabel.isHidden = true
    }
    
    func widthNotCorrect(){
        widthView.backgroundColor = UIColor.cFFD8D8
        widthTextField.backgroundColor = UIColor.cFFD8D8
        widthTextField.textColor = UIColor.c383838
        lengthErrorMsgLabel.isHidden = false
        //lengthErrorMsgLabel.text = "Width Exceeds maximum dimensions of selected shipping rate"
    }
    
    func widthCorrect(){
        widthView.backgroundColor = UIColor.white
        widthTextField.backgroundColor = UIColor.white
        widthTextField.textColor = UIColor.c383838
        lengthErrorMsgLabel.isHidden = true
    }
    
    func heightNotCorrect(){
        heightView.backgroundColor = UIColor.cFFD8D8
        heightTextField.backgroundColor = UIColor.cFFD8D8
        heightTextField.textColor = UIColor.c383838
        lengthErrorMsgLabel.isHidden = false
        //lengthErrorMsgLabel.text = "Height Exceeds maximum dimensions of selected shipping rate"
    }
    
    func heightCorrect(){
        heightView.backgroundColor = UIColor.white
        heightTextField.backgroundColor = UIColor.white
        heightTextField.textColor = UIColor.c383838
        lengthErrorMsgLabel.isHidden = true
    }
    
    func selectShippingRateScreen(shippingWeightInDetail:ShippingWeightInDetail){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.SELECT_SHIPPING_RATE_STORYBOARD, bundle: nil)
        let shippingRateViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.SHIPPING_RATE_SCREEN) as! SelectShippingRateViewController
        shippingRateViewController.shippingWeightInDetail = shippingWeightInDetail
        shippingRateViewController.TYPE = self.TYPE ?? 0
        if self.shippingInfo != nil{
            shippingRateViewController.selectedRate = self.shippingInfo
            shippingRateViewController.isShippingInfoNil = false
        }else{
            shippingRateViewController.isShippingInfoNil = true
        }
        shippingRateViewController.getRateDelegate = self
        self.navigationController?.pushViewController(shippingRateViewController, animated: false)
        
    }
    
    func checkFreeShippingAndCalculateShipping(pounds:Double,ounces:Double,weightTotal:Double,length:Double,width:Double,height:Double){
        
        if weightTotal <= 0{
            doneButtonDisabled()
        }else if length <= 0{
            doneButtonDisabled()
        }else if width <= 0{
            doneButtonDisabled()
        }else if height <= 0{
            doneButtonDisabled()
        }else if length >= 27{
            doneButtonDisabled()
            lengthNotCorrect()
            lengthErrorMsgLabel.text = "Length must be less than 27 inches"
        }else if shippingType <= 0{
            doneButtonDisabled()
        }else if poundsTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == nil{
            doneButtonDisabled()
        }else{
            doneButtonEnabled()
            lengthCorrect()
        }
        print(pounds,ounces,weightTotal,length,width,height)
    }
    
    func checkFlatRate(pounds:Double,ounces:Double,weightTotal:Double,length:Double,width:Double,height:Double){
        if self.shippingInfo != nil{
            if length >= 26{
                doneButtonDisabled()
                lengthNotCorrect()
                lengthErrorMsgLabel.text = "Length must be less than 27 inches"
                //flatRateViewDisabled()
            }else if weightTotal > (self.shippingInfo?.shippingWeightOunceMax ?? 0) {
                doneButtonDisabled()
                weightExceeds()
                self.rateIncorrect()
                //flatRateViewDisabled()
            }else if length >  (self.shippingInfo?.maxLengthDimension ?? 0){
                doneButtonDisabled()
                lengthNotCorrect()
                lengthErrorMsgLabel.text = "Length Exceeds maximum dimensions of selected shipping rate"
                //flatRateViewDisabled()
            }else if width >  (self.shippingInfo?.maxWidthDimension ?? 0){
                doneButtonDisabled()
                widthNotCorrect()
                lengthErrorMsgLabel.text = "Width Exceeds maximum dimensions of selected shipping ratees"
                //flatRateViewDisabled()
            }else if height >  (self.shippingInfo?.maxHeightDimension ?? 0){
                doneButtonDisabled()
                heightNotCorrect()
                lengthErrorMsgLabel.text = "Height Exceeds maximum dimensions of selected shipping rate"
                //flatRateViewDisabled()
            }else{
                doneButtonEnabled()
                lengthCorrect()
                widthCorrect()
                heightCorrect()
                self.rateCorrect()
                //flatRateViewEnabled()
            }
        }else{
            if weightTotal <= 0{
                doneButtonDisabled()
                flatRateViewDisabled()
                flatRateViewDisabled()
            }else if length <= 0{
                doneButtonDisabled()
                flatRateViewDisabled()
            }else if width <= 0{
                doneButtonDisabled()
                flatRateViewDisabled()
            }else if height <= 0{
                doneButtonDisabled()
                flatRateViewDisabled()
            }else if length >= 27{
                doneButtonDisabled()
                lengthNotCorrect()
                lengthErrorMsgLabel.text = "Length must be less than 27 inches"
                flatRateViewDisabled()
            }else{
                //doneButtonEnabled()
                lengthCorrect()
                flatRateViewEnabled()
                widthCorrect()
                heightCorrect()
            }
        }
    }
    
    func setFlatRateView(shippingInfo: ShippingInfo){
        //rateCorrect()
        self.priceLabel.text = "$\(shippingInfo.rate ?? 0)"
        self.classLabel.text = shippingInfo.shippingType?.type ?? ""
        self.weightLabel.text = "(\(Int(shippingInfo.shippingWeightOunceMin ?? 0)) - \(Int(shippingInfo.shippingWeightOunceMax ?? 0)) lbs)"
        self.deliveryTimeLabel.text = (shippingInfo.shippingType?.description ?? "")
        
        self.flatRateMainView.isHidden = true
        self.shippingMainView.isHidden = false
        
        let pounds = Double(poundsTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let ounces = Double(ouncesTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let weightTotal = pounds + ounces
        
        let length = Double(lengthTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let width = Double(widthTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let height = Double(heightTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        
        self.checkFlatRate(pounds: pounds, ounces: ounces, weightTotal: weightTotal, length: length, width: width, height: height)
    }
    
    func addShadowToView(shadowView:UIView){
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.alpha = CGFloat(0.90)
        shadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        shadowView.layer.shadowOpacity = 1
    }
    
    func rateIncorrect(){
        self.flatRateSelectedView.backgroundColor = UIColor.cAA0000
        self.flatRateInnerView.backgroundColor = UIColor.cFFD8D8
        self.flatRateErrorImageView.isHidden = false
        self.doneButtonDisabled()
    }
    
    func rateCorrect(){
        self.flatRateSelectedView.backgroundColor = UIColor.c272727
        self.flatRateInnerView.backgroundColor = UIColor.white
        self.flatRateErrorImageView.isHidden = true
    }
    
    func updateButton(){
        var listingData : ListingDetail?
        let shortDescription = self.listingDetail?.description
        let pounds = Int(poundsTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let ounces = Int(ouncesTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        
        let length = Double(lengthTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let width = Double(widthTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let height = Double(heightTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        
        if shortDescription != nil{
            
            if self.shippingRateId == 0{
                listingData = ListingDetail(description: self.listingDetail?.description ?? "",shippingWeightPounds:pounds,shippingWeightOunces:ounces,shippingLength:length,shippingHeight:height,shippingDepth:width,shippingType:self.shippingType)
            }else{
                listingData = ListingDetail(shippingRateId: self.shippingRateId,description: self.listingDetail?.description ?? "",shippingWeightPounds:pounds,shippingWeightOunces:ounces,shippingLength:length,shippingHeight:height,shippingDepth:width,shippingType:self.shippingType)
            }
            
        }else{
            if self.shippingRateId == 0{
                listingData = ListingDetail(shippingWeightPounds:pounds,shippingWeightOunces:ounces,shippingLength:length,shippingHeight:height,shippingDepth:width,shippingType:self.shippingType)
            }else{
                listingData = ListingDetail(shippingRateId: self.shippingRateId,shippingWeightPounds:pounds,shippingWeightOunces:ounces,shippingLength:length,shippingHeight:height,shippingDepth:width,shippingType:self.shippingType)
            }
        }
        self.updateData(id: (listingDetail?.id!)!,listingDetail: listingData!)
    }
    
     func updateData(id:Int,listingDetail:ListingDetail) {
           self.doneView.isHidden = false
           profileViewModel.updateEditListing(accessToken: AppDefaults.getAccessToken(), id: id, data: listingDetail, { (response) in
               self.doneView.isHidden = true
               if response.response?.statusCode == 200 {
                   let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                   if data.isError {
                       let error = Utils.getErrorMessage(errors: data.errors)
                       self.showMessageDialog(message: error)
                   } else {
                    if let viewControllers = self.navigationController?.viewControllers
                    {
                        for controller in viewControllers
                        {
                            if controller is UserBynViewController
                            {
                                self.popToUserByn()
                            }
                            
                            if controller is ListDetailViewController
                            {
                                self.listingDetailRefreshDelegate?.listingDetailRefresh()
                                //self.navigationController?.popViewController(animated: false)
                                //self.userBynViewController()
                            }
                            
                        }
                    }
                   }
               }else{
                   let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                   if data.isError {
                       let error = Utils.getErrorMessage(errors: data.errors)
                       self.showMessageDialog(message: error)
                   }
                self.doneView.isHidden = true
               }
           } , { (error) in
               self.doneView.isHidden = true
               if error.statusCode == AppConstants.NO_INTERNET {
                   self.showInternetError()
            }
           })
    }
    func popToUserByn(){
        let myBynViewController = self.navigationController!.viewControllers[1] as! UserBynViewController
        myBynViewController.listNeedsRefresh = true
        self.navigationController!.popToViewController(myBynViewController, animated: false)
    }
    
    func uploadListing() {
        self.doneView.isHidden = false
        var tags:[String] = []
        var colorsData:[Int] = []
        
        for i in 1..<tagsArray.count{
            tags.append(tagsArray[i].tag!)
            
            let itemsArray = tags
            let searchToSearch = tagsArray[i].tag!

            let filteredStrings = itemsArray.filter({(item: String) -> Bool in

                let stringMatch = item.lowercased().range(of: searchToSearch.lowercased())
                return stringMatch != nil ? true : false
            })
            print("filteredStrings:",filteredStrings)


            if (filteredStrings as NSArray).count > 0
            {
                //Record found
                //MARK:- You can also print the result and can do any kind of work with them
            }
            else
            {
                //Record Not found
            }
            
        }
        
        for i in 0..<colors!.count{
            colorsData.append(colors![i].colorId!)
        }
        
        var listingData :ListingDetail?
        
        let pounds = Int(poundsTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let ounces = Int(ouncesTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        
        let length = Double(lengthTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let width = Double(widthTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        let height = Double(heightTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "") ?? 0
        
        if lowestAllowableOffer == nil || lowestAllowableOffer == 0{
             
            if self.shippingRateId == 0{
                listingData = ListingDetail(title: titleText ?? "", description: shortDescText ?? "", tags: tags, conditionId: conditionId ?? 0, originalPrice: originalPrice ?? 0, listingPrice: listingPrice ?? 0, categoryId: subCategoryId ?? 0, categoryTypeId: categoryId ?? 0, sizeId: sizeId ?? 0, colorIds: colorsData, brandId: brandId!, sizeTypeId:5, availabilityId:1, shippingWeightPounds:pounds,shippingWeightOunces:ounces,shippingLength:length,shippingHeight:width,shippingDepth:height,shippingType:self.shippingType)
            }else{
                listingData = ListingDetail(title: titleText ?? "", description: shortDescText ?? "", tags: tags, conditionId: conditionId ?? 0, originalPrice: originalPrice ?? 0, listingPrice: listingPrice ?? 0, categoryId: subCategoryId ?? 0, categoryTypeId: categoryId ?? 0, sizeId: sizeId ?? 0, colorIds: colorsData, brandId: brandId!,shippingRateId:self.shippingRateId , sizeTypeId:5, availabilityId:1, shippingWeightPounds:pounds,shippingWeightOunces:ounces,shippingLength:length,shippingHeight:width,shippingDepth:height,shippingType:self.shippingType)
            }
            
        }else{
            if self.shippingRateId == 0{
                listingData = ListingDetail(title: titleText ?? "", description: shortDescText ?? "", tags: tags, conditionId: conditionId ?? 0, originalPrice: originalPrice ?? 0, listingPrice: listingPrice ?? 0, lowestAllowableOffer: lowestAllowableOffer ?? 0, categoryId: subCategoryId ?? 0, categoryTypeId: categoryId ?? 0, sizeId: sizeId ?? 0, colorIds: colorsData, brandId: brandId ?? 0, sizeTypeId:5, availabilityId:1, shippingWeightPounds:pounds,shippingWeightOunces:ounces,shippingLength:length,shippingHeight:width,shippingDepth:height,shippingType:self.shippingType)
            }else{
                listingData = ListingDetail(title: titleText ?? "", description: shortDescText ?? "", tags: tags, conditionId: conditionId ?? 0, originalPrice: originalPrice ?? 0, listingPrice: listingPrice ?? 0, lowestAllowableOffer: lowestAllowableOffer ?? 0, categoryId: subCategoryId ?? 0, categoryTypeId: categoryId ?? 0, sizeId: sizeId ?? 0, colorIds: colorsData, brandId: brandId ?? 0,shippingRateId:self.shippingRateId , sizeTypeId:5, availabilityId:1, shippingWeightPounds:pounds,shippingWeightOunces:ounces,shippingLength:length,shippingHeight:width,shippingDepth:height,shippingType:self.shippingType)
            }
            
        }
        
        createViewModel.createListing(accessToken: getAccessToken(), data: listingData!,{(response) in
            if response.response?.statusCode == 201 {
                let data: ApiResponse<Int> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let listingId = data.data
                    var images : [UIImage] = []
                    for i in 0..<(self.selectedImages?.count)!-1 {
                        images.append(self.selectedImages![i].userImage!)
                    }
                    if images.count > 0{
                        self.uploadImages(listingId:listingId ?? 0,images: images)
                    }else{
                        self.uploadVideo(listingId:listingId ?? 0,videoURL:self.compressedVideoURL!)
                    }
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.doneView.isHidden = true
            }
        } , { (error) in
            self.doneView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func uploadImages(listingId:Int,images:[UIImage]){
        self.createViewModel.uploadListingImage(accessToken: self.getAccessToken(), id: listingId, images: images, {(response) in
            self.doneView.isHidden = true
            if response.response?.statusCode == 201 {
                let data: ApiResponse<[SliderImages]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    if self.compressedVideoURL == nil{
                        self.listingCompletion(listingId:listingId)
                    }else{
                        self.uploadVideo(listingId:listingId,videoURL:self.compressedVideoURL!)
                    }
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.doneView.isHidden = true
            }
        } , { (error) in
            self.doneView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func uploadVideo(listingId:Int,videoURL:URL){
        self.createViewModel.uploadListingVideo(accessToken: self.getAccessToken(), id: listingId,videoURL: videoURL ,{(response) in
            if response.response?.statusCode == 201 || response.response?.statusCode == 200{
                print("video uploaded successfully!")
                do{
                    try self.deleteCachedVideos()
                }catch let error{
                    print(error)
                }
                self.listingCompletion(listingId:listingId)
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.doneView.isHidden = true
            }
        } , { (error) in
            self.doneView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func listingCompletion(listingId:Int){
        self.trackListing(listingId: listingId)
        AppConstants.LISTING_LOST = false
        self.onCreateClick(type: AppConstants.CLEAR_STACK_OF_CREATE_LISTING_TYPE)
    }
    
    func trackListing(listingId:Int){
        self.createViewModel.trackListings(accessToken: self.getAccessToken(), listingId: listingId, {(response) in
            self.doneView.isHidden = true
            if response.response?.statusCode == 201 || response.response?.statusCode == 200{
                print("tracked Listing")
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.doneView.isHidden = true
            }
        } , { (error) in
            self.doneView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
}
extension CalculateShippingViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: AppConstants.SHIPPING_TITLE)
    }
}
