import UIKit

class CategoryViewController: BaseViewController {
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    
    let categoryHandler = CategoryHandler()
    let flowLayoutHelper = FlowLayoutHelper()
    let createViewModel = CreateViewModel()
    var categorySelectionDelegate : CategorySelectionProtocol?
    var categorySelected:CategoryType? = nil
    var subCategorySelected:Category?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.clipsToBounds = false
        
        navigationItem.title = AppConstants.Strings.SELECT_CATEGORY
        let sellerPolicyBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.SELLER_POLICY_ICON), style: .plain, target: self, action: #selector(onSellerPolicyBarButtonItem))
        self.navigationItem.rightBarButtonItem  = sellerPolicyBarButton
        collectionView.delegate = categoryHandler
        collectionView.dataSource = categoryHandler
        //collectionView.collectionViewLayout = flowLayoutHelper
        collectionView.contentInset = UIEdgeInsets(top: 8, left: 4, bottom: 10, right: 6)
        self.getCategory()
        self.categoryClick()
        let backBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.BACK_BUTTON), style: .plain, target: self, action: #selector(onBackBarButtonItem))
        self.navigationItem.leftBarButtonItem  = backBarButton
    }
    
    @objc func onBackBarButtonItem(){
        self.navigationController?.popViewController(animated: false)
    }
    
    func categoryClick(){
        categoryHandler.onCategoryClick = { categoryType in
            let storyboard = UIStoryboard(name: AppConstants.Storyboard.SUB_CATEGORY_BOARD, bundle: nil)
            let subCategoryViewControlller = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.SUB_CATEGORY_SCREEN) as! SubCategoryViewController
            subCategoryViewControlller.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
            subCategoryViewControlller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
            subCategoryViewControlller.categoryType = categoryType
            subCategoryViewControlller.subCategorySelected = self.subCategorySelected
            subCategoryViewControlller.categorySelectionDelegate = self.categorySelectionDelegate
            self.present(subCategoryViewControlller, animated: true, completion: nil)
        }
    }
    
    @objc func onSellerPolicyBarButtonItem(){
        self.sellerPolicyScreen()
    }
    
    func getCategory() {
        createViewModel.getCategory(accessToken: AppDefaults.getAccessToken(), { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[CategoryType]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    var categoryData = data.data
                    for i in 0..<categoryData!.count {
                        if self.categorySelected != nil && categoryData![i].categoryTypeId == self.categorySelected?.categoryTypeId {
                            categoryData![i].isSelected = true
                        } else {
                            categoryData![i].isSelected = false
                        }
                    }
                    self.categoryHandler.categoryType = categoryData!
                    self.collectionView.reloadData()
                    self.screenActivityIndicator.stopAnimating()
                    self.collectionView.isHidden = false
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                
            }
        })
    }
    
    
    
}
