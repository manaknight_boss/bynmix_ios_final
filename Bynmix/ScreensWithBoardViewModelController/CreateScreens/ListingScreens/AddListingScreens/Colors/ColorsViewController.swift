import UIKit

class ColorsViewController: BaseViewController {
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    
    let colorsHandler = ColorsHandler()
    let flowLayoutHelper = FlowLayoutHelper()
    let createViewModel = CreateViewModel()
    var colorsSelectDelegate: ColorSelectProtocol?
    var selectedColors: [Colors] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.clipsToBounds = false
        
        navigationItem.title = AppConstants.Strings.SELECT_COLORS_TITLE
        let selectColorBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.SELECT_ICON), style: .plain, target: self, action: #selector(onSelectColorBarButtonItem))
        self.navigationItem.rightBarButtonItem  = selectColorBarButton
        collectionView.delegate = colorsHandler
        collectionView.dataSource = colorsHandler
        //collectionView.collectionViewLayout = flowLayoutHelper
        collectionView.contentInset = UIEdgeInsets(top: 8, left: 4, bottom: 10, right: 6)
        getColors()
        colorsHandler.onMoreThanTwoColorsClick = {
            self.view.makeToast(AppConstants.Strings.ADD_UP_ONLY_TWO_COLORS_MESSAGE)
        }
        let backBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.BACK_BUTTON), style: .plain, target: self, action: #selector(onBackBarButtonItem))
        self.navigationItem.leftBarButtonItem  = backBarButton
    }
    
    @objc func onBackBarButtonItem(){
           self.navigationController?.popViewController(animated: false)
       }
    
    @objc func onSelectColorBarButtonItem(){
        if colorsHandler.getSelectedColors().count > 0 {
            colorsSelectDelegate?.colorSelect(selectedColors: colorsHandler.getSelectedColors())
            self.navigationController?.popViewController(animated: false)
        } else {
            let text:String = AppConstants.Strings.SELECT_ONE_COLOR_MESSAGE
            let textColor:String = ""
            showCreatePostMessageDialog(text: text, textColor: textColor)
        }
    }
    
    func getColors() {
        createViewModel.getColors(accessToken: AppDefaults.getAccessToken(), { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[Colors]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    var colorsData = data.data
                    
                    for i in 0..<colorsData!.count {
                        var selected = false
                        if self.selectedColors.count > 0 {
                            if self.selectedColors[0].colorId == colorsData![i].colorId {
                                colorsData![i].isSelected = true
                                selected = true
                            }
                            if self.selectedColors.count == 2 {
                                if self.selectedColors[1].colorId == colorsData![i].colorId {
                                    colorsData![i].isSelected = true
                                    selected = true
                                }
                            }
                        }
                        colorsData![i].isSelected = selected
                        
                    }
                    
                    self.colorsHandler.colorsList = colorsData!
                    self.collectionView.reloadData()
                    self.screenActivityIndicator.stopAnimating()
                    self.collectionView.isHidden = false
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
}
