import UIKit
import AVFoundation
import AVKit
import Photos
import MobileCoreServices
import Regift
import AssetsLibrary
import FittedSheets

class CoverVideoViewController: BaseViewController,CameraProtocol,LoadAssetProtocol, SetVideoDurationProtocol, SetVideoUrlInSheetProtocol {
    
    func setVideoUrlInSheetMethod(url: URL) {
        self.setVideoURL(url: url)
        self.resizeBottomSheet()
    }
    
    func setVideoDurationMethod(totalTime: Int) {
        self.videoDuration = Float(totalTime)
        self.resizeBottomSheet()
    }
    
    func loadAssetMethod(asset: AVAsset) {
        self.loadAsset(asset)
        self.resizeBottomSheet()
    }
    
    func videoURL(url: URL) {
        self.setVideoURL(url: url)
        self.resizeBottomSheet()
    }
    
    func videoAsset(asset:AVAsset,duration:String){
        let durationInSec = duration.components(separatedBy: ":")
        if durationInSec.count > 1{
            let hour = Int(durationInSec[0]) ?? 0 * 60
            let seconds = Int(durationInSec[1]) ?? 0
            let total = hour + seconds
            self.videoDuration = Float(total)
        }
        self.loadAsset(asset)
        self.resizeBottomSheet()
    }
    
    func videoFormat() {
        self.view.makeToast("Video format or duration not correct.")
    }
    
    func CameraProtocol(url: URL) {
        self.dismiss(animated: false, completion: nil)
        let videoUrl = url.absoluteString
        let videoAsset: AVAsset = AVAsset(url: URL(string: videoUrl)!) as AVAsset
        self.setVideoURL(url: url)
        self.loadAsset(videoAsset)
        self.resizeBottomSheet()
    }
    
    @IBOutlet weak var durationView: TrimmerView!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var videoPlayButton: UIButton!
    @IBOutlet var createBoomerangButton: UIButton!
    @IBOutlet var mainScreenView: UIView!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    
    var photos: PHFetchResult<PHAsset>!
    var photoLibrary:[UIImage] = []
    var timeDuration:[String] = []
    var minutes:Int? = 0
    var seconds:Int? = 0
    
    var imageDictionary = [String: AnyObject]()
    var videoDictionary = [String: AnyObject]()
    let coverVideoCell = "galleryCell"
    let cameraCell = "cameraCell"
    var player: AVPlayer?
    var playbackTimeCheckerTimer: Timer?
    var trimmerPositionChangedTimer: Timer?
    var assetTrimmedLink:URL?
    var videoStartTime:Float? = 0
    var videoDuration:Float? = 0
    var videoEndTime:Float? = 0
    //var videoHandler = VideoHandler()
    var convertedVideoDelegate:ConvertedVideoProtocol?
    var editVideo:AVAsset? = nil
    
    var frames:[UIImage] = []
    private var generator:AVAssetImageGenerator!
    var bottomSheetViewController : SheetViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.Strings.CREATE_COVER_VIDEO

        let selectBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.SELECT_ICON), style: .plain, target: self, action: #selector(onSelectBarButtonItem))
        self.navigationItem.rightBarButtonItem  = selectBarButton
        setVideoPlaySettings()
        if self.editVideo != nil{
            self.loadAsset(editVideo!)
        }
        
        let backBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.BACK_BUTTON), style: .plain, target: self, action: #selector(goBack))
        self.navigationItem.leftBarButtonItem  = backBarButton
        self.navigationItem.leftBarButtonItem?.imageInsets = UIEdgeInsets(top: 0, left: -8, bottom: 0, right: 0)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        checkIfPermissionGranted()
    }
    
    func checkIfPermissionGranted(){
        PHPhotoLibrary.execute(controller: self, onAccessHasBeenGranted: {
            self.grabPhotos()
        })
    }
    
    func setVideoPlaySettings(){
        durationView.handleColor = UIColor.cBB189C
        durationView.mainColor = UIColor.cBB189C
        durationView.maxDuration = 5
        durationView.minDuration = 2
        
        videoPlayButton.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.pausePlayer()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
    }
    
    func loadAsset(_ asset: AVAsset) {
        videoPlayButton.isHidden = false
        durationView.asset = asset
        durationView.delegate = self
        addVideoPlayer(with: asset, playerView: videoView)
    }
    
    func getAllFrames(videoUrl:URL) {
        let asset:AVAsset = AVAsset(url:videoUrl)
        let duration:Float64 = CMTimeGetSeconds(asset.duration)
        self.generator = AVAssetImageGenerator(asset:asset)
        self.generator.appliesPreferredTrackTransform = true
        self.frames = []
        for index:Int in 0 ..< Int(duration) {
            self.getFrame(fromTime:Float64(index))
        }
        for item in self.frames.reversed() {
            self.frames.append(item)
        }
        
        if self.frames.count <= 48{
            self.frames.append(contentsOf: self.frames + self.frames)
        }
        print("frames:",self.frames.count,"duration:",duration)
        self.generator = nil
        self.makeVideoFromArray()
    }
    
    private func getFrame(fromTime:Float64) {
        let time:CMTime = CMTimeMakeWithSeconds(fromTime, preferredTimescale:600)
        print("Time:",time,"From Time:",fromTime)
        let image:CGImage
        do {
            try image = self.generator.copyCGImage(at:time, actualTime:nil)
        } catch {
            return
        }
        self.frames.append(UIImage(cgImage:image))
    }
    
    
    func createImage(_ withFPS: Int, videoUrl: URL) {
        let asset = AVURLAsset(url: videoUrl, options: nil)
        
        let generator = AVAssetImageGenerator(asset: asset)
        generator.requestedTimeToleranceAfter = .zero
        generator.requestedTimeToleranceBefore = .zero

        for i in 0..<(Int(CMTimeGetSeconds(asset.duration) * Double(withFPS))) {
            autoreleasepool {
                let time = CMTimeMake(value: Int64(i), timescale: Int32(withFPS))
                var _: Error?
                var actualTime: CMTime = asset.duration
                var image: CGImage? = nil
                do {
                    image = try generator.copyCGImage(at: time, actualTime: &actualTime)
                } catch _ {
                }
                var generatedImage: UIImage? = nil
                if let image = image {
                    generatedImage = UIImage(cgImage: image)
                }
                 
                self.frames.append(generatedImage!)
//                CGImageRelease(image!)
            }
        }
        for item in self.frames.reversed() {
            self.frames.append(item)
        }
        
        if self.frames.count <= 48{
            self.frames.append(contentsOf: self.frames + self.frames)
        }
        print("frames:",self.frames.count)
        self.makeVideoFromArray()
    }
    
  
    
    @IBAction func videoPlayButtonAction(_ sender: UIButton) {
        playPauseOnTap()
    }
    
    @IBAction func videoPayPauseTapAction(_ sender: UITapGestureRecognizer) {
        playPauseOnTap()
    }
    
    func playPauseOnTap(){
        guard let player = player else { return }
        
        if !player.isPlaying {
            player.play()
            startPlaybackTimeChecker()
            videoPlayButton.isHidden = true
        } else {
            player.pause()
            stopPlaybackTimeChecker()
            videoPlayButton.isHidden = false
        }
    }
    
    private func addVideoPlayer(with asset: AVAsset, playerView: UIView) {
        let playerItem = AVPlayerItem(asset: asset)
        player = AVPlayer(playerItem: playerItem)
        player?.isMuted = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(itemDidFinishPlaying(_:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
        
        let layer: AVPlayerLayer = AVPlayerLayer(player: player)
        layer.backgroundColor = UIColor.white.cgColor
        layer.frame = CGRect(x: 0, y: 0, width: playerView.frame.width, height: playerView.frame.height)
        //layer.frame = self.videoView.frame
        layer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerView.layer.sublayers?.forEach({$0.removeFromSuperlayer()})
        playerView.layer.addSublayer(layer)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func itemDidFinishPlaying(_ notification: Notification) {
        if let startTime = durationView.startTime {
            player?.seek(to: startTime)
        }
        self.player?.pause()
        self.videoPlayButton.isHidden = false
    }
    
    func startPlaybackTimeChecker() {
        
        stopPlaybackTimeChecker()
        playbackTimeCheckerTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self,
                                                        selector:
                                                            #selector(onPlaybackTimeChecker), userInfo: nil, repeats: true)
    }
    
    func stopPlaybackTimeChecker() {
        
        playbackTimeCheckerTimer?.invalidate()
        playbackTimeCheckerTimer = nil
    }
    
    @objc func onPlaybackTimeChecker() {
        
        guard let startTime = durationView.startTime, let endTime = durationView.endTime, let player = player else {
            return
        }
        
        let playBackTime = player.currentTime()
        durationView.seek(to: playBackTime)
        
        if playBackTime >= endTime {
            player.seek(to: startTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
            durationView.seek(to: startTime)
            self.player?.pause()
            self.videoPlayButton.isHidden = false
        }
    }
    
    @IBAction func createBoomerangButtonAction(_ sender: UIButton) {
        onCreateVideoButtonClick(type: AppConstants.TYPE_BOOMERANG)
    }
    
    @objc func onSelectBarButtonItem(){
        onCreateVideoButtonClick(type: AppConstants.TYPE_VIDEO_UPLOAD)
    }
    
    func onCreateVideoButtonClick(type:Int){
        self.pausePlayer()
        if self.assetTrimmedLink != nil{
            self.disableButton()
            if self.videoEndTime == 0 || self.videoEndTime == nil{
                if self.videoDuration ?? 0 < 5{
                    self.videoEndTime = self.videoDuration ?? 0
                }else{
                    self.videoEndTime = 3
                }
            }
            self.cropVideo(self.assetTrimmedLink!, statTime: self.videoStartTime ?? 0, endTime: self.videoEndTime ?? 0,type: type)
        }else{
            self.view.makeToast("Please Select a Video First.")
        }
    }
    
    func cropVideo(_ sourceURL1: URL, statTime:Float, endTime:Float,type:Int){
        let videoAsset: AVAsset = AVAsset(url: sourceURL1) as AVAsset
        let composition = AVMutableComposition()
        composition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: CMPersistentTrackID())
        
        let videoComposition = AVMutableVideoComposition()
        videoComposition.renderSize = CGSize(width: 1280, height: 768)
        videoComposition.frameDuration = CMTimeMake(value: 8, timescale: 15)
        
        let instruction = AVMutableVideoCompositionInstruction()
        let length = Float(videoAsset.duration.value)
        print(length)
        
        instruction.timeRange = CMTimeRangeMake(start: CMTime.zero, duration: CMTimeMakeWithSeconds(60, preferredTimescale: 30))
        
        let start = statTime
        let end = endTime
        
        let exportSession = AVAssetExportSession(asset: videoAsset, presetName: AVAssetExportPresetMediumQuality)!
        exportSession.outputFileType = AVFileType.mp4
        
        let startTime = CMTime(seconds: Double(start ), preferredTimescale: 1000)
        let endTime = CMTime(seconds: Double(end ), preferredTimescale: 1000)
        let timeRange = CMTimeRange(start: startTime, end: endTime)
        
        exportSession.timeRange = timeRange
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"
        
        let date = Date()
        
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString
        let outputPath = "\(documentsPath)/\(formatter.string(from: date)).mp4"
        let outputURL = URL(fileURLWithPath: outputPath)
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
        exportSession.exportAsynchronously(completionHandler: { () -> Void in
            DispatchQueue.main.async(execute: {
                self.saveToPhotos(file:outputURL.absoluteString,url: outputURL.absoluteURL,type: type)
            })
        })
    }

    func saveToPhotos(file:String,url:URL,type:Int){
        PHPhotoLibrary.shared().performChanges({
            let request = PHAssetCreationRequest.forAsset()
            request.addResource(with: .video, fileURL: URL(string: file)!, options: nil)
        }) { (success, error) in
            if let error = error {
                print(error.localizedDescription)
                self.enableButton()
            } else {
                print("Success")
                
                let videoAsset: AVAsset = AVAsset(url: URL(string: file)!) as AVAsset
                self.videoUrls = [URL(string: file)!]
                
                    if type == AppConstants.TYPE_VIDEO_UPLOAD{
                        self.convertedVideoDelegate?.convertedVideo(url: "", avAsset: videoAsset, asset: PHAsset(), avUrlAsset: URL(string: file)!)
                    }else{
                        //self.getAllFrames(videoUrl:URL(string: file)!)
                        self.createImage(8, videoUrl:URL(string: file)!)
                    }
            }
        }
        self.enableButton()
        NotificationCenter.default.removeObserver(self)
    }
    
    func makeVideoFromArray(){
        var uiImages = [UIImage]()
        uiImages = self.frames

        let settings = CXEImagesToVideo.videoSettings(codec: AVVideoCodecType.h264.rawValue, width: (uiImages[0].cgImage?.width)!, height: (uiImages[0].cgImage?.height)!)
        let movieMaker = CXEImagesToVideo(videoSettings: settings)
        movieMaker.createMovieFrom(images: uiImages){ (fileURL:URL) in
            let video = AVAsset(url: fileURL)
            self.convertedVideoDelegate?.convertedVideo(url: "", avAsset: video, asset: PHAsset(), avUrlAsset: fileURL)
        }
    }
    
    func disableButton(){
        DispatchQueue.main.async {
            self.view.makeToastActivity(.center)
            self.navigationItem.rightBarButtonItem?.isEnabled = false
            self.createBoomerangButton.isEnabled = false
        }
    }
    
    func enableButton(){
        DispatchQueue.main.async {
            self.view.hideToastActivity()
            self.navigationItem.rightBarButtonItem?.isEnabled = true
            self.createBoomerangButton.isEnabled = true
        }
    }
    
    func grabPhotos(){
        let imgManager = PHImageManager.default()
        
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        requestOptions.deliveryMode = .highQualityFormat
        
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        let fetchResult : PHFetchResult = PHAsset.fetchAssets(with: .video, options: fetchOptions) 
            if fetchResult.count > 0 {
                self.imageDictionary.updateValue(UIImage(named: AppConstants.Images.RECORD_VIDEOS_ICON) as AnyObject, forKey: "0")
                self.videoDictionary.updateValue("" as AnyObject, forKey: "VideoDuration[0]")
                for i in 0..<fetchResult.count{
                    //Used for fetch Image//
                    imgManager.requestImage(for: fetchResult.object(at: i) as PHAsset , targetSize: CGSize(width: 93, height: 93), contentMode: .aspectFill, options: requestOptions, resultHandler: {
                        image, error in
                        
                        var imageOfVideo: UIImage?
                        
                        if image != nil{
                            imageOfVideo = (image)! as UIImage
                            let key = "\(i + 1)"
                            self.imageDictionary[key] = imageOfVideo
                        }
                    })
                    //Used for fetch Video//
                    imgManager.requestAVAsset(forVideo: fetchResult.object(at: i) as PHAsset, options: PHVideoRequestOptions(), resultHandler: {(avAsset, audioMix, info) -> Void in
                        if let asset = avAsset {
                            let videoURL = avAsset as? AVURLAsset
                            let duration : CMTime = asset.duration
                            //let durationInSecond = CMTimeGetSeconds(duration)
                            let totalDurationOfTime = duration.seconds
                            let durationInMinutes = Int(totalDurationOfTime) / 60
                            let durationInSeconds = Int(totalDurationOfTime - Double(durationInMinutes * 60))
                            print("time",durationInMinutes,durationInSeconds)
                            print(asset)
                            
                            var totalTime:AnyObject?
                            if durationInSeconds < 10{
                                totalTime = "\(durationInMinutes):0\(durationInSeconds)" as AnyObject
                            }else{
                                totalTime = "\(durationInMinutes):\(durationInSeconds)" as AnyObject
                            }
                            
                            if durationInSeconds != 0 && videoURL != nil{
                                let key = "\(i + 1)"
                                self.videoDictionary[key] = ["VideoPath" : asset,"VideoURL": videoURL?.url as Any,"VideoDuration" : totalTime!] as AnyObject
                            }else{
                                return
                            }
                        }else{
                            print("empty")
                        }
                    })
                }
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    print(self.imageDictionary,self.videoDictionary)
//                    self.videoHandler.imageDictionary = self.imageDictionary
//                    self.videoHandler.videoDictionary = self.videoDictionary
//                    self.collectionView.reloadData()
                    if self.videoDictionary.count > 1{
                        let key = "1"
                        
                        let singleVideo = self.videoDictionary[key] as? [String: AnyObject]
                        //let singleVideo = self.videoDictionary.values. as? [String: AnyObject]
                        let asset = singleVideo?["VideoPath"] as! AVAsset
                        let videoUrl = singleVideo?["VideoURL"] as! URL
                        
                        self.setVideoURL(url: videoUrl)
                        self.loadAsset(asset)
                    }
                    self.screenActivityIndicator.stopAnimating()
                    self.mainScreenView.isHidden = false
                    self.setBottomSheet()
                    
                }
            }
            else{
                self.showMessageDialog(message: "Looks Like there's no video in your Photos.")
            }
        
    }
    
    func setBottomSheet(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.BOTTOM_SHEET_STORYBOARD, bundle: nil)
        let bottomSheetViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.BOTTOM_SHEET_SCREEN) as! BottomSheetViewController
        bottomSheetViewController.imageDictionary = self.imageDictionary
        bottomSheetViewController.videoDictionary = self.videoDictionary
        bottomSheetViewController.loadAssetDelegate = self
        bottomSheetViewController.setVideoDurationDelegate = self
        bottomSheetViewController.setVideoUrlInSheetDelegate = self

        let options = SheetOptions(
            pullBarHeight:12, useFullScreenMode: false,useInlineMode: true
        )

        let sheetController = SheetViewController(controller: bottomSheetViewController, sizes: [.fixed(115), .marginFromTop(20)], options: options)
        sheetController.allowGestureThroughOverlay = true

        // Add child
        sheetController.willMove(toParent: self)
        self.addChild(sheetController)
        view.addSubview(sheetController.view)
        sheetController.didMove(toParent: self)

        sheetController.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            sheetController.view.topAnchor.constraint(equalTo: view.topAnchor),
            sheetController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            sheetController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            sheetController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])

        // animate in
        sheetController.animateIn()
        sheetController.dismissOnPull = false
        sheetController.gripColor = UIColor.c979797
        sheetController.gripSize = CGSize(width: 62, height: 3)
        sheetController.pullBarBackgroundColor = UIColor.white
        sheetController.minimumSpaceAbovePullBar = 0
        sheetController.overlayColor = UIColor.clear
        sheetController.cornerRadius = 0
        self.bottomSheetViewController = sheetController
    }
    
    func resizeBottomSheet(){
        self.bottomSheetViewController?.resize(to: .fixed(115))
    }
    
    func setVideoURL(url: URL) {
        assetTrimmedLink = url
    }
    
    func cameraScreen(){
        self.setAppAudioSettings()
        let cameraViewController = CameraViewController()
        cameraViewController.modalPresentationStyle = .overFullScreen
        cameraViewController.camerasDelegate = self
        self.navigationController?.present(cameraViewController, animated: false, completion: nil)
    }
    
    public func setAppAudioSettings() {
        let audioSession = AVAudioSession.sharedInstance()
        do {
            let options: AVAudioSession.CategoryOptions = [
                .mixWithOthers//, .allowBluetooth, .allowBluetoothA2DP, .allowAirPlay
            ]

            try audioSession.setCategory(
                AVAudioSession.Category.playback,
                mode: AVAudioSession.Mode.default, options: options)
            try audioSession.setActive(true)
        } catch {
            print(error)
            print("Failed to set background audio preference \(error.localizedDescription)")
        }
    }
    
    func pausePlayer(){
        if player != nil{
            player?.pause()
        }
    }
    
}
extension CoverVideoViewController: TrimmerViewDelegate {
    func positionBarStoppedMoving(_ playerTime: CMTime) {
        player?.seek(to: playerTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
        if playerTime.seconds == 0{
            self.videoPlayButton.isHidden = false
            player?.pause()
        }else{
            player?.play()
            self.videoPlayButton.isHidden = true
        }
        startPlaybackTimeChecker()
    }
    
    func didChangePositionBar(_ playerTime: CMTime) {
        stopPlaybackTimeChecker()
        player?.pause()
        videoPlayButton.isHidden = false
        player?.seek(to: playerTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
        let duration = (durationView.endTime! - durationView.startTime!).seconds
        print(duration)
        print(durationView.startTime!)
        self.videoStartTime = Float(durationView.startTime!.seconds)
        self.videoDuration = Float(duration)
        self.videoEndTime = Float(durationView.endTime!.seconds)
    }
}








