import UIKit

class CreateListingNavigationViewController: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let createStoryBoard = UIStoryboard(name: AppConstants.Storyboard.CREATE_BOARD, bundle: nil)
        let createViewController = createStoryBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.CREATE_SCREEN) as! CreateViewController
        createViewController.modalPresentationStyle = .overCurrentContext
        self.present(createViewController, animated: false,completion: nil)
        
        let createListingStoryBoard = UIStoryboard(name: AppConstants.Storyboard.CREATE_LISTING_BOARD, bundle: nil)
        let createListingViewController = createListingStoryBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.CREATE_LISTING_SCREEN) as! CreateListingViewController
        createListingViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        createListingViewController.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(createListingViewController, animated: false,completion: nil)
        
    }
    
}
