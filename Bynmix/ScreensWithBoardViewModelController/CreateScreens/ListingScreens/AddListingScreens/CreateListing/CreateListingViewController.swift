import UIKit
import XLPagerTabStrip
import AVKit
import Photos
import Mantis

class CreateListingViewController: BaseViewController,AddImageProtocol,ConvertedVideoProtocol {
    func convertedVideo(url: String, avAsset: AVAsset, asset: PHAsset, avUrlAsset: URL) {
        DispatchQueue.main.async {
            if self.TYPE == AppConstants.TYPE_EDIT_LISTING{
                let screen = UIScreen.main.bounds
                var height : CGFloat = 0
                height = screen.height * 0.07
                self.topConstraint.constant = height
            }else{
                self.topConstraint.constant = 10
            }
            self.videoImageView.isHidden = true
            self.videoMainView.isHidden = false
            self.navigationController?.popViewController(animated: false)
            //self.nextButtonView.isHidden = false
        }
        DispatchQueue.main.async {
            self.initSelectedVideo(url: url,avAsset:avAsset,asset: asset, avUrlAsset: avUrlAsset)
        }
    }
    
    func addImage(type: Int) {
        if type == AppConstants.TYPE_IMAGE_SELECTED{
            self.nextButtonView.isHidden = true
        }else{
            self.nextButtonView.isHidden = false
        }
    }
    
    @IBOutlet var addImagecollectionView: UICollectionView!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var editDeleteButtonsView: UIView!
    @IBOutlet weak var deleteVideoButton:UIButton!
    @IBOutlet weak var editVideoButton:UIButton!
    @IBOutlet weak var videoImageView: UIImageView!
    @IBOutlet weak var nextButtonView: UIView!
    @IBOutlet var videoImageViewHeight: NSLayoutConstraint!
    @IBOutlet var videoImageViewWidth: NSLayoutConstraint!
    
    @IBOutlet var videoView: UIView!
    @IBOutlet var videoMainView: UIView!
    @IBOutlet var videoViewHeight: NSLayoutConstraint!
    @IBOutlet var videoViewWidth: NSLayoutConstraint!
    @IBOutlet var videoPlayButton: UIButton!
    @IBOutlet var durationView: TrimmerView!
    
    var addListingImagesHandler = AddListingImagesHandler()
    let imagePickerManager = ImagePickerManager()
    var sellerPolicyBarButtonItem:UIBarButtonItem?
    var images: [AddListingImage]? = []
    var TYPE:Int?
    var profileViewModel = ProfileViewModel()
    var createViewModel = CreateViewModel()
    var listingDetail:ListingDetail?
    var listingImagesData: [GetListingImages] = []
    var isAlreadyPoped = false
    fileprivate var longPressGesture: UILongPressGestureRecognizer!
    var imageListingCell = "CreateListingImageCell"
    
    var player: AVPlayer?
    var compressedAsset:AVAsset? = nil
    var compressedVideoURL:URL?
    var listingVideoDeleted:Bool = false
    
    var playerLayer:AVPlayerLayer?
    
    @IBOutlet weak var dialogView: UIView!
    
    var doesListingHasVideoUrl:Bool = false
    var degree:CGFloat = 0
    var listingDetailRefreshDelegate:ListingDetailRefreshProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if listingDetail?.videoUrl != nil && listingDetail?.videoUrl != ""{
            doesListingHasVideoUrl = true
        }else{
            doesListingHasVideoUrl = false
        }
        self.videoMainView.isHidden = true
        self.topConstraint.constant = 35
        videoImageViewHeight.constant = 169
        videoImageViewWidth.constant = 161
        videoImageView.contentMode = .scaleAspectFit
        videoView.backgroundColor = UIColor.cE8E8E8
        durationView.backgroundColor = UIColor.cE8E8E8
        addImagecollectionView.delegate = addListingImagesHandler
        addImagecollectionView.dataSource = addListingImagesHandler
        getItemsClick()
        navigationItem.title = AppConstants.CREATE_LISTING_TITLE
        let sellerPolicyBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.SELLER_POLICY_ICON), style: .plain, target: self, action: #selector(onSellerPolicyBarButtonItem))
        self.navigationItem.rightBarButtonItem  = sellerPolicyBarButton
        if TYPE == AppConstants.TYPE_EDIT_LISTING {
            self.nextButtonView.isHidden = false
        } else {
            self.nextButtonView.isHidden = true
        }
        addListingImagesHandler.addImageDelegate = self
        onTypeSelect()
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongGesture(gesture:)))
        addImagecollectionView.addGestureRecognizer(longPressGesture)
        setVideoPlaySettings()
    }
    
    func setVideoPlaySettings(){
        durationView.handleColor = UIColor.cE8E8E8
        durationView.mainColor = UIColor.cE8E8E8
        durationView.maxDuration = 15
        durationView.minDuration = 10
        videoPlayButton.isHidden = true
    }
    
    func initSelectedVideo(url:String,avAsset:AVAsset,asset:PHAsset, avUrlAsset: URL) {
        print("url:",url)
        self.compressedAsset = avAsset
        self.loadAsset(avAsset)
    }
    
    func loadAsset(_ asset: AVAsset) {
        videoPlayButton.isHidden = false
        durationView.asset = asset
        durationView.delegate = self
        addVideoPlayer(with: asset, playerView: videoView)
    }
    
    func urlOfCurrentlyPlayingInPlayer(player : AVPlayer) -> URL? {
        return ((player.currentItem?.asset) as? AVURLAsset)?.url
    }
    
    private func addVideoPlayer(with asset: AVAsset, playerView: UIView) {
        let playerItem = AVPlayerItem(asset: asset)
        player = AVPlayer(playerItem: playerItem)
        player?.isMuted = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(itemDidFinishPlaying(_:)),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: playerItem)
        
        let layer: AVPlayerLayer = AVPlayerLayer(player: player)
        layer.backgroundColor = UIColor.cE8E8E8.cgColor
        let screen = UIScreen.main.bounds
        let screenWidth = screen.size.width * 0.03
        print("screenWidth:",screenWidth)
        layer.frame = CGRect(x:0, y: 0, width: playerView.frame.width, height: playerView.frame.height)
        //layer.frame = self.videoView.frame
        layer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerView.layer.sublayers?.forEach({$0.removeFromSuperlayer()})
        playerView.layer.addSublayer(layer)
        self.playerLayer = layer
        
        self.compressedVideoURL = urlOfCurrentlyPlayingInPlayer(player: player!)
        print("compressedVideoURL:",compressedVideoURL!)
        
        player?.pause()
        
        NotificationCenter.default.addObserver(self, selector: #selector(loopVideo), name: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func itemDidFinishPlaying(_ notification: Notification) {
        if let startTime = durationView.startTime {
            player?.seek(to: startTime)
        }
        //self.videoPlayButton.isHidden = false
    }
    
    @objc func handleLongGesture(gesture: UILongPressGestureRecognizer) {
        switch(gesture.state) {
        
        case .began:
            guard let selectedIndexPath = addImagecollectionView.indexPathForItem(at: gesture.location(in: addImagecollectionView)) else {
                break
            }
            self.transparentImageView(gesture: gesture)
            addImagecollectionView.beginInteractiveMovementForItem(at: selectedIndexPath)
        case .changed:
            addImagecollectionView.updateInteractiveMovementTargetPosition(gesture.location(in: gesture.view!))
        case .ended:
            self.opaqueImageView(gesture: gesture)
            addImagecollectionView.endInteractiveMovement()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.addImagecollectionView.reloadData()
            }
        default:
            addImagecollectionView.cancelInteractiveMovement()
        }
    }
    
    func opaqueImageView(gesture: UILongPressGestureRecognizer){
        let p = gesture.location(in: self.addImagecollectionView)
        
        if let indexPath = self.addImagecollectionView.indexPathForItem(at: p) {
            let cell = self.addImagecollectionView.cellForItem(at: indexPath) as! ImageListingCollectionViewCell
            self.makeImageViewOpaque(cell:cell)
        } else {
            print("couldn't find index path")
        }
    }
    
    func transparentImageView(gesture: UILongPressGestureRecognizer){
        let p = gesture.location(in: self.addImagecollectionView)
        
        if let indexPath = self.addImagecollectionView.indexPathForItem(at: p) {
            let cell = self.addImagecollectionView.cellForItem(at: indexPath) as! ImageListingCollectionViewCell
            self.makeImageViewTransparent(cell:cell)
        } else {
            print("couldn't find index path")
        }
    }
    
    func makeImageViewTransparent(cell:ImageListingCollectionViewCell){
        cell.backgroundColor = UIColor.clear
        cell.userImageView.alpha = 0.55
    }
    
    func makeImageViewOpaque(cell:ImageListingCollectionViewCell){
        cell.backgroundColor = UIColor.cE8E8E8
        cell.userImageView.alpha = 1
    }
    
    func onTypeSelect(){
        if self.TYPE == AppConstants.TYPE_EDIT_LISTING{
            self.addListingImagesHandler.TYPE = AppConstants.TYPE_EDIT_LISTING
            self.nextButton.setTitle(AppConstants.UPDATE, for: .normal)
            let screen = UIScreen.main.bounds
            var height : CGFloat = 0
            if self.listingDetail?.videoUrl == nil{
                height = screen.height * 0.09
                self.doesnotContainVideo()
            }else{
                height = screen.height * 0.07
                self.containsVideo()
            }
            self.topConstraint.constant = height
            self.mainView.isHidden = true
            self.getListingImages(id: (listingDetail?.id!)!)
        }else{
            self.mainView.isHidden = false
            self.screenActivityIndicator.stopAnimating()
            self.topConstraint.constant = 35
            let sellerPolicyBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.SELLER_POLICY_ICON), style: .plain, target: self, action: #selector(onSellerPolicyBarButtonItem))
            self.navigationItem.rightBarButtonItem  = sellerPolicyBarButton
            navigationItem.title = AppConstants.CREATE_LISTING_TITLE
            self.nextButton.setTitle(AppConstants.NEXT, for: .normal)
            addListingImagesHandler.addImage.append(AddListingImage(userImage: UIImage(named:AppConstants.Images.CREATE_ADD_PHOTO_ICON)!,type:0))
            self.doesnotContainVideo()
        }
    }
    
    func containsVideo(){
        DispatchQueue.main.async {
            self.videoImageView.isHidden = true
            self.videoMainView.isHidden = false
            self.addPlayerToVideo(playerView: self.videoView)
        }
    }
    
    func doesnotContainVideo(){
        self.videoImageView.isHidden = false
        self.videoMainView.isHidden = true
    }
    
    func addPlayerToVideo(playerView:UIView){
        self.videoView.layer.zPosition = 0
        let videoURL = NSURL(string: self.listingDetail?.videoUrl ?? "")
        
        player = AVPlayer(url: videoURL! as URL)
        player?.isMuted = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem)
        
        let layer: AVPlayerLayer = AVPlayerLayer(player: player)
        layer.backgroundColor = UIColor.cE8E8E8.cgColor
        layer.zPosition = -1
        let screen = UIScreen.main.bounds
        let screenWidth = screen.size.width * 0.03
        print("screenWidth:",screenWidth)
        layer.frame = CGRect(x: 0, y: 0, width: playerView.frame.width, height: playerView.frame.height)
        //layer.frame = self.videoView.frame
        layer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerView.layer.sublayers?.forEach({$0.removeFromSuperlayer()})
        playerView.layer.addSublayer(layer)
        self.playerLayer = layer
        self.videoPlayButton.isHidden = false
        self.videoPlayButton.layer.zPosition = 1
        
        player?.pause()
        
        NotificationCenter.default.addObserver(self, selector: #selector(loopVideo), name: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem)
        
    }
    
    @objc func loopVideo() {
        player?.seek(to: CMTime.zero)
        player?.pause()
        self.videoPlayButton.isHidden = false
    }
    
    @objc func playerDidFinishPlaying(notification: NSNotification) {
        if let playerItem = notification.object as? AVPlayerItem {
            playerItem.seek(to: CMTime.zero, completionHandler: nil)
        }
        //self.videoPlayButton.isHidden = false
        print("Video Finished")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationItem.hidesBackButton = true
        DispatchQueue.main.async {
            self.navigationController?.setNavigationBarHidden(false, animated: animated)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if self.TYPE != AppConstants.TYPE_EDIT_LISTING{
            if self.addListingImagesHandler.addImage.count > 1{
                AppConstants.LISTING_LOST = true
            }else{
                AppConstants.LISTING_LOST = false
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.navigationItem.hidesBackButton = false
    }
    
    @objc func onSellerPolicyBarButtonItem(){
        self.sellerPolicyScreen()
    }
    
    func getItemsClick(){
        addListingImagesHandler.oncoverImageClick = {
//            self.imagePickerManager.pickImage(self,cameraTitle: AppConstants.CAMERA,galleryTitle: AppConstants.GALLERY){ image in
//
//                self.addListingImagesHandler.addImage.insert(AddListingImage(userImage: image,type:1), at: self.addListingImagesHandler.addImage.count - 1)
//
//                if self.addListingImagesHandler.addImage.count > 1{
//                    if self.TYPE != AppConstants.TYPE_EDIT_LISTING{
//                        AppConstants.LISTING_LOST = true
//                    }
//                    self.nextButtonView.isHidden = false
//                }else{
//                    self.nextButtonView.isHidden = true
//                    if self.TYPE != AppConstants.TYPE_EDIT_LISTING{
//                        AppConstants.LISTING_LOST = false
//                    }
//                }
//                self.addImagecollectionView.reloadData()
//
//            }
            self.pickImage()
        }
    
        addListingImagesHandler.onRemoveImageClick = {
//            if self.addListingImagesHandler.addImage.count == 1 && self.videoImageView.isHidden == false{
//                self.nextButtonView.isHidden = true
//                if self.TYPE != AppConstants.TYPE_EDIT_LISTING{
//                    AppConstants.LISTING_LOST = false
//                }
//            }else{
//                self.nextButtonView.isHidden = false
//                if self.TYPE != AppConstants.TYPE_EDIT_LISTING{
//                    AppConstants.LISTING_LOST = true
//                }
//            }
            
            if self.addListingImagesHandler.addImage.count == 1{
                self.nextButtonView.isHidden = true
                if self.TYPE != AppConstants.TYPE_EDIT_LISTING{
                    AppConstants.LISTING_LOST = false
                }
            }else{
                self.nextButtonView.isHidden = false
                if self.TYPE != AppConstants.TYPE_EDIT_LISTING{
                    AppConstants.LISTING_LOST = true
                }
            }
            
        }
    }
    
    func getListingImages(id:Int) {
        profileViewModel.getListingImages(accessToken: getAccessToken(), id: id,{(response) in
            self.screenActivityIndicator.stopAnimating()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[GetListingImages]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.listingImagesData = data.data!
                    for i in 0..<self.listingImagesData.count{
                        self.listingImagesData[i].type = 0
                    }
                    self.listingImagesData.sort{$0.displayOrder ?? 0 < $1.displayOrder ?? 0}
                    for listingImage in self.listingImagesData {
                        self.addListingImagesHandler.addImage.append(AddListingImage(imageUrl: listingImage.imageUrl!, imageId: listingImage.listingImageId!, type: 1))
                    }
                    
                    self.addListingImagesHandler.addImage.append(AddListingImage(userImage: UIImage(named:AppConstants.Images.CREATE_ADD_PHOTO_ICON)!,type:0))
                    self.mainView.isHidden = false
                    self.addImagecollectionView.reloadData()
                }
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    @IBAction func nextButtonAction(_ sender: UIButton) {
        if self.TYPE == AppConstants.TYPE_EDIT_LISTING{
            updateImages()
        }else{
            listingDetailScreen()
        }
    }
    
    func updateImages() {
        var deletedImages: [Int] = []
        for listingImage in listingImagesData {
            if !isImageExistNow(listingImage: listingImage) {
                deletedImages.append(listingImage.listingImageId!)
            }
        }
        var newImages: [UIImage] = []
        for listingImage in addListingImagesHandler.addImage {
            if listingImage.type == 1 && listingImage.userImage != nil {
                newImages.append(listingImage.userImage!)
            }
        }
        
        var orderChangedImages:[SliderImages] = []
        var displayOrder = 0
        for listingImage in addListingImagesHandler.addImage{
            if listingImage.imageId != nil{
                displayOrder = displayOrder + 1
                if listingImage.imageId ?? 0 > 0{
                    orderChangedImages.append(SliderImages(listingImageId: listingImage.imageId, displayOrder: displayOrder))
                }
            }
        }
        
        print("delete", deletedImages.count,deletedImages)
        print("new", newImages.count)
        
        if deletedImages.count > 0 &&  newImages.count > 0{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                self.photosDeletedAndUploaded(id :(self.listingDetail?.id!)!,images:newImages,deleteImages: deletedImages)
            }
        }else if newImages.count > 0 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                self.uploadListingImages(id : (self.listingDetail?.id!)!,images:newImages)
            }
        }else if deletedImages.count > 0 &&  newImages.count == 0{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                self.deleteListingImages(id :(self.listingDetail?.id!)!,deleteImages: deletedImages)
            }
        }
        else if deletedImages.count == 0 &&  newImages.count == 0{
            let data = ListingImagesOrder(listingImages: orderChangedImages)
            self.updateImagesOrder(id: listingDetail?.id ?? 0, data: data)
        }
        else{
            self.checkController()
        }
        
    }
    
    func popToUserByn(){
        if !isAlreadyPoped {
            isAlreadyPoped = true
            let myBynViewController = self.navigationController!.viewControllers[1] as! UserBynViewController
            myBynViewController.listNeedsRefresh = true
            self.navigationController!.popToViewController(myBynViewController, animated: false)
        }
    }
    
    func uploadListingImages(id :Int,images:[UIImage]) {
        self.dialogView.isHidden = false
        profileViewModel.updateEditListingImages(accessToken: getAccessToken(), id: id, images: images,{(response) in
            self.dialogView.isHidden = true
            if response.response?.statusCode == 201 || response.response?.statusCode == 200{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    
                    self.checkController()
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.dialogView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func deleteListingImages(id :Int,deleteImages: [Int]) {
        self.dialogView.isHidden = false
        let request = DeleteImagesRequest(imageIds: deleteImages)
        profileViewModel.deleteEditListingImages(accessToken: getAccessToken(), id: id, data: request,{(response) in
            self.dialogView.isHidden = true
            let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
            if response.response?.statusCode == 201 || response.response?.statusCode == 200{
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.checkController()
                }
            }else{
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.dialogView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func deleteVideo(id :Int) {
        createViewModel.deleteListingVideo(accessToken: getAccessToken(), id: id,{(response) in
            let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
            if response.response?.statusCode == 201 || response.response?.statusCode == 200{
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.goToController()
                }
            }else{
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.dialogView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func photosDeletedAndUploaded(id :Int,images:[UIImage],deleteImages: [Int]){
        self.dialogView.isHidden = false
        profileViewModel.updateEditListingImages(accessToken: getAccessToken(), id: id, images: images,{(response) in
            if response.response?.statusCode == 201 || response.response?.statusCode == 200{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let request = DeleteImagesRequest(imageIds: deleteImages)
                    self.profileViewModel.deleteEditListingImages(accessToken: self.getAccessToken(), id: id, data: request,{(response) in
                        self.dialogView.isHidden = true
                        let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                        if response.response?.statusCode == 201 || response.response?.statusCode == 200{
                            if data.isError {
                                let error = Utils.getErrorMessage(errors: data.errors)
                                self.showMessageDialog(message: error)
                            } else {
                                self.checkController()
                            }
                        }else{
                            if data.isError {
                                let error = Utils.getErrorMessage(errors: data.errors)
                                self.showMessageDialog(message: error)
                            }
                        }
                    } , { (error) in
                        self.dialogView.isHidden = true
                        if error.statusCode == AppConstants.NO_INTERNET {
                            self.showInternetError()
                        }
                    })
                    
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.dialogView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func checkController(){
        if self.listingVideoDeleted{
            if self.compressedVideoURL != nil{
                self.uploadVideo(listingId: self.listingDetail?.id ?? 0, videoURL: self.compressedVideoURL!)
            }else{
                if self.doesListingHasVideoUrl{
                    self.deleteVideo(id: self.listingDetail?.id ?? 0)
                }else{
                    self.goToController()
                }
            }
        }else{
            if self.compressedVideoURL == nil{
                self.goToController()
            }else{
                self.uploadVideo(listingId: self.listingDetail?.id ?? 0, videoURL: self.compressedVideoURL!)
            }
        }
    }
    
    func goToController(){
        if let viewControllers = self.navigationController?.viewControllers
        {
            for controller in viewControllers
            {
                if controller is UserBynViewController
                {
                    self.popToUserByn()
                }
                
                if controller is ListDetailViewController
                {
                    self.listingDetailRefreshDelegate?.listingDetailRefresh()
                    self.navigationController?.popViewController(animated: false)
                    //self.userBynViewController()
                }
            }
        }
    }
    
    func isImageExistNow(listingImage: GetListingImages) -> Bool {
        for image in addListingImagesHandler.addImage {
            if image.imageId == listingImage.listingImageId {
                return true
            }
        }
        return false
    }
    
    func listingDetailScreen(){
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.ADD_LISTING_DETAIL_BOARD, bundle:nil)
        let listingDetailViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.LISTING_SCREEN) as! ListingDetailViewController
        listingDetailViewController.selectedImages = addListingImagesHandler.addImage
        listingDetailViewController.compressedVideoURL = self.compressedVideoURL
        self.navigationController?.pushViewController(listingDetailViewController, animated:true)
    }
    
    @IBAction func deleteVideoButtonAction(_ sender: UIButton) {
        if self.TYPE == AppConstants.TYPE_EDIT_LISTING{
            let screen = UIScreen.main.bounds
            var height : CGFloat = 0
            height = screen.height * 0.09
            self.topConstraint.constant = height
            self.listingDetail?.videoUrl = ""
            self.listingVideoDeleted = true
        }else{
            self.topConstraint.constant = 35
        }
        self.compressedVideoURL = nil
        self.videoMainView.isHidden = true
        self.videoImageView.isHidden = false
        if self.addListingImagesHandler.addImage.count > 1{
            self.nextButtonView.isHidden = false
        }else{
            self.nextButtonView.isHidden = true
        }
    }
    
    @IBAction func editVideoButtonAction(_ sender: UIButton) {
        self.editCoverVideo(asset: self.compressedAsset)
    }
    
    @IBAction func videoPlayButtonAction(_ sender: UIButton) {
        self.playButtonAction()
    }
    
    @IBAction func videoImageViewTapAction(_ sender: UITapGestureRecognizer) {
        self.createCoverVideo()
    }
    
    @IBAction func playTapAction(_ sender: UITapGestureRecognizer) {
        self.playButtonAction()
    }
    
    @IBAction func rotateTapAction(_ sender: UITapGestureRecognizer) {
        degree = degree + 90
        self.rotateVideoPlayer(withDegree: degree)
        if degree == 360{
            degree = 0
        }
    }
    
    func degreeToRadian(_ x: CGFloat) -> CGFloat {
        .pi * x / 180.0
    }
    
    func rotateVideoPlayer(withDegree degree: CGFloat) {
        self.playerLayer?.setAffineTransform(CGAffineTransform(rotationAngle: degreeToRadian(degree)))
    }
    
    func playButtonAction(){
        if self.player?.isPlaying ?? false{
            self.player?.pause()
            self.videoPlayButton.isHidden = false
        }else{
            self.player?.play()
            self.videoPlayButton.isHidden = true
        }
    }
    
    func editCoverVideo(asset:AVAsset?){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.COVER_VIDEO_STORYBOARD, bundle: nil)
        let coverVideoViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.COVER_VIDEO_SCREEN) as! CoverVideoViewController
        //coverVideoViewController.editVideo = asset
        coverVideoViewController.convertedVideoDelegate = self
        self.navigationController?.pushViewController(coverVideoViewController, animated: false)
    }
    
    func createCoverVideo(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.COVER_VIDEO_STORYBOARD, bundle: nil)
        let coverVideoViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.COVER_VIDEO_SCREEN) as! CoverVideoViewController
        coverVideoViewController.convertedVideoDelegate = self
        self.navigationController?.pushViewController(coverVideoViewController, animated: false)
    }
    
    func pausePlayer(){
        if player != nil{
            player?.pause()
        }
    }
    
    func uploadVideo(listingId:Int,videoURL:URL){
        self.dialogView.isHidden = false
        self.createViewModel.uploadListingVideo(accessToken: self.getAccessToken(), id: listingId,videoURL: videoURL ,{(response) in
            self.dialogView.isHidden = true
            if response.response?.statusCode == 201 || response.response?.statusCode == 200{
                print("video uploaded successfully!")
                do{
                    try self.deleteCachedVideos()
                }catch let error{
                    print(error)
                }
                self.compressedVideoURL = nil
                self.goToController()
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.dialogView.isHidden = true
            }
        } , { (error) in
            self.dialogView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func updateImagesOrder(id:Int,data:ListingImagesOrder){
        self.dialogView.isHidden = false
        self.createViewModel.updateImagesOrder(accessToken: self.getAccessToken(), data: data,id: id, {(response) in
            self.dialogView.isHidden = true
            if response.response?.statusCode == 201 || response.response?.statusCode == 200{
                self.checkController()
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                
            }
        } , { (error) in
            self.dialogView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
}
extension CreateListingViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: AppConstants.PHOTOS)
    }
    
}
extension CreateListingViewController: TrimmerViewDelegate {
    func positionBarStoppedMoving(_ playerTime: CMTime) {
        player?.seek(to: playerTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
        player?.play()
        if playerTime.seconds == 0{
            self.videoPlayButton.isHidden = false
        }else{
            self.videoPlayButton.isHidden = true
        }
    }
    
    func didChangePositionBar(_ playerTime: CMTime) {
        player?.pause()
        videoPlayButton.isHidden = false
        player?.seek(to: playerTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
        let duration = (durationView.endTime! - durationView.startTime!).seconds
        print(duration)
        print(durationView.startTime!)
    }
}

extension CreateListingViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropViewControllerDelegate {
    
    func cropViewControllerDidCrop(_ cropViewController: CropViewController, cropped: UIImage, transformation: Transformation) {
        //set image
        
        self.addListingImagesHandler.addImage.insert(AddListingImage(userImage: cropped,type:1), at: self.addListingImagesHandler.addImage.count - 1)
        
        if self.addListingImagesHandler.addImage.count > 1{
            if self.TYPE != AppConstants.TYPE_EDIT_LISTING{
                AppConstants.LISTING_LOST = true
            }
            self.nextButtonView.isHidden = false
        }else{
            self.nextButtonView.isHidden = true
            if self.TYPE != AppConstants.TYPE_EDIT_LISTING{
                AppConstants.LISTING_LOST = false
            }
        }
        self.addImagecollectionView.reloadData()
        
        cropViewController.dismiss(animated: false)
    }
    
    func cropViewControllerDidCancel(_ cropViewController: CropViewController, original: UIImage) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    @objc public func pickImage() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let defaultAction = UIAlertAction(title: AppConstants.GALLERY, style: .default) { (action) in
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        let camera = UIAlertAction(title: AppConstants.CAMERA, style: .default) { (action) in
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        
        let cancel = UIAlertAction(title: AppConstants.CANCEL, style: .destructive)
        
        alertController.addAction(defaultAction)
        alertController.addAction(camera)
        alertController.addAction(cancel)
        alertController.modalPresentationStyle = .popover
        
        present(alertController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage) else { return }

        let config = Mantis.Config()
        
        let cropViewController = Mantis.cropViewController(image: image, config: config)
        cropViewController.modalPresentationStyle = .fullScreen
        cropViewController.delegate = self
        cropViewController.config.showRotationDial = false
        cropViewController.config.cropToolbarConfig.toolbarButtonOptions = .reset
        cropViewController.config.cropVisualEffectType = .light
        cropViewController.config.presetFixedRatioType = .alwaysUsingOnePresetFixedRatio(ratio: 16.0 / 16.0)
    
        picker.dismiss(animated: true, completion: {
            self.present(cropViewController, animated: true,completion: nil)
        })
        
    }
    
}
