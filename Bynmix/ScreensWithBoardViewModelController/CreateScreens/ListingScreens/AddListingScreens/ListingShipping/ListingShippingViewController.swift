import UIKit
import Toast_Swift
import XLPagerTabStrip
import Photos

class ListingShippingViewController: BaseViewController {

    @IBOutlet var weightTextField: UITextField!
    @IBOutlet var doneButton: UIButton!
    @IBOutlet var doneView: UIView!
    @IBOutlet var innerView: UIView!
    @IBOutlet var shippingView: UIView!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var noteLabel: UILabel!
    @IBOutlet var tableViewHeight: NSLayoutConstraint!
    
    var selectedImages:[AddListingImage]?
    var titleText:String?
    var shortDescText:String?
    var tagsArray = [AddTags]()
    var conditionId: Int?
    var originalPrice:Int?
    var listingPrice:Int?
    var lowestAllowableOffer:Int?
    var categoryId:Int?
    var subCategoryId:Int?
    var sizeId:Int?
    var colors:[Colors]?
    var brandId:Int?
    let createViewModel = CreateViewModel()
    var shippingRateId:Int?
    var shippingWeight:Double?
    var shippingInfo : [ShippingInfo] = []
    var info : ShippingInfo?
    var TYPE:Int?
    var listingDetail:ListingDetail?
    let profileViewModel = ProfileViewModel()
    var shippingHandler = ShippingHandler()
    var compressedVideoURL:URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = shippingHandler
        tableView.dataSource = shippingHandler
        weightTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        self.innerView.layer.borderWidth = 0.3
        self.innerView.layer.borderColor = UIColor.white.cgColor
        onTypeSelect()
        hideKeyboardWhenTappedAround()
        setText()
    }
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        
//        resizeTableView()
//    }
    
    func setText(){
        let text:String = "NOTE: Be sure to weigh your item and select the correct shipping rate to prevent postal carrier returns."
        let textBold:String = "Be sure to weigh your item and select the correct shipping rate to prevent postal carrier returns."
        let textExtraBold:String = "NOTE:"
        let rangeBold = (text as NSString).range(of: textBold)
        let rangeExtraBold = (text as NSString).range(of: textExtraBold)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontBold(size: 12), range: rangeExtraBold)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontBold(size: 10), range: rangeBold)
        self.noteLabel.attributedText = attributedString
    }
    
    func onTypeSelect(){
        getShippingInfo()
        if self.TYPE == AppConstants.TYPE_EDIT_LISTING{
            self.topConstraint.constant = 56
            let weight = String(format: "%.2f",self.listingDetail?.shippingWeight! ?? 0)
            self.weightTextField.text = weight
            self.doneButton.setTitle(AppConstants.UPDATE, for: .normal)
            enableButton()
            AppConstants.LISTING_LOST = false
        }else{
            AppConstants.LISTING_LOST = true
            self.topConstraint.constant = 16
            self.doneButton.setTitle(AppConstants.Strings.DONE, for: .normal)
            navigationItem.title = AppConstants.CREATE_LISTING_TITLE
            let sellerPolicyBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.SELLER_POLICY_ICON), style: .plain, target: self, action: #selector(onSellerPolicyBarButtonItem))
            self.navigationItem.rightBarButtonItem  = sellerPolicyBarButton
            disableButton()
        }
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
      
        if textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
            enableButton()
        }else{
            disableButton()
        }
        applyBorderBlack()
        weightCall()
    }
    
    func enableButton(){
        self.doneButton.backgroundColor = UIColor.cBB189C
        self.doneButton.isEnabled = true
    }
    
    func disableButton(){
        self.doneButton.backgroundColor = UIColor.cBCBCBC
        self.doneButton.isEnabled = false
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let oldText = textField.text, let r = Range(range, in: oldText) else {
            return true
        }
        
        let newText = oldText.replacingCharacters(in: r, with: string)
        let isNumeric = newText.isEmpty || (Double(newText) != nil)
        let numberOfDots = newText.components(separatedBy: ".").count - 1
        
        let numberOfDecimalDigits: Int
        if let dotIndex = newText.firstIndex(of: ".") {
            numberOfDecimalDigits = newText.distance(from: dotIndex, to: newText.endIndex) - 1
        } else {
            numberOfDecimalDigits = 0
        }
        
        return isNumeric && numberOfDots <= 1 && numberOfDecimalDigits <= 2
        
    }
    
    func uploadListing() {
        self.doneView.isHidden = false
        var tags:[String] = []
        var colorsData:[Int] = []
        
        for i in 1..<tagsArray.count{
            tags.append(tagsArray[i].tag!)
            
            let itemsArray = tags
            let searchToSearch = tagsArray[i].tag!

            let filteredStrings = itemsArray.filter({(item: String) -> Bool in

                let stringMatch = item.lowercased().range(of: searchToSearch.lowercased())
                return stringMatch != nil ? true : false
            })
            print("filteredStrings:",filteredStrings)


            if (filteredStrings as NSArray).count > 0
            {
                //Record found
                //MARK:- You can also print the result and can do any kind of work with them
            }
            else
            {
                //Record Not found
            }
            
        }
        
        
        for i in 0..<colors!.count{
            colorsData.append(colors![i].colorId!)
        }
        
        var listingData :ListingDetail?
        
        if lowestAllowableOffer == nil || lowestAllowableOffer == 0{
            
            listingData = ListingDetail(title: titleText ?? "", description: shortDescText ?? "", tags: tags, conditionId: conditionId ?? 0, originalPrice: originalPrice ?? 0, listingPrice: listingPrice ?? 0, categoryId: subCategoryId ?? 0, categoryTypeId: categoryId ?? 0, sizeId: sizeId ?? 0, colorIds: colorsData, brandId: brandId!,shippingWeight:self.shippingWeight ?? 0,shippingRateId:self.shippingRateId ?? 0, sizeTypeId:5, availabilityId:1, shippingInfo: self.info!)
            
        }else{
            listingData = ListingDetail(title: titleText ?? "", description: shortDescText ?? "", tags: tags, conditionId: conditionId ?? 0, originalPrice: originalPrice ?? 0, listingPrice: listingPrice ?? 0, lowestAllowableOffer: lowestAllowableOffer ?? 0, categoryId: subCategoryId ?? 0, categoryTypeId: categoryId ?? 0, sizeId: sizeId ?? 0, colorIds: colorsData, brandId: brandId ?? 0,shippingWeight:self.shippingWeight ?? 0,shippingRateId:self.shippingRateId ?? 0, sizeTypeId:5, availabilityId:1, shippingInfo: self.info!)
        }
        
        createViewModel.createListing(accessToken: getAccessToken(), data: listingData!,{(response) in
            if response.response?.statusCode == 201 {
                let data: ApiResponse<Int> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let listingId = data.data
                    var images : [UIImage] = []
                    for i in 0..<(self.selectedImages?.count)!-1 {
                        images.append(self.selectedImages![i].userImage!)
                    }
                    if images.count > 0{
                        self.uploadImages(listingId:listingId ?? 0,images: images)
                    }else{
                        self.uploadVideo(listingId:listingId ?? 0,videoURL:self.compressedVideoURL!)
                    }
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.doneView.isHidden = true
            }
        } , { (error) in
            self.doneView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func uploadImages(listingId:Int,images:[UIImage]){
        self.createViewModel.uploadListingImage(accessToken: self.getAccessToken(), id: listingId, images: images, {(response) in
            self.doneView.isHidden = true
            if response.response?.statusCode == 201 {
                let data: ApiResponse<[SliderImages]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    if self.compressedVideoURL == nil{
                        self.listingCompletion(listingId:listingId)
                    }else{
                        self.uploadVideo(listingId:listingId,videoURL:self.compressedVideoURL!)
                    }
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.doneView.isHidden = true
            }
        } , { (error) in
            self.doneView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func uploadVideo(listingId:Int,videoURL:URL){
        self.createViewModel.uploadListingVideo(accessToken: self.getAccessToken(), id: listingId,videoURL: videoURL ,{(response) in
            if response.response?.statusCode == 201 || response.response?.statusCode == 200{
                print("video uploaded successfully!")
                do{
                    try self.deleteCachedVideos()
                }catch let error{
                    print(error)
                }
                self.listingCompletion(listingId:listingId)
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.doneView.isHidden = true
            }
        } , { (error) in
            self.doneView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func listingCompletion(listingId:Int){
        self.trackListing(listingId: listingId)
        AppConstants.LISTING_LOST = false
        self.onCreateClick(type: AppConstants.CLEAR_STACK_OF_CREATE_LISTING_TYPE)
    }
    
    
    func trackListing(listingId:Int){
        self.createViewModel.trackListings(accessToken: self.getAccessToken(), listingId: listingId, {(response) in
            self.doneView.isHidden = true
            if response.response?.statusCode == 201 || response.response?.statusCode == 200{
                print("tracked Listing")
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.doneView.isHidden = true
            }
        } , { (error) in
            self.doneView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getShippingInfo() {
        createViewModel.getShippingSlab(accessToken: AppDefaults.getAccessToken(), { (response) in
            self.screenActivityIndicator.stopAnimating()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[ShippingInfo]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let shippingData = data.data
                    self.shippingInfo = shippingData!
                    self.shippingRateId = self.shippingInfo[0].shippingRateId
                    self.setScreenUI(shipping:self.shippingInfo)
                    
                    if self.TYPE == AppConstants.TYPE_EDIT_LISTING{
                        self.weightCall()
                    }
                }
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func setScreenUI(shipping:[ShippingInfo]){
        shippingHandler.shippingInfo = shipping
        tableView.reloadData()
        applyBorderBlack()
        //self.resizeTableView()
        self.screenActivityIndicator.stopAnimating()
        self.mainScrollView.isHidden = false
    }
    
    func resizeTableView(){
        if self.shippingInfo.count == 0{
            tableViewHeight.constant = 12
        }else{
           var tableViewHeight: CGFloat {
               self.tableView.layoutIfNeeded()
               return self.tableView.contentSize.height
           }
           self.tableViewHeight?.constant = tableViewHeight
        }
    }
    
    func weightCall(){
        let weight:Double = Double(weightTextField.text ?? "") ?? 0
        
        for i in 0..<shippingInfo.count{
            if weight <= shippingInfo[i].shippingWeightMax ?? 0{
                self.shippingWeight = weight
                self.info = shippingInfo[i]
                let indexPath = IndexPath(row: i, section: 0)
                guard let cell = tableView.cellForRow(at: indexPath) as? ShippingTableViewCell else { return }
                let mainView = cell.shippingView!
                let shadowView = cell.shadowView!
                shippingHandler.addBorder(mainView: mainView, shadowView: shadowView)
                break
            }
        }
        tableView.reloadData()
        
    }
    
    func applyBorderBlack(){
        for i in 0..<shippingInfo.count{
            let indexPath = IndexPath(row: i, section: 0)
            guard let cell = tableView.cellForRow(at: indexPath) as? ShippingTableViewCell else { return }
            let mainView = cell.shippingView!
            let shadowView = cell.shadowView!
            shippingHandler.addBorderBlack(mainView: mainView, shadowView: shadowView)
        }
        tableView.reloadData()
    }
    
    @objc func onSellerPolicyBarButtonItem(){
        self.sellerPolicyScreen()
    }
    
    @IBAction func onDoneButtonAction(_ sender: UIButton) {
        if weightTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
            let num = Double(weightTextField.text!)
            if num != 0 && num != Double(AppConstants.DECIMAL) && num != Double("0.") && num! <= 20.0{
                if self.TYPE == AppConstants.TYPE_EDIT_LISTING{
                    //update button action
                    self.updateButton()
                }else{
                    uploadListing()
                }
                
            }else if num! > 20.0 {
                let text:String = AppConstants.Strings.SHIPPING_WEIGHT_CANNOT_BE_GREATER_THAN_20
                showCreatePostMessageDialog(text: text, textColor: AppConstants.Strings.ITEM_WEIGHT)
            }else{
                let text:String = AppConstants.Strings.SHIPPING_WEIGHT_CANNOT_BE_ZERO
                showCreatePostMessageDialog(text: text, textColor: AppConstants.Strings.ITEM_WEIGHT)
            }
            
        }else{
           
        }
        
    }
    
    func updateButton(){
        var listingData : ListingDetail?
        let shortDescription = self.listingDetail?.description
        let shippingWeight = self.weightTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let weight = Double(shippingWeight!)
        if shortDescription != nil{
             listingData = ListingDetail(shippingWeight: weight!, shippingRateId: self.shippingRateId!, shippingInfo: self.info!,description: self.listingDetail?.description ?? "")
        }else{
             listingData = ListingDetail(shippingWeight: weight!, shippingRateId: self.shippingRateId!, shippingInfo: self.info!)
        }
        self.updateData(id: (listingDetail?.id!)!,listingDetail: listingData!)
    }
    
     func updateData(id:Int,listingDetail:ListingDetail) {
           self.doneView.isHidden = false
           profileViewModel.updateEditListing(accessToken: AppDefaults.getAccessToken(), id: id, data: listingDetail, { (response) in
               self.doneView.isHidden = true
               if response.response?.statusCode == 200 {
                   let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                   if data.isError {
                       let error = Utils.getErrorMessage(errors: data.errors)
                       self.showMessageDialog(message: error)
                   } else {
                    if let viewControllers = self.navigationController?.viewControllers
                    {
                        for controller in viewControllers
                        {
                            if controller is UserBynViewController
                            {
                                self.popToUserByn()
                            }
                            
                            if controller is ListDetailViewController
                            {
                                self.navigationController?.popViewController(animated: false)
                                self.userBynViewController()
                            }
                            
                        }
                    }
                   }
               }else{
                   let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                   if data.isError {
                       let error = Utils.getErrorMessage(errors: data.errors)
                       self.showMessageDialog(message: error)
                   }
               }
           } , { (error) in
               self.doneView.isHidden = true
               if error.statusCode == AppConstants.NO_INTERNET {
                   self.showInternetError()
            }
           })
    }
    func popToUserByn(){
        let myBynViewController = self.navigationController!.viewControllers[1] as! UserBynViewController
        myBynViewController.listNeedsRefresh = true
        self.navigationController!.popToViewController(myBynViewController, animated: false)
    }
    
}
extension ListingShippingViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: AppConstants.SHIPPING_TITLE)
    }
    
}
