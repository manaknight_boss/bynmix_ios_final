import UIKit

class SelectShippingMethodViewController: BaseViewController {
    
    
    @IBOutlet var shippingRateLabel: UILabel!
    @IBOutlet var shippingMediumLabel: UILabel!
    @IBOutlet var shippingDateLabel: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var selectButton: UIButton!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var screenActivityIndicator: UIActivityIndicatorView!
    
    var buyerId: Int?
    var addressId: Int?
    var listingId:Int?
    
    var shipRates:[ShipRates] = []
    var selectShippingMethodHandler = SelectShippingMethodHandler()
    var feedViewModel = FeedViewModel()
    var selectShipRateDelegate:SelectShipRateProtocol?
    var selectedRate:ShipRates?
    var TYPE:Int?
    var saleId:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.BACK_BUTTON), style: .plain, target: self, action: #selector(backBttonClick))
        self.navigationItem.leftBarButtonItem  = backBarButton
        backBarButton.imageInsets = UIEdgeInsets(top: 0, left: -7, bottom: 0, right: 0)
        
        tableView.delegate = selectShippingMethodHandler
        tableView.dataSource = selectShippingMethodHandler
        
        navigationItem.title = AppConstants.Strings.SELECT_A_SHIPPING_METHOD_TITLE
        if TYPE == AppConstants.TYPE_SELECT_SHIPPING_METHOD{
            self.setTableViewForPrintShippingLabel(shipRates:self.shipRates)
        }else{
            self.getRates(listingId:self.listingId ?? 0,buyerId:buyerId ?? 0,addressId:addressId ?? 0)
        }
        getratesClick()
    }
    
    @objc func backBttonClick(){
        self.navigationController?.popViewController(animated: false)
    }
    
    func getratesClick(){
        selectShippingMethodHandler.onRateClick = { shipRates in
            self.selectShipRateDelegate?.selectShipRate(shipRate:shipRates, saleId: self.saleId ?? 0)
        }
    }
    
    func getRates(listingId:Int,buyerId:Int,addressId:Int) {
        
        let data = ShipRates(buyerId: buyerId, addressId: addressId)
        feedViewModel.getShippingRates(accessToken: getAccessToken(), listingId: listingId, data: data, {(response) in
            if response.response?.statusCode == 200 || response.response?.statusCode == 201{
                let data: ApiResponse<[ShipRates]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print(error)
                } else {
                    let data = data.data!
                    self.setTableView(shipRates:data)
                }
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.screenActivityIndicator.stopAnimating()
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func setTableView(shipRates:[ShipRates]){
        
        var shippingRates : [ShipRates] = []
        for i in 0..<shipRates.count{
            if shipRates[i].bestValue ?? false && shipRates[i].cheapestValue ?? false{
                self.shippingRateLabel.text = "$\(String(shipRates[i].amount  ?? 0))"
                self.shippingDateLabel.text = shipRates[i].duration ?? ""
                self.shippingMediumLabel.text = shipRates[i].serviceLevelName ?? ""
                self.selectedRate = shipRates[i]
            }else{
                shippingRates.append(shipRates[i])
            }
        }
        self.selectShippingMethodHandler.shipRates = shippingRates
        tableView.reloadData()
        self.screenActivityIndicator.stopAnimating()
        self.tableView.isHidden = false
        self.footerView.isHidden = true
    }
    
    func setTableViewForPrintShippingLabel(shipRates:[ShipRates]){
        
        var shippingRates : [ShipRates] = []
        for i in 0..<shipRates.count{
            if shipRates[i].bestRate ?? false && shipRates[i].cheapestRate ?? false{
                self.shippingRateLabel.text = "$\(String(shipRates[i].rate  ?? 0))"
                self.shippingDateLabel.text = shipRates[i].durationDescription ?? ""
                self.shippingMediumLabel.text = shipRates[i].serviceLevel ?? ""
                self.selectedRate = shipRates[i]
            }else{
                shippingRates.append(shipRates[i])
            }
        }
        self.selectShippingMethodHandler.TYPE = AppConstants.TYPE_SELECT_SHIPPING_METHOD
        self.selectShippingMethodHandler.shipRates = shippingRates
        tableView.reloadData()
        self.screenActivityIndicator.stopAnimating()
        self.tableView.isHidden = false
        self.footerView.isHidden = true
    }
    
    
    @IBAction func editIconTapAction(_ sender: UITapGestureRecognizer) {
        self.selectShipRateDelegate?.selectShipRate(shipRate:self.selectedRate!,saleId: self.saleId ?? 0)
    }
    
    @IBAction func selectButtonAction(_ sender: UIButton) {
        
    }
    
    
    

}
