import UIKit
import Alamofire

class SellerPolicyViewController: BaseViewController,UINavigationBarDelegate {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var backBarButton: UIBarButtonItem!
    
    var sellerPolicyHandler = SellerPolicyHandler()
    let createViewModel = CreateViewModel()
    
    var TYPE:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = sellerPolicyHandler
        tableView.dataSource = sellerPolicyHandler
        if TYPE == AppConstants.TYPE_SALES_TAX{
            getsalesTax()
        }else{
            getsellerPolicy()
        }
        sellerPolicyHandler.onEmailClick = { terms in
            let url = "https://bynmix.com"
            self.openInAppWebView(socialLink:url)
        }
        setNavigationBar()
    }
    
    func openInAppWebView(socialLink:String){
        self.inAppWebBrowserScreen(link: socialLink, title: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if TYPE == AppConstants.TYPE_SALES_TAX{
            self.navigationController?.setNavigationBarHidden(true, animated: animated)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if TYPE == AppConstants.TYPE_SALES_TAX{
            self.navigationController?.setNavigationBarHidden(false, animated: animated)
        }
    }
    
    func setNavigationBar(){
        navBar.barStyle = .black
        navBar.barTintColor = .black
        if TYPE == AppConstants.TYPE_SALES_TAX{
            navBar.topItem?.title =  AppConstants.Strings.SALE_TAX_TITLE
        }else{
            navBar.topItem?.title = AppConstants.Strings.SELLER_POLICY_TITLE
        }
        navBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.fontRegular(size: 16)]
    }
    
    @IBAction func backButtonAction(_ sender: UIBarButtonItem) {
        if TYPE == AppConstants.TYPE_SALES_TAX{
            self.navigationController?.popViewController(animated: false)
        }else{
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    func getsellerPolicy(){
        createViewModel.getSellerPolicy(accessToken: getAccessToken(), { (response) in
            self.handleResponse(response: response)
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            } 
        })
    }
    
    func getsalesTax(){
         createViewModel.getSalesTax(accessToken: getAccessToken(), { (response) in
            self.handleResponse(response: response)
         } , { (error) in
             self.screenActivityIndicator.stopAnimating()
             if error.statusCode == AppConstants.NO_INTERNET {
                 self.showInternetError()
             }
         })
     }
    
    func handleResponse(response: AFDataResponse<Any>){
        if response.response?.statusCode == 200 {
            let data: ApiResponse<Terms> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            } else {
                let sellerPolicyData = data.data
                let sections = sellerPolicyData?.sections
                self.sellerPolicyHandler.TYPE = self.TYPE
                self.sellerPolicyHandler.sellerData = sections!
                self.tableView.reloadData()
                self.screenActivityIndicator.stopAnimating()
                self.tableView.isHidden = false
            }
        }else{
            let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            }
            self.screenActivityIndicator.stopAnimating()
        }
    }

}
