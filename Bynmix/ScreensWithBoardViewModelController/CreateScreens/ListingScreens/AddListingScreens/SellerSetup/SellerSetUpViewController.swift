import UIKit

class SellerSetUpViewController: BaseViewController,AddNewAddressInSellerSetUpProtocol,AddNewPaymentInSellerSetUpProtocol,SelectExistingAddressProtocol,ReplaceCardClickProtocol,StartSellingProtocol,SelectCardProtocol {
    
    func selectCard(type:Int,card: Card) {
        if type == AppConstants.TYPE_ADD_NEW_PAYMENT_IN_EXISTING_CARD{
            
            
        }else if type == AppConstants.TYPE_ADD_PAYMENT_IN_EXISTING_CARD{
            self.navigationController?.popViewController(animated: false)
            
        }
        self.addCard(card: card)
        self.selectedCard = card
    }
    
    func startSelling(type: Int) {
        if type == AppConstants.TYPE_LISTING{
            listingScreen()
        }else if type == AppConstants.TYPE_PROFILE{
            profileScreen()
        }
    }
    
    func replaceCardClick(oldCard: Card, newCard: Card) {
        self.navigationController?.popViewController(animated: false)
        self.addCard(card:newCard)
    }
    
    func SelectExistingAddress(type: Int, address: Address) {
        if type == AppConstants.TYPE_CHANGE_DEFAULT_ADDRESS{
            self.addressId = address.addressId
            self.selectedAddress = address
            addAddress(address: self.selectedAddress!)
        }else if type == AppConstants.TYPE_ADD_NEW_ADDRESS_IN_PAYMENT{
            self.selectedAddress = address
            addAddress(address: self.selectedAddress!)
        }
    }
    
    func AddNewAddressInSellerSetUp(address: Address) {
        self.selectedAddress = address
        addAddress(address:self.selectedAddress!)
    }
    
    func AddNewPaymentInSellerSetUp() {
        self.getMyCard()
    }
    
    @IBOutlet var stepOneStatusImageView: UIImageView!
    @IBOutlet var stepTwoStatusImageView: UIImageView!
    @IBOutlet var stepThreeStatusImageView: UIImageView!
    @IBOutlet var monthLabel: UILabel!
    @IBOutlet var dayLabel: UILabel!
    @IBOutlet var yearLabel: UILabel!
    @IBOutlet var sellingButton: UIButton!
    @IBOutlet var securitynumberTextField: UITextField!
    @IBOutlet var buttonActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var newAddressView: UIView!
    @IBOutlet var newAddressButtonView: UIView!
    @IBOutlet var newPaymentButtonView: UIView!
    @IBOutlet var newPaymentView: UIView!
    @IBOutlet var addressNameLabel: UILabel!
    @IBOutlet var addressLineOne: UILabel!
    @IBOutlet var addressLineTwoLabel: UILabel!
    @IBOutlet var addressThreeLabel:UILabel!
    @IBOutlet var cardImageView: UIImageView!
    @IBOutlet var cardInfoLabel: UILabel!
    @IBOutlet var nameOnCardLabel: UILabel!
    @IBOutlet var expiresInLabel: UILabel!
    
    var TYPE_DAY = 0
    var TYPE_MONTH = 1
    var TYPE_YEAR = 2
    var daySelect = [Int]()
    var monthSelect = [String]()
    var yearSelect = [Int]()
    let profileViewModel = ProfileViewModel()
    let createViewModel = CreateViewModel()
    var securityNumber:String?
    var month:String?
    var day:String?
    var year:String?
    var defaultAddress = false
    var defaultPayment = false
    var addressId:Int?
    var selectedMonth:Int?
    var allCards:[Card] = []
    var isYearSelected = false
    var isMonthSelected = false
    var isDaySelected = false
    var selectedAddress: Address?
    var selectedCard: Card?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.Strings.SELLER_SETUP_TITLE
        hideKeyboardWhenTappedAround()
        buttonActivityIndicator.isHidden = true
        dropDownData()
        securitynumberTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        sellingButton.isEnabled = false
        sellingButton.backgroundColor = UIColor.cCDCDCD
        self.newAddressView.isHidden = true
        self.newPaymentView.isHidden = true
        if selectedAddress == nil{
           self.getAddresses()
        }
        monthLabel.addObserver(self, forKeyPath: "text", options: [.old, .new], context: nil)
        dayLabel.addObserver(self, forKeyPath: "text", options: [.old, .new], context: nil)
        yearLabel.addObserver(self, forKeyPath: "text", options: [.old, .new], context: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.getAddresses()
        self.getMyCard()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "text" {
            
            let previousDOB:String = change?[.oldKey] as! String
            if previousDOB == AppConstants.DAY{
                self.isDaySelected = true
            }
            if previousDOB == AppConstants.YEAR{
                self.isYearSelected = true
            }
            if previousDOB == AppConstants.MONTH{
                self.isMonthSelected = true
            }
            let securityNumber = self.securitynumberTextField.text ?? ""
            let month = monthLabel.text ?? ""
            let day = dayLabel.text ?? ""
            let year = yearLabel.text ?? ""
            if securityNumber.count == 4 && month != AppConstants.MONTH && day != AppConstants.DAY && year != AppConstants.YEAR{
                self.stepOneStatusImageView.image = UIImage(named: AppConstants.Images.FORM_FILL_ICON)
            }else{
                self.stepOneStatusImageView.image = UIImage(named: AppConstants.Images.ERROR_ICON)
            }
            checkIfAllConditionsMet()
        }
    }
    
    func buttonSelected(){
        sellingButton.isEnabled = true
        sellingButton.backgroundColor = UIColor.cBB189C
    }
    
    func buttonUnSelected(){
        sellingButton.isEnabled = false
        sellingButton.backgroundColor = UIColor.cCDCDCD
    }
    
    func listingScreen(){
        self.dismiss(animated: false, completion: nil)
        AppDefaults.setSellerSetup(isSetupDone: true)
        self.navigationController?.popToRootViewController(animated: false)
//        guard let navigationController = self.navigationController else { return }
//        var navigationArray = navigationController.viewControllers // To get all UIViewController stack as Array
//        navigationArray.remove(at: navigationArray.count - 1) // To remove previous UIViewController
//        self.navigationController?.viewControllers = navigationArray
    }
    
    func profileScreen(){
        self.dismiss(animated: false, completion: nil)
        self.tabBarController?.selectedIndex = 4
        if let nav = self.tabBarController?.viewControllers?[4] as? ProfileNavigationViewController {
            nav.popToRootViewController(animated: false)
        }
    }
    
    func checkIfAllConditionsMet() {
        let securityNumber = self.securitynumberTextField.text ?? ""
        let month = monthLabel.text ?? ""
        let day = dayLabel.text ?? ""
        let year = yearLabel.text ?? ""
        if securityNumber.count == 4 && month != AppConstants.MONTH && day != AppConstants.DAY && year != AppConstants.YEAR && self.defaultAddress && self.defaultPayment{
            buttonSelected()
        }else{
            buttonUnSelected()
        }
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        let securityNumber = self.securitynumberTextField.text ?? ""
        let month = monthLabel.text ?? ""
        let day = dayLabel.text ?? ""
        let year = yearLabel.text ?? ""
        if securityNumber.count == 4 && month != AppConstants.MONTH && day != AppConstants.DAY && year != AppConstants.YEAR{
            self.stepOneStatusImageView.image = UIImage(named: AppConstants.Images.FORM_FILL_ICON)
        }else{
            self.stepOneStatusImageView.image = UIImage(named: AppConstants.Images.ERROR_ICON)
        }
        checkIfAllConditionsMet()
    }
    
    func dropDownData(){
        for i in 1...31{
            self.daySelect.append(i)
        }
        for i in stride(from: 2007, through: 1920, by: -1){
            self.yearSelect.append(i)
        }
        self.monthSelect = AppConstants.StaticLists.month
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newLength = (textField.text ?? "").count + string.count - range.length
        if(textField == securitynumberTextField) {
            return newLength <= 4
        }
        return true
    }
    
    func addAddress(address:Address){
        self.addressNameLabel.text = address.fullName
        self.addressLineOne.text = address.addressOne
        self.addressLineTwoLabel.text = address.addressTwo
        let city = address.city ?? ""
        let state = address.stateAbbreviation ?? ""
        let zipcode = address.zipCode ?? ""
        self.addressThreeLabel.text = (city + " " + state + ", " + zipcode)
        self.newAddressView.isHidden = false
        self.newAddressButtonView.isHidden = true
        self.stepTwoStatusImageView.image = UIImage(named: AppConstants.Images.FORM_FILL_ICON)
        self.defaultAddress = true
        self.addressId = address.addressId
        checkIfAllConditionsMet()
    }
    
    func getMyCard() {
        profileViewModel.getMyWalletCard(accessToken: AppDefaults.getAccessToken(), { (response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[Card]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let card = data.data! as [Card]
                    self.allCards = card
                    var isCardFound = false
                    if card.count > 0{
                        for i in 0 ..< card.count {
                            let selectedCard = card[i] as Card
                            if selectedCard.isDefault == true {
                                
                                self.addCard(card:selectedCard)
                                isCardFound = true
                            }
                        }
                    }
                    if(isCardFound) {
                        self.newPaymentButtonView.isHidden  = true
                        self.newPaymentView.isHidden = false
                        self.stepThreeStatusImageView.image = UIImage(named: AppConstants.Images.FORM_FILL_ICON)
                    } else{
                        self.newPaymentButtonView.isHidden  = false
                        self.newPaymentView.isHidden = true
                        self.stepThreeStatusImageView.image = UIImage(named: AppConstants.Images.ERROR_ICON)
                    }
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func addCard(card:Card){
        self.selectedCard = card
        let image = UIImage(named: AppConstants.Images.DEFAULT_CARD_ICON)
        
        if let imageUrl = card.brandImageUrl{
            self.cardImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        }
        
        let brand = card.brand ?? ""
        let lastFour = card.lastFour ?? ""
        let expireMonth = card.expirationMonth ?? 0
        let expireYear = card.expirationYear ?? 0
        
        self.cardInfoLabel.text = brand + " " + AppConstants.Strings.ENDING_IN + " " +  lastFour
        self.nameOnCardLabel.text = card.nameOnCard
        self.expiresInLabel.text = AppConstants.EXPIRES + " " + String(expireMonth) + AppConstants.FORWARD_SLASH + String(expireYear)
        
        let text:String = card.brand! + " " + AppConstants.Strings.ENDING_IN + " " +  card.lastFour!
        let textBrand:String = card.brand!
        let textLastFour:String = card.lastFour!
        let range = (text as NSString).range(of: textLastFour)
        let rangeLastFour = (text as NSString).range(of: textBrand)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: range)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontBold(size: 16), range: rangeLastFour)
        self.cardInfoLabel.attributedText = attributedString
        self.defaultPayment = true
        checkIfAllConditionsMet()
    }
    
    func getAddresses() {
        self.view.makeToastActivity(.center)
        profileViewModel.getAddresses(accessToken: AppDefaults.getAccessToken(), { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[Address]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let addresses = data.data! as [Address]
                    var isAddressFound = false
                    if addresses.count > 0{
                        for i in 0 ..< addresses.count {
                            let address = addresses[i] as Address
                            if address.isReturnAddress == true {
                                self.selectedAddress = address;
                                self.addAddress(address:address)
                                isAddressFound = true
                            }
                        }
                    }
                    if !isAddressFound {
                        self.newAddressView.isHidden = true
                        self.newAddressButtonView.isHidden = false
                        self.stepTwoStatusImageView.image = UIImage(named: AppConstants.Images.ERROR_ICON)
                    }
                    self.getMyCard()
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func sendSellerDetail(){
        self.view.makeToastActivity(.center)
        let securityNumber = self.securitynumberTextField.text ?? ""
        let month = self.selectedMonth ?? 1
        let day:Int = Int(dayLabel.text ?? "")!
        let year = Int(yearLabel.text ?? "")!
        let data = SellerPersonelInfo(dobDay: day, dobMonth: month, dobYear: year, addressId: self.addressId!, lastFourSocial: securityNumber)
        createViewModel.sendSellerDetail(accessToken: getAccessToken(), data: data, { (response) in
            if response.response?.statusCode == 201 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    self.setReturnShipping()
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func setReturnShipping() {
        var address = self.selectedAddress
        address?.isReturnAddress = true
        address?.needsVerification = false
        profileViewModel.updateAddresses(accessToken: getAccessToken(), id: (address?.addressId!)!, data: address!,{(response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    self.setDefaultCard()
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func setDefaultCard() {
        var cardAdd = self.selectedCard
        cardAdd?.isDefault = true
        profileViewModel.editPayment(accessToken: getAccessToken(), id: cardAdd!.cardId!, data: cardAdd!,{(response) in
            self.view.hideToastActivity()
            let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
            if response.response?.statusCode == 200 {
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    self.withdrawDialog()
                }
            }else{
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func withdrawDialog(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.WITHDRAW_DIALOG_BOARD, bundle: nil)
        let  startSellingViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.WITHDRAW_DIALOG) as! WithdrawDialogViewController
        startSellingViewController.TYPE = AppConstants.TYPE_START_SELLING
        startSellingViewController.modalPresentationStyle = .overFullScreen
        startSellingViewController.startSellingDelegate = self
        self.present(startSellingViewController, animated: false,completion: nil)
    }
    
    @IBAction func defaultAddressButtonAction(_ sender: UIButton) {
        let mainStoryBoard = UIStoryboard(name: AppConstants.Storyboard.ADD_NEW_ADDRESS_BOARD, bundle: nil)
        let addressViewController = mainStoryBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.ADD_NEW_ADDRESS_SCREEN) as! AddNewAddressViewController
        addressViewController.addNewAddressInSellerSetUpDelegate = self
        self.navigationController!.pushViewController(addressViewController, animated: false)
    }
 
    @IBAction func defaultPaymentButtonAction(_ sender: UIButton) {
        let mainStoryBoard = UIStoryboard(name: AppConstants.Storyboard.ADD_NEW_PAYMENT_BOARD, bundle: nil)
        let paymentViewController = mainStoryBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.ADD_NEW_PAYMENT_SCREEN) as! AddNewPaymentViewController
        paymentViewController.addNewPaymentInSellerSetUpDelegate = self
        //paymentViewController.TYPE = AppConstants.TYPE_SELECT_EXISTING_PAYMENT
        self.navigationController!.pushViewController(paymentViewController, animated: false)
    }
    
    @IBAction func sellingButtonAction(_ sender: UIButton) {
        sendSellerDetail()
    }
    
    @IBAction func onMonthTap(_ sender: UITapGestureRecognizer) {
        onMonthTapAction()
    }
    
    @IBAction func onDayTap(_ sender: UITapGestureRecognizer) {
      onDayTapAction()
    }
    
    @IBAction func yearTap(_ sender: UITapGestureRecognizer) {
      onYearTapAction()
    }
    
    func onYearTapAction(){
        showDropDown(tag: TYPE_YEAR, title: AppConstants.YEAR, dataSourse: self, delegate: self, {(row) in
            self.yearLabel.textColor = UIColor.black
            self.yearLabel.text = String(self.yearSelect[row])
            self.isYearSelected = true
        })
    }
    
    func onMonthTapAction(){
        showDropDown(tag: TYPE_MONTH, title: AppConstants.MONTH, dataSourse: self, delegate: self, {(row) in
            self.monthLabel.textColor = UIColor.black
            self.monthLabel.text = self.monthSelect[row]
            self.isMonthSelected = true
        })
    }
    
    func onDayTapAction(){
        showDropDown(tag: TYPE_DAY, title: AppConstants.DAY, dataSourse: self, delegate: self, {(row) in
            self.dayLabel.textColor = UIColor.black
            self.dayLabel.text = String(self.daySelect[row])
            self.isDaySelected = true
        })
    }
    
    @IBAction func editAddressButtonAction(_ sender: UIButton) {
        let mainStoryBoard = UIStoryboard(name: AppConstants.Storyboard.SELECT_EXISTING_ADDRESS_BOARD, bundle: nil)
        let addressViewController = mainStoryBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.SELECT_EXISTING_ADDRESS_SCREEN) as! SelectExistingAddressViewController
        addressViewController.TYPE = AppConstants.TYPE_CHANGE_DEFAULT_ADDRESS
        addressViewController.selectExistingAddressDelegate = self
        self.navigationController!.pushViewController(addressViewController, animated: false)
    }
    
    @IBAction func editpaymentButtonAction(_ sender: UIButton) {
        selectCard()
    }
    
    func selectCard(){
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.SELECT_EXISTING_CARD_BOARD, bundle:nil)
        let selectCardViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.SELECT_EXISTING_CARD_SCREEN) as! SelectExistingCardViewController
        selectCardViewController.card = self.allCards
        selectCardViewController.selectCardDelegate = self
        self.navigationController?.pushViewController(selectCardViewController, animated:true)
    }
    
    @IBAction func monthButtonAction(_ sender: UIButton) {
        onMonthTapAction()
    }
    
    @IBAction func dayButtonAction(_ sender: UIButton) {
        onDayTapAction()
    }
    
    @IBAction func yearButtonAction(_ sender: UIButton) {
        onYearTapAction()
    }
    
}
extension SellerSetUpViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == TYPE_DAY {
            return daySelect.count
        }else if pickerView.tag == TYPE_MONTH {
            return monthSelect.count
        }else{
            return yearSelect.count
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == TYPE_DAY {
            return String(daySelect[row])
        }else if pickerView.tag == TYPE_MONTH {
            return monthSelect[row]
        }else {
            return String(yearSelect[row])
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == TYPE_MONTH {
            print(row + 1)
            self.selectedMonth = row + 1
        }
    }

}
