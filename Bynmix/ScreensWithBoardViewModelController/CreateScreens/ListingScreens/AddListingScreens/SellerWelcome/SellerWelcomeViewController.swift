import UIKit

class SellerWelcomeViewController: BaseViewController {

    @IBOutlet var usernameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let text:String = AppConstants.HI + " " + AppDefaults.getUsername() + AppConstants.EXCLAMATION_MARK
        let username:String = AppDefaults.getUsername()
        let range = (text as NSString).range(of: username)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 22), range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cFF6DE3, range: range)
        usernameLabel.attributedText = attributedString
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.navigationBar.isHidden = true
        
        if AppDefaults.isSellerSetupDone(){
            let mainStoryBoard = UIStoryboard(name: AppConstants.Storyboard.CREATE_LISTING_BOARD, bundle: nil)
            let createListingViewController = mainStoryBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.CREATE_LISTING_SCREEN) as! CreateListingViewController
            self.navigationController!.setViewControllers([createListingViewController], animated: false)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func sellerSetUpScreen(_ sender: UIButton) {
        let loginVC = UIStoryboard(name: AppConstants.Storyboard.SELLER_SETUP_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.SELLER_SETUP_SCREEN) as! SellerSetUpViewController
        self.navigationController!.pushViewController(loginVC, animated: true)
    }
    
}
