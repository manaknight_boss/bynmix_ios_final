import UIKit

class SelectShippingRateViewController: BaseViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var screenActivityIndicator: UIActivityIndicatorView!
    
    var shippingRateHandler = SelectShippingRateHandler()
    let createViewModel = CreateViewModel()
    var shippingWeightInDetail = ShippingWeightInDetail()
    var TYPE:Int?
    var selectedRate:ShippingInfo?
    var shippingInfo:[ShippingInfo] = []
    var getRateDelegate:GetRateProtocol?
    var isShippingInfoNil:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let backBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.BACK_BUTTON), style: .plain, target: self, action: #selector(backBttonClick))
        self.navigationItem.leftBarButtonItem  = backBarButton
        backBarButton.imageInsets = UIEdgeInsets(top: 0, left: -7, bottom: 0, right: 0)
        
        let sellerPolicyBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.SELLER_POLICY_ICON), style: .plain, target: self, action: #selector(onSellerPolicyBarButtonItem))
        self.navigationItem.rightBarButtonItem  = sellerPolicyBarButton
        tableView.delegate = shippingRateHandler
        tableView.dataSource = shippingRateHandler
        tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 10, right: 0)
        
        getShippingInfo()
        onRateClick()
        if TYPE == AppConstants.TYPE_EDIT_LISTING{
            navigationItem.title = AppConstants.Strings.EDIT_LISTING_TITLE
        }else{
            navigationItem.title = AppConstants.CREATE_LISTING_TITLE
        }
    }
    
    @objc func backBttonClick(){
        self.navigationController?.popViewController(animated: false)
    }
    
    @objc func onSellerPolicyBarButtonItem(){
        self.sellerPolicyScreen()
    }
    
    func onRateClick(){
        self.shippingRateHandler.onRateClick = { shippingInfo in
            self.selectedRate = shippingInfo
            self.getRateDelegate?.getRate(shippingInfo: shippingInfo)
        }
    }
    
    func getShippingInfo() {
        createViewModel.getShippingSlab(accessToken: AppDefaults.getAccessToken(), { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[ShippingInfo]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let shippingData = data.data!
                    self.shippingInfo = shippingData
                    self.shippingRateHandler.shippingInfo = shippingData
                    self.shippingRateHandler.shippingWeightInDetail = self.shippingWeightInDetail
                    self.tableView.reloadData()
                    self.screenActivityIndicator.stopAnimating()
                    self.tableView.isHidden = false
                    
                    if self.shippingInfo.count > 0{
                        for i in 0..<self.shippingRateHandler.shippingInfo.count{
                            if self.selectedRate != nil && self.selectedRate?.shippingRateId == self.shippingRateHandler.shippingInfo[i].shippingRateId{
                                if self.isShippingInfoNil{
                                    self.shippingRateHandler.shippingInfo[i].isSelected = false
                                }else{
                                    self.shippingRateHandler.shippingInfo[i].isSelected = true
                                }
                            }else{
                                self.shippingRateHandler.shippingInfo[i].isSelected = false
                            }
                        }
                        self.tableView.reloadData()
                    }
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } 
                self.screenActivityIndicator.stopAnimating()
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }


}
