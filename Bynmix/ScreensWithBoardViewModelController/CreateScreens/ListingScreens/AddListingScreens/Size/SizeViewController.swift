import UIKit
import XLPagerTabStrip

class SizeViewController: BaseViewController {
    
    @IBOutlet var sizeTabsTableView: UITableView!
    
    let sizeTabHandler = SizeTabHandler()
    let createViewModel = CreateViewModel()
    var tabName: String?
    var sizeList: [SizeType] = []
    var sizeSelectDelegate:SizeSelectProtocol?
    var selectedSize:Sizes?
  
    override func viewDidLoad() {
        super.viewDidLoad()
        sizeTabsTableView.delegate = sizeTabHandler
        sizeTabsTableView.dataSource = sizeTabHandler
        sizeTabsTableView.backgroundColor = UIColor.cE8E8E8
        sizeTabsTableView.contentInset = UIEdgeInsets(top: 40, left: 0, bottom: 0, right: 0)
        sizeTabHandler.onSizesClick = { id in
            for i in 0..<self.sizeList.count {
                let sizes = self.sizeList[i].sizes
                for j in 0..<sizes!.count {
                    if sizes![j].sizeId == id {
                        self.sizeSelectDelegate?.sizeSelect(size: sizes![j])
                        self.navigationController?.popViewController(animated: false)
                        break
                    }
                }
            }
        }
        getSizes()
    }
    
    func getSizes() {
        self.view.makeToastActivity(.center)
        createViewModel.getSizes(accessToken: AppDefaults.getAccessToken(), { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[SizeType]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    var allSizes = data.data!
                    
                    for i in 0..<allSizes.count {
                        var sizes = allSizes[i].sizes
                        var requiredSizes: [Sizes] = []
                        for j in 0..<sizes!.count {
                            if sizes![j].fit == self.tabName {
                                if self.selectedSize != nil && sizes![j].sizeId == self.selectedSize?.sizeId {
                                    sizes![j].isSelected = true
                                } else {
                                    sizes![j].isSelected = false
                                }
                                requiredSizes.append(sizes![j])
                            }
                        }
                        if requiredSizes.count > 0 {
                            allSizes[i].sizes = requiredSizes
                            self.sizeList.append(allSizes[i])
                        }
                    }
                    self.sizeTabHandler.sizeData = self.sizeList
                    self.sizeTabsTableView.reloadData()
                    self.view.hideToastActivity()
                }
            }else{
                self.view.hideToastActivity()
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    
}

extension SizeViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: tabName?.uppercased())
    }
}
