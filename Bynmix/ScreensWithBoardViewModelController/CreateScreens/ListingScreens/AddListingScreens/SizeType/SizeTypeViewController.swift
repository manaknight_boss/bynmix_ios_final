import UIKit
import XLPagerTabStrip

class SizeTypeViewController: ButtonBarPagerTabStripViewController {
    
    var sellerPolicyBarButtonItem:UIBarButtonItem?
    let createViewModel = CreateViewModel()
    var baseViewController = BaseViewController()
    var sizeNames: [String] = []
    let sizeInfoHandler = SizeInfoHandler()
    var sizeSelectDelegate:SizeSelectProtocol?
    var selectedSize:Sizes?
    @IBOutlet var leftButton: UIButton!
    @IBOutlet var rightButton: UIButton!
    
    override func viewDidLoad() {
        loadDesign()
        getSizeTabName()
        super.viewDidLoad()
        view.isUserInteractionEnabled = false
        navigationItem.title = AppConstants.Strings.SELECT_SIZE_TITLE
        let sellerPolicyBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.SELLER_POLICY_ICON), style: .plain, target: self, action: #selector(onSellerPolicyBarButtonItem))
        self.navigationItem.rightBarButtonItem  = sellerPolicyBarButton
        let backBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.BACK_BUTTON), style: .plain, target: self, action: #selector(onBackBarButtonItem))
        self.navigationItem.leftBarButtonItem  = backBarButton
        leftButton.contentVerticalAlignment = .fill
        rightButton.contentVerticalAlignment = .fill
        leftButton.contentHorizontalAlignment = .fill
        rightButton.contentHorizontalAlignment = .fill
        
    }
    
    @objc func onSellerPolicyBarButtonItem(){
        sellerPolicyScreen()
    }
    
    func sellerPolicyScreen(){
        let sellerPolicyController = UIStoryboard(name: AppConstants.Storyboard.SELLER_POLICY_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.SELLER_POLICY_SCREEN) as! SellerPolicyViewController
        sellerPolicyController.modalPresentationStyle = .overFullScreen
        self.present(sellerPolicyController, animated: false, completion: nil)
    }
    
    @objc func onBackBarButtonItem(){
        self.navigationController?.popViewController(animated: false)
    }
    
    override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool){
        super.updateIndicator(for: viewController, fromIndex: fromIndex, toIndex: toIndex, withProgressPercentage: progressPercentage, indexWasChanged: indexWasChanged)
        if indexWasChanged{
            if currentIndex == 0{
                self.leftButton.isHidden = true
                self.rightButton.isHidden = false
            }else if currentIndex == sizeNames.count - 1{
                self.leftButton.isHidden = false
                self.rightButton.isHidden = true
            }else{
                self.rightButton.isHidden = false
                self.leftButton.isHidden = false
            }
        }
        
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        var viewControllers : [UIViewController] = []
        if sizeNames.count > 0 {
            for i in 0..<sizeNames.count {
                let tabSizeViewController = UIStoryboard(name: AppConstants.Storyboard.SIZE_BOARD, bundle: nil).instantiateViewController(withIdentifier: "SizeTestSID") as! SizeViewController
                
                tabSizeViewController.tabName = sizeNames[i]
                tabSizeViewController.sizeSelectDelegate = self.sizeSelectDelegate
                tabSizeViewController.selectedSize = self.selectedSize
                viewControllers.append(tabSizeViewController)
            }
        } else {
            let tabSizeViewController = UIStoryboard(name: AppConstants.Storyboard.SIZE_BOARD, bundle: nil).instantiateViewController(withIdentifier: "SizeTestSID") as! SizeViewController
            
            tabSizeViewController.tabName = ""
            tabSizeViewController.sizeSelectDelegate = self.sizeSelectDelegate
            viewControllers.append(tabSizeViewController)
        }
        
        viewControllers.forEach { let _ = $0.view }
        
        return viewControllers
    }
    
    func loadDesign() {
        self.settings.style.buttonBarBackgroundColor = UIColor.white
        self.settings.style.buttonBarItemBackgroundColor = UIColor.white
        self.settings.style.selectedBarBackgroundColor = UIColor.cFB9D52
        self.settings.style.buttonBarItemFont = UIFont.fontSemiBold(size: 12)
        self.settings.style.selectedBarHeight = 2.0
        self.settings.style.buttonBarMinimumLineSpacing = 0
        self.settings.style.buttonBarItemTitleColor = UIColor.black
        self.settings.style.buttonBarItemsShouldFillAvailableWidth = true
        self.settings.style.buttonBarLeftContentInset = 10
        self.settings.style.buttonBarRightContentInset = 10
        
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            
            oldCell?.label.textColor = UIColor.c2A2A2A
            oldCell?.label.font = UIFont.fontSemiBold(size: 12)
            newCell?.label.textColor = UIColor.cBB189C
            newCell?.label.font = UIFont.fontSemiBold(size: 12)
           
        }
    }
    
    func getSizeTabName() {
        createViewModel.getSizeTabName(accessToken: AppDefaults.getAccessToken(), { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[String]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.baseViewController.showMessageDialog(message: error)
                } else {
                    self.sizeNames = data.data!
                    self.reloadPagerTabStripView()
                    if self.selectedSize != nil{
                        var index = 0
                        for i in 0..<self.sizeNames.count{
                            if self.selectedSize?.fit == self.sizeNames[i]{
                                index = i
                                break
                            }
                        }
                        self.moveToViewController(at: index, animated: true)
                    }
                    //self.getSizes()
                    self.view.isUserInteractionEnabled = true
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.baseViewController.showInternetError()
            }
        })
    }
    
    func getSizes() {
        createViewModel.getSizes(accessToken: AppDefaults.getAccessToken(), { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[SizeType]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.baseViewController.showMessageDialog(message: error)
                } else {
                    let allSizes = data.data!
                    var tabName:String?
                    
                    for i in 0..<allSizes.count {
                        let sizes = allSizes[i].sizes
                        for j in 0..<sizes!.count {
                        tabName = sizes![j].fit
                        }
                    }
                    
                    var index = 0
                    for i in 0..<self.sizeNames.count{
                        if tabName == self.sizeNames[i]{
                            index = i
                            break
                        }
                    }
                    
                    self.moveToViewController(at: index, animated: true)
                    //set value
                    self.view.isUserInteractionEnabled = true
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.baseViewController.showInternetError()
            }
        })
    }
    
    @IBAction func leftButtonAction(_ sender: UIButton) {
        moveToViewController(at: currentIndex - 1, animated: true)
    }
    
    @IBAction func rightButtonAction(_ sender: UIButton) {
        moveToViewController(at: currentIndex + 1, animated: true)
    }
    
    
}
