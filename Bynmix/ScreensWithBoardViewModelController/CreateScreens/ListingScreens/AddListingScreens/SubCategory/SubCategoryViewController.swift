import UIKit

class SubCategoryViewController: BaseViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet var categoryLabel: UILabel!
    @IBOutlet var outerView: UIView!
    
    var subCategoryHandler = SubCategoryHandler()
    var categoryType : CategoryType?
    var subCategorySelected : Category?
    var categorySelectionDelegate : CategorySelectionProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = subCategoryHandler
        tableView.dataSource = subCategoryHandler
        var categories = categoryType?.categories
        for i in 0..<categories!.count {
            if self.subCategorySelected != nil && categories![i].categoryId == self.subCategorySelected?.categoryId {
                categories![i].isSelected = true
            } else {
                categories![i].isSelected = false
            }
        }
        subCategoryHandler.categoryType = categories!
        categoryLabel.text = categoryType!.categoryTypeName?.uppercased()
        tableView.reloadData()
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        subCategoryHandler.onSubCategoryClick = { category in
            self.categorySelectionDelegate?.onCategoryClick(categoryType: self.categoryType!, selectedCategory: category)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func backButtonAction(_ sender: UIButton) {
       self.dismiss(animated: true, completion: nil)
    }
    
    @objc func tapDetected() {
        self.dismiss(animated: true, completion: nil)
    }
    
}
