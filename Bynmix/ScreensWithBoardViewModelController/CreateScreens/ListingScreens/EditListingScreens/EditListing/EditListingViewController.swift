import UIKit
import XLPagerTabStrip

class EditListingViewController: ButtonBarPagerTabStripViewController,UITabBarControllerDelegate,CreateClickProtocol {
    
    func onCreateClick(type: Int) {
         if type == AppConstants.CREATE_TYPE_POST {
             if let nav = self.tabBarController?.viewControllers?[2] as? CreateNavigationViewController {
                 nav.viewControllerType = AppConstants.CREATE_TYPE_POST
                 nav.popToRootViewController(animated: false)
             }
             self.tabBarController?.selectedIndex = 2
         } else if type == AppConstants.CLEAR_STACK_OF_CREATE_SCREEN_TYPE {
             AppConstants.IS_JUST_CREATED = AppConstants.CREATE_TYPE_POST
             self.tabBarController?.selectedIndex = 4
             if let nav = self.tabBarController?.viewControllers?[4] as? ProfileNavigationViewController {
                 nav.popToRootViewController(animated: false)
             }
         } else if type == AppConstants.CLEAR_STACK_OF_CREATE_LISTING_TYPE {
             AppConstants.IS_JUST_CREATED = AppConstants.CREATE_TYPE_LISTING
             self.tabBarController?.selectedIndex = 4
             if let nav = self.tabBarController?.viewControllers?[4] as? ProfileNavigationViewController {
                 nav.popToRootViewController(animated: false)
             }
         } else if type == AppConstants.CREATE_TYPE_LISTING {
             if let nav = self.tabBarController?.viewControllers?[2] as? CreateNavigationViewController {
                 nav.popToRootViewController(animated: false)
                 nav.viewControllerType = AppConstants.CREATE_TYPE_LISTING
             }
             self.tabBarController?.selectedIndex = 2
         } else if type == AppConstants.CREATE_TYPE_NONE {
             // todo fix here
             //BaseViewController.self.selectedTab = -1
         }else{
             print("else block")
         }
     }
    
    var listingDetail:ListingDetail?
    var listingDetailRefreshDelegate:ListingDetailRefreshProtocol?
    
    override func viewDidLoad() {
        loadDesign()
        super.viewDidLoad()
        navigationItem.title = AppConstants.Strings.EDIT_LISTING_TITLE
        buttonBarView.translatesAutoresizingMaskIntoConstraints = true
        self.view.hideToastActivity()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.reloadPagerTabStripView()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        buttonBarView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
           self.tabBarController?.delegate = self
       }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let tabPhotosViewController = UIStoryboard(name: AppConstants.Storyboard.CREATE_LISTING_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.CREATE_LISTING_SCREEN) as! CreateListingViewController
        tabPhotosViewController.TYPE = AppConstants.TYPE_EDIT_LISTING
        tabPhotosViewController.listingDetail = self.listingDetail
        tabPhotosViewController.listingDetailRefreshDelegate = listingDetailRefreshDelegate
        
        let tabBasicsViewController = UIStoryboard(name: AppConstants.Storyboard.EDIT_LISTING_BASICS_STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.EDIT_LISTING_BASICS_SCREEN) as! EditListingBasicViewController
        tabBasicsViewController.listingDetail = self.listingDetail
        tabBasicsViewController.listingDetailRefreshDelegate = listingDetailRefreshDelegate
        
        let tabDetailsViewController = UIStoryboard(name: AppConstants.Storyboard.EDIT_LISTING_DETAILS_STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.EDIT_LISTING_DETAILS_SCREEN) as! EditListingDetailsViewController
        tabDetailsViewController.listingDetail = self.listingDetail
        tabDetailsViewController.listingDetailRefreshDelegate = listingDetailRefreshDelegate
        
//        let tabShippingViewController = UIStoryboard(name: AppConstants.Storyboard.LISTING_SHIPPING_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.SHIPPING_SCREEN) as! ListingShippingViewController
//        tabShippingViewController.TYPE = AppConstants.TYPE_EDIT_LISTING
//        tabShippingViewController.listingDetail = self.listingDetail
        
        let tabCalculateShippingViewController = UIStoryboard(name: AppConstants.Storyboard.CALCULATE_SHIPPING_STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.CALCULATE_SHIPPING_SCREEN) as! CalculateShippingViewController
        tabCalculateShippingViewController.TYPE = AppConstants.TYPE_EDIT_LISTING
        tabCalculateShippingViewController.listingDetail = self.listingDetail
        tabCalculateShippingViewController.listingDetailRefreshDelegate = listingDetailRefreshDelegate
        return [tabPhotosViewController,tabBasicsViewController,tabDetailsViewController,tabCalculateShippingViewController]
    }
    
    func loadDesign() {
        self.settings.style.buttonBarBackgroundColor = UIColor.white
        self.settings.style.buttonBarItemBackgroundColor = UIColor.white
        self.settings.style.selectedBarBackgroundColor = UIColor.cFB9D52
        self.settings.style.buttonBarItemFont = UIFont.fontSemiBold(size: 12)
        self.settings.style.selectedBarHeight = 2.0
        self.settings.style.buttonBarMinimumLineSpacing = 0
        self.settings.style.buttonBarItemTitleColor = UIColor.black
        self.settings.style.buttonBarItemsShouldFillAvailableWidth = true
        self.settings.style.buttonBarLeftContentInset = 10
        self.settings.style.buttonBarRightContentInset = 10
        
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            
            oldCell?.label.textColor = UIColor.c2A2A2A
            oldCell?.label.font = UIFont.fontSemiBold(size: 12)
            newCell?.label.textColor = UIColor.cBB189C
            newCell?.label.font = UIFont.fontSemiBold(size: 12)
            
        }
    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
           self.dismiss(animated: false, completion: nil)
           let selectedIndex = tabBarController.viewControllers!.firstIndex(of: viewController)!
           AppConstants.SELECTED_TAB = selectedIndex
           if viewController is CreateNavigationViewController || selectedIndex == 2{
               
               let createViewController = tabBarController.storyboard?.instantiateViewController(withIdentifier: AppConstants.Controllers.CREATE_SCREEN) as! CreateViewController
               createViewController.modalPresentationStyle = .overCurrentContext
               createViewController.createClickDelegate = self
               tabBarController.present(createViewController, animated: false,completion: nil)
               return false
           }
           
           return true
       }


}
