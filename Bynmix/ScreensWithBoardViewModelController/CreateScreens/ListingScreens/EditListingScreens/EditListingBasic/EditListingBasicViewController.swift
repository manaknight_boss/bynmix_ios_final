import UIKit
import XLPagerTabStrip

class EditListingBasicViewController: BaseViewController,onPostTypeChangeProtocol,onTagsClickProtocol {
    
    func onTagsClick(type: Int) {
        if type == 1{
            UIView.animate(withDuration: 0.5) {
                self.topConstraint.constant = -450
                self.view.layoutIfNeeded()
            }
        }else if type == 3{
            let seconds = 0.4
            DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
                
            UIView.animate(withDuration: 0.4) {
                self.topConstraint.constant = 56
                self.view.layoutIfNeeded()
            }
            let bottomOffset = CGPoint(x: 0, y: self.screenScrollView.contentSize.height - self.screenScrollView.bounds.size.height)
            self.screenScrollView.setContentOffset(bottomOffset, animated: true)
            
            }
            
        }
    }
    
    func onPostTypeChange(type: Int) {
        if type == AppConstants.TYPE_DIRECT_PURCHASE{
            self.directPurchaseSwitch.isOn = true
            self.allowanceOfferView.isHidden = true
        }else{
            self.directPurchaseSwitch.isOn = false
        }
    }
    
    @IBOutlet var titleTextField: UITextField!
    @IBOutlet var descriptionTextView: UITextView!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var originalPriceTextField: UITextField!
    @IBOutlet var listingPriceTextField: UITextField!
    @IBOutlet var directPurchaseSwitch: UISwitch!
    @IBOutlet var allowanceOfferView: UIView!
    @IBOutlet var lowestAllowanceOfferTextField: UITextField!
    @IBOutlet var updateButton: UIButton!
    @IBOutlet var screenScrollView: UIScrollView!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
        
    var listingDetail:ListingDetail?
    let profileViewModel = ProfileViewModel()
    var selectStatus = [ListingStatuses]()
    var selectedStatus:Int?
    var type_status = 0
    let tagsHandler = AddTagsHandler()
    var listingDetailRefreshDelegate:ListingDetailRefreshProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = tagsHandler
        collectionView.dataSource = tagsHandler
        setListingData(listingDetail: listingDetail!)
        hideKeyboardWhenTappedAround()
        tagsHandler.onTagsClickDelegate = self
        initalTopConstraintValueOnTap()
        //keyboardRequired(topConstraint: topConstraint)
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == self.titleTextField {
            
        }else{
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField != self.titleTextField{
            
            guard let oldText = textField.text, let r = Range(range, in: oldText) else {
                return true
            }
            
            let newText = oldText.replacingCharacters(in: r, with: string)
            let isNumeric = newText.isEmpty || (Double(newText) != nil)
            let numberOfDots = newText.components(separatedBy: ".").count - 1
            
            let numberOfDecimalDigits: Int
            if let dotIndex = newText.firstIndex(of: ".") {
                numberOfDecimalDigits = newText.distance(from: dotIndex, to: newText.endIndex) - 1
            } else {
                numberOfDecimalDigits = 0
            }
            
            return isNumeric && numberOfDots <= 1 && numberOfDecimalDigits <= 2
        }else if textField == self.titleTextField{
            let maxLength = 150
            let currentString: NSString = (textField.text ?? "") as NSString
            let newString: NSString =
                currentString.replacingCharacters(in: range, with: string) as NSString
            return newString.length <= maxLength
        }else{
            return true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        directPurchaseSwitch.layer.borderColor = UIColor.c696969.cgColor
        directPurchaseSwitch.layer.borderWidth = 2
        directPurchaseSwitch.layer.cornerRadius = 16
    }
    
    func initalTopConstraintValueOnTap() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(topConstraintInitialValue))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func topConstraintInitialValue() {
        view.endEditing(true)
        
        UIView.animate(withDuration: 0.5) {
            self.topConstraint.constant = 56
            self.view.layoutIfNeeded()
        }
        self.tagsHandler.enterTagCollectionViewCell?.headerView.isHidden = false
        self.tagsHandler.enterTagCollectionViewCell?.dataView.isHidden = true
        self.tagsHandler.enterTagCollectionViewCell?.addTagTextField.text = ""
    }
    
    func setListingData(listingDetail:ListingDetail){
        self.tagsHandler.dataObject.append(AddTags(tag:"", type:1))
        self.titleTextField.text = listingDetail.title
        let description =  listingDetail.description
        if !(description == nil || description == ""){
            self.descriptionTextView.text = listingDetail.description
            self.descriptionTextView.textColor = UIColor.black
            self.descriptionTextView.font = UIFont.fontRegular(size: 18)
        }else{
            descriptionTextView.text = AppConstants.Strings.SHORT_DESCRIPTION_ABOUT_ITEM_TEXT
            descriptionTextView.textColor = UIColor.cDCDCDC
            descriptionTextView.font = UIFont.fontRegular(size: 18)
        }
        self.originalPriceTextField.text = String(listingDetail.originalPrice!)
        self.listingPriceTextField.text = String(listingDetail.price!)
        
        if !listingDetail.isBuyItNowOnly!{
            self.directPurchaseSwitch.isOn = false
            self.allowanceOfferView.isHidden = false
            self.stackViewHeight.constant = 95
            if let lowestOffer = listingDetail.lowestAllowableOffer {
                self.lowestAllowanceOfferTextField.text = String(lowestOffer)
            } else {
                self.lowestAllowanceOfferTextField.text = ""
            }
        }else{
            self.directPurchaseSwitch.isOn = true
            self.allowanceOfferView.isHidden = true
            self.stackViewHeight.constant = 0
        }
        
        if listingDetail.listingTags != nil{
            let tags = listingDetail.listingTags!.count
            let listingTags = listingDetail.listingTags!
            for i in 0..<tags{
                self.tagsHandler.dataObject.append(AddTags(listingTags:(listingTags[i]),type:2))
            }
            self.collectionView.reloadData()
        }
        
        self.getListingStatuses()
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if descriptionTextView.text == AppConstants.Strings.SHORT_DESCRIPTION_ABOUT_ITEM_TEXT {
            descriptionTextView.text = nil
            descriptionTextView.textColor = UIColor.black
            descriptionTextView.font = UIFont.fontRegular(size: 18)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if descriptionTextView.text.isEmpty {
            descriptionTextView.text = AppConstants.Strings.SHORT_DESCRIPTION_ABOUT_ITEM_TEXT
            descriptionTextView.textColor = UIColor.cDCDCDC
            descriptionTextView.font = UIFont.fontRegular(size: 18)
        }
    }
    
    @IBAction func updateButtonAction(_ sender: UIButton) {
        let titleTextField = self.titleTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let shortDesc = self.descriptionTextView.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let addtagsCount = self.tagsHandler.dataObject.count
        let originalPrice = self.originalPriceTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let listingPrice = self.listingPriceTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let lowestAllowableOffer = self.lowestAllowanceOfferTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let statusText = statusLabel.text
        let listPriceInt = Double(listingPrice ?? "1")
        
        var status:Int = 0
        for i in 0..<selectStatus.count{
            if statusText == selectStatus[i].statusName{
                status = selectStatus[i].status!
            }
        }
        
        if titleTextField == ""{
            let text:String = AppConstants.Strings.LISTING_TITLE_REQUIRED
            let textColor:String = AppConstants.Strings.LISTING_TITLE
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        if shortDesc == "" || shortDesc == AppConstants.Strings.SHORT_DESCRIPTION_ABOUT_ITEM_TEXT{
            let text:String = AppConstants.Strings.SHORT_DESC_REQUIRED
            let textColor:String = AppConstants.Strings.SHORT_DESCRIPTION_TEXT
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        if addtagsCount == 1{
            let text:String = AppConstants.Strings.LISTING_TAGS_REQUIRED
            let textColor:String = AppConstants.Strings.LISTING_TAGS
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        if originalPrice == ""{
            let text:String = AppConstants.Strings.ORIGINAL_PRICE_REQUIRED
            let textColor:String = AppConstants.Strings.ORIGINAL_PRICE
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        if listingPrice == ""{
            let text:String = AppConstants.Strings.LISTING_PRICE_REQUIRED
            let textColor:String = AppConstants.Strings.LISTING_PRICE
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        
        if listingPrice != "" && listPriceInt! < 10{
            let text:String = AppConstants.Strings.LISTING_PRICE_TEN_DOLLAR_TEXT
            let textColor:String = AppConstants.Strings.LISTING_PRICE
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        
        if  lowestAllowableOffer != "" && Double(listingPrice ?? "0")! <= Double(lowestAllowableOffer ?? "1")!{
            let text:String = AppConstants.Strings.LOWEST_ALLOWABLE_OFFER
            let textColor:String = ""
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        
        if lowestAllowableOffer != "" && Double(lowestAllowableOffer ?? "1")! < 10{
            let text:String = AppConstants.Strings.LOWEST_OFFER_TEN_DOLLAR_TEXT
            let textColor:String = AppConstants.Strings.LOWEST_OFFER
            showCreatePostMessageDialog(text: text, textColor: textColor)
            return
        }
        
        var tags:[String] = []
        if addtagsCount != 0{
            
            for i in 1..<addtagsCount{
                if self.tagsHandler.dataObject[i].tag != nil{
                    tags.append((self.tagsHandler.dataObject[i].tag!))
                    
                    let itemsArray = tags
                    let searchToSearch = self.tagsHandler.dataObject[i].tag!
                    
                    let filteredStrings = itemsArray.filter({(item: String) -> Bool in
                        
                        let stringMatch = item.lowercased().range(of: searchToSearch.lowercased())
                        return stringMatch != nil ? true : false
                    })
                    print("filteredStrings:",filteredStrings)
                    
                    
                    if (filteredStrings as NSArray).count > 0
                    {
                        //Record found
                        //MARK:- You can also print the result and can do any kind of work with them
                    }
                    else
                    {
                        //Record Not found
                    }
                    
                    
                }
                if self.tagsHandler.dataObject[i].listingTags?.tagName != nil{
                    tags.append((self.tagsHandler.dataObject[i].listingTags?.tagName!)!)
                    
                    let itemsArray = tags
                    let searchToSearch = (self.tagsHandler.dataObject[i].listingTags?.tagName!)!
                    
                    let filteredStrings = itemsArray.filter({(item: String) -> Bool in
                        
                        let stringMatch = item.lowercased().range(of: searchToSearch.lowercased())
                        return stringMatch != nil ? true : false
                    })
                    print("filteredStrings:",filteredStrings)
                    
                    
                    if (filteredStrings as NSArray).count > 0
                    {
                        //Record found
                        //MARK:- You can also print the result and can do any kind of work with them
                    }
                    else
                    {
                        //Record Not found
                    }
                }
            }
        }
        
        let originPrice = Int(originalPrice!)!
        let listPrice = Int(listingPrice!)!
        let lowestOffer = Int(lowestAllowableOffer ?? "") ?? 0
        var listingData: ListingDetail
        
        if lowestAllowableOffer == ""{
            listingData = ListingDetail(title: titleTextField!, description: shortDesc!, tags: tags, originalPrice: originPrice, listingPrice: listPrice, statusId: status, isBuyNowOnly: self.directPurchaseSwitch.isOn)
        } else {
            listingData = ListingDetail(title: titleTextField!, description: shortDesc!, tags: tags, originalPrice: originPrice, listingPrice: listPrice, lowestAllowableOffer: lowestOffer, statusId: status, isBuyNowOnly: self.directPurchaseSwitch.isOn)
        }
        
        self.updateData(id: (listingDetail?.id!)!,listingDetail: listingData )
        
    }
    
    func getListingStatuses() {
        profileViewModel.getListingStatuses(accessToken: AppDefaults.getAccessToken(), { (response) in
            self.screenActivityIndicator.stopAnimating()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[ListingStatuses]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let statusData = data.data
                    self.selectStatus = statusData!
                    for status in statusData! {
                        if status.status == self.listingDetail?.status?.status {
                            self.setCurrentStatus(status: status)
                        }
                    }
                    self.scrollView.isHidden = false
                }
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func setCurrentStatus(status: ListingStatuses) {
        self.statusLabel.text = status.statusName
    }
    
    func updateData(id:Int,listingDetail:ListingDetail) {
        self.dialogView.isHidden = false
        profileViewModel.updateEditListing(accessToken: AppDefaults.getAccessToken(), id: id, data: listingDetail, { (response) in
            self.dialogView.isHidden = true
            if response.response?.statusCode == 200 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    if let viewControllers = self.navigationController?.viewControllers
                    {
                        for controller in viewControllers
                        {
                            if controller is UserBynViewController
                            {
                                self.popToUserByn()
                            }
                            
                            if controller is ListDetailViewController
                            {
                                self.listingDetailRefreshDelegate?.listingDetailRefresh()
                                self.navigationController?.popViewController(animated: false)
                                self.userBynViewController()
                            }
                            
                        }
                    }
                    
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.dialogView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func popToUserByn(){
        let myBynViewController = self.navigationController!.viewControllers[1] as! UserBynViewController
        myBynViewController.listNeedsRefresh = true
        self.navigationController!.popToViewController(myBynViewController, animated: false)
    }
    
    @IBAction func statusTapAction(_ sender: UITapGestureRecognizer) {
        onStatusTapAction()
    }
    
    @IBAction func directPurchaseSwitchAction(_ sender: UISwitch) {
        print("change")
    }
    
    @IBAction func statusButtonAction(_ sender: UIButton) {
        onStatusTapAction()
    }
    
    func onStatusTapAction(){
        showDropDown(tag: type_status, title: AppConstants.STAUS_TITLE, dataSourse: self, delegate: self, {(row) in
            self.statusLabel.textColor = UIColor.black
            self.statusLabel.text = self.selectStatus[row].statusName
        })
    }
    
    @IBAction func onDirectPurchaseAction(_ sender: UISwitch) {
        
        if (self.directPurchaseSwitch?.isOn)!{
            let storyBoard = UIStoryboard(name: AppConstants.Storyboard.POST_CHECK_DIALOG_BOARD, bundle: nil)
            let postCheckViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.POST_CHECK_DIALOG) as! PostCheckDialogViewController
            postCheckViewController.delegate = self
            postCheckViewController.TYPE = AppConstants.TYPE_DIRECT_PURCHASE
            self.navigationController?.present(postCheckViewController, animated: false)
        }else{
            self.allowanceOfferView.isHidden = false
            self.stackViewHeight.constant = 95
        }
        
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
}
extension EditListingBasicViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: AppConstants.BASICS)
    }
    
}
extension EditListingBasicViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        return selectStatus.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return selectStatus[row].statusName
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(row + 1)
        self.selectedStatus = row + 1
    }
    
    
}
