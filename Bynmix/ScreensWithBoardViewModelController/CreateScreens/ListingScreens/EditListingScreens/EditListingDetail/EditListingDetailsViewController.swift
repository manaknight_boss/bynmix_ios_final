import UIKit
import XLPagerTabStrip

class EditListingDetailsViewController: BaseViewController,ColorSelectProtocol, CategorySelectionProtocol,BrandsSelectProtocol,SizeSelectProtocol {
    
    func colorSelect(selectedColors: [Colors]) {
        self.colorsStackView.isHidden = false
        self.colorTwoView.isHidden = true
        self.selectedColors = selectedColors
        self.colorNameOneLabel.text = selectedColors[0].colorName
        self.visibleColorOneview.backgroundColor = UIColor(hex: selectedColors[0].hexCode!)
        if selectedColors.count == 2 {
            self.colorTwoView.isHidden = false
            self.colorNameTwoLabel.text = selectedColors[1].colorName
            self.visibleColorTwoview.backgroundColor = UIColor(hex: selectedColors[1].hexCode!)
        }
    }
    
    func onCategoryClick(categoryType: CategoryType, selectedCategory: Category) {
        self.mainCategoryView.isHidden = false
        self.categoryLabel.text = categoryType.categoryTypeName
        self.categoryTypeLabel.text = selectedCategory.categoryName
        self.selectedCategory = categoryType
        self.selectedSubCategory = selectedCategory
        self.dismiss(animated: false, completion: nil)
        self.navigationController?.popViewController(animated: false)
    }
    
    func brandsSelect(brand: Brand) {
        self.mainBrandView.isHidden = false
        self.brandLabel.text = brand.brandName
        self.selectedBrand = brand
    }
    
    func sizeSelect(size: Sizes) {
        self.mainSizeView.isHidden = false
        self.sizelabel.text = size.sizeName
        self.selectedSize = size
    }
    
    @IBOutlet var conditionSwitch: UISwitch!
    @IBOutlet var categoryView: UIView!
    @IBOutlet var categoryTypeView: UIView!
    @IBOutlet var categoryLabel: UILabel!
    @IBOutlet var categoryTypeLabel: UILabel!
    @IBOutlet var sizelabel: UILabel!
    @IBOutlet var sizeView: UIView!
    @IBOutlet var colorOneView: UIView!
    @IBOutlet var colorTwoView: UIView!
    @IBOutlet var visibleColorOneview: UIView!
    @IBOutlet var visibleColorTwoview: UIView!
    @IBOutlet var colorNameOneLabel: UILabel!
    @IBOutlet var colorNameTwoLabel: UILabel!
    @IBOutlet var brandView: UIView!
    @IBOutlet var brandLabel: UILabel!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var screenScrollView: UIScrollView!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var mainCategoryView: UIView!
    @IBOutlet var mainSizeView: UIView!
    @IBOutlet var mainBrandView: UIView!
    @IBOutlet var colorsStackView: UIStackView!
    @IBOutlet weak var dialogView: UIView!

    var selectedCategory:CategoryType?
    var selectedSubCategory:Category?
    var selectedColors:[Colors]?
    var selectedBrand:Brand?
    var selectedImages:[AddListingImage]?
    var selectedSize:Sizes?
    let tagsHandler = AddTagsHandler()
    var listingDetail:ListingDetail?
    let profileViewModel = ProfileViewModel()
    var listingDetailRefreshDelegate:ListingDetailRefreshProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setData(listingDetail:self.listingDetail!)
        hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
          conditionSwitch.layer.borderColor = UIColor.c696969.cgColor
          conditionSwitch.layer.borderWidth = 2
          conditionSwitch.layer.cornerRadius = 16
          addBorderBlack(view:categoryView)
          addBorderBlack(view:categoryTypeView)
          addBorderBlack(view:sizeView)
          addBorderBlack(view:colorOneView)
          addBorderBlack(view:colorTwoView)
          addBorderBlack(view:brandView)
          colorRoundView(view:visibleColorOneview)
          colorRoundView(view:visibleColorTwoview)
      }
    
    func addBorderBlack(view:UIView){
        view.layer.borderWidth = 1.0
        view.layer.borderColor = UIColor.c545454.cgColor
        view.layer.masksToBounds = true
    }
    
    func colorRoundView(view:UIView){
        view.layer.borderWidth = 0.5
        view.layer.masksToBounds = false
        view.layer.borderColor = UIColor.c2E2E2E.cgColor
        view.clipsToBounds = true
        view.layer.cornerRadius = view.frame.size.width/2
    }
    
    func setData(listingDetail:ListingDetail){
        if listingDetail.condition?.conditionId == 1{
            conditionSwitch.isOn = false
        }else{
            conditionSwitch.isOn = true
        }
        
        categoryLabel.text = listingDetail.categoryType?.categoryTypeName!
        categoryTypeLabel.text = listingDetail.category?.categoryName!
        sizelabel.text = listingDetail.size?.sizeName!
        colorNameOneLabel.text = listingDetail.colors![0].colorName
        let colorOne = listingDetail.colors![0].hexCode!
        visibleColorOneview.backgroundColor = UIColor(hex: colorOne)
        if listingDetail.colors!.count > 1{
           colorNameTwoLabel.text = listingDetail.colors![1].colorName
            let colorTwo = listingDetail.colors![1].hexCode!
            visibleColorTwoview.backgroundColor = UIColor(hex: colorTwo)
            colorTwoView.isHidden = false
        }else{
            colorTwoView.isHidden = true
        }
        brandLabel.text = listingDetail.brand?.brandName!
        screenActivityIndicator.stopAnimating()
        screenScrollView.isHidden = false
        self.selectedCategory = listingDetail.categoryType!
        self.selectedSubCategory = listingDetail.category!
        self.selectedColors = listingDetail.colors!
        self.selectedBrand = listingDetail.brand!
        self.selectedSize = listingDetail.size!
    }
    
    @IBAction func categoryTapAction(_ sender: UITapGestureRecognizer) {
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.CATEGORY_BOARD, bundle:nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.CATEGORY_SCREEN) as! CategoryViewController
        viewController.categorySelectionDelegate = self
        viewController.categorySelected = self.selectedCategory
        viewController.subCategorySelected = self.selectedSubCategory
        self.navigationController?.pushViewController(viewController, animated:false)
    }
    
    @IBAction func sizeTapAction(_ sender: UITapGestureRecognizer) {
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.SIZE_TYPE_BOARD, bundle:nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.SIZES_TABS_SCREEN) as! SizeTypeViewController
        viewController.sizeSelectDelegate = self
        viewController.selectedSize = self.selectedSize
        self.navigationController?.pushViewController(viewController, animated:false)
    }
    
    @IBAction func colorOneTapAction(_ sender: UITapGestureRecognizer) {
        onColorClick()
    }
    
    @IBAction func colorTwoTapAction(_ sender: UITapGestureRecognizer) {
        onColorClick()
    }
    
    func onColorClick() {
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.COLORS_BOARD, bundle:nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.COLORS_SCREEN) as! ColorsViewController
        viewController.colorsSelectDelegate = self
        viewController.selectedColors  = self.selectedColors!
        self.navigationController?.pushViewController(viewController, animated:false)
    }
    
    @IBAction func brandTapAction(_ sender: UITapGestureRecognizer) {
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.BRANDS_BOARD, bundle:nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.BRANDS_SCREEN) as! BrandsViewController
        viewController.brandSelectDelegate = self
        viewController.selectedBrand = self.selectedBrand
        self.navigationController?.pushViewController(viewController, animated:false)
    }
    
    @IBAction func UpdateButtonAction(_ sender: UIButton) {
        
        var conditionId:Int = 0
        
        if self.conditionSwitch.isOn {
            conditionId = 2
        }else{
            conditionId = 1
        }
        
        var colorsData:[Int] = []
    
        for i in 0..<selectedColors!.count{
            colorsData.append(selectedColors![i].colorId!)
        }
        
        let listingData = ListingDetail(conditionId: conditionId, categoryId: (self.selectedSubCategory?.categoryId!)!, categoryTypeId: (self.selectedCategory?.categoryTypeId!)!, sizeId: self.selectedSize!.sizeId!, colorIds: colorsData, brandId: self.selectedBrand!.brandId!, sizeTypeId: 1,lowestAllowableOffer: (self.listingDetail?.lowestAllowableOffer ?? 0) ,description: (self.listingDetail?.description ?? "") )
        
        self.updateData(id: (listingDetail?.id!)!,listingDetail: listingData)
    }
    
    func updateData(id:Int,listingDetail:ListingDetail) {
        self.dialogView.isHidden = false
        profileViewModel.updateEditListing(accessToken: AppDefaults.getAccessToken(), id: id, data: listingDetail, { (response) in
            self.dialogView.isHidden = true
            if response.response?.statusCode == 200 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    if let viewControllers = self.navigationController?.viewControllers
                    {
                        for controller in viewControllers
                        {
                            if controller is UserBynViewController
                            {
                                self.popToUserByn()
                            }
                            
                            if controller is ListDetailViewController
                            {
                                self.listingDetailRefreshDelegate?.listingDetailRefresh()
                                self.navigationController?.popViewController(animated: false)
                                self.userBynViewController()
                            }
                            
                        }
                    }
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.dialogView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func popToUserByn(){
        let myBynViewController = self.navigationController!.viewControllers[1] as! UserBynViewController
        myBynViewController.listNeedsRefresh = true
        self.navigationController!.popToViewController(myBynViewController, animated: false)
    }
    
}

extension EditListingDetailsViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: AppConstants.DETAILS)
    }
    
}
