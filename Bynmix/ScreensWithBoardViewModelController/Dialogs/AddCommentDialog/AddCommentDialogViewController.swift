import UIKit

class AddCommentDialogViewController: BaseViewController {
    
    @IBOutlet weak var innerView: UIView!
    @IBOutlet var outerView: UIView!
    @IBOutlet var textView: UITextView!
    @IBOutlet weak var evidenceOneImageView: UIImageView!
    @IBOutlet weak var evidenceTwoImageView: UIImageView!
    @IBOutlet weak var evidenceThreeImageView: UIImageView!
    @IBOutlet weak var photosView: UIView!
    @IBOutlet weak var evidenceOneRemoveButton: UIButton!
    @IBOutlet weak var evidenceTwoRemoveButton: UIButton!
    @IBOutlet weak var evidenceThreeRemoveButton: UIButton!
    
    var noCommentDelegate:NoCommentProtocol?
    var addCommentInDisputeDelegate:AddCommentInDisputeProtocol?
    var disputeDetails:DisputeDetails?
    let imagePickerManager = ImagePickerManager()
    var isThereProblemWithReturn = false
    var imageList:[UIImage] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.innerView.layer.borderWidth = 0.3
        self.innerView.layer.borderColor = UIColor.white.cgColor
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
        
        textView.text = AppConstants.Strings.COMMENT_IN_DISPUTE
        textView.textColor = UIColor.cADADAD
        self.textView.font = UIFont.fontRegular(size: 16)
        self.textView.delegate = self
        
        if self.isThereProblemWithReturn{
            photosView.isHidden = false
        }else{
            photosView.isHidden = true
        }
        evidenceOneImageView.image = UIImage(named: AppConstants.Images.ADD_PHOTO_ICON)
        evidenceTwoImageView.image = UIImage(named: AppConstants.Images.ADD_PHOTO_ICON)
        evidenceThreeImageView.image = UIImage(named: AppConstants.Images.ADD_PHOTO_ICON)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == AppConstants.Strings.COMMENT_IN_DISPUTE {
            textView.text = nil
            textView.textColor = UIColor.cD3D3D3
            textView.font = UIFont.fontRegular(size: 18)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = AppConstants.Strings.COMMENT_IN_DISPUTE
            textView.textColor = UIColor.cADADAD
            textView.font = UIFont.fontRegular(size: 18)
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        return numberOfChars < 250
    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        let commment = textView.text.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if evidenceOneImageView.image != UIImage(named: AppConstants.Images.ADD_PHOTO_ICON){
            self.imageList.append(evidenceOneImageView!.image!)
        }
        if evidenceTwoImageView.image != UIImage(named: AppConstants.Images.ADD_PHOTO_ICON){
            self.imageList.append(evidenceTwoImageView!.image!)
        }
        if evidenceThreeImageView.image != UIImage(named: AppConstants.Images.ADD_PHOTO_ICON){
            self.imageList.append(evidenceThreeImageView!.image!)
        }
        
        if commment != AppConstants.Strings.ENTER_YOUR_COMMENT_HERE && commment != ""{
            addCommentInDisputeDelegate?.addCommentInDispute(dispute: self.disputeDetails!, comment: commment,images: self.imageList)
        }else{
            noCommentDelegate?.noComment()
        }
    }
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func closeButtonAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func tapDetected() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func click() {
    }
    
    @IBAction func imageOneTapAction(_ sender: UITapGestureRecognizer) {
        if evidenceOneImageView.image == UIImage(named: AppConstants.Images.ADD_PHOTO_ICON){
            imagePickerManager.pickImage(self,cameraTitle: AppConstants.CAMERA,galleryTitle: AppConstants.GALLERY){ image in
                self.evidenceOneImageView.image = image
                self.evidenceOneRemoveButton.isHidden = false
                self.evidenceOneImageView.layer.masksToBounds = true
                self.evidenceOneImageView.layer.borderWidth = 1
                self.evidenceOneImageView.layer.borderColor = UIColor.black.cgColor
                self.evidenceOneImageView.layer.cornerRadius = 5
            }
        }else{
            print("Hello world")
        }
    }
    
    @IBAction func imageTwoTapAction(_ sender: UITapGestureRecognizer) {
        if evidenceTwoImageView.image == UIImage(named: AppConstants.Images.ADD_PHOTO_ICON){
            imagePickerManager.pickImage(self,cameraTitle: AppConstants.CAMERA,galleryTitle: AppConstants.GALLERY){ image in
                self.evidenceTwoImageView.image = image
                self.evidenceTwoRemoveButton.isHidden = false
                self.evidenceTwoImageView.layer.masksToBounds = true
                self.evidenceTwoImageView.layer.borderWidth = 1
                self.evidenceTwoImageView.layer.borderColor = UIColor.black.cgColor
                self.evidenceTwoImageView.layer.cornerRadius = 5
            }
        }else{
            print("Hello world")
        }
    }
    
    @IBAction func imageThreeTapAction(_ sender: UITapGestureRecognizer) {
        if evidenceThreeImageView.image == UIImage(named: AppConstants.Images.ADD_PHOTO_ICON){
            imagePickerManager.pickImage(self,cameraTitle: AppConstants.CAMERA,galleryTitle: AppConstants.GALLERY){ image in
                self.evidenceThreeImageView.image = image
                self.evidenceThreeRemoveButton.isHidden = false
                self.evidenceThreeImageView.layer.masksToBounds = true
                self.evidenceThreeImageView.layer.borderWidth = 1
                self.evidenceThreeImageView.layer.borderColor = UIColor.black.cgColor
                self.evidenceThreeImageView.layer.cornerRadius = 5
            }
        }else{
            print("Hello world")
        }
    }
    
    @IBAction func evidenceOneRemoveButtonAction(_ sender: UIButton) {
        evidenceOneImageView.image = UIImage(named: AppConstants.Images.ADD_PHOTO_ICON)
        self.evidenceOneRemoveButton.isHidden = true
        self.evidenceOneImageView.layer.masksToBounds = false
        self.evidenceOneImageView.layer.borderWidth = 0
        self.evidenceOneImageView.layer.borderColor = UIColor.clear.cgColor
        self.evidenceOneImageView.layer.cornerRadius = 0
        
    }
    
    @IBAction func evidenceTwoRemoveButtonAction(_ sender: UIButton) {
        evidenceTwoImageView.image = UIImage(named: AppConstants.Images.ADD_PHOTO_ICON)
        self.evidenceTwoRemoveButton.isHidden = true
        self.evidenceTwoImageView.layer.masksToBounds = false
        self.evidenceTwoImageView.layer.borderWidth = 0
        self.evidenceTwoImageView.layer.borderColor = UIColor.clear.cgColor
        self.evidenceTwoImageView.layer.cornerRadius = 0
        
    }
    
    @IBAction func evidenceThreeRemoveButtonAction(_ sender: UIButton) {
        evidenceThreeImageView.image = UIImage(named: AppConstants.Images.ADD_PHOTO_ICON)
        self.evidenceThreeRemoveButton.isHidden = true
        self.evidenceThreeImageView.layer.masksToBounds = false
        self.evidenceThreeImageView.layer.borderWidth = 0
        self.evidenceThreeImageView.layer.borderColor = UIColor.clear.cgColor
        self.evidenceThreeImageView.layer.cornerRadius = 0
    }
    
}
