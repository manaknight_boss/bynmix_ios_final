import UIKit

class ConnectURLDialogViewController: BaseViewController {
    
    @IBOutlet var innerView: UIView!
    @IBOutlet var outerView: UIView!
    @IBOutlet var textLabel: UILabel!
    @IBOutlet var iconImageView: UIImageView!
    @IBOutlet var urlTextView: UITextView!
    @IBOutlet var errorButton: UIButton!
    
    var TYPE:Int?
    var urlDialogDelegate:URLDialogProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.errorButton.isHidden = true
        self.innerView.layer.borderWidth = 0.3
        self.innerView.layer.borderColor = UIColor.white.cgColor
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
        
        self.onTypeChange()
    }
    
    func onTypeChange(){
        switch TYPE {
        case AppConstants.TYPE_ACCOUNT_TIKTOK:
            self.textLabel.text = AppConstants.Strings.CONNECT_TIKTOK_ACCOUNT
            self.iconImageView.image = UIImage(named: AppConstants.Images.TIKTOK_DIALOG_ICON)
            self.urlTextView.text = AppConstants.Strings.COPY_AND_PASTE_TIKTOK_URL
        case AppConstants.TYPE_ACCOUNT_FB:
            self.textLabel.text = AppConstants.Strings.CONNECT_FB_ACCOUNT
            self.iconImageView.image = UIImage(named: AppConstants.Images.SOCIAL_LINK_FACEBOOK_DIALOG_ICON)
            self.urlTextView.text = AppConstants.Strings.COPY_AND_PASTE_FB_URL
        case AppConstants.TYPE_ACCOUNT_YOUTUBE:
            self.textLabel.text = AppConstants.Strings.CONNECT_YOUTUBE_ACCOUNT
            self.iconImageView.image = UIImage(named: AppConstants.Images.SOCIAL_LINK_YOUTUBE_DIALOG_ICON)
            self.urlTextView.text = AppConstants.Strings.COPY_AND_PASTE_YOUTUBE_URL
        case AppConstants.TYPE_ACCOUNT_INSTAGRAM:
            self.textLabel.text = AppConstants.Strings.CONNECT_INSTAGRAM_ACCOUNT
            self.iconImageView.image = UIImage(named: AppConstants.Images.SOCIAL_LINK_INSTAGRAM_DIALOG_ICON)
            self.urlTextView.text = AppConstants.Strings.COPY_AND_PASTE_INSTAGRAM_URL
        case AppConstants.TYPE_ACCOUNT_PINTEREST:
            self.textLabel.text = AppConstants.Strings.CONNECT_PINTEREST_ACCOUNT
            self.iconImageView.image = UIImage(named: AppConstants.Images.SOCIAL_LINK_PINTEREST_DIALOG_ICON)
            self.urlTextView.text = AppConstants.Strings.COPY_AND_PASTE_PINTEREST_URL
        default:
            break
        }
        urlTextView.font = UIFont.fontMedium(size: 16)
        urlTextView.textColor = UIColor.cADADAD
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.errorButton.isHidden = true
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == AppConstants.Strings.COPY_AND_PASTE_TIKTOK_URL {
            textView.text = nil
            textView.textColor = UIColor.cD3D3D3
            textView.font = UIFont.fontMedium(size: 16)
        }else if textView.text == AppConstants.Strings.COPY_AND_PASTE_PINTEREST_URL {
            textView.text = nil
            textView.textColor = UIColor.cD3D3D3
            textView.font = UIFont.fontMedium(size: 16)
        }else if textView.text == AppConstants.Strings.COPY_AND_PASTE_FB_URL {
            textView.text = nil
            textView.textColor = UIColor.cD3D3D3
            textView.font = UIFont.fontMedium(size: 16)
        }else if textView.text == AppConstants.Strings.COPY_AND_PASTE_YOUTUBE_URL {
            textView.text = nil
            textView.textColor = UIColor.cD3D3D3
            textView.font = UIFont.fontMedium(size: 16)
        }else if textView.text == AppConstants.Strings.COPY_AND_PASTE_INSTAGRAM_URL {
            textView.text = nil
            textView.textColor = UIColor.cD3D3D3
            textView.font = UIFont.fontMedium(size: 16)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            if TYPE == AppConstants.TYPE_ACCOUNT_TIKTOK{
                textView.text = AppConstants.Strings.COPY_AND_PASTE_TIKTOK_URL
            }else if TYPE == AppConstants.TYPE_ACCOUNT_FB{
                textView.text = AppConstants.Strings.COPY_AND_PASTE_FB_URL
            }else if TYPE == AppConstants.TYPE_ACCOUNT_YOUTUBE{
                textView.text = AppConstants.Strings.COPY_AND_PASTE_YOUTUBE_URL
            }else if TYPE == AppConstants.TYPE_ACCOUNT_INSTAGRAM{
                textView.text = AppConstants.Strings.COPY_AND_PASTE_INSTAGRAM_URL
            }else if TYPE == AppConstants.TYPE_ACCOUNT_PINTEREST{
                textView.text = AppConstants.Strings.COPY_AND_PASTE_PINTEREST_URL
            }
            textView.textColor = UIColor.cADADAD
            textView.font = UIFont.fontMedium(size: 16)
        }
    }
    
    @IBAction func onDismissClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onConnectClick(_ sender: UIButton) {
        let url = urlTextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if url == AppConstants.Strings.COPY_AND_PASTE_TIKTOK_URL || url == AppConstants.Strings.COPY_AND_PASTE_FB_URL || url == AppConstants.Strings.COPY_AND_PASTE_YOUTUBE_URL || url == AppConstants.Strings.COPY_AND_PASTE_INSTAGRAM_URL || url == AppConstants.Strings.COPY_AND_PASTE_PINTEREST_URL || url == ""{
            self.errorButton.isHidden = false
            //self.view.makeToast(AppConstants.Strings.LINK_MISSING, duration: 0.3, position: .top, title: nil, image: nil, style: .init(), completion: nil)
        }
        else{
            self.errorButton.isHidden = true
            self.urlDialogDelegate?.URLDialog(type:self.TYPE ?? 0,url: url)
        }
    }
    
    @IBAction func onCancelClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func errorButtonAction(_ sender: UIButton) {
        self.showErrorTooltip(errorMessage: AppConstants.Strings.LINK_MISSING, view: sender)
    }
    
    @objc func tapDetected() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func click() {
        
    }

}
