import UIKit

class DeleteImageDialogViewController: BaseViewController {

    @IBOutlet var innerView: UIView!
    @IBOutlet var outerView: UIView!
    @IBOutlet var textLabel: UILabel!
    @IBOutlet var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    
    var postImageCloseClickDelegate:PostImageCloseClickProtocol?
    var TYPE:Int?
    var sale:SalesDetail?
    var onRelistItemClickDelegate:onRelistItemClickProtocol?
    var listingTitle:String? = ""
    var confirmDeliveryDelegate:ConfirmDeliveryProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.innerView.layer.borderWidth = 0.3
        self.innerView.layer.borderColor = UIColor.white.cgColor
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
        onTypeChange()
    }
    
    func onTypeChange(){
        if TYPE == AppConstants.TYPE_RELIST_ITEM{
            firstButton.setTitle(AppConstants.Strings.YES_RELIST, for: .normal)
            setText(text:AppConstants.Strings.ARE_YOU_SURE_YOU_WANT_TO_RELIST_ITEM + (sale?.listingTitle!)! + AppConstants.COMMA_QUESTION_MARK,textColor:(sale?.listingTitle!)!)
        }else if TYPE == AppConstants.TYPE_CONFIRM_DELEVERY{
            let text = "It looks like the buyer has returned your listing. \n\n By clicking “YES, RETURN RECEIVED”, you agree that you received “" + self.listingTitle! + "\" in the same original condition or in the condition that the buyer described. The dispute will be resolved and buyer will be given a full refund. \n\n If not, please describe what you received by leaving a comment in the listing’s dispute details page."
            
            let textWhite:String = "YES, RETURN RECEIVED"
            self.setTextConfirmDelivery(text:text,textWhite: textWhite,textPink:self.listingTitle!)
            firstButton.setTitle(AppConstants.Strings.RETURN_RECEIVED, for: .normal)
            secondButton.setTitle(AppConstants.Strings.THERE_WAS_PROBLEM_WITH_RETURN, for: .normal)
            secondButton.titleLabel?.textAlignment = .center
        }else{
            textLabel.text = AppConstants.Strings.DELETE_IMAGE_TEXT
            firstButton.setTitle(AppConstants.Strings.DELETE_IMAGE_AND_TAGS, for: .normal)
        }
    }
    
    @IBAction func onDismissClick(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onCancelClick(_ sender: UIButton) {
        if TYPE == AppConstants.TYPE_CONFIRM_DELEVERY{
            self.confirmDeliveryDelegate?.confirmDelivery(type: AppConstants.TYPE_UPLOAD_IMAGES)
        }else{
            self.dismiss(animated: false, completion: nil)
        }
        //second button
    }
    
    @IBAction func onDeleteButtonClick(_ sender: UIButton) {
        if TYPE == AppConstants.TYPE_RELIST_ITEM{
            onRelistItemClickDelegate?.onRelistItemClick(sale: sale!)
        }else if TYPE == AppConstants.TYPE_CONFIRM_DELEVERY{
            self.confirmDeliveryDelegate?.confirmDelivery(type: AppConstants.TYPE_CONFIRM_DELEVERY_CLICK)
        }else{
            postImageCloseClickDelegate?.postImageCloseClick()
        }
        self.dismiss(animated: false, completion: nil)
        //first button
    }
    
    @objc func tapDetected() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func click() {
    }
    
    func setTextConfirmDelivery(text:String,textWhite:String,textPink:String){
        let text:String = text
        let textColorWhite:String = textWhite
        let textColorPink:String = textPink
        let rangeWhite = (text as NSString).range(of: textColorWhite)
        let rangePink = (text as NSString).range(of: textColorPink)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: rangeWhite)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: rangePink)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cEA489E, range: rangePink)
        textLabel.attributedText = attributedString
    }
    
    func setText(text:String,textColor:String){
        let text:String = text
        let textColor:String = textColor
        let range = (text as NSString).range(of: textColor)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cEA9DDC, range: range)
        textLabel.attributedText = attributedString
    }
    
    
}
