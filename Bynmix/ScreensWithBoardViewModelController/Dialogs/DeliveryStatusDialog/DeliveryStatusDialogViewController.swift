import UIKit

class DeliveryStatusDialogViewController: BaseViewController {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var trackingNumberLabel: UILabel!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var innerView: UIView!
    @IBOutlet var outerView: UIView!
    @IBOutlet var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var buyerSellerTextLabel: UILabel!
    
    var trackingInfoHandler = TrackingInfoHandler()
    var sale:SalesDetail?
    var purchase:PurchaseDetail?
    var TYPE:Int?
    var id:Int?
    let profileViewModel = ProfileViewModel()
    var shippingHistory:[ShippingHistory] = []
    var returnHistory:[ShippingHistory] = []
    var listingTitle:String?
    var canReturnInfoForReturnLabel:Bool?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = trackingInfoHandler
        tableView.dataSource = trackingInfoHandler
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 34
        
        trackingNumberLabel.font = UIFont.fontBold(size: 12)
        trackingNumberLabel.textColor = UIColor.cFF50A9
        
        self.innerView.layer.borderWidth = 0.3
        self.innerView.layer.borderColor = UIColor.white.cgColor
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
        if self.canReturnInfoForReturnLabel ?? false{
            self.buyerSellerTextLabel.text = AppConstants.Strings.RETURN_INFO_FOR
        }else{
//            if TYPE == AppConstants.TYPE_PURCHASE{
//                self.buyerSellerTextLabel.text = AppConstants.Strings.LEAVE_SELLER_FEEDBACK_FOR
//            }else{
//                self.buyerSellerTextLabel.text = AppConstants.Strings.LEAVE_BUYER_FEEDBACK_FOR
//            }
            self.buyerSellerTextLabel.text = AppConstants.Strings.SHIPPING_INFO_FOR
        }
        trackingInfo(id:self.id!,canReturnInfoForReturnLabel: self.canReturnInfoForReturnLabel ?? false, type: self.TYPE!)
    }
    
    func trackingInfo(id:Int,canReturnInfoForReturnLabel:Bool,type:Int) {
        self.view.makeToastActivity(.center)
        profileViewModel.getTrackInformation(accessToken: AppDefaults.getAccessToken(), id: id, canReturnInfoForReturnLabel: canReturnInfoForReturnLabel, type: type, { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Tracking> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    let trackingData = data.data
                    let trackingNumber = trackingData?.trackingNumber
                    
                    if self.canReturnInfoForReturnLabel ?? false{
                        
                        let returnHistory = trackingData?.returnHistory
                        self.returnHistory = returnHistory!
                        
                        if trackingNumber != nil{
                            self.trackingNumberLabel.text = trackingNumber
                        }else{
                            self.trackingNumberLabel.text = AppConstants.Strings.SELLER_NOT_SHIPPED_ITEM
                        }
                        
                        if (returnHistory?.count)! > 0{
    
                            self.trackingInfoHandler.hasShipping = true
                            self.trackingInfoHandler.shippingHistory = returnHistory!
                        }else{
                            
                            self.trackingInfoHandler.hasShipping = false
                            self.trackingInfoHandler.shippingNil = [AppConstants.Strings.NO_TRACKING_INFO_AVAILABLE]
                        }
                        
                    }else{
                        let shippingHistory = trackingData?.shippingHistory
                        self.shippingHistory = shippingHistory!
                        
                        if trackingNumber != nil{
                            self.trackingNumberLabel.text = trackingNumber
                        }else{
                            self.trackingNumberLabel.text = AppConstants.Strings.SELLER_NOT_SHIPPED_ITEM
                        }
                        
                        if (shippingHistory?.count)! > 0{
                            self.trackingInfoHandler.hasShipping = true
                            self.trackingInfoHandler.shippingHistory = shippingHistory!
                        }else{
                            self.trackingInfoHandler.hasShipping = false
                            self.trackingInfoHandler.shippingNil = [AppConstants.Strings.NO_TRACKING_INFO_AVAILABLE]
                        }
                    }
                    
                    self.tableView.reloadData()
                    self.setText(title:self.listingTitle!)
                    self.innerView.isHidden = false
                    self.view.hideToastActivity()
                    self.resizeDeliveryStatusTableView()
                }
            }else if response.response?.statusCode == 404{
                self.trackingNumberLabel.text = AppConstants.Strings.SELLER_NOT_SHIPPED_ITEM
                self.trackingInfoHandler.hasShipping = false
                self.trackingInfoHandler.shippingNil = [AppConstants.Strings.NO_TRACKING_INFO_AVAILABLE]
                self.tableView.reloadData()
                self.setText(title: self.listingTitle!)
                self.innerView.isHidden = false
                self.view.hideToastActivity()
                self.resizeDeliveryStatusTableView()
                
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func resizeDeliveryStatusTableView(){
        
        if self.canReturnInfoForReturnLabel ?? false{
            
            if self.returnHistory.count == 0{
                tableViewHeightConstraint.constant = 34
            }else{
                
                var tableViewHeight: CGFloat {
                    self.tableView.layoutIfNeeded()
                    return self.tableView.contentSize.height
                }
                tableViewHeightConstraint.constant = tableViewHeight
                print(tableViewHeight)
                //let height = yourDataArray.count * Int(popOverViewController.tableView.rowHeight)
                self.preferredContentSize = CGSize(width: self.view.frame.size.width, height: 251 + tableViewHeight)
            }
            
        }else{
            if self.shippingHistory.count == 0{
                tableViewHeightConstraint.constant = 34
            }else{
                
                var tableViewHeight: CGFloat {
                    self.tableView.layoutIfNeeded()
                    return self.tableView.contentSize.height
                }
                tableViewHeightConstraint.constant = tableViewHeight
                self.preferredContentSize = CGSize(width: self.view.frame.size.width, height: 251 + tableViewHeight)
            }
        }
        
    }
    
    func setText(title:String){
        let text:String = AppConstants.COMMA + title + AppConstants.COMMA
        let textColor:String =  title 
        let range = (text as NSString).range(of: textColor)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cFF50A9, range: range)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 20), range: range)
        titleLabel.attributedText = attributedString
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func tapDetected() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func click() {
    }
    
}
