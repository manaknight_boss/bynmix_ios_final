import UIKit

class DisputeDialogViewController: UIViewController {
    
    @IBOutlet var innerView: UIView!
    @IBOutlet var outerView: UIView!
    @IBOutlet var textLabel: UILabel!
    @IBOutlet var firstButton: UIButton!
    @IBOutlet var secondButton: UIButton!
    @IBOutlet weak var thirdButton: UIButton!
    
    var TYPE:Int?
    var disputeDetails:DisputeDetails?
    var escalateDisputeDelegate:EscalateDisputeProtocol?
    var TYPE_BUYER_OR_SELLER:Int?
    var refundBuyerWithoutReturnDelegate:RefundBuyerWithoutReturnProtocol?
    var sellerSideEscalateDisputeDelegate:SellerSideEscalateDisputeProtocol?
    var sellerSideBackToOptionsDelegate:SellerSideBackToOptionsProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstButton.titleLabel?.textAlignment = .center
        self.innerView.layer.borderWidth = 0.3
        self.innerView.layer.borderColor = UIColor.white.cgColor
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
        dialogType()
    }
    
    func dialogType(){
        if TYPE == AppConstants.TYPE_SELLER_SIDE_ESCLATE_DISPUTE{
            textLabel.text = AppConstants.Strings.ESCALATE_DISPUTE_SELLER_SIDE_TEXT
            UIView.performWithoutAnimation {
                firstButton.setTitle(AppConstants.Strings.YES_ESCALATE_DISPUTE, for: .normal)
                secondButton.setTitle(AppConstants.Strings.BACK_TO_OPTIONS, for: .normal)
                firstButton.layoutIfNeeded()
                secondButton.layoutIfNeeded()
            }
            firstButton.backgroundColor = UIColor.cBB189C
            secondButton.backgroundColor = UIColor.cDC4293
            secondButton.isHidden = false
            
        }else if TYPE == AppConstants.TYPE_BUYER_SIDE_ESCLATE_DISPUTE{
            UIView.performWithoutAnimation {
                firstButton.setTitle(AppConstants.Strings.YES_ESCALATE_DISPUTE, for: .normal)
                secondButton.setTitle(AppConstants.Strings.BACK_TO_OPTIONS, for: .normal)
                firstButton.layoutIfNeeded()
                secondButton.layoutIfNeeded()
            }
            textLabel.text = AppConstants.Strings.ESCALATE_DISPUTE_BUYER_SIDE_TEXT
            firstButton.backgroundColor = UIColor.cBB189C
            secondButton.backgroundColor = UIColor.cDC4293
            secondButton.isHidden = true
        }else if TYPE == AppConstants.TYPE_SELLER_SIDE_REFUND_BUYER_WITHOUT_RETURN{
            UIView.performWithoutAnimation {
                firstButton.setTitle(AppConstants.Strings.YES_REFUND_WITHOUT_PURCHASE_RETURN, for: .normal)
                secondButton.setTitle(AppConstants.Strings.BACK_TO_OPTIONS, for: .normal)
                firstButton.layoutIfNeeded()
                secondButton.layoutIfNeeded()
            }
            textLabel.text = AppConstants.Strings.REFUND_BUYER_WITHOUT_RETURN_SELLER_SIDE
            firstButton.backgroundColor = UIColor.cBB189C
            secondButton.backgroundColor = UIColor.cDC4293
            secondButton.isHidden = false
            
        }else if TYPE == AppConstants.TYPE_SELLER_SIDE_DISPUTE_OPTIONS_EXPANDED{
            UIView.performWithoutAnimation {
                firstButton.setTitle(AppConstants.Strings.REFUND_WITHOUT_PURCHASE_RETURN, for: .normal)
                firstButton.layoutIfNeeded()
            }
            textLabel.text = AppConstants.Strings.SELLER_SIDE_DISPUTE_OPTIONS_EXPANDED_TEXT
            if disputeDetails?.canRefundBuyer ?? false{
                self.enableFirstButton()
            }else{
                self.disableFirstButton()
            }
            
            if disputeDetails?.escalateRequested ?? false{
                UIView.performWithoutAnimation {
                    secondButton.setTitle(AppConstants.Strings.DISPUTE_ESCALATED, for: .normal)
                    secondButton.layoutIfNeeded()
                }
                secondButton.backgroundColor = UIColor.cCBCBCB
                secondButton.isEnabled = false
            }else{
                UIView.performWithoutAnimation {
                    secondButton.setTitle(AppConstants.Strings.ESCALATE_DISPUTE, for: .normal)
                    secondButton.layoutIfNeeded()
                }
                secondButton.backgroundColor = UIColor.cDC4293
                secondButton.isEnabled = true
            }
            secondButton.isHidden = false
        }else if TYPE == AppConstants.TYPE_SELLER_SIDE_DISPUTE_OPTIONS_EXPANDED_DISPUTE_ESCLATED{
            
            UIView.performWithoutAnimation {
                firstButton.setTitle(AppConstants.Strings.REFUND_WITHOUT_PURCHASE_RETURN, for: .normal)
                secondButton.setTitle(AppConstants.Strings.DISPUTE_ESCALATED, for: .normal)
                firstButton.layoutIfNeeded()
                secondButton.layoutIfNeeded()
            }
            textLabel.text = AppConstants.Strings.SELLER_SIDE_DISPUTE_OPTIONS_EXPANDED_TEXT
            self.enableFirstButton()
            secondButton.backgroundColor = UIColor.cCBCBCB
            secondButton.isHidden = false
            checkCanEscalateDispute()
        }
        
    }
    
    func enableFirstButton(){
        firstButton.backgroundColor = UIColor.cBB189C
        firstButton.isEnabled = true
    }
    
    func disableFirstButton(){
        firstButton.backgroundColor = UIColor.cCBCBCB
        firstButton.isEnabled = false
    }
    
    func checkCanEscalateDispute(){
        if disputeDetails?.canEscalateDispute ?? false{
            secondButton.isHidden = false
            secondButton.backgroundColor = UIColor.cDC4293
            secondButton.isEnabled = true
        }else{
            secondButton.isHidden = false
            secondButton.backgroundColor = UIColor.cCBCBCB
            secondButton.isEnabled = false
        }
    }
    
    @IBAction func closeButtonAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func firstButton(_ sender: UIButton) {
        if TYPE_BUYER_OR_SELLER == AppConstants.TYPE_PURCHASE{
            self.escalateDisputeDelegate?.escalateDispute(type:self.TYPE!,disputeDetails: disputeDetails!)
        }else{
            if self.TYPE == AppConstants.TYPE_SELLER_SIDE_DISPUTE_OPTIONS_EXPANDED{
                self.refundBuyerWithoutReturnDelegate?.refundBuyerWithoutReturn(type:self.TYPE!,disputeDetails: disputeDetails!)
            }else{
                self.escalateDisputeDelegate?.escalateDispute(type:self.TYPE!,disputeDetails: disputeDetails!)
            }
        }
    }
    
    @IBAction func secondButton(_ sender: UIButton) {
        if TYPE == AppConstants.TYPE_SELLER_SIDE_DISPUTE_OPTIONS_EXPANDED{
            self.sellerSideEscalateDisputeDelegate?.sellerSideEscalateDispute(type:self.TYPE!,disputeDetails:disputeDetails!)
        }else{
            self.sellerSideBackToOptionsDelegate?.sellerSideBackToOptions(type: self.TYPE!, disputeDetails: disputeDetails!)
        }
    }
    
    @IBAction func thirdButton(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func tapDetected() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func click() {
    }
    
    
}
