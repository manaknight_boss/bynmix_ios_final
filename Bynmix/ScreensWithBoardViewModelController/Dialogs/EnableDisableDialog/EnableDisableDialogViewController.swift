import UIKit
import Foundation

class EnableDisableDialogViewController: BaseViewController {
    
    @IBOutlet var innerView: UIView!
    @IBOutlet var outerView: UIView!
    @IBOutlet var textLabel: UILabel!
    @IBOutlet var firstButton: UIButton!
    @IBOutlet var secondButton: UIButton!
    @IBOutlet weak var thirdButton: UIButton!
    
    var type:Int?
    var onDisableClickDelegate: OnDisableClickProtocol?
    var onDeleteClickDelegate:OnDeleteClickProtocol?
    var feed: Feed?
    var address: Address?
    var card: Card?
    var onCloseClickAddressOrPaymentDelegate: OnCloseClickAddressOrPaymentProtocol?
    var addNewAddressDelegate:AddNewAddressProtocol?
    var setDefaultPaymentDelegate: SetDefaultPaymentProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.innerView.layer.borderWidth = 0.3
        self.innerView.layer.borderColor = UIColor.white.cgColor
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
        dialogType()
    }
    
    func dialogType(){
         if type == 2{
            firstButton.isHidden = true
            secondButton.setTitle(AppConstants.Strings.BUTTON_TITLE_DISABLE, for: .normal)
            secondButton.backgroundColor = UIColor.cBB189C
            disableSetText()
        }else if type == 3{
            if feed?.isActive == true{
                secondButton.isHidden = false
            }else{
                secondButton.isHidden = true
            }
            firstButton.isHidden = false
            firstButton.setTitle(AppConstants.Strings.BUTTON_TITLE_DELETE, for: .normal)
            secondButton.backgroundColor = UIColor.cDC4293
            secondButton.setTitle(AppConstants.Strings.BUTTON_TITLE_DISABLE, for: .normal)
            deleteSetText()
        }else if type == 4{
            firstButton.isHidden = true
            secondButton.backgroundColor = UIColor.cBB189C
            secondButton.setTitle(AppConstants.Strings.RE_ENABLE_LISTING, for: .normal)
            reEnableSetText()
        }else if type == 5{
            firstButton.isHidden = true
            secondButton.setTitle(AppConstants.Strings.DISABLE_FOR_NOW, for: .normal)
            secondButton.backgroundColor = UIColor.cBB189C
            disablePostSetText()
        }else if type == 6{
            if feed?.isActive == true{
                secondButton.isHidden = false
            }else{
                secondButton.isHidden = true
            }
            firstButton.isHidden = false
            firstButton.setTitle(AppConstants.Strings.BUTTON_TITLE_DELETE, for: .normal)
            secondButton.backgroundColor = UIColor.cDC4293
            secondButton.setTitle(AppConstants.Strings.BUTTON_TITLE_DISABLE_POST, for: .normal)
            deletePostSetText()
        }else if type == 7{
            firstButton.isHidden = true
            secondButton.backgroundColor = UIColor.cBB189C
            secondButton.setTitle(AppConstants.Strings.RE_ENABLE_POST, for: .normal)
            reEnablePostSetText()
         }else if type == AppConstants.TYPE_CANNOT_MAKE_OFFER{
            if self.card == nil{
                firstButton.isHidden = false
            }else{
                firstButton.isHidden = true
            }
            if self.address == nil{
                secondButton.isHidden = false
            }else{
                secondButton.isHidden = true
            }
            
            if self.address == nil && self.card == nil{
                self.textLabel.text = AppConstants.Strings.NO_CARD_OR_DEFAULT_PAYMENT
            }else if self.address == nil{
                self.textLabel.text = AppConstants.Strings.NO_ADDRESS
            }else{
                self.textLabel.text = AppConstants.Strings.NO_CARD
            }
            firstButton.setTitle(AppConstants.Strings.ADD_PAYMENT_BUTTON_TITLE, for: .normal)
            secondButton.setTitle(AppConstants.Strings.ADD_ADDRESS_BUTTON_TITLE, for: .normal)
            thirdButton.setTitle(AppConstants.CANCEL, for: .normal)
            secondButton.backgroundColor = UIColor.cDC4293
            firstButton.backgroundColor = UIColor.cBB189C
        }
        
    }
    
    func disableSetText(){
        let text:String = AppConstants.Strings.DISABLE_TEXT + "\(feed?.title ?? "")" + AppConstants.Strings.DISABLE_LIST_TEXT
        setText(text:text)
    }
    
    func disablePostSetText(){
        let text:String = AppConstants.Strings.DISABLE_TEXT + "\(feed?.title ?? "")" + AppConstants.Strings.DISABLE_POST_TEXT
       setTextForDisablePost(text:text)
    }
    
    func deleteSetText(){
        if feed?.isActive == true{
            let text:String = AppConstants.Strings.DELETE_TEXT + "\(feed?.title ?? "")" + AppConstants.Strings.DELETE_LIST_TEXT
            setText(text:text)
        }else{
            let text:String = AppConstants.Strings.DELETE_TEXT + "\(feed?.title ?? "")" + AppConstants.QUESTION_MARK
            setText(text:text)
        }
        
    }
    
    func deletePostSetText(){
        if feed?.isActive == true{
            let text:String = AppConstants.Strings.DELETE_TEXT + "\(feed?.title ?? "")" + AppConstants.Strings.DELETE_POST_TEXT

            setTextForDeletePost(text:text)
        }else{
            let text:String = AppConstants.Strings.POST_DETAIL_FOR + "\(feed?.title ?? "")" + AppConstants.Strings.RE_ENABLE_POST_TEXT

            setTextForDeletePost(text:text)
        }
        
    }
    
    func reEnablePostSetText(){
        let text:String = AppConstants.Strings.POST_DETAIL_FOR + "\(feed?.title ?? "")" + AppConstants.Strings.RE_ENABLE_POST_TEXT

        setText(text:text)
    }
    
    func reEnableSetText(){
        let text:String = AppConstants.Strings.LISTING_FOR_TEXT + "\(feed?.title ?? "")" + AppConstants.Strings.RE_ENABLE_TEXT
        
        setText(text:text)
    }
    
    func setTextForDeletePost(text:String){
        let textRegular:String = "\(feed?.title ?? "")"
        let range = (text as NSString).range(of: textRegular)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cFF70E7, range: range)
        textLabel.attributedText = attributedString
    }
    
    func setTextForDisablePost(text:String){
        let textRegular:String = "\(feed?.title ?? "")"
        let range = (text as NSString).range(of: textRegular)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cFF8BEA, range: range)
        textLabel.attributedText = attributedString
    }
    
    func setText(text:String){
        let textRegular:String = "\(feed?.title ?? "")"
        let range = (text as NSString).range(of: textRegular)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cFF6DE3, range: range)
        textLabel.attributedText = attributedString
    }
    
    @IBAction func onDismissClick(_ sender: UIButton) {
        if type == AppConstants.TYPE_CANNOT_MAKE_OFFER{
            onCloseClickAddressOrPaymentDelegate?.onCloseClickAddressOrPayment()
        }else{
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func onDisableClick(_ sender: UIButton) {
        if type == AppConstants.TYPE_CANNOT_MAKE_OFFER{
            // add address action
            self.addNewAddressDelegate?.addNewAddress(type: AppConstants.TYPE_CANNOT_MAKE_OFFER)
        }else{
            if feed?.isActive == true{
                feed?.isActive = false
            }else{
                feed?.isActive = true
            }
            onDisableClickDelegate!.onDisableClick(feed: feed!)
            self.dismiss(animated: false, completion: nil)
        }
        
    }
    
    @IBAction func onCancelClick(_ sender: UIButton) {
        if type == AppConstants.TYPE_CANNOT_MAKE_OFFER{
            onCloseClickAddressOrPaymentDelegate?.onCloseClickAddressOrPayment()
        }else{
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @IBAction func onDeleteButtonClick(_ sender: UIButton) {
        if type == AppConstants.TYPE_CANNOT_MAKE_OFFER{
            // add payment action
            self.setDefaultPaymentDelegate?.setDefaultPayment(type: AppConstants.TYPE_CANNOT_MAKE_OFFER)
        }else{
            onDeleteClickDelegate?.onDeleteClick(id: (feed?.id)!)
            self.dismiss(animated: false, completion: nil)
        }
        
    }
    
    @objc func tapDetected() {
        if type == AppConstants.TYPE_CANNOT_MAKE_OFFER{
            // no dismiss
        }else{
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @objc func click() {
        
    }
    
    
}
