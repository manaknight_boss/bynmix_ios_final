import UIKit

class FailesMessageViewController: BaseViewController {
    
    var message:String?
    @IBOutlet var messageLabel: UILabel!
    
    @IBOutlet var outerView: UIView!
    @IBOutlet weak var innerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        messageLabel.text = message!
        self.innerView.layer.borderWidth = 0.3
        self.innerView.layer.borderColor = UIColor.white.cgColor
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
    }
    
    @IBAction func crossButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func tapDetected() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func click() {
        
    }


}
