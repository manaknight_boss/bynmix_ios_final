import UIKit

class FeedbackDialogViewController: BaseViewController {
    
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var feedbackTextView: UITextView!
    @IBOutlet var ratingOneButton: UIButton!
    @IBOutlet var ratingTwoButton: UIButton!
    @IBOutlet var ratingThreeButton: UIButton!
    @IBOutlet var ratingFourButton: UIButton!
    @IBOutlet var ratingFiveButton: UIButton!
    @IBOutlet var innerView: UIView!
    @IBOutlet var outerView: UIView!
    @IBOutlet var buyerSellerTextLabel: UILabel!
    
    var TYPE:Int?
    var sale:SalesDetail?
    var purchase:PurchaseDetail?
    var rating:Int = 0
    var comment = ""
    var onFeedbackSubmitClickDelegate:OnFeedbackSubmitClickProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.innerView.layer.borderWidth = 0.3
        self.innerView.layer.borderColor = UIColor.white.cgColor
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
        var title:String = ""
        if TYPE == AppConstants.TYPE_PURCHASE{
            title = (purchase?.listingTitle!)!
            setText(title: title)
            self.buyerSellerTextLabel.text = AppConstants.Strings.LEAVE_SELLER_FEEDBACK_FOR
        }else{
            title = (sale?.listingTitle!)!
            setText(title: title)
            self.buyerSellerTextLabel.text = AppConstants.Strings.LEAVE_BUYER_FEEDBACK_FOR
        }
        
        feedbackTextView.text = AppConstants.Strings.ENTER_ANY_FEEDBACK_COMMENTS_HERE
        feedbackTextView.textColor = UIColor.cADADAD
        feedbackTextView.font = UIFont.fontRegular(size: 18)
    }
    
    func setText(title:String){
        let text:String = AppConstants.COMMA + title + AppConstants.COMMA
        let textColor:String = AppConstants.COMMA + title + AppConstants.COMMA
        let range = (text as NSString).range(of: textColor)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 20), range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cBB189C, range: range)
        descriptionLabel.attributedText = attributedString
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if feedbackTextView.text == AppConstants.Strings.ENTER_ANY_FEEDBACK_COMMENTS_HERE {
            feedbackTextView.text = nil
            feedbackTextView.textColor = UIColor.cD3D3D3
            feedbackTextView.font = UIFont.fontRegular(size: 18)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if feedbackTextView.text.isEmpty {
            feedbackTextView.text = AppConstants.Strings.ENTER_ANY_FEEDBACK_COMMENTS_HERE
            feedbackTextView.textColor = UIColor.cADADAD
            feedbackTextView.font = UIFont.fontRegular(size: 18)
        }
    }
    
    @IBAction func ratingOneButton(_ sender: UIButton) {
        if ratingOneButton.hasImage(named: AppConstants.Images.FEEDBACK_HEART, for: .normal){
            ratingOneButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART_SELECTED), for: .normal)
            self.rating = 1
        }else{
            ratingTwoButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART), for: .normal)
            ratingThreeButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART), for: .normal)
            ratingFourButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART), for: .normal)
            ratingFiveButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART), for: .normal)
        }
        
    }
    
    @IBAction func ratingTwoButton(_ sender: UIButton) {
        if ratingTwoButton.hasImage(named: AppConstants.Images.FEEDBACK_HEART, for: .normal){
            ratingOneButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART_SELECTED), for: .normal)
            ratingTwoButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART_SELECTED), for: .normal)
            self.rating = 2
        }else{
            ratingThreeButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART), for: .normal)
            ratingFourButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART), for: .normal)
            ratingFiveButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART), for: .normal)
        }
    }
    
    @IBAction func ratingThreeButton(_ sender: UIButton) {
        if ratingThreeButton.hasImage(named: AppConstants.Images.FEEDBACK_HEART, for: .normal){
            ratingOneButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART_SELECTED), for: .normal)
            ratingTwoButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART_SELECTED), for: .normal)
            ratingThreeButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART_SELECTED), for: .normal)
            self.rating = 3
        }else{
            ratingFourButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART), for: .normal)
            ratingFiveButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART), for: .normal)
        }
    }
    
    @IBAction func ratingFourButton(_ sender: UIButton) {
        if ratingFourButton.hasImage(named: AppConstants.Images.FEEDBACK_HEART, for: .normal){
            ratingOneButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART_SELECTED), for: .normal)
            ratingTwoButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART_SELECTED), for: .normal)
            ratingThreeButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART_SELECTED), for: .normal)
            ratingFourButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART_SELECTED), for: .normal)
            self.rating = 4
        }else{
            ratingFiveButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART), for: .normal)
        }
    }
    
    @IBAction func ratingFiveButton(_ sender: UIButton) {
        if ratingFiveButton.hasImage(named: AppConstants.Images.FEEDBACK_HEART, for: .normal){
            ratingOneButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART_SELECTED), for: .normal)
            ratingTwoButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART_SELECTED), for: .normal)
            ratingThreeButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART_SELECTED), for: .normal)
            ratingFourButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART_SELECTED), for: .normal)
            ratingFiveButton.setImage(UIImage(named: AppConstants.Images.FEEDBACK_HEART_SELECTED), for: .normal)
            self.rating = 5
        }
    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        var id:Int?
        if TYPE == AppConstants.TYPE_PURCHASE{
            id = (purchase?.purchaseId)!
        }else{
            id = (sale?.saleId)!
        }
        comment = self.feedbackTextView.text
        onFeedbackSubmitClickDelegate?.onFeedbackSubmitClick(type:TYPE! ,id:id!, rating: self.rating, comment: self.comment)
    }
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func tapDetected() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func click() {
    }
    
    @IBAction func crossButton(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
}
