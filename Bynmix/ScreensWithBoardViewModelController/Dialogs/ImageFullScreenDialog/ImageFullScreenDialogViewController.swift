import UIKit

class ImageFullScreenDialogViewController: UIViewController {
    @IBOutlet var outerView: UIView!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet var innerView: UIView!
    
    var itemImage:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
        
        let image = UIImage(named: AppConstants.Images.DEFAULT_OFFER_PLACEHOLDER)
        self.itemImageView.kf.setImage(with: URL(string: self.itemImage!)!,placeholder: image)
    }
    
    @objc func tapDetected() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func click() {
        
    }
    
}
