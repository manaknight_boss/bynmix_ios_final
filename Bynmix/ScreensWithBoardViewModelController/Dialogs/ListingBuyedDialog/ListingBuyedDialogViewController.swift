import UIKit

class ListingBuyedDialogViewController: UIViewController {
    
    @IBOutlet var innerView: UIView!
    @IBOutlet var outerView: UIView!
    @IBOutlet var messageLabel: UILabel!
    @IBOutlet weak var greetLabel: UILabel!
    
    var accountInfo:AccountInfo?
    var itemTitle:String?
    var TYPE:Int?
    
    var username:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
        
        if self.TYPE == AppConstants.TYPE_BUY_NOW{
            let usernameTwo = self.username! + " "
            let text:String = AppConstants.Strings.OFFER_SUCCESSFULLY_SENT_TO + self.username! + "." + "\n" +  AppConstants.Strings.HOLD_TIGHT_TO_SEE_WHAT + usernameTwo + AppConstants.Strings.THINKS
            let textPink:String = self.username!
            let textPinkTwo :String  = usernameTwo
            let range = (text as NSString).range(of: textPink)
            let rangeTwo = (text as NSString).range(of: textPinkTwo)
            let attributedString = NSMutableAttributedString(string: text)
        
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cEA9DDC, range: range)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cEA9DDC, range: rangeTwo)
            messageLabel.attributedText = attributedString
            
        }else{
            let text:String = AppConstants.Strings.YOU_SUCCESSFULLY_PURCHASED + "\n" +  self.itemTitle!
            let textPink:String = self.itemTitle!
            let range = (text as NSString).range(of: textPink)
            let attributedString = NSMutableAttributedString(string: text)
            
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cEA9DDC, range: range)
            messageLabel.attributedText = attributedString
        }
        self.greetLabel.text = AppConstants.CONGRATULATIONS
        
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func tapDetected() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func click() {
        
    }
    
    
}
