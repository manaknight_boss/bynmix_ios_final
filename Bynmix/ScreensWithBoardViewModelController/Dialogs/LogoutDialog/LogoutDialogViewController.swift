import UIKit

class LogoutDialogViewController: BaseViewController {
    
    @IBOutlet var innerView: UIView!
    @IBOutlet var outerView: UIView!
    @IBOutlet var textLabel: UILabel!
    @IBOutlet var logoutButton: UIButton!
    
    var logoutDelegate:LogoutProtocol?
    var TYPE:Int?
    var TYPE_ACCOUNT:Int?
    var accountUnlinkDelegate:AccountUnlinkProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.innerView.layer.borderWidth = 0.3
        self.innerView.layer.borderColor = UIColor.white.cgColor
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
        if self.TYPE == AppConstants.TYPE_UNLINK{
            self.setTextUnLink()
        }else{
            self.setText()
        }
    }
    
    func setText(){
        logoutButton.setTitle("LOGOUT", for: .normal)
        let text:String = "Are you sure you want to logout?"
        let textRegular:String = "Are you sure you want to"
        let textColorWhite = "?"
        let textPink:String = "logout"
        let range = (text as NSString).range(of: textRegular)
        let rangeWhite = (text as NSString).range(of: textColorWhite)
        let rangePink = (text as NSString).range(of: textPink)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 16), range: range)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 16), range: rangeWhite)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: rangeWhite)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: rangePink)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cFF6DE3, range: rangePink)
        textLabel.attributedText = attributedString
        
    }
    
    func setTextUnLink(){
        logoutButton.setTitle("UNLINK", for: .normal)
        var account = ""
        switch self.TYPE_ACCOUNT {
        case AppConstants.TYPE_ACCOUNT_FB:
            account = "Facebook"
            break
        case AppConstants.TYPE_ACCOUNT_TIKTOK:
            account = "TikTok"
            break
        case AppConstants.TYPE_ACCOUNT_INSTAGRAM:
            account = "Instagram"
            break
        case AppConstants.TYPE_ACCOUNT_YOUTUBE:
            account = "Youtube"
            break
        case AppConstants.TYPE_ACCOUNT_PINTEREST:
            account = "Pinterest"
            break
        default:
            break
        }
        
        let text:String = "Are you sure you want to unlink your \(account)?"
        let textRegular:String = "Are you sure you want to"
        let textColorWhite = "?"
        let textPink:String = "unlink your \(account)"
        let range = (text as NSString).range(of: textRegular)
        let rangeWhite = (text as NSString).range(of: textColorWhite)
        let rangePink = (text as NSString).range(of: textPink)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 16), range: range)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 16), range: rangeWhite)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: rangeWhite)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: rangePink)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cFF6DE3, range: rangePink)
        textLabel.attributedText = attributedString
        
        self.view.layoutIfNeeded()
        
    }
    
    @IBAction func onDismissClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onLogoutClick(_ sender: UIButton) {
        logoutDelegate?.logout()
        accountUnlinkDelegate?.accountUnlink(type: self.TYPE_ACCOUNT ?? 0)
    }
    
    @IBAction func onCancelClick(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func tapDetected() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func click() {
        
    }
    
}
