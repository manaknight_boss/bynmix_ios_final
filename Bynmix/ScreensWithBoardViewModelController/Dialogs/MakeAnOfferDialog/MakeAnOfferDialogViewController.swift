import UIKit

class MakeAnOfferDialogViewController: BaseViewController {
    
    @IBOutlet var innerView: UIView!
    @IBOutlet var outerView: UIView!
    @IBOutlet weak var positiveButton: UIButton!
    @IBOutlet weak var negativeButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var listingTitleLabel: UILabel!
    @IBOutlet weak var currentPriceLabel: UILabel!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var errorButton: UIButton!
    @IBOutlet var priceTextLabel: UILabel!
    
    var listingTitle:String?
    var listingDetail:ListingDetail?
    var makeAnOfferDelegate: MakeAnOfferProtocol?
    
    var TYPE:Int?
    var subOfferHistory:[SubOfferHistory]?
    var isBuyer:Bool? = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.innerView.layer.borderWidth = 0.3
        self.innerView.layer.borderColor = UIColor.white.cgColor
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
        setText()
        onTypeChange()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard let oldText = textField.text, let r = Range(range, in: oldText) else {
            return true
        }
        
        let newText = oldText.replacingCharacters(in: r, with: string)
        let isNumeric = newText.isEmpty || (Double(newText) != nil)
        let numberOfDots = newText.components(separatedBy: ".").count - 1
        
        let numberOfDecimalDigits: Int
        if let dotIndex = newText.firstIndex(of: ".") {
            numberOfDecimalDigits = newText.distance(from: dotIndex, to: newText.endIndex) - 1
        } else {
            numberOfDecimalDigits = 0
        }
        
        return isNumeric && numberOfDots <= 1 && numberOfDecimalDigits <= 2
        
    }
    
    func onTypeChange(){
        let currentPrice:String = String(listingDetail!.price!)
        var lastOfferPrice = 0
        
        if (self.subOfferHistory?.count ?? 0) > 0{
            let lastOffer = self.subOfferHistory?.last
            
            if lastOffer?.isActiveOffer ?? false{
                if isBuyer ?? false{
                    if(lastOffer?.isBuyerOffer ?? false){
                        self.makeANewOffer()
                    }else {
                        self.counterOffer()
                    }
                }else{
                    if(lastOffer?.isSellerCounterOffer ?? false){
                        self.makeANewOffer()
                    }else {
                        self.counterOffer()
                    }
                }
            }else{
                self.makeANewOffer()
            }
            
            lastOfferPrice = Int(lastOffer?.offerPrice ?? 0)
            self.currentPriceLabel.text = AppConstants.DOLLAR + String(lastOfferPrice)
            self.priceTextLabel.text = "Last Offer:"
        }else{
            self.titleLabel.text = AppConstants.Strings.MAKE_AN_OFFER_FOR
            self.priceTextLabel.text = "Current Price:"
            self.currentPriceLabel.text = AppConstants.DOLLAR + currentPrice
        }
        
    }
    
    func makeANewOffer(){
        self.titleLabel.text = AppConstants.Strings.MAKE_A_NEW_OFFER_FOR
    }
    
    func counterOffer(){
        self.titleLabel.text = AppConstants.Strings.COUNTER_OFFER_FOR
    }
    
    func setText(){
        let text:String = "\"" + listingTitle! + "\""
        let textPink:String = listingTitle!
        let rangePink = (text as NSString).range(of: textPink)
        let attributedString = NSMutableAttributedString(string: text)
        
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cFF6DE3, range: rangePink)
        listingTitleLabel.attributedText = attributedString
    }
    
    @IBAction func okButtonAction(_ sender: UIButton) {
        let price:String = (priceTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? "")
        var isError = false
        if price == "" {
            errorButton.isHidden = false
            isError = true
            return
        }
        let priceDouble:Int = Int(price)!
        let currentPrice:Int = (listingDetail?.price!)!
        let lowestAllowableOffer = listingDetail?.lowestAllowableOffer ?? 0
    
        if priceDouble <= 10 || priceDouble <= lowestAllowableOffer || priceDouble >= currentPrice{
            errorButton.isHidden = false
            isError = true
        } else {
            errorButton.isHidden = true
        }
        
        hideToolTip()
        
        if isError {
            return
        }
        
        self.makeAnOfferDelegate?.makeAnOffer(listingDetail:self.listingDetail!,type:self.TYPE!, offeredPrice: priceDouble)
        
    }
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func tapDetected() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func click() {
        
    }
    
    @IBAction func errorButtonAction(_ sender: UIButton) {
        let price:String = priceTextField.text ?? ""
        let priceDouble:Int = Int(priceTextField.text ?? "") ?? 0
        let currentPrice:Int = (listingDetail?.price!)!
        let lowestAllowableOffer = listingDetail?.lowestAllowableOffer ?? 0
        
        if price != "" && priceDouble >= 10 && priceDouble <= currentPrice && priceDouble > lowestAllowableOffer{
            // make an offer click
        }else if price == ""{
            showErrorTooltip(errorMessage: AppConstants.Strings.ADD_OFFER_FIRST, view: sender)
        }else if priceDouble < 10  {
            showErrorTooltip(errorMessage: AppConstants.Strings.PRICE_LESS_THAN_TEN_DOLLAR, view: sender)
        }else if priceDouble > currentPrice {
            showErrorTooltip(errorMessage: AppConstants.Strings.MAKE_OFFER_LOWER_THAN_CURRENT_PRICE, view: sender)
        }else{
            showErrorTooltip(errorMessage: AppConstants.Strings.OFFER_TOO_LOW, view: sender)
        }

    }
    
    @IBAction func closeButtonAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
}
