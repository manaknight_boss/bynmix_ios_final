import UIKit

class MenuPopUpViewController: BaseViewController,PrintShippingLabelProtocol,onRelistItemClickProtocol,OnFeedbackSubmitClickProtocol,PrintReturnLabelProtocol {
    
    func printReturnLabel(type: Int, id: Int) {
        self.removePopUp()
        if type == AppConstants.TYPE_PRINT_RETURN_LABEL{
            self.emailReturnLabel(id:id)
        }else if type == AppConstants.TYPE_RETURN_LABEL_DONE{
            self.dismiss(animated: false , completion: nil)
        }
    }
    
    func onFeedbackSubmitClick(type: Int, id: Int, rating: Int, comment: String) {
        if type == AppConstants.TYPE_PURCHASE{
            purchaseFeedback(id:id,rating:rating,comment:comment)
        }else{
            sellerFeedback(id:id,rating:rating,comment:comment)
        }
    }
    
    func onRelistItemClick(sale: SalesDetail) {
        relistItem(sale:sale)
    }
    
    func PrintShippingLabel(type: Int, id: Int) {
        
        if type == AppConstants.TYPE_PRINT_SHIPPING_LABEL{
            self.emailShippingLabel(id:id)
        }else{
            self.dismiss(animated: false , completion: nil)
        }
    }
    
    @IBOutlet var outerView: UIView!
    @IBOutlet var innerView: UIView!
    
    @IBOutlet var cancelOrderButton: UIButton!
    @IBOutlet var leaveFeedBackButton: UIButton!
    @IBOutlet var ratingView: UIView!
    @IBOutlet var withoutSubHeadingView: UIView!
    @IBOutlet var withoutSubHeadingInnerView:UIView!
    @IBOutlet var itemStatusImageView: UIImageView!
    @IBOutlet var itemStatusLabel: UILabel!
    @IBOutlet var withSubStatusView: UIView!
    @IBOutlet var withSubStatusItemStatusLabel: UILabel!
    @IBOutlet var withSubStatusItemSubStatusLabel: UILabel!
    @IBOutlet var reportAnIssueButton: UIButton!
    @IBOutlet var disputeDetailButton: UIButton!
    @IBOutlet var relistItemButton: UIButton!
    @IBOutlet var buttonOneView: UIView!
    @IBOutlet var starOneImageView: UIImageView!
    @IBOutlet var starTwoImageView: UIImageView!
    @IBOutlet var starThreeImageView: UIImageView!
    @IBOutlet var starFourImageView: UIImageView!
    @IBOutlet var starFiveImageView: UIImageView!
    @IBOutlet var buttonTwoView: UIView!
    @IBOutlet var returnDeliveredImageView: UIImageView!
    
    @IBOutlet weak var confirmDeliveryView: UIView!
    @IBOutlet weak var confirmDeliveryImageView: UIImageView!
    @IBOutlet weak var confirmDeliveryStatusLabel: UILabel!
    @IBOutlet weak var confirmDeliverySubStatusLabel: UILabel!

    var TYPE:Int?
    var sale:SalesDetail?
    var purchase:PurchaseDetail?
    var profileViewModel = ProfileViewModel()
    var top: CGFloat = 0
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    var cancelOrderPurchasePopUpDelegate:CancelOrderPurchasePopUpProtocol?
    var cancelOrderSalePopUpDelegate:CancelOrderSalePopUpProtocol?
    var disputeDetailSaleMenuPopUpDelegate:DisputeDetailSaleMenuPopUpProtocol?
    var disputeDetailPurchaseMenuPopUpDelegate:DisputeDetailPurchaseMenuPopUpProtocol?
    var reportAnIssueSalePopUpDelegate:ReportAnIssueSalePopUpProtocol?
    var reportAnIssuePurchasePopUpDelegate:ReportAnIssuePurchasePopUpProtocol?
    var returnDeliveredDelegate:ReturnDeliveredProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        onTypeChange(type:self.TYPE!)
        let height = self.view.frame.height
        if (top + 350) > height {
            topConstraint.constant = top - 120
        } else {
            topConstraint.constant = top + 20
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.willMove(toParent: nil)
        self.view.removeFromSuperview()
        self.removeFromParent()
    }
    
    func onTypeChange(type:Int){
        var statusText:String = ""
        var subStatusText:String = ""
        
        self.confirmDeliveryView.isHidden = true
        
        if type == AppConstants.TYPE_PURCHASE{
            
            relistItemButton.isHidden = true
            statusText = purchase!.purchaseStatus!.uppercased()
            subStatusText = purchase!.purchaseSubStatus ?? "".uppercased()
            
            if (purchase!.canOpenDispute! || purchase!.hasOpenDispute! || purchase!.hasViewableDispute!){
                reportAnIssueButton.backgroundColor = UIColor.black
                reportAnIssueButton.isEnabled = true
            }else{
                reportAnIssueButton.backgroundColor = UIColor.cCBCBCB
                reportAnIssueButton.isEnabled = false
            }
            if purchase!.canLeaveFeedback!{
                leaveFeedBackButton.backgroundColor = UIColor.cBB189C
                leaveFeedBackButton.isEnabled = true
            }else{
                leaveFeedBackButton.backgroundColor = UIColor.cCBCBCB
                leaveFeedBackButton.isEnabled = false
            }
            
        }else{
            
            statusText = sale!.saleStatus!.uppercased()
            subStatusText = sale!.saleSubStatus ?? "".uppercased()
            
            if (sale!.canOpenDispute! || sale!.hasOpenDispute! || sale!.hasViewableDispute!){
                reportAnIssueButton.backgroundColor = UIColor.black
                reportAnIssueButton.isEnabled = true
            }else{
                reportAnIssueButton.backgroundColor = UIColor.cCBCBCB
                reportAnIssueButton.isEnabled = false
            }
            
            if sale!.canLeaveFeedback!{
                leaveFeedBackButton.backgroundColor = UIColor.cBB189C
                leaveFeedBackButton.isEnabled = true
            }else{
                leaveFeedBackButton.backgroundColor = UIColor.cCBCBCB
                leaveFeedBackButton.isEnabled = false
            }
            
        }
        
        if type == AppConstants.TYPE_PURCHASE{
            
            if (purchase!.hasSubStatus!) {
                self.withoutSubHeadingView.isHidden = true
                self.withSubStatusView.isHidden = false
                self.returnDeliveredImageView.isHidden = true
                self.withSubStatusItemStatusLabel.text = statusText
                self.withSubStatusItemSubStatusLabel.text = subStatusText
                self.withSubStatusView.backgroundColor = UIColor.white
            } else {
                self.itemStatusLabel.text = statusText
                self.withSubStatusView.isHidden = true
                self.withoutSubHeadingView.isHidden = false
                self.withoutSubHeadingView.backgroundColor = UIColor.cDC4293
                self.withoutSubHeadingInnerView.backgroundColor = UIColor.cDC4293
            }
            
            if (purchase!.hasOpenDispute!) {
                self.disputeDetailButton.isHidden = false
                self.reportAnIssueButton.isHidden = true
            } else {
                if(purchase!.hasViewableDispute!) {
                    self.disputeDetailButton.isHidden = false
                    self.reportAnIssueButton.isHidden = true
                } else {
                    self.reportAnIssueButton.isHidden = false
                    self.disputeDetailButton.isHidden = true
                }
            }
            
            if (purchase!.canCancelPurchase!) {
                self.cancelOrderButton.isHidden = false
                self.leaveFeedBackButton.isHidden = true
                self.ratingView.isHidden = true
            } else {
                self.cancelOrderButton.isHidden = true
                self.leaveFeedBackButton.isHidden = false
                self.ratingView.isHidden = true
                if (purchase!.rating ?? 0 > 0) {
                    self.cancelOrderButton.isHidden = true
                    self.leaveFeedBackButton.isHidden = true
                    self.cancelOrderButton.isOpaque = true
                    self.leaveFeedBackButton.isOpaque = true
                    self.ratingView.isHidden = false
                    
                    let rating = purchase!.rating!
                    if rating == 1{
                        self.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starTwoImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        self.starThreeImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        self.starFourImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        self.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    }else if rating == 2{
                        self.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starThreeImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        self.starFourImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        self.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    }else if rating == 3{
                        self.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starThreeImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starFourImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        self.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    }else if rating == 4{
                        self.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starThreeImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starFourImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    }else{
                        self.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starThreeImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starFourImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starFiveImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    }
                    
                } else {
                    self.cancelOrderButton.isHidden = true
                    self.leaveFeedBackButton.isHidden = false
                    self.cancelOrderButton.isOpaque = true
                    self.ratingView.isOpaque = true
                    self.ratingView.isHidden = true
                }
            }
        }else{
            if (sale!.hasSubStatus!) {
                self.withoutSubHeadingView.isHidden = true
                //cell.withSubStatusView.isHidden = false
                self.withSubStatusItemStatusLabel.text = statusText
                self.withSubStatusItemSubStatusLabel.text = subStatusText
                
                if sale?.saleStatusId == AppConstants.RETURN_ORDER_DELIVERED{
                    
                    self.confirmDeliveryView.isHidden = false
                    self.withSubStatusView.isHidden = true
                    
                    self.confirmDeliveryView.backgroundColor = UIColor.cDC4293
                    self.confirmDeliveryStatusLabel.textColor = UIColor.white
                    self.confirmDeliverySubStatusLabel.textColor = UIColor.white
                    self.confirmDeliveryImageView.isHidden = false
                    self.confirmDeliveryStatusLabel.text = statusText
                    self.confirmDeliverySubStatusLabel.text = subStatusText
                    
                }else{
                    self.confirmDeliveryView.isHidden = true
                    self.withSubStatusView.isHidden = false
                    self.withSubStatusView.backgroundColor = UIColor.white
                    self.withSubStatusItemStatusLabel.textColor = UIColor.c232323
                    self.withSubStatusItemSubStatusLabel.textColor = UIColor.c232323
                    self.returnDeliveredImageView.isHidden = true
                }
                
            } else {
                self.itemStatusLabel.text = statusText
                self.withSubStatusView.isHidden = true
                self.withoutSubHeadingView.isHidden = false
                self.withoutSubHeadingView.backgroundColor = UIColor.cDC4293
                self.withoutSubHeadingInnerView.backgroundColor = UIColor.cDC4293
            }
            
            if (sale!.hasOpenDispute!) {
                self.disputeDetailButton.isHidden = false
                self.reportAnIssueButton.isHidden = true
            } else {
                if(sale!.hasViewableDispute!) {
                    self.disputeDetailButton.isHidden = false
                    self.reportAnIssueButton.isHidden = true
                } else {
                    self.reportAnIssueButton.isHidden = false
                    self.disputeDetailButton.isHidden = true
                }
            }
            
            if (sale!.canCancelSale!) {
                self.cancelOrderButton.isHidden = false
                self.leaveFeedBackButton.isHidden = true
                self.ratingView.isHidden = true
            } else {
                self.cancelOrderButton.isHidden = true
                self.leaveFeedBackButton.isHidden = false
                self.ratingView.isHidden = true
                
                if (sale!.rating ?? 0 > 0) {
                    self.cancelOrderButton.isHidden = true
                    self.leaveFeedBackButton.isHidden = true
                    self.ratingView.isHidden = false
                    let rating = sale!.rating!
                    
                    if rating == 1{
                        self.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starTwoImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        self.starThreeImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        self.starFourImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        self.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    }else if rating == 2{
                        self.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starThreeImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        self.starFourImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        self.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    }else if rating == 3{
                        self.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starThreeImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starFourImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                        self.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    }else if rating == 4{
                        self.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starThreeImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starFourImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    }else{
                        self.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starThreeImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starFourImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                        self.starFiveImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    }
                    
                } else {
                    self.cancelOrderButton.isHidden = true
                    self.leaveFeedBackButton.isHidden = false
                    self.cancelOrderButton.isOpaque = true
                    self.ratingView.isOpaque = true
                    self.ratingView.isHidden = true
                }
                
            }
        }
        
        if TYPE == AppConstants.TYPE_PURCHASE{
            let imageUrl = purchase?.iconImageUrlShort ?? ""
            if purchase?.iconImageUrlShort != ""{
                let image = AppConstants.IMAGES_URL + imageUrl + AppConstants.IMAGES_EXTENSION
                self.itemStatusImageView.kf.setImage(with: URL(string: image)!)
            }
        }else{
            let imageUrl = sale?.iconImageUrlShort ?? ""
            let image = AppConstants.IMAGES_URL + imageUrl + AppConstants.IMAGES_EXTENSION
            self.itemStatusImageView.kf.setImage(with: URL(string: image)!)
        }
        
    }
    
    func removePopUp(){
        for view in self.view.subviews {
            view.removeFromSuperview()
        }
    }
    
    @IBAction func cancelOrderAction(_ sender: UIButton) {
        removePopUp()
        if self.TYPE == AppConstants.TYPE_PURCHASE{
            self.cancelOrderPurchasePopUpDelegate?.cancelOrderPurchasePopUp(purchase: self.purchase!)
        }else{
            self.cancelOrderSalePopUpDelegate?.cancelOrderSalePopUp(sale:self.sale!)
        }
    }
    
    @IBAction func leaveFeedbackAction(_ sender: UIButton) {
        feedbackDialog()
    }
    
    func feedbackDialog(){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.FEEDBACK_DIALOG_BOARD, bundle:nil)
        let feedbackDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.FEEDBACK_DIALOG) as! FeedbackDialogViewController
        feedbackDialogViewController.modalPresentationStyle = .overFullScreen
        feedbackDialogViewController.sale = self.sale
        feedbackDialogViewController.purchase = self.purchase
        feedbackDialogViewController.TYPE = self.TYPE
        feedbackDialogViewController.onFeedbackSubmitClickDelegate = self
        self.navigationController?.present(feedbackDialogViewController, animated:false,completion: nil)
    }
    
    @IBAction func relistItemAction(_ sender: UIButton) {
        relistItemClick(type:AppConstants.TYPE_SALE , sale:self.sale!)
    }
    
    @IBAction func disputeDetailAction(_ sender: UIButton) {
        removePopUp()
        if self.TYPE == AppConstants.TYPE_PURCHASE{
            disputeDetailPurchaseMenuPopUpDelegate?.disputeDetailPurchaseMenuPop(purchase: self.purchase!)
        }else{
            disputeDetailSaleMenuPopUpDelegate?.disputeDetailSaleMenuPopUp(sale:self.sale!)
        }
    }
    
    @IBAction func reportIssueAction(_ sender: UIButton) {
        removePopUp()
        if self.TYPE == AppConstants.TYPE_PURCHASE{
            reportAnIssuePurchasePopUpDelegate?.reportAnIssuePurchasePopUp(purchase: self.purchase!)
        }else{
            reportAnIssueSalePopUpDelegate?.reportAnIssueSalePopUp(sale:self.sale!)
        }
    }
    
    @IBAction func buttonTwoAction(_ sender: UITapGestureRecognizer) {
        var id = 0
        var title = ""
        if TYPE == AppConstants.TYPE_PURCHASE{
            id = (self.purchase!.purchaseId!)
            title = (self.purchase?.listingTitle)!
            if purchase?.canPrintReturnLabel ?? false{
                self.emailReturnLabelDialog(id:id,title:title)
            }else{
                self.trackingInfoDialog(id:id,title: title)
            }
        }else{
            let listingId = (self.sale!.listingId!)
            title = (self.sale!.listingTitle!)
            id = (self.sale!.saleId!)
            if (self.sale!.canPrintShippingLabel!){
                self.getShippingRate(listingId:listingId,title:title,id:id)
            }else{
                if self.sale!.saleStatusId == AppConstants.RETURN_ORDER_DELIVERED{
                    self.returnDeliveredDelegate?.returnDelivered()
                }else{
                    self.trackingInfoDialog(id:id, title:title)
                }
            }
        }
        
    }
    
    func emailReturnLabelDialog(id:Int,title:String){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.PRINT_SHIPPING_LABEL_DIALOG_BOARD, bundle:nil)
        let printShippingLabelDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.PRINT_SHIPPING_LABEL_DIALOG_SCREEN) as! PrintShippingLabelDialogViewController
        printShippingLabelDialogViewController.modalPresentationStyle = .overFullScreen
        printShippingLabelDialogViewController.id = id
        printShippingLabelDialogViewController.TYPE = AppConstants.TYPE_PRINT_RETURN_LABEL
        printShippingLabelDialogViewController.listingTitle = title
        printShippingLabelDialogViewController.printReturnLabelDelegate = self
        self.navigationController?.present(printShippingLabelDialogViewController, animated:false,completion: nil)
    }
    
    func trackingInfoDialog(id:Int,title:String){
        
        var canReturnInfoForReturnLabel = false
        
        if self.TYPE == AppConstants.TYPE_PURCHASE{
            if  self.purchase?.purchaseStatusId == AppConstants.RETURN_LABEL_PRINTED || self.purchase?.purchaseStatusId == AppConstants.RETURN_ORDER_SHIPPED || self.purchase?.purchaseStatusId == AppConstants.RETURN_ORDER_IN_TRANSIT || self.purchase?.purchaseStatusId == AppConstants.RETURN_ORDER_DELIVERED{
                canReturnInfoForReturnLabel = true
            }else{
                canReturnInfoForReturnLabel = false
            }
        }else{
            if self.sale?.saleStatusId == AppConstants.RETURN_LABEL_PRINTED || self.sale?.saleStatusId == AppConstants.RETURN_ORDER_SHIPPED || self.sale?.saleStatusId == AppConstants.RETURN_ORDER_IN_TRANSIT || self.sale?.saleStatusId == AppConstants.RETURN_ORDER_DELIVERED{
                canReturnInfoForReturnLabel = true
            }else{
                canReturnInfoForReturnLabel = false
            }
            
        }
        
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.DELIVERY_STATUS_DIALOG_BOARD, bundle:nil)
        let feedbackDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.DELIVERY_STATUS_DIALOG_SCREEN) as! DeliveryStatusDialogViewController
        feedbackDialogViewController.modalPresentationStyle = .overFullScreen
        feedbackDialogViewController.id = id
        feedbackDialogViewController.TYPE = self.TYPE
        feedbackDialogViewController.listingTitle = title
        feedbackDialogViewController.canReturnInfoForReturnLabel = canReturnInfoForReturnLabel
        self.navigationController?.present(feedbackDialogViewController, animated:false,completion: nil)
    }
    
    func emailShippingLabelDialog(id:Int,title:String,shippingRate:Double){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.PRINT_SHIPPING_LABEL_DIALOG_BOARD, bundle:nil)
        let printShippingLabelDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.PRINT_SHIPPING_LABEL_DIALOG_SCREEN) as! PrintShippingLabelDialogViewController
        printShippingLabelDialogViewController.modalPresentationStyle = .overFullScreen
        printShippingLabelDialogViewController.id = id
        printShippingLabelDialogViewController.TYPE = AppConstants.TYPE_PRINT_SHIPPING_LABEL
        printShippingLabelDialogViewController.listingTitle = title
        printShippingLabelDialogViewController.shippingRate = shippingRate
        printShippingLabelDialogViewController.printShippingLabelDelegate = self
        self.navigationController?.present(printShippingLabelDialogViewController, animated:false,completion: nil)
    }
    
    func emailShippingLabel(id:Int) {
        self.view.makeToastActivity(.center)
        self.dismiss(animated: false, completion: nil)
        profileViewModel.emailShippingLabel(accessToken: AppDefaults.getAccessToken(), id: id, { (response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 201 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    let storyBoard = UIStoryboard(name: AppConstants.Storyboard.PRINT_SHIPPING_LABEL_DIALOG_BOARD, bundle:nil)
                    let printShippingLabelDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.PRINT_SHIPPING_LABEL_DIALOG_SCREEN) as! PrintShippingLabelDialogViewController
                    printShippingLabelDialogViewController.modalPresentationStyle = .overFullScreen
                    printShippingLabelDialogViewController.TYPE = AppConstants.TYPE_SHIPPING_LABEL_DONE
                    printShippingLabelDialogViewController.printShippingLabelDelegate = self
                    printShippingLabelDialogViewController.listingTitle = self.sale?.listingTitle!
                    self.navigationController?.present(printShippingLabelDialogViewController, animated:false,completion: nil)
                    
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func emailReturnLabel(id:Int) {
            self.view.makeToastActivity(.center)
            self.dismiss(animated: false, completion: nil)
            profileViewModel.returnLabel(accessToken: AppDefaults.getAccessToken(), id: id, { (response) in
                if response.response?.statusCode == 201 || response.response?.statusCode == 200{
                    let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                    if data.isError {
                        let error = Utils.getErrorMessage(errors: data.errors)
                        self.showMessageDialog(message: error)
                    }else{
                      self.view.hideToastActivity()
                        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.PRINT_SHIPPING_LABEL_DIALOG_BOARD, bundle:nil)
                        let printShippingLabelDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.PRINT_SHIPPING_LABEL_DIALOG_SCREEN) as! PrintShippingLabelDialogViewController
                        printShippingLabelDialogViewController.modalPresentationStyle = .overFullScreen
                        printShippingLabelDialogViewController.TYPE = AppConstants.TYPE_RETURN_LABEL_DONE
                        printShippingLabelDialogViewController.printReturnLabelDelegate = self
                        printShippingLabelDialogViewController.listingTitle = self.purchase?.listingTitle!
                        self.navigationController?.present(printShippingLabelDialogViewController, animated:false,completion: nil)
                    }
                }else{
                    let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                    if data.isError {
                        let error = Utils.getErrorMessage(errors: data.errors)
                        self.showMessageDialog(message: error)
                    }
                  self.view.hideToastActivity()
                }
            } , { (error) in
                self.view.hideToastActivity()
                if error.statusCode == AppConstants.NO_INTERNET {
                    self.showInternetError()
                }
            })
        }
    
    func relistItemClick(type:Int,sale:SalesDetail){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.DELETE_IMAGE_DIALOG_BOARD, bundle:nil)
        let relistItemDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.DELETE_IMAGE_DIALOG) as! DeleteImageDialogViewController
        relistItemDialogViewController.TYPE = AppConstants.TYPE_RELIST_ITEM
        relistItemDialogViewController.sale = sale
        relistItemDialogViewController.onRelistItemClickDelegate = self
        relistItemDialogViewController.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(relistItemDialogViewController, animated:false,completion: nil)
    }
    
    func relistItem(sale:SalesDetail) {
        self.view.makeToastActivity(.center)
        profileViewModel.relistItem(accessToken: AppDefaults.getAccessToken(), listingId: sale.listingId!, { (response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 201 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    self.openMyBynScreen()
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func openMyBynScreen() {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.USER_BYN_BOARD, bundle: nil)
        let userBynViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.USER_BYN_SCREEN) as! UserBynViewController
        
        var vcArray = self.navigationController?.viewControllers
        vcArray!.removeLast()
        vcArray!.append(userBynViewController)
        self.navigationController?.setViewControllers(vcArray!, animated: false)
    }
    
    func purchaseFeedback(id:Int,rating:Int,comment:String) {
        let purchaseData = PurchaseDetail(purchaseId: id, rating: rating, feedback: comment)
        profileViewModel.buyerFeedback(accessToken: AppDefaults.getAccessToken(), id: id, data: purchaseData, { (response) in
            if response.response?.statusCode == 201 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    self.navigationController?.dismiss(animated: false, completion: nil)
                    self.view.makeToastActivity(.center)
                    
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func sellerFeedback(id:Int,rating:Int,comment:String) {
        let sellerData = SalesDetail(saleId: id, rating: rating, feedback: comment)
        profileViewModel.sellerFeedback(accessToken: AppDefaults.getAccessToken(), id: id, data: sellerData, { (response) in
            if response.response?.statusCode == 201 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    self.navigationController?.dismiss(animated: false, completion: nil)
                    self.view.makeToastActivity(.center)
                    
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getShippingRate(listingId:Int,title:String,id:Int) {
        self.view.makeToastActivity(.center)
        profileViewModel.shippingRate(accessToken: AppDefaults.getAccessToken(), id: listingId, { (response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<ShippingRate> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    let shipping = data.data!
                    let shippingRate:Double = shipping.shippingRate!
                    self.emailShippingLabelDialog(id:id, title:title,shippingRate:shippingRate)
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    
}
