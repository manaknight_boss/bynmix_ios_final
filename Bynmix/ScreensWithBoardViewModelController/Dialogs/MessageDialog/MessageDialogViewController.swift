import UIKit

class MessageDialogViewController: UIViewController {

    @IBOutlet var outerView: UIView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet weak var errorMessageLabel: UILabel!
    @IBOutlet var crossButton: UIButton!
    
    var message : String?
    var text:String?
    var textColor:String?
    var buyNowReturnDelegate:BuyNowReturnProtocol?
    var TYPE:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.innerView.layer.borderWidth = 0.3
        self.innerView.layer.borderColor = UIColor.white.cgColor
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
        
        if text != nil && textColor != nil{
             setText()
        }else{
            errorMessageLabel.text = message
        }
       
    }
    
    func setText(){
        let text:String = self.text!
        let textColor:String = self.textColor!
        let range = (text as NSString).range(of: textColor)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cEA9DDC, range: range)
        errorMessageLabel.attributedText = attributedString
    }
    
    
    @IBAction func dismissButtonTapped(_ sender: Any) {
        if self.TYPE == AppConstants.TYPE_POP_BUY_NOW_SCREEN{
            self.buyNowReturnDelegate?.buyNowReturn()
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func tapDetected() {
        if self.TYPE == AppConstants.TYPE_POP_BUY_NOW_SCREEN{
            self.buyNowReturnDelegate?.buyNowReturn()
        }else{
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func click() {
    }

    
}
