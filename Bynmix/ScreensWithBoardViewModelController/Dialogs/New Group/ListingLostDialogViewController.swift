import UIKit

class ListingLostDialogViewController: BaseViewController {

    @IBOutlet var innerView: UIView!
    @IBOutlet var outerView: UIView!
    @IBOutlet weak var positiveButton: UIButton!
    @IBOutlet weak var negativeButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    var onBackButtonPressDelegate:onBackButtonPressProtocol?
    var onCancelButtonClickDelegate:OnCancelButtonClickProtocol?
    var shippingRateDelegate:ShippingRateProtocol?
    var subOfferHistory:SubOfferHistory?

    var TYPE:Int?
    var index:Int?
    var listingTitle:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.innerView.layer.borderWidth = 0.3
        self.innerView.layer.borderColor = UIColor.white.cgColor
        
        self.innerView.centerXAnchor.constraint(equalTo: self.outerView.centerXAnchor).isActive = true
        self.innerView.centerYAnchor.constraint(equalTo: self.outerView.centerYAnchor).isActive = true
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
        onTypeChange()
    }
    
    func onTypeChange(){
        if TYPE == AppConstants.TYPE_CANCEL_OFFER{
            let listingTitle = subOfferHistory?.listingTitle!
            self.setText(listingTitle: listingTitle!)
            positiveButton.setTitle(AppConstants.Strings.CANCEL_MY_OFFER_TEXT, for: .normal)
            negativeButton.setTitle(AppConstants.Strings.BUTTON_TITLE_NO_CHANGED_MY_MIND, for: .normal)
        }else if TYPE == AppConstants.TYPE_CALCULATED_SHIPPING_RATE{
            positiveButton.setTitle(AppConstants.Strings.YES_CALCULATE_SHIPPING_RATE, for: .normal)
            negativeButton.setTitle(AppConstants.Strings.BUTTON_TITLE_NO_CHANGED_MY_MIND, for: .normal)
            self.setTextForListingCalculated(listingTitle:self.listingTitle)
        }else if TYPE == AppConstants.TYPE_FLAT_RATE{
            positiveButton.setTitle(AppConstants.Strings.YES_SELECT_FLAT_RATE, for: .normal)
            self.setTextForListing(listingTitle:self.listingTitle)
            negativeButton.setTitle(AppConstants.Strings.BUTTON_TITLE_NO_CHANGED_MY_MIND, for: .normal)
        }else if TYPE == AppConstants.TYPE_FREE_SHIPPING{
            negativeButton.setTitle(AppConstants.Strings.BUTTON_TITLE_NO_CHANGED_MY_MIND, for: .normal)
            positiveButton.setTitle(AppConstants.Strings.YES_SELECT_FREE_SHIPPING, for: .normal)
            self.setTextForFreeShipping(listingTitle:self.listingTitle)
        }else{
            titleLabel.text = AppConstants.Strings.LISTING_LOST_MESSAGE
            positiveButton.setTitle(AppConstants.Strings.YES, for: .normal)
            negativeButton.setTitle(AppConstants.CANCEL, for: .normal)
        }
    }
    
    func setText(listingTitle:String){
        let text:String = AppConstants.Strings.CANCEL_OFFER_FOR_LISTING + AppConstants.COMMA + listingTitle + AppConstants.COMMA_QUESTION_MARK
        let textPink:String = listingTitle
        let rangePink = (text as NSString).range(of: textPink)
        
        let attributedString = NSMutableAttributedString(string: text)
        
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cFF6DE3, range: rangePink)
        titleLabel.attributedText = attributedString
    }
    
    func setTextForListing(listingTitle:String){
        let text:String = "Are you sure you want to change \"“\(listingTitle)”\" to use a set shipping rate? \n\n Converting to a set shipping rate means that any active offers for this listing will be declined and buyers will be notified."
        let textPink:String = listingTitle
        let rangePink = (text as NSString).range(of: textPink)
        let rangeText = (text as NSString).range(of: text)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 18), range: rangeText)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cFF86DD, range: rangePink)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 18), range: rangePink)
        titleLabel.attributedText = attributedString
    }
    
    func setTextForListingCalculated(listingTitle:String){
        let text:String = "Are you sure you want to change \"“\(listingTitle)”\" to use a calculated shipping rate? \n\n  Converting to a calculated shipping rate means that any active offers for this listing will be declined and buyers will be notified."
        let textPink:String = listingTitle
        let rangePink = (text as NSString).range(of: textPink)
        let rangeText = (text as NSString).range(of: text)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 18), range: rangeText)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cFF86DD, range: rangePink)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 18), range: rangePink)
        titleLabel.attributedText = attributedString
    }
    
    func setTextForFreeShipping(listingTitle:String){
        let text:String = "Are you sure you want to change \"“\(listingTitle)”\" to use free shipping? \n\n  Converting to a free shipping means that any active offers for this listing will be declined and buyers will be notified."
        let textPink:String = listingTitle
        let rangePink = (text as NSString).range(of: textPink)
        let rangeText = (text as NSString).range(of: text)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 18), range: rangeText)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cFF86DD, range: rangePink)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 18), range: rangePink)
        titleLabel.attributedText = attributedString
    }
    
    @IBAction func okButtonAction(_ sender: UIButton) {
        self.onCancelButtonClickDelegate?.onCancelButtonClick(subOfferHistory: subOfferHistory!, index: self.index!)
        self.onBackButtonPressDelegate?.onBackButtonPress(type: TYPE ?? 0)
        self.shippingRateDelegate?.shippingRate(type:TYPE ?? 0)
        self.dismiss(animated: false, completion: nil)
        if TYPE == AppConstants.TYPE_LISTING_LOST_ON_TAB_SELECT{
            if let tabBarController = self.presentingViewController as? UITabBarController {
                tabBarController.selectedIndex = AppConstants.SELECTED_TAB
            }
        }
    }
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func closeButtonAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func tapDetected() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func click() {
        
    }
}
