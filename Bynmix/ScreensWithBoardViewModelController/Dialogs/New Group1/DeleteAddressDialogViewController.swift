import UIKit

class DeleteAddressDialogViewController: BaseViewController {
    
    @IBOutlet var innerView: UIView!
    @IBOutlet var outerView: UIView!
    @IBOutlet var nameLabel:UILabel!
    @IBOutlet var addressOneLabel:UILabel!
    @IBOutlet var addressTwoLabel:UILabel!
    @IBOutlet var addressThreeLabel:UILabel!
    
    var address: Address?
    var deleteAddressDelegate:DeleteAddressClickProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.innerView.layer.borderWidth = 0.3
        self.innerView.layer.borderColor = UIColor.white.cgColor
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
        
        nameLabel.text = address?.fullName
        addressOneLabel.text = address?.addressOne
        addressTwoLabel.text = address?.addressTwo
        let city:String = address?.city ?? ""
        let state:String = address?.stateAbbreviation ?? ""
        let zipcode:String = address?.zipCode ?? ""
        let addressThree:String = (city + " " + state + ", " + zipcode)
        addressThreeLabel.text = addressThree

    }
    
    @IBAction func deleteButtonAction(_ sender: UIButton) {
        self.deleteAddressDelegate?.DeleteAddressClick(type: AppConstants.TYPE_DELETE_ADDRESS, address: address!)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func changedMyMindButtonAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func tapDetected() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func click() {
        
    }
    
}
