import UIKit

class PostCheckDialogViewController: BaseViewController {

    @IBOutlet var innerView: UIView!
    @IBOutlet var outerView: UIView!
    @IBOutlet var textLabel: UILabel!
    @IBOutlet var positiveButton: UIButton!
    @IBOutlet var negativeButton:UIButton!
    
    var delegate:onPostTypeChangeProtocol?
    var TYPE:Int?
    var switchValue:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.innerView.layer.borderWidth = 0.3
        self.innerView.layer.borderColor = UIColor.white.cgColor
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
        if TYPE == AppConstants.TYPE_BLOG_POST{
            setText(text:AppConstants.Strings.BLOG_POST_SELECTED,textRegular:AppConstants.BLOG_POST_TEXT)
            positiveButton.setTitle(AppConstants.Strings.CHANGE_TO_VIDEO_POST, for: .normal)
            negativeButton.setTitle(AppConstants.Strings.NO_ADDING_BLOG_POST, for: .normal)
        } else if TYPE == AppConstants.TYPE_VIDEO_POST{
            setText(text:AppConstants.Strings.VIDEO_POST_SELECTED,textRegular:AppConstants.VIDEO_POST_TEXT)
            positiveButton.setTitle(AppConstants.Strings.CHANGE_TO_BLOG_POST, for: .normal)
            negativeButton.setTitle(AppConstants.Strings.NO_ADDING_VIDEO_POST, for: .normal)
        }else if TYPE == AppConstants.TYPE_DIRECT_PURCHASE {
            setText(text:AppConstants.Strings.DIRECT_PURCHASE_TEXT,textRegular:AppConstants.Strings.DIRECT_PURCHASE)
            positiveButton.setTitle(AppConstants.Strings.YES_DIRECT_PURCHASE, for: .normal)
            negativeButton.setTitle(AppConstants.Strings.NO_I_CHANGED_MY_MIND, for: .normal)
        }
        
    }
    
    func setText(text:String,textRegular:String){
        let range = (text as NSString).range(of: textRegular)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cEA9DDC, range: range)
        textLabel.attributedText = attributedString
    }
    
    @IBAction func onpositiveButtonAction(_ sender: UIButton) {
        delegate?.onPostTypeChange(type: TYPE!)
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func onNegativeButtonAction(_ sender: UIButton) {
        delegate?.onPostTypeChange(type: AppConstants.TYPE_SWITCH_VALUE)
        dismiss(animated: false, completion: nil)
    }
    
    
    @IBAction func onCloseButtonAction(_ sender: UIButton) {
        delegate?.onPostTypeChange(type: AppConstants.TYPE_SWITCH_VALUE)
        dismiss(animated: false, completion: nil)
    }
    @objc func tapDetected() {
        delegate?.onPostTypeChange(type: AppConstants.TYPE_SWITCH_VALUE)
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func click() {
        
    }

}
