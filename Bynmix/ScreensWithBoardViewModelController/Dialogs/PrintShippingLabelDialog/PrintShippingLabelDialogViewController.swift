import UIKit

class PrintShippingLabelDialogViewController: BaseViewController {

    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var shippingInfoLabel: UILabel!
    @IBOutlet var innerView: UIView!
    @IBOutlet var outerView: UIView!
    @IBOutlet var emailShippingLabelButton: UIButton!
    @IBOutlet var emailShippingView: UIView!
    @IBOutlet var emailShippingDoneView: UIView!
    @IBOutlet var outputLabel: UILabel!
    @IBOutlet var buyerSellerTextLabel: UILabel!
    @IBOutlet weak var shippingTypeView: UIView!
    @IBOutlet weak var shippingTypeLabel: UILabel!
    
    var TYPE:Int?
    var id:Int?
    var listingTitle:String?
    var printShippingLabelDelegate: PrintShippingLabelProtocol?
    var printReturnLabelDelegate:PrintReturnLabelProtocol?
    var shippingRate:Double?
    var shippingData:ShippingRate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailShippingLabelButton.backgroundColor = UIColor.cBB189C
        
        self.innerView.layer.borderWidth = 0.3
        self.innerView.layer.borderColor = UIColor.white.cgColor
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
        
        if TYPE == AppConstants.TYPE_PRINT_SHIPPING_LABEL{
            AppDefaults.setTitle(title: self.listingTitle!)
            emailShippingDoneView.isHidden = true
            emailShippingView.isHidden = false
            self.setText(title:self.listingTitle!)
            if self.shippingData != nil{
                if shippingData?.listingShippingTypeId == AppConstants.SHIPPING_TYPE_FREE{
                    self.emailShippingLabelButton.setTitle("SELECT SHIPPING METHOD", for: .normal)
                }else{
                    self.emailShippingLabelButton.setTitle(AppConstants.Strings.EMAIL_SHIPPING_LABEL_BUTTON_TEXT, for: .normal)
                }
            }
        }else if TYPE == AppConstants.TYPE_PRINT_RETURN_LABEL{
            AppDefaults.setTitle(title: self.listingTitle!)
            self.emailShippingLabelButton.setTitle(AppConstants.Strings.EMAIL_RETURN_LABEL_BUTTON_TEXT, for: .normal)
            emailShippingDoneView.isHidden = true
            emailShippingView.isHidden = false
            self.setText(title:self.listingTitle!)
            shippingTypeView.isHidden = true
        }else if TYPE == AppConstants.TYPE_RETURN_LABEL_DONE{
            self.emailShippingLabelButton.setTitle(AppConstants.Strings.GOT_IT_TEXT, for: .normal)
            emailShippingDoneView.isHidden = false
            emailShippingView.isHidden = true
            setTextShippingDone(title:AppDefaults.getTitle())
            shippingTypeView.isHidden = true
        }else{
            self.emailShippingLabelButton.setTitle(AppConstants.Strings.GOT_IT_TEXT, for: .normal)
            emailShippingDoneView.isHidden = false
            emailShippingView.isHidden = true
            setTextShippingDone(title:AppDefaults.getTitle())
            shippingTypeView.isHidden = true
        }
    }
    
    func setText(title:String){
        if TYPE == AppConstants.TYPE_PRINT_SHIPPING_LABEL{
            self.buyerSellerTextLabel.text = AppConstants.Strings.PRINT_SHIPPING_LABEL_FOR
            
            if self.shippingData != nil{
                if shippingData?.listingShippingTypeId == AppConstants.SHIPPING_TYPE_FREE{
                    shippingInfoLabel.text = "Since you offered Free Shipping for this listing, select a shipping method from the list and we’ll charge your account for the label once you accept."
                    shippingTypeView.isHidden = true
                }else{
                    let shippingText:String = AppConstants.Strings.EMAIL_SHIPPING_LABEL + AppConstants.DOLLAR + String(format: "%.2f", self.shippingRate!) + AppConstants.Strings.EMAIL_SHIPPING_LABEL_TAIL + "\n\nNOTE: Make sure to use the correct shipping packaging for the label provided.\n\nYour shipping label is for the following shipping type:"
                    let shippingTextColor:String = AppConstants.DOLLAR + String(format: "%.2f", self.shippingRate!)
                    let shippingEmailLabel:String = AppConstants.Strings.EMAIL_SHIPPING_LABEL_TEXT
                    
                    let shippingRange = (shippingText as NSString).range(of: shippingEmailLabel)
                    let shippingColorRange = (shippingText as NSString).range(of: shippingTextColor)
                    
                    let shippingAttributedString = NSMutableAttributedString(string: shippingText)
                    
                    shippingAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cFF50A9, range: shippingColorRange)
                    shippingAttributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 14), range: shippingColorRange)
                    shippingAttributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 14), range: shippingRange)
                    
                    shippingInfoLabel.attributedText = shippingAttributedString
                    shippingTypeView.isHidden = false
                    shippingTypeLabel.text = shippingData?.shippingType ?? ""
                }
            }
        }else{
            self.buyerSellerTextLabel.text = AppConstants.Strings.PRINT_RETURN_LABEL_FOR
            
            let text = AppConstants.Strings.RETURN_LABEL_WARNING
            let textFontChange = AppConstants.Strings.EMAIL_RETURN_LABEL_TEXT
            let range = (text as NSString).range(of: textFontChange)
            let attributedString = NSMutableAttributedString(string: text)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 14), range: range)
            
            shippingInfoLabel.attributedText = attributedString
        }
        
        let text:String = AppConstants.COMMA + title + AppConstants.COMMA
        let textColor:String =  title
        let range = (text as NSString).range(of: textColor)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cFF50A9, range: range)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 20), range: range)
        titleLabel.attributedText = attributedString
        
    }
    
    func setTextShippingDone(title:String){
        if TYPE == AppConstants.TYPE_RETURN_LABEL_DONE{
            let text:String = AppConstants.Strings.EMAILED_RETURN_LABEL + AppConstants.COMMA + title + AppConstants.COMMA + AppConstants.Strings.RETURN_EARLIEST_FOR_REFUND
            let textColor:String =  title
            let range = (text as NSString).range(of: textColor)
            let attributedString = NSMutableAttributedString(string: text)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cFF50A9, range: range)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 14), range: range)
            outputLabel.attributedText = attributedString
        }else{
            let text:String = AppConstants.Strings.EMAILED_SHIPPING_LABEL + AppConstants.COMMA + title + AppConstants.COMMA + AppConstants.Strings.NEGATIVE_FEEDBACK
            let textColor:String =  title
            let range = (text as NSString).range(of: textColor)
            let attributedString = NSMutableAttributedString(string: text)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cFF50A9, range: range)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 14), range: range)
            outputLabel.attributedText = attributedString
        }
        
    }
    
    @IBAction func emailShippingLabelAction(_ sender: UIButton) {
        if TYPE == AppConstants.TYPE_PRINT_SHIPPING_LABEL{
            if self.shippingData?.listingShippingTypeId == AppConstants.SHIPPING_TYPE_FREE{
                self.printShippingLabelDelegate?.PrintShippingLabel(type: AppConstants.TYPE_PRINT_SHIPPING_LABEL_FREE_SHIPPING, id: self.id!)
            }else{
                self.printShippingLabelDelegate?.PrintShippingLabel(type: AppConstants.TYPE_PRINT_SHIPPING_LABEL, id: self.id!)
            }
        }else if TYPE == AppConstants.TYPE_PRINT_RETURN_LABEL{
            self.printReturnLabelDelegate?.printReturnLabel(type: AppConstants.TYPE_PRINT_RETURN_LABEL, id: self.id!)
        }else if TYPE == AppConstants.TYPE_RETURN_LABEL_DONE{
            self.printReturnLabelDelegate?.printReturnLabel(type: AppConstants.TYPE_RETURN_LABEL_DONE, id: 0)
        }else{
            self.printShippingLabelDelegate?.PrintShippingLabel(type: AppConstants.TYPE_SHIPPING_LABEL_DONE, id: 0)
        }
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func tapDetected() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func click() {
    }
    
}
