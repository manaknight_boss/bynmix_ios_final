import UIKit

class SelectExistingAddressDialogViewController: BaseViewController {
    
    var isdefaultAddress = false
    var address : [Address] = []
    
    @IBOutlet var outerView: UIView!
    @IBOutlet var innerView: UIView!
    @IBOutlet var addressesTableView: UITableView!
    
    var isShippingAddress:Bool = false
    var isReturnAddress:Bool = false
    var updateAddressDelegate:UpdateAddressProtocol?
    let existingAddressDialogHandler = ExistingAddressDialogHandler()
    var currentAddress :Address?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
        addressesTableView.delegate = existingAddressDialogHandler
        addressesTableView.dataSource = existingAddressDialogHandler
        existingAddressDialogHandler.address = address
        addressesTableView.reloadData()
        existingAddressDialogHandler.onDefaultButtonClick = { address in
            self.isdefaultAddress = true
            self.currentAddress = address
        }
    }

    @IBAction func onUpdateAction(_ sender: UIButton) {
        self.updateAddressDelegate?.updateAddress(type: AppConstants.TYPE_UPDATE_ADDRESS, isDefault: self.isdefaultAddress,address: self.currentAddress!)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func closeButtonAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func tapDetected() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func click() {
        
    }
    
}
