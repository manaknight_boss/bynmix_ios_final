import UIKit

class SuccessMessageViewController: BaseViewController {
    
    @IBOutlet var innerView: UIView!
    @IBOutlet var outerView: UIView!
    @IBOutlet var textLabel: UILabel!
    @IBOutlet weak var greetLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var lastLabel: UILabel!
    
    var accountInfo:AccountInfo?
    var TYPE:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
        
        if TYPE == AppConstants.TYPE_CANCEL_DISPUTE{
            messageLabel.text = AppConstants.Strings.DISPUTE_CANCELLED
            textLabel.text = AppConstants.Strings.LET_SELLER_KNOW
            lastLabel.isHidden = true
            
        }else if TYPE == AppConstants.TYPE_REPORT_AN_ISSUE{
            greetLabel.text = AppConstants.Strings.THANK_YOU
            messageLabel.text = AppConstants.Strings.REPORTED_ISSUE_TEXT
            textLabel.isHidden = true
            lastLabel.isHidden = true
        }else{
            
            lastLabel.isHidden = false
            let amount:String = "\(accountInfo?.amount! ?? 0)"
            let lastFour:String = String(accountInfo?.lastFour! ?? "")
            let date: String = (accountInfo?.arrivalDateFormatted!)!
            
            textLabel.text = AppConstants.Strings.AMOUNT_TEXT + amount + AppConstants.Strings.HAS_BEEN_PROCESSED + lastFour + AppConstants.Strings.IS_EXPECTED + date
            
            let text:String = AppConstants.Strings.AMOUNT_TEXT + amount + AppConstants.Strings.HAS_BEEN_PROCESSED + lastFour + AppConstants.Strings.IS_EXPECTED + date
            let textPink:String = AppConstants.DOLLAR + amount + "0"
            let range = (text as NSString).range(of: textPink)
            let textPinkOther:String = lastFour
            let rangeOther = (text as NSString).range(of: textPinkOther)
            let textPinkDate:String = date
            let rangeDate = (text as NSString).range(of: textPinkDate)
            let attributedString = NSMutableAttributedString(string: text)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: range)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cEA9DDC, range: range)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: rangeOther)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cEA9DDC, range: rangeOther)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: rangeDate)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cEA9DDC, range: rangeDate)
            textLabel.attributedText = attributedString
            
        }
        
    }
    
    @IBAction func closeButton(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func tapDetected() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func click() {
        
    }
    
}
