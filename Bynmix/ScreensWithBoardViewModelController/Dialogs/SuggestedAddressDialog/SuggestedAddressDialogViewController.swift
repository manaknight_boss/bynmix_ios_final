import UIKit

class SuggestedAddressDialogViewController: BaseViewController {

    @IBOutlet var outerView: UIView!
    @IBOutlet weak var innerView: UIView!
    @IBOutlet var crossButton: UIButton!
    @IBOutlet var suggestedAddressLabel: UILabel!
    @IBOutlet var originalAddressLabel: UILabel!
    
    var suggestedAddress:Address?
    var originalAddress:Address?
    var fullName:String?
    var addressId:Int?
    var suggestedAddressDelegate: SuggestedAddressProtocol?
    var updateSuggestedAddress:UpdateSuggestedAddressProtocol?
    var addPaymentSuggestedAddressDelegate:AddPaymentSuggestedAddressProtocol?
    var token:String?
    var expirationMonth:Int?
    var expirationYear:Int?
    var cardId:Int?
    var nameOnCard:String?
    var editPaymentSuggestedAddressDelegate:EditPaymentSuggestedAddressProtocol?
    var addPaymentWithExistingAddressSuggestedAddressDelegate:AddPaymentWithExistingAddressSuggestedAddressProtocol?
    var editPaymentWithExistingAddressSuggestedAddressDelegate:EditPaymentWithExistingAddressSuggestedAddressProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.innerView.layer.borderWidth = 0.3
        self.innerView.layer.borderColor = UIColor.white.cgColor
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
        
        let currentSuggestedAddress = suggestedAddress!
        let currentOriginalAddress = originalAddress!
        
        let suggestedAddressOne = currentSuggestedAddress.addressOne!
        let suggestedAddressTwo = currentSuggestedAddress.addressTwo!
        
        let suggestedAddressCity = currentSuggestedAddress.city!
        let suggestedAddressStateAbbreviation = currentSuggestedAddress.stateAbbreviation!
        let suggestedAddressZipCode = currentSuggestedAddress.zipCode!
        
        let originalAddressOne = currentOriginalAddress.addressOne!
        let originalAddressTwo = currentOriginalAddress.addressTwo!
        
        let originalAddressCity = currentOriginalAddress.city!
        let originalAddressStateAbbreviation = currentOriginalAddress.stateAbbreviation!
        let originalAddressZipCode = currentOriginalAddress.zipCode!
        
        var suggestedAddress = ""
        var originalAddress = ""
        
        suggestedAddress = suggestedAddressOne + AppConstants.NEWLINE_N + (suggestedAddressTwo != "" ? (suggestedAddressTwo + AppConstants.NEWLINE_N) : "") + suggestedAddressCity + AppConstants.EXTRA_SPACE + suggestedAddressStateAbbreviation + AppConstants.SEPERATOR_COMMA + AppConstants.EXTRA_SPACE + suggestedAddressZipCode
        
        originalAddress = originalAddressOne + AppConstants.NEWLINE_N + (originalAddressTwo == "" ? "" : (originalAddressTwo + AppConstants.NEWLINE_N)) + originalAddressCity + AppConstants.EXTRA_SPACE + originalAddressStateAbbreviation + AppConstants.SEPERATOR_COMMA + AppConstants.EXTRA_SPACE + originalAddressZipCode

        suggestedAddressLabel.text = suggestedAddress
        
        originalAddressLabel.text = originalAddress
        
    }
    
    @IBAction func suggestedAddressAction(_ sender: UIButton) {
        self.suggestedAddressDelegate?.suggestedAddress(address: self.suggestedAddress!, fullName: self.fullName!,type: AppConstants.TYPE_SUGGESTED_ADDRESS)
        self.updateSuggestedAddress?.updateSuggestedAddress(address: self.suggestedAddress!, fullName: self.fullName!,type: AppConstants.TYPE_SUGGESTED_ADDRESS, addressId: self.addressId!)
        self.addPaymentSuggestedAddressDelegate?.addPaymentSuggestedAddress(token: self.token!, address: self.suggestedAddress!, fullName: self.fullName!, type: AppConstants.TYPE_SUGGESTED_ADDRESS)
        self.editPaymentSuggestedAddressDelegate?.editPaymentSuggestedAddres(token: self.token!, address: self.suggestedAddress!, fullName: self.fullName!, type: AppConstants.TYPE_SUGGESTED_ADDRESS, month: self.expirationMonth!, year: self.expirationYear!, cardId: self.cardId!, nameOnCard: self.nameOnCard!)
        self.addPaymentWithExistingAddressSuggestedAddressDelegate?.addPaymentWithExistingAddressSuggestedAddress(token: self.token!,type: AppConstants.TYPE_SUGGESTED_ADDRESS)
        self.editPaymentWithExistingAddressSuggestedAddressDelegate?.editPaymentWithExistingAddressSuggestedAddress(cardId:self.cardId!,nameOnCard: self.nameOnCard!, expirationMonth: self.expirationMonth!, expirationYear: self.expirationYear!)
    }
    
    @IBAction func originalAddressAction(_ sender: UIButton) {
        self.suggestedAddressDelegate?.suggestedAddress(address: self.originalAddress!, fullName: self.fullName!,type: AppConstants.TYPE_SUGGESTED_ADDRESS)
        self.updateSuggestedAddress?.updateSuggestedAddress(address: self.originalAddress!, fullName: self.fullName!,type: AppConstants.TYPE_SUGGESTED_ADDRESS, addressId: self.addressId!)
        self.addPaymentSuggestedAddressDelegate?.addPaymentSuggestedAddress(token: self.token!, address: self.originalAddress!, fullName: self.fullName!, type: AppConstants.TYPE_SUGGESTED_ADDRESS)
        self.editPaymentSuggestedAddressDelegate?.editPaymentSuggestedAddres(token: self.token!, address: self.originalAddress!, fullName: self.fullName!, type: AppConstants.TYPE_SUGGESTED_ADDRESS, month: self.expirationMonth!, year: self.expirationYear!, cardId: self.cardId!, nameOnCard: self.nameOnCard!)
    self.addPaymentWithExistingAddressSuggestedAddressDelegate?.addPaymentWithExistingAddressSuggestedAddress(token: self.token!,type: AppConstants.TYPE_SUGGESTED_ADDRESS)
    self.editPaymentWithExistingAddressSuggestedAddressDelegate?.editPaymentWithExistingAddressSuggestedAddress(cardId: self.cardId!, nameOnCard: self.nameOnCard!, expirationMonth: self.expirationMonth!, expirationYear: self.expirationYear!)
    }
    
    @IBAction func crossButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func tapDetected() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func click() {
    }
    
}
