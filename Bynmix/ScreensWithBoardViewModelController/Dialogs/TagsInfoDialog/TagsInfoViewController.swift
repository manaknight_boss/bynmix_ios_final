import UIKit
import AVFoundation
import AVKit

class TagsInfoViewController: BaseViewController {
    
    @IBOutlet var whatPostTagsView: UIView!
    @IBOutlet var addProductTagsView: UIView!
    @IBOutlet var removePostTagsView: UIView!
    @IBOutlet var linkPostTagsView: UIView!
    var player: AVPlayer?
    @IBOutlet var videoView: UIView!
    @IBOutlet var innerView: UIView!
    @IBOutlet var outerView: UIView!
    @IBOutlet var postTagsImageView: UIImageView!
    @IBOutlet var addPostTagsImageView: UIImageView!
    @IBOutlet var removePostTagsTitleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.innerView.layer.borderWidth = 0.3
        self.innerView.layer.borderColor = UIColor.white.cgColor
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
        whatPostTagsView.isHidden = false
        addProductTagsView.isHidden = true
        removePostTagsView.isHidden = true
        linkPostTagsView.isHidden = true
        postTagsImageView.contentMode = .scaleAspectFit
        addPostTagsImageView.contentMode = .scaleAspectFit
        removePostTagsTitleLabel.text = AppConstants.Strings.REMOVE_POST_TAGS_TEXT
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initVideo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        if player != nil{
            player?.replaceCurrentItem(with: nil)
            player = nil
        }
    }
    
    func initVideo() {
        
        videoView.layer.zPosition = 0;
        let videoURL: NSURL = Bundle.main.url(forResource: AppConstants.HOW_TO_DELETE_POST_TAGS, withExtension: AppConstants.VIDEO_EXTENSION)! as NSURL
        
        player = AVPlayer(url: videoURL as URL)
        player?.isMuted = true
        
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.zPosition = -1
        
        playerLayer.frame = self.videoView.bounds
        
        videoView.layer.addSublayer(playerLayer)
        
        player?.play()
        
        NotificationCenter.default.addObserver(self, selector: #selector(loopVideo), name: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem)
    }
    
    @IBAction func addProductTagsAction(_ sender: UIButton) {
        whatPostTagsView.isHidden = true
        addProductTagsView.isHidden = false
        removePostTagsView.isHidden = true
        linkPostTagsView.isHidden = true
    }
    
    @IBAction func removePostTagsAction(_ sender:  UIButton) {
        whatPostTagsView.isHidden = true
        addProductTagsView.isHidden = true
        removePostTagsView.isHidden = false
        linkPostTagsView.isHidden = true
    }
    
    
    @IBAction func linkTagsAction(_ sender: UIButton) {
        whatPostTagsView.isHidden = true
        addProductTagsView.isHidden = true
        removePostTagsView.isHidden = true
        linkPostTagsView.isHidden = false
        
    }
    
    @IBAction func gotItAction(_ sender: UIButton) {
        dismiss(animated: false, completion: nil)
    }
    
    @objc func loopVideo() {
        player?.seek(to: CMTime.zero)
        player?.play()
    }
    
    @objc func tapDetected() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func click() {
        
    }
    
    @IBAction func onCloseButtonAction(_ sender: UIButton) {
        dismiss(animated: false, completion: nil)
    }
    
    
}
