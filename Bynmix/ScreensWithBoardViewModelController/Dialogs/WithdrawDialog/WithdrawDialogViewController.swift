import UIKit

class WithdrawDialogViewController: BaseViewController {

    @IBOutlet var outerView: UIView!
    @IBOutlet var innerView: UIView!
    @IBOutlet var firstLabel: UILabel!
    @IBOutlet var secondLabel: UILabel!
    @IBOutlet var positiveButton: UIButton!
    @IBOutlet var negativeButton: UIButton!
    
    var TYPE:Int?
    var amount:String?
    var withdrawClickDelegate:WithdrawClickProtocol?
    var accountInfo:AccountInfo?
    var selectedBankAccount :Int?
    var UploadBankDetailsDelegate:UploadBankDetailsProtocol?
    var card:Card?
    var OnDeleteCardClickDelegate:OnDeleteCardClickProtocol?
    var startSellingDelegate:StartSellingProtocol?
    var itemTitle:String?
    var cancelPurchaseDelegate:CancelPurchaseProtocol?
    var purchaseDetail:PurchaseDetail?
    var disputeDetails:DisputeDetails?
    var cancelDisputeDelegate:CancelDisputeProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
        typeSelect()
    }
    
    func typeSelect(){
        if TYPE == AppConstants.TYPE_WITHDRAW{
            self.positiveButton.setTitle(AppConstants.Strings.YES_WITHDRAW, for: .normal)
            self.negativeButton.setTitle(AppConstants.CANCEL, for: .normal)
            let text:String = AppConstants.Strings.ARE_YOU_SURE_U_WANT_TO_TRANSFER + AppConstants.DOLLAR + (amount!) + AppConstants.Strings.FROM_YOUR_BALANCE
            let textPink:String = AppConstants.DOLLAR + amount!
            let range = (text as NSString).range(of: textPink)
            let attributedString = NSMutableAttributedString(string: text)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: range)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cFF7AC0, range: range)
            firstLabel.attributedText = attributedString
            
            let textOther:String = AppConstants.Strings.TRANSFER_FEE
            let textOtherPink:String = AppConstants.Strings.TWO_DOLLAR_TRANSFER_FREE
            let rangeTwo = (textOther as NSString).range(of: textOtherPink)
            let attributedStringTwo = NSMutableAttributedString(string: textOther)
            attributedStringTwo.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: rangeTwo)
            attributedStringTwo.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cFF77C1, range: rangeTwo)
            secondLabel.attributedText = attributedStringTwo
            
        }else if TYPE == AppConstants.TYPE_BANK_DETAIL{
            self.positiveButton.setTitle(AppConstants.Strings.YES_WITHDRAW, for: .normal)
            self.negativeButton.setTitle(AppConstants.CANCEL, for: .normal)
            let text:String = AppConstants.Strings.ARE_YOU_SURE_U_WANT_TO_TRANSFER + AppConstants.DOLLAR + String(format: "%.2f", accountInfo!.withdrawAmount!) + AppConstants.Strings.FROM_YOUR_BALANCE
            let textOther:String = AppConstants.Strings.TRANSFER_FEE
            let textPink:String = AppConstants.DOLLAR + String(format: "%.2f", accountInfo!.withdrawAmount!)
            let textOtherPink:String = AppConstants.Strings.TWO_DOLLAR_TRANSFER_FREE
            let range = (text as NSString).range(of: textPink)
            let rangeTwo = (textOther as NSString).range(of: textOtherPink)
            let attributedString = NSMutableAttributedString(string: text)
            let attributedStringTwo = NSMutableAttributedString(string: textOther)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: range)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cEA9DDC, range: range)
            firstLabel.attributedText = attributedString
            attributedStringTwo.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: rangeTwo)
            attributedStringTwo.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cEA9DDC, range: rangeTwo)
            secondLabel.attributedText = attributedStringTwo
        }else if TYPE == AppConstants.TYPE_DELETE_CARD{
            self.positiveButton.setTitle(AppConstants.Strings.BUTTON_TITLE_DELETE, for: .normal)
            self.negativeButton.setTitle(AppConstants.Strings.BUTTON_TITLE_NO_CHANGED_MY_MIND, for: .normal)
            let brandName:String = (self.card?.brand)!
            let lastFour:String = (self.card?.lastFour)!
            let text:String = AppConstants.Strings.ARE_YOU_SURE_PERMANENLY_DELETE + brandName +  AppConstants.Strings.PAYMENT_IN_ENDING_IN + lastFour + AppConstants.QUESTION_MARK_ONLY
            let textPink:String = brandName
            let textPinkLastFour:String = lastFour
            let range = (text as NSString).range(of: textPink)
            let rangeTwo = (text as NSString).range(of: textPinkLastFour)
            let attributedString = NSMutableAttributedString(string: text)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: range)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cEA9DDC, range: range)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: rangeTwo)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cEA9DDC, range: rangeTwo)
            firstLabel.attributedText = attributedString
            secondLabel.text = ""
        }else if TYPE == AppConstants.TYPE_REPLACE_CARD{
            self.positiveButton.setTitle(AppConstants.Strings.BUTTON_SELECT_A_DIFFERENT_CARD, for: .normal)
            self.negativeButton.setTitle(AppConstants.Strings.BUTTON_TITLE_NO_CHANGED_MY_MIND, for: .normal)
            firstLabel.text = AppConstants.Strings.ACTIVE_OFFERS
            secondLabel.text = ""
        }else if TYPE == AppConstants.TYPE_START_SELLING{
            self.positiveButton.setTitle(AppConstants.Strings.YES_LIST_NOW, for: .normal)
            self.negativeButton.setTitle(AppConstants.Strings.NOT_RIGHT_NOW, for: .normal)
            
            let text:String = AppConstants.Strings.AWESOME
            let textFontChange:String = AppConstants.Strings.AWESOME
            let range = (text as NSString).range(of: textFontChange)
            let attributedString = NSMutableAttributedString(string: text)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 18), range: range)
            self.firstLabel.attributedText = attributedString
            self.secondLabel.text = AppConstants.Strings.START_SELLING_TEXT
        }else if TYPE == AppConstants.TYPE_REPORT_AN_ISSUE{
            self.firstLabel.text = AppConstants.Strings.ITEM_NOT_DELIVERED_IN_TIME
            
            let text:String = AppConstants.Strings.CANCEL_PURCHASE_OF + "\"" + self.itemTitle! + "\" ?"
            let textPink:String = self.itemTitle!
            let range = (text as NSString).range(of: textPink)
            let attributedString = NSMutableAttributedString(string: text)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cEA9DDC, range: range)
            self.secondLabel.attributedText = attributedString
            
            self.positiveButton.setTitle(AppConstants.Strings.YES_CANCEL_THIS_PURCHASE, for: .normal)
            self.negativeButton.setTitle(AppConstants.Strings.NOT_YET, for: .normal)
            
        }else if TYPE == AppConstants.TYPE_CANCEL_DISPUTE{
            let text:String = AppConstants.Strings.SURE_TO_CANCEL_DISPUTE + "\"" + (self.disputeDetails?.listingTitle!)! + "\" ?"
            let textPink:String = (self.disputeDetails?.listingTitle!)!
            let range = (text as NSString).range(of: textPink)
            let attributedString = NSMutableAttributedString(string: text)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cFF74C2, range: range)
            attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 14), range: range)
            
            self.firstLabel.attributedText = attributedString
            self.secondLabel.text = AppConstants.Strings.DISPUTE_ONCE_CANCELLED_CANNOT_BE_OPEN
            
            self.positiveButton.setTitle(AppConstants.Strings.YES_CANCEL_DISPUTE, for: .normal)
            self.negativeButton.setTitle(AppConstants.Strings.BUTTON_TITLE_NO_CHANGED_MY_MIND, for: .normal)
        }
    }
    
    @IBAction func closeButton(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func postiveButtonAction(_ sender: UIButton) {
        if TYPE == AppConstants.TYPE_WITHDRAW{
            withdrawClickDelegate?.withdrawClick(type: AppConstants.TYPE_WITHDRAW,amount:amount!,selectedBankAccount: selectedBankAccount!)
        }else if TYPE == AppConstants.TYPE_BANK_DETAIL{
            UploadBankDetailsDelegate?.uploadBankDetails(type: AppConstants.TYPE_BANK_DETAIL, accountINfo: accountInfo!)
        }else if TYPE == AppConstants.TYPE_DELETE_CARD{
            OnDeleteCardClickDelegate?.OnDeleteCardClick(type: AppConstants.TYPE_DELETE_CARD, card: self.card!)
        }else if TYPE == AppConstants.TYPE_REPLACE_CARD{
            OnDeleteCardClickDelegate?.OnDeleteCardClick(type: AppConstants.TYPE_REPLACE_CARD, card: self.card!)
        }else if TYPE == AppConstants.TYPE_START_SELLING{
            startSellingDelegate?.startSelling(type: AppConstants.TYPE_LISTING)
        }
        else if TYPE == AppConstants.TYPE_REPORT_AN_ISSUE{
            cancelPurchaseDelegate?.cancelPurchase(purchase:self.purchaseDetail!)
        }else if TYPE == AppConstants.TYPE_CANCEL_DISPUTE{
            cancelDisputeDelegate?.cancelDispute(disputeDetails:self.disputeDetails!)
        }
    }
    
    @IBAction func negativeButtonAction(_ sender: UIButton) {
        if TYPE == AppConstants.TYPE_START_SELLING{
            startSellingDelegate?.startSelling(type: AppConstants.TYPE_PROFILE)
        }else{
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    @objc func tapDetected() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func click() {
        
    }
    
}
