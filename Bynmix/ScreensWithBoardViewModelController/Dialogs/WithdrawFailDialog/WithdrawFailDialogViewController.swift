import UIKit

class WithdrawFailDialogViewController: BaseViewController {

    @IBOutlet var messageOneLabel: UILabel!
    @IBOutlet var messageTwoLabel: UILabel!
    @IBOutlet var innerView: UIView!
    @IBOutlet var outerView: UIView!
    
    var TYPE:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.innerView.layer.borderWidth = 0.3
        self.innerView.layer.borderColor = UIColor.white.cgColor
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        outerView.isUserInteractionEnabled = true
        outerView.addGestureRecognizer(singleTap)
        let singleTap2 = UITapGestureRecognizer(target: self, action: #selector(click))
        innerView.addGestureRecognizer(singleTap2)
        onTypeChange()
    }
    
    func onTypeChange(){
        if TYPE == AppConstants.TYPE_WITHDRAW_FAIL{
          messageOneLabel.text = AppConstants.Strings.HANG_TIGHT
          messageTwoLabel.text = AppConstants.Strings.WITHDRAW_FAIL_MESSAGE
        }
    }
    
    @IBAction func crossButton(_ sender: UIButton) {
       self.dismiss(animated: false, completion: nil)
    }
    
    @objc func tapDetected() {
        self.dismiss(animated: false, completion: nil)
    }
    
    @objc func click() {
        
    }
    
}
