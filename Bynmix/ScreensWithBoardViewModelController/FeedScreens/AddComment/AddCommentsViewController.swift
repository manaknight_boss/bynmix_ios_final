import UIKit

class AddCommentsViewController: BaseViewController {
    
    @IBOutlet weak var replyCommentView: UIView!
    @IBOutlet weak var replyCommentLabel: UILabel!
    @IBOutlet weak var mentionTableView: UITableView!
    @IBOutlet weak var textViewMention: UITextView!
    @IBOutlet weak var topSeperatorView: UIView!
    
    var replyClicked:Bool?
    var comments:Comments?
    var addCommentDelegate:AddCommentProtocol?
    var listingDetail:ListingDetail?
    var feedViewModel = FeedViewModel()
    var mentionsHandler = MentionsHandler()
    var usersInComment:[MentionItem] = []
    var searchUsernameString = ""
    var isMentionStarted = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.Strings.ADD_LISTING_COMMENTS
        
        if replyClicked ?? false{
            replyCommentView.isHidden = false
            replyCommentLabel.text = "@\(comments?.username ?? "")"
        }else{
            replyCommentView.isHidden = true
        }
        replyCommentView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        
        textViewMention.text = AppConstants.Strings.ENTER_YOUR_COMMENT_HERE
        textViewMention.textColor = UIColor.cADADAD
        textViewMention.font = UIFont.fontRegular(size: 18)
        textViewMention.delegate = self
        
        mentionTableView.delegate = mentionsHandler
        mentionTableView.dataSource = mentionsHandler
        getMentionClick()
        let commentBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.SELECT_ICON), style: .plain, target: self, action: #selector(commentBarButtonItemAction))
        self.navigationItem.rightBarButtonItem  = commentBarButton
        self.hideUsersList()
    }
    
    @objc func commentBarButtonItemAction(){
        if textViewMention.text.trimmingCharacters(in: .whitespacesAndNewlines) != "" && textViewMention.text != AppConstants.Strings.ENTER_YOUR_COMMENT_HERE{
            self.submitButtonActionCommon()
        }else{
            self.view.makeToast(AppConstants.Strings.ADD_COMMENT_FIRST)
        }
    }
    
    func getMentionClick(){
        mentionsHandler.onUserClick = { mention in
            self.onMentionClickAction(mention:mention)
        }
    }
    
    func onMentionClickAction(mention:MentionItem){
        var mentionItem = mention
        
        let offset = self.textViewMention.text.lastIndexOfCharacter("@")
        
        let start = self.textViewMention.text!.index(self.textViewMention.text!.startIndex, offsetBy: offset!)
        let end = self.textViewMention.text!.endIndex
        let result = self.textViewMention.text!.replacingCharacters(in: start..<end, with: "@")
        print("result:",result)
        
        self.textViewMention.text = result + mention.username! + " "
        
        let startPosition = self.textViewMention.text.lastIndexOfCharacter("@")
        
        mentionItem.startPosition = startPosition
        mentionItem.endPosition = self.textViewMention.text.count - 2
        self.usersInComment.append(mentionItem)
        
        textViewMention.attributedText = resolveHashTags(text: textViewMention.text)
        textViewMention.linkTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.cFF80EE]
        
        self.hideUsersList()
        print(usersInComment)
        self.isMentionStarted = false
    }
    
    func resolveHashTags(text : String) -> NSAttributedString{
        var length : Int = 0
        let text:String = text
        let words:[String] = text.separate(withChar: " ")
        let hashtagWords = words.flatMap({$0.separate(withChar: "@")})
        let attrs = [NSAttributedString.Key.font : UIFont.fontRegular(size: 18),NSAttributedString.Key.foregroundColor : UIColor.black]
        let attrString = NSMutableAttributedString(string: text, attributes:attrs)
        for word in hashtagWords {
            if word.hasPrefix("@") && word.count > 1{
                let matchRange:NSRange = NSMakeRange(length, word.count)
                let stringifiedWord:String = word
                
                attrString.addAttribute(NSAttributedString.Key.link, value: "hash:\(stringifiedWord)", range: matchRange)
            }
            length += word.count
        }
        return attrString
    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        self.submitButtonActionCommon()
    }
    
    func submitButtonActionCommon(){
        var commment = ""
        var replyToUserId = 0
        
        if replyClicked ?? false{
            replyToUserId = comments?.userId ?? 0
            let replyName = "@" + (comments?.username)!
            textViewMention.text.insert(contentsOf: replyName + " ", at: textViewMention.text.startIndex)
            commment = textViewMention.text.trimmingCharacters(in: .whitespacesAndNewlines)
            var mentionItem = MentionItem(username: replyName, userId: replyToUserId)
            mentionItem.startPosition = 0
            mentionItem.endPosition = replyName.count - 1
            usersInComment.append(mentionItem)
        }else{
            commment = textViewMention.text.trimmingCharacters(in: .whitespacesAndNewlines)
        }
        
        var userMentions = usersInComment
        
        print("userMentions:",userMentions)
        print("mentionText:",textViewMention.text!)
        
        let comments = Comments(comment: commment, replyToUserId: replyToUserId, userMentions: userMentions)
        if commment != AppConstants.Strings.ENTER_YOUR_COMMENT_HERE && commment != ""{
            addCommentDelegate?.addComment(listingDetail: self.listingDetail!, Comment: comments)
        }else{
            self.view.makeToast(AppConstants.Strings.ADD_COMMENT_FIRST)
        }
        usersInComment.removeAll()
        userMentions.removeAll()
    }
    
    @IBAction func cancelButtonAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: false)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.trimmingCharacters(in: .whitespacesAndNewlines).count > 0{
            if textView.text.trimmingCharacters(in: .whitespacesAndNewlines) != ""{
                
                if !(textView.text.last?.containsEmoji ?? false){
                    textView.attributedText = resolveHashTags(text: textView.text)
                    textView.linkTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.cFF80EE]
                }
                
                let text = textView.text
                let separated = text!.components(separatedBy: "@")
                if textView.text.contains("@"){
                    
                    if textView.text.count > 0{
                        
                        if (text?.hasSuffix(" @"))! {
                            self.isMentionStarted = true
                        }else if text == " "{
                            self.isMentionStarted = false
                        }else if text?.last == "@"{
                            self.isMentionStarted = true
                        }
                        
                    }
                    
                    if let textAfterAt = separated.last {
                        print(textAfterAt)
                        self.searchUsernameString = textAfterAt
                        
                        if self.isMentionStarted{
                            
                            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reloadUsers), object: nil)
                            self.perform(#selector(self.reloadUsers), with: nil, afterDelay: 0.6)
                            
                        }else{
                            self.hideUsersList()
                        }
                    }
                }
                
                if isMentionStarted{
                    self.showUsersList()
                }else{
                    self.hideUsersList()
                }
            }else{
                self.hideUsersList()
            }
        }else{
            self.hideUsersList()
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool{
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        let numberOfChars = newText.count
        
            if text == ""{
                self.isMentionStarted = false
                
                if let selectedRange = textView.selectedTextRange{
                    let cursorOffset = textView.offset(from: textView.beginningOfDocument, to: selectedRange.start)
                    if let myText = textView.text{
                        
                        if !(textView.text.last?.containsEmoji ?? false) && (textView.text.last != "@") && (textView.text.last) != " "{
                        
                        let index = myText.index(myText.startIndex, offsetBy: cursorOffset)
                        let substring = myText[..<index]
                        if let lastword = substring.components(separatedBy: " ").last{
                            
                            if lastword != "@"{
                                if lastword.hasPrefix("@"){
                                    //Check complete word
                                    
                                    self.hideUsersList()

                                    let completeLastword = myText.components(separatedBy: " ").filter{$0.contains(lastword)}.last

                                    textView.text = myText.replacingOccurrences(of: completeLastword!, with: "")
                                    
                                    if text != "" || textView.text != AppConstants.Strings.ENTER_YOUR_COMMENT_HERE{
                                        textView.attributedText = resolveHashTags(text: textViewMention.text)
                                        textView.linkTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.cFF80EE]
                                    }
                                    self.removeUnwantedUsers()
                                    
                                    return false
                                }
                            }else{
                                self.hideUsersList()
                            }
                            
                        }
                    }
                }
                
                }
                
            }
        
        if textView.text.count == 0{
            if text == "@", text != "" {
                print(text)
                self.isMentionStarted = true
            }else if text == " "{
                self.isMentionStarted = false
            }
        }
        
        if text == " " || text == ""{
            self.isMentionStarted = false
        }
        print("current text:",text)
        
        if isMentionStarted{
            self.showUsersList()
        }else{
            self.hideUsersList()
        }
        
        return  numberOfChars < 250
    }
    
    func removeUnwantedUsers(){
        if usersInComment.count > 0{
            for i in 0..<usersInComment.count{
                if !(textViewMention.text.contains("@" + (usersInComment[i].username ?? ""))){
                    if usersInComment.count > 1{
                        usersInComment.removeLast()
                    }else{
                        usersInComment.remove(at: i)
                    }
                    break
                }
            }
        }
    }
    
    func hideUsersList(){
        self.mentionTableView.isHidden = true
        self.topSeperatorView.isHidden = true
    }
    
    func showUsersList(){
        self.mentionTableView.isHidden = false
        self.topSeperatorView.isHidden = false
    }
    
    @objc func reloadUsers() {
        if textViewMention.text.trimmingCharacters(in: .whitespacesAndNewlines) != "" && textViewMention.text.trimmingCharacters(in: .whitespacesAndNewlines) != AppConstants.Strings.ENTER_YOUR_COMMENT_HERE{
            self.getUsersListForMentions(search: self.searchUsernameString)
        }else{
            self.hideUsersList()
        }
    }
    
    public func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: AppConstants.Strings.ENTER_YOUR_COMMENT_HERE)
            attributeString.removeAttribute(NSAttributedString.Key.link, range: NSMakeRange(0, attributeString.length))
            attributeString.addAttributes([NSAttributedString.Key.foregroundColor : UIColor.cADADAD], range: NSMakeRange(0, attributeString.length))
            attributeString.addAttributes([NSAttributedString.Key.font : UIFont.fontRegular(size: 18)], range: NSMakeRange(0, attributeString.length))
            textView.attributedText = attributeString
        }
    }
    
    public func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == AppConstants.Strings.ENTER_YOUR_COMMENT_HERE {
            textView.text = nil
            textView.textColor = UIColor.cD3D3D3
            textView.font = UIFont.fontRegular(size: 18)
        }
    }
    
    func getUsersListForMentions(search:String){
        self.view.makeToastActivity(.center)
        feedViewModel.getUsersList(accessToken: getAccessToken(),search:search,page: 1  ,{(response) in
            if response.response?.statusCode == 200 || response.response?.statusCode == 201{
                let data: ApiResponse<MentionItem> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    let mentions = data.data!
                    
                    if self.mentionsHandler.mentionItems.count > 0{
                        self.mentionsHandler.mentionItems.removeAll()
                        self.mentionTableView.reloadData()
                    }
                    
                    if let mentionData = mentions.items{
                        self.mentionsHandler.mentionItems = mentionData
                        self.mentionTableView.reloadData()
                    }
                    self.view.hideToastActivity()
                }
            }else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.view.hideToastActivity()
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
}
