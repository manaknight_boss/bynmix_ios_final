import UIKit
import XLPagerTabStrip

class AllFeedViewController: BaseViewController,LoadMoreProtocol {
    
    func onLoadMore() {
        if !isApiCalled {
            isApiCalled = true
            getFeedAll()
        }
    }
    
    var feedViewModel = FeedViewModel()
    
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var tableViewLayout: UITableView!
    @IBOutlet var noDataShadowView: UIView!
    @IBOutlet var noDataView: UIView!
    @IBOutlet var noDataImageView: UIImageView!
    @IBOutlet var noDataLabel: UILabel!
    
    
    let feedAllHandler = FeedAllHandler()
    var feed:Feed? = nil
    var refreshControl:UIRefreshControl?
    var currentPage = 1
    var isApiCalled = false
    var dots: [Dots] = []
    let milliSeconds = 0.25
    let milliSecs = 0.60
    var searchText:String = ""
    var feedOfUser : [Feed] = []
    var feedOfAll : [Feed] = []
    var isFeedListCleared = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewLayout.rowHeight = UITableView.automaticDimension
        self.tableViewLayout.estimatedRowHeight = 477
        tableViewLayout.isHidden = true
        tableViewLayout.delegate = feedAllHandler
        tableViewLayout.dataSource = feedAllHandler
        getItemsClick()

        self.tableViewLayout.contentInset = UIEdgeInsets(top: 40, left: 0, bottom: 0, right: 0)
        addRefreshControl()
        feedAllHandler.loadMoreDelegate = self
        self.feedAllTableView = tableViewLayout
        hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        noDataShadowView.layer.cornerRadius = 2
        noDataShadowView.layer.shadowColor = UIColor.black.cgColor
        noDataShadowView.alpha = CGFloat(0.26)
        noDataShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        noDataShadowView.layer.shadowOpacity = 1
        noDataShadowView.layer.zPosition = -1
        
        searchTextOnTabChange()
        
        NotificationCenter.default.addObserver(self,selector: #selector(feedAllSearchText),name:Notification.Name(AppConstants.Notifications.FEED_ALL_BAR_BUTTON),object: nil)
    }
    
    func searchTextOnTabChange(){
        let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
        if selectedIndex == 0{
            searchText = AppConstants.SEARCH_TEXT_FOR_FEED
            if AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_FEED_ALL{
                if self.feedOfUser.count > 0{
                 self.feedOfUser.removeAll()
                }
            }
        }else if selectedIndex == 1{
            searchText = AppConstants.SEARCH_TEXT_FOR_BROWSE
            if AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_BROWSE_ALL{
                if self.feedOfAll.count > 0 {
                    self.feedOfAll.removeAll()
                }
            }
        }
        
            if self.searchText == ""{
                let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
                if selectedIndex == 0{
                    if  self.feedOfUser.count == 0{
                        self.showActivityIndicator()
                        self.currentPage = 1
                        self.getFeedAll()
                        self.searchButtonClickedForFeedAll()
                    }
                }else if selectedIndex == 1{
                    if self.feedOfAll.count == 0{
                        self.showActivityIndicator()
                        self.currentPage = 1
                        self.getFeedAll()
                        self.searchButtonClickedForBrowseAll()
                    }
                }
            }else{
                var filterData = FilterData()
                filterData.searchText = self.searchText
                let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
                if selectedIndex == 0{
                    if AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_FEED_ALL{
                        if  self.feedOfUser.count == 0{
                            self.showActivityIndicator()
                            self.currentPage = 1
                            self.getFeedAll(data:filterData)
                            self.searchButtonClickedForFeedAll()
                        }
                    }
                }else if selectedIndex == 1{
                    if AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_BROWSE_ALL{
                        if self.feedOfAll.count == 0{
                            self.showActivityIndicator()
                            self.currentPage = 1
                            self.getFeedAll(data:filterData)
                            self.searchButtonClickedForBrowseAll()
                        }
                    }
                }
            }
    }
    
    func showActivityIndicator(){
        if self.tableViewLayout.isHidden == false{
            self.view.makeToastActivity(.center)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(false)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: AppConstants.Notifications.FEED_ALL_BAR_BUTTON), object: nil)
    }
    
    @objc func feedAllSearchText(_ notification: Notification) {
        if feedAllHandler.feed.count > 0{
            feedAllHandler.feed.removeAll()
        }
        let barbuttonEnable:String = notification.userInfo?[AppConstants.Notifications.FEED_ALL_SEARCH_TEXT] as! String
        
        let searchText = barbuttonEnable
        self.searchText = searchText
        var filterData = FilterData()
        if searchText != ""{
            self.showLoader()
            currentPage = 1
            filterData.searchText = searchText
            
            self.getFeedAll(data:filterData)
            let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
            if selectedIndex == 0{
                self.searchButtonClickedForFeedAll()
            }else{
                self.searchButtonClickedForBrowseAll()
            }
        }else{
            self.showLoader()
            currentPage = 1
            self.getFeedAll()
            let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
            if selectedIndex == 0{
                self.searchButtonClickedForFeedAll()
            }else{
                self.searchButtonClickedForBrowseAll()
            }
        }
        
    }
    
    func showLoader(){
        self.view.makeToastActivity(.center)
    }
    
    func getItemsClick(){
        feedAllHandler.itemClickHandler = { feed in
            let storyboard = UIStoryboard(name: AppConstants.Storyboard.FOLLOW_FOLLOWING_BOARD, bundle: nil)
            let likeViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.FOLLOWERS_FOLLOWING) as! FollowersFollowingViewController
            if(feed.type == AppConstants.POST) {
                likeViewController.TYPE = AppConstants.TYPE_LIKE_POST
            } else {
                likeViewController.TYPE = AppConstants.TYPE_LIKE_LISTING
            }
            
            likeViewController.ID = feed.id
            self.navigationController?.pushViewController(likeViewController, animated: true)
        }
        feedAllHandler.onItemImageViewClick = { feed in
            if feed.type == AppConstants.POST {
                let storyboard = UIStoryboard(name: AppConstants.Storyboard.POST_DETAIL_BOARD, bundle: nil)
                let likeViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.POST_DETAIL_SCREEN) as! PostDetailViewController
                likeViewController.feed = feed
                self.navigationController?.pushViewController(likeViewController, animated: true)
            }else{
                let storyboard = UIStoryboard(name: AppConstants.Storyboard.LISTING_DETAIL_BOARD, bundle: nil)
                let listingDetailViewController = storyboard.instantiateViewController(withIdentifier:AppConstants.Controllers.Listing_Detail_Screen) as! ListDetailViewController
                listingDetailViewController.itemId = feed.id
                self.navigationController?.pushViewController(listingDetailViewController, animated: true)
            }
        }
        
        self.feedAllHandler.onHeartButtonClick = { feed in
            if feed.type == AppConstants.LISTING {
                self.onHeartClickButton(id: feed.id!)
            }else{
                self.onHeartPostClickButton(id: feed.id!)
            }
        }
        
        self.feedAllHandler.onOtherUserClick = { feed in
            if feed.userId == AppDefaults.getUserId(){
                //self.tabBarController?.selectedIndex = 4
                self.openUserProfile()
            }else{
                self.openProfile(id:feed.userId!,username:feed.username!)
            }
            //self.openProfile(id:feed.userId!,username:feed.username!)
        }
        
        self.feedAllHandler.onTagsButtonClick = { index, feed in
            self.onTagsButtonClick(index: index, feed: feed)
        }
        
        self.feedAllHandler.onDotClick = { dot in
            self.postDotType(dot: dot)
        }
        
        feedAllHandler.onHeartButtonClick = { feed in
            if feed.type == AppConstants.LISTING {
                self.onHeartClickButton(id: feed.id!)
            }else{
                self.onHeartPostClickButton(id: feed.id!)
            }
        }
        
        feedAllHandler.onOtherUserClick = { feed in
            if feed.userId == AppDefaults.getUserId(){
                //self.tabBarController?.selectedIndex = 4
                self.openUserProfile()
            }else{
                self.openProfile(id:feed.userId!,username:feed.username!)
            }
            //self.openProfile(id:feed.userId!,username:feed.username!)
        }
    }
    
    func postDotType(dot:Dots){
        if(dot.postDotTypeId! == AppConstants.TYPE_ID_MY_LISTING){
            self.listingScreen(listingId: dot.listingId!)
        }else{
            self.inAppWebBrowserScreen(dots: dot)
        }
    }
    
    func listingScreen(listingId:Int){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.LISTING_DETAIL_BOARD, bundle: nil)
        let listingDetailViewController = storyboard.instantiateViewController(withIdentifier:AppConstants.Controllers.Listing_Detail_Screen) as! ListDetailViewController
        listingDetailViewController.itemId = listingId
        self.navigationController?.pushViewController(listingDetailViewController, animated: true)
    }
    
    func inAppWebBrowserScreen(dots:Dots){
        self.inAppWebBrowserScreen(link: dots.link, title: dots.title)
    }
    
    func onTagsButtonClick(index: Int, feed:Feed){
        if feed.postDots != nil && feed.postDots!.count > 0 {
            var feedTemp = feed
            feedTemp.containDots = true
            var feeds = self.feedAllHandler.feed
            feeds[index] = feedTemp
            
            self.feedAllHandler.feed = feeds
            let position = IndexPath(row: index, section: 0)
            self.tableViewLayout.reloadRows(at: [position], with: .none)
        } else {
            self.view.makeToastActivity(.center)
            feedViewModel.getDots(accessToken: getTemporaryToken(), id: feed.id!, {(response) in
                self.view.hideToastActivity()
                if response.response?.statusCode == 200 {
                    let data: ApiResponse<[Dots]> = Utils.convertResponseToData(data: response.data!)
                    if data.isError {
                        let error = Utils.getErrorMessage(errors: data.errors)
                        self.showMessageDialog(message: error)
                    }else{
                        self.dots = data.data!
                        var feedTemp = feed
                        feedTemp.postDots = self.dots
                        feedTemp.containDots = true
                        var feeds = self.feedAllHandler.feed
                        feeds.remove(at: index)
                        feeds.insert(feedTemp, at: index)
                        self.feedAllHandler.feed = feeds
                        let position = IndexPath(row: index, section: 0)
                        self.tableViewLayout.reloadRows(at: [position], with: .none)
                        
                    }
                }else {
                    let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                    if data.isError {
                        let error = Utils.getErrorMessage(errors: data.errors)
                        print("error:",error)
                    }
                }
            } , { (error) in
                self.view.hideToastActivity()
                if error.statusCode == AppConstants.NO_INTERNET {
                    self.showInternetError()
                }
            })
        }
    }
    
    func getFeedAll(data:FilterData? = nil) {
        var filters: FilterData
        if data == nil {
            filters = FilterData()
        } else {
            filters = data!
        }
//        if self.feedAllHandler.feed.count > 0 && data != nil{
//            self.feedAllHandler.feed.removeAll()
//        }
        feedViewModel.getFeedAll(accessToken: getAccessToken(), data: filters,page:currentPage,{(response) in
            self.refreshControl?.endRefreshing()
            self.view.hideToastActivity()
            self.isApiCalled = false
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Feed> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print("error in 200",error)
                } else {
                    let feed = data.data
                    if var feeds = feed?.items {
                        for i in 0..<feeds.count{
                            feeds[i].isActive = true
                            feeds[i].containDots = false
                        }
                        if self.currentPage == 1 {
                            self.feedAllHandler.feed = feeds
                        } else {
                            self.feedAllHandler.feed.append(contentsOf: feeds)
                        }
                        let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
                        if selectedIndex == 0{
                            self.feedOfUser = feeds
                        }else{
                            self.feedOfAll = feeds
                        }
                        if feed?.totalPages == self.currentPage {
                            self.isApiCalled = true
                            self.feedAllHandler.isLoadMoreRequired = false
                        }
                        
                        //self.tableViewLayout.reloadData()
                        UIView.performWithoutAnimation {
                            self.tableViewLayout.reloadData()
                            self.tableViewLayout.beginUpdates()
                            self.tableViewLayout.endUpdates()
                        }
                        self.screenActivityIndicator.stopAnimating()
                        self.tableViewLayout.isHidden = false
                        self.noDataShadowView.isHidden = true
                        self.noDataView.isHidden = true
                    }
                }
                self.currentPage = self.currentPage + 1
                
            }else if response.response?.statusCode == 404 {
                self.screenActivityIndicator.stopAnimating()
                self.noDataShadowView.isHidden = false
                self.noDataView.isHidden = false
                if self.feedAllHandler.feed.count > 0{
                    self.feedAllHandler.feed.removeAll()
                }
                self.tableViewLayout.reloadData()
                self.noDataLabel.text = AppConstants.Strings.NOTHING_TO_SHOW
                self.noDataImageView.image = UIImage(named: AppConstants.Images.LOGO_BLACK)
                
            } else {
                self.screenActivityIndicator.stopAnimating()
                self.noDataView.isHidden = false
                self.noDataShadowView.isHidden = false
                if self.feedAllHandler.feed.count > 0{
                    self.feedAllHandler.feed.removeAll()
                }
                self.tableViewLayout.reloadData()
                self.noDataLabel.text = AppConstants.Strings.SOMETHING_WRONG_HERE
                self.noDataImageView.image = UIImage(named: AppConstants.Images.LOGO_SAD_CLOUD)
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print("error:",error)
                }
            }
            
            if filters.searchText != nil && filters.searchText != ""{
                self.trackSearchCalculate(userFilterData: filters)
            }
            
        } , { (error) in
            self.view.hideToastActivity()
            self.screenActivityIndicator.stopAnimating()
            self.refreshControl?.endRefreshing()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            } 
        })
    }
    
    func trackSearchCalculate(userFilterData:FilterData? = nil){
        var filter = FilterData()
        var trackSearchOfUser = TrackSearchOfUser()
        var jsonString = ""
        
        if trackSearchDictionary.count > 0{
            jsonString = convertIntoJSONString(arrayObject: trackSearchDictionary)!
            print("jsonString - \(jsonString)")
        }
        
        let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
        if selectedIndex == 0{
            trackSearchOfUser.searchBarLocation = AppConstants.TYPE_SEARCH_BAR_LOCATION_FEED_ALL
        }else{
            trackSearchOfUser.searchBarLocation = AppConstants.TYPE_SEARCH_BAR_LOCATION_BROWSE_ALL
        }
        
        if userFilterData != nil && userFilterData?.searchText != nil{
            filter = userFilterData!
            filter.searchText = userFilterData?.searchText ?? ""
            
            trackSearchOfUser.searchString = userFilterData?.searchText ?? ""
            if jsonString.count > 0{
                trackSearchOfUser.searchFilters = jsonString
            }
            self.trackSearch(trackSearchOfUser: trackSearchOfUser)
        }else if userFilterData != nil{
            filter = userFilterData!
            if jsonString.count > 0{
                trackSearchOfUser.searchFilters = jsonString
            }
            self.trackSearch(trackSearchOfUser: trackSearchOfUser)
        }else{
            filter.searchText = userFilterData?.searchText ?? ""
            trackSearchOfUser.searchString = userFilterData?.searchText ?? ""
            if userFilterData?.searchText?.count ?? 0 > 0{
                self.trackSearch(trackSearchOfUser: trackSearchOfUser)
            }
        }
    }
    
    func addRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        tableViewLayout.addSubview(refreshControl!)
    }
    @objc func refreshList(){
        currentPage = 1
        isApiCalled = false
        self.feedAllHandler.isLoadMoreRequired = true
        if self.searchText != ""{
            var filterData = FilterData()
            filterData.searchText = self.searchText
            self.getFeedAll(data:filterData)
        }else{
            self.getFeedAll()
        }
    }
    
    func onHeartClickButton(id:Int){
        feedViewModel.updateLikeStatus(accessToken: getTemporaryToken(), id: id, {(response) in
            if response.response?.statusCode == 200 {
                print("success")
            }else {
                print("something went wrong")
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func onHeartPostClickButton(id:Int){
        feedViewModel.updatePostLikeStatus(accessToken: getTemporaryToken(), id: id, {(response) in
            if response.response?.statusCode == 200 {
                print("success")
            }else{
                print("something went wrong")
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    
    func setDelegate(feedScroll: FeedScrollProtocol) {
        feedAllHandler.scrollDelegate = feedScroll
    }
    
}

extension AllFeedViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: AppConstants.Strings.ALL_TAB)
    }
}
