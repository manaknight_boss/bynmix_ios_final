import UIKit
import Alamofire
import Kingfisher

class BuyNowListingViewController: BaseViewController,OnCloseClickAddressOrPaymentProtocol,SelectExistingAddressProtocol,SelectCardProtocol,AddNewAddressProtocol,SetDefaultPaymentProtocol,AddAddressBuyNowProtocol,BuyNowReturnProtocol,SelectShipRateProtocol {
    
    func selectShipRate(shipRate:ShipRates,saleId:Int){
        self.navigationController?.popViewController(animated: false)
        self.shippingPriceLabel.text = "$" + String(shipRate.amount ?? 0)
        self.calculateInAppRateLabel.text = "$" + String(shipRate.amount ?? 0)
        self.calculateInAppPriorityLabel.text = shipRate.serviceLevelName ?? ""
        self.calculateInAppWeightLabel.text = shipRate.weightRangeDescription
        self.calculateInAppDeliveryInfoLabel.text = shipRate.duration ?? ""
        self.shippingRateObjectId = shipRate.objectId ?? ""
    }
    
    func buyNowReturn() {
        self.dismiss(animated: false, completion: nil)
        if !self.isScreenLoaded{
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    func addAddressBuyNow(address:Address){
        self.addAddress(address:address)
        self.checkIfBuyerOrSeller()
    }
    
    func setDefaultPayment(type: Int) {
        self.addPayment()
    }
    
    func addNewAddress(type: Int) {
        self.openAddressScreen()
    }
    
    func selectCard(type: Int, card: Card) {
        if type == AppConstants.TYPE_ADD_PAYMENT_IN_EXISTING_CARD{
            self.navigationController?.popViewController(animated: false)
        }
        self.addCard(card: card)
        self.checkIfBuyerOrSeller()
    }
    
    func SelectExistingAddress(type: Int, address: Address) {
        self.addAddress(address:address)
        if type > 1 {
            self.navigationController?.popViewController(animated: false)
        }
        self.checkIfBuyerOrSeller()
    }
    
    func onCloseClickAddressOrPayment() {
        self.dismiss(animated: false, completion: nil)
        var vcArray = self.navigationController?.viewControllers
        vcArray!.removeLast()
        self.navigationController?.setViewControllers(vcArray!, animated: false)
    }
    
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var listingNameLabel: UILabel!
    @IBOutlet weak var currentPriceLabel: UILabel!
    @IBOutlet weak var shippingPriceLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var addressOneLabel: UILabel!
    @IBOutlet var addressTwoLabel: UILabel!
    @IBOutlet var editAddressButton: UIButton!
    @IBOutlet var addressThreeLabel: UILabel!
    
    @IBOutlet var brandImageView: UIImageView!
    @IBOutlet var cardNameWithNumberLabel: UILabel!
    @IBOutlet var cardholderNameLabel: UILabel!
    @IBOutlet var expirationDateLabel: UILabel!
    
    @IBOutlet var bottomView: UIView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var editPaymentButton: UIButton!
    @IBOutlet weak var purchaseButton: UIButton!
    @IBOutlet weak var buttonActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet weak var buyItNowLabel: UILabel!
    @IBOutlet weak var currentPriceTextLabel: UILabel!
    @IBOutlet weak var totalChargeTextLabel: UILabel!
    
    @IBOutlet weak var shippingTextLabel: UILabel!
    
    @IBOutlet weak var paymentView: UIView!
    @IBOutlet var addressBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var shippingPriceTextLabel: UILabel!
    @IBOutlet var whatIsSaleTaxLabel: UILabel!
    @IBOutlet var pointsLabel: UILabel!
    @IBOutlet var salesTaxLabel: UILabel!
    @IBOutlet var promotionalDiscountLabel: UILabel!
    @IBOutlet var promotionalDiscountView: UIView!
    @IBOutlet var promotionDescView: UIView!
    @IBOutlet var promotionDescLabel: UILabel!
    
    @IBOutlet var pointsPromotionDescLabel: UILabel!
    @IBOutlet var pointsPromotionDescView: UIView!
    @IBOutlet var shippingPromotionDescView: UIView!
    @IBOutlet var shippingPromotionDescLabel: UILabel!
    @IBOutlet var salesTaxView: UIView!
    
    @IBOutlet var pointsTextLabel: UILabel!
    
    @IBOutlet weak var freeShippingView: UIView!
    @IBOutlet weak var flatRateShippingView: UIView!
    @IBOutlet weak var calculateInAppShippingView: UIView!
    
    @IBOutlet weak var flateRateShippingRateLabel: UILabel!
    @IBOutlet weak var flateRatePriorityLabel: UILabel!
    @IBOutlet weak var flateRateWeightLabel: UILabel!
    @IBOutlet weak var flatRateDeliveryInfoLabel: UILabel!
    
    @IBOutlet weak var calculateInAppRateLabel: UILabel!
    @IBOutlet weak var calculateInAppPriorityLabel: UILabel!
    @IBOutlet weak var calculateInAppWeightLabel: UILabel!
    @IBOutlet weak var calculateInAppDeliveryInfoLabel: UILabel!
    @IBOutlet weak var bestValueLabel: UILabel!
    
    @IBOutlet weak var cardView: UIView!
    
    var subOfferHistory:[SubOfferHistory]?
    
    var feedViewModel = FeedViewModel()
    var profileViewModel = ProfileViewModel()
    var listingDetail:ListingDetail?
    var allCards:[Card] = []
    var allAddresses:[Address] = []
    var card:Card?
    var address:Address?
    var alertsViewModel = AlertsViewModel()
    
    var offeredPrice:Int?
    
    var TYPE:Int?
    var enableToCalculateSalesTaxTrue = false
    var isScreenLoaded = false
    var shipRates:[ShipRates] = []
    var shippingRateObjectId:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.buttonActivityIndicator.isHidden = true
        onTypeChange()
        whatsThisLabelMakeAttributed()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        itemImageView.layer.borderWidth = 2.0
        itemImageView.layer.masksToBounds = false
        itemImageView.layer.borderColor = UIColor.white.cgColor
        itemImageView.clipsToBounds = true
        
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.alpha = CGFloat(0.26)
        shadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        shadowView.layer.shadowOpacity = 1
        shadowView.layer.zPosition = -1
    }
    
    func whatsThisLabelMakeAttributed(){
        let text:String = AppConstants.Strings.WHAT_IS_THIS
        let bracketStart:String = AppConstants.BRACKET_ON
        let bracketEnd:String = AppConstants.BRACKET_OFF
        let textBlue = AppConstants.Strings.WHAT_IS_THIS_TEXT
        let rangeBracketStart = (text as NSString).range(of: bracketStart)
        let rangeBracketEnd = (text as NSString).range(of: bracketEnd)
        let rangeTextBlue = (text as NSString).range(of: textBlue)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 10), range: rangeBracketStart)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 10), range: rangeBracketEnd)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 10), range: rangeTextBlue)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.c0001C2, range: rangeTextBlue)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: rangeTextBlue)
        self.whatIsSaleTaxLabel.attributedText = attributedString
    }
    
    override func viewDidLayoutSubviews() {
        if self.TYPE == AppConstants.TYPE_BUY_NOW{
            self.addressBottomConstraint.isActive = true
        }else{
            if listingDetail?.userId == AppDefaults.getUserId(){
                self.addressBottomConstraint.isActive = false
            }else{
                self.addressBottomConstraint.isActive = true
            }
        }
    }
    
    func onTypeChange(){
        if self.TYPE == AppConstants.TYPE_BUY_NOW{
            navigationItem.title = AppConstants.BUY_NOW_TITLE
            self.buyItNowLabel.text = AppConstants.Strings.BUY_IT_NOW
            self.currentPriceTextLabel.text = AppConstants.Strings.CURRENT_PRICE
            self.totalChargeTextLabel.text = AppConstants.Strings.TOTAL_CHARGE
            self.purchaseButton.setTitle(AppConstants.Strings.PURCHASE_BUTTON_TITLE, for: .normal)
            self.getDefaultCard()
            self.salesTaxView.isHidden = false
        }else{
            
            if listingDetail?.userId == AppDefaults.getUserId(){
                //counter offer
                self.totalChargeTextLabel.text = AppConstants.Strings.TOTAL_PROFITS_IF_ACCEPTED
                navigationItem.title = AppConstants.Strings.COUNTER_OFFER_TITLE
                self.buyItNowLabel.text = AppConstants.Strings.COUNTER_OFFER_FOR
                self.paymentView.isHidden = true
                self.shippingTextLabel.text = AppConstants.Strings.RETURN_ADDRESS
                self.shippingPriceTextLabel.text = AppConstants.Strings.MINUS_FEES
                self.getDefaultReturnAddress()
                self.salesTaxView.isHidden = true
            }else{
                //submit offer
                self.totalChargeTextLabel.text = AppConstants.Strings.TOTAL_CHARGE_IF_ACCEPTED
                navigationItem.title = AppConstants.Strings.MAKE_AN_OFFER_TITLE
                self.buyItNowLabel.text = AppConstants.Strings.MAKE_AN_OFFER_FOR
                self.getDefaultCard()
                self.salesTaxView.isHidden = false
            }
            view.layoutIfNeeded()
            self.currentPriceTextLabel.text = AppConstants.Strings.YOUR_OFFER
            self.purchaseButton.setTitle(AppConstants.Strings.SUBMIT_BUTTON_TITLE, for: .normal)
        }
    }
    
    func addCard(card:Card){
        self.card = card
        let image = UIImage(named: AppConstants.Images.DEFAULT_CARD_ICON)
        
        if let imageUrl = card.brandImageUrl{
            self.brandImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        }else{
            self.brandImageView.image = image
        }
        
        let brand = card.brand ?? ""
        let lastFour = card.lastFour ?? ""
        let expireMonth = card.expirationMonth ?? 0
        let expireYear = card.expirationYear ?? 0
        
        self.cardholderNameLabel.text = card.nameOnCard
        self.expirationDateLabel.text = AppConstants.EXPIRES + " " + String(expireMonth) + AppConstants.FORWARD_SLASH + String(expireYear)
        
        let text:String = brand + " " + AppConstants.Strings.ENDING_IN + " " +  lastFour
        let textBrand:String = card.brand!
        let textLastFour:String = card.lastFour!
        let range = (text as NSString).range(of: textLastFour)
        let rangeLastFour = (text as NSString).range(of: textBrand)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: range)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontBold(size: 16), range: rangeLastFour)
        self.cardNameWithNumberLabel.attributedText = attributedString
    }
    
    func getMyCard() {
        profileViewModel.getMyWalletCard(accessToken: AppDefaults.getAccessToken(), { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[Card]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let card = data.data! as [Card]
                    self.allCards = card
                    self.makeViewsVisible()
                }
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func onPurchaseClick(data:SubOfferHistory,id:Int) {
        self.buttonActivityIndicator.isHidden  = false
        self.buttonActivityIndicator.startAnimating()
        self.buttonActivityIndicator.hidesWhenStopped = true
        self.purchaseButton.isEnabled = false
        feedViewModel.purchaseListing(accessToken: AppDefaults.getAccessToken(), data: data, id: id, { (response) in
            if response.response?.statusCode == 200 || response.response?.statusCode == 201{
                let data: ApiResponse<[EmptyResponse]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.buttonActivityIndicator.stopAnimating()
                    self.purchaseButton.isEnabled = true
                    self.successMessageDialog(type: AppConstants.TYPE_LISTING_BUYED)
                    self.openMyPurchasesScreen(type: AppConstants.TYPE_PURCHASE)
                    
                }
            }else{
                let data: ApiResponse<[EmptyResponse]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print(error)
                }
                self.buttonActivityIndicator.stopAnimating()
                self.purchaseButton.isEnabled = true
            }
        } , { (error) in
            self.buttonActivityIndicator.stopAnimating()
            self.purchaseButton.isEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func successMessageDialog(type:Int){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.LISTING_BUYED_DIALOG_STORYBOARD, bundle: nil)
        let  inventoryTypeViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.LISTING_BUYED_DIALOG) as! ListingBuyedDialogViewController
        inventoryTypeViewController.itemTitle = self.listingDetail?.title!
        inventoryTypeViewController.modalPresentationStyle = .overFullScreen
        navigationController?.present(inventoryTypeViewController, animated: true,completion: nil)
    }
    
    func openMyPurchasesScreen(type:Int) {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.INVENTORY_TYPE_BOARD, bundle: nil)
        let  inventoryTypeViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.INVENTORY_TYPE_SCREEN) as! InventoryTypeViewController
        inventoryTypeViewController.TYPE = type
        
        var vcArray = self.navigationController?.viewControllers
        vcArray!.removeLast(2)
        vcArray!.append(inventoryTypeViewController)
        self.navigationController?.setViewControllers(vcArray!, animated: false)
    }
    
    func addAddress(address:Address){
        self.nameLabel.text = address.fullName
        self.addressOneLabel.text = address.addressOne
        self.addressTwoLabel.text = address.addressTwo
        let city = address.city ?? ""
        let state = address.stateAbbreviation ?? ""
        let zipcode:String = address.zipCode ?? ""
        let addressThree = city + " " + state + ", " + zipcode
        addressThreeLabel.text = addressThree
        self.address = address
    }
    
    func getDefaultCard() {
        feedViewModel.getDefaultCard(accessToken: getAccessToken(), {(response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Card> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let card = data.data
                    self.card = card
                    self.getDefaultAddress()
                }
            }else if response.response?.statusCode == 404{
                self.getDefaultAddress()
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print(error)
                }
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getDefaultAddress() {
        feedViewModel.getDefaultAddress(accessToken: getAccessToken(), {(response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Address> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let address = data.data
                    self.address = address
                    if address != nil && self.card != nil{
                        self.setData(listingDetail:self.listingDetail!,card:self.card!,address:address!)
                    }else{
                        self.openNoPaymentOrAddressDialog()
                    }
                }
            }else if response.response?.statusCode == 404{
                self.openNoPaymentOrAddressDialog()
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print(error)
                }
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getDefaultReturnAddress() {
        feedViewModel.getDefaultReturnAddress(accessToken: getAccessToken(), {(response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Address> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let address = data.data
                    self.address = address
                    self.setDataForCounterOffer(listingDetail:self.listingDetail!,address: address!)
                    
                }
            }else if response.response?.statusCode == 404{
                self.openNoPaymentOrAddressDialog()
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print(error)
                }
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func setData(listingDetail:ListingDetail,card:Card,address:Address){
        let image = UIImage(named: AppConstants.Images.DEFAULT_USER)!
        if let imageUrl = listingDetail.sliderImages![0].imageUrl{
            self.itemImageView.kf.setImage(with: URL(string:imageUrl), placeholder: image)
        }else{
            self.itemImageView.image = image
        }
        
        let listingTitle = "\"" + listingDetail.title! + "\""
        setText(text: listingTitle, listingDetail: listingDetail)
        if self.TYPE == AppConstants.TYPE_BUY_NOW{
            self.currentPriceLabel.text = AppConstants.DOLLAR + String(listingDetail.price!)
            self.getPromotionsBuyerEnd(id:listingDetail.id!,offer:Int(listingDetail.price!), listingDetail: listingDetail,offerId: nil,addressId: address.addressId!,cardId: card.cardId!)
        }else{
            let lastoffer =  subOfferHistory?.last
            let lastOfferId = lastoffer?.offerId
            self.currentPriceLabel.text = AppConstants.DOLLAR + String(self.offeredPrice!)
            self.getPromotionsBuyerEnd(id:listingDetail.id!,offer:Int(self.offeredPrice!), listingDetail: listingDetail,offerId: lastOfferId,addressId: address.addressId!,cardId: card.cardId!)
        }
        
        let defaultImage = UIImage(named: AppConstants.Images.DEFAULT_CARD_ICON)
        
        if let imageUrl = card.brandImageUrl{
            brandImageView.kf.setImage(with: URL(string: imageUrl),placeholder: defaultImage)
        }
        
        cardholderNameLabel.text = card.nameOnCard
        expirationDateLabel.text = AppConstants.EXPIRES + " " + String(card.expirationMonth!) + AppConstants.FORWARD_SLASH + String(card.expirationYear!)
        
        let text:String = card.brand! + " " + AppConstants.Strings.ENDING_IN + " " +  card.lastFour!
        let textBrand:String = card.brand!
        let textLastFour:String = card.lastFour!
        let range = (text as NSString).range(of: textLastFour)
        let rangeLastFour = (text as NSString).range(of: textBrand)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: range)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontBold(size: 16), range: rangeLastFour)
        
        cardNameWithNumberLabel.attributedText = attributedString
        
        nameLabel.text = address.fullName
        addressOneLabel.text = address.addressOne
        addressTwoLabel.text = address.addressTwo ?? ""
        let city = address.city ?? ""
        let state = address.stateAbbreviation ?? ""
        let zipcode:String = address.zipCode ?? ""
        let addressThree = city + " " + state + ", " + zipcode
        addressThreeLabel.text = addressThree
    
    }
    
    func setDataForCounterOffer(listingDetail:ListingDetail,address:Address){
        let image = UIImage(named: AppConstants.Images.DEFAULT_USER)!
        if let imageUrl = listingDetail.sliderImages![0].imageUrl{
            self.itemImageView.kf.setImage(with: URL(string:imageUrl), placeholder: image)
        }else{
            self.itemImageView.image = image
        }
        
        let listingTitle = "\"" + listingDetail.title! + "\""
        setText(text: listingTitle, listingDetail: listingDetail)
        
//        self.currentPriceLabel.text = AppConstants.DOLLAR + String(format:AppConstants.FORMAT_TWO_DECIMAL,self.offeredPrice!)
        let shippingRate = Int(listingDetail.shippingInfo?.rate ?? 0)
        let totalPrice = String((self.offeredPrice ?? 0) - shippingRate)
        self.totalPriceLabel.text = AppConstants.DOLLAR + totalPrice
        
        //self.shippingPriceLabel.text = AppConstants.DOLLAR + String(format:AppConstants.FORMAT_TWO_DECIMAL,(listingDetail.shippingInfo?.rate!)!)
        
        nameLabel.text = address.fullName
        addressOneLabel.text = address.addressOne
        addressTwoLabel.text = address.addressTwo ?? ""
        let city = address.city ?? ""
        let state = address.stateAbbreviation ?? ""
        let zipcode:String = address.zipCode ?? ""
        let addressThree = city + " " + state + ", " + zipcode
        addressThreeLabel.text = addressThree
        
        let lastoffer =  subOfferHistory?.last
        let lastOfferId = (lastoffer?.offerId!)!
        self.getPromotionsSellerEnd(id:listingDetail.id!,offerId:lastOfferId,counterOffer:Int(self.offeredPrice!),addressId: address.addressId!)
        
    }
    
    func makeViewsVisible(){
        self.screenActivityIndicator.stopAnimating()
        self.bottomView.isHidden = false
        self.scrollView.isHidden = false
        self.view.hideToastActivity()
        self.isScreenLoaded = true
    }
    
    func setText(text:String ,listingDetail:ListingDetail){
        let textRegular:String = "\(listingDetail.title ?? "")"
        let range = (text as NSString).range(of: textRegular)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cBB189C, range: range)
        listingNameLabel.attributedText = attributedString
    }
    
    func openNoPaymentOrAddressDialog() {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.ENABLE_DISABLE_DIALOG_BOARD, bundle: nil)
        let noPaymentOrAddressViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.ENABLE_DISABLE_LISTING_DIALOG) as! EnableDisableDialogViewController
        noPaymentOrAddressViewController.modalPresentationStyle = .overFullScreen
        noPaymentOrAddressViewController.modalTransitionStyle = .crossDissolve
        if self.address != nil{
            noPaymentOrAddressViewController.address = self.address
        }
        if self.card != nil{
            noPaymentOrAddressViewController.card = self.card
        }
        noPaymentOrAddressViewController.type = AppConstants.TYPE_CANNOT_MAKE_OFFER
        noPaymentOrAddressViewController.onCloseClickAddressOrPaymentDelegate = self
        noPaymentOrAddressViewController.addNewAddressDelegate = self
        noPaymentOrAddressViewController.setDefaultPaymentDelegate = self
        navigationController?.present(noPaymentOrAddressViewController, animated: false,completion: nil)
    }
    
    @IBAction func purchaseButtonAction(_ sender: UIButton) {
        
        if self.enableToCalculateSalesTaxTrue{
            self.showMessageDialog(message: "Either your address or payment is wrong. Please change it and try again.")
            return
        }
        
        if self.TYPE == AppConstants.TYPE_BUY_NOW{
            
            let id  = listingDetail?.id
            let addressId = self.address?.addressId!
            let cardId = self.card?.cardId!
            let listingTitle = self.listingDetail?.title!
            let sellerName = self.listingDetail?.username!
            
            var subOfferHistory : SubOfferHistory?
            
            if self.shippingRateObjectId == nil{
                subOfferHistory = SubOfferHistory(offerPrice: 0, addressId: addressId!, listingId: id!, listingTitle: listingTitle!, offerId: 0, sellerUsername: sellerName!, buyerUsername: "", cardId: cardId!)
            }else{
                subOfferHistory = SubOfferHistory(offerPrice: 0, addressId: addressId!, listingId: id!, listingTitle: listingTitle!, offerId: 0, sellerUsername: sellerName!, buyerUsername: "", cardId: cardId!,shippingRateObjectId: self.shippingRateObjectId ?? "")
            }
            
            self.onPurchaseClick(data: subOfferHistory!,id:id!)
            
        }else{
            
            let offerPrice = self.offeredPrice!
            let addressId = self.address?.addressId!
            let listingId = self.listingDetail?.id!
            let listingTitle = self.listingDetail?.title!
            let offerId = self.listingDetail?.offerId ?? 0
            let sellerName = self.listingDetail?.username ?? ""
            let buyerName = self.listingDetail?.buyerName ?? ""
            let cardId = self.card?.cardId!
            
            if listingDetail?.userId == AppDefaults.getUserId(){
                //counter offer api
                
                var subOfferHistory : SubOfferHistory?
                
                if self.shippingRateObjectId == nil{
                    subOfferHistory = SubOfferHistory(offerPrice: (offerPrice), addressId: addressId!, listingId: listingId!, listingTitle: listingTitle!, offerId: offerId, sellerUsername: sellerName, buyerUsername: buyerName)
                }else{
                    subOfferHistory = SubOfferHistory(offerPrice: (offerPrice), addressId: addressId!, listingId: listingId!, listingTitle: listingTitle!, offerId: offerId, sellerUsername: sellerName, buyerUsername: buyerName,shippingRateObjectId: self.shippingRateObjectId ?? "")
                }
                
                self.counterOffer(listingId: listingId!,offerId:offerId,subOfferHistory:subOfferHistory!, sellerName: buyerName)
            }else{
                //submit offer api
                
                var subOfferHistory : SubOfferHistory?
                
                if self.shippingRateObjectId == nil{
                    subOfferHistory = SubOfferHistory(offerPrice: (offerPrice), addressId: addressId!, listingId: listingId!, listingTitle: listingTitle!, offerId: 0, sellerUsername: sellerName, buyerUsername: buyerName, cardId: cardId!)
                }else{
                    subOfferHistory = SubOfferHistory(offerPrice: (offerPrice), addressId: addressId!, listingId: listingId!, listingTitle: listingTitle!, offerId: 0, sellerUsername: sellerName, buyerUsername: buyerName, cardId: cardId!,shippingRateObjectId: self.shippingRateObjectId ?? "")
                }
                
                self.submitOffer(id: listingId!,subOfferHistory: subOfferHistory!, sellerName: sellerName)
            }
            
        }
        
    }
    
    @IBAction func editAddressButtonAction(_ sender: UIButton) {
        selectAddress()
    }
    
    func selectAddress(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.SELECT_EXISTING_ADDRESS_BOARD, bundle: nil)
        let  existingAddressViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.SELECT_EXISTING_ADDRESS_SCREEN) as! SelectExistingAddressViewController
        existingAddressViewController.TYPE = AppConstants.TYPE_SELECT_DIFFERENT_ADDRESS
        existingAddressViewController.selectExistingAddressDelegate = self
        navigationController?.pushViewController(existingAddressViewController, animated: true)
    }
    
    @IBAction func editPaymentButtonAction(_ sender: UIButton) {
        selectCard()
    }
    
    func selectCard(){
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.SELECT_EXISTING_CARD_BOARD, bundle:nil)
        let selectCardViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.SELECT_EXISTING_CARD_SCREEN) as! SelectExistingCardViewController
        selectCardViewController.card = self.allCards
        selectCardViewController.selectCardDelegate = self
        self.navigationController?.pushViewController(selectCardViewController, animated:true)
    }
    
    func addPayment(){
        self.dismiss(animated: false, completion: nil)
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.ADD_NEW_PAYMENT_BOARD, bundle:nil)
        let editPaymentViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.ADD_NEW_PAYMENT_SCREEN) as! AddNewPaymentViewController
        editPaymentViewController.TYPE = AppConstants.TYPE_CANNOT_MAKE_OFFER
        self.navigationController?.pushViewController(editPaymentViewController, animated:true)
    }
    
    func openAddressScreen(){
        self.dismiss(animated: false, completion: nil)
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.ADDRESSES_BOARD, bundle: nil)
        let addressViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.ADDRESSES_SCREEN) as! AddressesViewController
        addressViewController.addAddressBuyNowDelegate = self
        navigationController?.pushViewController(addressViewController, animated: true)
    }
    
    func submitOffer(id:Int,subOfferHistory:SubOfferHistory,sellerName:String) {
        self.buttonActivityIndicator.isHidden = false
        self.buttonActivityIndicator.startAnimating()
        self.buttonActivityIndicator.hidesWhenStopped = true
        self.purchaseButton.isEnabled = false
        alertsViewModel.submitOffer(accessToken: getAccessToken(), id: id, data: subOfferHistory, {(response) in
            if response.response?.statusCode == 200 || response.response?.statusCode == 201{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.buttonActivityIndicator.stopAnimating()
                    self.purchaseButton.isEnabled = true
                    self.offerCompletedProcess(sellerName: sellerName)
                }
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.buttonActivityIndicator.stopAnimating()
                self.purchaseButton.isEnabled = true
            }
        } , { (error) in
            self.purchaseButton.isEnabled = true
            self.buttonActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func offerCompletedProcess(sellerName:String){
        if self.tabBarController?.selectedIndex == 3{
            self.navigationController!.popToRootViewController(animated: false)
        }
        AppConstants.TYPE_IS_ADDED_NEW_OFFER = true
        AppConstants.SELLER_NAME = sellerName
        Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(switchToDataTabCont), userInfo: nil, repeats: false)
    }
    
    @objc func switchToDataTabCont(){
        self.tabBarController?.selectedIndex = 3
    }
    
    func counterOffer(listingId:Int,offerId:Int,subOfferHistory:SubOfferHistory,sellerName:String) {
        self.buttonActivityIndicator.isHidden = false
        self.buttonActivityIndicator.startAnimating()
        self.buttonActivityIndicator.hidesWhenStopped = true
        self.purchaseButton.isEnabled = false
        alertsViewModel.counterOffer(accessToken: getAccessToken(), listingId: listingId, offerId: offerId, data: subOfferHistory, {(response) in
            if response.response?.statusCode == 200 || response.response?.statusCode == 201{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print(error)
                } else {
                    self.buttonActivityIndicator.stopAnimating()
                    self.purchaseButton.isEnabled = true
                    self.offerCompletedProcess(sellerName: sellerName)
                }
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.buttonActivityIndicator.stopAnimating()
                self.purchaseButton.isEnabled = true
            }
        } , { (error) in
            self.buttonActivityIndicator.stopAnimating()
            self.purchaseButton.isEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    @IBAction func salesTaxTapAction(_ sender: UITapGestureRecognizer) {
        saleTaxScreen()
    }
    
    func saleTaxScreen(){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.SELLER_POLICY_BOARD, bundle: nil)
        let saleTaxViewController  = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.SELLER_POLICY_SCREEN) as! SellerPolicyViewController
        saleTaxViewController.TYPE = AppConstants.TYPE_SALES_TAX
        self.navigationController?.pushViewController(saleTaxViewController, animated: false)
    }
    
    func getPromotionsBuyerEnd(id:Int,offer:Int,listingDetail:ListingDetail,offerId:Int?,addressId:Int,cardId:Int) {
        let offersCheck = OffersCheck(offerId: offerId, offer: offer, shippingAddressId: addressId, cardId: cardId)
        feedViewModel.getPromotionsBuyerEnd(accessToken: getAccessToken(), listingId: id,data: offersCheck, {(response) in
            self.handleResponse(response: response)
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getPromotionsSellerEnd(id:Int,offerId:Int,counterOffer:Int,addressId:Int) {
        let offersCheck = OffersCheck(offerId: offerId, counterOffer: counterOffer, ReturnAddressId: addressId)
        feedViewModel.getPromotionsSellerEnd(accessToken: getAccessToken(), listingId: id, data: offersCheck, {(response) in
            self.handleResponseForSellerEnd(response: response)
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func handleResponse(response:AFDataResponse<Any>){
        if response.response?.statusCode == 200 {
            let data: ApiResponse<CheckPromotions> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            } else {
                let promotionsStatus = data.data!
                self.enableToCalculateSalesTaxTrue = false
                self.setDataForBuyerEnd(promotionStatus:promotionsStatus)
            }
        } else if response.response?.statusCode == 400{
            self.enableToCalculateSalesTaxTrue = true
            self.screenActivityIndicator.stopAnimating()
            self.view.hideToastActivity()
            let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error,type: AppConstants.TYPE_POP_BUY_NOW_SCREEN)
            }
        } else {
            let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            }
            self.screenActivityIndicator.stopAnimating()
            self.view.hideToastActivity()
            self.enableToCalculateSalesTaxTrue = false
        }
    }
    
    func showMessageDialog(message: String,type:Int) {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.MESSAGE_DIALOG_BOARD, bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.MESSAGE_DIALOG) as! MessageDialogViewController
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        myAlert.message = message
        myAlert.TYPE = type
        myAlert.buyNowReturnDelegate = self
        self.present(myAlert, animated: true, completion: nil)
    }
    
    func handleResponseForSellerEnd(response:AFDataResponse<Any>){
        if response.response?.statusCode == 200 {
            let data: ApiResponse<CheckPromotionsSellerEnd> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                print( error)
            } else {
                let promotionsStatus = data.data!
                self.setDataForSellerEnd(promotionStatus: promotionsStatus)
            }
        } else {
            let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                print(error)
                self.showMessageDialog(message: error)
            }
            self.screenActivityIndicator.stopAnimating()
            self.view.hideToastActivity()
        }
    }
    
    func setDataForBuyerEnd(promotionStatus:CheckPromotions){
        self.totalPriceLabel.text = AppConstants.DOLLAR + String(format:AppConstants.FORMAT_TWO_DECIMAL, Double(promotionStatus.totalFinalAmount ?? 0))
        self.setDataForPromotions(promotionStatus:promotionStatus)
    }
    
    func setDataForSellerEnd(promotionStatus:CheckPromotionsSellerEnd){
        pointsTextLabel.text = AppConstants.Strings.AWARDED_POINTS
        pointsLabel.text = String(promotionStatus.pointsAwarded!)
        promotionalDiscountView.isHidden = true
        shippingPromotionDescView.isHidden = true
        salesTaxView.isHidden = true
        pointsPromotionDescView.isHidden = true
        promotionDescView.isHidden = true
        self.totalPriceLabel.text = AppConstants.DOLLAR + String(format:AppConstants.FORMAT_TWO_DECIMAL, Double(promotionStatus.totalFinalProfit ?? 0))
        self.currentPriceLabel.text = AppConstants.DOLLAR + String((promotionStatus.offerAmount ?? 0))
        self.shippingPriceLabel.text = AppConstants.DOLLAR + String(promotionStatus.commission ?? 0)
        
//        let shippingTypeId = promotionStatus.shippingTypeId ?? 0
//        let shipRates = promotionStatus.shipRates ?? []
        
        //self.checkShippingType(shippingTypeId:shippingTypeId,shipRates:shipRates)
        
        paymentView.isHidden  = true
        cardView.isHidden = true
        
        self.getMyCard()
    }
    
    func setDataForPromotions(promotionStatus:CheckPromotions){
        pointsTextLabel.text = AppConstants.Strings.ELIGIBLE_POINTS
        if promotionStatus.offerDetails?.hasCurrentPromotion ?? false{
            self.currentPriceLabel.textColor = UIColor.c00A200
            self.currentPriceLabel.font = UIFont.fontBoldItalic(size: 13)
            self.promotionalDiscountView.isHidden = false
            self.promotionalDiscountLabel.text = promotionStatus.offerDetails?.promotionDescription
        }else{
            self.currentPriceLabel.textColor = UIColor.c414141
            self.currentPriceLabel.font = UIFont.fontSemiBold(size: 13)
            self.promotionalDiscountView.isHidden = true
        }
        
        if promotionStatus.shippingDetails?.hasCurrentPromotion ?? false{
            self.shippingPriceLabel.textColor = UIColor.c00A200
            self.shippingPriceLabel.font = UIFont.fontBoldItalic(size: 13)
            self.shippingPromotionDescView.isHidden = false
            self.shippingPromotionDescLabel.text = promotionStatus.shippingDetails?.promotionDescription
            self.shippingPriceLabel.text = AppConstants.DOLLAR + String(promotionStatus.shippingDetails?.shippingPrice ?? 0)
        }else{
            self.shippingPriceLabel.textColor = UIColor.c414141
            self.shippingPriceLabel.font = UIFont.fontSemiBold(size: 13)
            self.shippingPromotionDescView.isHidden = true
            //self.shippingPriceLabel.text = AppConstants.DOLLAR + String(self.listingDetail?.shippingInfo?.rate ?? 0)
            self.shippingPriceLabel.text = AppConstants.DOLLAR + String(promotionStatus.shippingDetails?.shippingPrice ?? 0)
        }
        
        if promotionStatus.pointsDetails?.hasCurrentPromotion ?? false{
            self.pointsLabel.textColor = UIColor.c00A200
            self.pointsLabel.font = UIFont.fontBoldItalic(size: 13)
            self.pointsPromotionDescLabel.text = promotionStatus.pointsDetails?.promotionDescription
            self.pointsPromotionDescView.isHidden = false
        }else{
            self.pointsLabel.textColor = UIColor.c414141
            self.pointsLabel.font = UIFont.fontSemiBold(size: 13)
            self.pointsPromotionDescView.isHidden = true
        }
        self.pointsLabel.text = String(Int(promotionStatus.pointsDetails?.pointsAwarded ?? 0))
        
        if promotionStatus.promotionInfoDescription != nil && promotionStatus.promotionInfoDescription != ""{
            self.promotionDescView.isHidden = false
            self.promotionDescLabel.text = promotionStatus.promotionInfoDescription
        }else{
            self.promotionDescView.isHidden = true
        }
        
        let shippingTypeId = promotionStatus.shippingTypeId ?? 0
        let shipRates = promotionStatus.shipRates ?? []
        
        self.checkShippingType(shippingTypeId: shippingTypeId,shipRates:shipRates)
        
        self.salesTaxLabel.text = AppConstants.DOLLAR + String(format:AppConstants.FORMAT_TWO_DECIMAL,promotionStatus.salesTax!)
        self.getMyCard()
    }
    
    func checkShippingType(shippingTypeId:Int,shipRates:[ShipRates]){
        if shippingTypeId == AppConstants.SHIPPING_TYPE_FREE{
            self.freeShippingView.isHidden = false
            self.calculateInAppShippingView.isHidden = true
            self.flatRateShippingView.isHidden = true
        }else if shippingTypeId == AppConstants.SHIPPING_TYPE_FLATE_RATE{
            self.freeShippingView.isHidden = true
            self.calculateInAppShippingView.isHidden = true
            self.flatRateShippingView.isHidden = false
            self.flateRateShippingRateLabel.text =  "$" + String(shipRates[0].rate ?? 0)
            self.flateRatePriorityLabel.text = shipRates[0].serviceLevel ?? ""
            self.flateRateWeightLabel.text = shipRates[0].weightRangeDescription
            self.flatRateDeliveryInfoLabel.text = shipRates[0].durationDescription ?? ""
        }else{
            self.freeShippingView.isHidden = true
            self.calculateInAppShippingView.isHidden = false
            self.flatRateShippingView.isHidden = true
            
            for i in 0..<shipRates.count{
                if shipRates[i].bestRate ?? false{
                    self.calculateInAppRateLabel.text = "$" + String(shipRates[i].rate ?? 0)
                    self.calculateInAppPriorityLabel.text = shipRates[i].serviceLevel ?? ""
                    self.calculateInAppWeightLabel.text = shipRates[i].weightRangeDescription
                    self.calculateInAppDeliveryInfoLabel.text = shipRates[i].durationDescription ?? ""
                    self.shippingRateObjectId = shipRates[i].shippingRateObjectId ?? ""
                }
            }
            self.shipRates = shipRates
        }
        
    }
    
    func checkIfBuyerOrSeller(){
        self.view.makeToastActivity(.center)
        if listingDetail?.userId == AppDefaults.getUserId(){
            let lastoffer =  subOfferHistory?.last
            let lastOfferId = (lastoffer?.offerId!) ?? 0
            let listingId = (self.listingDetail?.id!) ?? 0
            let counterOffer = Int(self.offeredPrice ?? 0)
            let addressId = (self.address?.addressId ?? 0)
            self.getPromotionsSellerEnd(id: listingId, offerId: lastOfferId, counterOffer: counterOffer, addressId: addressId)
        }else{
            let listingId = (self.listingDetail?.id ?? 0)
            let price = Int(self.listingDetail?.price ?? 0)
            let addressId = (self.address?.addressId ?? 0)
            let cardId = (self.card?.cardId ?? 0)
            self.getPromotionsBuyerEnd(id:listingId,offer:price, listingDetail: self.listingDetail!,offerId: nil,addressId: addressId,cardId: cardId)
        }
    }
    
    
    @IBAction func editShippingButtonAction(_ sender: UIButton) {
        let userId = (self.address?.userId ?? 0)
        let addressId = (self.address?.addressId ?? 0)
        let listingId = self.listingDetail?.id ?? 0
        self.editShippingScreen(shipRates:self.shipRates,buyerId: userId,addressId: addressId, listingId: listingId)
    }
    
    func editShippingScreen(shipRates:[ShipRates],buyerId:Int,addressId:Int,listingId:Int){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.SELECT_SHIPPING_METHOD_STORYBOARD, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.SELECT_SHIPPING_METHOD_SCREEN) as! SelectShippingMethodViewController
        viewController.shipRates = shipRates
        viewController.buyerId = buyerId
        viewController.addressId = addressId
        viewController.listingId = listingId
        viewController.selectShipRateDelegate = self
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    
}
