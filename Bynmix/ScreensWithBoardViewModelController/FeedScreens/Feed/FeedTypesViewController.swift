import UIKit
import XLPagerTabStrip

class FeedTypesViewController: ButtonBarPagerTabStripViewController {
    
    override func viewDidLoad() {
        loadDesign()
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        //reloadPagerTabStripView()
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let tabAllFeedViewController = UIStoryboard(name: AppConstants.Storyboard.ALL_FEED_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.ALL_FEED_SCREEN) as! AllFeedViewController
        
        let tabListingsFeedViewController = UIStoryboard(name: AppConstants.Storyboard.LISTING_FEED_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.LISTINGS_FEED_SCREEN) as! ListingsFeedViewController
        
        let tabPostsFeedViewController = UIStoryboard(name: AppConstants.Storyboard.POSTS_FEED_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.POSTS_FEED_SCREEN) as! PostsFeedViewController
        
        return [tabAllFeedViewController,tabListingsFeedViewController,tabPostsFeedViewController]
    }
    
    override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool) {
        super.updateIndicator(for: viewController, fromIndex: fromIndex, toIndex: toIndex, withProgressPercentage: progressPercentage, indexWasChanged: indexWasChanged)
        
        if indexWasChanged {
            if progressPercentage != 1.0 {
                NotificationCenter.default.post(name: Notification.Name(AppConstants.Notifications.BAR_BUTTON),object: nil,userInfo:[AppConstants.Notifications.BAR_BUTTON_ENABLE: currentIndex])
                AppConstants.INDEX_OF_TAB = currentIndex
                print("currentIndex:",currentIndex)
            }
        }
    }
    
    func loadDesign() {
        self.settings.style.buttonBarBackgroundColor = UIColor.white
        self.settings.style.buttonBarItemBackgroundColor = UIColor.white
        self.settings.style.selectedBarBackgroundColor = UIColor.cFB9D52
        self.settings.style.buttonBarItemFont = UIFont.fontSemiBold(size: 12)
        self.settings.style.selectedBarHeight = 2.0
        self.settings.style.buttonBarMinimumLineSpacing = 0
        self.settings.style.buttonBarItemTitleColor = UIColor.black
        self.settings.style.buttonBarItemsShouldFillAvailableWidth = true
        self.settings.style.buttonBarLeftContentInset = 10
        self.settings.style.buttonBarRightContentInset = 10
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            
            oldCell?.label.textColor = UIColor.c2A2A2A
            oldCell?.label.font = UIFont.fontSemiBold(size: 12)
            newCell?.label.textColor = UIColor.cBB189C
            newCell?.label.font = UIFont.fontSemiBold(size: 12)
            
        }
    }
    
    func setDelegate(feedScroll: FeedScrollProtocol) {
        let allTabs = self.viewControllers
        let tabAllFeedViewController = allTabs[0] as! AllFeedViewController
        tabAllFeedViewController.setDelegate(feedScroll: feedScroll)
    }
    
    func setDelegateListing(feedScroll: FeedScrollProtocol) {
        let allTabs = self.viewControllers
        let tabListingsFeedViewController = allTabs[1] as! ListingsFeedViewController
        tabListingsFeedViewController.setDelegate(feedScroll: feedScroll)
    }
    
    func setDelegatePosts(feedScroll: FeedScrollProtocol) {
        let allTabs = self.viewControllers
        let tabPostsFeedViewController = allTabs[2] as! PostsFeedViewController
        tabPostsFeedViewController.setDelegate(feedScroll: feedScroll)
    }
    
}
