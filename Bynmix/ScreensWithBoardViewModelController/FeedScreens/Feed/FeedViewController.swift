import UIKit
import Alamofire
import XLPagerTabStrip

class FeedViewController: BaseViewController, FeedScrollProtocol,UserFilterClearAllProtocol,UserFilterApplyProtocol, UISearchBarDelegate {
    
    func userFilterClearAll() {
        AppConstants.clearFilterList = true
        NotificationCenter.default.post(name: Notification.Name(AppConstants.Notifications.FEED_CLEAR_ALL_FILTER_BUTTON),object: nil,userInfo:[AppConstants.Notifications.FEED_CLEAR_FILTER_LIST: AppConstants.clearFilterList])
        self.navigationController?.popViewController(animated: false)
    }
    
    func userFilterApply(selectedFilter: [SelectedFilter], heightModal: HeightModel) {
        NotificationCenter.default.post(name: Notification.Name(AppConstants.Notifications.FILTER_LISTING_BAR_BUTTON),object: nil,userInfo:[AppConstants.Notifications.SELECTED_FILTER_LIST: selectedFilter])
        self.navigationController?.popViewController(animated: false)
    }
    
    func onScroll(scrollValue: CGFloat) {
        self.scrollValue = scrollValue
        if scrollValue < -self.addMorePeopleView.frame.height{
            topConstraint.constant = 0
        } else if scrollValue <= self.collectionViewLayout.frame.height {
            let value = scrollValue + self.addMorePeopleView.frame.height
            topConstraint.constant = -value
        } else {
            topConstraint.constant = -(self.addMorePeopleView.frame.height+self.collectionViewLayout.frame.height)
        }
        
    }
    
    @IBOutlet var addMorePeopleView: UIView!
    @IBOutlet var collectionViewLayout: UICollectionView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var listingsFilterBarButton: UIBarButtonItem!
    @IBOutlet var containerView: UIView!
    @IBOutlet var followStatusLabel: UILabel!
    @IBOutlet var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet var welcomeView: UIView!
    @IBOutlet var welcomViewLabel: UILabel!
    @IBOutlet var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var promotionsTableView: UITableView!
    @IBOutlet weak var promotionsTableViewHeight: NSLayoutConstraint!
    
    var searchBar:UISearchBar?
    let feedViewModel = FeedViewModel()
    let feedsHandler = FeedsHandler()
    var user = [User]()
    var followFollowingUser = [User]()
    var checkUser:Int?
    let milliSeconds = 0.25
    var promotionsHandler = PromotionsHandler()
    var scrollValue: CGFloat = 0
    var checkIfApiIsCalled = false
    var notFollowedAnyone:Bool = false
    var isUserOnAllTab = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + milliSeconds) {
            self.getUsers()
            if !AppConstants.isPromotionsAllListLoaded{
                self.getAllPromotions()
            }
        }
        scrollView.delegate = self
        makeSearchBar()
        collectionViewLayout.delegate = feedsHandler
        collectionViewLayout.dataSource = feedsHandler
        navigationItem.rightBarButtonItem = nil
        NotificationCenter.default.addObserver(self,selector: #selector(onTabChange),name:Notification.Name(AppConstants.Notifications.BAR_BUTTON),object: nil)
        welcomeViewStatus()
        getItemClick()
        hideKeyboardWhenTappedAroundForSearchBar()
        promotionsTableView.delegate = promotionsHandler
        promotionsTableView.dataSource = promotionsHandler
        getPromotionsClick()
        AppConstants.INDEX_OF_TAB = 0
        promotionsTableView.rowHeight = UITableView.automaticDimension
        promotionsTableView.estimatedRowHeight = 60
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + milliSeconds) {
            if AppConstants.isPromotionsAllListLoaded{
                self.getPromotions()
            }
        }
    }
    
    func getPromotionsClick(){
        promotionsHandler.onCancelButtonClick = { promotions,index in
            self.promotionsHandler.promotions.remove(at: index)
            self.promotionsTableView?.reloadData()
            let promotionId = promotions.promotionId!
            
            AppDefaults.setPromotionId(promotionId: promotionId)
            print(AppDefaults.getPromotionIds())
            self.resizePromotionsTableView(promotions: self.promotionsHandler.promotions)
        }
    }
    
    func hideKeyboardWhenTappedAroundForSearchBar() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboardForSearchBar))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboardForSearchBar() {
        self.searchBar?.endEditing(true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        callNotifications(searchText: searchBar.text ?? "")
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton = false
        searchBar.endEditing(true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.count == 0{
            callNotifications(searchText: searchText )
            searchBar.resignFirstResponder()
            let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
            if selectedIndex == 0{
                searchButtonForFeed()
            }else{
                searchButtonForBrowse()
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)  {
        if searchBar.text != ""{
            callNotifications(searchText: searchBar.text ?? "")
            searchBar.resignFirstResponder()
            let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
            if selectedIndex == 0{
                searchButtonForFeed()
            }else{
                searchButtonForBrowse()
            }
        }
        if checkScrollView(scrollValue: self.scrollValue){
            self.topConstraint.constant = 0
        }
    }
    
    func checkScrollView(scrollValue: CGFloat) -> Bool{
        var value = false
        if scrollValue > (self.addMorePeopleView.frame.height+self.collectionViewLayout.frame.height){
            value = true
        }
        return value
    }
    
    func searchButtonForFeed(){
        AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_FEED_ALL = true
        AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_FEED_LISTING = true
        AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_FEED_POST = true
    }
    
    func searchButtonForBrowse(){
        AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_BROWSE_ALL = true
        AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_BROWSE_LISTING = true
        AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_BROWSE_POST = true
    }
    
    func callNotifications(searchText:String){
        let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
        if selectedIndex == 0{
            AppConstants.SEARCH_TEXT_FOR_FEED = searchText
        }else{
            AppConstants.SEARCH_TEXT_FOR_BROWSE = searchText
        }
        NotificationCenter.default.post(name: Notification.Name(AppConstants.Notifications.FEED_POSTS_BAR_BUTTON),object: nil,userInfo:[AppConstants.Notifications.FEED_POSTS_SEARCH_TEXT: searchText])
        NotificationCenter.default.post(name: Notification.Name(AppConstants.Notifications.FEED_LISTINGS_BAR_BUTTON),object: nil,userInfo:[AppConstants.Notifications.FEED_LISTINGS_SEARCH_TEXT: searchText])
        NotificationCenter.default.post(name: Notification.Name(AppConstants.Notifications.FEED_ALL_BAR_BUTTON),object: nil,userInfo:[AppConstants.Notifications.FEED_ALL_SEARCH_TEXT: searchText])
    }
    
    func getItemClick(){
        
        feedsHandler.onOtherUserClick = { user in
            let storyboard = UIStoryboard(name: AppConstants.Storyboard.OTHER_USER_PROFILE_BOARD, bundle: nil)
            let otherUserViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.OTHER_USER_PROFILE) as! OtherUserProfileViewController
            otherUserViewController.otherUserId = user.userId
            otherUserViewController.USERNAME = user.username
            self.navigationController?.pushViewController(otherUserViewController, animated: true)
        }
        
        feedsHandler.onMoreUserClick = {
            if self.isUserOnAllTab{
                self.followFollowingScreen(type: AppConstants.TYPE_SEARCH_USERS, username: "")
            }else{
                self.followFollowingScreen(type: AppConstants.TYPE_FOLLOWING, username: AppDefaults.getUsername())
            }
        }
    }
    
    func followFollowingScreen(type:Int,username:String){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.FOLLOW_FOLLOWING_BOARD, bundle: nil)
        let followFollowingViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.FOLLOWERS_FOLLOWING) as! FollowersFollowingViewController
        followFollowingViewController.TYPE = type
        followFollowingViewController.USERNAME = username
        self.navigationController?.pushViewController(followFollowingViewController, animated: true)
    }
    
    func welcomeViewStatus(){
        let showWelcomeView =  AppDefaults.getWelcomeView()
        if showWelcomeView {
            welcomeView.isHidden = false
            welcomViewLabel.text = AppConstants.WELCOME_VIEW_TEXT
            AppDefaults.setWelcomeView(welocmeView: false)
        } else {
            welcomeView.isHidden = true
        }
    }
    
    @objc func onTabChange(_ notification: Notification) {
        let barbuttonEnable:NSNumber = notification.userInfo?[AppConstants.Notifications.BAR_BUTTON_ENABLE] as! NSNumber
        let isProfile = notification.userInfo?[AppConstants.Notifications.IS_PROFILE]
        if barbuttonEnable == 0 {
            if isProfile == nil {
                if user.count ==  0 {
                    self.getUsers()
                }else{
                    self.collectionViewLayout.isHidden = false
                    self.feedsHandler.users = self.user
                    self.collectionViewLayout.reloadData()
                }
                self.checkFollowStatus()
                self.collectionViewHeight.constant = 90
            }
            navigationItem.rightBarButtonItem = nil
            self.isUserOnAllTab = true
            
        } else if barbuttonEnable == 1 {
            if isProfile == nil {
                if followFollowingUser.count == 0 && !self.notFollowedAnyone{
                    self.getFollowFollowingUsers()
                }else if followFollowingUser.count > 0{
                    self.feedsHandler.users = self.followFollowingUser
                    self.collectionViewLayout.isHidden = false
                    self.collectionViewLayout.reloadData()
                }
                
                self.checkIfUserFollowing()
            }
            navigationItem.rightBarButtonItem = listingsFilterBarButton
            self.isUserOnAllTab = false
            
        } else {
            if followFollowingUser.count == 0 && !self.notFollowedAnyone{
                    self.getFollowFollowingUsers()
            }else if followFollowingUser.count > 0{
                self.feedsHandler.users = self.followFollowingUser
                self.collectionViewLayout.isHidden = false
                self.collectionViewLayout.reloadData()
            }
            navigationItem.rightBarButtonItem = nil
            self.isUserOnAllTab = false
            self.checkIfUserFollowing()
        }
        self.addMorePeopleView.isHidden = false
    }
    
    func checkFollowStatus(){
        if self.user.count > 0{
            followStatusLabel.text = AppConstants.Strings.ADD_MORE_PEOPLE_TO_FOLLOW
        }else{
            followStatusLabel.text = AppConstants.Strings.NOT_FOLLOWED_ANYONE
        }
    }
    
    func checkIfUserFollowing(){
        if self.checkIfApiIsCalled{
            if self.followFollowingUser.count > 0{
                followStatusLabel.text = AppConstants.Strings.PEOPLE_YOU_FOLLOW_WHAT_NEW_WITH_THEM
                self.collectionViewHeight.constant = 90
            }else{
                followStatusLabel.text = AppConstants.Strings.NOT_FOLLOWED_ANYONE
                self.collectionViewHeight.constant = 0
            }
        }else{
            followStatusLabel.text = AppConstants.Strings.PEOPLE_YOU_FOLLOW_WHAT_NEW_WITH_THEM
            self.collectionViewHeight.constant = 90
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let childViewController = segue.destination as? FeedTypesViewController {
            childViewController.view.translatesAutoresizingMaskIntoConstraints = true
            childViewController.setDelegate(feedScroll: self)
            childViewController.setDelegateListing(feedScroll: self)
            childViewController.setDelegatePosts(feedScroll: self)
        }
    }
    
    func makeSearchBar() {
        searchBar = UISearchBar()
        searchBar!.sizeToFit()
        if let textfield = searchBar?.value(forKey: "searchField") as? UITextField {
            //textfield.borderStyle = .line
            textfield.backgroundColor = UIColor.c343434
            textfield.attributedPlaceholder = NSAttributedString(string: AppConstants.Strings.SEARCH_BYNMIX,
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.c696969])
            textfield.layer.cornerRadius = 16
            textfield.clipsToBounds = true
            textfield.font = UIFont.fontRegular(size: 16)
            if let leftView = textfield.leftView as? UIImageView {
                leftView.image = leftView.image?.withRenderingMode(.alwaysTemplate)
                leftView.tintColor = UIColor.cB5B5B5
            }
            
        }
        searchBar?.placeholder = AppConstants.Strings.SEARCH_BYNMIX
        searchBar?.delegate = self
        
        let searchBarTextAttributes = [
            NSAttributedString.Key.font: UIFont.fontRegular(size: 16),NSAttributedString.Key.foregroundColor: UIColor.cD3D3D3
        ]
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = searchBarTextAttributes
        
        navigationItem.titleView = searchBar
    }
    
    func getUsers() {
        checkUser = 1
        feedViewModel.getFeedUsers(accessToken: getAccessToken(), { (response) in
            self.handleResponse(response: response,checkUser : self.checkUser!)
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getFollowFollowingUsers() {
        checkUser = 2
        feedViewModel.getFollowFollowingUsers(accessToken: getAccessToken(), { (response) in
            self.handleResponse(response: response, checkUser: self.checkUser!)
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func handleResponse(response: AFDataResponse<Any>,checkUser:Int) {
        if response.response?.statusCode == 200 {
            let data: ApiResponse<User> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                print(error)
            } else {
                let userdata = data.data
                if let users = userdata?.items {
                    
                    if users.count > 0{
                        if checkUser == 1{
                            self.user = users
                        }else if checkUser == 2{
                            self.followFollowingUser = users
                        }
                        self.collectionViewHeight.constant = 90
                        self.collectionViewLayout.isHidden = false
                        self.addMorePeopleView.isHidden = false
                        if checkUser == 1{
                            self.checkFollowStatus()
                        }else if checkUser == 2{
                            self.checkIfUserFollowing()
                        }
                        self.feedsHandler.users = users
                    }else{
                        if checkUser == 2{
                            self.notFollowedAnyone = true
                        }
                        self.collectionViewHeight.constant = 0
                        self.collectionViewLayout.isHidden = true
                        followStatusLabel.text = AppConstants.Strings.NOT_FOLLOWED_ANYONE
                    }
                    self.collectionViewLayout.reloadData()
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + milliSeconds) {
                    if self.tabBarController?.selectedIndex == 0{
                        if self.isAlertsApiCalled{
                            self.getAllAlerts()
                        }
                    }
                }
            }
        }else if response.response?.statusCode == 404 {
            self.collectionViewLayout.isHidden = true
            self.collectionViewHeight.constant = 0
            followStatusLabel.text = AppConstants.Strings.YOU_FOLLOWED_EVERYONE
            
            if checkUser == 2{
                self.notFollowedAnyone = true
            }
        }else {
            print("error")
        }
        if checkUser == 2{
            self.checkIfApiIsCalled = true
        }
    }
    
    @IBAction func onFilterListingsClick(_ sender: UIBarButtonItem) {
        self.filterListing()
    }
    
    func filterListing(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.LISTING_FILTER_STORYBOARD, bundle: nil)
        let filterListingViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.LISTING_FILTER_SCREEN) as! FilterListingsViewController
        filterListingViewController.filterApplyDelegate = self
        filterListingViewController.filterClearAllDelegate = self
        self.navigationController?.pushViewController(filterListingViewController, animated: false)
    }
    
    @IBAction func onWelcomeViewCloseClick(_ sender: UIButton) {
        welcomeView.isHidden = true
    }
    
    @IBAction func userFilterBarButtonAction(_ sender: UIBarButtonItem) {
        self.searchUsersScreen()
    }
    
    func getPromotions() {
        let promotions = AppConstants.PROMOTIONS_ALL_LIST
        var promotionsNew : [Promotions] = []
        if AppDefaults.getPromotionIds().count > 0{
            print("promotions:",AppDefaults.getPromotionIds())
            for i in 0..<promotions.count{
                if  !AppDefaults.getPromotionIds().contains(promotions[i].promotionId!){
                    promotionsNew.append(promotions[i])
                }
            }
            self.promotionsHandler.promotions = promotionsNew
        }else{
            self.promotionsHandler.promotions = promotions
        }
        self.promotionsTableView.reloadData()
        self.promotionsTableView.isHidden = false
        self.resizePromotionsTableView(promotions: promotions)
        
    }
    
    func resizePromotionsTableView(promotions:[Promotions]){
        if promotions.count == 0{
            promotionsTableViewHeight.constant = 0
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                var tableViewHeight: CGFloat {
                    self.promotionsTableView.layoutIfNeeded()
                    return self.promotionsTableView.contentSize.height
                }
                self.promotionsTableViewHeight?.constant = tableViewHeight
            }
        }
    }
    
}

