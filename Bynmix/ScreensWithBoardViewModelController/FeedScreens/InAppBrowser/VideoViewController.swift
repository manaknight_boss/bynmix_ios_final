import UIKit
import WebKit

class VideoViewController: BaseViewController,WKNavigationDelegate,UINavigationBarDelegate {

    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var webView: WKWebView!
    @IBOutlet var navBar: UINavigationBar!
    @IBOutlet var backButton: UIBarButtonItem!
    
    var videoLink:String?
    var linkTitle:String?

    override func viewDidLoad() {
        super.viewDidLoad()
        var link:String?
        if !(videoLink!.hasPrefix(AppConstants.HTTP) || videoLink!.hasPrefix(AppConstants.HTTPS)){
            link = AppConstants.HTTP + (videoLink ?? AppConstants.Strings.BYNMIX_SUPPORT_LINK)
        }else{
            link =  videoLink ?? AppConstants.Strings.BYNMIX_SUPPORT_LINK
        }
        if verifyUrl(urlString: link){
            let validURL = URL(string: link ?? AppConstants.Strings.BYNMIX_SUPPORT_LINK)!
            webView.navigationDelegate = self
            webView.load(URLRequest(url: validURL))
        }else{
            self.view.makeToast(AppConstants.Strings.LINK_NOT_VALID)
            screenActivityIndicator.stopAnimating()
        }
        setNavigationBar()
    }
    
    func setNavigationBar(){
        navBar.barStyle = .black
        navBar.barTintColor = .black
        navBar.topItem?.title = linkTitle ?? AppConstants.Strings.BYNMIX_TITLE
        navBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.fontRegular(size: 16)]
    }
    
    @objc func backBarButton(){
        self.dismiss(animated: false, completion: nil)
    }
    
    func verifyUrl (urlString: String?) -> Bool {
       let urlRegEx = "^(https?://)?(www\\.)?([-a-z0-9]{1,63}\\.)*?[a-z0-9][-a-z0-9]{0,61}[a-z0-9]\\.[a-z]{2,6}(/[-\\w@\\+\\.~#\\?&/=%]*)?$"
       let urlTest = NSPredicate(format:"SELF MATCHES %@", urlRegEx)
       let result = urlTest.evaluate(with: urlString)
       return result
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        screenActivityIndicator.stopAnimating()
    }
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        screenActivityIndicator.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        screenActivityIndicator.stopAnimating()
    }
    
    @IBAction func backButtonAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
}
