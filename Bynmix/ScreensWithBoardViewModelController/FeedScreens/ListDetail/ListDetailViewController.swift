import UIKit
import Alamofire
import AVKit

class ListDetailViewController: BaseViewController,AddCommentProtocol,ReportAnIssueUserProtocol,OnCloseClickAddressOrPaymentProtocol,AddNewAddressProtocol,SetDefaultPaymentProtocol,AddAddressBuyNowProtocol,CommentsIdProtocol {
    
    func commentsId(id:Int){
        if id == AppDefaults.getUserId(){
            self.openUserProfile()
        }else{
            self.openProfile(id:id,username:"")
        }
    }
    
    func addAddressBuyNow(address: Address) {
        //self.addAddress(address:address)
    }
    
    func setDefaultPayment(type: Int) {
        self.addPayment()
    }
    
    func addNewAddress(type: Int) {
        self.openAddressScreen()
    }
    
    func onCloseClickAddressOrPayment() {
        self.view.hideToastActivity()
        self.dismiss(animated: false, completion: nil)
    }
    
    func reportAnIssueUser() {
        self.navigationController?.popViewController(animated: false)
        self.reportAnIssueSuccessMessageDialog()
    }
    
    func addComment(listingDetail:ListingDetail,Comment:Comments){
        self.navigationController?.popViewController(animated: false)
        self.view.makeToastActivity(.center)
        let listingId = listingDetail.id!
        self.addComment(id:listingId,comment:Comment)
    }
    
    @IBOutlet var userImageView: UIImageView!
    @IBOutlet var userShadowView: UIView!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var shareButton: UIButton!
    @IBOutlet var heartButton: UIButton!
    @IBOutlet var reportIssueButton: UIButton!
    @IBOutlet var itemTitleLabel: UILabel!
    @IBOutlet var itemSizelabel: UILabel!
    @IBOutlet var itemDescriptionLabel: UILabel!
    @IBOutlet var itemPriceLabel: UILabel!
    @IBOutlet var itemSizeTypeLabel: UILabel!
    @IBOutlet var brandNameLabel: UILabel!
    @IBOutlet var colorLabel: UILabel!
    @IBOutlet var otherColorLabel: UILabel!
    @IBOutlet var categoryLabel: UILabel!
    @IBOutlet var otherCategoryLabel: UILabel!
    @IBOutlet var shippingDetailLabel: UILabel!
    @IBOutlet var commentsTableView: UITableView!
    @IBOutlet var sliderScrollView: UIScrollView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var previousButton: UIButton!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var sliderCollectionView: UICollectionView!
    @IBOutlet var mainScreenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var mainScreenScrollView: UIScrollView!
    @IBOutlet var colorView: UIView!
    @IBOutlet var anotherColorView: UIView!
    @IBOutlet var sizeBorderView: UIView!
    @IBOutlet var sizeTypeBorderView: UIView!
    @IBOutlet var brandBorderView: UIView!
    @IBOutlet var otherColorView: UIView!
    @IBOutlet var categoryBorderView: UIView!
    @IBOutlet var categoryTypeBorderView: UIView!
    @IBOutlet var likesLabel: UILabel!
    @IBOutlet var userHeartButton: UIButton!
    @IBOutlet var colorCodeView: UIView!
    @IBOutlet var commentsTableViewHeight: NSLayoutConstraint!
    @IBOutlet var likesView: UIView!
    @IBOutlet var priceLabel: UILabel!
    @IBOutlet var offerLabel: UILabel!
    @IBOutlet var statusView: UIView!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var listingPriceStatusView: UIView!
    @IBOutlet var userLikedView: UIView!
    
    @IBOutlet var videoPlayButton:UIButton!
    @IBOutlet var durationView: TrimmerView!
    @IBOutlet var statusShadowView: UIView!
        
    var frame = CGRect(x: 0, y: 0, width: 0, height: 0)
    let sliderHandler = SliderHeaderHandler()
    var heartBarButton:UIBarButtonItem?
    let feedViewModel = FeedViewModel()
    var listingDetail:ListingDetail?
    var itemId:Int?
    var likesCount:Int?
    var onReplyClick:Bool = false
    var isCommentAdded:Bool = false
    let seconds = 0.25
    var previousListingDetail:ListingDetail?
    var comments = [Comments]()
    var originalusersList = [MentionItem]()
    var userNameList :[String] = []
    var card:Card?
    var address:Address?
    var listingUserId:Int = 0
    var commentsHandler = CommentsHandler()
    var player: AVPlayer?
    var videoView:UIView = UIView()
    var videoIcon:UIImageView = UIImageView()
    var doesPageNeedsRefresh:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.LISTING_DETAIL_TITLE
        videoPlayButton.isHidden = true
        commentsTableView.delegate = commentsHandler
        commentsTableView.dataSource = commentsHandler
        let heartBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.LISTING_LIKES_HEART_BUTTON), style: .plain, target: self, action: #selector(onHeartBarButtonClick))
        self.navigationItem.rightBarButtonItem  = heartBarButton
        commentsTableView.estimatedRowHeight = 120
        commentsTableView.rowHeight = UITableView.automaticDimension
        
        let backBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.BACK_BUTTON), style: .plain, target: self, action: #selector(backBttonClick))
        self.navigationItem.leftBarButtonItem  = backBarButton
        self.commentsHandler.commentsIdDelegate = self
        getItemsClick()
        getListing(id: itemId!)
        setStatusShadow()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("listingDetailRefresh"), object: nil)
    }
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        self.view.makeToastActivity(.center)
        getListing(id: itemId!)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.resizecommentsTableView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.player?.pause()
    }
    
    func setStatusShadow(){
        statusShadowView.layer.shadowColor = UIColor.white.cgColor
        //statusShadowView.alpha = CGFloat(0.80)
        statusShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        statusShadowView.layer.shadowOpacity = 1
    }
    
    func getItemsClick(){
        self.commentsHandler.onUserImageViewClick = { comments in
            if comments.userId! == AppDefaults.getUserId(){
                self.openUserProfile()
            }else{
                self.openProfile(id:comments.userId!,username:comments.username!)
            }
        }
        self.commentsHandler.onreplyClick = { comments in
            self.addCommentOnReplyClick(comments:comments)
        }
        
    }
    
    func addCommentOnReplyClick(comments:Comments){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.ADD_COMMENT_STORYBOARD, bundle: nil)
        let addCommentViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.ADD_COMMENTS_SCREEN) as! AddCommentsViewController
        addCommentViewController.addCommentDelegate = self
        addCommentViewController.comments = comments
        addCommentViewController.replyClicked = true
        addCommentViewController.listingDetail = self.listingDetail
        self.navigationController?.pushViewController(addCommentViewController, animated: false)
    }
    
    @objc func backBttonClick(){
        self.navigationController?.popViewController(animated: false)
    }
    
    func onLikeClick(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.FOLLOW_FOLLOWING_BOARD, bundle: nil)
        let likeViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.FOLLOWERS_FOLLOWING) as! FollowersFollowingViewController
        
        likeViewController.TYPE = AppConstants.TYPE_LIKE_LISTING
        likeViewController.ID = listingDetail!.id
        self.navigationController?.pushViewController(likeViewController, animated: true)
    }
    
    func resizecommentsTableView(){
        if listingDetail?.comments?.count == 0{
            commentsTableViewHeight.constant = 12
        }else{
            var tableViewHeight: CGFloat {
                self.commentsTableView.layoutIfNeeded()
                return self.commentsTableView.contentSize.height
            }
            print("tableViewHeight:",tableViewHeight)
            self.commentsTableViewHeight?.constant = tableViewHeight + 12
        }
    }
    
    func afterCommentAdded(){
        if isCommentAdded{
            DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
                self.mainScreenScrollView.scrollToBottom(animated: true)
                self.isCommentAdded = false
            }
        }
        self.showScreen()
    }
    
    func showScreen(){
        self.view.hideToastActivity()
        self.mainScreenActivityIndicator.stopAnimating()
        self.mainScreenScrollView.isHidden = false
        self.listingPriceStatusView.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        userImageView.layer.borderWidth = 2.0
        userImageView.layer.masksToBounds = false
        userImageView.layer.borderColor = UIColor.white.cgColor
        userImageView.clipsToBounds = true
        userImageView.layer.cornerRadius = userImageView.frame.size.width/2
        
        userShadowView.layer.cornerRadius = userShadowView.frame.size.width/2
        userShadowView.layer.shadowColor = UIColor.black.cgColor
        userShadowView.alpha = CGFloat(0.26)
        userShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        userShadowView.layer.shadowOpacity = 1
        userShadowView.layer.zPosition = -1
        
        sizeBorderView.layer.borderColor = UIColor.c545454.cgColor
        sizeTypeBorderView.layer.borderColor = UIColor.c545454.cgColor
        brandBorderView.layer.borderColor = UIColor.c545454.cgColor
        colorView.layer.borderColor = UIColor.c545454.cgColor
        otherColorView.layer.borderColor = UIColor.c545454.cgColor
        categoryBorderView.layer.borderColor = UIColor.c545454.cgColor
        categoryTypeBorderView.layer.borderColor = UIColor.c545454.cgColor
        
        colorCodeView.layer.cornerRadius = colorCodeView.frame.size.width/2
        colorCodeView.layer.borderColor = UIColor.c545454.cgColor
        anotherColorView.layer.cornerRadius = anotherColorView.frame.size.width/2
        anotherColorView.layer.borderColor = UIColor.c545454.cgColor

    }
    func getListing(id:Int){
        feedViewModel.getListingDetail(accessToken: getAccessToken(), id: id,{(response) in
            self.handleResponse(response:response)
        } , { (error) in
            self.mainScreenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getListingDetailImages(id:Int){
        feedViewModel.getListingDetailImages(accessToken: getAccessToken(), id: id,{(response) in
            self.handleResponseImages(response:response)
        } , { (error) in
            self.mainScreenActivityIndicator.stopAnimating()
            self.mainScreenScrollView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
        self.view.hideToastActivity()
    }
    
    func handleResponse(response:AFDataResponse<Any>){
        if response.response?.statusCode == 200 {
            let data: ApiResponse<ListingDetail> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                print(error)
            } else {
                if let list = data.data{
                    self.listingDetail = list
                    
                    if previousListingDetail == nil{
                        previousListingDetail = list
                    }
                    
                    if list.canUserReportAnIssue!{
                        self.reportIssueButton.isHidden = false
                    }else{
                        self.reportIssueButton.isHidden = true
                    }
                    
                    self.getListingDetailImages(id:(self.listingDetail?.id)!)
                }
            }
        }
    }
    
    func handleResponseImages(response:AFDataResponse<Any>){
        self.mainScreenScrollView.isHidden = true
        self.listingPriceStatusView.isHidden = true
        if response.response?.statusCode == 200 {
            let data: ApiResponse<[SliderImages]> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                print(error)
            } else {
                let sliderImages = data.data
                self.listingDetail?.sliderImages = sliderImages
                self.getListingComments(listingId:(self.listingDetail?.id!)!)
            }
        }
    }
    
    @objc func onHeartBarButtonClick(){
       pushViewController(storyboardName: AppConstants.Storyboard.MY_LIKES_BOARD, viewControllerIdentifier: AppConstants.Controllers.MY_LIKES_SCREEN)
    }
    
    func setData(){
        
        sliderHandler.pageControl = self.pageControl
        if self.listingDetail?.videoUrl != nil{
            let videoUrl = SliderImages(listingImageId: 0, displayOrder: 0, imageUrl: "", videoUrl: self.listingDetail?.videoUrl ?? "")
            self.listingDetail?.sliderImages?.append(videoUrl)
        }
        sliderHandler.images = (self.listingDetail?.sliderImages)!
        
        sliderCollectionView.delegate = sliderHandler
        sliderCollectionView.dataSource = sliderHandler
        
        if self.listingDetail?.comments != nil{
            self.comments = (self.listingDetail?.comments)!
            self.commentsHandler.comments = (self.listingDetail?.comments)!
            commentsTableView.reloadData()
        }
        DispatchQueue.main.async {
            self.addScroll()
        }
        
        let image : UIImage = UIImage(named: AppConstants.Images.DEFAULT_USER)!
        
        if let imageUrl = listingDetail!.userPhotoUrl{
            userImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        } else {
            userImageView.image =  image
        }
        
        self.listingUserId = (listingDetail?.userId ?? 0)
        self.commentsHandler.listingUserId = (listingDetail?.userId ?? 0)
        usernameLabel.text = listingDetail?.username
        dateLabel.text = listingDetail?.createdDateFormatted
        itemTitleLabel.text = listingDetail?.title
        itemPriceLabel.text = AppConstants.Strings.DOLLAR_SIGN + String(listingDetail!.price!)
        itemSizelabel.text = listingDetail?.size?.sizeName
        itemSizeTypeLabel.text = listingDetail?.condition?.conditionName
        itemDescriptionLabel.text = listingDetail?.description
        brandNameLabel.text = listingDetail?.brand?.brandName
        colorCodeView.backgroundColor = UIColor(hex: (listingDetail?.colors![0].hexCode)!)
        colorLabel.text = listingDetail?.colors![0].colorName
        
        if (listingDetail?.colors?.count)! > 1 {
            otherColorView.isHidden = false
            otherColorLabel.text = listingDetail?.colors![1].colorName
            anotherColorView.backgroundColor = UIColor(hex: (listingDetail?.colors![1].hexCode)!)
        }else{
            otherColorView.isHidden = true
        }
        
        categoryLabel.text = listingDetail?.categoryType?.categoryTypeName ?? ""
        otherCategoryLabel.text = listingDetail?.category?.categoryName ?? ""
//        let rate = listingDetail?.shippingInfo?.rate ?? 0
//        let shippingType:String = (listingDetail?.shippingInfo?.shippingType?.type ?? "")
//        let shippingDesc:String = (listingDetail?.shippingInfo?.shippingType?.description ?? "")
//
//        let shippingtext = AppConstants.Strings.DOLLAR_SIGN + String(rate) + " " + shippingType + " " +  shippingDesc + " " + AppConstants.SHIPPING
        
        shippingDetailLabel.text = listingDetail?.shippingDescription ?? ""
        likesCount = listingDetail?.likesCount
        let text:String = "\(likesCount ?? 0)" + " " + AppConstants.Strings.FASHIONISTAS + " " + AppConstants.Strings.LIKE_THIS
        let textRegular:String = "\(likesCount ?? 0)" + " " + AppConstants.Strings.FASHIONISTAS
        let range = (text as NSString).range(of: textRegular)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontBold(size: 14), range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cFF80EE, range: range)
        
        if likesCount! < 1{
            likesView.isHidden = true
        }else{
            likesView.isHidden = false
            likesLabel.attributedText = attributedString
        }
        
        if listingDetail?.userLiked == true {
            heartButton.setImage(UIImage(named: AppConstants.Images.LISTING_DETAIL_HEART_SELECTED), for: .normal)
            userHeartButton.setImage(UIImage(named: AppConstants.Images.LISTING_DEATIL_USER_HEART_SELECTED), for: .normal)
            
        }else{
            heartButton.setImage(UIImage(named: AppConstants.Images.LISTING_DETAIL_HEART), for: .normal)
            userHeartButton.setImage(UIImage(named: AppConstants.Images.LISTING_DETAIL_USER_HEART), for: .normal)
        }
        priceLabel.text = AppConstants.Strings.DOLLAR_SIGN + String(listingDetail!.price!)
        
        switch listingDetail?.status?.statusName?.lowercased() {
            case AppConstants.SOLD:
                statusView.backgroundColor = UIColor.cB70000
                statusLabel.text = AppConstants.STATUS_SOLD
                offerLabel.isHidden = true
                break;
            case AppConstants.ON_HOLD:
                statusView.backgroundColor = UIColor.cF5A623
                statusLabel.text = AppConstants.STATUS_ON_HOLD
                offerLabel.isHidden = true
                break;
            case AppConstants.NOT_FOR_SALE:
                statusView.backgroundColor = UIColor.c333333
                statusLabel.text = AppConstants.STATUS_NOT_FOR_SALE
                
                offerLabel.isHidden = true
                break;
            default:
                statusView.backgroundColor = UIColor.cBB189C
                if listingDetail?.isBuyItNowOnly == true {
                    offerLabel.isHidden = true
                } else {
                    if listingDetail?.userId == AppDefaults.getUserId(){
                        self.priceLabel.isHidden = true
                        self.statusLabel.text = AppConstants.STATUS_EDIT
                        self.offerLabel.isHidden = false
                        self.offerLabel.text = AppConstants.Strings.VIEW_OFFERS
                        
                    }else{
                        self.statusLabel.text = AppConstants.STATUS_BUY_NOW
                        self.priceLabel.isHidden = false
                        if listingDetail?.doesLoggedInUserHaveActiveBid ?? false{
                            self.offerLabel.text = AppConstants.Strings.VIEW_ACTIVE_OFFER
                        }else{
                            self.offerLabel.text = AppConstants.Strings.MAKE_AN_OFFER
                        }
                        self.offerLabel.isHidden = false
                    }
            }
            
        }
        
        if listingDetail!.canUserLikeListing!{
            self.heartButton.isHidden = false
            self.userHeartButton.isHidden = false
            self.userLikedView.isHidden = false
        }else{
            self.heartButton.isHidden = true
            self.userHeartButton.isHidden = true
            self.userLikedView.isHidden = false
        }
        
        resizecommentsTableView()
        afterCommentAdded()
    }
    
    func heartButtonMethod() {
        
        if !(listingDetail?.userLiked!)! {
            heartButton.setImage(UIImage(named: AppConstants.Images.LISTING_DETAIL_HEART_SELECTED), for: .normal)
            userHeartButton.setImage(UIImage(named: AppConstants.Images.LISTING_DEATIL_USER_HEART_SELECTED), for: .normal)
            
            self.likesCount = likesCount! + 1
            listingDetail?.userLiked = true
            
        } else {
            heartButton.setImage(UIImage(named: AppConstants.Images.LISTING_DETAIL_HEART), for: .normal)
            userHeartButton.setImage(UIImage(named: AppConstants.Images.LISTING_DETAIL_USER_HEART), for: .normal)
            self.likesCount = likesCount! - 1
            listingDetail?.userLiked = false
        }
        
        if (likesCount)! < 1 {
            likesView.isHidden = true
        }else{
            likesView.isHidden = false
        }
        
        let text:String = "\(likesCount ?? 0)" + " " + AppConstants.Strings.FASHIONISTAS + " " + AppConstants.Strings.LIKE_THIS
        let textRegular:String = "\(likesCount ?? 0)" + " " + AppConstants.Strings.FASHIONISTAS
        let range = (text as NSString).range(of: textRegular)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontBold(size: 14), range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cFF80EE, range: range)
        
        likesLabel.attributedText = attributedString
        self.onHeartListingClickButton(id: listingDetail!.id ?? 0)
        
    }
    
    func onHeartListingClickButton(id:Int){
        feedViewModel.updateLikeStatus(accessToken: getAccessToken(), id: id, {(response) in
            if response.response?.statusCode == 200 {
            }else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print(error)
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            } 
        })
    }
    
    @IBAction func onLikesViewAction(_ sender: UITapGestureRecognizer) {
        onLikeClick()
    }
    
    @IBAction func shareButtonAction(_ sender: UIButton) {
        
    }
    
    @IBAction func heartButtonAction(_ sender: UIButton) {
        heartButtonMethod()
    }
    
    @IBAction func addCommentAction(_ sender: UIButton) {
        addComment()
    }
    
    @IBAction func sliderPreviousButtonAction(_ sender: UIButton) {
        var index = pageControl.currentPage
        index = index - 1
        scrollToPage(page: index)
    }
    
    @IBAction func sliderNextButtonAction(_ sender: UIButton) {
        var index = pageControl.currentPage
        index = index + 1
        scrollToPage(page: index)
    }
    
    @IBAction func userShareAction(_ sender: UIButton) {
    }
    
    @IBAction func userHeartAction(_ sender: UIButton) {
        heartButtonMethod()
    }
    
    @IBAction func reportIssueAction(_ sender: UIButton) {
        reportAnIssueScreen()
    }
    
    func reportAnIssueScreen(){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.REPORT_AN_ISSUE_FEED_STORYBOARD, bundle: nil)
        let reportAnIssueViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.REPORT_AN_ISSUE_FEED_SCREEN) as! ReportAnIssueFeedViewController
        reportAnIssueViewController.TYPE = AppConstants.TYPE_REPORT_AN_ISSUE_LISTING
        reportAnIssueViewController.listingDetail = self.listingDetail!
        reportAnIssueViewController.reportAnIssueUserDelegate = self
        self.navigationController?.pushViewController(reportAnIssueViewController, animated: false)
    }
    
    @IBAction func onUsernameTapAction(_ sender: UITapGestureRecognizer) {
        if listingDetail!.userId! == AppDefaults.getUserId(){
            self.openUserProfile()
        }else{
            self.openProfile(id:listingDetail!.userId!,username:listingDetail!.username!)
        }
    }
    
    @IBAction func userImageViewTapAction(_ sender: UITapGestureRecognizer) {
        if listingDetail!.userId! == AppDefaults.getUserId(){
            self.openUserProfile()
        }else{
            self.openProfile(id:listingDetail!.userId!,username:listingDetail!.username!)
        }
    }
    
    func addScroll() {
        pageControl.numberOfPages = (self.listingDetail?.sliderImages!.count)!
        
        if (self.listingDetail?.sliderImages!.count)! > 1 {
            nextButton.isHidden = false
        }
        
        for index in 0..<(self.listingDetail?.sliderImages!.count ?? 0)!{
            
            frame.origin.x = sliderScrollView.frame.size.width * CGFloat(index)
            frame.size = sliderScrollView.frame.size
            
            let imageView = UIImageView(frame: frame)
            imageView.contentMode = .scaleAspectFill
            let sliderImagePlaceholder =  UIImage(named: AppConstants.Images.ITEM_PLACEHOLDER)!
            
            if (self.listingDetail?.sliderImages![index].imageUrl)! != ""{
                imageView.kf.setImage(with: URL(string: (self.listingDetail?.sliderImages![index].imageUrl)!)!,placeholder: sliderImagePlaceholder)
                
                self.sliderScrollView.addSubview(imageView)
            }else{
                
                let videoView = UIView(frame: frame)
                self.videoView = videoView
                self.sliderScrollView.addSubview(self.videoView)
                self.containsVideo()
            }
        }
        sliderScrollView.contentSize = CGSize(width: (sliderScrollView.frame.size.width * CGFloat((self.listingDetail?.sliderImages!.count)!)), height: sliderScrollView.frame.size.height)
        sliderScrollView.delegate = self
    }
    
    func scrollToPage(page: Int) {
        var frame: CGRect = self.sliderScrollView.frame
        frame.origin.x = frame.size.width * CGFloat(page)
        frame.origin.y = 0
        self.sliderScrollView.scrollRectToVisible(frame, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let pageNumber = scrollView.contentOffset.x / scrollView.frame.size.width
        pageControl.currentPage = Int(pageNumber)
        
        if pageNumber > 0 {
            previousButton.isHidden = false
            if Int(pageNumber) == (self.listingDetail?.sliderImages!.count)!  - 1 {
                self.nextButton.isHidden = true
            } else {
                self.nextButton.isHidden = false
            }
        } else {
            self.previousButton.isHidden = true
        }
        sliderCollectionView.reloadData()
    }

    @IBAction func onStatusTapAction(_ sender: UITapGestureRecognizer) {
        if self.address != nil && self.card != nil{
            self.statusTap()
        }else{
            if statusLabel.text == AppConstants.STATUS_EDIT{
                self.getListingDetailEdit(id:(self.listingDetail?.id!)!)
            }else{
                self.view.makeToastActivity(.center)
                self.getDefaultCard(type: AppConstants.TYPE_STATUS)
            }
        }
    }
    
    func statusTap(){
        if statusLabel.text == AppConstants.STATUS_EDIT{
            self.getListingDetailEdit(id:(self.listingDetail?.id!)!)
        }else if statusLabel.text == AppConstants.STATUS_BUY_NOW{
            self.buyNowListingScreen()
        }
        self.view.hideToastActivity()
    }
    
    func buyNowListingScreen(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.BUY_NOW_LISTING_STORYBOARD, bundle: nil)
        let  buyNowViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.BUY_NOW_LISTING_SCREEN) as! BuyNowListingViewController
        buyNowViewController.TYPE = AppConstants.TYPE_BUY_NOW
        buyNowViewController.listingDetail = self.listingDetail
        navigationController?.pushViewController(buyNowViewController, animated: false)
    }
    
    @IBAction func offerHistoryTapAction(_ sender: UITapGestureRecognizer) {
        if self.address != nil && self.card != nil{
            self.offerHistoryTap()
            self.view.hideToastActivity()
        }else{
            self.view.makeToastActivity(.center)
            self.getDefaultCard(type: AppConstants.TYPE_OFFER_HISTORY)
        }
    }
    
    func offerHistoryTap(){
        if offerLabel.text != ""{
            if (self.listingDetail?.userId == AppDefaults.getUserId()) {
                openActiveOffersScreen()
            } else {
                openOffersScreen()
            }
        }
    }
    
    func openActiveOffersScreen(){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.OFFERS_STORYBOARD, bundle: nil)
        let activeOffersViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.OFFERS_SCREEN) as! OffersViewController
        activeOffersViewController.TYPE = AppConstants.TYPE_COUNTER_OFFER
        activeOffersViewController.listingId = self.itemId!
        self.navigationController?.pushViewController(activeOffersViewController, animated: false)
    }
    
    func openOffersScreen(){
        let storybosrd = UIStoryboard(name: AppConstants.Storyboard.OFFER_DETAIL_STORYBOARD, bundle:  nil)
        let makeAnOfferViewController = storybosrd.instantiateViewController(withIdentifier: AppConstants.Controllers.OFFERS_DETAIL_SCREEN) as! OfferDetailViewController
        makeAnOfferViewController.TYPE = AppConstants.TYPE_MAKE_A_NEW_OFFER
        makeAnOfferViewController.listingId = self.itemId!
        self.navigationController?.pushViewController(makeAnOfferViewController, animated: false)
    }
    
    func addComment(){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.ADD_COMMENT_STORYBOARD, bundle: nil)
        let addCommentViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.ADD_COMMENTS_SCREEN) as! AddCommentsViewController
        addCommentViewController.addCommentDelegate = self
        addCommentViewController.replyClicked = self.onReplyClick
        addCommentViewController.listingDetail = self.listingDetail
        self.navigationController?.pushViewController(addCommentViewController, animated: false)
    }
    
    func addComment(id:Int,comment:Comments){
        var comments = Comments()
        if comment.replyToUserId! == 0{
            comments.comment = comment.comment!
            comments.userMentions = comment.userMentions!
        }else if comment.userMentions?.count == 0{
            comments.comment = comment.comment!
            comments.replyToUserId = comment.replyToUserId!
        }else if comment.replyToUserId! == 0 && comment.userMentions?.count == 0{
            comments.comment = comment.comment!
        }else{
            comments.comment = comment.comment!
            comments.replyToUserId = comment.replyToUserId!
            comments.userMentions = comment.userMentions!
        }
        feedViewModel.addComment(accessToken: getAccessToken(), data: comments, id: id, {(response) in
            if response.response?.statusCode == 200 || response.response?.statusCode == 201{
                self.isCommentAdded = true
                self.getListing(id: id)
            }else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print(error)
                }
                self.view.hideToastActivity()
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
        
    func getListingComments(listingId:Int){
         feedViewModel.getListingComments(accessToken: getAccessToken(),listingId:listingId ,{(response) in
             if response.response?.statusCode == 200 || response.response?.statusCode == 201{
                 let data: ApiResponse<Comments> = Utils.convertResponseToData(data: response.data!)
                 if data.isError {
                     let error = Utils.getErrorMessage(errors: data.errors)
                     print(error)
                 }else{
                     let comment = data.data!
                     if let commentData = comment.items{
                        //self.commentsHandler.comments = commentData
                        self.listingDetail?.comments = commentData
                     }
                    self.setData()
                 }
             }else {
                 print("error")
                self.mainScreenActivityIndicator.stopAnimating()
             }
         } , { (error) in
             self.mainScreenActivityIndicator.stopAnimating()
             if error.statusCode == AppConstants.NO_INTERNET {
                 self.showInternetError()
             }
         })
     }
    
    func getDefaultCard(type:Int) {
        feedViewModel.getDefaultCard(accessToken: getAccessToken(), {(response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Card> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let card = data.data
                    self.card = card
                    self.getDefaultAddress(type: type)
                }
            }else if response.response?.statusCode == 404{
                self.getDefaultAddress(type: type)
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print(error)
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
      
    func getDefaultAddress(type:Int) {
        feedViewModel.getDefaultAddress(accessToken: getAccessToken(), {(response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Address> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let address = data.data
                    self.address = address
                    if self.card == nil || self.address == nil{
                        self.openNoPaymentOrAddressDialog()
                        self.view.hideToastActivity()
                    }else{
                        if type == AppConstants.TYPE_OFFER_HISTORY{
                            self.offerHistoryTap()
                            self.view.hideToastActivity()
                        }else{
                            self.statusTap()
                        }
                    }
                }
            }else if response.response?.statusCode == 404{
                self.openNoPaymentOrAddressDialog()
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print(error)
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func openNoPaymentOrAddressDialog() {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.ENABLE_DISABLE_DIALOG_BOARD, bundle: nil)
        let noPaymentOrAddressViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.ENABLE_DISABLE_LISTING_DIALOG) as! EnableDisableDialogViewController
        noPaymentOrAddressViewController.modalPresentationStyle = .overFullScreen
        noPaymentOrAddressViewController.modalTransitionStyle = .crossDissolve
        if self.address != nil{
            noPaymentOrAddressViewController.address = self.address
        }
        if self.card != nil{
            noPaymentOrAddressViewController.card = self.card
        }
        noPaymentOrAddressViewController.type = AppConstants.TYPE_CANNOT_MAKE_OFFER
        noPaymentOrAddressViewController.onCloseClickAddressOrPaymentDelegate = self
        noPaymentOrAddressViewController.addNewAddressDelegate = self
        noPaymentOrAddressViewController.setDefaultPaymentDelegate = self
        navigationController?.present(noPaymentOrAddressViewController, animated: false,completion: nil)
    }
    
    func openAddressScreen(){
        self.dismiss(animated: false, completion: nil)
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.ADDRESSES_BOARD, bundle: nil)
        let addressViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.ADDRESSES_SCREEN) as! AddressesViewController
        addressViewController.addAddressBuyNowDelegate = self
        navigationController?.pushViewController(addressViewController, animated: true)
    }
    
    func addPayment(){
        self.dismiss(animated: false, completion: nil)
        self.view.hideToastActivity()
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.ADD_NEW_PAYMENT_BOARD, bundle:nil)
        let editPaymentViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.ADD_NEW_PAYMENT_SCREEN) as! AddNewPaymentViewController
        editPaymentViewController.TYPE = AppConstants.TYPE_CANNOT_MAKE_OFFER
        self.navigationController?.pushViewController(editPaymentViewController, animated:true)
    }
    
    func containsVideo(){
        let videoTap = UITapGestureRecognizer(target: self, action: #selector (onVideoViewTapAction))
        self.videoView.addGestureRecognizer(videoTap)
        self.addPlayerToVideo(playerView: self.videoView)
    }
    
    @objc func onVideoViewTapAction(sender: UITapGestureRecognizer){
        if self.player?.isPlaying ?? false{
            self.player?.pause()
            self.videoIcon.isHidden = false
        }else{
            self.player?.play()
            self.videoIcon.isHidden = true
        }
    }
    
    func addPlayerToVideo(playerView:UIView){
        self.videoView.layer.zPosition = 0
        let videoURL = NSURL(string: self.listingDetail?.videoUrl ?? "")
        
        player = AVPlayer(url: videoURL! as URL)
        player?.isMuted = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying),
                                               name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem)
        
        let layer: AVPlayerLayer = AVPlayerLayer(player: player)
        layer.backgroundColor = UIColor.white.cgColor
        layer.zPosition = -1
        layer.frame = CGRect(x: 0, y: 0, width: playerView.frame.width, height: playerView.frame.height)
        layer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerView.layer.sublayers?.forEach({$0.removeFromSuperlayer()})
        playerView.layer.addSublayer(layer)
        let videoIconImage = UIImage(named: AppConstants.Images.VIDEO_ICON)!
        
        let size = videoIconImage.size.width
        let sizeHeight = videoIconImage.size.height
        
        let screenWidth = self.videoView.frame.size.width
        
        let screenHeight = self.videoView.frame.size.height
        
        let videoIcon = UIImageView(frame: CGRect(x: (screenWidth / 2) - (size / 2), y: (screenHeight / 2) - (sizeHeight / 2), width: videoIconImage.size.width, height: videoIconImage.size.height))
        videoIcon.image = videoIconImage
        self.videoIcon = videoIcon
        self.videoView.addSubview(self.videoIcon)
        self.videoIcon.isHidden = false
        self.videoIcon.layer.zPosition = 1
        
        player?.pause()
        
        NotificationCenter.default.addObserver(self, selector: #selector(loopVideo), name: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem)
        
    }
    
    @objc func loopVideo() {
        player?.seek(to: CMTime.zero)
        player?.pause()
        self.videoIcon.isHidden = false
        print("Video in loop")
    }
    
    @objc func playerDidFinishPlaying(notification: NSNotification) {
        if let playerItem = notification.object as? AVPlayerItem {
            playerItem.seek(to: CMTime.zero, completionHandler: nil)
        }
        print("Video Finished")
    }
    
    @IBAction func playButtonAction(_ sender: UIButton) {
        //playButtonTapAction()
    }
    
    func playButtonTapAction(){
        if self.player?.isPlaying ?? false{
            self.player?.pause()
            self.videoIcon.isHidden = false
        }else{
            self.player?.play()
            self.videoIcon.isHidden = true
        }
    }
    
    
}
extension ListDetailViewController: TrimmerViewDelegate {
    func positionBarStoppedMoving(_ playerTime: CMTime) {
        player?.seek(to: playerTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
        player?.play()
        if playerTime.seconds == 0{
            self.videoIcon.isHidden = false
        }else{
            self.videoIcon.isHidden = true
        }
    }
    
    func didChangePositionBar(_ playerTime: CMTime) {
        player?.pause()
        videoIcon.isHidden = false
        player?.seek(to: playerTime, toleranceBefore: CMTime.zero, toleranceAfter: CMTime.zero)
        let duration = (durationView.endTime! - durationView.startTime!).seconds
        print(duration)
        print(durationView.startTime!)
    }
}
