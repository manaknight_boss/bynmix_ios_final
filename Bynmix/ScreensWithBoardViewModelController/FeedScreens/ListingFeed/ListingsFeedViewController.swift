import UIKit
import XLPagerTabStrip
import Alamofire

class ListingsFeedViewController: BaseViewController,LoadMoreProtocol {
    
    func onLoadMore() {
        if !isApiCalled {
            isApiCalled = true
            getFeedListings()
        }
    }
    
    var feedViewModel = FeedViewModel()
    var profileViewModel = ProfileViewModel()
    
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var collectionView: UICollectionView!
    
    @IBOutlet var noDataShadowView: UIView!
    @IBOutlet var noDataView: UIView!
    @IBOutlet var noDataImageView: UIImageView!
    @IBOutlet var noDataLabel: UILabel!
    
    @IBOutlet var feedNoDataShadowView: UIView!
    @IBOutlet var feedNoDataView: UIView!
    @IBOutlet var feedNoDataImageView: UIImageView!
    @IBOutlet var feedNoDataLabel: UILabel!
    
    @IBOutlet var browseNoDataShadowView: UIView!
    @IBOutlet var browseNoDataView: UIView!
    @IBOutlet var browseNoDataImageView: UIImageView!
    @IBOutlet var browseNoDataLabel: UILabel!
    
    let feedListingsHandler = FeedListingsHandler()
    var feed:Feed? = nil
    var refreshControl:UIRefreshControl = UIRefreshControl()
    var currentPage = 1
    var isApiCalled = false
    var viewControllerType:Int?
    var isClearFilterCalled = true
    let milliSeconds = 0.25
    let milliSecs = 0.60
    var searchText:String = ""
    static var feedOfUser : [Feed] = []
    static var feedOfAll : [Feed] = []
    static var feedOfMyLikes : [Feed] = []
    var isFeedListCleared = false
    var TYPE:Int?
    
    static var feedFilterData:FilterData = FilterData()
    static var browseFilterData:FilterData = FilterData()
    static var myLikesFilterData:FilterData = FilterData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.itemSize = UICollectionViewFlowLayout.automaticSize
        }
        collectionView.isHidden = true
        collectionView.contentInset = UIEdgeInsets(top: 40, left: 5, bottom: 5, right: 5)
        collectionView.delegate = feedListingsHandler
        collectionView.dataSource = feedListingsHandler
        getItemsClick()
        
        addRefreshControl()
        feedListingsHandler.loadMoreDelegate = self
        self.feedListingCollectionView = collectionView
        hideKeyboardWhenTappedAround()
        settingNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUIOfNoData()
        searchTextOnTabChange()
    }
    
    func setUIOfNoData(){
        noDataShadowView.layer.cornerRadius = 2
        noDataShadowView.layer.shadowColor = UIColor.black.cgColor
        noDataShadowView.alpha = CGFloat(0.26)
        noDataShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        noDataShadowView.layer.shadowOpacity = 1
        noDataShadowView.layer.zPosition = -1
        
        feedNoDataShadowView.layer.cornerRadius = 2
        feedNoDataShadowView.layer.shadowColor = UIColor.black.cgColor
        feedNoDataShadowView.alpha = CGFloat(0.26)
        feedNoDataShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        feedNoDataShadowView.layer.shadowOpacity = 1
        feedNoDataShadowView.layer.zPosition = -1
        
        browseNoDataShadowView.layer.cornerRadius = 2
        browseNoDataShadowView.layer.shadowColor = UIColor.black.cgColor
        browseNoDataShadowView.alpha = CGFloat(0.26)
        browseNoDataShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        browseNoDataShadowView.layer.shadowOpacity = 1
        browseNoDataShadowView.layer.zPosition = -1
        
    }
    
    func settingNotifications(){
        NotificationCenter.default.addObserver(self,selector: #selector(listingFilterApply),name:Notification.Name(AppConstants.Notifications.FILTER_LISTING_BAR_BUTTON),object: nil)
        
        NotificationCenter.default.addObserver(self,selector: #selector(clearFilters),name:Notification.Name(AppConstants.Notifications.FEED_CLEAR_ALL_FILTER_BUTTON),object: nil)
        
        NotificationCenter.default.addObserver(self,selector: #selector(myLikesListingFilterApply),name:Notification.Name(AppConstants.Notifications.MY_LIKES_APPLY_FILTER_BUTTON),object: nil)
        
        NotificationCenter.default.addObserver(self,selector: #selector(myLikesClearFilters),name:Notification.Name(AppConstants.Notifications.MY_LIKES_CLEAR_ALL_FILTER_BUTTON),object: nil)
        
        NotificationCenter.default.addObserver(self,selector: #selector(feedListingsSearchText),name:Notification.Name(AppConstants.Notifications.FEED_LISTINGS_BAR_BUTTON),object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(AppConstants.Notifications.FILTER_LISTING_BAR_BUTTON), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(AppConstants.Notifications.MY_LIKES_APPLY_FILTER_BUTTON), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(AppConstants.Notifications.FEED_CLEAR_ALL_FILTER_BUTTON), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(AppConstants.Notifications.MY_LIKES_CLEAR_ALL_FILTER_BUTTON), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(AppConstants.Notifications.FEED_LISTINGS_BAR_BUTTON), object: nil)
    }
    
    func searchTextOnTabChange(){
        
        if self.TYPE == AppConstants.TYPE_MY_LIKES_LISTING{
            self.feedListingsHandler.TYPE = AppConstants.TYPE_MY_LIKES
        }else{
            let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
            if selectedIndex == 0{
                self.feedListingsHandler.TYPE = AppConstants.TYPE_FEED_LISTING
            }else{
                self.feedListingsHandler.TYPE = AppConstants.TYPE_BROWSE_LISTING
            }
        }
        if self.TYPE == AppConstants.TYPE_MY_LIKES_LISTING{
            
        }else{
            let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
            if selectedIndex == 0{
                searchText = AppConstants.SEARCH_TEXT_FOR_FEED
                if AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_FEED_LISTING{
                    if ListingsFeedViewController.feedOfUser.count > 0{
                        ListingsFeedViewController.feedOfUser.removeAll()
                    }
                }
            }else if selectedIndex == 1{
                searchText = AppConstants.SEARCH_TEXT_FOR_BROWSE
                if AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_BROWSE_LISTING{
                    if ListingsFeedViewController.feedOfAll.count > 0 {
                        ListingsFeedViewController.feedOfAll.removeAll()
                    }
                }
            }
        }

        if self.searchText.count == 0{
                if self.TYPE == AppConstants.TYPE_MY_LIKES_LISTING{
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        if ListingsFeedViewController.feedOfMyLikes.count == 0{
                            self.currentPage = 1
                            self.getFeedListings()
                        }
                    }
                }else{
                    let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
                    if selectedIndex == 0{
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if  ListingsFeedViewController.feedOfUser.count == 0{
                                self.showActivityIndicator()
                                self.currentPage = 1
                                self.getFeedListings()
                                self.searchButtonClickedForFeedListing()
                            }
                        }
                    }else if selectedIndex == 1{
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if ListingsFeedViewController.feedOfAll.count == 0{
                                self.showActivityIndicator()
                                self.currentPage = 1
                                self.getFeedListings()
                                self.searchButtonClickedForBrowseListing()
                            }
                        }
                    }
                }
            }else{
                var filterData = FilterData()
                filterData.searchText = self.searchText
                if self.TYPE == AppConstants.TYPE_MY_LIKES_LISTING{
                    self.showActivityIndicator()
                    self.currentPage = 1
                    self.getFeedListings(data:filterData)
                }else{
                    let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
                    if selectedIndex == 0{
                        if AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_FEED_LISTING{
                            if ListingsFeedViewController.feedOfUser.count == 0 {
                                self.showActivityIndicator()
                                self.currentPage = 1
                                self.getFeedListings(data:filterData)
                                self.searchButtonClickedForFeedListing()
                            }
                        }
                    }else if selectedIndex == 1{
                        if AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_BROWSE_LISTING {
                            if ListingsFeedViewController.feedOfAll.count == 0 {
                                self.showActivityIndicator()
                                self.currentPage = 1
                                self.getFeedListings(data:filterData)
                                self.searchButtonClickedForBrowseListing()
                            }
                        }
                    }
                }
                
            }
        
    }
    
    func showActivityIndicator(){
        if self.collectionView.isHidden == false{
            self.view.makeToastActivity(.center)
        }
    }
    
    @objc func feedListingsSearchText(_ notification: Notification) {
        self.view.makeToastActivity(.center)
        self.removeListIfNotFound()
        let barbuttonEnable:String = notification.userInfo?[AppConstants.Notifications.FEED_LISTINGS_SEARCH_TEXT] as! String
        
        let searchText = barbuttonEnable
        self.searchText = searchText
        var filterData = FilterData()
        if searchText != ""{
            self.showLoader()
            currentPage = 1
            filterData.searchText = searchText
            DispatchQueue.main.asyncAfter(deadline: .now() + milliSecs) {
                self.getFeedListings(data:filterData)
                let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
                if selectedIndex == 0{
                    self.searchButtonClickedForFeedListing()
                }else if selectedIndex == 1{
                    self.searchButtonClickedForBrowseListing()
                }
            }
        }else{
            self.showLoader()
            currentPage = 1
            DispatchQueue.main.asyncAfter(deadline: .now() + milliSecs) {
                self.getFeedListings()
                let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
                if selectedIndex == 0{
                    self.searchButtonClickedForFeedListing()
                }else if selectedIndex == 1{
                    self.searchButtonClickedForBrowseListing()
                }
            }
        }
    }
    
    func showLoader(){
        self.view.makeToastActivity(.center)
    }
    
    @objc func listingFilterApply(_ notification: Notification) {
        self.removeListIfNotFound()
        var selectedFilterList:[SelectedFilter] = notification.userInfo?[AppConstants.Notifications.SELECTED_FILTER_LIST] as! [SelectedFilter]
        
        if selectedFilterList.count > 0{
            self.view.makeToastActivity(.center)
            self.setFilterData(selectedFilterList: selectedFilterList)
        }
        selectedFilterList.removeAll()
    }
    
    @objc func myLikesListingFilterApply(_ notification: Notification) {
        self.removeListIfNotFound()
        var selectedFilterList:[SelectedFilter] = notification.userInfo?[AppConstants.Notifications.MY_LIKES_APPLY_FILTER_LIST] as! [SelectedFilter]
        
        if selectedFilterList.count > 0{
            self.view.makeToastActivity(.center)
            self.setFilterData(selectedFilterList: selectedFilterList)
        }
        selectedFilterList.removeAll()
    }
    
    @objc func clearFilters(_ notification: Notification) {
        let barbuttonEnable = notification.userInfo?[AppConstants.Notifications.FEED_CLEAR_FILTER_LIST] as! Bool
        
        let isFeed = notification.userInfo?[AppConstants.Notifications.isFeed]
        if isFeed == nil{
            if barbuttonEnable{
                self.view.makeToastActivity(.center)
                self.currentPage = 1
                self.clearFilterData()
                if self.searchText.count > 0{
                    var filterData = FilterData()
                    filterData.searchText = self.searchText
                    self.getFeedListings(data: filterData)
                }else{
                    self.getFeedListings()
                }
            }
        }
    }
    
    @objc func myLikesClearFilters(_ notification: Notification) {
        let barbuttonEnable = notification.userInfo?[AppConstants.Notifications.MY_LIKES_CLEAR_FILTER_LIST] as! Bool
        
        let isFeed = notification.userInfo?[AppConstants.Notifications.isFeed]
        if isFeed == nil{
            if barbuttonEnable{
                self.view.makeToastActivity(.center)
                self.currentPage = 1
                self.clearFilterData()
                self.getFeedListings()
            }
        }
    }
    
    func clearFilterData(){
        if self.TYPE == AppConstants.TYPE_MY_LIKES_LISTING{
            ListingsFeedViewController.myLikesFilterData = FilterData()
        }else{
            let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
            if selectedIndex == 0{
                ListingsFeedViewController.feedFilterData = FilterData()
            }else if selectedIndex == 1{
                ListingsFeedViewController.browseFilterData = FilterData()
            }
        }
    }
    
    func setFilterData(selectedFilterList :[SelectedFilter] = []) {
        var filterData = FilterData()
        var conditionIds:[Int] = []
        var sizeIds :[Int] = []
        var colorIds:[Int] = []
        var categoriesIds:[Int] = []
        var brandsIds:[Int] = []
        var categoryTypeIds:[Int] = []
        
        var conditionNames:[String] = []
        var sizeNames:[String] = []
        var colorNames:[String] = []
        var categoryNames:[String] = []
        var priceNames:[String] = []
        var brandNames:[String] = []
        
        for i in 0..<selectedFilterList.count {
            if (selectedFilterList[i].id != 0) {
                if (selectedFilterList[i].type == AppConstants.CONDITION_FILTER) {
                    if (selectedFilterList[i].id != 0) {
                        conditionIds.append(selectedFilterList[i].id!)
                        filterData.conditionIds = conditionIds
                        
                        conditionNames.append(selectedFilterList[i].name!)
                        trackSearchDictionary.updateValue(conditionNames, forKey: "condition")
                    }
                } else if (selectedFilterList[i].type == AppConstants.SIZE_FILTER) {
                    if (selectedFilterList[i].id != 0) {
                        sizeIds.append(selectedFilterList[i].subType!)
                        filterData.sizeIds = sizeIds
                        
                        sizeNames.append(selectedFilterList[i].name!)
                        trackSearchDictionary.updateValue(sizeNames, forKey: "size")
                    }
                } else if (selectedFilterList[i].type == AppConstants.COLOR_FILTER) {
                    colorIds.append(selectedFilterList[i].id!)
                    filterData.colorIds = colorIds
                    colorNames.append(selectedFilterList[i].name!)
                    trackSearchDictionary.updateValue(colorNames, forKey: "color")
                } else if (selectedFilterList[i].type == AppConstants.CATEGORY_FILTER) {
                    if (selectedFilterList[i].subType != 0) {
                        categoriesIds.append(selectedFilterList[i].subType!)
                        categoryNames.append(selectedFilterList[i].name!)
                        trackSearchDictionary.updateValue(categoryNames, forKey: "category")
                    }else{
                        categoryTypeIds.append(selectedFilterList[i].id!)
                    }
                    filterData.categoryIds = categoriesIds
                    filterData.categoryTypeIds = categoryTypeIds
                    
                } else if (selectedFilterList[i].type == AppConstants.BRAND_FILTER) {
                    if (selectedFilterList[i].id != 0) {
                        brandsIds.append(selectedFilterList[i].id!)
                        filterData.brandIds = brandsIds
                        
                        brandNames.append(selectedFilterList[i].name!)
                        trackSearchDictionary.updateValue(brandNames, forKey: "brand")
                    }
                } else if (selectedFilterList[i].type == AppConstants.PRICE_FILTER) {
                    if selectedFilterList[i].id == 1 {
                        filterData.customMinPrice = 0
                        filterData.customMaxPrice = 20
                    } else if selectedFilterList[i].id == 2 {
                        filterData.customMinPrice = 21
                        filterData.customMaxPrice = 50
                    } else if selectedFilterList[i].id == 3 {
                        filterData.customMinPrice = 51
                        filterData.customMaxPrice = 100
                    } else if selectedFilterList[i].id == 4 {
                        filterData.customMinPrice = 100
                        filterData.customMaxPrice = 200
                    } else if selectedFilterList[i].id == 5{
                        filterData.customMinPrice = 200
                        filterData.customMaxPrice = 500
                    }
                    priceNames.append(selectedFilterList[i].name!)
                    trackSearchDictionary.updateValue(priceNames, forKey: "price")
                }
            }
        }
        if self.TYPE == AppConstants.TYPE_MY_LIKES_LISTING{
            ListingsFeedViewController.myLikesFilterData = filterData
            BaseViewController.mSelectedMyLikesFilterList = selectedFilterList
        }else{
            let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
            if selectedIndex == 0{
                ListingsFeedViewController.feedFilterData = filterData
                BaseViewController.mSelectedFeedFilterList = selectedFilterList
            }else if selectedIndex == 1{
                ListingsFeedViewController.browseFilterData = filterData
                BaseViewController.mSelectedBrowseFilterList = selectedFilterList
            }
        }
        
        print(filterData)
        self.currentPage = 1
        if self.searchText.count > 0 {
            filterData.searchText = self.searchText
        }
        self.getFeedListings(data: filterData)
    }
    
    func getItemsClick(){
        feedListingsHandler.itemClickHandler = { feed in
            let storyboard = UIStoryboard(name: AppConstants.Storyboard.FOLLOW_FOLLOWING_BOARD, bundle: nil)
            let likeViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.FOLLOWERS_FOLLOWING) as! FollowersFollowingViewController
            if(feed.type == AppConstants.POST) {
                likeViewController.TYPE = AppConstants.TYPE_LIKE_POST
            } else {
                likeViewController.TYPE = AppConstants.TYPE_LIKE_LISTING
            }
            
            likeViewController.ID = feed.id
            self.navigationController?.pushViewController(likeViewController, animated: true)
        }
        
        feedListingsHandler.onItemImageViewClick = { feed in
            
            if feed.type == AppConstants.POST {
                let storyboard = UIStoryboard(name: AppConstants.Storyboard.POST_DETAIL_BOARD, bundle: nil)
                let postDetailViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.POST_DETAIL_SCREEN) as! PostDetailViewController
                postDetailViewController.feed = feed
                self.navigationController?.pushViewController(postDetailViewController, animated: true)
            }else{
                let storyboard = UIStoryboard(name: AppConstants.Storyboard.LISTING_DETAIL_BOARD, bundle: nil)
                let listingDetailViewController = storyboard.instantiateViewController(withIdentifier:AppConstants.Controllers.Listing_Detail_Screen) as! ListDetailViewController
                listingDetailViewController.itemId = feed.id
                self.navigationController?.pushViewController(listingDetailViewController, animated: true)
            }
        }
        
        feedListingsHandler.onHeartButtonClick = { feed in
            self.onHeartClickButton(id: feed.id!)
        }
        
        feedListingsHandler.onOtherUserClick = { feed in
            if feed.userId! == AppDefaults.getUserId(){
                self.openUserProfile()
            }else{
                self.openProfile(id:feed.userId!,username:feed.username!)
            }
        }
    }
    
    func getFeedListings(data:FilterData? = nil) {
        var filters: FilterData = FilterData()
        if data == nil {
            filters = FilterData()
        } else {
            filters = data!
        }
        
        if self.TYPE == AppConstants.TYPE_MY_LIKES_LISTING{
            self.feedListingsHandler.TYPE = AppConstants.TYPE_MY_LIKES
            self.profileViewModel.getLikesListing(accessToken: self.getAccessToken(), sort: AppConstants.LIKED_DATE, page: self.currentPage, data: filters,{(response) in
                self.view.hideToastActivity()
                self.handleResponse(response: response, type: AppConstants.TYPE_MY_LIKES)
            } , { (error) in
                self.view.hideToastActivity()
                self.refreshControl.endRefreshing()
                if error.statusCode == AppConstants.NO_INTERNET {
                    self.showInternetError()
                }
            })
        }else{
            let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
            var type: Int = 0
            if selectedIndex == 0{
                type = AppConstants.TYPE_FEED_LISTING
            }else if selectedIndex == 1{
                type = AppConstants.TYPE_BROWSE_LISTING
            }
            self.feedListingsHandler.TYPE = type
            self.feedViewModel.getFeedListings(accessToken: self.getAccessToken(), data: filters,page:self.currentPage,{(response) in
                self.view.hideToastActivity()
                self.handleResponse(response: response,filters: filters, type: type)
            } , { (error) in
                self.view.hideToastActivity()
                self.refreshControl.endRefreshing()
                if error.statusCode == AppConstants.NO_INTERNET {
                    self.showInternetError()
                }
            })
        }
        
    }
    
    func handleResponse(response: AFDataResponse<Any>,filters:FilterData? = nil,type:Int) {
        self.isApiCalled = false
        self.refreshControl.endRefreshing()
        if response.response?.statusCode == 200 {
            let data: ApiResponse<Feed> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                print("error in 200",error)
            } else {
                let feed = data.data
                
                if (feed?.items?.count ?? 0) > 0{
                    if let feeds = feed?.items {
                        
                        if self.currentPage == 1 {
                            if self.TYPE == AppConstants.TYPE_MY_LIKES_LISTING{
                                FeedListingsHandler.myLikes = feeds
                            }else{
                                let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
                                if selectedIndex == 0{
                                    FeedListingsHandler.feed = feeds
                                }else if selectedIndex == 1{
                                    FeedListingsHandler.browse = feeds
                                }
                            }
                        } else {
                            if self.TYPE == AppConstants.TYPE_MY_LIKES_LISTING{
                                FeedListingsHandler.myLikes.append(contentsOf: feeds)
                            }else{
                                let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
                                if selectedIndex == 0{
                                    FeedListingsHandler.feed.append(contentsOf: feeds)
                                }else if selectedIndex == 1{
                                    FeedListingsHandler.browse.append(contentsOf: feeds)
                                }
                            }
                        }
                        if self.TYPE == AppConstants.TYPE_MY_LIKES_LISTING{
                            ListingsFeedViewController.feedOfMyLikes = feeds
                        }else{
                            let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
                            if selectedIndex == 0{
                                ListingsFeedViewController.feedOfUser = feeds
                            }else if selectedIndex == 1{
                                ListingsFeedViewController.feedOfAll = feeds
                            }
                        }
                        
                        if feed?.totalPages == self.currentPage {
                            self.isApiCalled = true
                            self.feedListingsHandler.isLoadMoreRequired = false
                            let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
                            layout.footerReferenceSize = CGSize(width: 0, height: 0)
                        }
                        self.collectionView.reloadData()
                        self.screenActivityIndicator.stopAnimating()
                        self.collectionView.isHidden = false
                        self.hideNoDataForAll()
                    }
                }else{
                    self.noDataDialog()
                }
                
            }
            self.currentPage = self.currentPage + 1
            
        }else if response.response?.statusCode == 404 {
            self.noDataDialog()
            if self.TYPE == AppConstants.TYPE_MY_LIKES_LISTING{
                self.noDataLabel.text = AppConstants.Strings.MY_LIKES_NO_LIST
            }else {
                self.feedNoDataLabel.text = AppConstants.Strings.NOTHING_TO_SHOW
                self.browseNoDataLabel.text = AppConstants.Strings.NOTHING_TO_SHOW
            }
            self.noDataImageView.image = UIImage(named: AppConstants.Images.LOGO_BLACK)
            self.feedNoDataImageView.image = UIImage(named: AppConstants.Images.LOGO_BLACK)
            self.browseNoDataImageView.image = UIImage(named: AppConstants.Images.LOGO_BLACK)
            
        } else {
            self.noDataDialog()
            self.noDataLabel.text = AppConstants.Strings.SOMETHING_WRONG_HERE
            self.feedNoDataLabel.text = AppConstants.Strings.SOMETHING_WRONG_HERE
            self.browseNoDataLabel.text = AppConstants.Strings.SOMETHING_WRONG_HERE
            self.noDataImageView.image = UIImage(named: AppConstants.Images.LOGO_SAD_CLOUD)
            self.feedNoDataImageView.image = UIImage(named: AppConstants.Images.LOGO_SAD_CLOUD)
            self.browseNoDataImageView.image = UIImage(named: AppConstants.Images.LOGO_SAD_CLOUD)
        }
        
        if (filters?.searchText != nil && filters?.searchText != ""){
            self.trackSearchCalculate(userFilterData: filters)
        }else if filters?.brandIds?.count ?? 0 > 0 || filters?.categoryIds?.count ?? 0 > 0 || filters?.colorIds?.count ?? 0 > 0 || filters?.conditionIds?.count ?? 0 > 0 || filters?.sizeIds?.count ?? 0 > 0 || filters?.customMinPrice != nil || filters?.customMaxPrice != nil{
            self.trackSearchCalculate(userFilterData: filters)
        }
        
    }
    
    func hideNoDataForAll(){
        if self.TYPE == AppConstants.TYPE_MY_LIKES_LISTING{
            self.hideNoData()
        }else{
            let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
            if selectedIndex == 0{
                self.feedHideNoData()
            }else if selectedIndex == 1{
                self.browseHideNoData()
            }
        }
        self.collectionView.reloadData()
    }
    
    func noDataDialog(){
        self.screenActivityIndicator.stopAnimating()
        if self.TYPE == AppConstants.TYPE_MY_LIKES_LISTING{
            self.showNoData()
        }else{
            let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
            if selectedIndex == 0{
                self.feedShowNoData()
            }else if selectedIndex == 1{
                self.browseShowNoData()
            }
        }
        self.removeListIfNotFound()
        self.collectionView.reloadData()
    }
    
    func showNoData(){
        self.noDataView.isHidden = false
        self.noDataShadowView.isHidden = false
    }
    
    func hideNoData(){
        self.noDataView.isHidden = true
        self.noDataShadowView.isHidden = true
    }
    
    func feedShowNoData(){
        self.feedNoDataView.isHidden = false
        self.feedNoDataShadowView.isHidden = false
    }
    
    func feedHideNoData(){
        self.feedNoDataView.isHidden = true
        self.feedNoDataShadowView.isHidden = true
    }
    
    func browseShowNoData(){
        self.browseNoDataView.isHidden = false
        self.browseNoDataShadowView.isHidden = false
    }
    
    func browseHideNoData(){
        self.noDataView.isHidden = true
        self.noDataShadowView.isHidden = true
    }
    
    func removeListIfNotFound(){
        if self.TYPE == AppConstants.TYPE_MY_LIKES_LISTING{
            if FeedListingsHandler.myLikes.count > 0{
                FeedListingsHandler.myLikes.removeAll()
            }
        }else{
            let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
            if selectedIndex == 0{
                if FeedListingsHandler.feed.count > 0{
                    FeedListingsHandler.feed.removeAll()
                }
            }else if selectedIndex == 1{
                if FeedListingsHandler.browse.count > 0{
                    FeedListingsHandler.browse.removeAll()
                }
            }
        }
    }
    
    func hideAllViewsForNoData(){
        self.browseHideNoData()
        self.hideNoData()
        self.feedHideNoData()
    }
    
    func trackSearchCalculate(userFilterData:FilterData? = nil){
        var filter = FilterData()
        var trackSearchOfUser = TrackSearchOfUser()
        var jsonString = ""
        
        if trackSearchDictionary.count > 0{
            jsonString = convertIntoJSONString(arrayObject: trackSearchDictionary)!
            print("jsonString - \(jsonString)")
        }
        
        let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
        if selectedIndex == 0{
            trackSearchOfUser.searchBarLocation = AppConstants.TYPE_SEARCH_BAR_LOCATION_FEED_LISTINGS
        }else{
            trackSearchOfUser.searchBarLocation = AppConstants.TYPE_SEARCH_BAR_LOCATION_BROWSE_LISTINGS
        }
        
        if userFilterData != nil && userFilterData?.searchText != nil{
            filter = userFilterData!
            filter.searchText = userFilterData?.searchText ?? ""
            
            trackSearchOfUser.searchString = userFilterData?.searchText ?? ""
            if jsonString.count > 0{
                trackSearchOfUser.searchFilters = jsonString
            }
            self.trackSearch(trackSearchOfUser: trackSearchOfUser)
        }else if userFilterData != nil{
            filter = userFilterData!
            if jsonString.count > 0{
                trackSearchOfUser.searchFilters = jsonString
            }
            trackSearchOfUser.searchString = userFilterData?.searchText ?? ""
            self.trackSearch(trackSearchOfUser: trackSearchOfUser)
        }else{
            filter.searchText = userFilterData?.searchText ?? ""
            trackSearchOfUser.searchString = userFilterData?.searchText ?? ""
            if userFilterData?.searchText?.count ?? 0 > 0{
                self.trackSearch(trackSearchOfUser: trackSearchOfUser)
            }
        }
    }
    
    func addRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        collectionView.addSubview(refreshControl)
    }
    @objc func refreshList(){
        currentPage = 1
        isApiCalled = false
        self.feedListingsHandler.isLoadMoreRequired = true
        
        let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
        
        var commonFilterData = FilterData()
        
        if selectedIndex == 0{
            let feedFilterData = ListingsFeedViewController.feedFilterData
            if (feedFilterData.brandIds?.count ?? 0) > 0 || (feedFilterData.categoryIds?.count ?? 0) > 0 || (feedFilterData.colorIds?.count ?? 0 > 0) || (feedFilterData.conditionIds?.count ?? 0 > 0) || (feedFilterData.sizeIds?.count ?? 0 > 0) || (feedFilterData.customMinPrice ?? 0 > 0) || (feedFilterData.customMaxPrice ?? 0 > 0) || searchText.trimmingCharacters(in: .whitespacesAndNewlines).count > 0{
                commonFilterData = feedFilterData
                commonFilterData.searchText = searchText
                self.getFeedListings(data:commonFilterData)
            }else{
                self.getFeedListings()
            }
        }else if selectedIndex == 1{
            let browseFilterData = ListingsFeedViewController.browseFilterData
            if (browseFilterData.brandIds?.count ?? 0) > 0 || (browseFilterData.categoryIds?.count ?? 0) > 0 || (browseFilterData.colorIds?.count ?? 0 > 0) || (browseFilterData.conditionIds?.count ?? 0 > 0) || (browseFilterData.sizeIds?.count ?? 0 > 0) || (browseFilterData.customMinPrice ?? 0 > 0) || (browseFilterData.customMaxPrice ?? 0 > 0) || searchText.trimmingCharacters(in: .whitespacesAndNewlines).count > 0{
                commonFilterData = browseFilterData
                commonFilterData.searchText = searchText
                self.getFeedListings(data:commonFilterData)
            }else{
                self.getFeedListings()
            }
        }else{
            let myLikesFilterData = ListingsFeedViewController.myLikesFilterData
            if (myLikesFilterData.brandIds?.count ?? 0) > 0 || (myLikesFilterData.categoryIds?.count ?? 0) > 0 || (myLikesFilterData.colorIds?.count ?? 0 > 0) || (myLikesFilterData.conditionIds?.count ?? 0 > 0) || (myLikesFilterData.sizeIds?.count ?? 0 > 0) || (myLikesFilterData.customMinPrice ?? 0 > 0) || (myLikesFilterData.customMaxPrice ?? 0 > 0) {
                commonFilterData = myLikesFilterData
                self.getFeedListings(data:commonFilterData)
            }else{
                self.getFeedListings()
            }
        }
    }
    
    func onHeartClickButton(id:Int){
        feedViewModel.updateLikeStatus(accessToken: AppDefaults.getAccessToken(), id: id, {(response) in
            if response.response?.statusCode == 200 {
                
            }else if response.response?.statusCode == 500 {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG_TRY_AGAIN_LATER)
            }else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func setDelegate(feedScroll: FeedScrollProtocol) {
        feedListingsHandler.scrollDelegate = feedScroll
    }
}

extension ListingsFeedViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: AppConstants.Strings.LISTINGS_TAB)
    }
}
