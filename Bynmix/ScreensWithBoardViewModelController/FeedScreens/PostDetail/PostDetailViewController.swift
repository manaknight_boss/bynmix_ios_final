import UIKit
import Alamofire

class PostDetailViewController: BaseViewController,ReportAnIssueUserProtocol {
    
    func reportAnIssueUser() {
        self.navigationController?.popViewController(animated: false)
        self.reportAnIssueSuccessMessageDialog()
    }
    
    @IBOutlet var userImageView: UIImageView!
    @IBOutlet var userShadowView: UIView!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var heartButton: UIButton!
    @IBOutlet var shareButton: UIButton!
    @IBOutlet var middleView: UIView!
    @IBOutlet var warningButton: UIButton!
    @IBOutlet var itemImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var userTwoImageView: UIImageView!
    @IBOutlet var userTwoShadowView: UIView!
    @IBOutlet var usernameTwoLabel: UILabel!
    @IBOutlet var timeTwoLabel: UILabel!
    @IBOutlet var shareTwoButton: UIButton!
    @IBOutlet var heartTwoButton: UIButton!
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var linkButton: UIButton!
    @IBOutlet var dotsView: UIView!
    @IBOutlet var dotsLabel: UILabel!
    @IBOutlet var videoImageView: UIImageView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    
    var feed:Feed? = nil
    var feedViewModel = FeedViewModel()
    var dots:[Dots] = []
    var redDot:Dots?
    var yellowDot:Dots?
    var purpleDot:Dots?
    var greenDot:Dots?
    var blueDot:Dots?
    var id:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.Strings.POST_DETAIL
        if feed != nil{
            setData()
            getPostDetail(id:(self.feed?.id!)!)
        }else{
            getPostDetail(id:self.id!)
        }
        onUserShadowClick()
        let backBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.BACK_BUTTON), style: .plain, target: self, action: #selector(backBttonClick))
        self.navigationItem.leftBarButtonItem  = backBarButton
    }
    
    @objc func backBttonClick(){
        self.navigationController?.popViewController(animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        userImageView.layer.borderWidth = 2.0
        userImageView.layer.masksToBounds = false
        userImageView.layer.borderColor = UIColor.white.cgColor
        userImageView.clipsToBounds = true
        userImageView.layer.cornerRadius = userImageView.frame.size.width/2
        
        userShadowView.layer.cornerRadius = userShadowView.frame.size.width/2
        userShadowView.layer.shadowColor = UIColor.black.cgColor
        userShadowView.alpha = CGFloat(0.26)
        userShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        userShadowView.layer.shadowOpacity = 1
        userShadowView.layer.zPosition = -1
        
        userTwoImageView.layer.borderWidth = 2.0
        userTwoImageView.layer.masksToBounds = false
        userTwoImageView.layer.borderColor = UIColor.white.cgColor
        userTwoImageView.clipsToBounds = true
        userTwoImageView.layer.cornerRadius = userTwoImageView.frame.size.width/2
        
        userTwoShadowView.layer.cornerRadius = userTwoShadowView.frame.size.width/2
        userTwoShadowView.layer.shadowColor = UIColor.black.cgColor
        userTwoShadowView.alpha = CGFloat(0.26)
        userTwoShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        userTwoShadowView.layer.shadowOpacity = 1
        userTwoShadowView.layer.zPosition = -1
    }
    
    func setData() {
        let image : UIImage
        
        usernameLabel.text = feed?.username
        timeLabel.text = feed?.createdDateFormatted
        
        if feed!.postType == AppConstants.VIDEO_POST {
            image =  UIImage(named: AppConstants.Images.ITEM_VIDEO_PLACEHOLDER)!
            self.videoImageView.isHidden = false
            self.videoImageView.image = UIImage(named: AppConstants.Images.VIDEO_ICON)
            self.linkButton.setTitle(AppConstants.Strings.WATCH_VIDEO, for: UIControl.State.normal)
        }else {
            self.videoImageView.isHidden = true
            image =  UIImage(named: AppConstants.Images.ITEM_PLACEHOLDER)!
            self.linkButton.setTitle(AppConstants.Strings.CONTINUE_READING, for: UIControl.State.normal)
        }
        
        if let imageUrl = feed!.photoUrl{
            itemImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        } else {
            itemImageView.image =  image
        }
        
        titleLabel.text = feed?.title
        
        if let imageUrl = feed!.userPhotoUrl{
            userImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
            userTwoImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        } else {
            userImageView.image =  image
            userTwoImageView.image =  image
        }
        
        usernameTwoLabel.text = feed?.username
        timeTwoLabel.text = feed?.createdDateFormatted
        
        descriptionLabel.text = feed?.description
        
        if feed?.hasPostDots == true {
            dotsView.isHidden = false
        }else {
            dotsView.isHidden = true
        }
        if feed!.userId != AppDefaults.getUserId(){
            self.heartButton.isHidden = false
            self.heartTwoButton.isHidden = false
        }else{
            self.heartButton.isHidden = true
            self.heartTwoButton.isHidden = true
        }
        
        if feed!.userLiked!{
            heartButton.setImage(UIImage(named: AppConstants.Images.LISTING_DETAIL_HEART_SELECTED), for: .normal)
            heartTwoButton.setImage(UIImage(named: AppConstants.Images.LISTING_DETAIL_HEART_SELECTED), for: .normal)
        }else{
            heartButton.setImage(UIImage(named: AppConstants.Images.LISTING_DETAIL_HEART), for: .normal)
            heartTwoButton.setImage(UIImage(named: AppConstants.Images.LISTING_DETAIL_HEART), for: .normal)
        }
        
        let buttonIcon = UIImage(named: AppConstants.Images.POST_DETAIL_BAR_BUTTON)
        
        let rightBarButton = UIBarButtonItem(image: buttonIcon,style: UIBarButtonItem.Style(rawValue: UIBarButtonItem.Style.done.rawValue)!, target: self, action: #selector(onMyPostsClick))
        self.navigationItem.rightBarButtonItem = rightBarButton
    
    }
    
    @objc func onMyPostsClick(_ sender:UIBarButtonItem!)
    {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.MY_LIKES_BOARD, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.MY_LIKES_SCREEN) as! MyLikesViewController
        viewController.currentTabIndex = AppConstants.SET_POST_DEFAULT_TAB
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func heartButtonMethod() {
        
        if heartButton.hasImage(named: AppConstants.Images.LISTING_DETAIL_HEART, for: .normal)  && heartTwoButton.hasImage(named: AppConstants.Images.LISTING_DETAIL_HEART, for: .normal) {
            heartButton.setImage(UIImage(named: AppConstants.Images.LISTING_DETAIL_HEART_SELECTED), for: .normal)
            heartTwoButton.setImage(UIImage(named: AppConstants.Images.LISTING_DETAIL_HEART_SELECTED), for: .normal)
            self.onHeartPostClickButton(id: feed!.id!)
        } else {
            heartButton.setImage(UIImage(named: AppConstants.Images.LISTING_DETAIL_HEART), for: .normal)
            heartTwoButton.setImage(UIImage(named: AppConstants.Images.LISTING_DETAIL_HEART), for: .normal)
            self.onHeartPostClickButton(id: feed!.id!)
        }
    }
    
    @IBAction func onShareButtonClick(_ sender: UIButton) {
        
    }
    
    @IBAction func onHeartButtonClick(_ sender: UIButton) {
        heartButtonMethod()
    }
    
    @IBAction func onShareButtonTwoClick(_ sender: UIButton) {
        
    }
    
    @IBAction func onHeartButtonTwoClick(_ sender: UIButton) {
        heartButtonMethod()
    }
    
    
    @IBAction func onContinueReadingClick(_ sender: UIButton) {
        self.inAppWebBrowserScreen(link: feed?.link, title: nil)
    }
    
    @IBAction func onDotsLabelClick(_ sender: UITapGestureRecognizer) {
        if dotsLabel.text == AppConstants.Strings.TAG_PRODUCTS{
            dotsLabel.text = AppConstants.Strings.HIDE_TAG_PRODUCTS
            ontagsClick()
        }else{
            dotsLabel.text = AppConstants.Strings.TAG_PRODUCTS
            removeDots()
        }
    }
    
    func removeDots(){
        for view in (itemImageView.subviews) {
            view.removeFromSuperview()
        }
    }
    
    @IBAction func onPlayTapAction(_ sender: UITapGestureRecognizer) {
        self.inAppWebBrowserScreen(link: feed?.link, title: nil)
    }
    
    func onHeartPostClickButton(id:Int){
        feedViewModel.updatePostLikeStatus(accessToken: getTemporaryToken(), id: id, {(response) in
            if response.response?.statusCode == 200 {
            }else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print(error)
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            } 
        })
    }
    
    func getPostDetail(id:Int){
        feedViewModel.getPostDetail(accessToken: getAccessToken(), id: id,{(response) in
            self.screenActivityIndicator.stopAnimating()
            self.handleResponse(response:response)
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func handleResponse(response:AFDataResponse<Any>){
        if response.response?.statusCode == 200 {
            let data: ApiResponse<Feed> = Utils.convertResponseToData(data: response.data!)
            if data.isError{
                let error = Utils.getErrorMessage(errors: data.errors)
                print(error)
            } else {
                if let list = data.data{
                    if self.feed == nil{
                        self.feed = list
                        self.setData()
                    }
                    if list.canUserReportAnIssue!{
                        self.warningButton.isHidden = false
                    }else{
                        self.warningButton.isHidden = true
                    }
                    self.scrollView.isHidden = false
                    if list.postDots!.count > 0{
                        self.dots = list.postDots!
                    }
                }
            }
        }
    }
    
    func onUserShadowClick(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.userShadowClick))
        userShadowView.addGestureRecognizer(tap)
        
        let userTwoShadowTap = UITapGestureRecognizer(target: self, action: #selector(self.userTwoShadowClick))
        userTwoShadowView.addGestureRecognizer(userTwoShadowTap)
    }
    
    @objc func userShadowClick(_ sender: UITapGestureRecognizer)
    {
       otherUserClick()
    }
    
    @objc func userTwoShadowClick(_ sender: UITapGestureRecognizer)
    {
        otherUserClick()
    }
    
    @IBAction func onUserImageAction(_ sender: UITapGestureRecognizer) {
        otherUserClick()
    }
    
    @IBAction func onUserNameAction(_ sender: UITapGestureRecognizer) {
        otherUserClick()
    }
    
    @IBAction func onUserImageByAction(_ sender: UITapGestureRecognizer) {
        otherUserClick()
    }
    
    @IBAction func onUsernameByAction(_ sender: UITapGestureRecognizer) {
        otherUserClick()
    }
    
    @IBAction func reportAnIssueButtonAction(_ sender: UIButton) {
        reportAnIssueScreen()
    }
    
    func reportAnIssueScreen(){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.REPORT_AN_ISSUE_FEED_STORYBOARD, bundle: nil)
        let reportAnIssueViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.REPORT_AN_ISSUE_FEED_SCREEN) as! ReportAnIssueFeedViewController
        reportAnIssueViewController.TYPE = AppConstants.TYPE_REPORT_AN_ISSUE_POST
        reportAnIssueViewController.feed = self.feed!
        reportAnIssueViewController.reportAnIssueUserDelegate = self
        self.navigationController?.pushViewController(reportAnIssueViewController, animated: false)
    }
    
    func otherUserClick(){
        if feed?.userId! == AppDefaults.getUserId(){
            //self.tabBarController?.selectedIndex = 4
            self.openUserProfile()
        }else{
            self.openProfile(id:feed!.userId!,username:feed!.username!)
        }
        
    }
    
    func ontagsClick(){        
        let dots = self.dots
        for i in 0..<dots.count{
            self.setDot(index: i)
        }
    }
    
    func setDot(index: Int) {
        var dotImage: UIImage
        if dots[index].hexColor == AppConstants.DOT_COLOR_RED{
            dotImage = UIImage(named: AppConstants.Images.VIEW_RED_ICON)!
        }else if dots[index].hexColor == AppConstants.DOT_COLOR_YELLOW{
            dotImage = UIImage(named: AppConstants.Images.VIEW_YELLOW_ICON)!
        }else if dots[index].hexColor == AppConstants.DOT_COLOR_BLUE{
            dotImage = UIImage(named: AppConstants.Images.VIEW_BLUE_ICON)!
        }else if dots[index].hexColor == AppConstants.DOT_COLOR_GREEN{
            dotImage = UIImage(named: AppConstants.Images.VIEW_GREEN_ICON)!
        }else {
            dotImage = UIImage(named: AppConstants.Images.VIEW_PURPLE_ICON)!
        }
        
        let imageWidth = Double(self.itemImageView.frame.width)
        let imageHeight = Double(self.itemImageView.frame.height)
        let x = dots[index].xCoordinate! * imageWidth
        let y = dots[index].yCoordinate! * imageHeight
        
        let dotImageWidth = Double((dotImage.size.width))
        let dotImageHeight = Double((dotImage.size.height))
        let dot = UIImageView(frame: CGRect(x: x - (dotImageWidth/2), y: y - (dotImageHeight), width: dotImageWidth, height: dotImageHeight))
        dot.image = dotImage
        dot.contentMode = .scaleAspectFit
        self.itemImageView.addSubview(dot)
    
        let dotTap = UITapGestureRecognizer(target: self, action: #selector(self.onDotTapDetected))
        dot.tag = index
        dot.isUserInteractionEnabled = true
        dot.addGestureRecognizer(dotTap)
    }
    
    @objc func onDotTapDetected(sender: UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        self.postDotType(dots: self.dots[index])
    }
    
    @objc func onRedDotTapDetected(tapGestureRecognizer: UITapGestureRecognizer){
        self.postDotType(dots: self.redDot!)
    }
    
    @objc func onYellowDotTapDetected(tapGestureRecognizer: UITapGestureRecognizer){
        self.postDotType(dots: self.yellowDot!)
    }
    
    @objc func onBlueDotTapDetected(tapGestureRecognizer: UITapGestureRecognizer){
        self.postDotType(dots: self.blueDot!)
    }
    
    @objc func onGreenDotTapDetected(tapGestureRecognizer: UITapGestureRecognizer){
        self.postDotType(dots: self.greenDot!)
    }
    
    @objc func onPurpleDotTapDetected(tapGestureRecognizer: UITapGestureRecognizer){
        self.postDotType(dots: self.purpleDot!)
    }
    
    func postDotType(dots:Dots){
        if(dots.postDotType!.caseInsensitiveCompare(AppConstants.TYPE_POST_DOT_LISTING) == .orderedSame){
            self.listingScreen(listingId: dots.listingId!)
        }else{
            self.inAppWebBrowserScreen(dots: dots)
        }
    }
    
    func listingScreen(listingId:Int){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.LISTING_DETAIL_BOARD, bundle: nil)
        let listingDetailViewController = storyboard.instantiateViewController(withIdentifier:AppConstants.Controllers.Listing_Detail_Screen) as! ListDetailViewController
        listingDetailViewController.itemId = listingId
        self.navigationController?.pushViewController(listingDetailViewController, animated: true)
    }
    
    func inAppWebBrowserScreen(dots:Dots){
        self.inAppWebBrowserScreen(link: dots.link, title: dots.title)
    }
    
}

