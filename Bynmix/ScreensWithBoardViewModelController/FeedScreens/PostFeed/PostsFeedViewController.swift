import UIKit
import XLPagerTabStrip
import Alamofire

class PostsFeedViewController: BaseViewController,LoadMoreProtocol {
    
    func onLoadMore() {
        if !isApiCalled {
            isApiCalled = true
            getFeedPosts()
        }
    }
    
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var tableViewLayout: UITableView!
    @IBOutlet var noDataShadowView: UIView!
    @IBOutlet var noDataView: UIView!
    @IBOutlet var noDataImageView: UIImageView!
    @IBOutlet var noDataLabel: UILabel!
    
    let feedPostsHandler = FeedPostsHandler()
    var feed:Feed? = nil
    var refreshControl:UIRefreshControl?
    var currentPage = 1
    var isApiCalled = false
    var viewControllerType:Int? = nil
    var feedViewModel = FeedViewModel()
    var profileViewModel = ProfileViewModel()
    var dots: [Dots] = []
    let milliSecs = 0.60
    let milliSeconds = 0.25
    var searchText = ""
    var feedOfUser : [Feed] = []
    var feedOfAll : [Feed] = []
    var feedOfMyLikes : [Feed] = []
    var isFeedListCleared = false
    var TYPE:Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableViewLayout.rowHeight = UITableView.automaticDimension
        tableViewLayout.isHidden = true
        tableViewLayout.delegate = feedPostsHandler
        tableViewLayout.dataSource = feedPostsHandler
        getItemsClick()
        
        self.tableViewLayout.contentInset = UIEdgeInsets(top: 40, left: 0, bottom: 0, right: 0)
        addRefreshControl()
        feedPostsHandler.loadMoreDelegate = self
        self.feedPoststableView = tableViewLayout
        hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        noDataShadowView.layer.cornerRadius = 2
        noDataShadowView.layer.shadowColor = UIColor.black.cgColor
        noDataShadowView.alpha = CGFloat(0.26)
        noDataShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        noDataShadowView.layer.shadowOpacity = 1
        noDataShadowView.layer.zPosition = -1
        
        searchTextOnTabChange()
        
        NotificationCenter.default.addObserver(self,selector: #selector(feedPostsSearchText),name:Notification.Name(AppConstants.Notifications.FEED_POSTS_BAR_BUTTON),object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(false)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: AppConstants.Notifications.FEED_POSTS_BAR_BUTTON), object: nil)
    }
    
    func searchTextOnTabChange(){
        if self.TYPE == AppConstants.TYPE_MY_LIKES_POSTS{
            
        }else{
            let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
            if selectedIndex == 0{
                searchText = AppConstants.SEARCH_TEXT_FOR_FEED
                if AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_FEED_POST{
                    if self.feedOfUser.count > 0{
                        self.feedOfUser.removeAll()
                    }
                }
            }else if selectedIndex == 1{
                searchText = AppConstants.SEARCH_TEXT_FOR_BROWSE
                if AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_BROWSE_POST{
                    if self.feedOfAll.count > 0 {
                        self.feedOfAll.removeAll()
                    }
                }
            }
        }
       
        DispatchQueue.main.asyncAfter(deadline: .now() + milliSeconds) {
            if self.searchText == ""{
                if self.TYPE == AppConstants.TYPE_MY_LIKES_POSTS{
                    if self.feedOfMyLikes.count <= 0{
                        self.getFeedPosts()
                    }
                }else{
                    let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
                    if selectedIndex == 0{
                        if  self.feedOfUser.count == 0{
                            self.showActivityIndicator()
                            self.getFeedPosts()
                            self.searchButtonClickedForFeedPost()
                        }
                    }else if selectedIndex == 1{
                        if self.feedOfAll.count == 0{
                            self.showActivityIndicator()
                            self.getFeedPosts()
                            self.searchButtonClickedForBrowsePost()
                        }
                    }
                }
            }else{
                var filterData = FilterData()
                filterData.searchText = self.searchText
                let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
                if selectedIndex == 0{
                    if AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_FEED_POST{
                        if self.feedOfUser.count == 0{
                            self.showActivityIndicator()
                            self.getFeedPosts(data:filterData)
                            self.searchButtonClickedForFeedPost()
                        }
                    }
                }else if selectedIndex == 1{
                    if AppConstants.IS_SEARCH_CLEAR_BUTTON_CLICKED_FOR_BROWSE_POST{
                        if self.feedOfAll.count == 0{
                            self.showActivityIndicator()
                            self.getFeedPosts(data:filterData)
                            self.searchButtonClickedForBrowsePost()
                        }
                    }
                }
            }
        }
    }
    
    func showActivityIndicator(){
        if self.tableViewLayout.isHidden == false{
            self.view.makeToastActivity(.center)
        }
    }
    
    @objc func feedPostsSearchText(_ notification: Notification) {
        self.view.makeToastActivity(.center)
        if self.feedPostsHandler.feed.count > 0{
            self.feedPostsHandler.feed.removeAll()
        }
        let barbuttonEnable:String = notification.userInfo?[AppConstants.Notifications.FEED_POSTS_SEARCH_TEXT] as! String
        
        let searchText = barbuttonEnable
        self.searchText = searchText
        var filterData = FilterData()
        if searchText != ""{
            self.showLoader()
            currentPage = 1
            filterData.searchText = searchText
            DispatchQueue.main.asyncAfter(deadline: .now() + milliSecs) {
                self.getFeedPosts(data:filterData)
                let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
                if selectedIndex == 0{
                    self.searchButtonClickedForFeedPost()
                }else{
                    self.searchButtonClickedForBrowsePost()
                }
            }
        }else{
            currentPage = 1
            self.showLoader()
            DispatchQueue.main.asyncAfter(deadline: .now() + milliSecs) {
                self.getFeedPosts()
                let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
                if selectedIndex == 0{
                    self.searchButtonClickedForFeedPost()
                }else{
                    self.searchButtonClickedForBrowsePost()
                }
            }
        }
        
    }
    
    func showLoader(){
        self.view.makeToastActivity(.center)
    }
    
    func getItemsClick(){
        feedPostsHandler.itemClickHandler = { feed in
            let storyboard = UIStoryboard(name: AppConstants.Storyboard.FOLLOW_FOLLOWING_BOARD, bundle: nil)
            let likeViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.FOLLOWERS_FOLLOWING) as! FollowersFollowingViewController
            if(feed.type == AppConstants.POST) {
                likeViewController.TYPE = AppConstants.TYPE_LIKE_POST
            } else {
                likeViewController.TYPE = AppConstants.TYPE_LIKE_LISTING
            }
            likeViewController.ID = feed.id
            self.navigationController?.pushViewController(likeViewController, animated: true)
        }
        feedPostsHandler.onItemImageViewClick = { feed in
            let storyboard = UIStoryboard(name: AppConstants.Storyboard.POST_DETAIL_BOARD, bundle: nil)
            let postDetailViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.POST_DETAIL_SCREEN) as! PostDetailViewController
            postDetailViewController.feed = feed
            if feed.type == AppConstants.POST {
                self.navigationController?.pushViewController(postDetailViewController, animated: true)
            }
        }
        
        feedPostsHandler.onHeartButtonClick = { feed in
            self.onHeartPostClickButton(id: feed.id ?? 0)
        }
        
        feedPostsHandler.onOtherUserClick = { feed in
            if feed.userId ?? 0 == AppDefaults.getUserId(){
                self.openUserProfile()
            }else{
                self.openProfile(id:feed.userId ?? 0,username:feed.username ?? "")
            }
        }
        
        feedPostsHandler.onTagsViewClick = { index, feed in
            self.onTagsButtonClick(index: index, feed: feed)
        }
        
        feedPostsHandler.onDotClick = { dot in
            self.postDotType(dot: dot)
        }
        
    }
    
    func onTagsButtonClick(index: Int, feed:Feed){
        if feed.postDots != nil && feed.postDots!.count > 0 {
            var feedTemp = feed
            feedTemp.containDots = true
            var feeds = self.feedPostsHandler.feed
            feeds[index] = feedTemp
            
            self.feedPostsHandler.feed = feeds
            let position = IndexPath(row: index, section: 0)
            self.tableViewLayout.reloadRows(at: [position], with: .none)
        } else {
            self.view.makeToastActivity(.center)
            feedViewModel.getDots(accessToken: getTemporaryToken(), id: feed.id!, {(response) in
                self.view.hideToastActivity()
                if response.response?.statusCode == 200 {
                    let data: ApiResponse<[Dots]> = Utils.convertResponseToData(data: response.data!)
                    if data.isError {
                        let error = Utils.getErrorMessage(errors: data.errors)
                        print("error",error)
                    }else{
                        self.dots = data.data!
                        var feedTemp = feed
                        feedTemp.postDots = self.dots
                        feedTemp.containDots = true
                        var feeds = self.feedPostsHandler.feed
                        feeds.remove(at: index)
                        feeds.insert(feedTemp, at: index)
                        self.feedPostsHandler.feed = feeds
                        let position = IndexPath(row: index, section: 0)
                        self.tableViewLayout.reloadRows(at: [position], with: .none)
                        
                        
                    }
                }else {
                    let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                    if data.isError {
                        let error = Utils.getErrorMessage(errors: data.errors)
                        print(error)
                    }
                }
            } , { (error) in
                self.view.hideToastActivity()
                if error.statusCode == AppConstants.NO_INTERNET {
                    self.showInternetError()
                }
            })
        }
    }
    
    func updateDots(feed:Feed,dots:[Dots]){
        for i in 0..<self.feedPostsHandler.feed.count {
            if self.feedPostsHandler.feed[i].id == feed.id {
                self.feedPostsHandler.feed[i].containDots! = true
                self.feedPostsHandler.feed[i].postDots = dots
            }else{
                self.feedPostsHandler.feed[i].containDots! = false
            }
        }
    }
    
    func postDotType(dot:Dots){
        if(dot.postDotTypeId! == AppConstants.TYPE_ID_MY_LISTING){
            self.listingScreen(listingId: dot.listingId!)
        }else{
            self.inAppWebBrowserScreen(link: dot.link, title: dot.title)
        }
    }
    
    func listingScreen(listingId:Int){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.LISTING_DETAIL_BOARD, bundle: nil)
        let listingDetailViewController = storyboard.instantiateViewController(withIdentifier:AppConstants.Controllers.Listing_Detail_Screen) as! ListDetailViewController
        listingDetailViewController.itemId = listingId
        self.navigationController?.pushViewController(listingDetailViewController, animated: true)
    }
    
    func getFeedPosts(data:FilterData? = nil) {
        var filters: FilterData
        if data == nil {
            filters = FilterData()
        } else {
            filters = data!
        }
//        if self.feedPostsHandler.feed.count > 0 && data != nil{
//            self.feedPostsHandler.feed.removeAll()
//        }
        
        if self.TYPE == AppConstants.TYPE_MY_LIKES_POSTS{
            profileViewModel.getLikesPost(accessToken: getAccessToken(), page: currentPage,{(response) in
                self.view.hideToastActivity()
                self.handleResponse(response:response)
            } , { (error) in
                self.view.hideToastActivity()
                self.refreshControl?.endRefreshing()
                if error.statusCode == AppConstants.NO_INTERNET {
                    self.showInternetError()
                }
            })
        }else{
            feedViewModel.getFeedPosts(accessToken: getAccessToken(), data: filters,page:currentPage,{(response) in
                self.view.hideToastActivity()
                self.handleResponse(response:response,filters:filters)
            } , { (error) in
                self.view.hideToastActivity()
                self.refreshControl?.endRefreshing()
                if error.statusCode == AppConstants.NO_INTERNET {
                    self.showInternetError()
                }
            })
        }
    }
    
    func handleResponse(response:AFDataResponse<Any>,filters:FilterData? = nil){
        self.isApiCalled = false
        self.refreshControl?.endRefreshing()
        if response.response?.statusCode == 200 {
            self.screenActivityIndicator.stopAnimating()
            let data: ApiResponse<Feed> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                print(error)
            } else {
                let feed = data.data
                if var feeds = feed?.items {
                    for i in 0..<feeds.count{
                        feeds[i].isActive = true
                        feeds[i].containDots = false
                    }
                    if self.currentPage == 1 {
                        self.feedPostsHandler.feed = feeds
                    } else {
                        self.feedPostsHandler.feed.append(contentsOf: feeds)
                    }
                    let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
                    if selectedIndex == 0{
                        self.feedOfUser = feeds
                    }else if selectedIndex == 1{
                        self.feedOfAll = feeds
                    }else{
                        self.feedOfMyLikes = feeds
                    }
                    if feed?.totalPages == self.currentPage {
                        self.isApiCalled = true
                        self.feedPostsHandler.isLoadMoreRequired = false
                    }
                    self.tableViewLayout.reloadData()
                    self.screenActivityIndicator.stopAnimating()
                    self.tableViewLayout.isHidden = false
                    self.noDataShadowView.isHidden = true
                    self.noDataView.isHidden = true
                }
            }
            self.currentPage = self.currentPage + 1
            
        }else if response.response?.statusCode == 404 {
            self.noDataShadowView.isHidden = false
            self.noDataView.isHidden = false
            if self.feedPostsHandler.feed.count > 0{
                self.feedPostsHandler.feed.removeAll()
            }
            self.tableViewLayout.reloadData()
            if AppConstants.CURRENT_TAB == 0 || AppConstants.CURRENT_TAB == 1{
                self.noDataLabel.text = AppConstants.Strings.NOTHING_TO_SHOW
            }else if AppConstants.CURRENT_TAB == 4{
                self.noDataLabel.text = AppConstants.Strings.MY_LIKES_NO_POST
            }
            self.noDataImageView.image = UIImage(named: AppConstants.Images.LOGO_BLACK)
            
        } else {
            self.noDataView.isHidden = false
            self.noDataShadowView.isHidden = false
            if self.feedPostsHandler.feed.count > 0{
                self.feedPostsHandler.feed.removeAll()
            }
            self.tableViewLayout.reloadData()
            self.noDataLabel.text = AppConstants.Strings.SOMETHING_WRONG_HERE
            self.noDataImageView.image = UIImage(named: AppConstants.Images.LOGO_SAD_CLOUD)
            let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                print(error)
            }
        }
        
        if filters?.searchText != nil && filters?.searchText != ""{
            self.trackSearchCalculate(userFilterData: filters)
        }
    }
    
    
    func addRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        tableViewLayout.addSubview(refreshControl!)
    }
    
    @objc func refreshList(){
        currentPage = 1
        isApiCalled = false
        self.feedPostsHandler.isLoadMoreRequired = true
        if self.searchText != ""{
            var filterData = FilterData()
            filterData.searchText = searchText
            DispatchQueue.main.asyncAfter(deadline: .now() + milliSecs) {
                self.getFeedPosts(data:filterData)
            }
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + milliSecs) {
                self.getFeedPosts()
            }
        }
    }
    
    func onHeartPostClickButton(id:Int){
        feedViewModel.updatePostLikeStatus(accessToken: getTemporaryToken(), id: id, {(response) in
            if response.response?.statusCode == 200 {
            }else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print(error)
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func trackSearchCalculate(userFilterData:FilterData? = nil){
        var filter = FilterData()
        var trackSearchOfUser = TrackSearchOfUser()
        var jsonString = ""
        
        if trackSearchDictionary.count > 0{
            jsonString = convertIntoJSONString(arrayObject: trackSearchDictionary)!
            print("jsonString - \(jsonString)")
        }
        
        let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
        if selectedIndex == 0{
            trackSearchOfUser.searchBarLocation = AppConstants.TYPE_SEARCH_BAR_LOCATION_FEED_POSTS
        }else{
            trackSearchOfUser.searchBarLocation = AppConstants.TYPE_SEARCH_BAR_LOCATION_BROWSE_POSTS
        }
        
        if userFilterData != nil && userFilterData?.searchText != nil{
            filter = userFilterData!
            filter.searchText = userFilterData?.searchText ?? ""
            
            trackSearchOfUser.searchString = userFilterData?.searchText ?? ""
            if jsonString.count > 0{
                trackSearchOfUser.searchFilters = jsonString
            }
            self.trackSearch(trackSearchOfUser: trackSearchOfUser)
        }else if userFilterData != nil{
            filter = userFilterData!
            if jsonString.count > 0{
                trackSearchOfUser.searchFilters = jsonString
            }
            self.trackSearch(trackSearchOfUser: trackSearchOfUser)
        }else{
            filter.searchText = userFilterData?.searchText ?? ""
            trackSearchOfUser.searchString = userFilterData?.searchText ?? ""
            if userFilterData?.searchText?.count ?? 0 > 0{
                self.trackSearch(trackSearchOfUser: trackSearchOfUser)
            }
        }
    }
    
    func setDelegate(feedScroll: FeedScrollProtocol) {
        feedPostsHandler.scrollDelegate = feedScroll
    }
    
}

extension PostsFeedViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: AppConstants.Strings.POSTS_TAB)
    }
}
