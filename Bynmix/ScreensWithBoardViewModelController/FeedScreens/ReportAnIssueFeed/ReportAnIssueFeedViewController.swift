import UIKit

class ReportAnIssueFeedViewController: BaseViewController {
    
    @IBOutlet weak var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var buttonActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var mainScreenView: UIView!
    @IBOutlet weak var issueTextView: UITextView!
    @IBOutlet weak var dropDownLabel: UILabel!
    
    var feedViewModel = FeedViewModel()
    var reportedIssueTypeSelect = [ReportedIssueType]()
    var TYPE_REPORT_AN_ISSUE = 0
    var selectedIssueTypes = 0
    var isIssueSelected = false
    var isIssueDescribed = false
    
    var TYPE:Int?
    var listingDetail:ListingDetail?
    var feed:Feed?
    var user:User?
    var reportAnIssueUserDelegate:ReportAnIssueUserProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        makeButtonDisable()
        getReportedIssues()
        dropDownLabel.addObserver(self, forKeyPath: "text", options: [.old, .new], context: nil)
        hideKeyboardWhenTappedAround()
        navigationItem.title = AppConstants.Strings.REPORT_AN_ISSUE_TITLE
        buttonActivityIndicator.isHidden = true
        issueTextView.text = AppConstants.Strings.REASON_OF_REPORTING
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "text" {
            let previousIssue:String = change?[.oldKey] as! String
            if previousIssue == AppConstants.Strings.SELECT_AN_ISSUE_TYPE{
                self.isIssueSelected = true
            }
            if isIssueSelected{
                 checkConditionMet()
            }
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if issueTextView.text.trimmingCharacters(in: .whitespacesAndNewlines) == AppConstants.Strings.REASON_OF_REPORTING {
            issueTextView.text = nil
            issueTextView.textColor = UIColor.black
            issueTextView.font = UIFont.fontRegular(size: 18)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if issueTextView.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
            issueTextView.text = AppConstants.Strings.REASON_OF_REPORTING
            issueTextView.textColor = UIColor.cCDCDCD
            issueTextView.font = UIFont.fontRegular(size: 18)
            self.isIssueDescribed = false
        }else{
            if issueTextView.text.trimmingCharacters(in: .whitespacesAndNewlines) != AppConstants.Strings.REASON_OF_REPORTING{
                self.isIssueDescribed = true
            }else{
                self.isIssueDescribed = false
            }
        }
        checkConditionMet()
    }
    
    func checkConditionMet(){
        if isIssueSelected && isIssueDescribed{
            makeButtonEnable()
        }else{
            makeButtonDisable()
        }
    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        sendData()
    }
    
    func sendData() {
        self.view.makeToastActivity(.center)
        var dispute = Dispute()
        var disputeIssueId = 0
        disputeIssueId = self.selectedIssueTypes
        let comment = self.issueTextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
        dispute.comments = comment
        dispute.reportedIssueTypeId = disputeIssueId
        dispute.issueTypeId = TYPE
        if(TYPE == AppConstants.TYPE_REPORT_AN_ISSUE_LISTING) {
            dispute.entityId = listingDetail?.id
        } else if(TYPE == AppConstants.TYPE_REPORT_AN_ISSUE_POST) {
            dispute.entityId = feed?.id
        } else {
            dispute.entityId = user?.userId
        }
        sendApi(dispute:dispute)
    }
    
    func sendApi(dispute:Dispute){
        feedViewModel.reportAnIssue(accessToken: getTemporaryToken(), data: dispute, {(response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 201 {
                let data: ApiResponse<(Int)> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    self.reportAnIssueUserDelegate?.reportAnIssueUser()
                }
            }else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    @IBAction func dropDownTapAction(_ sender: UITapGestureRecognizer) {
        showDropDown(tag: TYPE_REPORT_AN_ISSUE, title:  AppConstants.Strings.SELECT_AN_ISSUE_TYPE, dataSourse: self, delegate: self, {(row) in
            self.dropDownLabel.textColor = UIColor.black
            self.dropDownLabel.text = self.reportedIssueTypeSelect[row].name
            self.selectedIssueTypes = self.reportedIssueTypeSelect[row].reportedIssueTypeId!
        })
    }
    
    func makeButtonDisable(){
        self.submitButton.isEnabled = false
        self.submitButton.backgroundColor = UIColor.cCDCDCD
    }
    
    func makeButtonEnable(){
        self.submitButton.isEnabled = true
        self.submitButton.backgroundColor = UIColor.cBB189C
    }
    
}
extension ReportAnIssueFeedViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return reportedIssueTypeSelect.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return reportedIssueTypeSelect[row].name
    }
    
    func getReportedIssues() {
        feedViewModel.getIssues(accessToken: getAccessToken(), {(response) in
            self.screenActivityIndicator.stopAnimating()
            self.mainScreenView.isHidden = false
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[ReportedIssueType]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.reportedIssueTypeSelect = data.data!
                }
            } else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            self.mainScreenView.isHidden = false
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
}

