import UIKit

class FilterListingsViewController: BaseViewController,ScrollEndedInFilterProtocol,LoadMoreProtocol, FilterSelectedProtocol {
    
    func onLoadMore() {
        if !isApiCalled {
            isApiCalled = true
            self.getBrands()
        }
    }
    
    func ScrollEndedInFilterProtocol(type:Int) {
        if type == AppConstants.TYPE_RIGHT_BUTTON{
            self.rightScrollButton.isHidden = true
            self.leftScrollButton.isHidden = false
        }else if type == AppConstants.TYPE_LEFT_BUTTON{
            self.leftScrollButton.isHidden = true
            self.rightScrollButton.isHidden = false
        }else{
            self.leftScrollButton.isHidden = false
            self.rightScrollButton.isHidden = false
        }
    }
    
    @IBOutlet weak var leftScrollButton: UIButton!
    @IBOutlet weak var rightScrollButton: UIButton!
    @IBOutlet weak var filterCollectionView: UICollectionView!
    @IBOutlet weak var conditionTableView: UITableView!
    @IBOutlet weak var applyFiltersButton: UIButton!
    @IBOutlet weak var colorsTableView: UITableView!
    @IBOutlet weak var priceTableView: UITableView!
    @IBOutlet weak var brandsTextField: UITextField!
    @IBOutlet weak var brandsTableView: UITableView!
    @IBOutlet weak var selectCategoryTableView: UITableView!
    
    @IBOutlet weak var selectSubCategoryView: UIView!
    @IBOutlet weak var subCategoryTableView: UITableView!
    @IBOutlet weak var subCategoryNameLabel: UILabel!
    
    @IBOutlet weak var sizesDetailView: UIView!
    @IBOutlet weak var sizesDetailTableView: UITableView!
    
    @IBOutlet weak var sizeNameLabel: UILabel!
    
    @IBOutlet weak var conditionTickImageView: UIImageView!
    @IBOutlet weak var sizeTickImageView: UIImageView!
    @IBOutlet weak var colorsTickImageView: UIImageView!
    @IBOutlet weak var categoryTickImageView: UIImageView!
    @IBOutlet weak var priceTickImageView: UIImageView!
    @IBOutlet weak var brandTickImageView: UIImageView!
    
    @IBOutlet weak var conditionLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var brandLabel: UILabel!
    
    @IBOutlet weak var sizesTableView: UITableView!
    
    @IBOutlet var conditionActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var sizesActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var colorActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var categoryActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var priceActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var brandsActivityIndicator: UIActivityIndicatorView!
    
    var filterClearAllDelegate:UserFilterClearAllProtocol?
    var filterApplyDelegate:UserFilterApplyProtocol?
    
    var filtersHandler = FiltersHandler()
    var filterConditionHandler = FilterConditionHandler()
    var filterColorsHandler = FilterColorsHandler()
    var filterPriceHandler = FilterPriceHandler()
    var filterBrandHandler = FilterUserBrandHandler()
    var selectCategoryHandler = FilterSelectCategoryHandler()
    var selectSubCategoryHandler = FilterSelectSubCategoryHandler()
    var selectSizesDetailHandler = FilterSizesDetailHandler()
    var filterSizesHandler = FilterSizesHandler()
    
    let profileViewModel = ProfileViewModel()
    let createViewModel = CreateViewModel()
    let editProfileViewModel = EditProfileViewModel()
    
    var allBrands : [Brand] = []
    var brands:[Brand] = []
    var conditionList:[Condition] = []
    var filterSizeTypeList:[FilterSizeType] = []
    var filterSizeList:[FilterSizes] = []
    var filteredBrands: [Brand] = []
    var priceList:[Price] = []
    var filterList:[FilterList] = []
    var colorsList:[Colors] = []
    var categoryTypeList:[CategoryType] = []
    var categoryList:[Category] = []
    static var selectedFilterList:[SelectedFilter] = []
    static var applyButtonPressed = false
    let milliSeconds = 0.25
    var onBoardingViewModel = OnBoardingViewModel()
    var currentPage = 1
    var isApiCalled = false
    var refreshControl : UIRefreshControl?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHandlers()
        navigationItem.title = "Filter Listings"
        getBrands()
        getColors()
        getSizes()
        getCategory()
        self.makeConditionBold()
        brandsTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        addPriceData()
        getCondition()
        setPriceView()
        self.conditionTableView.isHidden = false
        getItemsClick()
        setScrollButtons()
        subCategoryNameLabel.isUserInteractionEnabled = true
        self.filtersHandler.scrollEndedInFilterDelegate = self
        filterBrandHandler.loadMoreDelegate = self
        addRefreshControl()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        checkPreviousViewController()
    }
    
    func checkPreviousViewController(){
        var id  = 0
        var name = ""
        var filterType = 0
        var subType = 0
        var subTypeName = ""
        if let viewcontrollers = self.navigationController?.viewControllers {
            let previousVC = viewcontrollers[viewcontrollers.count - 2]
            if previousVC is FeedViewController {
                let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
                if selectedIndex == 0{
                    if BaseViewController.mSelectedFeedFilterList.count > 0{
                        FilterListingsViewController.selectedFilterList = BaseViewController.mSelectedFeedFilterList
                        self.filtersHandler.filter = FilterListingsViewController.selectedFilterList
                        self.filterCollectionView.reloadData()
                        for i in 0..<BaseViewController.mSelectedFeedFilterList.count{
                            let filter = BaseViewController.mSelectedFeedFilterList[i]
                            id  = filter.id!
                            name = filter.name!
                            filterType = filter.type!
                            subType = filter.subType ?? 0
                            subTypeName = filter.subTypeName ?? ""
                            self.setCheck(filterType:filterType)
                            self.setSelectedView(id: id, name: name, filterType: filterType, subType: subType, subTypeName: subTypeName)
                        }
                    }else{
                        self.removeMainList()
                    }
                }else if selectedIndex == 1{
                    if BaseViewController.mSelectedBrowseFilterList.count > 0{
                        FilterListingsViewController.selectedFilterList = BaseViewController.mSelectedBrowseFilterList
                        self.filtersHandler.filter = FilterListingsViewController.selectedFilterList
                        self.filterCollectionView.reloadData()
                        for i in 0..<BaseViewController.mSelectedBrowseFilterList.count{
                            let filter = BaseViewController.mSelectedBrowseFilterList[i]
                            id  = filter.id!
                            name = filter.name!
                            filterType = filter.type!
                            subType = filter.subType ?? 0
                            subTypeName = filter.subTypeName ?? ""
                            self.setCheck(filterType:filterType)
                            self.setSelectedView(id: id, name: name, filterType: filterType, subType: subType, subTypeName: subTypeName)
                        }
                    }else{
                        self.removeMainList()
                    }
                }
            }else if previousVC is UserBynViewController{
                if BaseViewController.mSelectedUserBynFilterList.count > 0{
                    FilterListingsViewController.selectedFilterList = BaseViewController.mSelectedUserBynFilterList
                    self.filtersHandler.filter = FilterListingsViewController.selectedFilterList
                    self.filterCollectionView.reloadData()
                    for i in 0..<BaseViewController.mSelectedUserBynFilterList.count{
                        let filter = BaseViewController.mSelectedUserBynFilterList[i]
                        id  = filter.id!
                        name = filter.name!
                        filterType = filter.type!
                        subType = filter.subType ?? 0
                        subTypeName = filter.subTypeName ?? ""
                        self.setCheck(filterType:filterType)
                        self.setSelectedView(id: id, name: name, filterType: filterType, subType: subType, subTypeName: subTypeName)
                    }
                }else{
                    self.removeMainList()
                }
            }else if previousVC is MyLikesViewController{
                if BaseViewController.mSelectedMyLikesFilterList.count > 0{
                    FilterListingsViewController.selectedFilterList = BaseViewController.mSelectedMyLikesFilterList
                    self.filtersHandler.filter = FilterListingsViewController.selectedFilterList
                    self.filterCollectionView.reloadData()
                    for i in 0..<BaseViewController.mSelectedMyLikesFilterList.count{
                        let filter = BaseViewController.mSelectedMyLikesFilterList[i]
                        id  = filter.id!
                        name = filter.name!
                        filterType = filter.type!
                        subType = filter.subType ?? 0
                        subTypeName = filter.subTypeName ?? ""
                        self.setCheck(filterType:filterType)
                        self.setSelectedView(id: id, name: name, filterType: filterType, subType: subType, subTypeName: subTypeName)
                    }
                }else{
                    self.removeMainList()
                }
            }else if previousVC is OtherUserProfileViewController{
                if BaseViewController.mSelectedOtherUserProfileFilterList.count > 0{
                    FilterListingsViewController.selectedFilterList = BaseViewController.mSelectedOtherUserProfileFilterList
                    self.filtersHandler.filter = FilterListingsViewController.selectedFilterList
                    self.filterCollectionView.reloadData()
                    for i in 0..<BaseViewController.mSelectedOtherUserProfileFilterList.count{
                        let filter = BaseViewController.mSelectedOtherUserProfileFilterList[i]
                        id  = filter.id!
                        name = filter.name!
                        filterType = filter.type!
                        subType = filter.subType ?? 0
                        subTypeName = filter.subTypeName ?? ""
                        self.setCheck(filterType:filterType)
                        self.setSelectedView(id: id, name: name, filterType: filterType, subType: subType, subTypeName: subTypeName)
                    }
                }else{
                    self.removeMainList()
                }
            }
        }
    }
    
    func removeMainList(){
        FilterListingsViewController.selectedFilterList.removeAll()
    }
    
    override func willMove(toParent parent: UIViewController?){
        super.willMove(toParent: parent)
        if parent == nil{
            if !FilterListingsViewController.applyButtonPressed{
                FilterListingsViewController.selectedFilterList.removeAll()
                self.clearParticularList()
            }
        }
    }
    
    func setScrollButtons(){
        leftScrollButton.contentVerticalAlignment = .fill
        rightScrollButton.contentVerticalAlignment = .fill
        leftScrollButton.contentHorizontalAlignment = .fill
        rightScrollButton.contentHorizontalAlignment = .fill
    }
    
    func getItemsClick(){
        filterConditionHandler.onConditionClick = { condition in
            let id = condition.conditionId!
            let name = condition.conditionName!
            let filterType = AppConstants.CONDITION_FILTER
            self.setSelectedView(id: id, name: name, filterType: filterType, subType: 0, subTypeName: nil)
        }
        filterSizesHandler.onSizeTypeClick = { sizeType in
            if sizeType.sizeTypeId == 0{
                let name = sizeType.sizeTypeName ?? ""
                let filterType = AppConstants.SIZE_FILTER
                self.setSelectedView(id: 0, name: name, filterType: filterType, subType: 0, subTypeName: nil)
                return
            }else{
                self.colorsTableView.isHidden = true
                self.conditionTableView.isHidden = true
                self.sizesTableView.isHidden = true
                self.selectCategoryTableView.isHidden = true
                self.priceTableView.isHidden = true
                self.brandsTableView.isHidden = true
                self.selectSubCategoryView.isHidden = true
                
                //self.selectSizesDetailHandler.filterSizesDetailViewHandler.filterSelectedDelegate = self
                self.sizeNameLabel.text = sizeType.sizeTypeName?.uppercased()
                self.selectSizesDetailHandler.sizeList = sizeType.sizes!
                self.selectSizesDetailHandler.subTypeId = sizeType.sizeTypeId ?? 0
                self.selectSizesDetailHandler.subTypeName = sizeType.sizeTypeName ?? ""
//                self.selectSizesDetailHandler.collectionView?.layoutIfNeeded()
                self.sizesDetailTableView.reloadData()
                self.sizesDetailView.isHidden = false
            }
        }
        
        selectSizesDetailHandler.onSizeDetailClick = { size,subTypeId,sizeTypeName in
            let id = size.sizeId!
            let name = size.sizeName!
            let filterType = AppConstants.SIZE_FILTER
            self.setSelectedView(id: subTypeId, name: sizeTypeName, filterType: filterType, subType: id, subTypeName: name)
            self.sizesTableView.reloadData()
            self.sizesDetailTableView.reloadData()
            //self.selectSizesDetailHandler.filterSizesDetailViewHandler.filterSelectedDelegate = self
//            self.selectSizesDetailHandler.collectionView?.reloadData()
        }
        
        filterColorsHandler.onColorsClick = { color in
            let id = color.colorId!
            let name = color.colorName!
            let filterType = AppConstants.COLOR_FILTER
            self.setSelectedView(id: id, name: name, filterType: filterType, subType: 0, subTypeName: nil)
        }
        selectCategoryHandler.onCategoryTypeClick = { categoryType in
            if categoryType.categoryTypeId == 0{
                let name = categoryType.categoryTypeName ?? ""
                let filterType = AppConstants.CATEGORY_FILTER
                self.setSelectedView(id: 0, name: name, filterType: filterType, subType: 0, subTypeName: nil)
                return
            }else{
                self.colorsTableView.isHidden = true
                self.conditionTableView.isHidden = true
                self.sizesTableView.isHidden = true
                self.sizesDetailView.isHidden = true
                self.selectCategoryTableView.isHidden = true
                self.priceTableView.isHidden = true
                self.brandsTableView.isHidden = true
                
                var isAllOptionAlreadyAdded = false
                for i in 0..<categoryType.categories!.count{
                    if (categoryType.categories![i].categoryId == 0) {
                        isAllOptionAlreadyAdded = true
                        break
                    }
                }
                var categoryList:[Category] = []
                if (!isAllOptionAlreadyAdded) {
                    let categoryShowAll = Category(categoryId: 0, categoryName: AppConstants.Strings.SHOW_ALL_TEXT)
                    categoryList = categoryType.categories!
                    categoryList.insert(categoryShowAll, at: 0)
                }
                
                self.subCategoryNameLabel.text = categoryType.categoryTypeName?.uppercased()
                self.selectSubCategoryHandler.category = categoryList
                self.selectSubCategoryHandler.subTypeId = categoryType.categoryTypeId!
                self.selectSubCategoryHandler.subTypeName = categoryType.categoryTypeName!
                self.subCategoryTableView.reloadData()
                self.selectSubCategoryView.isHidden = false
            }
        }
        
        selectSubCategoryHandler.onCategoryClick = { category,subTypeId,categoryTypeName in
            let id = category.categoryId!
            let name = category.categoryName!
            let filterType = AppConstants.CATEGORY_FILTER
            self.setSelectedView(id: subTypeId, name: categoryTypeName, filterType: filterType, subType: id, subTypeName: name)
            self.selectCategoryTableView.reloadData()
            
        }
        filterPriceHandler.onPriceClick = { price in
            let id = price.id!
            let name = price.price!
            let filterType = AppConstants.PRICE_FILTER
            self.setSelectedView(id: id, name: name, filterType: filterType, subType: 0, subTypeName: nil)
        }
        filterBrandHandler.onBrandClick = { brands in
            let id = brands.brandId!
            let name = brands.brandName!
            let filterType = AppConstants.BRAND_FILTER
            self.setSelectedView(id: id, name: name, filterType: filterType, subType: 0, subTypeName: nil)
        }
        
        self.filtersHandler.onRemoveButtonClick = { selectedFilter,index in
            self.removeCheck(selectedFilter: selectedFilter, index: index)
        }
        
    }
    
    func removeCheck(selectedFilter:SelectedFilter,index:Int){
        var itemCount = 0
        for i in 0..<FilterListingsViewController.selectedFilterList.count{
            if FilterListingsViewController.selectedFilterList[i].type == selectedFilter.type{
                itemCount+=1
            }
            
        }
        
        if itemCount == 1{
            switch (selectedFilter.type) {
            case AppConstants.CONDITION_FILTER:
                self.conditionTickImageView.isHidden = true
                break
            case AppConstants.SIZE_FILTER:
                self.sizeTickImageView.isHidden = true
                break
            case AppConstants.COLOR_FILTER:
                self.colorsTickImageView.isHidden = true
                break
            case AppConstants.CATEGORY_FILTER:
                self.categoryTickImageView.isHidden = true
                break
            case AppConstants.PRICE_FILTER:
                self.priceTickImageView.isHidden = true
                break
            case AppConstants.BRAND_FILTER:
                self.brandTickImageView.isHidden = true
                break
            default:
                break
            }
        }
        
        FilterListingsViewController.selectedFilterList.remove(at:index)
//        if FilterListingsViewController.applyButtonPressed{
//            clearParticularItemFromList(index:index)
//        }
        AppConstants.filterCellWidth.remove(at: index)
        refreshAllAdapters()
    }
    
    func clearParticularItemFromList(index:Int){
        if let viewcontrollers = self.navigationController?.viewControllers {
            let previousVC = viewcontrollers[viewcontrollers.count - 2]
            if previousVC is FeedViewController {
                let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
                if selectedIndex == 0{
                    if BaseViewController.mSelectedFeedFilterList.count > 0{
                        BaseViewController.mSelectedFeedFilterList.remove(at: index)
                    }
                }else if selectedIndex == 1{
                    if BaseViewController.mSelectedBrowseFilterList.count > 0 {
                        BaseViewController.mSelectedBrowseFilterList.remove(at: index)
                    }
                }
            }else if previousVC is UserBynViewController{
                if BaseViewController.mSelectedUserBynFilterList.count > 0{
                    BaseViewController.mSelectedUserBynFilterList.remove(at: index)
                }
            }else if previousVC is MyLikesViewController{
                if BaseViewController.mSelectedMyLikesFilterList.count > 0{
                    BaseViewController.mSelectedMyLikesFilterList.remove(at: index)
                }
            }else if previousVC is OtherUserProfileViewController{
                if BaseViewController.mSelectedOtherUserProfileFilterList.count > 0{
                    BaseViewController.mSelectedOtherUserProfileFilterList.remove(at: index)
                }
            }
        }
    }
    
    func refreshAllAdapters() {
        if !(filterColorsHandler.colorsList.isEmpty){
            colorsTableView.reloadData()
        }
        if !(selectCategoryHandler.categoryType.isEmpty){
            selectCategoryTableView.reloadData()
        }
        if !(selectSubCategoryHandler.category.isEmpty){
            subCategoryTableView.reloadData()
        }
        if !(filterBrandHandler.brands.isEmpty){
            brandsTableView.reloadData()
        }
        if !(filterSizesHandler.sizeType.isEmpty){
            sizesTableView.reloadData()
        }
        if !(selectSizesDetailHandler.sizeList.isEmpty){
            sizesDetailTableView.reloadData()
        }
        if !(filterConditionHandler.condition.isEmpty){
            conditionTableView.reloadData()
        }
        if !(filterPriceHandler.price.isEmpty){
            priceTableView.reloadData()
        }
        if !(filtersHandler.filter.isEmpty){
            filterCollectionView.reloadData()
        }
        leftRightButtonHiddenShow()
    }
    
    func setSelectedView(id:Int, name :String, filterType:Int, subType:Int, subTypeName:String?) {
        setCheck(filterType: filterType)
        let selectedFilter = SelectedFilter(id: id, name: name, type: filterType, subType: subType,subTypeName: subTypeName)
        var filtersToRemove : [SelectedFilter] = []
        for i in 0..<FilterListingsViewController.selectedFilterList.count{
            if filterType == FilterListingsViewController.selectedFilterList[i].type{
                if FilterListingsViewController.selectedFilterList[i].subType == subType{
                    if id == FilterListingsViewController.selectedFilterList[i].id{
                        //self.view.makeToast("Already Selected")
                        return
                    }
                }
            }
            
        }
        
        if filterType == AppConstants.CONDITION_FILTER{
            filtersToRemove.append(contentsOf: getAllSelectedFilters(type: AppConstants.CONDITION_FILTER))
        }
        if filterType == AppConstants.PRICE_FILTER{
            filtersToRemove.append(contentsOf: getAllSelectedFilters(type: AppConstants.PRICE_FILTER))
        }
        if (filterType == AppConstants.CATEGORY_FILTER) {
            if (id == 0) {
                filtersToRemove.append(contentsOf: getAllSelectedFilters(type: AppConstants.CATEGORY_FILTER))
            } else {
                if (subType == 0) {
                    filtersToRemove.append(contentsOf: getAllSelectedSubCategories(categoryId: id))
                }else{
                    let showAllFilter = getShowAllFilter(type: AppConstants.CATEGORY_FILTER, id: id)
                    if (showAllFilter != nil) {
                        filtersToRemove.append(showAllFilter!)
                    }
                }
                let showAllFilter = getShowAllFilter(type: AppConstants.CATEGORY_FILTER)
                if (showAllFilter != nil) {
                    filtersToRemove.append(showAllFilter!)
                }
            }
        }
        
        if (filterType == AppConstants.SIZE_FILTER) {
            if (id == 0) {
                filtersToRemove.append(contentsOf: getAllSelectedFilters(type: AppConstants.SIZE_FILTER))
            } else {
                let showAllFilter = getShowAllFilter(type: AppConstants.SIZE_FILTER)
                if (showAllFilter != nil) {
                    filtersToRemove.append(showAllFilter!)
                }
            }
        }
        if (filterType == AppConstants.BRAND_FILTER) {
            if (id == 0) {
                filtersToRemove.append(contentsOf: getAllSelectedFilters(type: AppConstants.BRAND_FILTER))
            } else {
                let showAllFilter = getShowAllFilter(type: AppConstants.BRAND_FILTER)
                if (showAllFilter != nil) {
                    filtersToRemove.append(showAllFilter!)
                }
            }
        }
        var ids:[Int] = []
        for j in 0..<FilterListingsViewController.selectedFilterList.count {
            for i in 0..<filtersToRemove.count{
                if filtersToRemove[i].id == FilterListingsViewController.selectedFilterList[j].id && filtersToRemove[i].name == FilterListingsViewController.selectedFilterList[j].name && filtersToRemove[i].subType == FilterListingsViewController.selectedFilterList[j].subType && filtersToRemove[i].type == FilterListingsViewController.selectedFilterList[j].type {
                    ids.append(j)
                }
            }
        }
        
        FilterListingsViewController.selectedFilterList = FilterListingsViewController.selectedFilterList.enumerated()
            .filter { !ids.contains($0.offset) }
            .map { $0.element }
        
        //FilterListingsViewController.selectedFilterList.remove(at: ids)

        FilterListingsViewController.selectedFilterList.append(selectedFilter)
        setSelectedViewAdapter()
    }
    
    private func setSelectedViewAdapter() {
        filtersHandler.filter = FilterListingsViewController.selectedFilterList
        filterCollectionView.reloadData()
        print("filters:",FilterListingsViewController.selectedFilterList)
        self.leftRightButtonHiddenShow()
    }
    
    private func getAllSelectedFilters(type:Int) -> [SelectedFilter] {
        var selectedFilters = [SelectedFilter]()
        for i in 0..<FilterListingsViewController.selectedFilterList.count{
            if FilterListingsViewController.selectedFilterList[i].type == type{
                selectedFilters.append(FilterListingsViewController.selectedFilterList[i])
            }
        }
        return selectedFilters
    }
    
    private func getAllSelectedSubCategories(categoryId:Int) -> [SelectedFilter] {
        var selectedFilters = [SelectedFilter]()
        for i in 0..<FilterListingsViewController.selectedFilterList.count {
            if (FilterListingsViewController.selectedFilterList[i].type == AppConstants.CATEGORY_FILTER && FilterListingsViewController.selectedFilterList[i].id == categoryId) {
                selectedFilters.append(FilterListingsViewController.selectedFilterList[i])
            }
        }
        return selectedFilters
    }
    
    private func getShowAllFilter(type:Int) -> SelectedFilter?{
        for i in 0..<FilterListingsViewController.selectedFilterList.count {
            if (FilterListingsViewController.selectedFilterList[i].type == type && FilterListingsViewController.selectedFilterList[i].id == 0) {
                return FilterListingsViewController.selectedFilterList[i]
            }
        }
        return nil
    }
    
    private func getShowAllFilter(type:Int, id:Int) -> SelectedFilter?{
        for i in 0..<FilterListingsViewController.selectedFilterList.count {
            let filter = FilterListingsViewController.selectedFilterList[i]
            if (filter.type == type && filter.id != 0 && filter.subType == 0) {
                return filter
            }
        }
        return nil
    }
    
    func setFilterCollectionView(){
        self.filtersHandler.filter = FilterListingsViewController.selectedFilterList
        self.filterCollectionView.reloadData()
    }
    
    func setCheck(filterType:Int){
        switch (filterType) {
        case AppConstants.CONDITION_FILTER:
            self.conditionTickImageView.isHidden = false
            break
        case AppConstants.SIZE_FILTER:
            self.sizeTickImageView.isHidden = false
            break
        case AppConstants.COLOR_FILTER:
            self.colorsTickImageView.isHidden = false
            break
        case AppConstants.CATEGORY_FILTER:
            self.categoryTickImageView.isHidden = false
            break
        case AppConstants.PRICE_FILTER:
            self.priceTickImageView.isHidden = false
            break
        case AppConstants.BRAND_FILTER:
            self.brandTickImageView.isHidden = false
            break
        default:
            break
        }
        
    }
    
    func leftRightButtonHiddenShow(){
        self.filterCollectionView.layoutIfNeeded()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            print("widths:",self.filterCollectionView.contentSize.width , UIScreen.main.bounds.width)
            if self.filterCollectionView.contentSize.width > UIScreen.main.bounds.width{
                self.leftScrollButton.isHidden = false
                self.rightScrollButton.isHidden = false
            }else{
                self.leftScrollButton.isHidden = true
                self.rightScrollButton.isHidden = true
            }
        }
    }
    
    func setPriceView() {
        if (priceList.count == 0) {
            addPriceData()
        }
        self.filterPriceHandler.price = self.priceList
        self.priceTableView.reloadData()
        self.priceActivityIndicator.stopAnimating()
    }
    
    func addPriceData() {
        priceList.append(Price(id: 1, price: AppConstants.Strings.PRICE_UNDER_20, isPriceSelected: false))
        priceList.append(Price(id: 2, price: AppConstants.Strings.PRICE_UNDER_50, isPriceSelected: false))
        priceList.append(Price(id: 3, price: AppConstants.Strings.PRICE_UNDER_100, isPriceSelected: false))
        priceList.append(Price(id: 4, price: AppConstants.Strings.PRICE_UNDER_200, isPriceSelected: false))
        priceList.append(Price(id: 5, price: AppConstants.Strings.PRICE_ABOVE_200, isPriceSelected: false))
    }
    
    func setHandlers(){
        filterCollectionView.delegate = filtersHandler
        filterCollectionView.dataSource = filtersHandler
        
        filtersHandler.TYPE = AppConstants.TYPE_LISTING_FILTER
        
        self.conditionTableView.delegate = filterConditionHandler
        self.conditionTableView.dataSource = filterConditionHandler
        self.filterConditionHandler.filterSelectedDelegate = self
        
        self.sizesTableView.delegate = filterSizesHandler
        self.sizesTableView.dataSource = filterSizesHandler
        self.filterSizesHandler.filterSelectedDelegate = self
        
        self.sizesDetailTableView.delegate = selectSizesDetailHandler
        self.sizesDetailTableView.dataSource = selectSizesDetailHandler
        self.selectSizesDetailHandler.filterSelectedDelegate = self
         
        self.colorsTableView.delegate = filterColorsHandler
        self.colorsTableView.dataSource = filterColorsHandler
        self.filterColorsHandler.filterSelectedDelegate = self 
        
        self.selectCategoryTableView.delegate = selectCategoryHandler
        self.selectCategoryTableView.dataSource = selectCategoryHandler
        self.selectCategoryHandler.filterSelectedDelegate = self
        
        self.subCategoryTableView.delegate = selectSubCategoryHandler
        self.subCategoryTableView.dataSource = selectSubCategoryHandler
        self.selectSubCategoryHandler.filterSelectedDelegate = self
        
        self.priceTableView.delegate = filterPriceHandler
        self.priceTableView.dataSource = filterPriceHandler
        self.filterPriceHandler.filterSelectedDelegate = self
        
        self.brandsTableView.delegate = filterBrandHandler
        self.brandsTableView.dataSource = filterBrandHandler
        filterBrandHandler.TYPE = AppConstants.BRAND_FILTER
        self.filterBrandHandler.filterSelectedDelegate = self
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == self.brandsTextField{
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reloadBrands), object: nil)
            self.perform(#selector(self.reloadBrands), with: nil, afterDelay: 0.6)
        }
        
    }
    
    @objc func reloadBrands() {
        self.currentPage = 1
        self.getBrands(searchQuery: brandsTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines))
    }
    
    @IBAction func clearFiltersButtonAction(_ sender: UIButton) {
        FilterListingsViewController.selectedFilterList.removeAll()
        clearParticularList()
        filterClearAllDelegate?.userFilterClearAll()
    }
    
    func clearParticularList(){
        if let viewcontrollers = self.navigationController?.viewControllers {
            let previousVC = viewcontrollers[viewcontrollers.count - 2]
            if previousVC is FeedViewController {
                let selectedIndex = AppConstants.INDEX_OF_CURRENT_SELECTED_TAB
                if selectedIndex == 0{
                    BaseViewController.mSelectedFeedFilterList.removeAll()
                }else if selectedIndex == 1{
                    BaseViewController.mSelectedBrowseFilterList.removeAll()
                }
            }else if previousVC is UserBynViewController{
                BaseViewController.mSelectedUserBynFilterList.removeAll()
            }else if previousVC is MyLikesViewController{
                BaseViewController.mSelectedMyLikesFilterList.removeAll()
            }else if previousVC is OtherUserProfileViewController{
                BaseViewController.mSelectedOtherUserProfileFilterList.removeAll()
            }
        }
    }
    
    @IBAction func applyFiltersButtonAction(_ sender: UIButton) {
        FilterListingsViewController.applyButtonPressed = true
        if FilterListingsViewController.selectedFilterList.count > 0{
            //todo height
            self.filterApplyDelegate?.userFilterApply(selectedFilter: FilterListingsViewController.selectedFilterList, heightModal: HeightModel())
        }else{
            self.view.makeToast("To apply select atleast one filter.")
        }
    }
    
    @IBAction func sizesBackButtonAction(_ sender: UIButton) {
    }
    
    @IBAction func categoryBackButtonAction(_ sender: UIButton) {
        showCategoryList()
    }
    
    @IBAction func categoryBackTapAction(_ sender: UITapGestureRecognizer) {
        showCategoryList()
    }
    
    func showCategoryList(){
        self.colorsTableView.isHidden = true
        self.conditionTableView.isHidden = true
        self.sizesTableView.isHidden = true
        self.sizesDetailView.isHidden = true
        self.selectCategoryTableView.isHidden = false
        self.priceTableView.isHidden = true
        self.brandsTableView.isHidden = true
        self.selectSubCategoryView.isHidden = true
    }
    
    @IBAction func sizeBackButonAction(_ sender: UIButton) {
        showSizesTypeList()
    }
    
    @IBAction func sizeTypeBackTapAction(_ sender: UITapGestureRecognizer) {
        showSizesTypeList()
    }
    
    func showSizesTypeList(){
        self.colorsTableView.isHidden = true
        self.conditionTableView.isHidden = true
        self.sizesTableView.isHidden = false
        self.sizesDetailView.isHidden = true
        self.selectCategoryTableView.isHidden = true
        self.priceTableView.isHidden = true
        self.brandsTableView.isHidden = true
        self.selectSubCategoryView.isHidden = true
    }
    
    @IBAction func conditionTapAction(_ sender: UITapGestureRecognizer) {
        self.makeColorRegular()
        self.makeConditionBold()
        self.makeSizeRegular()
        self.makeCategoryRegular()
        self.makePriceRegular()
        self.makeBrandsRegular()
        self.colorsTableView.isHidden = true
        self.conditionTableView.isHidden = false
        self.sizesTableView.isHidden = true
        self.sizesDetailView.isHidden = true
        self.selectCategoryTableView.isHidden = true
        self.selectSubCategoryView.isHidden = true
        self.priceTableView.isHidden = true
        self.brandsTableView.isHidden = true
    }
    
    @IBAction func sizeTapAction(_ sender: UITapGestureRecognizer) {
        self.makeColorRegular()
        self.makeConditionRegular()
        self.makeSizeBold()
        self.makeCategoryRegular()
        self.makePriceRegular()
        self.makeBrandsRegular()
        self.colorsTableView.isHidden = true
        self.conditionTableView.isHidden = true
        self.sizesTableView.isHidden = false
        self.sizesDetailView.isHidden = true
        self.selectCategoryTableView.isHidden = true
        self.selectSubCategoryView.isHidden = true
        self.priceTableView.isHidden = true
        self.brandsTableView.isHidden = true
    }
    
    @IBAction func colorsTapAction(_ sender: UITapGestureRecognizer) {
        self.makeColorBold()
        self.makeConditionRegular()
        self.makeSizeRegular()
        self.makeCategoryRegular()
        self.makePriceRegular()
        self.makeBrandsRegular()
        self.colorsTableView.isHidden = false
        self.conditionTableView.isHidden = true
        self.sizesTableView.isHidden = true
        self.sizesDetailView.isHidden = true
        self.selectCategoryTableView.isHidden = true
        self.selectSubCategoryView.isHidden = true
        self.priceTableView.isHidden = true
        self.brandsTableView.isHidden = true
    }
    
    @IBAction func categoryTapAction(_ sender: UITapGestureRecognizer) {
        self.makeColorRegular()
        self.makeConditionRegular()
        self.makeSizeRegular()
        self.makeCategoryBold()
        self.makePriceRegular()
        self.makeBrandsRegular()
        self.colorsTableView.isHidden = true
        self.conditionTableView.isHidden = true
        self.sizesTableView.isHidden = true
        self.sizesDetailView.isHidden = true
        self.selectCategoryTableView.isHidden = false
        self.selectSubCategoryView.isHidden = true
        self.priceTableView.isHidden = true
        self.brandsTableView.isHidden = true
    }
    
    @IBAction func priceTapAction(_ sender: UITapGestureRecognizer) {
        self.makeColorRegular()
        self.makeConditionRegular()
        self.makeSizeRegular()
        self.makeCategoryRegular()
        self.makePriceBold()
        self.makeBrandsRegular()
        self.colorsTableView.isHidden = true
        self.conditionTableView.isHidden = true
        self.sizesTableView.isHidden = true
        self.sizesDetailView.isHidden = true
        self.selectCategoryTableView.isHidden = true
        self.selectSubCategoryView.isHidden = true
        self.priceTableView.isHidden = false
        self.brandsTableView.isHidden = true
    }
    
    @IBAction func brandsTapAction(_ sender: UITapGestureRecognizer) {
        self.makeColorRegular()
        self.makeConditionRegular()
        self.makeSizeRegular()
        self.makeCategoryRegular()
        self.makePriceRegular()
        self.makeBrandsBold()
        self.colorsTableView.isHidden = true
        self.conditionTableView.isHidden = true
        self.sizesTableView.isHidden = true
        self.sizesDetailView.isHidden = true
        self.selectCategoryTableView.isHidden = true
        self.selectSubCategoryView.isHidden = true
        self.priceTableView.isHidden = true
        self.brandsTableView.isHidden = false
    }
    
    func getCondition() {
        createViewModel.getCondition(accessToken: AppDefaults.getAccessToken(), { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[Condition]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let conditionData = data.data!
                    self.updateConditionTypes(condition: conditionData)
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.conditionActivityIndicator.stopAnimating()
            }
        } , { (error) in
            self.conditionActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func updateConditionTypes(condition:[Condition]) {
        self.conditionList = condition
        let condition = Condition(conditionId: 0, conditionName: AppConstants.Strings.SHOW_ALL_TEXT, isSelected: false)
        self.conditionList.insert(condition, at: 0)
        self.filterConditionHandler.condition = self.conditionList
        self.conditionTableView.reloadData()
        self.conditionActivityIndicator.stopAnimating()
    }
    
    func getBrands(searchQuery:String? = nil) {
        createViewModel.getBrandsForFilters(accessToken: getAccessToken(),search:searchQuery ?? "", page: self.currentPage,{(response) in
            self.isApiCalled = false
            self.refreshControl?.endRefreshing()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Brand> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let brands = data.data!
                    
                    if brands.items!.count > 0{
                        if var allBrands = brands.items{
                            if self.currentPage == 1 {
                                let brand = Brand(brandId: 0, brandName: AppConstants.Strings.SHOW_ALL_TEXT)
                                allBrands.insert(brand, at: 0)
                                self.filterBrandHandler.brands = allBrands
                            } else {
                                self.filterBrandHandler.brands.append(contentsOf: allBrands)
                            }
                            if brands.totalPages == self.currentPage {
                                self.isApiCalled = true
                                self.filterBrandHandler.isLoadMoreRequired = false
                            }
                            self.brands = allBrands
                            self.showBrands( brands: allBrands)
                        }
                    }else{
                        self.filterBrandHandler.brands.removeAll()
                        self.brandsTableView.reloadData()
                        self.brandsActivityIndicator.stopAnimating()
                    }
                    
                }
                self.currentPage += 1
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.brandsActivityIndicator.stopAnimating()
                self.refreshControl?.endRefreshing()
            }
        } , { (error) in
            self.brandsActivityIndicator.stopAnimating()
            self.refreshControl?.endRefreshing()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func showBrands( brands:[Brand]) {
        self.brandsTableView.reloadData()
        self.brandsActivityIndicator.stopAnimating()
    }
    
    func getColors() {
        createViewModel.getColors(accessToken: AppDefaults.getAccessToken(), { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[Colors]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let colorsData = data.data!
                    self.setColorList(colorList: colorsData)
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.colorActivityIndicator.stopAnimating()
            }
        } , { (error) in
            self.colorActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func setColorList(colorList:[Colors]) {
        self.colorsList = colorList
        self.filterColorsHandler.colorsList = self.colorsList
        self.colorsTableView.reloadData()
        self.colorActivityIndicator.stopAnimating()
    }
    
    func getSizes() {
        createViewModel.getSizesWithFilterView(accessToken: AppDefaults.getAccessToken(), { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[FilterSizeType]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let sizeType = data.data!
                    self.setSizeList(filterSizeTypeList:sizeType)
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.sizesActivityIndicator.stopAnimating()
            }
        } , { (error) in
            self.sizesActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func setSizeList(filterSizeTypeList:[FilterSizeType]) {
        self.filterSizeTypeList = filterSizeTypeList
        let sizeType = FilterSizeType(sizeTypeId: 0, sizeTypeName: AppConstants.Strings.SHOW_ALL_TEXT)
        self.filterSizeTypeList.insert(sizeType, at: 0)
        self.filterSizesHandler.sizeType = self.filterSizeTypeList
//        self.selectSizesDetailHandler.filterSizesDetailViewHandler.sizeType = self.filterSizeTypeList
        self.sizesTableView.reloadData()
        self.sizesActivityIndicator.stopAnimating()
    }
    
    func getCategory() {
        createViewModel.getCategory(accessToken: AppDefaults.getAccessToken(), { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[CategoryType]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let categoryData = data.data!
                    self.setCategoryTypeList(categoryTypeList: categoryData)
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.categoryActivityIndicator.stopAnimating()
            }
        } , { (error) in
            self.categoryActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func setCategoryTypeList(categoryTypeList:[CategoryType]) {
        self.categoryTypeList = categoryTypeList
        let categoryType = CategoryType(categoryTypeId: 0, categoryTypeName: AppConstants.Strings.SHOW_ALL_TEXT)
        self.categoryTypeList.insert(categoryType, at: 0)
        self.selectCategoryHandler.categoryType = self.categoryTypeList
        self.selectCategoryTableView.reloadData()
        self.categoryActivityIndicator.stopAnimating()
    }
    
    func makeBrandsBold(){
        self.brandLabel.font = UIFont.fontBold(size: 14)
        self.brandLabel.textColor = UIColor.cF87DE8
    }
    
    func makeBrandsRegular(){
        self.brandLabel.font = UIFont.fontRegular(size: 14)
        self.brandLabel.textColor = UIColor.white
    }
    
    func makeConditionBold(){
        self.conditionLabel.font = UIFont.fontBold(size: 14)
        self.conditionLabel.textColor = UIColor.cF87DE8
    }
    
    func makeConditionRegular(){
        self.conditionLabel.font = UIFont.fontRegular(size: 14)
        self.conditionLabel.textColor = UIColor.white
    }
    
    func makeSizeBold(){
        self.sizeLabel.font = UIFont.fontBold(size: 14)
        self.sizeLabel.textColor = UIColor.cF87DE8
    }
    
    func makeSizeRegular(){
        self.sizeLabel.font = UIFont.fontRegular(size: 14)
        self.sizeLabel.textColor = UIColor.white
    }
    
    func makeColorBold(){
        self.colorLabel.font = UIFont.fontBold(size: 14)
        self.colorLabel.textColor = UIColor.cF87DE8
    }
    
    func makeColorRegular(){
        self.colorLabel.font = UIFont.fontRegular(size: 14)
        self.colorLabel.textColor = UIColor.white
    }
    
    func makeCategoryBold(){
        self.categoryLabel.font = UIFont.fontBold(size: 14)
        self.categoryLabel.textColor = UIColor.cF87DE8
    }
    
    func makeCategoryRegular(){
        self.categoryLabel.font = UIFont.fontRegular(size: 14)
        self.categoryLabel.textColor = UIColor.white
    }
    
    func makePriceBold(){
        self.priceLabel.font = UIFont.fontBold(size: 14)
        self.priceLabel.textColor = UIColor.cF87DE8
    }
    
    func makePriceRegular(){
        self.priceLabel.font = UIFont.fontRegular(size: 14)
        self.priceLabel.textColor = UIColor.white
    }
    
    @IBAction func leftScrollButtonAction(_ sender: UIButton) {
        let contentOffset = CGFloat(floor(self.filterCollectionView.contentOffset.x - 100))
        self.moveCollectionToFrame(contentOffset: contentOffset)
    }
    
    @IBAction func rightScrollButtonAction(_ sender: UIButton) {
        let contentOffset = CGFloat(floor(self.filterCollectionView.contentOffset.x + 100))
        self.moveCollectionToFrame(contentOffset: contentOffset)
    }
    
    func moveCollectionToFrame(contentOffset : CGFloat) {
        let frame: CGRect = CGRect(x : contentOffset ,y : self.filterCollectionView.contentOffset.y ,width : self.filterCollectionView.frame.width,height : self.filterCollectionView.frame.height)
        self.filterCollectionView.scrollRectToVisible(frame, animated: true)
    }
    
    func addRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        brandsTableView.addSubview(refreshControl!)
    }
    
    @objc func refreshList(){
        currentPage = 1
        isApiCalled = false
        self.filterBrandHandler.isLoadMoreRequired = true
        self.getBrands()
    }
    
    func isFilterSelected(type:Int, id:Int , subType:Int , isParentType:Bool) -> Bool{
        for i in 0..<FilterListingsViewController.selectedFilterList.count {
            let selectedFilter = FilterListingsViewController.selectedFilterList[i]
            if (selectedFilter.type == type && selectedFilter.id == id && selectedFilter.subType == subType) {
                return true
            }
            if (type == AppConstants.SIZE_FILTER && selectedFilter.type == AppConstants.SIZE_FILTER && isParentType && selectedFilter.id == id) {
                return true
            }
            if (type == AppConstants.CATEGORY_FILTER && selectedFilter.type == AppConstants.CATEGORY_FILTER && isParentType && selectedFilter.id == id) {
                return true
            }
        }
        return false
    }
    
}
