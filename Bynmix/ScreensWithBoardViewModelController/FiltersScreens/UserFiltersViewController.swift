import UIKit
import RangeSeekSlider

class UserFiltersViewController: BaseViewController,ScrollEndedInFilterProtocol,LoadMoreProtocol,FilterSelectedProtocol {
    
    func onLoadMore() {
        if !isApiCalled {
            isApiCalled = true
            self.getBrands()
        }
    }
    
    func ScrollEndedInFilterProtocol(type:Int) {
        if type == AppConstants.TYPE_RIGHT_BUTTON{
            self.rightScrollButton.isHidden = true
            self.leftScrollButton.isHidden = false
        }else if type == AppConstants.TYPE_LEFT_BUTTON{
            self.leftScrollButton.isHidden = true
            self.rightScrollButton.isHidden = false
        }else{
            self.leftScrollButton.isHidden = false
            self.rightScrollButton.isHidden = false
        }
    }
    
    @IBOutlet weak var styleTickImageView: UIImageView!
    @IBOutlet weak var bodyTypeTickImageView: UIImageView!
    @IBOutlet weak var brandsTickImageView: UIImageView!
    @IBOutlet weak var heightTickImageView: UIImageView!
    @IBOutlet weak var filtersShowCollectionView: UICollectionView!
    @IBOutlet weak var leftScrollButton: UIButton!
    @IBOutlet weak var rightScrollButton: UIButton!
    @IBOutlet weak var styleLabel: UILabel!
    @IBOutlet weak var bodyTypeLabel: UILabel!
    @IBOutlet weak var brandsLabel: UILabel!
    @IBOutlet weak var heightLabel: UILabel!
    @IBOutlet weak var stylesTableView: UITableView!
    @IBOutlet weak var searchStylesTextFeild: UITextField!
    @IBOutlet weak var heightView: UIView!
    @IBOutlet weak var showAllHeightButton: UIButton!
    @IBOutlet weak var selectHeightView: RangeSeekSlider!
    @IBOutlet weak var heightInTextLabel: UILabel!
    @IBOutlet weak var brandsTableView: UITableView!
    @IBOutlet weak var searchBrandTextField: UITextField!
    @IBOutlet weak var bodyTypeTableView: UITableView!
    @IBOutlet weak var selectTextLabel: UILabel!
    @IBOutlet var stylesActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var brandsActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var bodyTypeActivityIndicator: UIActivityIndicatorView!
    
    var filterStyleHandler = FilterStylesHandler()
    var filterUserBrandHandler = FilterUserBrandHandler()
    var filterUserBodyTypeHandler = FilterUserBodyTypeHandler()
    var filtersHandler = FiltersHandler()
    let profileViewModel = ProfileViewModel()
    let createViewModel = CreateViewModel()
    let editProfileViewModel = EditProfileViewModel()
    var styles:[Styles] = []
    var selectedStyles:[Styles] = []
    var currentPage = 1
    var isApiCalled = false
    var refreshControl : UIRefreshControl?
    
    var filterList:[FilterList] = []
    static var selectedFilter:[SelectedFilter] = []
    
    var bodyType:[BodyType] = []
    var brands:[Brand] = []
    var allBrands : [Brand] = []
    var filteredBrands: [Brand] = []
    static var selectedHeightModel = HeightModel()
    var divFactor:Double = 1.66
    var minHeight = 3
    var userFilterApplyDelegate:UserFilterApplyProtocol?
    var userFilterClearAllDelegate:UserFilterClearAllProtocol?
    static var applyButtonPressed:Bool?
    var onBoardingViewModel = OnBoardingViewModel()
    let milliSeconds = 0.25
    let milliSecs = 0.60
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setHandlers()
        getItemsClick()
        self.makeStyleBold()
        navigationItem.title = AppConstants.Strings.FILTER_USERS_TITLE
        searchStylesTextFeild.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        searchBrandTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        selectHeightView.delegate = self
        selectHeightView.minDistance = 20
        setScrollButtons()
        getStyles()
        getBrands()
        getBodyType()
        self.filtersHandler.scrollEndedInFilterDelegate = self
        self.heightInTextLabel.text = "3" + " ft " + "0" + " in " + " - " + "8" + " ft " + "0" + " in "
        filterUserBrandHandler.loadMoreDelegate = self
        addRefreshControl()
        self.stylesTableView.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        if UserFiltersViewController.selectedFilter.count > 0{
            self.filtersHandler.filter = UserFiltersViewController.selectedFilter
            self.filtersShowCollectionView.reloadData()
            var type:Int = 0
            var hasHeight:Bool = false
            for i in 0..<UserFiltersViewController.selectedFilter.count{
                let filter = UserFiltersViewController.selectedFilter[i]
                type = filter.type!
                if type == AppConstants.HEIGHT_FILTER{
                    hasHeight = true
                    self.setHeightUI()
                }
                let id  = filter.id!
                let name = filter.name!
                let filterType = filter.type!
                let subType = filter.subType ?? 0
                let subTypeName = filter.subTypeName ?? ""
                self.setCheck(filterType:type, noHeight: !hasHeight)
                self.setSelectedView(id: id, name: name, filterType: filterType, subType: subType, subTypeName: subTypeName)
            }
        }
        print(UserFiltersViewController.selectedFilter)
    }
    
    override func willMove(toParent parent: UIViewController?){
        super.willMove(toParent: parent)
        if parent == nil{
            if !(UserFiltersViewController.applyButtonPressed ?? false){
                UserFiltersViewController.selectedFilter.removeAll()
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.leftRightButtonHiddenShow()
        
    }
    
    func setScrollButtons(){
        leftScrollButton.contentVerticalAlignment = .fill
        rightScrollButton.contentVerticalAlignment = .fill
        leftScrollButton.contentHorizontalAlignment = .fill
        rightScrollButton.contentHorizontalAlignment = .fill
    }
    
    func setHandlers(){
        stylesTableView.delegate = filterStyleHandler
        stylesTableView.dataSource = filterStyleHandler
        filterStyleHandler.filterSelectedDelegate = self
        
        filtersShowCollectionView.delegate = filtersHandler
        filtersShowCollectionView.dataSource = filtersHandler
        
        filtersHandler.TYPE = AppConstants.TYPE_USER_FILTER
        
        brandsTableView.delegate = filterUserBrandHandler
        brandsTableView.dataSource = filterUserBrandHandler
        filterUserBrandHandler.TYPE = AppConstants.USER_BRAND_FILTER
        filterUserBrandHandler.filterSelectedDelegate = self
        
        bodyTypeTableView.delegate = filterUserBodyTypeHandler
        bodyTypeTableView.dataSource = filterUserBodyTypeHandler
        filterUserBodyTypeHandler.filterSelectedDelegate = self
    }
    
    func setHeightDefaultValue(){
        UserFiltersViewController.selectedHeightModel.isHeightShowAll  = false
        self.heightInTextLabel.text = "3" + " ft " + "0" + " in " + " - " + "8" + " ft " + "0" + " in "
        let leftValue:Double = Double(0)
        let rightValue:Double = Double(100)
        let selectedMinValue:Int = (Int(leftValue / divFactor))
        UserFiltersViewController.selectedHeightModel.customMinHeightInches = selectedMinValue % 12
        var selectedMinFeetValue = selectedMinValue / 12
        selectedMinFeetValue = self.minHeight + selectedMinFeetValue
        UserFiltersViewController.selectedHeightModel.customMinHeightFeet = selectedMinFeetValue
        UserFiltersViewController.selectedHeightModel.minPinValue = Float(leftValue)
        let selectedMaxValue:Int = (Int(rightValue / divFactor))
        UserFiltersViewController.selectedHeightModel.customMaxHeightInches = selectedMaxValue % 12
        var selectedMaxFeetValue:Int = selectedMaxValue / 12
        selectedMaxFeetValue = minHeight + selectedMaxFeetValue
        UserFiltersViewController.selectedHeightModel.customMaxHeightFeet = selectedMaxFeetValue
        UserFiltersViewController.selectedHeightModel.maxPinValue = Float(rightValue)
        
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == self.searchStylesTextFeild{
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reloadStyle), object: nil)
            self.perform(#selector(self.reloadStyle), with: nil, afterDelay: 0.6)
        }else if textField == self.searchBrandTextField{
            NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reloadBrands), object: nil)
            self.perform(#selector(self.reloadBrands), with: nil, afterDelay: 0.6)
        }
        
    }
    
    @objc func reloadStyle() {
        self.getStyles(searchQuery:searchStylesTextFeild.text?.trimmingCharacters(in: .whitespacesAndNewlines))
    }
    
    @objc func reloadBrands() {
        self.currentPage = 1
        self.getBrands(searchQuery: searchBrandTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines))
    }
    
    func getItemsClick(){
        
        self.filterStyleHandler.onStyleClick = { styles,index in
            let id = styles.descriptorId!
            let name = styles.descriptorName!
            let filterType = AppConstants.USER_STYLE_FILTER
            self.setSelectedView(id: id, name: name, filterType: filterType, subType: 0, subTypeName: nil)
        }
        
        self.filterUserBodyTypeHandler.onBodyTypeClick = { bodyType in
            let id = bodyType.bodyTypeId!
            let name = bodyType.bodyTypeName!
            let filterType = AppConstants.USER_BODY_TYPE_FILTER
            self.setSelectedView(id: id, name: name, filterType: filterType, subType: 0, subTypeName: nil)
        }
        
        self.filterUserBrandHandler.onBrandClick = { brands in 
            let id = brands.brandId!
            let name = brands.brandName!
            let filterType = AppConstants.USER_BRAND_FILTER
            self.setSelectedView(id: id, name: name, filterType: filterType, subType: 0, subTypeName: nil)
        }
        
        self.filtersHandler.onRemoveButtonClick = { selectedFilter,index in
            self.removeCheck(selectedFilter: selectedFilter, index: index)
        }
        
    }
    
    func removeCheck(selectedFilter:SelectedFilter,index:Int){
        var itemCount = 0
        for i in 0..<UserFiltersViewController.selectedFilter.count{
            
            if UserFiltersViewController.selectedFilter[i].type == selectedFilter.type{
                if selectedFilter.type == AppConstants.HEIGHT_FILTER{
                    UserFiltersViewController.selectedHeightModel.isHeightShowAll = false
                    self.setHeightUI()
                }
                itemCount+=1
            }
            
        }
        if itemCount == 1{
            switch (selectedFilter.type) {
            case AppConstants.USER_STYLE_FILTER:
                self.styleTickImageView.isHidden = true
                break
            case AppConstants.USER_BODY_TYPE_FILTER:
                self.bodyTypeTickImageView.isHidden = true
                break
            case AppConstants.USER_BRAND_FILTER:
                self.brandsTickImageView.isHidden = true
                break
            case AppConstants.HEIGHT_FILTER:
                self.heightTickImageView.isHidden = true
                break
            default:
                break
            }
        }
        
        UserFiltersViewController.selectedFilter.remove(at:index)
        refreshAllAdapters()
    }
    
    func refreshAllAdapters() {
        if !(self.filtersHandler.filter.isEmpty){
            filtersShowCollectionView.reloadData()
        }
        if !(filterStyleHandler.styles.isEmpty){
            stylesTableView.reloadData()
        }
        if !(filterUserBodyTypeHandler.bodyType.isEmpty){
            bodyTypeTableView.reloadData()
        }
        if !(filterUserBrandHandler.brands.isEmpty){
            brandsTableView.reloadData()
        }
        leftRightButtonHiddenShow()
    }
    
    func leftRightButtonHiddenShow(){
        self.filtersShowCollectionView.layoutIfNeeded()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            print("widths:",self.filtersShowCollectionView.contentSize.width , UIScreen.main.bounds.width)
            if self.filtersShowCollectionView.contentSize.width > UIScreen.main.bounds.width{
                self.leftScrollButton.isHidden = false
                self.rightScrollButton.isHidden = false
            }else{
                self.leftScrollButton.isHidden = true
                self.rightScrollButton.isHidden = true
            }
        }
    }
    
    @IBAction func onStyleTapAction(_ sender: UITapGestureRecognizer) {
        self.makeStyleBold()
        self.makeBodyTypeRegular()
        self.makeBrandsRegular()
        self.makeHeightRegular()
        self.bodyTypeTableView.isHidden = true
        self.brandsTableView.isHidden = true
        self.heightView.isHidden = true
        self.stylesTableView.isHidden = false
    }
    
    @IBAction func bodyTypeTapAction(_ sender: UITapGestureRecognizer) {
        self.makeStyleRegular()
        self.makeBodyTypeBold()
        self.makeBrandsRegular()
        self.makeHeightRegular()
        self.bodyTypeTableView.isHidden = false
        self.brandsTableView.isHidden = true
        self.heightView.isHidden = true
        self.stylesTableView.isHidden = true
    }
    
    @IBAction func brandsTapAction(_ sender: UITapGestureRecognizer) {
        self.makeStyleRegular()
        self.makeBodyTypeRegular()
        self.makeBrandsBold()
        self.makeHeightRegular()
        self.bodyTypeTableView.isHidden = true
        self.brandsTableView.isHidden = false
        self.heightView.isHidden = true
        self.stylesTableView.isHidden = true
    }
    
    @IBAction func heightTapAction(_ sender: UITapGestureRecognizer) {
        self.makeStyleRegular()
        self.makeBodyTypeRegular()
        self.makeBrandsRegular()
        self.makeHeightBold()
        self.bodyTypeTableView.isHidden = true
        self.brandsTableView.isHidden = true
        self.heightView.isHidden = false
        self.stylesTableView.isHidden = true
    }
    
    @IBAction func leftScrollButtonAction(_ sender: UIButton) {
        let contentOffset = CGFloat(floor(self.filtersShowCollectionView.contentOffset.x - 100))
        self.moveCollectionToFrame(contentOffset: contentOffset)
    }
    
    @IBAction func rightScrollButtonAction(_ sender: UIButton) {
        let contentOffset = CGFloat(floor(self.filtersShowCollectionView.contentOffset.x + 100))
        self.moveCollectionToFrame(contentOffset: contentOffset)
    }
    
    @IBAction func clearFilterButtonAction(_ sender: UIButton) {
        UserFiltersViewController.selectedFilter.removeAll()
        self.userFilterClearAllDelegate?.userFilterClearAll()
    }
    
    @IBAction func applyFiltersButtonAction(_ sender: UIButton) {
        UserFiltersViewController.applyButtonPressed = true
        if UserFiltersViewController.selectedFilter.count > 0{
            //todo height
            self.userFilterApplyDelegate?.userFilterApply(selectedFilter: UserFiltersViewController.selectedFilter, heightModal: UserFiltersViewController.selectedHeightModel)
        }else{
            self.view.makeToast("To apply select atleast one filter.")
        }
        
    }
    
    @IBAction func ShowAllButtonAction(_ sender: UIButton) {
        UserFiltersViewController.selectedHeightModel.isHeightShowAll = !(UserFiltersViewController.selectedHeightModel.isHeightShowAll ?? false)
        setHeightUI()
        addHeightInSelectedFilterList(id:1, name: AppConstants.Strings.SHOW_ALL_HEIGHT_TEXT, type:AppConstants.HEIGHT_FILTER, subType:0,list:[],isSelected: UserFiltersViewController.selectedHeightModel.isHeightShowAll ?? false)
        self.selectHeightView.selectedMinValue = 0
        self.selectHeightView.selectedMaxValue = 100
        leftRightButtonHiddenShow()
    }
    
    func moveCollectionToFrame(contentOffset : CGFloat) {
        let frame: CGRect = CGRect(x : contentOffset ,y : self.filtersShowCollectionView.contentOffset.y ,width : self.filtersShowCollectionView.frame.width,height : self.filtersShowCollectionView.frame.height)
        self.filtersShowCollectionView.scrollRectToVisible(frame, animated: true)
    }
    
    func getStyles(searchQuery:String? = nil) {
        profileViewModel.getStyles(accessToken: AppDefaults.getAccessToken(),searchQuery: searchQuery ,{ (response) in
            if response.response?.statusCode == 200 || response.response?.statusCode == 201{
                let data: ApiResponse<[Styles]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let styles = data.data!
                    self.setStylesList(style: styles)
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.stylesActivityIndicator.stopAnimating()
            }
        } , { (error) in
            self.stylesActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getBrands(searchQuery:String? = nil) {
        createViewModel.getBrandsForFilters(accessToken: getAccessToken(),search: searchQuery ?? "",page:self.currentPage,{(response) in
            self.isApiCalled = false
            self.refreshControl?.endRefreshing()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Brand> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let brands = data.data!
                    
                    if brands.items!.count > 0{
                        if var allBrands = brands.items{
                            if self.currentPage == 1 {
                                let brand = Brand(brandId: 0, brandName: AppConstants.Strings.SHOW_ALL_TEXT)
                                allBrands.insert(brand, at: 0)
                                self.filterUserBrandHandler.brands = allBrands
                            } else {
                                self.filterUserBrandHandler.brands.append(contentsOf: allBrands)
                            }
                            if brands.totalPages == self.currentPage {
                                self.isApiCalled = true
                                self.filterUserBrandHandler.isLoadMoreRequired = false
                            }
                            self.brands = allBrands
                            self.showBrands( brands: allBrands)
                        }
                    }else{
                        self.filterUserBrandHandler.brands.removeAll()
                        self.brandsTableView.reloadData()
                        self.brandsActivityIndicator.stopAnimating()
                    }
                }
                self.currentPage = self.currentPage + 1
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.brandsActivityIndicator.stopAnimating()
            }
        } , { (error) in
            self.brandsActivityIndicator.stopAnimating()
            self.refreshControl?.endRefreshing()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func showBrands( brands:[Brand]) {
        self.brandsTableView.reloadData()
        self.brandsActivityIndicator.stopAnimating()
    }
    
    func getBodyType() {
        editProfileViewModel.getBodyType(accessToken: getTemporaryToken(), {(response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[BodyType]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let bodyType = data.data!
                    self.setBodyTypeList(bodyType:bodyType)
                }
            } else {
                self.bodyTypeActivityIndicator.stopAnimating()
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.bodyTypeActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func setStylesList(style:[Styles]){
        self.styles = style
        let style = Styles(descriptorId: 0, descriptorName: AppConstants.Strings.SHOW_ALL_TEXT)
        self.styles.insert(style, at: 0)
        self.filterStyleHandler.styles = self.styles
        self.stylesTableView.reloadData()
        self.stylesActivityIndicator.stopAnimating()
    }
    
    func setBodyTypeList(bodyType:[BodyType]){
        self.bodyType = bodyType
        let bodyType = BodyType(bodyTypeId: 0, bodyTypeName: AppConstants.Strings.SHOW_ALL_TEXT)
        self.bodyType.insert(bodyType, at: 0)
        self.filterUserBodyTypeHandler.bodyType = self.bodyType
        self.bodyTypeTableView.reloadData()
        self.bodyTypeActivityIndicator.stopAnimating()
    }
    
    func makeStyleBold(){
        self.styleLabel.font = UIFont.fontBold(size: 14)
        self.styleLabel.textColor = UIColor.cF87DE8
    }
    
    func makeStyleRegular(){
        self.styleLabel.font = UIFont.fontRegular(size: 14)
        self.styleLabel.textColor = UIColor.white
    }
    
    func makeBodyTypeBold(){
        self.bodyTypeLabel.font = UIFont.fontBold(size: 14)
        self.bodyTypeLabel.textColor = UIColor.cF87DE8
    }
    
    func makeBodyTypeRegular(){
        self.bodyTypeLabel.font = UIFont.fontRegular(size: 14)
        self.bodyTypeLabel.textColor = UIColor.white
    }
    
    func makeBrandsBold(){
        self.brandsLabel.font = UIFont.fontBold(size: 14)
        self.brandsLabel.textColor = UIColor.cF87DE8
    }
    
    func makeBrandsRegular(){
        self.brandsLabel.font = UIFont.fontRegular(size: 14)
        self.brandsLabel.textColor = UIColor.white
    }
    
    func makeHeightBold(){
        self.heightLabel.font = UIFont.fontBold(size: 14)
        self.heightLabel.textColor = UIColor.cF87DE8
    }
    
    func makeHeightRegular(){
        self.heightLabel.font = UIFont.fontRegular(size: 14)
        self.heightLabel.textColor = UIColor.white
    }
    
    func setSelectedView(id:Int, name:String , filterType:Int, subType:Int, subTypeName:String?) {
        setCheck(filterType: filterType, noHeight: false)
        let selectedFilter = SelectedFilter(id: id, name: name, type: filterType, subType: subType,subTypeName: subTypeName)
        var filtersToRemove : [SelectedFilter] = []
        for i in 0..<UserFiltersViewController.selectedFilter.count{
            if (filterType == UserFiltersViewController.selectedFilter[i].type) {
                if (id == UserFiltersViewController.selectedFilter[i].id) {
                    return
                } else {
                    if (id == 0) {
                        if (UserFiltersViewController.selectedFilter[i].type == filterType) {
                            filtersToRemove.append(contentsOf: getAllSelectedFilters(type: UserFiltersViewController.selectedFilter[i].type!))
                        }
                    } else {
                        
                        let showAllFilter = getShowAllFilter(type: UserFiltersViewController.selectedFilter[i].type!)
                        if (showAllFilter != nil) {
                            filtersToRemove.append(showAllFilter!)
                        }
                    }
                }
            }
        }
        
        var ids:[Int] = []
        for j in 0..<UserFiltersViewController.selectedFilter.count {
            for i in 0..<filtersToRemove.count{
                if filtersToRemove[i].id == UserFiltersViewController.selectedFilter[j].id && filtersToRemove[i].name == UserFiltersViewController.selectedFilter[j].name && filtersToRemove[i].type == UserFiltersViewController.selectedFilter[j].type {
                    ids.append(j)
                }
            }
        }
        
        UserFiltersViewController.selectedFilter = UserFiltersViewController.selectedFilter.enumerated()
            .filter { !ids.contains($0.offset) }
            .map { $0.element }
                
        //UserFiltersViewController.selectedFilter.remove(at: ids)
        UserFiltersViewController.selectedFilter.append(selectedFilter)
        setSelectedViewAdapter()
    }
    
    private func setSelectedViewAdapter() {
        filtersHandler.filter = UserFiltersViewController.selectedFilter
        filtersShowCollectionView.reloadData()
        self.leftRightButtonHiddenShow()
    }
    
    private func getAllSelectedFilters(type:Int) -> [SelectedFilter] {
        var selectedFilters = [SelectedFilter]()
        for i in 0..<UserFiltersViewController.selectedFilter.count{
            if UserFiltersViewController.selectedFilter[i].type == type{
                selectedFilters.append(UserFiltersViewController.selectedFilter[i])
            }
        }
        return selectedFilters
    }
    
    private func getShowAllFilter(type:Int) -> SelectedFilter?{
        for i in 0..<UserFiltersViewController.selectedFilter.count {
            if (UserFiltersViewController.selectedFilter[i].type == type && UserFiltersViewController.selectedFilter[i].id == 0) {
                return UserFiltersViewController.selectedFilter[i]
            }
        }
        return nil
    }
    
    func setFilterCollectionView(){
        self.filtersHandler.filter = UserFiltersViewController.selectedFilter
        self.filtersShowCollectionView.reloadData()
    }
    
    func setCheck(filterType:Int,noHeight:Bool){
        switch (filterType) {
        case AppConstants.USER_STYLE_FILTER:
            self.styleTickImageView.isHidden = false
            break
        case AppConstants.USER_BODY_TYPE_FILTER:
            self.bodyTypeTickImageView.isHidden = false
            break
        case AppConstants.USER_BRAND_FILTER:
            self.brandsTickImageView.isHidden = false
            break
        case AppConstants.HEIGHT_FILTER:
            self.heightTickImageView.isHidden = noHeight
            break
        default:
            break
        }
        
    }
    
    func addRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        brandsTableView.addSubview(refreshControl!)
    }
    
    @objc func refreshList(){
        currentPage = 1
        isApiCalled = false
        self.filterUserBrandHandler.isLoadMoreRequired = true
        self.getBrands()
    }
    
    func isFilterSelected(type:Int, id:Int , subType:Int , isParentType:Bool) -> Bool{
        for i in 0..<UserFiltersViewController.selectedFilter.count {
            let selectedFilter = UserFiltersViewController.selectedFilter[i]
            if (selectedFilter.type == type && selectedFilter.id == id && selectedFilter.subType == subType) {
                return true
            }
        }
        return false
    }
    
}

extension UserFiltersViewController: RangeSeekSliderDelegate{
    
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        print("Custom slider updated. Min Value: \(minValue) Max Value: \(maxValue)")
        let leftValue:Double = Double(minValue)
        let rightValue:Double = Double(maxValue)
        let selectedMinValue:Int = (Int(leftValue / divFactor))
        UserFiltersViewController.selectedHeightModel.customMinHeightInches = selectedMinValue % 12
        var selectedMinFeetValue = selectedMinValue / 12
        selectedMinFeetValue = self.minHeight + selectedMinFeetValue
        UserFiltersViewController.selectedHeightModel.customMinHeightFeet = selectedMinFeetValue
        UserFiltersViewController.selectedHeightModel.minPinValue = Float(leftValue)
        let selectedMaxValue:Int = (Int(rightValue / divFactor))
        UserFiltersViewController.selectedHeightModel.customMaxHeightInches = selectedMaxValue % 12
        var selectedMaxFeetValue:Int = selectedMaxValue / 12
        selectedMaxFeetValue = minHeight + selectedMaxFeetValue
        UserFiltersViewController.selectedHeightModel.customMaxHeightFeet = selectedMaxFeetValue
        UserFiltersViewController.selectedHeightModel.maxPinValue = Float(rightValue)
        addHeightInSelectedFilterList(id:1, name: self.heightInTextLabel.text ?? "", type:AppConstants.HEIGHT_FILTER, subType:0,list:[],isSelected:true)
        setHeightValue()
    }
    
    func addHeightInSelectedFilterList(id:Int, name:String, type:Int, subType:Int,list:[Int],isSelected:Bool) {
        for  i in 0..<UserFiltersViewController.selectedFilter.count {
            if (UserFiltersViewController.selectedFilter[i].type == AppConstants.HEIGHT_FILTER) {
                UserFiltersViewController.selectedFilter.remove(at:i)
                break
            }
        }
        if (isSelected) {
            let selectedFilter = SelectedFilter(id: id, name: name, type: type, subType: subType,subTypeName: nil)
            UserFiltersViewController.selectedFilter.append(selectedFilter)
        }
        self.filtersHandler.filter = UserFiltersViewController.selectedFilter
        self.filtersShowCollectionView.reloadData()
        self.setCheck(filterType: AppConstants.HEIGHT_FILTER, noHeight: !isSelected)
        
    }
    
    func didStartTouches(in slider: RangeSeekSlider) {
        print("did start touches")
    }
    
    func didEndTouches(in slider: RangeSeekSlider) {
        print("did end touches")
    }
    
    func setHeightValue() {
        
        let customMinHeightFeet = String(UserFiltersViewController.selectedHeightModel.customMinHeightFeet)
        let customMinHeightInches = String(UserFiltersViewController.selectedHeightModel.customMinHeightInches)
        let customMaxHeightFeet = String(UserFiltersViewController.selectedHeightModel.customMaxHeightFeet)
        let customMaxHeightInches = String(UserFiltersViewController.selectedHeightModel.customMaxHeightInches)
        
        let height = customMinHeightFeet + " ft " + customMinHeightInches + " in " + " - " + customMaxHeightFeet + " ft " + customMaxHeightInches + " in "
        self.heightInTextLabel.text = height
        
    }
    
    func setHeightUI() {
        if (UserFiltersViewController.selectedHeightModel.isHeightShowAll ?? false) {
            self.selectHeightView.isUserInteractionEnabled = false
            self.showAllHeightButton.setImage(UIImage(named: AppConstants.Images.CHECKBOX_SELECTED), for: .normal)
            self.selectHeightView.handleImage = UIImage(named: AppConstants.Images.HEIGHT_LOCKED_ICON)
            self.selectHeightView.colorBetweenHandles = UIColor.cB1B1B1
            self.selectHeightView.tintColor = UIColor.cB1B1B1
            
            UserFiltersViewController.selectedHeightModel.customMaxHeightFeet = 8
            UserFiltersViewController.selectedHeightModel.customMinHeightFeet = 3
            UserFiltersViewController.selectedHeightModel.customMaxHeightInches = 0
            UserFiltersViewController.selectedHeightModel.customMinHeightInches = 0
            self.selectTextLabel.textColor = UIColor.cB1B1B1
            self.heightInTextLabel.textColor = UIColor.cB1B1B1
            self.selectHeightView.selectedMinValue = 0
            self.selectHeightView.selectedMaxValue = 100
        } else {
            self.selectHeightView.isUserInteractionEnabled = true
            self.selectHeightView.colorBetweenHandles = UIColor.cBB189C
            self.selectHeightView.handleImage = UIImage(named: AppConstants.Images.FILTER_RANGE_ICON)
            
            self.selectTextLabel.textColor = UIColor.c313131
            self.heightInTextLabel.textColor = UIColor.c313131
            self.selectHeightView.tintColor = UIColor.c323232
            
            self.showAllHeightButton.setImage(UIImage(named: AppConstants.Images.CHECKBOX), for: .normal)
            self.selectHeightView.selectedMinValue = CGFloat(UserFiltersViewController.selectedHeightModel.minPinValue)
            self.selectHeightView.selectedMaxValue = CGFloat(UserFiltersViewController.selectedHeightModel.maxPinValue)
            
        }
        selectHeightView.layoutSubviews()
        setHeightValue()
    }
    
}
