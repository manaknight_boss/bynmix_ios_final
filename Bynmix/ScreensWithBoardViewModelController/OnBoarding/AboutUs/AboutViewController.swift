import UIKit

class AboutViewController: BaseViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    let createViewModel = CreateViewModel()
    let aboutHandler = AboutHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = aboutHandler
        tableView.dataSource = aboutHandler
        navigationItem.title = AppConstants.ABOUT_TITLE
        getAbout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    func getAbout(){
        tableView.indicatorStyle = UIScrollView.IndicatorStyle.white
        
        createViewModel.getAbout({ (response) in
            self.loader.stopAnimating()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<AppInfo> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let appInfo = data.data
                    if let about = appInfo?.sections {
                        self.aboutHandler.about = about
                        self.tableView.isHidden = false
                        self.tableView.reloadData()
                    }
                }
            } else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.loader.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
}
