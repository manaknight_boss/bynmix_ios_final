import UIKit
import AVKit
import AVFoundation

class CreateAccountViewController: BaseViewController {
    
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var googleButton: UIButton!
    @IBOutlet var loginLabel: UILabel!
    
    var player: AVPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        emailButton.clipsToBounds = true
        facebookButton.clipsToBounds = true
        googleButton.clipsToBounds = true
        setText()
    }
    
    func setText(){
        let text = "Already a member? Login here"
        let textUnderline = "Login here"
        let rangeWhole = (text as NSString).range(of: text)
        let range = (text as NSString).range(of: textUnderline)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 16), range: rangeWhole)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: rangeWhole)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range)
        loginLabel.attributedText = attributedString
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initBackgroundVideo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        if player != nil{
            player?.replaceCurrentItem(with: nil)
            player = nil
        }
    }
    
    func initBackgroundVideo() {
        videoView.layer.zPosition = 0;
        let videoURL: NSURL = Bundle.main.url(forResource: AppConstants.VIDEO_NAME_TWO, withExtension: AppConstants.VIDEO_EXTENSION)! as NSURL
        
        player = AVPlayer(url: videoURL as URL)
        player?.isMuted = true
        
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.zPosition = -1
        
        playerLayer.frame = view.frame
        
        view.layer.addSublayer(playerLayer)
        
        player?.play()
        
        NotificationCenter.default.addObserver(self, selector: #selector(loopVideo), name: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem)
    }
    
    @IBAction func GIDSignInButton(_ sender: AnyObject) {
        initGoogleLogin()
    }
    
    @IBAction func FacebookLoginButton(_ sender: Any) {
        loginButtonClicked(type: 0)
    }
    
    @IBAction func onloginClick(_ sender: UITapGestureRecognizer) {
     self.navigationController?.popToRootViewController(animated: true)
    }
    @objc func loopVideo() {
        player?.seek(to: CMTime.zero)
        player?.play()
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func registerButtonAction(_ sender: UIButton) {
        self.registerScreen()
    }
    
}
