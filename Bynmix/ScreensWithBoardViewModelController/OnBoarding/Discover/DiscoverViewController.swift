import UIKit

class DiscoverViewController: BaseViewController {
    
    @IBOutlet var disvoverView: UIView!
    @IBOutlet var pageControl: UIPageControl!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var introductionTableView: UITableView!
    @IBOutlet var introImageView: UIImageView!
    @IBOutlet var introTitleLabel: UILabel!
    @IBOutlet var tableViewHeight: NSLayoutConstraint!
    
    @IBOutlet var backgroundImageView: UIImageView!
    
    @IBOutlet var circleOneView: UIView!
    @IBOutlet var circleTwoView: UIView!
    @IBOutlet var circleThreeView: UIView!
    
    let onboardingViewModel = OnBoardingViewModel()
    var introductionHandler = IntroductionHandler()
    var introduction = [Introduction]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        makeViewsRound()
        introTitleLabel.font = UIFont.fontMontserratHairline(size: 30)
        backgroundImageView.contentMode = .scaleToFill
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        self.introductionTableView.delegate = introductionHandler
        self.introductionTableView.dataSource = introductionHandler
        introductionData()
    }
    
    @IBAction func nextButtonAction(_ sender: UIButton) {
        self.pageControlSwipeRight()
    }
    
    @IBAction func skipButtonAction(_ sender: UIButton) {
        // go to next screen
        self.onboardingStartScreen()
    }
    
    func onboardingStartScreen(){
        self.presentViewControllerForOnBoarding(storyboardName: AppConstants.Storyboard.ONBOARDING_START_STORYBOARD, viewControllerIdentifier: AppConstants.Controllers.ONBOARDING_START_SCREEN)
    }
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {

        if let swipeGesture = gesture as? UISwipeGestureRecognizer {

            switch swipeGesture.direction {
            case .right:
                self.pageControlSwipeLeft()
            case .left:
                self.pageControlSwipeRight()
            default:
                break
            }
        }
    }
    
    func pageControlSwipeRight(){
        if pageControl.currentPage == 0{
            pageControl.currentPage = 1
            self.circleTwoViewSelected()
        }else if pageControl.currentPage == 1{
            pageControl.currentPage = 2
            self.circleThreeViewSelected()
        }else if pageControl.currentPage == 2{
            self.onboardingStartScreen()
        }
        self.setData(introduction:self.introduction)
    }
    
    func pageControlSwipeLeft(){
        if pageControl.currentPage == 2{
            pageControl.currentPage = 1
            self.circleTwoViewSelected()
        }else if pageControl.currentPage == 1{
            pageControl.currentPage = 0
            self.circleOneViewSelected()
        }
        self.setData(introduction:self.introduction)
    }
    
    func introductionData() {
        onboardingViewModel.introduction(accessToken: getTemporaryToken(), {(response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<MainIntro> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let introduction = data.data!
                    self.pageControl.currentPage = 0
                    self.circleOneViewSelected()
                    if let intro = introduction.sections {
                        self.introduction = intro
                        self.pageControl.numberOfPages = self.introduction.count
                        self.setData(introduction:self.introduction)
                    }
                }
            }else{
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
                self.screenActivityIndicator.stopAnimating()
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func setData(introduction:[Introduction]){
        self.introTitleLabel.text = introduction[pageControl.currentPage].title
        self.introductionHandler.introduction = introduction[pageControl.currentPage].points!
        guard let imageurl = introduction[pageControl.currentPage].imageUrl else {
            return
        }
        self.introImageView.kf.setImage(with: URL(string: imageurl))
        self.introImageView.contentMode = .scaleAspectFit
        self.screenActivityIndicator.stopAnimating()
        introductionTableView.reloadData()
        self.mainView.isHidden = false
        self.resizeTableView(introduction:introduction)
    }
    
    func resizeTableView(introduction:[Introduction]){
        if introduction.count == 0{
            self.tableViewHeight.constant = 0
        }else{
            var tableViewHeight: CGFloat {
                self.introductionTableView.layoutIfNeeded()
                return self.introductionTableView.contentSize.height
            }
            print("tableViewHeight:",tableViewHeight)
            self.tableViewHeight?.constant = tableViewHeight
        }
    }
    
    func makeViewsRound(){
        self.circleOneView.layer.cornerRadius = self.circleOneView.frame.size.width / 2
        self.circleOneView.clipsToBounds = true
        self.circleTwoView.layer.cornerRadius = self.circleTwoView.frame.size.width / 2
        self.circleTwoView.clipsToBounds = true
        self.circleThreeView.layer.cornerRadius = self.circleThreeView.frame.size.width / 2
        self.circleThreeView.clipsToBounds = true
    }
    
    func circleOneViewSelected(){
        self.circleOneView.backgroundColor = UIColor.cBB189C
        self.circleTwoView.backgroundColor = UIColor.cEFEFEF
        self.circleThreeView.backgroundColor = UIColor.cEFEFEF
    }
    
    func circleTwoViewSelected(){
        self.circleTwoView.backgroundColor = UIColor.cBB189C
        self.circleOneView.backgroundColor = UIColor.cEFEFEF
        self.circleThreeView.backgroundColor = UIColor.cEFEFEF
    }
    
    func circleThreeViewSelected(){
        self.circleThreeView.backgroundColor = UIColor.cBB189C
        self.circleOneView.backgroundColor = UIColor.cEFEFEF
        self.circleTwoView.backgroundColor = UIColor.cEFEFEF
    }
    

}
