import UIKit
import AVKit
import AVFoundation
import Toast_Swift

class ForgotPasswordViewController: BaseViewController {
    
    @IBOutlet var videoView: UIView!
    @IBOutlet var buttonLoader: UIActivityIndicatorView!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var emailErrorIcon: UIButton!
    @IBOutlet var submitButton: UIButton!
    @IBOutlet var loginLabel: UILabel!
    
    var player: AVPlayer?
    let forgotPasswordViewModel = ForgotPasswordViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonLoader.stopAnimating()
        submitButton.isEnabled = true
        
        emailTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        setText()
    }
    
    func setText(){
        let text = "Back to login"
        let textUnderline = "Back to login"
        let range = (text as NSString).range(of: textUnderline)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontMedium(size: 16), range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: range)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range)
        loginLabel.attributedText = attributedString
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initBackgroundVideo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        if player != nil{
            player?.replaceCurrentItem(with: nil)
            player = nil
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailTextField:
            emailTextField.resignFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        switch textField {
        case emailTextField:
            emailErrorIcon.isHidden = true
        default:
            emailErrorIcon.isHidden = true
        }
        hideToolTip()
        
    }
    
    @IBAction func showErrorToolTip(_ sender: UIButton) {
         showErrorTooltip(errorMessage: AppConstants.Strings.INVALID_EMAIL, view: sender)
    }
    
   
    func initBackgroundVideo() {
        videoView.layer.zPosition = 0;
        let videoURL: NSURL = Bundle.main.url(forResource: AppConstants.VIDEO_NAME_ONE, withExtension: AppConstants.VIDEO_EXTENSION)! as NSURL
        
        player = AVPlayer(url: videoURL as URL)
        player?.isMuted = true
        
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.zPosition = -1
        
        playerLayer.frame = view.frame
        
        view.layer.addSublayer(playerLayer)
        
        player?.play()
        
        NotificationCenter.default.addObserver(self, selector: #selector(loopVideo), name: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem)
    }
    
    func sendCode(email: String) {
        buttonLoader.startAnimating()
        submitButton.isEnabled = false
        let forgotPassword = ForgotPasswordRequest(email: email)
        forgotPasswordViewModel.sendCode(accessToken: getTemporaryToken(), data: forgotPassword,{(response) in
            self.submitButton.isEnabled = true
            self.buttonLoader.stopAnimating()
            if response.response?.statusCode == 201 {
                self.verifyCodeScreen(message: email)
                
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.submitButton.isEnabled = true
            self.buttonLoader.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            } else if error.statusCode == 401 {
    
            }
        })
    }
    
    func verifyCodeScreen(message: String) {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.VERIFY_CODE, bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.VERIFY_CODE) as! VerifyCodeViewController
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        myAlert.message = message
        navigationController?.pushViewController(myAlert, animated: true)
    }
    
    @objc func loopVideo() {
        player?.seek(to: CMTime.zero)
        player?.play()
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backButtonLabel(_ sender: UITapGestureRecognizer) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSubmitButtonPressed(_ sender: UIButton) {
        let email:String = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        
        var isError = false
        if !ValidationHelper.isValidEmail(email: email) {
            emailErrorIcon.isHidden = false
            isError = true
        } else {
            emailErrorIcon.isHidden = true
        }
        hideToolTip()
        if isError {
            return
        }
        sendCode(email: email.trimmingCharacters(in: .whitespacesAndNewlines))
    }
    
    @IBAction func onCodeButtonTap(_ sender: UIButton) {
      self.pushViewController(storyboardName: AppConstants.Storyboard.VERIFY_CODE, viewControllerIdentifier: AppConstants.Controllers.VERIFY_CODE)
    }
    
}
