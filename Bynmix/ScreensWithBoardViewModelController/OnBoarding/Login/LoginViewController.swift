import UIKit
import AVFoundation
import AVKit

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var usernameErrorIcon: UIButton!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var passwordErrorIcon: UIButton!
    @IBOutlet weak var createAccountLabel: UILabel!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var googleButton: UIButton!
    @IBOutlet weak var privacyPolicyLabel: UILabel!
    @IBOutlet var aboutLabel: UILabel!
    
    var player: AVPlayer?
    let loginViewModel = LoginViewModel()
    
    @IBAction func prepareForUnwind(segue: UIStoryboardSegue) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loginViewModel.loginInputsDelegate = self
        
        loader.stopAnimating()
        keyboardRequired(topConstraint: topConstraint)
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self as? UIGestureRecognizerDelegate
        
        usernameTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        setText()
        //printFonts()
    }
    
    func setText(){
        let text = "Not a member? Create Account"
        let textAttributed = "Create Account"
        let range = (text as NSString).range(of: text)
        let textRange = (text as NSString).range(of: textAttributed)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 16), range: range)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: range)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: textRange)
        createAccountLabel.attributedText = attributedString
        
        let aboutText = "About"
        let aboutTextAttributed = "About"
        let aboutRange = (aboutText as NSString).range(of: aboutTextAttributed)
        let aboutAttributedString = NSMutableAttributedString(string: aboutText)
        aboutAttributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 10), range: aboutRange)
        aboutAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: aboutRange)
        aboutAttributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: aboutRange)
        aboutLabel.attributedText = aboutAttributedString
        
        let termsText = "Terms of Service"
        let termsTextAttributed = "Terms of Service"
        let termsRange = (termsText as NSString).range(of: termsTextAttributed)
        let termsAttributedString = NSMutableAttributedString(string: termsText)
        termsAttributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 10), range: termsRange)
        termsAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: termsRange)
        termsAttributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: termsRange)
        termsLabel.attributedText = termsAttributedString
        
        let privacyPolicyText = "Privacy Policy"
        let privacyPolicyTextAttributed = "Privacy Policy"
        let privacyPolicyRange = (privacyPolicyText as NSString).range(of: privacyPolicyTextAttributed)
        let privacyPolicyAttributedString = NSMutableAttributedString(string: privacyPolicyText)
        privacyPolicyAttributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 10), range: privacyPolicyRange)
        privacyPolicyAttributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: privacyPolicyRange)
        privacyPolicyAttributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: privacyPolicyRange)
        privacyPolicyLabel.attributedText = privacyPolicyAttributedString
    }
    
    func printFonts(){
        for family in UIFont.familyNames {
            print("\(family)")

            for name in UIFont.fontNames(forFamilyName: family) {
                print("\(name)")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initBackgroundVideo()
        let registrationCompleted = AppDefaults.getRegistrationCompleted()
        if registrationCompleted {
            AppDefaults.setRegistrationCompleted(registrationCompleted: false)
            let lastOnboardingScreen = AppDefaults.getLastOnBoardingScreen()
            let onBoardingStage =  AppDefaults.getOnBoardingStage()
            self.checkForOnBoardingStage(onBoardingComplete: onBoardingStage, lastOnboardingScreenCompleted: lastOnboardingScreen)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        if player != nil{
            player?.replaceCurrentItem(with: nil)
            player = nil
        }
    }
    
    func initBackgroundVideo() {
        videoView.layer.zPosition = 0
        let videoURL: NSURL = Bundle.main.url(forResource: AppConstants.VIDEO_NAME_ONE, withExtension: AppConstants.VIDEO_EXTENSION)! as NSURL
        
        player = AVPlayer(url: videoURL as URL)
        player?.isMuted = true
        
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.zPosition = -1
        
        playerLayer.frame = view.frame
        
        view.layer.addSublayer(playerLayer)
        
        player?.play()
        
        NotificationCenter.default.addObserver(self, selector: #selector(loopVideo), name: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem)
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        switch textField {
        case usernameTextField:
            usernameErrorIcon.isHidden = true
        default:
            passwordErrorIcon.isHidden = true
        }
        hideToolTip()
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case usernameTextField:
            passwordTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }
    
    @IBAction func loginButtonAction(_ sender: UIButton) {
        let username: String = usernameTextField.text ?? ""
        let password: String = passwordTextField.text ?? ""

        let isUsernameValid = loginViewModel.validateUsername(username)
        let isPasswordValid = loginViewModel.validatePassword(password)

        usernameErrorIcon.isHidden = isUsernameValid
        passwordErrorIcon.isHidden = isPasswordValid

        hideToolTip()

        guard isUsernameValid, isPasswordValid else {
            return
        }

        loader.startAnimating()
        sender.isEnabled = false
        facebookButton.isEnabled = false
        googleButton.isEnabled = false

        loginViewModel.login(with: username, andPassword: password, sender: sender)
        
    }
    
    @IBAction func emailErrorButton(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.INVALID_USERNAME, view: sender)
    }
    
    @IBAction func passwodErrorButton(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.INVALID_PASSWORD, view: sender)
    }
    
    @objc func loopVideo() {
        player?.seek(to: CMTime.zero)
        player?.play()
    }
    
    @IBAction func loginWithGoogle(_ sender: Any) {
        initGoogleLogin()
    }
    
    @IBAction func loginWithFacebook(_ sender: Any) {
        loginButtonClicked(type: 0)
    }
    
    @IBAction func privacyPolicyTap(_ sender: UITapGestureRecognizer) {
        privacyPolicyScreen()
    }
    
    @IBAction func termsTap(_ sender: UITapGestureRecognizer) {
        termsOfServiceScreen()
    }
    
    @IBAction func aboutTap(_ sender: UITapGestureRecognizer) {
        aboutUsScreen()
    }
    
    @IBAction func createAccountTap(_ sender: UITapGestureRecognizer) {
        createAccountScreen()
    }
    
    @IBAction func onForgotPasswordTapped(_ sender: UITapGestureRecognizer) {
        forgotPasswordScreen()
    }
    
}

extension LoginViewController: LoginViewInputs {
    func clearPasswordAndLogin() {
        self.usernameTextField.text = ""
        self.passwordTextField.text = ""
    }

    func showConnectionError() {
        self.showInternetError()
    }

    func showMessage(_ message: String) {
        self.showMessageDialog(message: message)
    }

    func enableButtons(loginButton: UIButton) {
        self.loader.stopAnimating()
        loginButton.isEnabled = true
        self.facebookButton.isEnabled = true
        self.googleButton.isEnabled = true
    }
}
