import Foundation
import UIKit

protocol LoginViewInputs: class {
    // Note: Those are actions which ViewController needs to implement so viewModel can inform it about changes
    // ViewModel should do ALL the logic. ViewController should only react to changes
    func showConnectionError()
    func showMessage(_ message: String)
    func clearPasswordAndLogin()
    func enableButtons(loginButton: UIButton)
    func setTemporaryToken(accessToken: String)
    func checkForOnBoardingStage(onBoardingComplete: Bool, lastOnboardingScreenCompleted: Int)
}
