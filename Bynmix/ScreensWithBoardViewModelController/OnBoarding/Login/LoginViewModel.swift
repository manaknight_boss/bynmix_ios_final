import Foundation
import UIKit

class LoginViewModel {
    // Note: Could use repositories instead of models here since those models only do calls to repos anyway
    let authViewModel = AuthViewModel()
    let profileViewModel = ProfileViewModel()
    
    // Note: It's a delegate to inform viewController to update its view state
    weak var loginInputsDelegate: LoginViewInputs?
    
    func validateUsername(_ username: String) -> Bool {
        return ValidationHelper.isEmptyAny(field: username)
    }
    
    func validatePassword(_ password: String) -> Bool {
        return ValidationHelper.isValidPassword(password: password)
    }
    
    func login(with username: String, andPassword password: String, sender: UIButton) {
        let userAuth = UserAuth(username: username.trimmingCharacters(in: .whitespacesAndNewlines), password: password.trimmingCharacters(in: .whitespacesAndNewlines))
        
        authViewModel.login(request: userAuth, { (response) in
            if let token = response.access_token {
                let refreshToken = response.refresh_token ?? ""
                self.loginInputsDelegate?.setTemporaryToken(accessToken: token)
                AppDefaults.setRefreshToken(token: refreshToken)
                
                let milliSeconds = 0.25
                DispatchQueue.main.asyncAfter(deadline: .now() + milliSeconds) {
                    
                    self.profileViewModel.getUser(accessToken: token, { (response) in
                        if response.response?.statusCode == 200 {
                            let data: ApiResponse<User> = Utils.convertResponseToData(data: response.data!)
                            self.loginInputsDelegate?.enableButtons(loginButton: sender)
                            if data.isError {
                                let error = Utils.getErrorMessage(errors: data.errors)
                                self.loginInputsDelegate?.showMessage( error)
                            } else {
                                
                                AppDefaults.setUsername(username: username)
                                let user = data.data!
                                
                                self.saveDeviceId(token: token, deviceId: AppDefaults.getDeviceId(),deviceType: AppConstants.DEVICE_TYPE)
                                
                                var eventType = UserAuth(eventType: AppConstants.TYPE_EVENT_UNKNOWN)
                                if AppDefaults.getTokenExpired(){
                                    eventType = UserAuth(eventType: AppConstants.TYPE_EVENT_IDLE_LOGIN)
                                }else{
                                    eventType = UserAuth(eventType: AppConstants.TYPE_EVENT_LOGIN)
                                }
                                
                                self.trackUsers(token: token, data: eventType)
                                
                                self.loginInputsDelegate?.clearPasswordAndLogin()
                                
                                AppDefaults.setUserId(userId: user.userId!)
                                self.loginInputsDelegate?.checkForOnBoardingStage(onBoardingComplete: user.onboardingComplete ?? false, lastOnboardingScreenCompleted: user.lastOnboardingScreenCompleted ?? 0)
                            }
                        }
                        
                    } , { (error) in
                        if error.statusCode == AppConstants.NO_INTERNET {
                            self.loginInputsDelegate?.showConnectionError()
                        }
                        self.loginInputsDelegate?.enableButtons(loginButton: sender)
                        
                    })
                    
                }
                
            }
            if let error = response.error_description {
                self.loginInputsDelegate?.showMessage(error)
                self.loginInputsDelegate?.enableButtons(loginButton: sender)
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.loginInputsDelegate?.showConnectionError()
            }
            
            self.loginInputsDelegate?.enableButtons(loginButton: sender)
        })
    }
    
    func saveDeviceId(token: String, deviceId: String, deviceType: String) {
        let deviceIdRequest = DeviceIdRequest(deviceId: deviceId,deviceType: deviceType)
        authViewModel.updateDeviceId(accessToken: token,data:deviceIdRequest,{ (response) in
            if response.response?.statusCode == 200 {
                
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.loginInputsDelegate?.showMessage(error)
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.loginInputsDelegate?.showConnectionError()
            } 
        })
    }
    
    func trackUsers(token: String, data: UserAuth) {
        let userAuth = data
        authViewModel.trackUsers(accessToken: token,data:userAuth,{ (response) in
            if response.response?.statusCode == 200 || response.response?.statusCode == 201{
                AppDefaults.setTokenExpired(isTokenExpired: false)
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.loginInputsDelegate?.showMessage(error)
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.loginInputsDelegate?.showConnectionError()
            }
        })
    }
    
    
}
