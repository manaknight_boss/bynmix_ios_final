import UIKit

class PrivacyPolicyViewController: BaseViewController {

    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var committedLabel: UILabel!
    
    var privacyPolicy = [Terms]()
    let appInfoViewModel = AppInfoViewModel()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        appInfoViewModel.getPrivacyPolicy({ (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<AppInfo> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let appInfo = data.data
                    if let sections = appInfo?.sections {
                        self.loader.isHidden = true
                        let section1 = sections[0]
                        let section2 = sections[1]
                        self.descriptionLabel.text = section1.description
                        self.committedLabel.text = section2.description
                        var message = ""
                        if let points = section1.points {
                            var i = 0
                            for point in points {
                                if i == 0 {
                                    message = message + "\n"
                                } else {
                                    message = message + "\n\n"
                                }
                                message = message + "- " + point
                                i = i + 1
                                
                            }
                        }
                        self.pointsLabel.text = message
                    }
                }
            } else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    
}
