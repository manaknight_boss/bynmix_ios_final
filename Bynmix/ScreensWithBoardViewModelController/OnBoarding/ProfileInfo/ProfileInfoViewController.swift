import UIKit
import Toast_Swift

class ProfileInfoViewController: BaseViewController {

    @IBOutlet var titleTextField: UITextField!
    @IBOutlet var genderLabel: UILabel!
    @IBOutlet var bodyTypeLabel: UILabel!
    @IBOutlet var feetLabel: UILabel!
    @IBOutlet var inchesLabel: UILabel!
    @IBOutlet var buttonLoader: UIActivityIndicatorView!
    @IBOutlet var onboardingCompleteButton: UIButton!
    @IBOutlet var whyLabel: UILabel!
    @IBOutlet var backgroundImageView: UIImageView!
    
    var genderSelect = [String]()
    var feetSelect = [String]()
    var inchesSelect = [String]()
    var bodyTypeSelect = [BodyType]()
    var selectedBodyType = 0
    
    let TYPE_GENDER : Int = 1
    let TYPE_FEET : Int = 2
    let TYPE_INCH : Int = 3
    let TYPE_BODY : Int = 4
    
    let profileViewModel = ProfileViewModel()
    let editProfileViewModel = EditProfileViewModel()
    let authViewModel = AuthViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundImageView.contentMode = .scaleToFill
        genderSelect = AppConstants.StaticLists.gender
        feetSelect = AppConstants.StaticLists.feet
        inchesSelect = AppConstants.StaticLists.inches
        self.view.makeToastActivity(.center)
        titleTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        setText()
        getBodyType()
        buttonLoader.stopAnimating()
    }
    
    func setText(){
        let text:String = "Tell us a little bit about you! ( Why? )"
        let textRegular:String = "Tell us a little bit about you! ( Why? )"
        let textUnderline = "Why? "
        let range = (text as NSString).range(of: textRegular)
        let rangeUnderline = (text as NSString).range(of: textUnderline)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 16), range: range)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: rangeUnderline)
        whyLabel.attributedText = attributedString
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        checkStateChanged()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case titleTextField:
            titleTextField.resignFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }

    @IBAction func onWhyClick(_ sender: Any) {
      showMessageDialog(message: AppConstants.Strings.PROFILE_INFO_DESC)
        }
    
    @IBAction func onGenderClick(_ sender: Any) {
        showDropDown(tag: TYPE_GENDER, title: AppConstants.Strings.CHOOSE_GENDER, dataSourse: self, delegate: self, {(row) in
            self.genderLabel.textColor = UIColor.cD3D3D3
            self.genderLabel.text = self.genderSelect[row]
            self.checkStateChanged()
        })
    }
    
    
    @IBAction func onFeetClick(_ sender: Any) {
        showDropDown(tag: TYPE_FEET, title: AppConstants.Strings.CHOOSE_FEET, dataSourse: self, delegate: self, {(row) in
            self.feetLabel.textColor = UIColor.cD3D3D3
            self.feetLabel.text = self.feetSelect[row]
            self.checkStateChanged()
        })
        
    }
    
    @IBAction func onInchesClick(_ sender: Any) {
        showDropDown(tag: TYPE_INCH, title: AppConstants.Strings.CHOOSE_INCHES, dataSourse: self, delegate: self, {(row) in
            self.inchesLabel.textColor = UIColor.cD3D3D3
            self.inchesLabel.text = self.inchesSelect[row]
            self.checkStateChanged()
        })
    }
    
    @IBAction func onBodyTypeClick(_ sender: Any) {
        showDropDown(tag: TYPE_BODY, title:  AppConstants.Strings.CHOOSE_BODY_TYPE, dataSourse: self, delegate: self, {(row) in
            self.bodyTypeLabel.textColor = UIColor.cD3D3D3
            self.bodyTypeLabel.text = self.bodyTypeSelect[row].bodyTypeName
            self.selectedBodyType = self.bodyTypeSelect[row].bodyTypeId!
            self.checkStateChanged()
        })
        
    }
    
    func checkStateChanged() {
        let feet = feetLabel.text == AppConstants.FEET ? 0 : Int(feetLabel.text ?? "")
        let inch = inchesLabel.text == AppConstants.INCHES ? 0 : Int(inchesLabel.text ?? "")
        var gender = 0
        if genderLabel.text == AppConstants.MALE {
            gender = 2
        } else if genderLabel.text == AppConstants.FEMALE {
            gender = 1
        }else{
            gender = 0
        }
        let title = titleTextField.text ?? ""
        
        if title == "" && gender == 0 && feet == 0 && selectedBodyType == 0 && inch == 0 {
            onboardingCompleteButton.setTitle(AppConstants.Strings.SKIP_THIS_STEP, for: .normal)
        } else {
            onboardingCompleteButton.setTitle(AppConstants.Strings.DONE, for: .normal)
        }
        
    }
    
    @IBAction func profileInfoClick(_ sender: UIButton) {
        self.onboardingCompleteButton.isEnabled = false
        let title:String = titleTextField.text ?? ""
        
        let feet = feetLabel.text == AppConstants.FEET ? 0 : Int(feetLabel.text ?? "")
        let inch = inchesLabel.text == AppConstants.INCHES ? 0 : Int(inchesLabel.text ?? "")
        var gender = 0
        if genderLabel.text == AppConstants.MALE {
            gender = 2
        } else if genderLabel.text == AppConstants.FEMALE {
            gender = 1
        } else {
            gender = 0
        }
        profileInfo(shortTitle: title.trimmingCharacters(in: .whitespacesAndNewlines),gender: gender,bodyTypeId: selectedBodyType,heightFeet: feet!,heightInches: inch!)
        AppDefaults.setWelcomeView(welocmeView: true)
    }
    
    func profileInfo(shortTitle: String,gender: Int,bodyTypeId: Int,heightFeet: Int,heightInches: Int) {
        buttonLoader.startAnimating()
        onboardingCompleteButton.isEnabled = false

        let user = User(shortTitle: shortTitle,gender: gender,bodyTypeId: bodyTypeId,heightFeet: heightFeet,heightInches: heightInches,onboardingComplete: true)
        
        profileViewModel.updateProfileInfo(accessToken: getTemporaryToken(), data: user,{(response) in
            if response.response?.statusCode == 200 {
                self.setAccessToken(accessToken: self.getTemporaryToken())
                self.saveDeviceId(token:AppDefaults.getAccessToken(),deviceId: AppDefaults.getDeviceId(),deviceType: AppConstants.DEVICE_TYPE)
                self.buttonLoader.stopAnimating()
                self.onboardingCompleteButton.isEnabled = true
                self.tabsController()
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.buttonLoader.stopAnimating()
                self.onboardingCompleteButton.isEnabled = true
            }
        } , { (error) in
            self.buttonLoader.stopAnimating()
            self.onboardingCompleteButton.isEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func saveDeviceId(token:String,deviceId: String,deviceType: String) {
        let deviceIdRequest = DeviceIdRequest(deviceId: deviceId,deviceType: deviceType)
        authViewModel.updateDeviceId(accessToken: token,data:deviceIdRequest,{ (response) in
            if response.response?.statusCode == 200 {
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
}

extension ProfileInfoViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == TYPE_GENDER {
            return genderSelect.count
        } else if pickerView.tag == TYPE_FEET {
            return feetSelect.count
        } else if pickerView.tag == TYPE_INCH {
            return inchesSelect.count
        } else {
            return bodyTypeSelect.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == TYPE_GENDER {
            return genderSelect[row]
        } else if pickerView.tag == TYPE_FEET {
            return feetSelect[row]
        } else if pickerView.tag == TYPE_INCH {
            return inchesSelect[row]
        } else {
            return bodyTypeSelect[row].bodyTypeName
        }
    }
 
    private func getBodyType() {
        editProfileViewModel.getBodyType(accessToken: getTemporaryToken(), {(response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[BodyType]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.bodyTypeSelect = data.data!
                }
            } else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            } 
        })
    }
    
}


