import UIKit
import AVFoundation
import AVKit

class RegisterViewController: BaseViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var firstnameTextField: UITextField!
    @IBOutlet weak var lastnameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var emailErrorIcon: UIButton!
    @IBOutlet weak var usernameErrorIcon: UIButton!
    @IBOutlet weak var firstnameErrorIcon: UIButton!
    @IBOutlet weak var lastnameErrorIcon: UIButton!
    @IBOutlet weak var passwordErrorIcon: UIButton!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var checkbox: UIImageView!
    @IBOutlet weak var termsLabel: UILabel!
    @IBOutlet var gotItButton: UIButton!
    
    var player: AVPlayer?
    let authViewModel = AuthViewModel()
    let profileViewModel = ProfileViewModel()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loader.stopAnimating()
        keyboardRequired(topConstraint: topConstraint)
        emailTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        usernameTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        firstnameTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        lastnameTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        passwordTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        checkbox.image = UIImage(named: AppConstants.Images.CHECKBOX_BLACK_ICON)
        setText()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initBackgroundVideo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        if player != nil{
            player?.replaceCurrentItem(with: nil)
            player = nil
        }
    }
    
    func setText(){
        let text = "I agree with Terms of Service"
        let textUnderline = "Terms of Service"
        let rangeWhole = (text as NSString).range(of: text)
        let range = (text as NSString).range(of: textUnderline)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 12), range: rangeWhole)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.white, range: rangeWhole)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range)
        termsLabel.attributedText = attributedString
    }
    
    @IBAction func registerButtonAction(_ sender: UIButton) {
        let email:String = emailTextField.text ?? ""
        let username:String =  usernameTextField.text ?? ""
        let firstname:String = firstnameTextField.text ?? ""
        let lastname:String = lastnameTextField.text ?? ""
        let password:String = passwordTextField.text ?? ""
        
        var isError = false
        if !ValidationHelper.isValidEmail(email: email) {
            emailErrorIcon.isHidden = false
            isError = true
        } else {
            emailErrorIcon.isHidden = true
        }
        if !ValidationHelper.isEmptyAny(field: username) {
            usernameErrorIcon.isHidden = false
            isError = true
        } else {
            usernameErrorIcon.isHidden = true
        }
        if !ValidationHelper.isEmptyAny(field: firstname) {
            firstnameErrorIcon.isHidden = false
            isError = true
        } else {
            firstnameErrorIcon.isHidden = true
        }
        if !ValidationHelper.isEmptyAny(field: lastname) {
            lastnameErrorIcon.isHidden = false
            isError = true
        } else {
            lastnameErrorIcon.isHidden = true
        }
        
        if !ValidationHelper.isValidPassword(password: password) {
            passwordErrorIcon.isHidden = false
            isError = true
        } else {
            passwordErrorIcon.isHidden = true
        }
        
        hideToolTip()
        
        if isError {
            return
        }
        
        if checkbox.image == UIImage(named: AppConstants.Images.CHECKBOX_BLACK_ICON) {
            showErrorTooltip(errorMessage: AppConstants.Strings.ACCEPT_TERMS, view: checkbox)
            return
        }
        
        loader.startAnimating()
        sender.isEnabled = false
        
        let userAuth = UserAuth(firstname: firstname.trimmingCharacters(in: .whitespacesAndNewlines), lastname: lastname.trimmingCharacters(in: .whitespacesAndNewlines), username: username.trimmingCharacters(in: .whitespacesAndNewlines), email: email.trimmingCharacters(in: .whitespacesAndNewlines), password: password.trimmingCharacters(in: .whitespacesAndNewlines))
        
        authViewModel.register(request: userAuth, { (response) in
            if response.response?.statusCode == 200 || response.response?.statusCode == 201 {
                print("register success")
                self.authViewModel.login(request: userAuth, { (response) in
                    if let token = response.access_token {
                        self.setTemporaryToken(accessToken: token)
                        let refreshToken = response.refresh_token ?? ""
                        AppDefaults.setRefreshToken(token: refreshToken)
                        self.profileViewModel.getUser(accessToken: token, { (response) in
                            if response.response?.statusCode == 200 {
                                print("login success")
                                let data: ApiResponse<User> = Utils.convertResponseToData(data: response.data!)
                                if data.isError {
                                    let error = Utils.getErrorMessage(errors: data.errors)
                                    self.showMessageDialog(message: error)
                                } else {
                                    AppDefaults.setUsername(username: username)
                                    let user = data.data!
                                    AppDefaults.setUserId(userId: user.userId!)
                                    self.firstnameTextField.text = ""
                                    self.lastnameTextField.text = ""
                                    self.usernameTextField.text = ""
                                    self.emailTextField.text = ""
                                    self.passwordTextField.text = ""
                                    
                                    let eventType = UserAuth(eventType: AppConstants.TYPE_EVENT_REGISTER)
                                    self.trackUsers(token: token, data: eventType)
                                    
                                    AppDefaults.setOnBoardingStage(onboardingComplete: user.onboardingComplete!)
                                    AppDefaults.setLastOnBoardingScreen(lastOnBoardingScreen: user.lastOnboardingScreenCompleted ?? 0)
                                    AppDefaults.setRegistrationCompleted(registrationCompleted: true)
                                    self.loader.stopAnimating()
                                    sender.isEnabled = true
                                    self.navigationController?.popToRootViewController(animated: false)
                                }
                            }else{
                                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                                if data.isError {
                                    let error = Utils.getErrorMessage(errors: data.errors)
                                    self.showMessageDialog(message: error)
                                }
                                self.loader.stopAnimating()
                                sender.isEnabled = true
                            }
                        } , { (error) in
                            if error.statusCode == AppConstants.NO_INTERNET {
                                self.showInternetError()
                            }
                            self.loader.stopAnimating()
                            sender.isEnabled = true
                            
                        })
                    }
                    if let error = response.error_description {
                        self.showMessageDialog(message: error)
                    }
                    
                } , { (error) in
                    if error.statusCode == AppConstants.NO_INTERNET {
                        self.showInternetError()
                    }
                    self.loader.stopAnimating()
                    sender.isEnabled = true
                })
            } else {
                let data: ApiResponse<UserAuth> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.loader.stopAnimating()
                sender.isEnabled = true
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
                self.loader.stopAnimating()
                sender.isEnabled = true
            }
        })
        
    }
    
    func trackUsers(token: String, data: UserAuth) {
        let userAuth = data
        authViewModel.trackUsers(accessToken: token,data:userAuth,{ (response) in
            if response.response?.statusCode == 200 || response.response?.statusCode == 201{
                print("track users success")
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailTextField:
            usernameTextField.becomeFirstResponder()
        case usernameTextField:
            firstnameTextField.becomeFirstResponder()
        case firstnameTextField:
            lastnameTextField.becomeFirstResponder()
        case lastnameTextField:
            passwordTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }
    
    @IBAction func emailErrorButton(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.INVALID_EMAIL, view: sender)
    }
    
    @IBAction func usernameErrorButton(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.INVALID_USERNAME, view: sender)
    }
    
    @IBAction func firstnameErrorButton(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.INVALID_FIRST_NAME, view: sender)
    }
    
    @IBAction func lastnameErrorIcon(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.INVALID_LAST_NAME, view: sender)
    }
    
    @IBAction func passwordErrorIcon(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.INVALID_PASSWORD, view: sender)
    }
    
    
    @objc func textFieldDidChange(textField: UITextField){
        switch textField {
        case emailTextField:
            emailErrorIcon.isHidden = true
        case usernameTextField:
            usernameErrorIcon.isHidden = true
        case firstnameTextField:
            firstnameErrorIcon.isHidden = true
        case lastnameTextField:
            lastnameErrorIcon.isHidden = true
        default:
            passwordErrorIcon.isHidden = true
        }
        hideToolTip()
        
    }
    
    func initBackgroundVideo() {
        videoView.layer.zPosition = 0;
        let videoURL: NSURL = Bundle.main.url(forResource: AppConstants.VIDEO_NAME_TWO, withExtension: AppConstants.VIDEO_EXTENSION)! as NSURL
        
        player = AVPlayer(url: videoURL as URL)
        player?.isMuted = true
        
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.zPosition = -1
        
        playerLayer.frame = view.frame
        
        view.layer.addSublayer(playerLayer)
        
        player?.play()
        
        NotificationCenter.default.addObserver(self, selector: #selector(loopVideo), name: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem)
    }
    
    @objc func loopVideo() {
        player?.seek(to: CMTime.zero)
        player?.play()
    }
    
    @IBAction func checkBoxTap(_ sender: UITapGestureRecognizer) {
        if checkbox.image == UIImage(named: AppConstants.Images.CHECKBOX_BLACK_ICON) {
            
            checkbox.image = UIImage(named: AppConstants.Images.CHECKBOX_SELECTED_PINK_ICON)
        }
        else {
            checkbox.image = UIImage(named: AppConstants.Images.CHECKBOX_BLACK_ICON)
        }
    }
    
    @IBAction func termsTap(_ sender: UITapGestureRecognizer) {
        self.termsOfServiceScreen()
    }
    @IBAction func backPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
