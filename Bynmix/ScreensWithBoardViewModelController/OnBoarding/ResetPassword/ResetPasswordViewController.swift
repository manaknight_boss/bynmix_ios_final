import UIKit
import AVKit
import AVFoundation

class ResetPasswordViewController: BaseViewController {
    
    @IBOutlet var newPasswordTextField: UITextField!
    @IBOutlet var reEnterPasswordTextField: UITextField!
    @IBOutlet var videoView: UIView!
    @IBOutlet var buttonLoader: UIActivityIndicatorView!
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var resetButton: UIButton!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    var player: AVPlayer?
    var message : String?
    let forgotPasswordViewModel = ForgotPasswordViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        keyboardRequired(topConstraint: topConstraint)
        buttonLoader.stopAnimating()
        newPasswordTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        reEnterPasswordTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initBackgroundVideo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        if player != nil{
            player?.replaceCurrentItem(with: nil)
            player = nil
        }
    }
    
    func initBackgroundVideo() {
        videoView.layer.zPosition = 0;
        let videoURL: NSURL = Bundle.main.url(forResource: AppConstants.VIDEO_NAME_ONE, withExtension: AppConstants.VIDEO_EXTENSION)! as NSURL
        
        player = AVPlayer(url: videoURL as URL)
        player?.isMuted = true
        
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.zPosition = -1
        
        playerLayer.frame = view.frame
        
        view.layer.addSublayer(playerLayer)
        
        player?.play()
        
        NotificationCenter.default.addObserver(self, selector: #selector(loopVideo), name: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem)
    }
    
    @objc func loopVideo() {
        player?.seek(to: CMTime.zero)
        player?.play()
    }
    
    @IBAction func onBackButtonClick(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onResetButtonClick(_ sender: UIButton) {
        let password:String = newPasswordTextField.text!
        var isError = false
        if !ValidationHelper.isValidPassword(password: newPasswordTextField.text ?? "") && !ValidationHelper.isValidPassword(password: reEnterPasswordTextField.text ?? "") {
            errorLabel.text = AppConstants.Strings.INVALID_PASSWORD
            errorLabel.isHidden = false
            isError = true
        } else {
            errorLabel.isHidden = true
        }
        hideToolTip()
        if isError {
            return
        }
        if newPasswordTextField.text! == reEnterPasswordTextField.text!  {
            resetPassword(email: message!,password: password.trimmingCharacters(in: .whitespacesAndNewlines))
            errorLabel.isHidden = true
        } else {
            errorLabel.text = AppConstants.Strings.PASSWORDS_NOT_MATCH
            errorLabel.isHidden = false
        }
        
    }
    
    func resetPassword(email: String,password: String) {
        buttonLoader.startAnimating()
        resetButton.isEnabled = false
        let forgotPassword = ResetPasswordRequest(email: email,password: password)
        forgotPasswordViewModel.resetPassword(accessToken: getAccessToken(), data: forgotPassword,{(response) in
            self.buttonLoader.stopAnimating()
            self.resetButton.isEnabled = true
            if response.response?.statusCode == 200 {
                self.showMessageDialog(message: AppConstants.Strings.PASSWORD_RESET_SUCCESS)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
                    self.navigationController?.popToRootViewController(animated: true)
                }
                
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.buttonLoader.stopAnimating()
            self.resetButton.isEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            } else if error.statusCode == 401 {
                
            }
        })
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        switch textField {
        case newPasswordTextField:
            errorLabel.isHidden = true
        case reEnterPasswordTextField:
            errorLabel.isHidden = true
        default:
            errorLabel.isHidden = true
        }
        hideToolTip()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case newPasswordTextField:
            reEnterPasswordTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }
}
