import UIKit
import Kingfisher

class BloggersViewController: BaseViewController,LoadMoreProtocol {
    
    func onLoadMore() {
        if !isApiCalled {
            isApiCalled = true
            self.getBloggers()
        }
    }
    
    @IBOutlet var bloggersCollectionView: UICollectionView!
    @IBOutlet var searchUserTextfield: UITextField!
    @IBOutlet var bloggersButton: UIButton!
    @IBOutlet var buttonLoader: UIActivityIndicatorView!
    @IBOutlet var headerView: UIView!
    @IBOutlet var footerView: UIView!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var loader: UIActivityIndicatorView!
    
    var bloggers = [User]()
    let onboardingViewModel = OnBoardingViewModel()
    var onboardingBloggersHandler = OnboardingBloggersHandler()
    var currentPage = 1
    var isApiCalled = false
    var selectedBloggers = [User]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let layout = CollectionViewWaterfallLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 26, bottom: 100, right: 26)
        //layout.footerHeight = 50
        layout.minimumColumnSpacing = 20
        layout.minimumInteritemSpacing = 16
        bloggersCollectionView.collectionViewLayout = layout
        bloggersCollectionView.delegate = onboardingBloggersHandler
        bloggersCollectionView.dataSource = onboardingBloggersHandler
        buttonLoader.stopAnimating()
        searchUserTextfield.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        getBloggers()
        loader.stopAnimating()
        getItemClick()
        onboardingBloggersHandler.loadMoreDelegate = self
        hideKeyboardWhenTappedAround()
    }
    
    func getItemClick(){
        self.onboardingBloggersHandler.bloggerClick = { blogger in
            self.checkBloggers(blogger: blogger)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    private func getBloggers() {
        let searchText = searchUserTextfield.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        onboardingViewModel.getBloggers(accessToken: getTemporaryToken(),page: self.currentPage,searchQuery: searchText, {(response) in
            self.isApiCalled = false
            if response.response?.statusCode == 200 {
                let data: ApiResponse<User> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let data = data.data!
                    if (data.items?.count ?? 0) > 0{
                        self.loader.stopAnimating()
                        if var blogger = data.items{
                            
                            for i in 0..<blogger.count{
                                blogger[i].isUserFollowing = false
                            }
                            
                            if self.currentPage == 1 {
                                self.bloggers = blogger
                            } else {
                                self.bloggers.append(contentsOf: blogger)
                            }
                            if data.totalPages == self.currentPage {
                                self.isApiCalled = true
                                self.onboardingBloggersHandler.isLoadMoreRequired = false
                            }
                            self.onboardingBloggersHandler.bloggers = self.bloggers
                        }
                    }else{
                        self.loader.stopAnimating()
                        self.onboardingBloggersHandler.bloggers.removeAll()
                        self.onboardingBloggersHandler.isLoadMoreRequired = false
                    }
                    self.headerView.isHidden = false
                    self.bloggersCollectionView.isHidden = false
                    self.footerView.isHidden = false
                    self.screenActivityIndicator.stopAnimating()
                    self.bloggersCollectionView.reloadData()
                }
                self.currentPage += 1
            } else {
                self.loader.stopAnimating()
                self.screenActivityIndicator.stopAnimating()
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.loader.stopAnimating()
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func checkBloggers(blogger:User) {
        
        if blogger.isUserFollowing ?? false{
            self.selectedBloggers.append(blogger)
        }else{
            if self.selectedBloggers.count > 0 {
                for i in 0..<self.selectedBloggers.count{
                    if blogger.userId == self.selectedBloggers[i].userId{
                        self.selectedBloggers.remove(at: i)
                        break
                    }
                }
            }
        }
        if self.selectedBloggers.count > 0 {
            bloggersButton.setTitle(AppConstants.Strings.DONE, for: .normal)
        } else {
            bloggersButton.setTitle(AppConstants.Strings.SKIP_THIS_STEP, for: .normal)
        }
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reloadBloggers), object: nil)
        self.perform(#selector(self.reloadBloggers), with: nil, afterDelay: 0.6)
    }
    
    @objc func reloadBloggers() {
        self.currentPage = 1
        getBloggers()
        loader.startAnimating()
    }
    
    @IBAction func onBloggersButtonClick(_ sender: UIButton) {
        self.bloggersButton.isEnabled = false
        var selectedBloggers = [Int]()
        
        for i in 0..<self.selectedBloggers.count{
            selectedBloggers.append(self.selectedBloggers[i].userId ?? 0)
        }
        
        if selectedBloggers.count > 0 {
            buttonLoader.startAnimating()
            onboardingViewModel.saveBloggers(accessToken: getTemporaryToken(), data: BloggerRequest(bloggerIds: selectedBloggers), {(response) in
                if response.response?.statusCode == 200 || response.response?.statusCode == 201{
                    self.enableButtonAndStopLoader()
                    self.profileInfoScreen()
                }else {
                    self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
                    self.enableButtonAndStopLoader()
                }
            } , { (error) in
                self.enableButtonAndStopLoader()
                if error.statusCode == AppConstants.NO_INTERNET {
                    self.showInternetError()
                }
            })
        } else {
            self.bloggersButton.isEnabled = true
            self.profileInfoScreen()
        }
    }
    
    func enableButtonAndStopLoader(){
        self.buttonLoader.stopAnimating()
        self.bloggersButton.isEnabled = true
    }
}
