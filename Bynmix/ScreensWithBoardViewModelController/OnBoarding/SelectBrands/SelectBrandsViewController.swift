import UIKit

class SelectBrandsViewController: BaseViewController,LoadMoreProtocol {
    
    func onLoadMore() {
        if !isApiCalled {
            isApiCalled = true
            self.getBrands()
        }
    }
    
    @IBOutlet weak var brandsButton: UIButton!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var autoCompleteTextfield: UITextField!
    @IBOutlet weak var collectionViewLayout: UICollectionView!
    @IBOutlet weak var brandsWhyLabel: UILabel!
    @IBOutlet var fullScreenLoader: UIActivityIndicatorView!
    @IBOutlet var buttonLoader: UIActivityIndicatorView!
    @IBOutlet var selectedBrandsCollectionView: UICollectionView!
    
    var brands = [Brand]()
    let onboardingViewModel = OnBoardingViewModel()
    var currentPage = 1
    var isApiCalled = false
    var onboardingBrandsHandler = OnboardingBrandsHandler()
    var onboardingSelectedBrandsHandler = OnboardingSelectedBrandsHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewLayout.delegate = onboardingBrandsHandler
        collectionViewLayout.dataSource = onboardingBrandsHandler
        selectedBrandsCollectionView.delegate = onboardingSelectedBrandsHandler
        selectedBrandsCollectionView.dataSource = onboardingSelectedBrandsHandler
        collectionViewLayout.contentInset = UIEdgeInsets(top: 70, left: 0, bottom: 90, right: 0)
        autoCompleteTextfield.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        collectionViewLayout!.collectionViewLayout = FlowLayoutHelper()
        getBrands()
        fullScreenLoader.stopAnimating()
        loader.stopAnimating()
        buttonLoader.stopAnimating()
        hideKeyboardWhenTappedAround()
        brandsButton.backgroundColor = UIColor.cBDBDBD
        onboardingBrandsHandler.loadMoreDelegate = self
        setText()
        onBrandsClick()
    }
    
    func onBrandsClick(){
        self.onboardingBrandsHandler.brandsClick = { brand in
            if self.onboardingSelectedBrandsHandler.brands.count < 3{
                if self.onboardingSelectedBrandsHandler.brands.count > 0{
                    if self.onboardingSelectedBrandsHandler.brands.count == 1{
                        if self.onboardingSelectedBrandsHandler.brands[0].brandId != brand.brandId{
                            self.onboardingSelectedBrandsHandler.brands.append(brand)
                        }
                    }else if self.onboardingSelectedBrandsHandler.brands.count == 2{
                        if self.onboardingSelectedBrandsHandler.brands[0].brandId != brand.brandId && self.onboardingSelectedBrandsHandler.brands[1].brandId != brand.brandId{
                            self.onboardingSelectedBrandsHandler.brands.append(brand)
                        }
                    }
                }else{
                    self.onboardingSelectedBrandsHandler.brands.append(brand)
                }
            }else{
                self.showSnackBar(message: AppConstants.Strings.MAX_BRANDS_SELECTED)
            }
            self.checkBrands()
            self.selectedBrandsCollectionView.reloadData()
            self.scrollToLastItem()
        }
        self.onboardingSelectedBrandsHandler.brandsClick = { brand,index in
            self.onboardingSelectedBrandsHandler.brands.remove(at: index)
            self.selectedBrandsCollectionView.reloadData()
            self.checkBrands()
        }
    }
    
    func scrollToLastItem() {
        let lastSection = self.selectedBrandsCollectionView.numberOfSections - 1
        let lastRow = selectedBrandsCollectionView.numberOfItems(inSection: lastSection)
        let indexPath = IndexPath(row: lastRow - 1, section: lastSection)
        self.selectedBrandsCollectionView.scrollToItem(at: indexPath, at: .right, animated: true)
    }
    
    func setText(){
        let text:String = "Select your top 3 favorite brands. ( Why? )"
        let textRegular:String = "Select your top 3 favorite brands. ( Why? )"
        let textUnderline = "Why? )"
        let range = (text as NSString).range(of: textRegular)
        let rangeUnderline = (text as NSString).range(of: textUnderline)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontMedium(size: 16), range: range)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: rangeUnderline)
        brandsWhyLabel.attributedText = attributedString
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func checkBrands() {
        if onboardingSelectedBrandsHandler.brands.count == 3 {
            self.brandsButton.isEnabled = true
            self.brandsButton.backgroundColor = UIColor.cBB189C
        } else {
            self.brandsButton.isEnabled = false
            self.brandsButton.backgroundColor = UIColor.cBDBDBD
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reloadBrands), object: nil)
        self.perform(#selector(self.reloadBrands), with: nil, afterDelay: 0.6)
    }
    
    @objc func reloadBrands() {
        self.currentPage = 1
        getBrands()
        loader.startAnimating()
    }
    
    private func getBrands() {
        let searchText = autoCompleteTextfield.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        onboardingViewModel.getBrands(accessToken: getTemporaryToken(),page:self.currentPage ,searchQuery: searchText, {(response) in
            self.isApiCalled = false
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Brand> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let brandData = data.data!
                    if (brandData.items?.count ?? 0) > 0{
                        self.loader.stopAnimating()
                        if let brands = brandData.items {
                            
                            if self.currentPage == 1 {
                                self.brands = brands
                            } else {
                                self.brands.append(contentsOf: brands)
                            }
                            if brandData.totalPages == self.currentPage {
                                self.isApiCalled = true
                                self.onboardingBrandsHandler.isLoadMoreRequired = false
                            }
                        }
                        self.onboardingBrandsHandler.brands = self.brands
                    }else{
                        self.loader.stopAnimating()
                        self.onboardingBrandsHandler.brands.removeAll()
                    }
                    self.collectionViewLayout.reloadData()
                    
                }
                self.currentPage = self.currentPage + 1
            } else {
                self.loader.stopAnimating()
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.loader.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            } 
        })
    }
    
    @IBAction func brandsButton(_ sender: UIButton) {
        self.buttonLoader.startAnimating()
        self.brandsButton.isEnabled = false
        var selectedBrands = [Int]()
        
        for i in 0..<self.onboardingSelectedBrandsHandler.brands.count{
            selectedBrands.append(self.onboardingSelectedBrandsHandler.brands[i].brandId ?? 0)
        }
        
        onboardingViewModel.saveBrands(accessToken: getTemporaryToken(), data: BrandRequest(brandIds: selectedBrands), {(response) in
            if response.response?.statusCode == 200 || response.response?.statusCode == 201{
                self.updateBrandsButton()
                self.selectTagsScreen()
            }else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
                self.updateBrandsButton()
            }
        } , { (error) in
            self.updateBrandsButton()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func updateBrandsButton(){
        self.brandsButton.isEnabled = true
        self.buttonLoader.stopAnimating()
    }
    
    @IBAction func whyTap(_ sender: UITapGestureRecognizer) {
        self.showMessageDialog(message: AppConstants.Strings.BRANDS_WHY_MESSAGE)
    }
    
}
