import UIKit

class TagsViewController: BaseViewController,LoadMoreProtocol {
    
    func onLoadMore() {
        if !isApiCalled {
            isApiCalled = true
            self.getTags()
        }
    }
    
    @IBOutlet weak var tagsButton: UIButton!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var autoCompleteTextfield: UITextField!
    @IBOutlet weak var collectionViewLayout: UICollectionView!
    @IBOutlet var fullScreenLoader: UIActivityIndicatorView!
    @IBOutlet var buttonLoader: UIActivityIndicatorView!
    @IBOutlet var selectedBrandsCollectionView: UICollectionView!
    @IBOutlet var brandsLabel: UILabel!
    
    var tags = [Tags]()
    let onboardingViewModel = OnBoardingViewModel()
    var currentPage = 1
    var isApiCalled = false
    var onboardingTagsHandler = OnboardingTagsHandler()
    var onboardingSelectedTagsHandler = OnboardingSelectedTagsHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setText()
        collectionViewLayout.delegate = onboardingTagsHandler
        collectionViewLayout.dataSource = onboardingTagsHandler
        selectedBrandsCollectionView.delegate = onboardingSelectedTagsHandler
        selectedBrandsCollectionView.dataSource = onboardingSelectedTagsHandler
        collectionViewLayout.contentInset = UIEdgeInsets(top: 70, left: 0, bottom: 90, right: 0)
        autoCompleteTextfield.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        collectionViewLayout!.collectionViewLayout = FlowLayoutHelper()
        getTags()
        fullScreenLoader.stopAnimating()
        loader.stopAnimating()
        buttonLoader.stopAnimating()
        hideKeyboardWhenTappedAround()
        tagsButton.backgroundColor = UIColor.cBDBDBD
        onboardingTagsHandler.loadMoreDelegate = self
        onBrandsClick()
    }
    
    func setText(){
        let text:String = "How would you describe your style? (Select 3)"
        let textRegular:String = "How would you describe your style? (Select 3)"
        let textSmall = "(Select 3)"
        let range = (text as NSString).range(of: textRegular)
        let rangeSmall = (text as NSString).range(of: textSmall)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 16), range: range)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 13), range: rangeSmall)
        brandsLabel.attributedText = attributedString
    }
    
    func onBrandsClick(){
        self.onboardingTagsHandler.tagsClick = { tag in
            if self.onboardingSelectedTagsHandler.tags.count < 3{
                if self.onboardingSelectedTagsHandler.tags.count > 0{
                    if self.onboardingSelectedTagsHandler.tags.count == 1{
                        if self.onboardingSelectedTagsHandler.tags[0].descriptorId != tag.descriptorId{
                            self.onboardingSelectedTagsHandler.tags.append(tag)
                        }
                    }else if self.onboardingSelectedTagsHandler.tags.count == 2{
                        if self.onboardingSelectedTagsHandler.tags[0].descriptorId != tag.descriptorId && self.onboardingSelectedTagsHandler.tags[1].descriptorId != tag.descriptorId{
                            self.onboardingSelectedTagsHandler.tags.append(tag)
                        }
                    }
                }else{
                    self.onboardingSelectedTagsHandler.tags.append(tag)
                }
            }else{
                self.showSnackBar(message: AppConstants.Strings.MAX_TAGS_SELECTED)
            }
            self.checkTags()
            self.selectedBrandsCollectionView.reloadData()
            self.scrollToLastItem()
        }
        self.onboardingSelectedTagsHandler.tagsClick = { tags,index in
            self.onboardingSelectedTagsHandler.tags.remove(at: index)
            self.selectedBrandsCollectionView.reloadData()
            self.checkTags()
        }
    }
    
    func scrollToLastItem() {
        let lastSection = self.selectedBrandsCollectionView.numberOfSections - 1
        let lastRow = selectedBrandsCollectionView.numberOfItems(inSection: lastSection)
        let indexPath = IndexPath(row: lastRow - 1, section: lastSection)
        self.selectedBrandsCollectionView.scrollToItem(at: indexPath, at: .right, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reloadBrands), object: nil)
        self.perform(#selector(self.reloadBrands), with: nil, afterDelay: 0.6)
    }
    
    @objc func reloadBrands() {
        self.currentPage = 1
        getTags()
        loader.startAnimating()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    func checkTags() {
        if self.onboardingSelectedTagsHandler.tags.count == 3 {
            tagsButton.isEnabled = true
            tagsButton.backgroundColor = UIColor.cBB189C
        } else {
            tagsButton.isEnabled = false
            tagsButton.backgroundColor = UIColor.cBDBDBD
        }
    }
    
    private func getTags() {
        let searchText = autoCompleteTextfield.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        onboardingViewModel.getTags(accessToken: getTemporaryToken(),page:self.currentPage ,searchQuery: searchText, {(response) in
            self.isApiCalled = false
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Tags> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let tagsData = data.data!
                    if (tagsData.items?.count ?? 0) > 0{
                        self.loader.stopAnimating()
                        if let tags = tagsData.items {
                            
                            if self.currentPage == 1 {
                                self.tags = tags
                            } else {
                                self.tags.append(contentsOf: tags)
                            }
                            if tagsData.totalPages == self.currentPage {
                                self.isApiCalled = true
                                self.onboardingTagsHandler.isLoadMoreRequired = false
                            }
                        }
                        self.onboardingTagsHandler.tags = self.tags
                    }else{
                        self.loader.stopAnimating()
                        self.onboardingTagsHandler.tags.removeAll()
                    }
                    self.collectionViewLayout.reloadData()
                    
                }
                self.currentPage = self.currentPage + 1
            } else {
                self.loader.stopAnimating()
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.loader.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    @IBAction func tagsButton(_ sender: UIButton) {
        self.buttonLoader.startAnimating()
        self.tagsButton.isEnabled = false
        var selectedtags = [Int]()
        
        for i in 0..<self.onboardingSelectedTagsHandler.tags.count{
            selectedtags.append(self.onboardingSelectedTagsHandler.tags[i].descriptorId ?? 0)
        }
        
        onboardingViewModel.saveTags(accessToken: getTemporaryToken(), data: TagRequest(descriptorIds: selectedtags), {(response) in
            if response.response?.statusCode == 200 || response.response?.statusCode == 201 {
                self.updateTagsButton()
                self.bloggersScreen()
            }else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
                self.updateTagsButton()
            }
        } , { (error) in
            self.updateTagsButton()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func updateTagsButton(){
        self.buttonLoader.stopAnimating()
        self.tagsButton.isEnabled = true
    }
    
    
}
