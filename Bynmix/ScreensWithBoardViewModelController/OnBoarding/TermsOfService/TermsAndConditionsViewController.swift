import UIKit
import Alamofire

class TermsAndConditionsViewController: BaseViewController,UITableViewDataSource, UITableViewDelegate {
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var loader: UIActivityIndicatorView!

    var terms = [Terms]()
    
    let termsCell:String = "terms_cell"
    let appInfoViewModel = AppInfoViewModel()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.indicatorStyle = UIScrollView.IndicatorStyle.white
        
        appInfoViewModel.getTerms({ (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<AppInfo> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let appInfo = data.data
                    if let terms = appInfo?.sections {
                        self.terms = terms
                        self.loader.isHidden = true
                        self.tableView.isHidden = false
                        self.tableView.reloadData()
                    }
                }
            } else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return terms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: termsCell, for: indexPath)
            as! TermsAndConditionsTableViewCell
        let data = self.terms[indexPath.row]
        let seperator: String = ". "
        
        cell.titleLabel?.text = "\(data.order ??  1)" + seperator + data.title!
        if let description = data.subSections {
            cell.descriptionTwoLabel.isHidden = false
            cell.pointsLabel.isHidden = false
            let subData = description[0]
            if let innerDescription = subData.description {
                cell.descriptionLabel?.text = innerDescription
            }
            var message = ""
            if let points = subData.points {
                var i = 0
                for point in points {
                    if i == 0 {
                        message = message + "\n"
                    } else {
                        message = message + "\n\n"
                    }
                    message = message + "- " + point
                    i = i + 1
                }
            }
            cell.pointsLabel?.text = message
            let subData2 = description[1]
            if let innerDescription = subData2.description {
                cell.descriptionTwoLabel?.text = innerDescription
            }
        } else {
            if let desc = data.description {
                cell.descriptionLabel?.text = desc
            }
            cell.descriptionTwoLabel.text = ""
            cell.pointsLabel.text = ""
            cell.descriptionTwoLabel.isHidden = true
            cell.pointsLabel.isHidden = true
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
}
