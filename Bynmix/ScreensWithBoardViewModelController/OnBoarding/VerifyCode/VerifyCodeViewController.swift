import UIKit
import AVKit
import AVFoundation
import Toast_Swift
class VerifyCodeViewController: BaseViewController {
    
    @IBOutlet var videoView: UIView!
    @IBOutlet var buttonLoader: UIActivityIndicatorView!
    @IBOutlet var emailTextField: UITextField!
    @IBOutlet var emailErrorIcon: UIButton!
    @IBOutlet var otpTextFieldOne: UITextField!
    @IBOutlet var otpTextFieldTwo: UITextField!
    @IBOutlet var otpTextFieldThree: UITextField!
    @IBOutlet var otpTextFieldFour: UITextField!
    @IBOutlet var otpTextFieldFive: UITextField!
    @IBOutlet var otpTextFieldSix: UITextField!
    @IBOutlet var nextButton: UIButton!
    @IBOutlet var resentEmailLabel: UILabel!
    @IBOutlet var verificationCodeLabel: UILabel!
    
    var player: AVPlayer?
    var message : String?
    let forgotPasswordViewModel = ForgotPasswordViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonLoader.stopAnimating()
        emailTextField.text = message
        
        emailTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        otpTextFieldOne.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        otpTextFieldTwo.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        otpTextFieldThree.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        otpTextFieldFour.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        otpTextFieldFive.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        otpTextFieldSix.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        
        if emailTextField.text == message {
            resentEmailLabel.isHidden = false
            verificationCodeLabel.text = AppConstants.Strings.VERIFY_MSG
            emailTextField.isUserInteractionEnabled = false
        } else {
            resentEmailLabel.isHidden = true
            verificationCodeLabel.text = AppConstants.Strings.VERIFY_MSG_WITH_EMAIL
            emailTextField.isUserInteractionEnabled = true
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        initBackgroundVideo()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
        if player != nil{
            player?.replaceCurrentItem(with: nil)
            player = nil
        }
    }
    
    func initBackgroundVideo() {
        videoView.layer.zPosition = 0;
        let videoURL: NSURL = Bundle.main.url(forResource: AppConstants.VIDEO_NAME_ONE, withExtension: AppConstants.VIDEO_EXTENSION)! as NSURL
        
        player = AVPlayer(url: videoURL as URL)
        player?.isMuted = true
        
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.zPosition = -1
        
        playerLayer.frame = view.frame
        
        view.layer.addSublayer(playerLayer)
        
        player?.play()
        
        NotificationCenter.default.addObserver(self, selector: #selector(loopVideo), name: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem)
    }
    
    
    @objc func loopVideo() {
        player?.seek(to: CMTime.zero)
        player?.play()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailTextField:
            otpTextFieldOne.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }
    
    @objc func textFieldDidChange(textField: UITextField){
        switch textField {
        case emailTextField:
            emailErrorIcon.isHidden = true
        default:
            emailErrorIcon.isHidden = true
        }
        hideToolTip()
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == emailTextField {
            return true
        }
        
        switch textField {
        case otpTextFieldOne:
            if string != "" {
                otpTextFieldTwo.becomeFirstResponder()
            }
            otpTextFieldOne.text = string
        case otpTextFieldTwo:
            if string != "" {
                otpTextFieldThree.becomeFirstResponder()
            } else {
                otpTextFieldOne.becomeFirstResponder()
            }
            otpTextFieldTwo.text = string
        case otpTextFieldThree:
            if string != "" {
                otpTextFieldFour.becomeFirstResponder()
            } else {
                otpTextFieldTwo.becomeFirstResponder()
            }
            
            otpTextFieldThree.text = string
        case otpTextFieldFour:
            if string != "" {
                otpTextFieldFive.becomeFirstResponder()
            } else {
                otpTextFieldThree.becomeFirstResponder()
            }
            
            otpTextFieldFour.text = string
        case otpTextFieldFive:
            if string != "" {
                otpTextFieldSix.becomeFirstResponder()
            } else {
                otpTextFieldFour.becomeFirstResponder()
            }
            otpTextFieldFive.text = string
        case otpTextFieldSix:
            if string != "" {
                otpTextFieldSix.resignFirstResponder()
            } else {
                otpTextFieldFive.becomeFirstResponder()
            }
            
            otpTextFieldSix.text = string
        default:
            break
        }
        
        
        return false
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onNextButtonPressed(_ sender: UIButton) {
        let email:String = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        
        var isError = false
        if !ValidationHelper.isValidEmail(email: email) {
            emailErrorIcon.isHidden = false
            isError = true
        } else {
            emailErrorIcon.isHidden = true
        }
        hideToolTip()
        if isError {
            return
        }
        
        var codeString: String? = otpTextFieldOne.text! + otpTextFieldTwo.text!
        
        codeString = codeString! + otpTextFieldThree.text! + otpTextFieldFour.text!
        
        codeString = codeString! + otpTextFieldFive.text! + otpTextFieldSix.text!
        
        let code: Int? = Int(codeString!)
        
        if codeString!.count < 5 {
            self.view.makeToast(AppConstants.Strings.INCORRECT_OTP)
        } else {
            
            self.verifyCode(email: email.trimmingCharacters(in: .whitespacesAndNewlines), code: code!)
        }
        
    }
    
    @IBAction func onResendEmailPressed(_ sender: UITapGestureRecognizer) {
        sendCode(email: emailTextField.text!)
    }
    
    func sendCode(email: String) {
        self.view.makeToastActivity(.center)
        nextButton.isEnabled = false
        let forgotPassword = ForgotPasswordRequest(email: email)
        forgotPasswordViewModel.sendCode(accessToken: getAccessToken(), data: forgotPassword,{(response) in
            self.view.hideToastActivity()
            self.nextButton.isEnabled = true
            if response.response?.statusCode == 201 {
                self.showMessageDialog(message: AppConstants.Strings.SUCCESS_MESSAGE)
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            self.nextButton.isEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            } 
        })
    }
    
    func verifyCode(email: String,code: Int) {
        buttonLoader.startAnimating()
        nextButton.isEnabled = false
        let forgotPassword = VerifyCodeRequest(email: email,code: code)
        forgotPasswordViewModel.verifyCode(accessToken: getTemporaryToken(), data: forgotPassword,{(response) in
            self.buttonLoader.stopAnimating()
            self.nextButton.isEnabled = true
            if response.response?.statusCode == 200 {
                self.ResetPasswordScreen(message: email)
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.buttonLoader.stopAnimating()
            self.nextButton.isEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func ResetPasswordScreen(message: String) {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.RESET_PASSWORD, bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.RESET_PASSWORD) as! ResetPasswordViewController
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        myAlert.message = message
        navigationController?.pushViewController(myAlert, animated: true)
    }
    
    
    @IBAction func emailErrorIconPressed(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.INVALID_EMAIL, view: sender)
    }
    
    
}



