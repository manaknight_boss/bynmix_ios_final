import UIKit

class WelcomeScreenViewController: BaseViewController {
    
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var welcomeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        welcomeLabel.font = UIFont.fontMontserratHairline(size: 30)
        backgroundImageView.contentMode = .scaleToFill
    }
    
    @IBAction func onButtonClick(_ sender: UIButton) {
        discoverScreen()
    }
    
    func discoverScreen(){
        self.presentViewControllerForOnBoarding(storyboardName: AppConstants.Storyboard.DISCOVER_STORYBOARD, viewControllerIdentifier: AppConstants.Controllers.DISCOVER_SCREEN)
    }

}
