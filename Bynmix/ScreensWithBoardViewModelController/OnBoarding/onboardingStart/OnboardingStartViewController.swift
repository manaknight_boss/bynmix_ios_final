import UIKit

class OnboardingStartViewController: BaseViewController {
    
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var muchMoreLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        muchMoreLabel.font = UIFont.fontMontserratHairline(size: 30)
        backgroundImageView.contentMode = .scaleToFill
    }
    
    @IBAction func gotItButtonAction(_ sender: UIButton){
        selectBrandsScreen()
    }
    
}
