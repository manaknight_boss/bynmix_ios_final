import UIKit
import XLPagerTabStrip

class AboutMeViewController: BaseViewController {
    
    @IBOutlet var cityTextField: UITextField!
    @IBOutlet var stateLabel: UILabel!
    @IBOutlet var feetLabel: UILabel!
    @IBOutlet var inchesLabel: UILabel!
    @IBOutlet var bodyTypeLabel: UILabel!
    @IBOutlet var genderLabel: UILabel!
    @IBOutlet var buttonLoader: UIActivityIndicatorView!
    @IBOutlet var saveButton: UIButton!
    
    var user:User? = nil
    
    var genderSelect = [String]()
    var feetSelect = [String]()
    var inchesSelect = [String]()
    var bodyTypeSelect = [BodyType]()
    var statesSelect = [states]()
    var selectedBodyType = 0
    var selectedStates = 0
    
    let TYPE_GENDER : Int = 1
    let TYPE_FEET : Int = 2
    let TYPE_INCH : Int = 3
    let TYPE_BODY : Int = 4
    let TYPE_STATES: Int = 5
    
    let profileViewModel = ProfileViewModel()
    let editProfileViewModel = EditProfileViewModel()
    var shortTitle:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUser()
        genderSelect = AppConstants.StaticLists.gender
        feetSelect = AppConstants.StaticLists.feet
        inchesSelect = AppConstants.StaticLists.inches
        self.view.makeToastActivity(.center)
        getBodyType()
        getState()
        shortTitle = user?.title
        self.view.hideToastActivity()
        buttonLoader.stopAnimating()
    }
    
    func setUser() {
        cityTextField.text = user?.city
        
        if user?.state == nil {
            stateLabel.text = AppConstants.Strings.SELECT_STATE
        } else {
            stateLabel.text = user?.state
            stateLabel.textColor = UIColor.black
        }
        if user?.heightFeet == 0 || user?.heightFeet == nil{
            feetLabel.text = AppConstants.Strings.CHOOSE_FEET
        }else {
            feetLabel.text = String(user?.heightFeet ?? 0)
            feetLabel.textColor = UIColor.black
        }
        if user?.heightInches == 0 || user?.heightInches == nil{
            inchesLabel.text = AppConstants.Strings.CHOOSE_INCHES
        }else {
            inchesLabel.text = String(user?.heightInches ?? 0)
            inchesLabel.textColor = UIColor.black
        }

        if user?.gender == 2 {
            genderLabel.text = AppConstants.MALE
            genderLabel.textColor = UIColor.black
        } else if user?.gender == 1 {
            genderLabel.text = AppConstants.FEMALE
            genderLabel.textColor = UIColor.black
        } else {
            genderLabel.text = AppConstants.Strings.SELECT_GENDER
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case cityTextField:
            cityTextField.resignFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }

    @IBAction func onStateViewClick(_ sender: UITapGestureRecognizer) {
        showDropDown(tag: TYPE_STATES, title:  AppConstants.Strings.SELECT_STATE, dataSourse: self, delegate: self, {(row) in
            self.stateLabel.textColor = UIColor.black
            self.stateLabel.text = self.statesSelect[row].name
            self.selectedStates = self.statesSelect[row].stateId!
        })
    }
    
    @IBAction func onFeetViewClick(_ sender: UITapGestureRecognizer) {
        showDropDown(tag: TYPE_FEET, title: AppConstants.Strings.CHOOSE_FEET, dataSourse: self, delegate: self, {(row) in
            self.feetLabel.textColor = UIColor.black
            self.feetLabel.text = self.feetSelect[row]
        })
    }
    
    @IBAction func onInchesViewClick(_ sender: UITapGestureRecognizer) {
        showDropDown(tag: TYPE_INCH, title: AppConstants.Strings.CHOOSE_INCHES, dataSourse: self, delegate: self, {(row) in
            self.inchesLabel.textColor = UIColor.black
            self.inchesLabel.text = self.inchesSelect[row]
        })
    }
    
    @IBAction func onBodyTypeViewClick(_ sender: UITapGestureRecognizer) {
        showDropDown(tag: TYPE_BODY, title:  AppConstants.Strings.CHOOSE_BODY_TYPE, dataSourse: self, delegate: self, {(row) in
            self.bodyTypeLabel.textColor = UIColor.black
            self.bodyTypeLabel.text = self.bodyTypeSelect[row].bodyTypeName
            self.selectedBodyType = self.bodyTypeSelect[row].bodyTypeId!
        })
    }
    
    @IBAction func onGenderViewClick(_ sender: UITapGestureRecognizer) {
        showDropDown(tag: TYPE_GENDER, title: AppConstants.Strings.CHOOSE_GENDER, dataSourse: self, delegate: self, {(row) in
            self.genderLabel.textColor = UIColor.black
            self.genderLabel.text = self.genderSelect[row]
        })
    }
    
    
    @IBAction func onSaveButtonClick(_ sender: UIButton) {
        let city:String = cityTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        let state = stateLabel.text == AppConstants.Strings.SELECT_STATE ? "" : stateLabel.text ?? ""
        let feet = feetLabel.text == AppConstants.Strings.CHOOSE_FEET ? 0 : Int(feetLabel.text ?? "")
        let inch = inchesLabel.text == AppConstants.Strings.CHOOSE_INCHES ? 0 : Int(inchesLabel.text ?? "")
        var gender = 0
        if genderLabel.text == AppConstants.MALE {
            gender = 2
        } else if genderLabel.text == AppConstants.FEMALE {
            gender = 1
        }
        
        updateProfileInfo(city: city,state: state,heightFeet: feet!,heightInches: inch!,bodyType: selectedBodyType,Gender: gender)
    }
  
    func updateProfileInfo(city: String,state: String,heightFeet: Int,heightInches: Int,bodyType: Int,Gender:Int) {
        buttonLoader.startAnimating()
        saveButton.isEnabled = false
        user?.city = city
        user?.state = state
        user?.heightFeet = heightFeet
        user?.heightInches = heightInches
        user?.bodyTypeId = bodyType
        user?.gender = Gender
        user?.shortTitle = self.shortTitle
        profileViewModel.updateProfileInfo(accessToken: getAccessToken(), data: user!,{(response) in
            self.buttonLoader.stopAnimating()
            self.saveButton.isEnabled = true
            if response.response?.statusCode == 200 {
                self.navigationController?.popViewController(animated: false)
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.buttonLoader.stopAnimating()
            self.saveButton.isEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
}

extension AboutMeViewController: IndicatorInfoProvider,UIPickerViewDelegate, UIPickerViewDataSource {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: AppConstants.Strings.ABOUT_ME)
    }
   
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == TYPE_GENDER {
            return genderSelect.count
        } else if pickerView.tag == TYPE_FEET {
            return feetSelect.count
        } else if pickerView.tag == TYPE_INCH {
            return inchesSelect.count
        } else if pickerView.tag == TYPE_BODY {
            return bodyTypeSelect.count
        } else {
            return statesSelect.count
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView.tag == TYPE_GENDER {
            return genderSelect[row]
        } else if pickerView.tag == TYPE_FEET {
            return feetSelect[row]
        } else if pickerView.tag == TYPE_INCH {
            return inchesSelect[row]
        } else if pickerView.tag == TYPE_BODY {
            return bodyTypeSelect[row].bodyTypeName
        }else {
            return statesSelect[row].name
        }
    }
    
    private func getBodyType() {
        editProfileViewModel.getBodyType(accessToken: getTemporaryToken(), {(response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[BodyType]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.bodyTypeSelect = data.data!
                    for bodyType in self.bodyTypeSelect {
                        if self.user?.bodyTypeId == bodyType.bodyTypeId {
                            self.bodyTypeLabel.text = bodyType.bodyTypeName
                            self.bodyTypeLabel.textColor = UIColor.black
                            break
                        }
                        
                    }
                }
            } else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    private func getState() {
        editProfileViewModel.getStates(accessToken: getTemporaryToken(), {(response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[states]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.statesSelect = data.data!
                }
            } else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
}


