import UIKit

class AccountSettingsViewController: BaseViewController {
    
    var user:User? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.Strings.ACCOUNT_SETTING_TITLE
    
    }
    
    func openEditProfileScreen() {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.EDIT_PROFILE_BOARD, bundle: nil)
        let editProfileViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.EDIT_PROFILE) as! EditProfileViewController
        //editProfileViewController.user = user
        navigationController?.pushViewController(editProfileViewController, animated: true)
    }
    
    func openBrandsScreen() {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.EDIT_BRANDS_STORYBOARD, bundle: nil)
        let editBrandsViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.EDIT_BRANDS_SCREEN) as! EditBrandsViewController
        navigationController?.pushViewController(editBrandsViewController, animated: false)
    }
    
    func openTagsScreen() {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.EDIT_TAGS_STORYBOARD, bundle: nil)
        let editTagsViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.EDIT_TAGS_SCREEN) as! EditTagsViewController
        navigationController?.pushViewController(editTagsViewController, animated: false)
    }
    
    @IBAction func editProfileTapAction(_ sender: UITapGestureRecognizer) {
        openEditProfileScreen()
    }
    
    @IBAction func editBrandsTapAction(_ sender: UITapGestureRecognizer) {
        openBrandsScreen()
    }
    
    @IBAction func editStylesTapAction(_ sender: UITapGestureRecognizer) {
        openTagsScreen()
    }
    
}
