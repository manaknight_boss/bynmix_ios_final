import UIKit

class AddNewAddressViewController: BaseViewController,SuggestedAddressProtocol {
    
    func suggestedAddress(address: Address,fullName:String,type: Int) {
        let name = fullName
        let addressTextField = address.addressOne!
        let addressOptionalTextField = address.addressTwo!
        let city = address.city!
        var newType = type
        let zipcode:String = address.zipCode!
        if self.TYPE == AppConstants.TYPE_SELECT_DIFFERENT_ADDRESS{
            newType = AppConstants.TYPE_SELECT_DIFFERENT_ADDRESS
        }
        let stateAbbr = address.stateAbbreviation
        for i in 0..<self.statesSelect.count{
            if stateAbbr == self.statesSelect[i].abbreviation{
                self.selectedStates = self.statesSelect[i].stateId ?? 0
                break
            }
        }
        
        addAddress(name: name, addressTextField: addressTextField, city: city, state: "", zipcode: zipcode, addressOptionalTextField: addressOptionalTextField, needsVerification: false, type: newType)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var addressTextField: UITextField!
    @IBOutlet var addressOptionalTextField: UITextField!
    @IBOutlet var cityTextField: UITextField!
    @IBOutlet var stateLabel: UILabel!
    @IBOutlet var zipcodeTextField: UITextField!
    @IBOutlet var buttonActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var defaultShippingButton: UIButton!
    @IBOutlet var defaultReturnAddress: UIButton!
    @IBOutlet var nameErrorButton: UIButton!
    @IBOutlet var addressErrorButton: UIButton!
    @IBOutlet var cityErrorButton: UIButton!
    @IBOutlet var zipcodeErrorButton: UIButton!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var scrollView:UIScrollView!
    @IBOutlet var addAddressButton: UIButton!
    
    let editProfileViewModel = EditProfileViewModel()
    let profileViewModel = ProfileViewModel()
    var addNewAddressDelegate:AddNewAddressProtocol?
    var addNewAddressInPaymentDelegate:AddNewAddressInPaymentProtocol?
    
    var statesSelect = [states]()
    var selectedStates = 0
    var isShippingAddress:Bool = false
    var isReturnAddress:Bool = false
    var TYPE:Int = 0
    var addNewAddressInSellerSetUpDelegate:AddNewAddressInSellerSetUpProtocol?
    var TYPE_TITLE:Int?
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    var addAddressBuyNowDelegate:AddAddressBuyNowProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.Strings.ADD_NEW_ADDRESS
        buttonActivityIndicator.isHidden = true
        nameTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        addressTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        cityTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        zipcodeTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        getState()
        hideKeyboardWhenTappedAround()
        defaultShippingButton.setImage(UIImage(named: AppConstants.Images.CHECKBOX), for: .normal)
        defaultReturnAddress.setImage(UIImage(named: AppConstants.Images.CHECKBOX), for: .normal)
        
        if TYPE == AppConstants.TYPE_SHIPPING_ADD_ADDRESS{
            defaultShippingButton.setImage(UIImage(named: AppConstants.Images.CHECKBOX_SELECTED), for: .normal)
            self.isShippingAddress = true
        } else if TYPE == AppConstants.TYPE_RETURN_ADD_ADDRESS{
            defaultReturnAddress.setImage(UIImage(named: AppConstants.Images.CHECKBOX_SELECTED), for: .normal)
            self.isReturnAddress = true
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIWindow.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIWindow.keyboardWillHideNotification, object: nil)
        keyboardRequired(topConstraint: topConstraint)
    }
    
  @objc func keyboardWillShow(notification:NSNotification){
    let userInfo = notification.userInfo!
    var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
      keyboardFrame = self.view.convert(keyboardFrame, from: nil)

      var contentInset:UIEdgeInsets = self.scrollView.contentInset
      contentInset.bottom = keyboardFrame.size.height
      scrollView.contentInset = contentInset
  }

  @objc func keyboardWillHide(notification:NSNotification){
      let contentInset:UIEdgeInsets = UIEdgeInsets.zero
      scrollView.contentInset = contentInset
  }
    
    @objc private func textFieldDidChange(textField: UITextField){
        switch textField {
        case nameTextField:
            nameErrorButton.isHidden = true
        case addressTextField:
            addressErrorButton.isHidden = true
        case cityTextField:
            cityErrorButton.isHidden = true
        case zipcodeTextField:
            zipcodeErrorButton.isHidden = true
        default:
            textField.isHidden = true
        }
        hideToolTip()
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case nameTextField:
            addressTextField.becomeFirstResponder()
        case addressTextField:
            addressOptionalTextField.becomeFirstResponder()
        case addressOptionalTextField:
            cityTextField.becomeFirstResponder()
        case cityTextField:
            zipcodeTextField.resignFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }
    
    @IBAction func onStateTapAction(_ sender: UITapGestureRecognizer) {
        showDropDown(tag: TYPE, title:  AppConstants.Strings.SELECT_STATE, dataSourse: self, delegate: self, {(row) in
            self.stateLabel.textColor = UIColor.black
            self.stateLabel.text = self.statesSelect[row].name
            self.selectedStates = self.statesSelect[row].stateId!
        })
    }
    
    @IBAction func defaultShippingAddressButtonAction(_ sender: UIButton) {
        if defaultShippingButton.currentImage == UIImage(named: AppConstants.Images.CHECKBOX){
           defaultShippingButton.setImage(UIImage(named: AppConstants.Images.CHECKBOX_SELECTED), for: .normal)
            self.isShippingAddress = true
        }else{
             self.isShippingAddress = false
            defaultShippingButton.setImage(UIImage(named: AppConstants.Images.CHECKBOX), for: .normal)
        }
    }
    
    @IBAction func defaultReturnAddressButtonAction(_ sender: UIButton) {
        if defaultReturnAddress.currentImage == UIImage(named: AppConstants.Images.CHECKBOX_SELECTED){
            defaultReturnAddress.setImage(UIImage(named: AppConstants.Images.CHECKBOX), for: .normal)
            self.isReturnAddress = false
        }else{
            defaultReturnAddress.setImage(UIImage(named: AppConstants.Images.CHECKBOX_SELECTED), for: .normal)
            self.isReturnAddress = true
        }
        
    }
    
    @IBAction func addAddressButtonAction(_ sender: UIButton) {
        let name:String = self.nameTextField.text ?? "".trimmingCharacters(in: .whitespacesAndNewlines)
        let addressTextField:String = self.addressTextField.text ?? "".trimmingCharacters(in: .whitespacesAndNewlines)
        let city:String = self.cityTextField.text ?? "".trimmingCharacters(in: .whitespacesAndNewlines)
        let state:String = self.stateLabel.text ?? "".trimmingCharacters(in: .whitespacesAndNewlines)
        let zipcode:String = self.zipcodeTextField.text ?? "".trimmingCharacters(in: .whitespacesAndNewlines)
        let addressOptionalTextField:String = self.addressOptionalTextField.text ?? "".trimmingCharacters(in: .whitespacesAndNewlines)
        
        var isError = false
        if !ValidationHelper.isEmptyAny(field: name) {
            nameErrorButton.isHidden = false
            isError = true
        } else {
            nameErrorButton.isHidden = true
        }
        if !ValidationHelper.isEmptyAny(field: addressTextField) {
            addressErrorButton.isHidden = false
            isError = true
        } else {
            addressErrorButton.isHidden = true
        }
        if !ValidationHelper.isEmptyAny(field: city) {
            cityErrorButton.isHidden = false
            isError = true
        } else {
            cityErrorButton.isHidden = true
        }
        if state == AppConstants.Strings.SELECT_A_STATE {
            self.view.makeToast(AppConstants.Strings.PLEASE_SELECT_STATE)
        } else {
        }
        if !ValidationHelper.isEmptyAny(field: zipcode) {
            zipcodeErrorButton.isHidden = false
            isError = true
        } else {
            zipcodeErrorButton.isHidden = true
        }
        
        hideToolTip()
        if isError {
            return
        }
        self.buttonActivityIndicator.isHidden = false
        self.buttonActivityIndicator.startAnimating()
        self.buttonActivityIndicator.hidesWhenStopped = true
        sender.isEnabled = false
        addAddress(name: name, addressTextField: addressTextField, city: city, state: state, zipcode: zipcode, addressOptionalTextField: addressOptionalTextField, needsVerification: true, type: self.TYPE)
    }
    
    func addAddress(name:String,addressTextField:String,city:String,state:String,zipcode:String,addressOptionalTextField:String,needsVerification:Bool,type:Int) {
        
        var address = Address(fullName: name, addressOne: addressTextField, addressTwo: addressOptionalTextField, city: city, state: statesSelect[selectedStates-1].name!, zipCode: zipcode, country: AppConstants.USA, isShippingAddress: self.isShippingAddress, isReturnAddress: self.isReturnAddress,needsVerification: needsVerification)
        profileViewModel.addAddresses(accessToken: getAccessToken(), data: address,{(response) in
            self.buttonActivityIndicator.stopAnimating()
            self.addAddressButton.isEnabled = true
            address.city = city + " " + (self.statesSelect[self.selectedStates-1].abbreviation ?? "")
            
            if response.response?.statusCode == 201 {
                let data: ApiResponse<Int> = Utils.convertResponseToData(data: response.data!)
                address.addressId = data.data
                if type == AppConstants.TYPE_SUGGESTED_ADDRESS{
                   self.navigationController?.popViewController(animated: false)
                }else{
                    self.addNewAddressInSellerSetUpDelegate?.AddNewAddressInSellerSetUp(address: address)
                    self.addNewAddressDelegate?.addNewAddress(type: AppConstants.TYPE_ADD_NEW_ADDRESS)
                    self.addNewAddressInPaymentDelegate?.AddNewAddressInPayment(type: AppConstants.TYPE_ADD_NEW_ADDRESS_IN_PAYMENT, address: address)
                    self.navigationController?.popViewController(animated: false)
                }
               
            }else if response.response?.statusCode == 409{
                let data: ApiResponse<SuggestedAddress> = Utils.convertResponseToData(data: response.data!)
                
                let addressData = data.data
                let originalAddress = addressData?.originalAddress!
                let suggestedAddress = addressData?.suggestedAddress!
                self.openSuggestedAddress(suggestedAddress: suggestedAddress!,originalAddress: originalAddress!, fullName: name)
                
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.buttonActivityIndicator.stopAnimating()
            self.addAddressButton.isEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func openSuggestedAddress(suggestedAddress:Address,originalAddress:Address,fullName:String){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.SUGGESTED_ADDRESS_DIALOG_BOARD, bundle: nil)
        let suggestedAddressViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.SUGGESTED_ADDRESS_DIALOG) as! SuggestedAddressDialogViewController
        suggestedAddressViewController.suggestedAddress = suggestedAddress
        suggestedAddressViewController.originalAddress = originalAddress
        suggestedAddressViewController.fullName = fullName
        suggestedAddressViewController.suggestedAddressDelegate = self
        suggestedAddressViewController.modalPresentationStyle = .overFullScreen
        navigationController?.present(suggestedAddressViewController, animated: false,completion: nil)
    }
    
    @IBAction func nameErorrButtonAction(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_FULL_NAME, view: sender)
    }
    
    @IBAction func addressErrorButtonAction(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_ADDRESS, view: sender)
    }
    
    @IBAction func cityErrorButtonAction(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_CITY, view: sender)
    }
    
    @IBAction func zipcodeErrorButtonAction(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_ZIPCODE, view: sender)
    }
    
}

extension AddNewAddressViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return statesSelect.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return statesSelect[row].name
    }
    
    private func getState() {
        editProfileViewModel.getStates(accessToken: getAccessToken(), {(response) in
            self.screenActivityIndicator.stopAnimating()
            self.scrollView.isHidden = false
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[states]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.statesSelect = data.data!
                }
            } else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            self.scrollView.isHidden = false
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            } 
        })
    }
    
}
