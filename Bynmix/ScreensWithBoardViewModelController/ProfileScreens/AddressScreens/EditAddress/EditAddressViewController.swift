import UIKit

class EditAddressViewController: BaseViewController,UpdateSuggestedAddressProtocol {
    
    func updateSuggestedAddress(address: Address, fullName: String, type: Int, addressId: Int) {
        let name = fullName
        let addressTextField = address.addressOne ?? ""
        let addressOptionalTextField = address.addressTwo ?? ""
        let city = address.city ?? ""
        let state = address.state ?? ""
        let zipcode:String = address.zipCode ?? ""
        print("updateAddressCurrent",addressId,name,addressTextField,addressOptionalTextField,city,state,zipcode)
        updateAddress(id: addressId, name: name, addressTextField: addressTextField, city: city, state: state, zipcode: zipcode, addressOptionalTextField: addressOptionalTextField, needsVerification: false, type: type)
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var addressTextField: UITextField!
    @IBOutlet var addressOptionalTextField: UITextField!
    @IBOutlet var cityTextField: UITextField!
    @IBOutlet var stateLabel: UILabel!
    @IBOutlet var zipcodeTextField: UITextField!
    @IBOutlet var buttonActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var nameErrorButton: UIButton!
    @IBOutlet var addressErrorButton: UIButton!
    @IBOutlet var cityErrorButton: UIButton!
    @IBOutlet var zipcodeErrorButton: UIButton!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var scrollView:UIScrollView!
    @IBOutlet var addAddressButton: UIButton!
    @IBOutlet var defaultShippingView: UIView!
    @IBOutlet var defaultReturnView: UIView!
    @IBOutlet var defaultShippingButton: UIButton!
    @IBOutlet var defaultReturnButton: UIButton!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    let editProfileViewModel = EditProfileViewModel()
    let profileViewModel = ProfileViewModel()
    
    let TYPE_STATES: Int = 1
    var statesSelect = [states]()
    var selectedStates = 0
    var isShippingAddress:Bool = false
    var isReturnAddress:Bool = false
    var address:Address?
    var updateAddressDelegate:UpdateAddressProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.Strings.EDIT_ADDRESS
        buttonActivityIndicator.isHidden = true
        nameTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        addressTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        cityTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        zipcodeTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        getState()
        hideKeyboardWhenTappedAround()
        nameTextField.text = address?.fullName
        addressTextField.text = address?.addressOne
        addressOptionalTextField.text = address?.addressTwo
        cityTextField.text = address?.city
        stateLabel.text = address?.state
        stateLabel.textColor = UIColor.black
        zipcodeTextField.text = address?.zipCode
        
        if (address?.isShippingAddress!)!{
            self.defaultShippingView.isHidden = true
            isShippingAddress = true
        }else{
            self.defaultShippingView.isHidden = false
            isShippingAddress = false
        }
        if (address?.isReturnAddress!)!{
            self.defaultReturnView.isHidden = true
            isReturnAddress = true
        }else{
            self.defaultReturnView.isHidden = false
            isReturnAddress = false
        }
        
        defaultShippingButton.setImage(UIImage(named: AppConstants.Images.CHECKBOX), for: .normal)
        defaultReturnButton.setImage(UIImage(named: AppConstants.Images.CHECKBOX), for: .normal)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIWindow.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIWindow.keyboardWillHideNotification, object: nil)
        keyboardRequired(topConstraint: topConstraint)
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
       let userInfo = notification.userInfo!
       var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
         keyboardFrame = self.view.convert(keyboardFrame, from: nil)

         var contentInset:UIEdgeInsets = self.scrollView.contentInset
         contentInset.bottom = keyboardFrame.size.height
         scrollView.contentInset = contentInset
     }

     @objc func keyboardWillHide(notification:NSNotification){
         let contentInset:UIEdgeInsets = UIEdgeInsets.zero
         scrollView.contentInset = contentInset
     }

    @objc private func textFieldDidChange(textField: UITextField){
        switch textField {
        case nameTextField:
            nameErrorButton.isHidden = true
        case addressTextField:
            addressErrorButton.isHidden = true
        case cityTextField:
            cityErrorButton.isHidden = true
        case zipcodeTextField:
            zipcodeErrorButton.isHidden = true
        default:
            textField.isHidden = true
        }
        hideToolTip()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case nameTextField:
            addressTextField.becomeFirstResponder()
        case addressTextField:
            addressOptionalTextField.becomeFirstResponder()
        case addressOptionalTextField:
            cityTextField.becomeFirstResponder()
        case cityTextField:
            zipcodeTextField.resignFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }
    
    @IBAction func onStateTapAction(_ sender: UITapGestureRecognizer) {
        showDropDown(tag: TYPE_STATES, title:  AppConstants.Strings.SELECT_STATE, dataSourse: self, delegate: self, {(row) in
            self.stateLabel.textColor = UIColor.black
            self.stateLabel.text = self.statesSelect[row].name
            self.selectedStates = self.statesSelect[row].stateId!
        })
    }
    
    @IBAction func updateAddressButtonAction(_ sender: UIButton) {
        let name:String = self.nameTextField.text ?? "".trimmingCharacters(in: .whitespacesAndNewlines)
        let addressTextField:String = self.addressTextField.text ?? "".trimmingCharacters(in: .whitespacesAndNewlines)
        let city:String = self.cityTextField.text ?? "".trimmingCharacters(in: .whitespacesAndNewlines)
        let state:String = self.stateLabel.text ?? "".trimmingCharacters(in: .whitespacesAndNewlines)
        let zipcode:String = self.zipcodeTextField.text ?? "".trimmingCharacters(in: .whitespacesAndNewlines)
        let addressOptionalTextField:String = self.addressOptionalTextField.text ?? "".trimmingCharacters(in: .whitespacesAndNewlines)
        
        var isError = false
        if !ValidationHelper.isEmptyAny(field: name) {
            nameErrorButton.isHidden = false
            isError = true
        } else {
            nameErrorButton.isHidden = true
        }
        if !ValidationHelper.isEmptyAny(field: addressTextField) {
            addressErrorButton.isHidden = false
            isError = true
        } else {
            addressErrorButton.isHidden = true
        }
        if !ValidationHelper.isEmptyAny(field: city) {
            cityErrorButton.isHidden = false
            isError = true
        } else {
            cityErrorButton.isHidden = true
        }
        if state == AppConstants.Strings.SELECT_A_STATE {
            self.view.makeToast(AppConstants.Strings.PLEASE_SELECT_STATE)
        } else {
        }
        if !ValidationHelper.isEmptyAny(field: zipcode) {
            zipcodeErrorButton.isHidden = false
            isError = true
        } else {
            zipcodeErrorButton.isHidden = true
        }
        
        hideToolTip()
        if isError {
            return
        }
        self.buttonActivityIndicator.isHidden = false
        self.buttonActivityIndicator.startAnimating()
        self.buttonActivityIndicator.hidesWhenStopped = true
        sender.isEnabled = false
        updateAddress(id: (address?.addressId!)!, name: name, addressTextField: addressTextField, city: city, state: state, zipcode: zipcode, addressOptionalTextField: addressOptionalTextField, needsVerification: true, type: 0)
    }
    
    func updateAddress(id:Int,name:String,addressTextField:String,city:String,state:String,zipcode:String,addressOptionalTextField:String,needsVerification:Bool,type:Int) {
        let state:String = self.stateLabel.text ?? "".trimmingCharacters(in: .whitespacesAndNewlines)
        let address = Address(fullName: name, addressOne: addressTextField, addressTwo: addressOptionalTextField, city: city, state: state, zipCode: zipcode, country: AppConstants.USA, isShippingAddress: self.isShippingAddress, isReturnAddress: self.isReturnAddress,needsVerification: needsVerification)
        profileViewModel.updateAddresses(accessToken: getAccessToken(), id: id, data: address,{(response) in
            self.buttonActivityIndicator.stopAnimating()
            self.addAddressButton.isEnabled = true
            if response.response?.statusCode == 200 {
            
                if type == AppConstants.TYPE_SUGGESTED_ADDRESS{
                   self.navigationController?.popViewController(animated: false)
                }else{
                    self.navigationController?.popViewController(animated: false)
                    self.updateAddressDelegate?.updateAddress(type: AppConstants.TYPE_UPDATE_ADDRESS, isDefault: true,address: address)
                }
            }else if response.response?.statusCode == 409{
                let data: ApiResponse<SuggestedAddress> = Utils.convertResponseToData(data: response.data!)
                
                let addressData = data.data
                let originalAddress = addressData?.originalAddress!
                let suggestedAddress = addressData?.suggestedAddress!
                self.openSuggestedAddress(suggestedAddress: suggestedAddress!,originalAddress: originalAddress!, fullName: name, addressId: id)
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.buttonActivityIndicator.stopAnimating()
            self.addAddressButton.isEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func openSuggestedAddress(suggestedAddress:Address,originalAddress:Address,fullName:String,addressId:Int){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.SUGGESTED_ADDRESS_DIALOG_BOARD, bundle: nil)
        let suggestedAddressViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.SUGGESTED_ADDRESS_DIALOG) as! SuggestedAddressDialogViewController
        suggestedAddressViewController.suggestedAddress = suggestedAddress
        suggestedAddressViewController.originalAddress = originalAddress
        suggestedAddressViewController.fullName = fullName
        suggestedAddressViewController.addressId = addressId
        suggestedAddressViewController.updateSuggestedAddress = self
        suggestedAddressViewController.modalPresentationStyle = .overFullScreen
        navigationController?.present(suggestedAddressViewController, animated: false,completion: nil)
    }
    
    @IBAction func nameErorrButtonAction(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_FULL_NAME, view: sender)
    }
    
    @IBAction func addressErrorButtonAction(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_ADDRESS, view: sender)
    }
    
    @IBAction func cityErrorButtonAction(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_CITY, view: sender)
    }
    
    @IBAction func zipcodeErrorButtonAction(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_ZIPCODE, view: sender)
    }
    
    @IBAction func defaultShippingAddressButtonAction(_ sender: UIButton) {
        if defaultShippingButton.currentImage == UIImage(named: AppConstants.Images.CHECKBOX){
            defaultShippingButton.setImage(UIImage(named: AppConstants.Images.CHECKBOX_SELECTED), for: .normal)
            self.isShippingAddress = true
        }else{
            self.isShippingAddress = false
            defaultShippingButton.setImage(UIImage(named: AppConstants.Images.CHECKBOX), for: .normal)
        }
    }
    
    @IBAction func defaultReturnAddressButtonAction(_ sender: UIButton) {
        if defaultReturnButton.currentImage == UIImage(named: AppConstants.Images.CHECKBOX_SELECTED){
            defaultReturnButton.setImage(UIImage(named: AppConstants.Images.CHECKBOX), for: .normal)
            self.isReturnAddress = false
        }else{
            defaultReturnButton.setImage(UIImage(named: AppConstants.Images.CHECKBOX_SELECTED), for: .normal)
            self.isReturnAddress = true
        }
    }

}

extension EditAddressViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return statesSelect.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return statesSelect[row].name
    }
    
    private func getState() {
        editProfileViewModel.getStates(accessToken: getTemporaryToken(), {(response) in
            self.screenActivityIndicator.stopAnimating()
            self.scrollView.isHidden = false
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[states]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.statesSelect = data.data!
                }
            } else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            self.scrollView.isHidden = false
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
}
