import UIKit

class AddressesViewController: BaseViewController,DeleteAddressClickProtocol,ExistingAddressProtocol,AddNewAddressProtocol,UpdateAddressProtocol, SelectExistingAddressProtocol,AddNewAddressInPaymentProtocol{
    
    func AddNewAddressInPayment(type: Int, address: Address) {
        if type == AppConstants.TYPE_ADD_NEW_ADDRESS_IN_PAYMENT{
            selectExistingAddressDelegate?.SelectExistingAddress(type: AppConstants.TYPE_ADD_NEW_ADDRESS_IN_PAYMENT, address: address)
            addAddressBuyNowDelegate?.addAddressBuyNow(address: address)
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    func SelectExistingAddress(type: Int, address: Address) {
        if type == AppConstants.TYPE_SELECT_EXISTING_ADDRESS{
            var newAddress: Address = address
            if self.addressType == AppConstants.TYPE_RETURN_ADD_ADDRESS {
                isReturnAddress = true
                newAddress.isReturnAddress = true
            } else {
                isdefaultAddress = true
                newAddress.isShippingAddress = true
            }
            updateAddress(id:newAddress.addressId!,address:newAddress)
        }
    }
    
    func updateAddress(type: Int,isDefault:Bool,address:Address) {
        if type == AppConstants.TYPE_UPDATE_ADDRESS{

        }
    }
    
    func addNewAddress(type: Int) {
        if type == AppConstants.TYPE_ADD_NEW_ADDRESS{
        
        }
    }
    
    func existingAddressClick(type: Int) {
        if type == AppConstants.TYPE_EXISTING_ADDRESS{
            print("test")

        }
    }
    
    func DeleteAddressClick(type: Int,address:Address) {
        if type == AppConstants.TYPE_DELETE_ADDRESS{
            deleteAddress(id:(address.addressId!))
            self.addressesTableView.reloadData()
        }
    }

    @IBOutlet var addressesTableView: UITableView!
    var profileViewModel = ProfileViewModel()
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var deleteItemView: UIView!
    let addressHandler = AddressHandler()
    var isInserted:Bool = false
    var allAddresses: [Address] = []
    var address: Address?
    var isdefaultAddress = false
    var isReturnAddress = false
    var addressType: Int?
    var addAddressBuyNowDelegate:AddAddressBuyNowProtocol?
    
    var selectExistingAddressDelegate:SelectExistingAddressProtocol?
    
    @IBOutlet weak var addButtonLayout: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        addressesTableView.delegate = addressHandler
        addressesTableView.dataSource = addressHandler
        navigationItem.title = AppConstants.Strings.ADDRESS_TITLE
        addressHandler.onEditButtonClick = { address in
            let storyboard = UIStoryboard(name: AppConstants.Storyboard.EDIT_ADDRESS_BOARD, bundle: nil)
            let  editAddressViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.EDIT_ADDRESS_SCREEN) as! EditAddressViewController
            editAddressViewController.address = address
            editAddressViewController.updateAddressDelegate = self
            self.navigationController?.pushViewController(editAddressViewController, animated: true)
            
        }
        addressHandler.onDeleteButtonClick = { address in
            let storyboard = UIStoryboard(name: AppConstants.Storyboard.DELETE_ADDRESS_DIALOG_BOARD, bundle: nil)
            let  deleteAddressViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.DELETE_ADDRESS_DIALOG) as! DeleteAddressDialogViewController
            deleteAddressViewController.address = address
            deleteAddressViewController.deleteAddressDelegate = self
            self.navigationController?.present(deleteAddressViewController, animated: true,completion: nil)
        }
        addressHandler.onShippingAddNewAddressButtonClick = {
            let storyboard = UIStoryboard(name: AppConstants.Storyboard.ADD_NEW_ADDRESS_BOARD, bundle: nil)
            let  addAddressViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.ADD_NEW_ADDRESS_SCREEN) as! AddNewAddressViewController
            addAddressViewController.addNewAddressDelegate = self
            addAddressViewController.TYPE = AppConstants.TYPE_SHIPPING_ADD_ADDRESS
            self.navigationController?.pushViewController(addAddressViewController, animated: true)
        }
        addressHandler.onReturnAddNewAddressButtonClick = {
            let storyboard = UIStoryboard(name: AppConstants.Storyboard.ADD_NEW_ADDRESS_BOARD, bundle: nil)
            let  addAddressViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.ADD_NEW_ADDRESS_SCREEN) as! AddNewAddressViewController
            addAddressViewController.addNewAddressDelegate = self
            addAddressViewController.TYPE = AppConstants.TYPE_RETURN_ADD_ADDRESS
            self.navigationController?.pushViewController(addAddressViewController, animated: true)
        }
        addressHandler.onSelectExistingAddressButtonClick = { type in
            self.addressType = type
            let storyboard = UIStoryboard(name: AppConstants.Storyboard.SELECT_EXISTING_ADDRESS_BOARD, bundle: nil)
            let  existingAddressViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.SELECT_EXISTING_ADDRESS_SCREEN) as! SelectExistingAddressViewController
            existingAddressViewController.selectExistingAddressDelegate = self
            self.navigationController?.pushViewController(existingAddressViewController, animated: true)
        }
        addressesTableView.tableHeaderView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 0.0, height: Double(Float.leastNormalMagnitude)))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if allAddresses.count > 0 {
            self.view.makeToastActivity(.center)
        }
        getAddresses()
    }
    
    func deleteAddress(id:Int) {
        self.deleteItemView.isHidden = false
        profileViewModel.deleteAddresses(accessToken: getAccessToken(), id: id,{(response) in
            self.deleteItemView.isHidden = true
            if response.response?.statusCode == 200 {
                self.addressHandler.deleteStatus(id: id)
                self.addressesTableView.reloadData()
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    
                }
            }
        } , { (error) in
            self.deleteItemView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func updateAddress(id:Int,address:Address) {
        profileViewModel.updateAddresses(accessToken: getAccessToken(), id: id, data: address,{(response) in
            if self.allAddresses.count > 0 {
                self.view.makeToastActivity(.center)
            }
            
            if response.response?.statusCode == 200 {
                
                self.getAddresses()
            }else{
                self.view.hideToastActivity()
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getAddresses() {
        profileViewModel.getAddresses(accessToken: AppDefaults.getAccessToken(), { (response) in
            self.view.hideToastActivity()
            self.screenActivityIndicator.stopAnimating()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[Address]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    var returnAddress: Address? = nil
                    var shippingAddress: Address? = nil
                    let addresses = data.data! as [Address]
                    self.allAddresses = addresses
                    self.addressHandler.allAddresses = addresses
                    if self.allAddresses.count > 0 {
                        self.addButtonLayout.isHidden = false
                    }
                    var otherAddresses: [Address] = []
                    for i in 0..<addresses.count {
                        var isDefault : Bool = false
                        if addresses[i].isShippingAddress! {
                            shippingAddress = addresses[i]
                            isDefault = true
                        }
                        
                        if addresses[i].isReturnAddress! {
                            returnAddress = addresses[i]
                            isDefault = true
                        }
                        
                        if !isDefault {
                            otherAddresses.append(addresses[i])
                        }
                    }
                    
                    self.addressHandler.shippingAddress = shippingAddress
                    self.addressHandler.returnAddress = returnAddress
                    self.addressHandler.otherAddresses = otherAddresses
                    self.addressesTableView.reloadData()
                    self.addressesTableView.isHidden = false
                 
                }
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    @IBAction func addNewAddressButtonAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.ADD_NEW_ADDRESS_BOARD, bundle: nil)
        let  addAddressViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.ADD_NEW_ADDRESS_SCREEN) as! AddNewAddressViewController
        addAddressViewController.addNewAddressInPaymentDelegate = self
        navigationController?.pushViewController(addAddressViewController, animated: true)
    }

}
