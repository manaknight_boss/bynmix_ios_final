import UIKit
import XLPagerTabStrip

class BasicInfoViewController: BaseViewController {
    
    @IBOutlet var usernameTextField: UITextField!
    @IBOutlet var firstNameTextField: UITextField!
    @IBOutlet var lastNameTextField: UITextField!
    @IBOutlet var shortTitleTextField: UITextField!
    @IBOutlet var shortDescTextView: UITextView!
    @IBOutlet var buttonLoader: UIActivityIndicatorView!
    @IBOutlet var usernameErrorIcon: UIButton!
    @IBOutlet var firstNameErrorIcon: UIButton!
    @IBOutlet var lastNameErrorIcon: UIButton!
    @IBOutlet var saveButton: UIButton!
    @IBOutlet var topConstraint: NSLayoutConstraint!
    
    var user:User? = nil
    let profileViewModel = ProfileViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonLoader.stopAnimating()
        keyboardRequired(topConstraint: topConstraint)
        hideKeyboardWhenTappedAround()
        usernameTextField.text = user?.username
        firstNameTextField.text = user?.firstname
        lastNameTextField.text = user?.lastname
        shortTitleTextField.text = user?.title
        
        if user?.shortBio == nil {
            shortDescTextView.text = AppConstants.Strings.SHORT_DESCRIPTION
            shortDescTextView.textColor = UIColor.cDCDCDC
        } else {
            shortDescTextView.text = user?.shortBio
            shortDescTextView.textColor = UIColor.black
        }
     
        usernameTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        firstNameTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        lastNameTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
       
    }
    
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if shortDescTextView.text == AppConstants.Strings.SHORT_DESCRIPTION {
            shortDescTextView.text = nil
            shortDescTextView.textColor = UIColor.black
            shortDescTextView.font = UIFont.fontRegular(size: 18)
        }
        UIView.animate(withDuration: 0.2) {
            self.topConstraint.constant = -50
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if shortDescTextView.text.isEmpty {
            shortDescTextView.text = AppConstants.Strings.SHORT_DESCRIPTION
            shortDescTextView.textColor = UIColor.cCDCDCD
            shortDescTextView.font = UIFont.fontRegular(size: 18)
        }
        UIView.animate(withDuration: 0.2) {
            self.topConstraint.constant = 55
        }
    }
    
    @objc private func textFieldDidChange(textField: UITextField){
        switch textField {
        case usernameTextField:
            usernameErrorIcon.isHidden = true
        case firstNameTextField:
            firstNameErrorIcon.isHidden = true
        case lastNameTextField:
            lastNameErrorIcon.isHidden = true
        default:
            textField.isHidden = true
        }
        hideToolTip()
        
    }
    
//     func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField == self.usernameTextField{
//            
//        }else if textField == self.firstNameTextField{
//            
//        }else if textField == self.lastNameTextField{
//            
//        }else if textField == self.shortTitleTextField{
//            
//        }
//    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case usernameTextField:
            firstNameTextField.becomeFirstResponder()
        case firstNameTextField:
            lastNameTextField.becomeFirstResponder()
        case lastNameTextField:
            shortTitleTextField.becomeFirstResponder()
        case shortTitleTextField:
            shortDescTextView.becomeFirstResponder()
        case shortDescTextView:
            shortDescTextView.resignFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }
    
    @IBAction func onUsernameErrorButtonClick(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.PLEASE_ENTER_USERNAME, view: sender)
    }
    
    @IBAction func onFirstNameErrorButtonClick(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.PLEASE_ENTER_FIRST_NAME, view: sender)
    }
    
    @IBAction func onLastNameErrorButtonClick(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.PLEASE_ENTER_LAST_NAME, view: sender)
    }
    
    @IBAction func onSaveButtonClick(_ sender: UIButton) {
        let username:String = usernameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        let firstname:String = firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        let lastname:String = lastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        let shortTitle:String = shortTitleTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        let shortDesc:String = shortDescTextView.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        
        var isError = false
        if !ValidationHelper.isEmptyAny(field: username) {
            usernameErrorIcon.isHidden = false
            isError = true
        } else {
            usernameErrorIcon.isHidden = true
        }
        if !ValidationHelper.isEmptyAny(field: firstname) {
            firstNameErrorIcon.isHidden = false
            isError = true
        } else {
            firstNameErrorIcon.isHidden = true
        }
        if !ValidationHelper.isEmptyAny(field: lastname) {
            lastNameErrorIcon.isHidden = false
            isError = true
        } else {
            lastNameErrorIcon.isHidden = true
        }
        
        hideToolTip()
        if isError {
            return
        }
        buttonLoader.startAnimating()
        sender.isEnabled = false
        
        updateProfileInfo(username: username,firstName: firstname,lastName: lastname,shortTitle: shortTitle,shortDesc: shortDesc)
        
    }
    
    func updateProfileInfo(username: String,firstName: String,lastName: String,shortTitle: String,shortDesc: String) {
        buttonLoader.startAnimating()
        saveButton.isEnabled = false
        user?.username = username
        user?.firstname = firstName
        user?.lastname = lastName
        user?.shortTitle = shortTitle
        if shortDesc == AppConstants.Strings.SHORT_DESCRIPTION {
            user?.shortBio = ""
        } else {
            user?.shortBio = shortDesc
        }
        profileViewModel.updateProfileInfo(accessToken: getAccessToken(), data: user!,{(response) in
            self.buttonLoader.stopAnimating()
            self.saveButton.isEnabled = true
            if response.response?.statusCode == 200 {
                self.navigationController?.popViewController(animated: false)
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.buttonLoader.stopAnimating()
            self.saveButton.isEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            } 
        })
    }
 
    
}

extension BasicInfoViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: AppConstants.Strings.BASIC_INFO)
    }
}
