import UIKit

class EditBrandsViewController: BaseViewController,LoadMoreProtocol {
    
    func onLoadMore() {
        if !isApiCalled {
            isApiCalled = true
            self.getBrands()
        }
    }
    
    @IBOutlet weak var brandsButton: UIButton!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var autoCompleteTextfield: UITextField!
    @IBOutlet weak var collectionViewLayout: UICollectionView!
    @IBOutlet var fullScreenLoader: UIActivityIndicatorView!
    @IBOutlet var buttonLoader: UIActivityIndicatorView!
    @IBOutlet var selectedBrandsCollectionView: UICollectionView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var footerView: UIView!
    
    var brands = [Brand]()
    let onboardingViewModel = OnBoardingViewModel()
    let profileViewModel = ProfileViewModel()
    var currentPage = 1
    var isApiCalled = false
    var onboardingBrandsHandler = OnboardingBrandsHandler()
    var onboardingSelectedBrandsHandler = OnboardingSelectedBrandsHandler()
    var user:User? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "My Favorite Brands"
        collectionViewLayout.delegate = onboardingBrandsHandler
        collectionViewLayout.dataSource = onboardingBrandsHandler
        selectedBrandsCollectionView.delegate = onboardingSelectedBrandsHandler
        selectedBrandsCollectionView.dataSource = onboardingSelectedBrandsHandler
        collectionViewLayout.contentInset = UIEdgeInsets(top: 56, left: 22, bottom: 65, right: 22)
        autoCompleteTextfield.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        collectionViewLayout!.collectionViewLayout = FlowLayoutHelper()
        loader.stopAnimating()
        buttonLoader.stopAnimating()
        hideKeyboardWhenTappedAround()
        brandsButton.backgroundColor = UIColor.cBB189C
        brandsButton.isEnabled = true
        onboardingBrandsHandler.loadMoreDelegate = self
        onBrandsClick()
        getSelectedBrands()
    }
    
    func onBrandsClick(){
        self.onboardingBrandsHandler.brandsClick = { brand in
            if self.onboardingSelectedBrandsHandler.brands.count > 0{
                for i in 0..<self.onboardingSelectedBrandsHandler.brands.count{
                    if self.onboardingSelectedBrandsHandler.brands[i].brandId == brand.brandId{
                        return
                    }
                }
                self.onboardingSelectedBrandsHandler.brands.append(brand)
            }else{
                self.onboardingSelectedBrandsHandler.brands.append(brand)
            }
            //self.checkBrands()
            self.selectedBrandsCollectionView.reloadData()
            self.scrollToLastItem()
        }
        self.onboardingSelectedBrandsHandler.brandsClick = { brand,index in
            self.onboardingSelectedBrandsHandler.brands.remove(at: index)
            self.selectedBrandsCollectionView.reloadData()
            //self.checkBrands()
        }
    }
    
    func scrollToLastItem() {
        let lastSection = self.selectedBrandsCollectionView.numberOfSections - 1
        let lastRow = selectedBrandsCollectionView.numberOfItems(inSection: lastSection)
        let indexPath = IndexPath(row: lastRow - 1, section: lastSection)
        self.selectedBrandsCollectionView.scrollToItem(at: indexPath, at: .right, animated: true)
    }
    
    func checkBrands() {
        if onboardingSelectedBrandsHandler.brands.count == 3 {
            self.brandsButton.isEnabled = true
            self.brandsButton.backgroundColor = UIColor.cBB189C
        } else {
            self.brandsButton.isEnabled = false
            self.brandsButton.backgroundColor = UIColor.cBDBDBD
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reloadBrands), object: nil)
        self.perform(#selector(self.reloadBrands), with: nil, afterDelay: 0.6)
    }
    
    @objc func reloadBrands() {
        self.currentPage = 1
        getBrands()
        loader.startAnimating()
    }
    
    private func getBrands() {
        let searchText = autoCompleteTextfield.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        onboardingViewModel.getBrands(accessToken: getAccessToken(),page:self.currentPage ,searchQuery: searchText, {(response) in
            self.isApiCalled = false
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Brand> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let brandData = data.data!
                    if (brandData.items?.count ?? 0) > 0{
                        self.loader.stopAnimating()
                        if let brands = brandData.items {
                            
                            if self.currentPage == 1 {
                                self.brands = brands
                            } else {
                                self.brands.append(contentsOf: brands)
                            }
                            if brandData.totalPages == self.currentPage {
                                self.isApiCalled = true
                                self.onboardingBrandsHandler.isLoadMoreRequired = false
                            }
                        }
                        self.onboardingBrandsHandler.brands = self.brands
                    }else{
                        self.loader.stopAnimating()
                        self.onboardingBrandsHandler.brands.removeAll()
                        self.onboardingBrandsHandler.isLoadMoreRequired = false
                    }
                    self.collectionViewLayout.reloadData()
                    self.getUser()
                    
                }
                self.currentPage = self.currentPage + 1
            } else {
                self.loader.stopAnimating()
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.loader.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func showScreenAndFooter(){
        self.fullScreenLoader.stopAnimating()
        self.mainView.isHidden = false
        self.footerView.isHidden = false
    }
    
    private func getSelectedBrands() {
        profileViewModel.getSelectedBrands(accessToken: getAccessToken(), {(response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[Brand]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let brands = data.data!
                    self.setSelectedBrands(brands: brands)
                    
                }
            } else {
                self.fullScreenLoader.stopAnimating()
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.fullScreenLoader.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getUser() {
        profileViewModel.getUser(accessToken: getAccessToken(), { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<User> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    BaseViewController().showMessageDialog(message: error)
                } else {
                    let userData = data.data!
                    self.user = userData
                    self.showScreenAndFooter()
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                BaseViewController().showInternetError()
            }
        })
    }
    
    func setSelectedBrands(brands:[Brand]){
        self.onboardingSelectedBrandsHandler.brands = brands
        selectedBrandsCollectionView.reloadData()
        
        getBrands()
    }
    
    @IBAction func brandsButton(_ sender: UIButton) {
        self.buttonLoader.startAnimating()
        self.brandsButton.isEnabled = false
        var selectedBrands = [String]()
        
        for i in 0..<self.onboardingSelectedBrandsHandler.brands.count{
            selectedBrands.append(String(self.onboardingSelectedBrandsHandler.brands[i].brandId ?? 0))
        }
        
        var users:User = self.user!
        users.shortTitle = users.title
        users.userBrands = selectedBrands
        
        profileViewModel.saveEditedBrands(accessToken: getAccessToken(), data: users, {(response) in
            if response.response?.statusCode == 200 || response.response?.statusCode == 201{
                self.updateBrandsButton()
                self.navigationController?.popViewController(animated: false)
            }else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
                self.updateBrandsButton()
            }
        } , { (error) in
            self.updateBrandsButton()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func updateBrandsButton(){
        self.brandsButton.isEnabled = true
        self.buttonLoader.stopAnimating()
    }
    
}
