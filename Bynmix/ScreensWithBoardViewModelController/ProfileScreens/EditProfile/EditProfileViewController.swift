import UIKit
import XLPagerTabStrip

class EditProfileViewController: ButtonBarPagerTabStripViewController {
    var user:User? = nil
    let profileViewModel = ProfileViewModel()
    
    override func viewDidLoad() {
        loadDesign()
        super.viewDidLoad()
        navigationItem.title = AppConstants.EDIT_PROFILE
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        getUser()
        fbLoginedOnScreen()
    }
    
    func fbLoginedOnScreen(){
        NotificationCenter.default.addObserver(self,selector: #selector(fbLoginedUpdateScreen),name:Notification.Name(AppConstants.Notifications.SOCIAL_LINK_FB_BAR_BUTTON),object: nil)
    }
    
    @objc func fbLoginedUpdateScreen(){
        self.getUser()
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let tabBasicInfoViewController = UIStoryboard(name: AppConstants.Storyboard.BASIC_INFO_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.BASIC_INFO) as! BasicInfoViewController
        tabBasicInfoViewController.user = user
        
        let tabAboutMeViewController = UIStoryboard(name: AppConstants.Storyboard.ABOUT_ME_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.ABOUT_ME) as! AboutMeViewController
        tabAboutMeViewController.user = user
        
        let tabSocialLinksViewController = UIStoryboard(name: AppConstants.Storyboard.SOCIAL_LINKS_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.SOCIAL_LINKS) as! SocialLinksViewController
        tabSocialLinksViewController.user = user
        
        return [tabBasicInfoViewController,tabAboutMeViewController,tabSocialLinksViewController]
    }
    
    func loadDesign() {
        self.settings.style.buttonBarBackgroundColor = UIColor.white
        self.settings.style.buttonBarItemBackgroundColor = UIColor.white
        self.settings.style.selectedBarBackgroundColor = UIColor.cFB9D52
        self.settings.style.buttonBarItemFont = UIFont.fontSemiBold(size: 12)
        self.settings.style.selectedBarHeight = 2.0
        self.settings.style.buttonBarMinimumLineSpacing = 0
        self.settings.style.buttonBarItemTitleColor = UIColor.black
        self.settings.style.buttonBarItemsShouldFillAvailableWidth = true
        self.settings.style.buttonBarLeftContentInset = 10
        self.settings.style.buttonBarRightContentInset = 10
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            
            oldCell?.label.textColor = UIColor.c2A2A2A
            oldCell?.label.font = UIFont.fontSemiBold(size: 12)
            newCell?.label.textColor = UIColor.cBB189C
            newCell?.label.font = UIFont.fontSemiBold(size: 12)
            
        }
    }
    
    func getUser() {
        self.view.makeToastActivity(.center)
        profileViewModel.getUser(accessToken: BaseViewController().getAccessToken(), { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<User> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    BaseViewController().showMessageDialog(message: error)
                } else {
                    let userData = data.data!
                    self.user = userData
                    AppConstants.editedUserList = userData
                    self.reloadPagerTabStripView()
                    self.view.hideToastActivity()
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                BaseViewController().showInternetError()
            }
        })
    }
    
}
