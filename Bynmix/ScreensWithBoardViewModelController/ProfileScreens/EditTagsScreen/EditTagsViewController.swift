import UIKit

class EditTagsViewController: BaseViewController,LoadMoreProtocol {
    
    func onLoadMore() {
        if !isApiCalled {
            isApiCalled = true
            self.getBrands()
        }
    }
    
    @IBOutlet weak var brandsButton: UIButton!
    @IBOutlet weak var loader: UIActivityIndicatorView!
    @IBOutlet weak var autoCompleteTextfield: UITextField!
    @IBOutlet weak var collectionViewLayout: UICollectionView!
    @IBOutlet var fullScreenLoader: UIActivityIndicatorView!
    @IBOutlet var buttonLoader: UIActivityIndicatorView!
    @IBOutlet var selectedBrandsCollectionView: UICollectionView!
    @IBOutlet var mainView: UIView!
    @IBOutlet var footerView: UIView!
    
    var tags = [Tags]()
    let onboardingViewModel = OnBoardingViewModel()
    let profileViewModel = ProfileViewModel()
    var currentPage = 1
    var isApiCalled = false
    var onboardingTagsHandler = OnboardingTagsHandler()
    var onboardingSelectedTagsHandler = OnboardingSelectedTagsHandler()
    var isTagAlreadyAdded = false
    var user:User? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "My Favorite Styles"
        collectionViewLayout.delegate = onboardingTagsHandler
        collectionViewLayout.dataSource = onboardingTagsHandler
        selectedBrandsCollectionView.delegate = onboardingSelectedTagsHandler
        selectedBrandsCollectionView.dataSource = onboardingSelectedTagsHandler
        collectionViewLayout.contentInset = UIEdgeInsets(top: 56, left: 22, bottom: 65, right: 22)
        autoCompleteTextfield.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        collectionViewLayout!.collectionViewLayout = FlowLayoutHelper()
        loader.stopAnimating()
        buttonLoader.stopAnimating()
        hideKeyboardWhenTappedAround()
        brandsButton.backgroundColor = UIColor.cBB189C
        brandsButton.isEnabled = true
        onboardingTagsHandler.loadMoreDelegate = self
        onBrandsClick()
        getSelectedBrands()
    }
    
    func onBrandsClick(){
        self.onboardingTagsHandler.tagsClick = { tag in
            if self.onboardingSelectedTagsHandler.tags.count > 0{
                for i in 0..<self.onboardingSelectedTagsHandler.tags.count{
                    if self.onboardingSelectedTagsHandler.tags[i].descriptorId == tag.descriptorId{
                        return
                    }
                }
                self.onboardingSelectedTagsHandler.tags.append(tag)
            }else{
                self.onboardingSelectedTagsHandler.tags.append(tag)
            }
            //self.checkTags()
            self.selectedBrandsCollectionView.reloadData()
            self.scrollToLastItem()
        }
        self.onboardingSelectedTagsHandler.tagsClick = { tags,index in
            self.onboardingSelectedTagsHandler.tags.remove(at: index)
            self.selectedBrandsCollectionView.reloadData()
            //self.checkTags()
        }
    }
    
    func scrollToLastItem() {
        let lastSection = self.selectedBrandsCollectionView.numberOfSections - 1
        let lastRow = selectedBrandsCollectionView.numberOfItems(inSection: lastSection)
        let indexPath = IndexPath(row: lastRow - 1, section: lastSection)
        self.selectedBrandsCollectionView.scrollToItem(at: indexPath, at: .right, animated: true)
    }
    
    func checkTags() {
        if self.onboardingSelectedTagsHandler.tags.count == 3 {
            brandsButton.isEnabled = true
            brandsButton.backgroundColor = UIColor.cBB189C
        } else {
            brandsButton.isEnabled = false
            brandsButton.backgroundColor = UIColor.cBDBDBD
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    @objc func textFieldDidChange(textField: UITextField) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reloadBrands), object: nil)
        self.perform(#selector(self.reloadBrands), with: nil, afterDelay: 0.6)
    }
    
    @objc func reloadBrands() {
        self.currentPage = 1
        getBrands()
        loader.startAnimating()
    }
    
    private func getBrands() {
        let searchText = autoCompleteTextfield.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        onboardingViewModel.getTags(accessToken: getAccessToken(),page:self.currentPage ,searchQuery: searchText, {(response) in
            self.isApiCalled = false
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Tags> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let brandData = data.data!
                    if (brandData.items?.count ?? 0) > 0{
                        self.loader.stopAnimating()
                        if let brands = brandData.items {
                            
                            if self.currentPage == 1 {
                                self.tags = brands
                            } else {
                                self.tags.append(contentsOf: brands)
                            }
                            if brandData.totalPages == self.currentPage {
                                self.isApiCalled = true
                                self.onboardingTagsHandler.isLoadMoreRequired = false
                            }
                        }
                        self.onboardingTagsHandler.tags = self.tags
                    }else{
                        self.loader.stopAnimating()
                        self.onboardingTagsHandler.tags.removeAll()
                        self.onboardingTagsHandler.isLoadMoreRequired = false
                        
                    }
                    self.collectionViewLayout.reloadData()
                    self.getUser()
                    
                }
                self.currentPage = self.currentPage + 1
            } else {
                self.loader.stopAnimating()
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.loader.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func showScreenAndFooter(){
        self.fullScreenLoader.stopAnimating()
        self.mainView.isHidden = false
        self.footerView.isHidden = false
    }
    
    private func getSelectedBrands() {
        profileViewModel.getSelectedDescriptors(accessToken: getAccessToken(), {(response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[Tags]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let tags = data.data!
                    self.setSelectedTags(tags: tags)
                    
                }
            } else {
                self.fullScreenLoader.stopAnimating()
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.fullScreenLoader.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func setSelectedTags(tags:[Tags]){
        self.onboardingSelectedTagsHandler.tags = tags
        selectedBrandsCollectionView.reloadData()
        
        getBrands()
    }
    
    @IBAction func brandsButton(_ sender: UIButton) {
        self.buttonLoader.startAnimating()
        self.brandsButton.isEnabled = false
        var selectedTags = [String]()
        
        for i in 0..<self.onboardingSelectedTagsHandler.tags.count{
            selectedTags.append(String(self.onboardingSelectedTagsHandler.tags[i].descriptorId ?? 0))
        }
        
        var users :User = self.user!
        users.shortTitle = users.title
        users.userDescriptors = selectedTags
        
        profileViewModel.saveEditedTags(accessToken: getAccessToken(), data: users, {(response) in
            if response.response?.statusCode == 200 || response.response?.statusCode == 201{
                self.updateBrandsButton()
                self.navigationController?.popViewController(animated: false)
            }else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
                self.updateBrandsButton()
            }
        } , { (error) in
            self.updateBrandsButton()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func updateBrandsButton(){
        self.brandsButton.isEnabled = true
        self.buttonLoader.stopAnimating()
    }
    
    func getUser() {
        profileViewModel.getUser(accessToken: getAccessToken(), { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<User> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    BaseViewController().showMessageDialog(message: error)
                } else {
                    let userData = data.data!
                    self.user = userData
                    self.showScreenAndFooter()
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                BaseViewController().showInternetError()
            }
        })
    }
    
}
