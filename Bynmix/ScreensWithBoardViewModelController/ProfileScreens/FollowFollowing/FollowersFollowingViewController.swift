import UIKit
import Alamofire

class FollowersFollowingViewController: BaseViewController, LoadMoreProtocol,UISearchBarDelegate,UserFilterApplyProtocol,UserFilterClearAllProtocol {
    
    func userFilterApply(selectedFilter: [SelectedFilter], heightModal: HeightModel) {
        self.currentPage = 1
        print("selectedFilter:",selectedFilter)
        self.setUserFilterData(selectedFilterList:selectedFilter,heightModel:heightModal)
        self.navigationController?.popViewController(animated: false)
        self.view.makeToastActivity(.center)
    }
    
    func userFilterClearAll() {
        self.view.makeToastActivity(.center)
        self.currentPage = 1
        FollowersFollowingViewController.userFiltersApplied = UserFilterData()
        self.getAllUsers(searchText: self.searchBar?.text?.trimmingCharacters(in: .whitespacesAndNewlines), userFilterData: nil)
        self.navigationController?.popViewController(animated: false)
    }
    
    func onLoadMore() {
        if !isApiCalled {
            isApiCalled = true
            self.typeFollowFollowing()
        }
    }
    
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var tableViewLayout: UITableView!
    
    @IBOutlet weak var noUsersView: UIView!
    @IBOutlet weak var noUsersShadowView: UIView!
    
    var refreshControl : UIRefreshControl?
    var users:User? = nil
    var USERNAME:String?
    var TYPE:Int?
    var ID:Int?
    let followersFollowingHandler = FollowersFollowingHandler()
    let profileViewModel = ProfileViewModel()
    let feedViewModel = FeedViewModel()
    let spinner = UIActivityIndicatorView(style: .gray)
    var currentPage = 1
    var isApiCalled = false
    var filterBarButton:UIBarButtonItem?
    var searchBar:UISearchBar?
    var selectedHeightModel = HeightModel()
    var selectedUserFilterList:[SelectedFilter] = []
    let milliSeconds = 0.6
    static var userFiltersApplied:UserFilterData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewLayout.isHidden = true
        typeFollowFollowing()
        tableViewLayout.delegate = followersFollowingHandler
        tableViewLayout.dataSource = followersFollowingHandler
        
        tableViewLayout.rowHeight = UITableView.automaticDimension
        tableViewLayout.estimatedRowHeight = 300
        self.navigationItem.backBarButtonItem?.title = ""
        addRefreshControl()
        activitySpinner()
        followersFollowingHandler.itemClickHandler = { user in
            self.updateFollowStatus(userId: user.userId!)
        }
        
        followersFollowingHandler.otherUserClickHandler = { user in
            if user.userId == AppDefaults.getUserId(){
                self.tabBarController?.selectedIndex = 4
            }else{
                self.openProfile(id:user.userId!,username:user.username!)
            }
        
        }
        
        followersFollowingHandler.loadMoreDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        noUsersShadowView.layer.cornerRadius = 2
        noUsersShadowView.layer.shadowColor = UIColor.black.cgColor
        noUsersShadowView.alpha = CGFloat(0.26)
        noUsersShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        noUsersShadowView.layer.shadowOpacity = 1
        noUsersShadowView.layer.zPosition = -1
    }
    
    func typeFollowFollowing() {
        if TYPE == AppConstants.TYPE_FOLLOWER {
            getfollowers()
            navigationItem.title = USERNAME! + AppConstants.FOLLOWERS
        } else if TYPE == AppConstants.TYPE_FOLLOWING{
            getfollowing()
            navigationItem.title = USERNAME! + AppConstants.FOLLOWING
        } else if TYPE == AppConstants.TYPE_LIKE_POST {
            getLikes(type: AppConstants.POSTS)
            navigationItem.title =  AppConstants.LIKES
        } else if TYPE == AppConstants.TYPE_LIKE_LISTING {
            getLikes(type: AppConstants.LISTINGS)
            navigationItem.title =  AppConstants.LIKES
        }else if TYPE == AppConstants.TYPE_OTHER_USER_FOLLOWER {
            getOtherUserFollowers(userId: ID!)
            navigationItem.title = USERNAME! + AppConstants.FOLLOWERS
        }else if TYPE == AppConstants.TYPE_OTHER_USER_FOLLOWING {
            getOtherUserFollowing(userId: ID!)
            navigationItem.title = USERNAME! + AppConstants.FOLLOWING
        }else if TYPE == AppConstants.TYPE_SEARCH_USERS {
            if FollowersFollowingViewController.userFiltersApplied == nil{
                FollowersFollowingViewController.userFiltersApplied = UserFilterData()
            }
            getAllUsers(searchText: self.searchBar?.text?.trimmingCharacters(in: .whitespacesAndNewlines), userFilterData: FollowersFollowingViewController.userFiltersApplied)
            makeSearchBar()
            let filterBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.FILTER_ICON), style: .plain, target: self, action: #selector(onFilterBarButtonClick))
            self.navigationItem.rightBarButtonItem  = filterBarButton
        }
    }
    
    @objc func onFilterBarButtonClick(){
        userFilters()
    }
    
    func userFilters(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.USER_FILTER_STORYBOARD, bundle: nil)
        let userFilterViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.USER_FILTERS_SCREEN) as! UserFiltersViewController
        userFilterViewController.userFilterApplyDelegate = self
        userFilterViewController.userFilterClearAllDelegate = self
        self.navigationController?.pushViewController(userFilterViewController, animated: false)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.reloadUsers), object: nil)
        self.perform(#selector(self.reloadUsers), with: nil, afterDelay: 0.6)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)  {
        searchBar.resignFirstResponder()
    }
    
    @objc func reloadUsers() {
        self.currentPage = 1
        if FollowersFollowingViewController.userFiltersApplied == nil{
            FollowersFollowingViewController.userFiltersApplied = UserFilterData()
        }
        self.getAllUsers(searchText: searchBar?.text?.trimmingCharacters(in: .whitespacesAndNewlines), userFilterData: FollowersFollowingViewController.userFiltersApplied)
    }
    
    func makeSearchBar() {
        searchBar = UISearchBar()
        searchBar!.sizeToFit()
        if let textfield = searchBar?.value(forKey: "searchField") as? UITextField {
            textfield.borderStyle = .line
            textfield.backgroundColor = UIColor.white
            textfield.layer.cornerRadius = 2
            textfield.clipsToBounds = true
            textfield.font = UIFont.fontRegular(size: 14)
            if let leftView = textfield.leftView as? UIImageView {
                leftView.image = leftView.image?.withRenderingMode(.alwaysTemplate)
                leftView.tintColor = UIColor.cB5B5B5
            }
        }
        searchBar?.placeholder = AppConstants.Strings.SEARCH_FOR_FASHIONISTS
        searchBar?.delegate = self
        
        let searchBarTextAttributes = [
            NSAttributedString.Key.font: UIFont.fontRegular(size: 14),NSAttributedString.Key.foregroundColor: UIColor.black
        ]
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = searchBarTextAttributes
        
        navigationItem.titleView = searchBar
    }
    
    func getfollowers() {
        profileViewModel.getFollowers(accessToken: getAccessToken(),page: currentPage ,{ (response) in
            self.handleResponse(response: response)
        } , { (error) in
            self.refreshControl?.endRefreshing()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getOtherUserFollowers(userId:Int) {
        profileViewModel.getOtherUserFollowers(accessToken: getAccessToken(),page: currentPage, userId: userId ,{ (response) in
            self.handleResponse(response: response)
        } , { (error) in
            self.refreshControl?.endRefreshing()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getfollowing() {
        profileViewModel.getFollowing(accessToken: getAccessToken(),page: currentPage, { (response) in
            self.handleResponse(response: response)
        } , { (error) in
            self.refreshControl?.endRefreshing()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getOtherUserFollowing(userId:Int) {
        profileViewModel.getOtherUserFollowing(accessToken: getAccessToken(),page: currentPage, userId: userId, { (response) in
            self.handleResponse(response: response)
        } , { (error) in
            self.refreshControl?.endRefreshing()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getLikes(type: String) {
        feedViewModel.getLikeUsers(accessToken: getAccessToken(),page:currentPage,id: ID!,likesType: type, { (response) in
            self.handleResponse(response: response)
        } , { (error) in
            self.refreshControl?.endRefreshing()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getAllUsers(searchText:String?,userFilterData:UserFilterData?) {
        var userFilter = UserFilterData()
        if userFilterData != nil && searchText != nil{
            userFilter = userFilterData!
            userFilter.searchText = searchText ?? ""
        }else if userFilterData != nil{
            userFilter = userFilterData!
        }else{
            userFilter.searchText = searchText ?? ""
        }
        
        if userFilter.searchText != ""{
            searchBar?.isLoading = true
        }
        
        feedViewModel.getAllUsers(accessToken: getAccessToken(),page:currentPage, data: userFilter, { (response) in
            self.view.hideToastActivity()
            self.handleResponseInAllUsers(response: response,searchText: searchText, userFilterData: userFilterData)
        } , { (error) in
            self.refreshControl?.endRefreshing()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func handleResponseInAllUsers(response: AFDataResponse<Any>,searchText:String?,userFilterData:UserFilterData?) {
          self.isApiCalled = false
          searchBar?.isLoading = false
          self.refreshControl?.endRefreshing()
          if response.response?.statusCode == 200 {
              let data: ApiResponse<User> = Utils.convertResponseToData(data: response.data!)
              if data.isError {
                  let error = Utils.getErrorMessage(errors: data.errors)
                  self.showMessageDialog(message: error)
              } else {
                  let userdata = data.data
                  if (userdata?.items!.count ?? 0) > 0{
                      
                      if let users = userdata?.items {
                          if self.currentPage == 1 {
                              self.followersFollowingHandler.users = users
                          } else {
                              self.followersFollowingHandler.users.append(contentsOf: users)
                          }
                          self.screenActivityIndicator.stopAnimating()
                          self.tableViewLayout.isHidden = false
                          if userdata?.totalPages == currentPage {
                              self.isApiCalled = true
                              self.followersFollowingHandler.isLoadMoreRequired = false
                          }
                          self.tableViewLayout.reloadData()
                          self.noUsersView.isHidden = true
                          self.noUsersShadowView.isHidden = true
                      }
                      
                  }else{
                      self.followersFollowingHandler.users.removeAll()
                      self.tableViewLayout.reloadData()
                      self.noUsersView.isHidden = false
                      self.noUsersShadowView.isHidden = false
                      
                  }
              }
              self.currentPage = self.currentPage + 1
            
            var userFilter = UserFilterData()
            var trackSearchOfUser = TrackSearchOfUser()
            trackSearchOfUser.searchBarLocation = AppConstants.TYPE_SEARCH_BAR_LOCATION_USERS
            
            var jsonString = ""
            
            if trackSearchDictionary.count > 0{
                jsonString = convertIntoJSONString(arrayObject: trackSearchDictionary)!
                print("jsonString - \(jsonString)")
            }
            
            if userFilterData != nil && searchText != nil{
                userFilter = userFilterData!
                userFilter.searchText = searchText ?? ""
                
                trackSearchOfUser.searchString = searchText ?? ""
                if jsonString.count > 0{
                    trackSearchOfUser.searchFilters = jsonString
                }
                self.trackSearch(trackSearchOfUser: trackSearchOfUser)
            }else if userFilterData != nil{
                userFilter = userFilterData!
                if jsonString.count > 0{
                    trackSearchOfUser.searchFilters = jsonString
                }
                self.trackSearch(trackSearchOfUser: trackSearchOfUser)
            }else{
                userFilter.searchText = searchText ?? ""
                trackSearchOfUser.searchString = searchText ?? ""
                if searchText?.count ?? 0 > 0{
                    self.trackSearch(trackSearchOfUser: trackSearchOfUser)
                }
            }
            
          } else {
            self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            self.noUsersView.isHidden = false
            self.noUsersShadowView.isHidden = false
          }
      }
    
    func handleResponse(response: AFDataResponse<Any>) {
        self.isApiCalled = false
        searchBar?.isLoading = false
        self.refreshControl?.endRefreshing()
        if response.response?.statusCode == 200 {
            let data: ApiResponse<User> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            } else {
                let userdata = data.data
                if (userdata?.items!.count)! > 0{
                    
                    if let users = userdata?.items {
                        if self.currentPage == 1 {
                            self.followersFollowingHandler.users = users
                        } else {
                            self.followersFollowingHandler.users.append(contentsOf: users)
                        }
                        self.screenActivityIndicator.stopAnimating()
                        self.tableViewLayout.isHidden = false
                        if userdata?.totalPages == currentPage {
                            self.isApiCalled = true
                            self.followersFollowingHandler.isLoadMoreRequired = false
                        }
                        self.tableViewLayout.reloadData()
                        self.noUsersView.isHidden = true
                        self.noUsersShadowView.isHidden = true
                    }
                    
                }else{
                    self.followersFollowingHandler.users.removeAll()
                    self.tableViewLayout.reloadData()
                    self.noUsersView.isHidden = false
                    self.noUsersShadowView.isHidden = false
                    
                }
                
            }
            self.currentPage = self.currentPage + 1
        } else {
            self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            self.noUsersView.isHidden = false
            self.noUsersShadowView.isHidden = false
        }
    }
    
    func getAnotherUser(userId: Int) {
        profileViewModel.getUser(accessToken: getAccessToken(),userId: userId, { (response) in
            if response.response?.statusCode == 200 {
                self.navigationController?.popViewController(animated: false)
                let data: ApiResponse<User> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.users = data.data!
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            } 
        })
    }
    
    func updateFollowStatus(userId: Int) {
        profileViewModel.updateFollowStatus(accessToken: getAccessToken(), userId: userId,{(response) in
            if response.response?.statusCode == 200 {
            }else{
                print(AppConstants.Strings.SOMETHING_WENT_WRONG)
                //self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func addRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        tableViewLayout.addSubview(refreshControl!)
    }
    
    @objc func refreshList(){
        currentPage = 1
        isApiCalled = false
        self.followersFollowingHandler.isLoadMoreRequired = true
        refreshListWithType()
    }
    
    func refreshListWithType() {
        if TYPE == AppConstants.TYPE_FOLLOWER {
            getfollowers()
        } else if TYPE == AppConstants.TYPE_FOLLOWING{
            getfollowing()
        } else if TYPE == AppConstants.TYPE_LIKE_POST {
            getLikes(type: AppConstants.POSTS)
        } else if TYPE == AppConstants.TYPE_LIKE_LISTING {
            getLikes(type: AppConstants.LISTINGS)
        }else if TYPE == AppConstants.TYPE_OTHER_USER_FOLLOWER {
            getOtherUserFollowers(userId: ID!)
        }else if TYPE == AppConstants.TYPE_OTHER_USER_FOLLOWING {
            getOtherUserFollowing(userId: ID!)
        }else if TYPE == AppConstants.TYPE_SEARCH_USERS {
            if FollowersFollowingViewController.userFiltersApplied == nil{
                FollowersFollowingViewController.userFiltersApplied = UserFilterData()
            }
            getAllUsers(searchText: self.searchBar?.text?.trimmingCharacters(in: .whitespacesAndNewlines), userFilterData: FollowersFollowingViewController.userFiltersApplied)
        }
    }
    
    func activitySpinner() {
        spinner.color = UIColor.darkGray
        spinner.hidesWhenStopped = true
        tableViewLayout.tableFooterView = spinner
    }
    
    func setUserFilterData(selectedFilterList:[SelectedFilter],heightModel:HeightModel) {
        var userFilterData:UserFilterData = UserFilterData()
        var stylesIds:[Int] = []
        var bodyTypeIds:[Int] = []
        var brandsIds:[Int] = []
        
        var styleNames:[String] = []
        var bodyTypeNames:[String] = []
        var brandNames:[String] = []
        var heightNames:[String] = []
        
//        self.selectedHeightModel = heightModel
//        self.selectedUserFilterList = selectedFilterList
        for i in 0..<selectedFilterList.count {
            if (selectedFilterList[i].type == AppConstants.USER_STYLE_FILTER) {
                if (selectedFilterList[i].id != 0) {
                    stylesIds.append(selectedFilterList[i].id!)
                    userFilterData.styleIds = stylesIds
                    styleNames.append(selectedFilterList[i].name!)
                    trackSearchDictionary.updateValue(styleNames, forKey: "style")
                }
            } else if (selectedFilterList[i].type == AppConstants.USER_BODY_TYPE_FILTER) {
                if (selectedFilterList[i].id != 0) {
                    bodyTypeIds.append(selectedFilterList[i].id!)
                    userFilterData.bodyTypeIds = bodyTypeIds
                    bodyTypeNames.append(selectedFilterList[i].name!)
                    trackSearchDictionary.updateValue(bodyTypeNames, forKey: "bodyType")
                }
            } else if (selectedFilterList[i].type == AppConstants.USER_BRAND_FILTER) {
                if (selectedFilterList[i].id != 0) {
                    brandsIds.append(selectedFilterList[i].id!)
                    userFilterData.brandIds = brandsIds
                    brandNames.append(selectedFilterList[i].name!)
                    trackSearchDictionary.updateValue(brandNames, forKey: "brand")
                }
            }
        }
        styleNames.removeAll()
        bodyTypeNames.removeAll()
        brandNames.removeAll()
        heightNames.removeAll()
        
        stylesIds.removeAll()
        bodyTypeIds.removeAll()
        brandsIds.removeAll()
        
        userFilterData.customMinHeightFeet = "" + String(heightModel.customMinHeightFeet)
        userFilterData.customMinHeightInches = "" + String(heightModel.customMinHeightInches)
        userFilterData.customMaxHeightFeet = "" + String(heightModel.customMaxHeightFeet)
        userFilterData.customMaxHeightInches = "" + String(heightModel.customMaxHeightInches)
        if (heightModel.isHeightShowAll ?? false) {
            userFilterData.customMinHeightFeet = nil
            userFilterData.customMinHeightInches = nil
            userFilterData.customMaxHeightFeet = nil
            userFilterData.customMaxHeightInches = nil
        } else if (heightModel.customMinHeightFeet == 3 && heightModel.customMaxHeightFeet == 8 && heightModel.customMinHeightInches == 0 && heightModel.customMaxHeightInches == 8) {
            userFilterData.customMinHeightFeet = nil
            userFilterData.customMinHeightInches = nil
            userFilterData.customMaxHeightFeet = nil
            userFilterData.customMaxHeightInches = nil
        }
        FollowersFollowingViewController.userFiltersApplied = userFilterData
        self.getAllUsers(searchText: searchBar?.text?.trimmingCharacters(in: .whitespacesAndNewlines),userFilterData: userFilterData)

    }
    
}
