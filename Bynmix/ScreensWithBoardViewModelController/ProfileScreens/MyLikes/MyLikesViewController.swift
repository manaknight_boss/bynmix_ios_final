import UIKit
import XLPagerTabStrip

class MyLikesViewController: ButtonBarPagerTabStripViewController,UserFilterApplyProtocol,UserFilterClearAllProtocol,UITabBarControllerDelegate,CreateClickProtocol {
    
    func onCreateClick(type: Int) {
        if type == AppConstants.CREATE_TYPE_POST {
            if let nav = self.tabBarController?.viewControllers?[2] as? CreateNavigationViewController {
                nav.viewControllerType = AppConstants.CREATE_TYPE_POST
                nav.popToRootViewController(animated: false)
            }
            self.tabBarController?.selectedIndex = 2
        } else if type == AppConstants.CLEAR_STACK_OF_CREATE_SCREEN_TYPE {
            AppConstants.IS_JUST_CREATED = AppConstants.CREATE_TYPE_POST
            self.tabBarController?.selectedIndex = 4
            if let nav = self.tabBarController?.viewControllers?[4] as? ProfileNavigationViewController {
                nav.popToRootViewController(animated: false)
            }
        } else if type == AppConstants.CLEAR_STACK_OF_CREATE_LISTING_TYPE {
            AppConstants.IS_JUST_CREATED = AppConstants.CREATE_TYPE_LISTING
            self.tabBarController?.selectedIndex = 4
            if let nav = self.tabBarController?.viewControllers?[4] as? ProfileNavigationViewController {
                nav.popToRootViewController(animated: false)
            }
        } else if type == AppConstants.CREATE_TYPE_LISTING {
            if let nav = self.tabBarController?.viewControllers?[2] as? CreateNavigationViewController {
                nav.popToRootViewController(animated: false)
                nav.viewControllerType = AppConstants.CREATE_TYPE_LISTING
            }
            self.tabBarController?.selectedIndex = 2
        } else if type == AppConstants.CREATE_TYPE_NONE {
            // todo fix here
            //BaseViewController.self.selectedTab = -1
        }else{
            print("else block")
        }
    }
    
    func userFilterApply(selectedFilter: [SelectedFilter], heightModal: HeightModel) {
        NotificationCenter.default.post(name: Notification.Name(AppConstants.Notifications.MY_LIKES_APPLY_FILTER_BUTTON),object: nil,userInfo:[AppConstants.Notifications.MY_LIKES_APPLY_FILTER_LIST: selectedFilter])
        self.navigationController?.popViewController(animated: false)
    }
    
    func userFilterClearAll() {
        AppConstants.clearFilterList = true
        NotificationCenter.default.post(name: Notification.Name(AppConstants.Notifications.MY_LIKES_CLEAR_ALL_FILTER_BUTTON),object: nil,userInfo:[AppConstants.Notifications.MY_LIKES_CLEAR_FILTER_LIST: AppConstants.clearFilterList])
        self.navigationController?.popViewController(animated: false)
    }
    
    var filterBarButton:UIBarButtonItem?
    var currentTabIndex:Int?
    
    override func viewDidLoad() {
        loadDesign()
        super.viewDidLoad()
        navigationItem.title = AppConstants.MY_LIKES
        
        let barButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.FILTER_ICON), style: .plain, target: self, action: #selector(onFilterClick))
        self.filterBarButton = barButton
        navigationItem.rightBarButtonItem = filterBarButton
        
        if currentTabIndex == 1{
            self.moveToViewController(at: 1,animated: true)
        }else{
            self.moveToViewController(at: 0,animated: true)
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) {
            self.reloadPagerTabStripView()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.tabBarController?.delegate = self
    }
    
    @objc func onFilterClick(){
        filterListing()
    }
    
    func filterListing(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.LISTING_FILTER_STORYBOARD, bundle: nil)
        let filterListingViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.LISTING_FILTER_SCREEN) as! FilterListingsViewController
        filterListingViewController.filterApplyDelegate = self
        filterListingViewController.filterClearAllDelegate = self
        self.navigationController?.pushViewController(filterListingViewController, animated: false)
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        
        let tabListingsProfileViewController = UIStoryboard(name: AppConstants.Storyboard.LISTING_FEED_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.LISTINGS_FEED_SCREEN) as! ListingsFeedViewController
        tabListingsProfileViewController.TYPE = AppConstants.TYPE_MY_LIKES_LISTING
        
        let tabPostsProfileViewController = UIStoryboard(name: AppConstants.Storyboard.POSTS_FEED_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.POSTS_FEED_SCREEN) as! PostsFeedViewController
        tabPostsProfileViewController.TYPE = AppConstants.TYPE_MY_LIKES_POSTS
        
        return [tabListingsProfileViewController,tabPostsProfileViewController]
    }
    
    override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool) {
        super.updateIndicator(for: viewController, fromIndex: fromIndex, toIndex: toIndex, withProgressPercentage: progressPercentage, indexWasChanged: indexWasChanged)
        if indexWasChanged {
            if progressPercentage != 1.0 {
                if currentIndex == 0 {
                    navigationItem.rightBarButtonItem = filterBarButton
                }
                else{
                    navigationItem.rightBarButtonItem = nil
                }
            }
        }
    }
    
    func loadDesign() {
        self.settings.style.buttonBarBackgroundColor = UIColor.white
        self.settings.style.buttonBarItemBackgroundColor = UIColor.white
        self.settings.style.selectedBarBackgroundColor = UIColor.cFB9D52
        self.settings.style.buttonBarItemFont = UIFont.fontSemiBold(size: 12)
        self.settings.style.selectedBarHeight = 2.0
        self.settings.style.buttonBarMinimumLineSpacing = 0
        self.settings.style.buttonBarItemTitleColor = UIColor.black
        self.settings.style.buttonBarItemsShouldFillAvailableWidth = true
        self.settings.style.buttonBarLeftContentInset = 10
        self.settings.style.buttonBarRightContentInset = 10
        
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            
            oldCell?.label.textColor = UIColor.c2A2A2A
            oldCell?.label.font = UIFont.fontSemiBold(size: 12)
            newCell?.label.textColor = UIColor.cBB189C
            newCell?.label.font = UIFont.fontSemiBold(size: 12)
            
        }

    }
    
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        self.dismiss(animated: false, completion: nil)
        let selectedIndex = tabBarController.viewControllers!.firstIndex(of: viewController)!
        AppConstants.SELECTED_TAB = selectedIndex
        if viewController is CreateNavigationViewController || selectedIndex == 2{
            
            let createViewController = tabBarController.storyboard?.instantiateViewController(withIdentifier: AppConstants.Controllers.CREATE_SCREEN) as! CreateViewController
            createViewController.modalPresentationStyle = .overCurrentContext
            createViewController.createClickDelegate = self
            tabBarController.present(createViewController, animated: false,completion: nil)
            return false
        }
        
        return true
    }
    
}
