import UIKit
import Stripe

class AddNewPaymentViewController: BaseViewController,SelectExistingAddressProtocol,AddPaymentSuggestedAddressProtocol,EditPaymentSuggestedAddressProtocol,AddPaymentWithExistingAddressSuggestedAddressProtocol,EditPaymentWithExistingAddressSuggestedAddressProtocol {
    
    func editPaymentWithExistingAddressSuggestedAddress(cardId:Int,nameOnCard: String, expirationMonth: Int, expirationYear: Int) {
        self.editPaymentWithExistingAddress(cardId:cardId,nameOnCard:nameOnCard,expirationMonth:expirationMonth,expirationYear:expirationYear)
    }
    
    func addPaymentWithExistingAddressSuggestedAddress(token: String, type: Int) {
        self.addPaymentWithExistingAddress(token:token)
    }
    
    func editPaymentSuggestedAddres(token: String, address: Address, fullName: String, type: Int, month: Int, year: Int,cardId:Int,nameOnCard:String) {
        let addressTextField = address.addressOne!
        let addressOptionalTextField = address.addressTwo!
        let city = address.city!
        let state = address.state ?? ""
        let zipcode:String = address.zipCode!
        editPayment(cardId: cardId, nameOnCard: nameOnCard, expirationMonth: month, expirationYear: year, name: fullName, addressTextField: addressTextField, addressOptionalTextField: addressOptionalTextField, city: city, state: state, zipcode: zipcode, needsVerification: false, type: 0)
    }
    
    func addPaymentSuggestedAddress(token: String, address: Address, fullName: String, type: Int) {
        let addressTextField = address.addressOne!
        let addressOptionalTextField = address.addressTwo!
        let city = address.city!
        var state = ""
        let zipcode:String = address.zipCode!
        
        for stateShort in self.statesSelect{
            if address.stateAbbreviation == stateShort.abbreviation{
                state = stateShort.name!
                break
            }
        }
        addPayment(token: token, name: fullName, addressTextField: addressTextField, city: city, state: state, zipcode: zipcode, addressOptionalTextField: addressOptionalTextField, needsVerification: false, type: type)
        self.dismiss(animated: false, completion: nil)
    }
    
    func SelectExistingAddress(type: Int, address: Address) {
        if type == AppConstants.TYPE_SELECT_EXISTING_ADDRESS{
            self.addAddress(address:address)
        }else if type == AppConstants.TYPE_ADD_NEW_ADDRESS_IN_PAYMENT{
            self.addAddress(address:address)
        }
    }
    
    func addAddress(address:Address){
        self.isNewAddress = false
        self.nameTextField.text = address.fullName
        self.addressTextField.text = address.addressOne
        self.addressOptionalTextField.text = address.addressTwo
        self.cityTextField.text = address.city
        self.stateLabel.text = address.state
        self.zipcodeTextField.text = address.zipCode
        self.stateLabel.textColor = UIColor.black
        self.addressId = address.addressId
        self.existingAddress = address
        self.name = address.fullName
        self.addressOne = address.addressOne
        self.addressTwo = address.addressTwo
        self.city = address.city
        self.zipcode = address.zipCode
        self.state = address.state
    }
    
    @IBOutlet var nameOnCardTextField: UITextField!
    @IBOutlet var creditCardNumberTextField: UITextField!
    @IBOutlet var expirationMonthTextField: UITextField!
    @IBOutlet var expirationYearTextField: UITextField!
    @IBOutlet var cvvTextField: UITextField!
    @IBOutlet var cardImageView: UIImageView!
    @IBOutlet var setDefaultPaymentButton: UIButton!
    @IBOutlet var selectExistingAddressButton: UIButton!
    @IBOutlet var nameOnCardErrorButton: UIButton!
    @IBOutlet var creditCardNumberErrorButton: UIButton!
    @IBOutlet var expirationMonthErrorButton: UIButton!
    @IBOutlet var expirationYearErrorButton: UIButton!
    @IBOutlet var cvvErrorButton: UIButton!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var addressTextField: UITextField!
    @IBOutlet var addressOptionalTextField: UITextField!
    @IBOutlet var cityTextField: UITextField!
    @IBOutlet var stateLabel: UILabel!
    @IBOutlet var zipcodeTextField: UITextField!
    @IBOutlet var buttonActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var nameErrorButton: UIButton!
    @IBOutlet var addressErrorButton: UIButton!
    @IBOutlet var cityErrorButton: UIButton!
    @IBOutlet var zipcodeErrorButton: UIButton!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var scrollView:UIScrollView!
    @IBOutlet var addPaymentButton: UIButton!
    @IBOutlet var enterNewAddressLabel: UILabel!
    @IBOutlet var topConstraintNameOnCard: NSLayoutConstraint!
    @IBOutlet var selectExistingAddressView: UIView!
    @IBOutlet var creditCardView: UIView!
    @IBOutlet var CVVView: UIView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    
    @IBOutlet var tokenTextField: UITextField!
    
    let TYPE_STATES: Int = 1
    var statesSelect = [states]()
    var selectedStates = 0
    let editProfileViewModel = EditProfileViewModel()
    let profileViewModel = ProfileViewModel()
    var isdefaultPayment = false
    var isNewAddress = true
    let cardParams = STPCardParams()
    var addressId:Int?
    var cardId:Int?
    var TYPE:Int?
    var sourceTokenOfStripe:String?
    var existingAddress:Address?
    var name:String?
    var addressOne :String?
    var addressTwo:String?
    var city:String?
    var state:String?
    var zipcode:String?
    var addNewPaymentInSellerSetUpDelegate:AddNewPaymentInSellerSetUpProtocol?
    var TYPE_CVV = 0
    var addNewPaymentDelegate:AddNewPaymentProtocol?
    var keyboardTappedInside = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.creditCardNumberTextField.addTarget(self, action: #selector(didChangeText(textField:)), for: .editingChanged)
        buttonActivityIndicator.isHidden = true
        nameOnCardTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        creditCardNumberTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        expirationMonthTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        expirationYearTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        cvvTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        nameTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        addressTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        cityTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        zipcodeTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        getState()
        screenType()
        hideKeyboardWhenTappedAround()
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: UIWindow.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: UIWindow.keyboardWillHideNotification, object: nil)
    }
    
    func screenType(){
        if TYPE == AppConstants.TYPE_EDIT_PAYMENT{
            self.creditCardView.isUserInteractionEnabled = false
            self.creditCardView.backgroundColor = UIColor.cDCDCDC
            self.creditCardNumberTextField.backgroundColor = UIColor.cDCDCDC
            self.CVVView.isUserInteractionEnabled = false
            self.CVVView.backgroundColor = UIColor.cDCDCDC
            self.cvvTextField.backgroundColor = UIColor.cDCDCDC
            self.getCardInfo(cardId: self.cardId!)
            navigationItem.title = AppConstants.Strings.EDIT_PAYMENT_OPTION
            self.addPaymentButton.setTitle(AppConstants.Strings.EDIT_PAYMENT,for: .normal)
            self.scrollView.isHidden = true
            self.screenActivityIndicator.isHidden = false
        }else if TYPE == AppConstants.TYPE_ADD_NEW_PAYMENT_IN_EXISTING_CARD{
            self.setDefaultPaymentButton.setImage(UIImage(named: AppConstants.Images.CHECKBOX_SELECTED), for: .normal)
            self.isdefaultPayment = true
            addNewPayment()
        }else{
            addNewPayment()
        }
    }
    
    func addNewPayment(){
        self.creditCardView.isUserInteractionEnabled = true
        self.creditCardView.backgroundColor = UIColor.white
        self.creditCardNumberTextField.backgroundColor = UIColor.white
        self.CVVView.isUserInteractionEnabled = true
        self.CVVView.backgroundColor = UIColor.white
        self.cvvTextField.backgroundColor = UIColor.white
        navigationItem.title = AppConstants.Strings.ADD_NEW_PAYMENT_OPTION
        self.addPaymentButton.setTitle(AppConstants.Strings.ADD_PAYMENT,for: .normal)
        self.scrollView.isHidden = false
        self.screenActivityIndicator.isHidden = true
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        let userInfo = notification.userInfo!
        var keyboardFrame:CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        
        var contentInset:UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = keyboardFrame.size.height
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        let contentInset:UIEdgeInsets = UIEdgeInsets.zero
        scrollView.contentInset = contentInset
    }
    
    func getMyCard(newCardId:Int) {
        profileViewModel.getMyWalletCard(accessToken: AppDefaults.getAccessToken(), { (response) in
            self.buttonActivityIndicator.stopAnimating()
            self.addPaymentButton.isEnabled = true
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[Card]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let card = data.data! as [Card]
                    if card.count > 0{
                        for i in 0..<card.count{
//                            if card[i].isDefault!{
//                                let cardNew = card[i]
//                                self.addNewPaymentDelegate?.addNewPayment(type: AppConstants.TYPE_ADD_NEW_PAYMENT_IN_EXISTING_CARD, card: cardNew)
//                            }
                            if card[i].cardId == newCardId{
                                let cardNew = card[i]
                                self.addNewPaymentDelegate?.addNewPayment(type: AppConstants.TYPE_ADD_NEW_PAYMENT_IN_EXISTING_CARD, card: cardNew)
                            }
                        }
                        
                    }
                }
            }
        } , { (error) in
            self.buttonActivityIndicator.stopAnimating()
            self.addPaymentButton.isEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getCardInfo(cardId:Int) {
        self.buttonActivityIndicator.startAnimating()
        self.buttonActivityIndicator.hidesWhenStopped = true
        profileViewModel.getCardInfo(accessToken: AppDefaults.getAccessToken(), id: cardId, { (response) in
            self.scrollView.isHidden = false
            self.screenActivityIndicator.isHidden = true
            self.buttonActivityIndicator.stopAnimating()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Card> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let cardData = data.data!
                    self.sourceTokenOfStripe = cardData.stripeCardId!
                    self.addressId = cardData.addressId
                    self.nameOnCardTextField.text = cardData.nameOnCard
                    self.creditCardNumberTextField.text = AppConstants.CREDIT_CARD_SECRET + cardData.lastFour!
                    self.expirationMonthTextField.text = String(cardData.expirationMonth ?? 0)
                    self.expirationYearTextField.text = String(cardData.expirationYear ?? 0)
                    self.cvvTextField.text = AppConstants.CVV_SECRET
                    if cardData.isDefault!{
                        self.setDefaultPaymentButton.setImage(UIImage(named: AppConstants.Images.CHECKBOX_SELECTED), for: .normal)
                    }else{
                        self.setDefaultPaymentButton.setImage(UIImage(named: AppConstants.Images.CHECKBOX), for: .normal)
                    }
                    self.nameTextField.text = cardData.fullName
                    self.addressTextField.text = cardData.addressLineOne
                    self.addressOptionalTextField.text = cardData.addressLineTwo
                    self.cityTextField.text = cardData.city
                    self.zipcodeTextField.text = cardData.zipcode
                    
                    for stateShort in self.statesSelect{
                        if cardData.state == stateShort.abbreviation{
                            self.stateLabel.text = stateShort.name
                            self.state = stateShort.name
                            self.stateLabel.textColor = UIColor.black
                            break
                        }
                    }
                    
                    self.name = cardData.fullName
                    self.addressOne = cardData.addressLineOne
                    self.addressTwo = cardData.addressLineTwo
                    self.city = cardData.city
                    self.zipcode = cardData.zipcode
                    
                    let brandName: String = cardData.brand!
                    if brandName == AppConstants.TYPE_MASTER_CARD{
                        self.cardImageView?.image = UIImage(named: AppConstants.Images.CARD_MASTER)
                    }else if brandName == AppConstants.TYPE_DISCOVER_CARD{
                        self.cardImageView?.image = UIImage(named: AppConstants.Images.CARD_DISCOVER)
                    }else if brandName == AppConstants.TYPE_JCB_CARD{
                        self.cardImageView?.image = UIImage(named: AppConstants.Images.CARD_JCB)
                    }else if brandName == AppConstants.TYPE_DINERS_CLUB_CARD{
                        self.cardImageView?.image = UIImage(named: AppConstants.Images.CARD_DINERS_CLUB)
                    }else if brandName == AppConstants.TYPE_VISA_CARD{
                        self.cardImageView?.image = UIImage(named: AppConstants.Images.CARD_VISA)
                    }else{
                        self.cardImageView?.image = UIImage(named: AppConstants.Images.CARD_AMERICAN_EXPRESS)
                    }
                }
            }
        } , { (error) in
            self.scrollView.isHidden = false
            self.screenActivityIndicator.isHidden = true
            self.buttonActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    @objc func cardDetails(){
        self.cardParams.number = (creditCardNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines))!
        self.cardParams.expMonth = UInt((expirationMonthTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)!
        self.cardParams.expYear = UInt((expirationYearTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines))!)!
        self.cardParams.cvc = (cvvTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines))!
        self.cardParams.name = self.nameOnCardTextField.text
        
        let name:String = self.nameTextField.text ?? ""
        let addressTextField:String = self.addressTextField.text ?? ""
        let addressOptionalTextField:String = self.addressOptionalTextField.text ?? ""
        let city:String = self.cityTextField.text ?? ""
        let state:String = self.stateLabel.text ?? ""
        let zipcode:String = self.zipcodeTextField.text ?? ""
        
        let existName = self.existingAddress?.fullName
        let existAddress = self.existingAddress?.addressOne
        let existOptionalAddress = self.existingAddress?.addressTwo
        let existCity = self.existingAddress?.city
        let existState = self.existingAddress?.state
        let existZipcode = self.existingAddress?.zipCode
        
        STPAPIClient.shared.createToken(withCard: cardParams) { token, error in
            guard let token = token else {
                print(error!.localizedDescription)
                self.view.makeToast(AppConstants.Strings.CARD_INVALID)
                self.buttonActivityIndicator.stopAnimating()
                self.addPaymentButton.isEnabled = true
                return
            }
            if let card = token.card {
                let brand = STPCard.string(from: card.brand)
                print("brandname",brand)
                self.tokenTextField.text = token.tokenId
                if self.isNewAddress{
                    self.addPayment(token: token.tokenId, name: name, addressTextField: addressTextField, city: city, state: state, zipcode: zipcode, addressOptionalTextField: addressOptionalTextField, needsVerification: true, type: 0)
                }else{
                    var check = 0
                    if existName == name{
                        if existAddress == addressTextField{
                            if existOptionalAddress == addressOptionalTextField{
                                if existCity == city{
                                    if existState == state{
                                        if existZipcode == zipcode{
                                            self.addPaymentWithExistingAddress(token:token.tokenId)
                                        }else{
                                            check = 1
                                        }
                                    }else{
                                        check = 1
                                    }
                                }else{
                                    check = 1
                                }
                            }else{
                                check = 1
                            }
                        }else{
                            check = 1
                        }
                    }else{
                        check = 1
                    }
                    if check == 1{
                        self.addPayment(token: token.tokenId, name: name, addressTextField: addressTextField, city: city, state: state, zipcode: zipcode, addressOptionalTextField: addressOptionalTextField, needsVerification: true, type: 0)
                        self.isNewAddress = true
                    }
                }
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField == self.cvvTextField{
//            cardDetails()
//        }
    }
    
    @objc private func textFieldDidChange(textField: UITextField){
        switch textField {
        case nameOnCardTextField:
            nameOnCardErrorButton.isHidden = true
        case creditCardNumberTextField:
            creditCardNumberErrorButton.isHidden = true
        case expirationMonthTextField:
            expirationMonthErrorButton.isHidden = true
        case expirationYearTextField:
            expirationYearErrorButton.isHidden = true
        case cvvTextField:
            cvvErrorButton.isHidden = true
        case nameTextField:
            nameErrorButton.isHidden = true
        case addressTextField:
            addressErrorButton.isHidden = true
        case cityTextField:
            cityErrorButton.isHidden = true
        case zipcodeTextField:
            zipcodeErrorButton.isHidden = true
        default:
            textField.isHidden = true
        }
        hideToolTip()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case nameOnCardTextField:
            creditCardNumberTextField.becomeFirstResponder()
        case creditCardNumberTextField:
            expirationMonthTextField.becomeFirstResponder()
        case expirationMonthTextField:
            expirationYearTextField.becomeFirstResponder()
        case expirationYearTextField:
            cvvTextField.becomeFirstResponder()
        case cvvTextField:
            nameTextField.becomeFirstResponder()
        case nameTextField:
            addressTextField.becomeFirstResponder()
        case addressTextField:
            addressOptionalTextField.becomeFirstResponder()
        case addressOptionalTextField:
            cityTextField.becomeFirstResponder()
        case cityTextField:
            zipcodeTextField.resignFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }
    
    @IBAction func onStateTapAction(_ sender: UITapGestureRecognizer) {
        showDropDown(tag: TYPE_STATES, title:  AppConstants.Strings.SELECT_STATE, dataSourse: self, delegate: self, {(row) in
            self.stateLabel.textColor = UIColor.black
            self.stateLabel.text = self.statesSelect[row].name
            self.selectedStates = self.statesSelect[row].stateId!
        })
    }
    
    @IBAction func defaultShippingAddressButtonAction(_ sender: UIButton) {
        if setDefaultPaymentButton.currentImage == UIImage(named: AppConstants.Images.CHECKBOX){
            setDefaultPaymentButton.setImage(UIImage(named: AppConstants.Images.CHECKBOX_SELECTED), for: .normal)
            self.isdefaultPayment = true
        }else{
            self.isdefaultPayment = false
            setDefaultPaymentButton.setImage(UIImage(named: AppConstants.Images.CHECKBOX), for: .normal)
        }
    }
    
    @objc func didChangeText(textField:UITextField) {
        textField.text = self.modifyCreditCardString(creditCardString: textField.text!)
        let cardNumber: String = textField.text!
        let cardBrand = STPCardValidator.brand(forNumber: cardNumber.replacingOccurrences(of: " ", with: ""))
//        let cardImage = STPImageLibrary.brandImage(for: cardBrand)
        let cardImage = STPImageLibrary.templatedBrandImage(for:  cardBrand)
        self.cardImageView?.image = cardImage
        let brandName = cardBrand.rawValue
        if brandName == AppConstants.BRAND_AMERICAN_EXPRESS{
            self.TYPE_CVV = AppConstants.BRAND_AMERICAN_EXPRESS
            self.cvvTextField.placeholder = AppConstants.FOUR_HIFAN
        }else{
            self.TYPE_CVV = 0
            self.cvvTextField.placeholder = AppConstants.THREE_HIFAN
        }
    }
    
    func modifyCreditCardString(creditCardString : String) -> String {
        let trimmedString = creditCardString.components(separatedBy: .whitespaces).joined()
        
        let arrOfCharacters = Array(trimmedString)
        var modifiedCreditCardString = ""
        
        if(arrOfCharacters.count > 0) {
            for i in 0...arrOfCharacters.count-1 {
                modifiedCreditCardString.append(arrOfCharacters[i])
                if((i+1) % 4 == 0 && i+1 != arrOfCharacters.count){
                    modifiedCreditCardString.append(" ")
                }
            }
        }
        return modifiedCreditCardString
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newLength = (textField.text ?? "").count + string.count - range.length
        if(textField == creditCardNumberTextField) {
            return newLength <= 19
        }else if (textField == expirationMonthTextField){
            return newLength <= 2
        }else if (textField == expirationYearTextField){
            return newLength <= 4
        }else if (textField == cvvTextField){
            if self.TYPE_CVV == AppConstants.BRAND_AMERICAN_EXPRESS{
                return newLength <= 4
            }else{
                return newLength <= 3
            }
        }
        return true
    }
    
    @IBAction func nameonCardErorrButtonAction(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_NAME_ON_CARD, view: sender)
    }
    
    @IBAction func cardNumberErorrButtonAction(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_CARD_NUMBER, view: sender)
    }
    
    @IBAction func expirationMonthErorrButtonAction(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_EXPIRY_MONTH, view: sender)
    }
    
    @IBAction func expirationYearErorrButtonAction(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_EXPIRY_YEAR, view: sender)
    }
    
    @IBAction func cvvErorrButtonAction(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_CARD_CVV, view: sender)
    }
    
    @IBAction func nameErorrButtonAction(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_FULL_NAME, view: sender)
    }
    
    @IBAction func addressErrorButtonAction(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_ADDRESS, view: sender)
    }
    
    @IBAction func cityErrorButtonAction(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_CITY, view: sender)
    }
    
    @IBAction func zipcodeErrorButtonAction(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_ZIPCODE, view: sender)
    }
    
    @IBAction func selectExistingAddressButton(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.SELECT_EXISTING_ADDRESS_BOARD, bundle: nil)
        let  existingAddressViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.SELECT_EXISTING_ADDRESS_SCREEN) as! SelectExistingAddressViewController
        existingAddressViewController.selectExistingAddressDelegate = self
        navigationController?.pushViewController(existingAddressViewController, animated: true)
    }
    
    @IBAction func addPaymentAction(_ sender: UIButton) {
        let nameOnCard:String = self.nameOnCardTextField.text ?? ""
        let cardNumber:String = self.creditCardNumberTextField.text ?? ""
        let cardMonth:String = self.expirationMonthTextField.text ?? ""
        let cardYear:String = self.expirationYearTextField.text ?? ""
        let cardCVV:String = self.cvvTextField.text ?? ""
        let name:String = self.nameTextField.text ?? ""
        let addressTextField:String = self.addressTextField.text ?? ""
        let city:String = self.cityTextField.text ?? ""
        let state:String = self.stateLabel.text ?? ""
        let zipcode:String = self.zipcodeTextField.text ?? ""
        let addressOptionalTextField:String = self.addressOptionalTextField.text ?? ""
        
        var isError = false
        if !ValidationHelper.isEmptyAny(field: nameOnCard) {
            nameOnCardErrorButton.isHidden = false
            isError = true
        } else {
            nameOnCardErrorButton.isHidden = true
        }
        if !ValidationHelper.isEmptyAny(field: cardNumber) {
            creditCardNumberErrorButton.isHidden = false
            isError = true
        } else {
            creditCardNumberErrorButton.isHidden = true
        }
        if !ValidationHelper.isEmptyAny(field: cardMonth) {
            expirationMonthErrorButton.isHidden = false
            isError = true
        } else {
            expirationMonthErrorButton.isHidden = true
        }
        if !ValidationHelper.isEmptyAny(field: cardYear) {
            expirationYearErrorButton.isHidden = false
            isError = true
        } else {
            expirationYearErrorButton.isHidden = true
        }
        if !ValidationHelper.isEmptyAny(field: cardCVV) {
            cvvErrorButton.isHidden = false
            isError = true
        } else {
            cvvErrorButton.isHidden = true
        }
        if !ValidationHelper.isEmptyAny(field: name) {
            nameErrorButton.isHidden = false
            isError = true
        } else {
            nameErrorButton.isHidden = true
        }
        if !ValidationHelper.isEmptyAny(field: addressTextField) {
            addressErrorButton.isHidden = false
            isError = true
        } else {
            addressErrorButton.isHidden = true
        }
        if !ValidationHelper.isEmptyAny(field: city) {
            cityErrorButton.isHidden = false
            isError = true
        } else {
            cityErrorButton.isHidden = true
        }
        if state == AppConstants.Strings.SELECT_A_STATE {
            self.view.makeToast(AppConstants.Strings.PLEASE_SELECT_STATE)
        } else {
        }
        if !ValidationHelper.isEmptyAny(field: zipcode) {
            zipcodeErrorButton.isHidden = false
            isError = true
        } else {
            zipcodeErrorButton.isHidden = true
        }
        
        hideToolTip()
        if isError {
            return
        }
        self.buttonActivityIndicator.isHidden = false
        self.buttonActivityIndicator.startAnimating()
        self.buttonActivityIndicator.hidesWhenStopped = true
        sender.isEnabled = false
        if self.TYPE == AppConstants.TYPE_EDIT_PAYMENT{
            if self.name == name && self.addressOne == addressTextField && self.addressTwo == addressOptionalTextField && self.city == city && self.state == state && self.zipcode == zipcode{
                self.editPaymentWithExistingAddress(cardId: self.cardId!, nameOnCard: nameOnCard, expirationMonth: Int(cardMonth)!, expirationYear: Int(cardYear)!)
            }else{
                self.editPayment(cardId: self.cardId!, nameOnCard: nameOnCard, expirationMonth: Int(cardMonth)!, expirationYear: Int(cardYear)!, name: name, addressTextField: addressTextField, addressOptionalTextField: addressOptionalTextField, city: city, state: state, zipcode: zipcode, needsVerification: true, type: 0)
            }
        }else{
            self.cardDetails()
        }
    }
    
    func addPayment(token:String,name:String,addressTextField:String,city:String,state:String,zipcode:String,addressOptionalTextField:String,needsVerification:Bool,type:Int) {
        let state:String = self.stateLabel.text ?? ""
        let cardAdd = Card(sourceToken: token,settingAddressId: 0 ,isNewAddress: true, name: name, addressCountry: AppConstants.COUNTRY_US, addressLine1: addressTextField, addressLine2: addressOptionalTextField, addressCity: city, addressState: state, addressZipcode: zipcode,isDefault:self.isdefaultPayment,needsVerification: needsVerification)
        print(cardAdd)
        profileViewModel.addPayment(accessToken: getAccessToken(), data: cardAdd,{(response) in
            if self.TYPE != AppConstants.TYPE_ADD_NEW_PAYMENT_IN_EXISTING_CARD{
                self.buttonActivityIndicator.stopAnimating()
                self.addPaymentButton.isEnabled = true
            }
            if response.response?.statusCode == 201 || response.response?.statusCode == 200{
                let data: ApiResponse<Int> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                if self.TYPE == AppConstants.TYPE_ADD_NEW_PAYMENT_IN_EXISTING_CARD{
                    let cardId = data.data!
                    self.getMyCard(newCardId: cardId)
                }
                self.addNewPaymentInSellerSetUpDelegate?.AddNewPaymentInSellerSetUp()
                self.navigationController?.popViewController(animated: false)
            }else if response.response?.statusCode == 409{
                
                let data: ApiResponse<SuggestedAddress> = Utils.convertResponseToData(data: response.data!)
                
                let addressData = data.data
                let originalAddress = addressData?.originalAddress!
                let suggestedAddress = addressData?.suggestedAddress!
                self.openSuggestedAddress(token: token, suggestedAddress: suggestedAddress!,originalAddress: originalAddress!, fullName: name)
                
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.buttonActivityIndicator.stopAnimating()
                self.addPaymentButton.isEnabled = true
            }
        } , { (error) in
            self.buttonActivityIndicator.stopAnimating()
            self.addPaymentButton.isEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func openSuggestedAddress(token:String,suggestedAddress:Address,originalAddress:Address,fullName:String){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.SUGGESTED_ADDRESS_DIALOG_BOARD, bundle: nil)
        let suggestedAddressViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.SUGGESTED_ADDRESS_DIALOG) as! SuggestedAddressDialogViewController
        suggestedAddressViewController.suggestedAddress = suggestedAddress
        suggestedAddressViewController.originalAddress = originalAddress
        suggestedAddressViewController.fullName = fullName
        suggestedAddressViewController.token = token
        suggestedAddressViewController.addPaymentSuggestedAddressDelegate = self
        suggestedAddressViewController.modalPresentationStyle = .overFullScreen
        navigationController?.present(suggestedAddressViewController, animated: false,completion: nil)
    }
    
    func editPayment (cardId:Int,nameOnCard:String,expirationMonth:Int,expirationYear:Int,name:String,addressTextField:String,addressOptionalTextField:String,city:String,state:String,zipcode:String,needsVerification:Bool,type:Int){
        let state:String = self.stateLabel.text ?? ""
        let cardAdd = Card(sourceToken: self.sourceTokenOfStripe! ,expirationYear:expirationYear,expirationMonth:expirationMonth,nameOnCard:nameOnCard ,isNewAddress: self.isNewAddress, name: name, addressCountry: AppConstants.COUNTRY_US, addressLine1: addressTextField, addressLine2: addressOptionalTextField, addressCity: city, addressState: state, addressZipcode: zipcode,isDefault:self.isdefaultPayment)
        profileViewModel.editPayment(accessToken: getAccessToken(), id: cardId, data: cardAdd,{(response) in
            self.buttonActivityIndicator.stopAnimating()
            self.addPaymentButton.isEnabled = true
            if response.response?.statusCode == 200 {
                self.navigationController?.popViewController(animated: false)
            }else if response.response?.statusCode == 409{
                let data: ApiResponse<SuggestedAddress> = Utils.convertResponseToData(data: response.data!)
                
                let addressData = data.data
                let originalAddress = addressData?.originalAddress!
                let suggestedAddress = addressData?.suggestedAddress!
                self.openSuggestedAddress(token: self.sourceTokenOfStripe!, suggestedAddress: suggestedAddress!,originalAddress: originalAddress!, fullName: name)
                self.editPaymentOpenSuggestedAddress(token:self.sourceTokenOfStripe!, suggestedAddress: suggestedAddress!,originalAddress: originalAddress!, fullName: name,expirationMonth:expirationMonth,expirationYear:expirationYear, cardId: cardId, nameOnCard: nameOnCard)
                
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.buttonActivityIndicator.stopAnimating()
                self.addPaymentButton.isEnabled = true
            }
        } , { (error) in
            self.buttonActivityIndicator.stopAnimating()
            self.addPaymentButton.isEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func editPaymentOpenSuggestedAddress(token:String,suggestedAddress:Address,originalAddress:Address,fullName:String,expirationMonth:Int,expirationYear:Int,cardId:Int,nameOnCard:String){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.SUGGESTED_ADDRESS_DIALOG_BOARD, bundle: nil)
        let suggestedAddressViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.SUGGESTED_ADDRESS_DIALOG) as! SuggestedAddressDialogViewController
        suggestedAddressViewController.suggestedAddress = suggestedAddress
        suggestedAddressViewController.originalAddress = originalAddress
        suggestedAddressViewController.fullName = fullName
        suggestedAddressViewController.token = token
        suggestedAddressViewController.expirationMonth = expirationMonth
        suggestedAddressViewController.expirationYear = expirationYear
        suggestedAddressViewController.cardId = cardId
        suggestedAddressViewController.nameOnCard = nameOnCard
        suggestedAddressViewController.editPaymentSuggestedAddressDelegate = self
        suggestedAddressViewController.modalPresentationStyle = .overFullScreen
        navigationController?.present(suggestedAddressViewController, animated: false,completion: nil)
    }
    
    func editPaymentWithExistingAddress(cardId:Int,nameOnCard:String,expirationMonth:Int,expirationYear:Int) {
        self.isNewAddress = false
        let cardData = Card(sourceToken: self.sourceTokenOfStripe!,expirationYear:expirationYear,expirationMonth: expirationMonth,nameOnCard: nameOnCard,settingAddressId: self.addressId!, isNewAddress: self.isNewAddress,isDefault:self.isdefaultPayment)
        print(self.isdefaultPayment)
        profileViewModel.editPayment(accessToken: getAccessToken(), id: cardId, data: cardData,{(response) in
            self.buttonActivityIndicator.stopAnimating()
            self.addPaymentButton.isEnabled = true
            if response.response?.statusCode == 200 {
                self.navigationController?.popViewController(animated: false)
            }else if response.response?.statusCode == 409{
                let data: ApiResponse<SuggestedAddress> = Utils.convertResponseToData(data: response.data!)
                
                let addressData = data.data
                let originalAddress = addressData?.originalAddress!
                let suggestedAddress = addressData?.suggestedAddress!
                self.editPaymentWithExistingAddressOpenSuggestedAddress(cardId: cardId, token:self.sourceTokenOfStripe!,suggestedAddress:suggestedAddress!,originalAddress:originalAddress!,nameOnCard:nameOnCard,month:expirationMonth,year:expirationYear)
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.buttonActivityIndicator.stopAnimating()
                self.addPaymentButton.isEnabled = true
            }
        } , { (error) in
            self.buttonActivityIndicator.stopAnimating()
            self.addPaymentButton.isEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func editPaymentWithExistingAddressOpenSuggestedAddress(cardId:Int,token:String,suggestedAddress:Address,originalAddress:Address,nameOnCard:String,month:Int,year:Int){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.SUGGESTED_ADDRESS_DIALOG_BOARD, bundle: nil)
        let suggestedAddressViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.SUGGESTED_ADDRESS_DIALOG) as! SuggestedAddressDialogViewController
        suggestedAddressViewController.suggestedAddress = suggestedAddress
        suggestedAddressViewController.originalAddress = originalAddress
        suggestedAddressViewController.token = token
        suggestedAddressViewController.nameOnCard = nameOnCard
        suggestedAddressViewController.expirationMonth = month
        suggestedAddressViewController.expirationYear = year
        suggestedAddressViewController.cardId = cardId
        suggestedAddressViewController.editPaymentWithExistingAddressSuggestedAddressDelegate = self
        suggestedAddressViewController.modalPresentationStyle = .overFullScreen
        navigationController?.present(suggestedAddressViewController, animated: false,completion: nil)
    }
    
    func addPaymentWithExistingAddress(token:String) {
        let cardData = Card(sourceToken: token,settingAddressId: self.addressId!, isNewAddress: self.isNewAddress,isDefault:self.isdefaultPayment)
        print(token)
        profileViewModel.addPayment(accessToken: getAccessToken(), data: cardData,{(response) in
            if self.TYPE != AppConstants.TYPE_ADD_NEW_PAYMENT_IN_EXISTING_CARD{
                self.buttonActivityIndicator.stopAnimating()
                self.addPaymentButton.isEnabled = true
            }
            if response.response?.statusCode == 201 {
                let data: ApiResponse<Int> = Utils.convertResponseToData(data: response.data!)
                self.addNewPaymentInSellerSetUpDelegate?.AddNewPaymentInSellerSetUp()
                if self.TYPE == AppConstants.TYPE_ADD_NEW_PAYMENT_IN_EXISTING_CARD{
                    let cardId = data.data!
                    self.getMyCard(newCardId: cardId)
                }
                self.navigationController?.popViewController(animated: false)
            }else if response.response?.statusCode == 409{
                let data: ApiResponse<SuggestedAddress> = Utils.convertResponseToData(data: response.data!)
                let addressData = data.data
                let originalAddress = addressData?.originalAddress!
                let suggestedAddress = addressData?.suggestedAddress!
                self.addPaymentWithExistingAddressOpenSuggestedAddress(token:token,suggestedAddress: suggestedAddress!,originalAddress: originalAddress!)
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.buttonActivityIndicator.stopAnimating()
                self.addPaymentButton.isEnabled = true
            }
        } , { (error) in
            self.buttonActivityIndicator.stopAnimating()
            self.addPaymentButton.isEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func addPaymentWithExistingAddressOpenSuggestedAddress(token:String,suggestedAddress:Address,originalAddress:Address){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.SUGGESTED_ADDRESS_DIALOG_BOARD, bundle: nil)
        let suggestedAddressViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.SUGGESTED_ADDRESS_DIALOG) as! SuggestedAddressDialogViewController
        suggestedAddressViewController.suggestedAddress = suggestedAddress
        suggestedAddressViewController.originalAddress = originalAddress
        suggestedAddressViewController.token = token
        suggestedAddressViewController.addPaymentWithExistingAddressSuggestedAddressDelegate = self
        suggestedAddressViewController.modalPresentationStyle = .overFullScreen
        navigationController?.present(suggestedAddressViewController, animated: false,completion: nil)
    }
    
    func getAddresses() {
        profileViewModel.getAddresses(accessToken: AppDefaults.getAccessToken(), { (response) in
            self.screenActivityIndicator.stopAnimating()
            self.scrollView.isHidden = false
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[Address]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let addresses = data.data! as [Address]
                    if addresses.count > 0 {
                        self.selectExistingAddressView.isHidden = false
                        self.enterNewAddressLabel.text = AppConstants.Strings.OR_ENTER_A_NEW_ADDRESS
                    }else{
                        self.selectExistingAddressView.isHidden = true
                        self.enterNewAddressLabel.text = AppConstants.Strings.ENTER_A_NEW_ADDRESS
                    }
                }
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            self.scrollView.isHidden = false
            if error.statusCode == AppConstants.NO_INTERNET {
                
            }
        })
    }
    
    
}
extension AddNewPaymentViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return statesSelect.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return statesSelect[row].name
    }
    
    private func getState() {
        editProfileViewModel.getStates(accessToken: getAccessToken(), {(response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[states]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.statesSelect = data.data!
                    self.getAddresses()
                }
            } else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            self.scrollView.isHidden = false
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
}
