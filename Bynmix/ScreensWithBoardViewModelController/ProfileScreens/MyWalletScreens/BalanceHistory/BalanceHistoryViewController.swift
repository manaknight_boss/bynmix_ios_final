import UIKit
import Alamofire

class BalanceHistoryViewController: BaseViewController,LoadMoreProtocol {
    
    func onLoadMore() {
        if !isApiCalled {
            isApiCalled = true
            self.getbalanceHistory()
        }
    }
    
    @IBOutlet var balanceHistoryTableView: UITableView!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var emptyStateView: UIView!
    @IBOutlet var emptyStateShadowView: UIView!
    
    let profileViewModel = ProfileViewModel()
    let balanceHistoryHandler = BalanceHistoryHandler()
    var currentPage = 1
    var isApiCalled = false
    var refreshControl : UIRefreshControl?
    var accountInfo:AccountInfo?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.Strings.WITHDRAW_HISTORY_TITLE
        balanceHistoryTableView.delegate = balanceHistoryHandler
        balanceHistoryTableView.dataSource = balanceHistoryHandler
        getbalanceHistory()
        addRefreshControl()
        balanceHistoryTableView.contentInset = UIEdgeInsets(top: 14, left: 0, bottom: 14, right: 0)
        balanceHistoryHandler.loadMoreDelegate = self
        if accountInfo != nil{
            let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.SUCCESS_MESSAGE_DIALOG_BOARD, bundle:nil)
            let withdrawDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.SUCCESS_MESSAGE_DIALOG) as! SuccessMessageViewController
            withdrawDialogViewController.accountInfo = accountInfo
            withdrawDialogViewController.modalPresentationStyle = .overCurrentContext
            withdrawDialogViewController.modalTransitionStyle = .crossDissolve
            self.navigationController?.present(withdrawDialogViewController, animated:false,completion: nil)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        emptyStateShadowView.layer.cornerRadius = 2
        emptyStateShadowView.layer.shadowColor = UIColor.black.cgColor
        emptyStateShadowView.alpha = CGFloat(0.26)
        emptyStateShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        emptyStateShadowView.layer.shadowOpacity = 1
        self.view.hideToastActivity()
        
    }
    
    func getbalanceHistory() {
        profileViewModel.getBalanceHistory(accessToken: getAccessToken(),page:currentPage, { (response) in
            self.handleResponse(response: response)
        } , { (error) in
            self.refreshControl?.endRefreshing()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            } 
        })
    }
    
    func handleResponse(response: AFDataResponse<Any>) {
        self.isApiCalled = false
        self.refreshControl?.endRefreshing()
        if response.response?.statusCode == 200 {
            let data: ApiResponse<BalanceHistory> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            } else {
                let userdata = data.data
                if (userdata?.items?.count)! < 1{
                    self.emptyStateShadowView.isHidden = false
                    self.emptyStateView.isHidden = false
                    self.balanceHistoryTableView.isHidden = true
                }else{
                    self.emptyStateShadowView.isHidden = true
                    self.emptyStateView.isHidden = true
                    self.balanceHistoryTableView.isHidden = false
                }
                if let users = userdata?.items {
                    if self.currentPage == 1 {
                        self.balanceHistoryHandler.balanceHistory = users
                    } else {
                        self.balanceHistoryHandler.balanceHistory.append(contentsOf: users)
                    }
                    if userdata?.totalPages == currentPage {
                        self.isApiCalled = true
                        self.balanceHistoryHandler.isLoadMoreRequired = false
                    }
                    self.balanceHistoryTableView.reloadData()
                    self.screenActivityIndicator.stopAnimating()
                    self.balanceHistoryTableView.isHidden = false
                }
            }
            self.currentPage = self.currentPage + 1
        }else if response.response?.statusCode == 404{
            self.emptyStateShadowView.isHidden = false
            self.emptyStateView.isHidden = false
            self.balanceHistoryTableView.isHidden = true
        } else {
            self.emptyStateShadowView.isHidden = true
            self.emptyStateView.isHidden = true
            self.balanceHistoryTableView.isHidden = false
            self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
        }
    }
    
    func addRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        balanceHistoryTableView.addSubview(refreshControl!)
    }
    
    @objc func refreshList(){
        currentPage = 1
        isApiCalled = false
        self.balanceHistoryHandler.isLoadMoreRequired  = true
        getbalanceHistory()
    }

}
