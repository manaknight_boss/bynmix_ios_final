import UIKit

class BankDetailViewController: BaseViewController,UploadBankDetailsProtocol {
    func uploadBankDetails(type: Int, accountINfo: AccountInfo) {
        if type == AppConstants.TYPE_BANK_DETAIL{
            self.uploadBankDetails(accountInfo:accountINfo)
        }
    }
    
    @IBOutlet var currentBalanceLabel: UILabel!
    @IBOutlet var bankAccountNickNameTextField: UITextField!
    @IBOutlet var fullNameTextField: UITextField!
    @IBOutlet var accountNumberTextField: UITextField!
    @IBOutlet var bankAccountRoutingNumberTextField: UITextField!
    @IBOutlet var withdrawAmountTextField: UITextField!
    @IBOutlet var withdrawButton: UIButton!
    @IBOutlet var buttonActivityActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var nickNameErrorButton: UIButton!
    @IBOutlet var fullNameErrorButton: UIButton!
    @IBOutlet var accountNumberErrorButton: UIButton!
    @IBOutlet var routingErrorButton: UIButton!
    @IBOutlet var amountErrorButton: UIButton!
    @IBOutlet var topConstraint: NSLayoutConstraint!
    @IBOutlet var payoutInProgressView: UIView!
    @IBOutlet var warningLabel: UILabel!
    @IBOutlet var warningImageView:UIImageView!
    
    var currentBalance:Int?
    let profileViewModel = ProfileViewModel()
    var sales:Sales?
    var TYPE:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.Strings.WITHDRAW_FUNDS_TITLE
        bankAccountNickNameTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        fullNameTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        accountNumberTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        bankAccountRoutingNumberTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        withdrawAmountTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        self.currentBalanceLabel.text = String(format: "%.2f", Double(self.currentBalance ?? 0))
        self.buttonActivityActivityIndicator.isHidden = true
        hideKeyboardWhenTappedAround()
        keyboardRequired(topConstraint: topConstraint)
        if (sales?.hasPayoutInProgress!)!{
            payoutInProgressView.isHidden = false
            setWarningLabelText(sales:sales!)
        }else{
            payoutInProgressView.isHidden = true
        }
        warningImageView.contentMode = .scaleAspectFit
    }
    
    func setWarningLabelText(sales:Sales){
        let text:String = sales.payoutProgressMessage!
        let textColor:String = AppConstants.Strings.NOTE_TEXT
        let range = (text as NSString).range(of: textColor)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 10), range: range)
        warningLabel.attributedText = attributedString
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.view.hideToastActivity()
    }
    
    @objc private func textFieldDidChange(textField: UITextField){
        switch textField {
        case bankAccountNickNameTextField:
            nickNameErrorButton.isHidden = true
        case fullNameTextField:
            fullNameErrorButton.isHidden = true
        case accountNumberTextField:
            accountNumberErrorButton.isHidden = true
        case bankAccountRoutingNumberTextField:
            routingErrorButton.isHidden = true
        case withdrawAmountTextField:
            amountErrorButton.isHidden = true
        default:
            textField.isHidden = true
        }
        hideToolTip()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case bankAccountNickNameTextField:
            fullNameTextField.becomeFirstResponder()
        case fullNameTextField:
            accountNumberTextField.becomeFirstResponder()
        case accountNumberTextField:
            bankAccountRoutingNumberTextField.becomeFirstResponder()
        case bankAccountRoutingNumberTextField:
            withdrawAmountTextField.becomeFirstResponder()
        default:
            textField.resignFirstResponder()
        }
        return false
    }
    
    @IBAction func bankAccountNickNameErrorButton(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_BANK_NICK_NAME, view: sender)
    }
    
    @IBAction func fullNameErrorButton(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_NAME_ASSOCIATED, view: sender)
    }
    
    @IBAction func accountNumberErrorButton(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_BANK_ACCOUNT, view: sender)
    }
    
    @IBAction func routingNumberErrorButton(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_ROUTING_NUMBER, view: sender)
    }
    
    @IBAction func withdrawAmountErrorButton(_ sender: UIButton) {
        showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_WITHDRAW_AMOUNT, view: sender)
    }
    
    @IBAction func withdrawButtonAction(_ sender: UIButton) {
        let accountNickName = bankAccountNickNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let fullName = fullNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let bankAccountNumber = accountNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let routingNumber = bankAccountRoutingNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let amount = withdrawAmountTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        
        var isError = false
        if !ValidationHelper.isEmptyAny(field: accountNickName!) {
            nickNameErrorButton.isHidden = false
            isError = true
        } else {
            nickNameErrorButton.isHidden = true
        }
        if !ValidationHelper.isEmptyAny(field: fullName!) {
            fullNameErrorButton.isHidden = false
            isError = true
        } else {
            fullNameErrorButton.isHidden = true
        }
        if !ValidationHelper.isEmptyAny(field: bankAccountNumber!) {
            accountNumberErrorButton.isHidden = false
            isError = true
        } else {
            accountNumberErrorButton.isHidden = true
        }
        if !ValidationHelper.isEmptyAny(field: routingNumber!) {
            routingErrorButton.isHidden = false
            isError = true
        } else {
            routingErrorButton.isHidden = true
        }
        if !ValidationHelper.isEmptyAny(field: amount!) {
            amountErrorButton.isHidden = false
            isError = true
        } else {
            amountErrorButton.isHidden = true
        }
        hideToolTip()
        if isError {
            return
        }
        let withdrawAmount:Double = Double(amount ?? "")!
        let accountDetails = AccountInfo(bankNickName: accountNickName!, routingNumber: routingNumber!, accountNumber: bankAccountNumber!, nameOnBankAccount: fullName!, withdrawAmount: withdrawAmount)
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.WITHDRAW_DIALOG_BOARD, bundle:nil)
        let withdrawDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.WITHDRAW_DIALOG) as! WithdrawDialogViewController
        withdrawDialogViewController.TYPE = AppConstants.TYPE_BANK_DETAIL
        withdrawDialogViewController.accountInfo = accountDetails
        withdrawDialogViewController.modalPresentationStyle = .overCurrentContext
        withdrawDialogViewController.UploadBankDetailsDelegate = self
        self.navigationController?.present(withdrawDialogViewController, animated:false,completion: nil)
        
    }
    
    func uploadBankDetails(accountInfo:AccountInfo) {
        self.dismiss(animated: false, completion: nil)
        self.view.makeToastActivity(.center)
        self.withdrawButton.isEnabled = false
        let amount = AccountInfo(bankNickName: accountInfo.bankNickName!, routingNumber: accountInfo.routingNumber!, accountNumber: accountInfo.accountNumber!, nameOnBankAccount: accountInfo.nameOnBankAccount!, withdrawAmount: accountInfo.withdrawAmount!)
        profileViewModel.uploadBankDetails(accessToken: getAccessToken(), data: amount,{(response) in
            self.withdrawButton.isEnabled = true
            self.view.hideToastActivity()
            if response.response?.statusCode == 200 || response.response?.statusCode == 201 {
                let data: ApiResponse<AccountInfo> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    self.view.hideToastActivity()
                    let accountInfo = data.data!
                    
                    if self.TYPE == AppConstants.TYPE_NO_BANK_HISTORY{
                        var currentVCStack = self.navigationController?.viewControllers
                        currentVCStack?.removeLast()

                        let balanceHistoryViewControler = UIStoryboard(name: AppConstants.Storyboard.BALANCE_HISTORY_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.BALANCE_HISTORY_SCREEN) as! BalanceHistoryViewController
                        balanceHistoryViewControler.accountInfo = accountInfo
                        currentVCStack?.append(balanceHistoryViewControler)
                        self.navigationController?.setViewControllers(currentVCStack!, animated: true)
                    }else{
                        var currentVCStack = self.navigationController?.viewControllers
                        currentVCStack?.removeLast(2)

                        let balanceHistoryViewControler = UIStoryboard(name: AppConstants.Storyboard.BALANCE_HISTORY_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.BALANCE_HISTORY_SCREEN) as! BalanceHistoryViewController
                        balanceHistoryViewControler.accountInfo = accountInfo
                        currentVCStack?.append(balanceHistoryViewControler)
                        self.navigationController?.setViewControllers(currentVCStack!, animated: true)
                    }
                    
                    let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.SUCCESS_MESSAGE_DIALOG_BOARD, bundle:nil)
                    let withdrawDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.SUCCESS_MESSAGE_DIALOG) as! SuccessMessageViewController
                    withdrawDialogViewController.accountInfo = accountInfo
                    withdrawDialogViewController.modalPresentationStyle = .overCurrentContext
                    withdrawDialogViewController.modalTransitionStyle = .crossDissolve
                    self.navigationController?.present(withdrawDialogViewController, animated:false,completion: nil)
                }
            }else if response.response?.statusCode == 400{
                let data: ApiResponse<AccountInfo> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showFailDialog(message: error)
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            self.withdrawButton.isEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
}
