import UIKit

class MyWalletViewController: BaseViewController,OnDeleteCardClickProtocol,ReplaceCardClickProtocol,SelectCardProtocol {
    
    func selectCard(type:Int,card: Card) {
        if type == AppConstants.TYPE_ADD_NEW_PAYMENT_IN_EXISTING_CARD{
            self.setCardDetails(card:card)
        }else if type == AppConstants.TYPE_ADD_PAYMENT_IN_EXISTING_CARD{
            self.navigationController?.popViewController(animated: false)
            self.setCardDetails(card:card)
        }
    }
    
    func setCardDetails(card:Card){
        setIsDefault(card:card)
        let image = UIImage(named: AppConstants.Images.DEFAULT_CARD_ICON)
        
        self.newPaymentWithExistingPaymentView.isHidden = true
        self.newPaymentButtonView.isHidden = true
        self.defaultPaymentView.isHidden = false
        self.addNewPaymentInDefaultPaymentView.isHidden = true
        var cardText = (card.brand!) + " "
        cardText = cardText + AppConstants.Strings.ENDING_IN + " "
        cardText = cardText + " " +  (card.lastFour!)
        self.cardNameWithNumberLabel.text = cardText
        self.cardholderNameLabel.text = card.nameOnCard
        self.expirationDateLabel.text = AppConstants.EXPIRES + " " + String(card.expirationMonth!) + AppConstants.FORWARD_SLASH + String(card.expirationYear!)
        
        if let imageUrl = card.brandImageUrl {
            print(imageUrl)
            self.brandImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        }
        
        var text:String = (card.brand!) + " "
        text = text + AppConstants.Strings.ENDING_IN + " "
        text = text + (card.lastFour!)
        let textBrand:String = (card.brand!)
        let textLastFour:String = (card.lastFour!)
        let range = (text as NSString).range(of: textLastFour)
        let rangeLastFour = (text as NSString).range(of: textBrand)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: range)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontBold(size: 16), range: rangeLastFour)
        self.cardNameWithNumberLabel.attributedText = attributedString
    }
    
    func replaceCardClick(oldCard: Card, newCard: Card) {
        self.navigationController?.popViewController(animated: true)
        self.replaceCard(oldCardId:oldCard.cardId!,newCardId:newCard.cardId!)
    }
    
    func OnDeleteCardClick(type: Int, card: Card) {
        if type == AppConstants.TYPE_DELETE_CARD{
            self.dismiss(animated: false, completion: nil)
            self.deleteCard(id:card.cardId!)
        }else if type == AppConstants.TYPE_REPLACE_CARD{
            self.dismiss(animated: false, completion: nil)
            let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.REPLACE_CARD_BOARD, bundle:nil)
            let replaceCardViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.REPLACE_CARD_SCREEN) as! ReplaceCardViewController
            replaceCardViewController.oldCard = card
            replaceCardViewController.card = self.allCard
            replaceCardViewController.replaceCardClickProtocol = self
            self.navigationController?.pushViewController(replaceCardViewController, animated:true)
        }
    }
    
    @IBOutlet var currentBalance: UILabel!
    @IBOutlet var withdrawButton: UIButton!
    @IBOutlet var balanceHistoryButton: UIButton!
    @IBOutlet var defaultPaymentView: UIView!
    @IBOutlet var addNewPaymentInDefaultPaymentView: UIView!
    @IBOutlet var brandImageView: UIImageView!
    @IBOutlet var cardNameWithNumberLabel: UILabel!
    @IBOutlet var cardholderNameLabel: UILabel!
    @IBOutlet var expirationDateLabel: UILabel!
    @IBOutlet var otherPaymentMethodTableView: UITableView!
    @IBOutlet var bottomView: UIView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var defaultPaymentInnerView: UIView!
    @IBOutlet var tableViewHeight: NSLayoutConstraint!
    @IBOutlet var newPaymentButtonView: UIView!
    @IBOutlet var newPaymentWithExistingPaymentView: UIView!
    @IBOutlet var selectExistingPaymentButton: UIButton!
    @IBOutlet var accountStatusLabel: UILabel!
    @IBOutlet var accountStatusViewHeight: NSLayoutConstraint!
    @IBOutlet var accountStatusView: UIView!
    
    var profileViewModel = ProfileViewModel()
    var allCard = [Card]()
    let myWalletHandler = MyWalletOtherPaymentHandler()
    var cardId:Int?
    var card:Card?
    var currentBalanceLabel:Int?
    var extraHeight: CGFloat = 70
    var sales:Sales?
    var checkAccountVerify:CheckAccountVerify?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.Strings.MY_WALLET_TITLE
        myWalletHandler.onEditButtonClick = { card in
            let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.ADD_NEW_PAYMENT_BOARD, bundle:nil)
            let editPaymentViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.ADD_NEW_PAYMENT_SCREEN) as! AddNewPaymentViewController
            editPaymentViewController.TYPE = AppConstants.TYPE_EDIT_PAYMENT
            editPaymentViewController.cardId = card.cardId
            self.navigationController?.pushViewController(editPaymentViewController, animated:true)
        }
        
        myWalletHandler.onDeleteButtonClick = { card in
            self.checkIfCardUsed(card:card)
        }
        selectExistingPaymentButton.titleLabel?.textAlignment = .center
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if allCard.count > 0 {
            self.scrollView.isHidden = true
            self.bottomView.isHidden = true
            self.view.makeToastActivity(.center)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getSales()
    }
    
    func checkIfCardUsed(card:Card) {
        self.view.makeToastActivity(.center)
        profileViewModel.checkIfOffersAccepted(accessToken: getAccessToken(), id: card.cardId!, {(response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Bool> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let cardUsed:Bool = data.data!
                    let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.WITHDRAW_DIALOG_BOARD, bundle:nil)
                    let deleteDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.WITHDRAW_DIALOG) as! WithdrawDialogViewController
                    deleteDialogViewController.modalPresentationStyle = .overCurrentContext
                    deleteDialogViewController.modalTransitionStyle = .crossDissolve
                    if cardUsed{
                        deleteDialogViewController.TYPE = AppConstants.TYPE_REPLACE_CARD
                        deleteDialogViewController.card = card
                        deleteDialogViewController.OnDeleteCardClickDelegate = self
                        self.navigationController?.present(deleteDialogViewController, animated:true,completion: nil)
                    }else{
                        deleteDialogViewController.TYPE = AppConstants.TYPE_DELETE_CARD
                        deleteDialogViewController.card = card
                        deleteDialogViewController.OnDeleteCardClickDelegate = self
                        self.navigationController?.present(deleteDialogViewController, animated:true,completion: nil)
                    }
                }
            } else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func deleteCard(id:Int) {
        self.view.makeToastActivity(.center)
        profileViewModel.deleteCard(accessToken: getAccessToken(), id: id, {(response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.myWalletHandler.deleteStatus(id: id)
                    self.otherPaymentMethodTableView.reloadData()
//                    self.resizeMyWalletTableView()
                }
            } else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        resizeMyWalletTableView()
    }
    
    func replaceCard(oldCardId:Int,newCardId:Int) {
        profileViewModel.replaceCard(accessToken: getAccessToken(), oldCardId: oldCardId,newCardId:newCardId, {(response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.myWalletHandler.deleteStatus(id: oldCardId)
                    self.otherPaymentMethodTableView.reloadData()
                    self.resizeMyWalletTableView()
                }
            } else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    private func getSales() {
        profileViewModel.getMyWalletSales(accessToken: getAccessToken(), {(response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Sales> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let sales = data.data!
                    self.sales = sales
                    self.currentBalance.text = String(format: "%.2f", Double(sales.totalAvailableForWithdrawal ?? 0))
                    self.currentBalanceLabel = (sales.totalAvailableForWithdrawal ?? 0)
                    
                    if sales.totalAvailableForWithdrawal == 0{
                        self.withdrawButton.backgroundColor = UIColor.cDCDCDC
                        self.withdrawButton.isEnabled = false
                    }else{
                        self.withdrawButton.backgroundColor = UIColor.cBB189C
                        self.withdrawButton.isEnabled = true
                    }
                    self.getMyCard()
                }
            } else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func resizeMyWalletTableView(){
        if allCard.count == 0{
            tableViewHeight.constant = 12
        }else{
           var tableViewHeight: CGFloat {
               self.otherPaymentMethodTableView.layoutIfNeeded()
               return self.otherPaymentMethodTableView.contentSize.height
           }
           self.tableViewHeight?.constant = tableViewHeight + extraHeight
        }
    }
    
    func getMyCard() {
        profileViewModel.getMyWalletCard(accessToken: AppDefaults.getAccessToken(), { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[Card]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let cards = data.data!
                    self.setCardData(data: cards)
                }
            }else{
                self.screenActivityIndicator.stopAnimating()
                self.view.hideToastActivity()
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func setCardData(data:[Card]? = nil){
        var defaultCard: Card? = nil
        var isDefault : Bool = false
        let cards = data
        
        var otherCards: [Card] = []
        for i in 0..<(cards?.count ?? 0) {
            isDefault = false
            if cards?[i].isDefault ?? false {
                defaultCard = cards?[i]
                isDefault = true
            }
            if !isDefault {
                otherCards.append(cards![i])
            }
        }
        
        if  !isDefault && cards?.count == 0{
            self.defaultPaymentView.isHidden = true
            self.addNewPaymentInDefaultPaymentView.isHidden = false
            self.bottomView.isHidden = true
            
            self.newPaymentButtonView.isHidden = false
            self.newPaymentWithExistingPaymentView.isHidden = true
            self.checkAccountIfVerified()
        }else{
            let image = UIImage(named: AppConstants.Images.DEFAULT_CARD_ICON)
            var isHeightCalculated = false
            if self.allCard.count > 0 {
                isHeightCalculated = true
            }
            self.allCard = cards!
            if !isHeightCalculated && self.allCard.count > 4 {
                self.extraHeight = 72
            } else {
                self.extraHeight = 0
            }
            
            if  defaultCard == nil {
                self.defaultPaymentView.isHidden = true
                self.addNewPaymentInDefaultPaymentView.isHidden = false
                self.newPaymentButtonView.isHidden = true
                self.newPaymentWithExistingPaymentView.isHidden = false
            }else {
                self.cardId = defaultCard?.cardId
                self.defaultPaymentView.isHidden = false
                self.addNewPaymentInDefaultPaymentView.isHidden = true
                var cardText = (defaultCard?.brand!)! + " "
                cardText = cardText + AppConstants.Strings.ENDING_IN + " "
                cardText = cardText + " " +  (defaultCard?.lastFour!)!
                self.cardNameWithNumberLabel.text = cardText
                
                self.cardholderNameLabel.text = defaultCard?.nameOnCard
                self.expirationDateLabel.text = AppConstants.EXPIRES + " " + String(defaultCard!.expirationMonth!) + AppConstants.FORWARD_SLASH + String(defaultCard!.expirationYear!)
                if let imageUrl = defaultCard?.brandImageUrl{
                    print(imageUrl)
                    self.brandImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
                }
                var text:String = (defaultCard?.brand!)! + " "
                text = text + AppConstants.Strings.ENDING_IN + " "
                text = text + (defaultCard?.lastFour!)!
                let textBrand:String = (defaultCard?.brand!)!
                let textLastFour:String = (defaultCard?.lastFour!)!
                let range = (text as NSString).range(of: textLastFour)
                let rangeLastFour = (text as NSString).range(of: textBrand)
                let attributedString = NSMutableAttributedString(string: text)
                attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: range)
                attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontBold(size: 16), range: rangeLastFour)
                
                self.cardNameWithNumberLabel.attributedText = attributedString
            }
            
            if otherCards.count > 0 {
                self.otherPaymentMethodTableView.isHidden = false
            } else {
                self.otherPaymentMethodTableView.isHidden = true
            }
            
            self.otherPaymentMethodTableView.delegate = self.myWalletHandler
            self.otherPaymentMethodTableView.dataSource = self.myWalletHandler
            self.myWalletHandler.card = otherCards
            self.otherPaymentMethodTableView.reloadData()
            self.resizeMyWalletTableView()
            self.checkAccountIfVerified()
        }
    }
    
    func showScreen(){
        self.screenActivityIndicator.stopAnimating()
        self.view.hideToastActivity()
        self.scrollView.isHidden = false
        self.bottomView.isHidden = false
    }
    
    private func getAccountInfo() {
        self.withdrawButton.isEnabled = false
        self.view.makeToastActivity(.center)
        profileViewModel.getAccountInfo(accessToken: getAccessToken(), {(response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[AccountInfo]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let accountInfo = data.data!
                    
                    self.withdrawButton.isEnabled = true
                    if accountInfo.count == 0{
                        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.BANK_DETAIL_BOARD, bundle:nil)
                        let bankDetailViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.BANK_DETAILS_SCREEN) as! BankDetailViewController
                        bankDetailViewController.currentBalance = self.currentBalanceLabel!
                        bankDetailViewController.sales = self.sales
                        bankDetailViewController.TYPE = AppConstants.TYPE_NO_BANK_HISTORY
                        self.navigationController?.pushViewController(bankDetailViewController, animated:true)
                    }else{
                        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.WITHDRAW_BOARD, bundle:nil)
                        let withdrawViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.WITHDRAW_SCREEN) as! WithdrawViewController
                        withdrawViewController.currentBalance = Int(self.currentBalanceLabel!)
                        withdrawViewController.accountInfo = accountInfo
                        withdrawViewController.sales = self.sales
                        self.navigationController?.pushViewController(withdrawViewController, animated:true)
                    }
                }
            } else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
                self.withdrawButton.isEnabled = true
            }
        } , { (error) in
            self.view.hideToastActivity()
            self.withdrawButton.isEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func setIsDefault(card:Card) {
        var cardAdd = card
        cardAdd.isDefault = true
        let jsonEncoder = JSONEncoder()
        let jsonData = try? jsonEncoder.encode(cardAdd)
        let json = String(data: jsonData!, encoding: String.Encoding.utf8)
        print(json ?? "")
        profileViewModel.editPayment(accessToken: getAccessToken(), id: cardAdd.cardId!, data: cardAdd,{(response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.getMyCard()
                }
            }
            
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    @IBAction func editDefaultPaymentMethodButton(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.ADD_NEW_PAYMENT_BOARD, bundle:nil)
        let editPaymentViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.ADD_NEW_PAYMENT_SCREEN) as! AddNewPaymentViewController
        editPaymentViewController.TYPE = AppConstants.TYPE_EDIT_PAYMENT
        editPaymentViewController.cardId = self.cardId
        self.navigationController?.pushViewController(editPaymentViewController, animated:true)
    }
    
    @IBAction func addNewPaymentButtonInDefaultButtonAction(_ sender: UIButton) {
        addNewPaymentScreen()
    }
    
    @IBAction func addNewpaymentButtonAction(_ sender: UIButton) {
      addNewPaymentScreen()
    }
    
    func addNewPaymentScreen(){
      pushViewController(storyboardName: AppConstants.Storyboard.ADD_NEW_PAYMENT_BOARD, viewControllerIdentifier: AppConstants.Controllers.ADD_NEW_PAYMENT_SCREEN)
    }
    
    @IBAction func withdrawButtonAction(_ sender: UIButton) {
        if self.checkAccountVerify != nil{
            if (self.checkAccountVerify?.isVerified ?? false) && !(self.checkAccountVerify?.isPending ?? false){
                self.getAccountInfo()
            }else if !(self.checkAccountVerify?.isVerified ?? false) && !(self.checkAccountVerify?.isPending ?? false){
                self.showDisabledReasons()
            }
        }else{
            self.getAccountInfo()
        }
    }
    
    @IBAction func balanceHistoryAction(_ sender: UIButton) {
        pushViewController(storyboardName: AppConstants.Storyboard.BALANCE_HISTORY_BOARD, viewControllerIdentifier: AppConstants.Controllers.BALANCE_HISTORY_SCREEN)
    }
    
    @IBAction func addNewPaymentButtonWithExistingPaymentAction(_ sender: UIButton) {
        addNewPaymentScreen()
    }
    
    @IBAction func selectExistingPayment(_ sender: UIButton) {
        selectCard()
    }
    
    func verifyAccountScreen(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.VERIFY_ACCOUNT_STORYBOARD, bundle: nil)
        let verifyAccountViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.VERIFY_ACCOUNT_SCREEN) as! VerifyAccountViewController
        self.navigationController?.pushViewController(verifyAccountViewController, animated: false)
    }
    
    func verifyAccountDocumentsScreen(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.VERIFY_ACCOUNT_DOCUMENT_STORYBOARD, bundle: nil)
        let verifyAccountDocumentsViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.VERIFY_ACCOUNT_DOCUMENTS_SCREEN) as! VerifyAccountDocumentsViewController
        self.navigationController?.pushViewController(verifyAccountDocumentsViewController, animated: false)
    }
    
    func selectCard(){
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.SELECT_EXISTING_CARD_BOARD, bundle:nil)
        let selectCardViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.SELECT_EXISTING_CARD_SCREEN) as! SelectExistingCardViewController
        selectCardViewController.card = self.allCard
        selectCardViewController.selectCardDelegate = self
        self.navigationController?.pushViewController(selectCardViewController, animated:true)
    }
    
    private func checkAccountIfVerified() {
        profileViewModel.checkIfAccountVerified(accessToken: getAccessToken(), {(response) in
            if response.response?.statusCode == 201 {
                let data: ApiResponse<CheckAccountVerify> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let verify = data.data!
                    self.checkAccountVerify = verify
                    if (verify.isVerified ?? false) && !(verify.isPending ?? false){
                        self.accountStatusLabel.text = AppConstants.Strings.VERIFIED_TEXT
                        self.withdrawButton.setTitle(AppConstants.Strings.WITHDRAW_TEXT, for: .normal)
                        self.withdrawButton.backgroundColor = UIColor.cBB189C
                        self.withdrawButton.isEnabled = true
                    }else if (verify.isPending ?? false){
                        self.accountStatusLabel.text = AppConstants.Strings.PENDING_TEXT
                        self.withdrawButton.setTitle(AppConstants.Strings.WITHDRAW_PENDING_UNTIL_VERIFICATION, for: .normal)
                        self.withdrawButton.backgroundColor = UIColor.cCBCBCB
                        self.withdrawButton.isEnabled = false
                    }else if !(verify.isVerified ?? false) && !(verify.isPending ?? false){
                        self.accountStatusLabel.text = AppConstants.Strings.UNVERIFIED_TEXT
                        self.withdrawButton.setTitle(AppConstants.Strings.ADDITIONAL_INFO_NEEDED_TO_VERIFY_ACCOUNT, for: .normal)
                        self.withdrawButton.backgroundColor = UIColor.cBB189C
                        self.withdrawButton.isEnabled = true
                    }
                    self.withdrawButton.titleLabel?.textAlignment = .center
                    self.accountStatusView.isHidden = false
                    self.accountStatusViewHeight.constant = 40
                    self.showScreen()
                }
            } else if response.response?.statusCode == 404{
                self.accountStatusView.isHidden = true
                self.accountStatusViewHeight.constant = 0
                self.view.hideToastActivity()
                self.screenActivityIndicator.stopAnimating()
                self.scrollView.isHidden = false
                if self.allCard.count > 0 {
                    self.bottomView.isHidden = false
                }else{
                    self.bottomView.isHidden = true
                }
            } else {
                self.view.hideToastActivity()
                self.screenActivityIndicator.stopAnimating()
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.view.hideToastActivity()
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func showDisabledReasons(){
        
        let reasons =  (self.checkAccountVerify?.disabledReasons)
        
        for i in 0..<reasons!.count{
            if(reasons?[i].requiredField?.caseInsensitiveCompare("SSN") == ComparisonResult.orderedSame) || (reasons?[i].requiredField?.caseInsensitiveCompare("Phone") == ComparisonResult.orderedSame){
                self.verifyAccountScreen()
                break
            }else if (reasons?[i].requiredField?.caseInsensitiveCompare("Document") == ComparisonResult.orderedSame){
                self.verifyAccountDocumentsScreen()
                break
            }
        }
    }
    
}
