import UIKit

class ReplaceCardViewController: BaseViewController {

    @IBOutlet var cardsTableView: UITableView!
    var profileViewModel = ProfileViewModel()
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var deleteButton: UIButton!
    
    var card:[Card] = []
    var replaceHandler = ReplaceCardHandler()
    var replaceCardClickProtocol:ReplaceCardClickProtocol?
    var selectCardDelegate:SelectCardProtocol?
    var oldCard:Card?
    var newCard:Card?
    var TYPE:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var cards:[Card] = []
        for i in 0..<self.card.count {
            if self.card[i].cardId != oldCard?.cardId {
                cards.append(card[i])
            }
        }
        replaceHandler.card = cards
        cardsTableView.delegate = replaceHandler
        cardsTableView.dataSource = replaceHandler
        screenActivityIndicator.stopAnimating()
        cardsTableView.isHidden = false
        replaceHandler.onSelectButtonClick = { card in
            self.deleteButton.isEnabled = true
            self.deleteButton.backgroundColor = UIColor.cBB189C
            self.newCard = card
        }
        if TYPE == AppConstants.TYPE_SELECT_EXISTING_PAYMENT{
            navigationItem.title = AppConstants.Strings.SELECT_EXISTING_PAYMENT
        }else{
            navigationItem.title = AppConstants.Strings.SELECT_NEW_PAYMENT
        }
        if TYPE == AppConstants.TYPE_SELECT_EXISTING_PAYMENT{
            deleteButton.setTitle(AppConstants.Strings.REPLACE_CARD, for: .normal)
        }
        
        if TYPE == AppConstants.TYPE_SELECT_EXISTING_CARD{
            deleteButton.setTitle(AppConstants.Strings.SELECT_CARD, for: .normal)
            navigationItem.title = AppConstants.Strings.SELECT_EXISTING_PAYMENT
        }
    }
    
    @IBAction func deleteButtonAction(_ sender: UIButton) {
        if TYPE == AppConstants.TYPE_SELECT_EXISTING_PAYMENT{
          self.replaceCardClickProtocol?.replaceCardClick(oldCard: self.newCard!, newCard: self.newCard!)
        }else if TYPE == AppConstants.TYPE_SELECT_EXISTING_CARD{
            self.selectCardDelegate?.selectCard(type: AppConstants.TYPE_SELECT_EXISTING_CARD, card: self.newCard!)
        }else{
            self.replaceCardClickProtocol?.replaceCardClick(oldCard: self.oldCard!, newCard: self.newCard!)
        }
    }
    
}
