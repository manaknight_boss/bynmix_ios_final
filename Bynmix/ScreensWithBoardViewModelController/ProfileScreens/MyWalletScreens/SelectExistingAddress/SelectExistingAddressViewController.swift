import UIKit

class SelectExistingAddressViewController: BaseViewController,AddNewAddressInPaymentProtocol {
    
    func AddNewAddressInPayment(type: Int, address: Address) {
        if type == AppConstants.TYPE_ADD_NEW_ADDRESS_IN_PAYMENT{
            self.selectExistingAddressDelegate?.SelectExistingAddress(type: AppConstants.TYPE_ADD_NEW_ADDRESS_IN_PAYMENT, address: address)
            self.navigationController?.popViewController(animated: false)
        }else if type == AppConstants.TYPE_SELECT_DIFFERENT_ADDRESS{
            self.selectExistingAddressDelegate?.SelectExistingAddress(type: AppConstants.TYPE_ADD_NEW_ADDRESS_IN_PAYMENT, address: address)
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    @IBOutlet var addressesTableView: UITableView!
    var profileViewModel = ProfileViewModel()
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    let selectExistingAddressHandler = SelectExistingAddressHandler()
    var allAddresses: [Address] = []
    var address: Address?
    var selectExistingAddressDelegate:SelectExistingAddressProtocol?
    var TYPE:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addressesTableView.delegate = selectExistingAddressHandler
        addressesTableView.dataSource = selectExistingAddressHandler
        addressesTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 92, right: 0)
        selectExistingAddressHandler.onAddressAddButtonClick = { address in
            self.selectExistingAddressDelegate?.SelectExistingAddress(type: AppConstants.TYPE_SELECT_EXISTING_ADDRESS, address: address)
            self.selectExistingAddressDelegate?.SelectExistingAddress(type: AppConstants.TYPE_CHANGE_DEFAULT_ADDRESS, address: address)
            self.navigationController?.popViewController(animated: false)
        }
        if TYPE == AppConstants.TYPE_CHANGE_DEFAULT_ADDRESS{
            navigationItem.title = AppConstants.Strings.CHANGE_DEFAULT_ADDRESS
        }else if TYPE == AppConstants.TYPE_SELECT_DIFFERENT_ADDRESS{
            navigationItem.title = AppConstants.Strings.SELECT_DIFFERENT_ADDRESS
        }else{
            navigationItem.title = AppConstants.Strings.SELECT_AN_EXISTING_ADDRESS
        }
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if allAddresses.count > 0 {
            self.view.makeToastActivity(.center)
        }
        getAddresses()
    }
    
    func getAddresses() {
        profileViewModel.getAddresses(accessToken: AppDefaults.getAccessToken(), { (response) in
            self.view.hideToastActivity()
            self.screenActivityIndicator.stopAnimating()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[Address]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    var returnAddress: Address? = nil
                    var shippingAddress: Address? = nil
                    let addresses = data.data! as [Address]
                    self.allAddresses = addresses
                    
                    var otherAddresses: [Address] = []
                    for i in 0..<addresses.count {
                        var isDefault : Bool = false
                        if addresses[i].isShippingAddress! {
                            shippingAddress = addresses[i]
                            isDefault = true
                        }
                        
                        if addresses[i].isReturnAddress! {
                            returnAddress = addresses[i]
                            isDefault = true
                        }
                        
                        if !isDefault {
                            otherAddresses.append(addresses[i])
                        }
                    }
                    
                    self.selectExistingAddressHandler.shippingAddress = shippingAddress
                    self.selectExistingAddressHandler.returnAddress = returnAddress
                    self.selectExistingAddressHandler.otherAddresses = otherAddresses
                    self.addressesTableView.reloadData()
                    self.addressesTableView.isHidden = false
                    
                }
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
   
    @IBAction func addNewAddressAction(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.ADD_NEW_ADDRESS_BOARD, bundle:nil)
        let newAddressViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.ADD_NEW_ADDRESS_SCREEN) as! AddNewAddressViewController
        newAddressViewController.TYPE = self.TYPE ?? 0
        newAddressViewController.addNewAddressInPaymentDelegate = self
        self.navigationController?.pushViewController(newAddressViewController, animated:true)
    }
    
}
