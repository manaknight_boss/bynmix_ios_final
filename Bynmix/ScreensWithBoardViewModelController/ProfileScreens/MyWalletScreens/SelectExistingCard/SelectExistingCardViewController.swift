import UIKit

class SelectExistingCardViewController: BaseViewController,AddNewPaymentProtocol {
    
    func addNewPayment(type: Int, card: Card) {
        if type == AppConstants.TYPE_ADD_NEW_PAYMENT_IN_EXISTING_CARD{
            self.selectCardDelegate?.selectCard(type:AppConstants.TYPE_ADD_NEW_PAYMENT_IN_EXISTING_CARD,card: card)
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    @IBOutlet var cardsTableView: UITableView!
    var profileViewModel = ProfileViewModel()
    @IBOutlet var defaultCardView: UIView!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var cardImageView: UIImageView!
    @IBOutlet var cardNameLabel: UILabel!
    @IBOutlet var cardHolderNameLabel: UILabel!
    @IBOutlet var cardExpiryLabel: UILabel!
    @IBOutlet var cardSelectButton: UIButton!
    
    @IBOutlet weak var headerHeight: NSLayoutConstraint!
    
    let selectExistingCardHandler = SelectExistingCardHandler()
    var card:[Card] = []
    var selectCardDelegate:SelectCardProtocol?
    var defaultSelectedCard:Card?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.Strings.SELECT_EXISTING_PAYMENT
        cardsTableView.delegate = selectExistingCardHandler
        cardsTableView.dataSource = selectExistingCardHandler
        
        cardsTableView.isHidden = true
        defaultCardView.isHidden = true
        seperateDefaultCard()
        onCardClick()
    }
    
    func onCardClick(){
        selectExistingCardHandler.onAddressClick = { card in
            self.selectCardDelegate?.selectCard(type: AppConstants.TYPE_ADD_PAYMENT_IN_EXISTING_CARD, card: card)
        }
    }
    
    func seperateDefaultCard(){
        var defaultCard:Card?
        var otherCards:[Card] = []
        
        for i in 0..<card.count{
            if card[i].isDefault ?? false{
                defaultCard = card[i]
            }else{
                otherCards.append(card[i])
            }
        }
        
        let stripeCardId = defaultCard?.stripeCardId!
        let addressId = defaultCard?.addressId!
        
        defaultCard?.sourceToken = stripeCardId!
        defaultCard?.isNewAddress = false
        defaultCard?.settingAddressId = addressId!
        
        self.defaultSelectedCard = defaultCard
        
        let image = UIImage(named: AppConstants.Images.DEFAULT_CARD_ICON)
        
        if let imageUrl = defaultCard?.brandImageUrl{
            cardImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        }
        
        cardHolderNameLabel.text = defaultCard?.nameOnCard
        
        let expiryMonth = String(defaultCard?.expirationMonth ?? 0)
        let expiryYear = String(defaultCard?.expirationYear ?? 0)
        
        cardExpiryLabel.text = AppConstants.EXPIRES + " " + expiryMonth + AppConstants.FORWARD_SLASH + expiryYear
        
         let brand = (defaultCard?.brand ?? "")
         let lastFour = (defaultCard?.lastFour ?? "")
        
        let text:String = brand + " " + AppConstants.Strings.ENDING_IN + " " + lastFour
        let textBrand:String = brand
        let textLastFour:String = lastFour
        let range = (text as NSString).range(of: textLastFour)
        let rangeLastFour = (text as NSString).range(of: textBrand)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: range)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontBold(size: 16), range: rangeLastFour)
        
        cardNameLabel.attributedText = attributedString
        
        if defaultCard == nil{
            defaultCardView.isHidden = true
            headerHeight.constant = 0
            cardsTableView.contentInset = UIEdgeInsets(top: -165, left: 0, bottom: 0, right: 0)
        }else{
            defaultCardView.isHidden = false
            headerHeight.constant = 165
            cardsTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
        
        if card.count > 0{
            selectExistingCardHandler.card = otherCards
            selectExistingCardHandler.defaultCard = defaultCard
            cardsTableView.reloadData()
            cardsTableView.isHidden = false
        }else{
            cardsTableView.isHidden = true
        }
    
        screenActivityIndicator.stopAnimating()
    }
    
    @IBAction func cardSelectButtonAction(_ sender: UIButton) {
        self.selectCardDelegate?.selectCard(type: AppConstants.TYPE_ADD_PAYMENT_IN_EXISTING_CARD, card: self.defaultSelectedCard!)
    }
    
    @IBAction func addNewPaymentButtonAction(_ sender: UIButton) {
        addPayment()
    }
    
    func addPayment(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.ADD_NEW_PAYMENT_BOARD, bundle: nil)
        let  addNewPaymentViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.ADD_NEW_PAYMENT_SCREEN) as! AddNewPaymentViewController
        addNewPaymentViewController.addNewPaymentDelegate = self
        addNewPaymentViewController.TYPE = AppConstants.TYPE_ADD_NEW_PAYMENT_IN_EXISTING_CARD
        navigationController?.pushViewController(addNewPaymentViewController, animated: true)
    }
    
}
