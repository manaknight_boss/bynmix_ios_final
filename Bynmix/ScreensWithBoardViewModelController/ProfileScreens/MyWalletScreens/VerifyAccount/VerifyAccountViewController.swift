import UIKit

class VerifyAccountViewController: BaseViewController {
    
    @IBOutlet var socialSecurityTextField: UITextField!
    @IBOutlet var phoneNumberTextField: UITextField!
    @IBOutlet var updateButton: UIButton!
    @IBOutlet var buttonActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var socialSecurityErrorButton: UIButton!
    @IBOutlet var phoneNumberErrorButton: UIButton!
    @IBOutlet var topConstraint: NSLayoutConstraint!
    
    @IBOutlet var ssnView: UIView!
    @IBOutlet var phoneView: UIView!

    let profileViewModel = ProfileViewModel()
    
    var ifBackspacePressed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = AppConstants.Strings.VERIFY_ACCOUNT_TITLE
        buttonActivityIndicator.isHidden = true
        self.socialSecurityTextField.addTarget(self, action: #selector(didChangeText(textField:)), for: .editingChanged)
        self.phoneNumberTextField.addTarget(self, action: #selector(didChangeTextInPhoneNumber(textField:)), for: .editingChanged)
        hideKeyboardWhenTappedAround()
        keyboardRequired(topConstraint: topConstraint)
        socialSecurityTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        phoneNumberTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newLength = (textField.text ?? "").count + string.count - range.length
        
        if string.count == 0 {
            self.ifBackspacePressed = true
        }else{
            self.ifBackspacePressed = false
        }
        
        if(textField == socialSecurityTextField) {
            return newLength <= 11
        }else if (textField == phoneNumberTextField){
            return newLength <= 14
        }
        return true
    }
    
    @objc func didChangeText(textField:UITextField) {
        let ssnTextField = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        
        if !ifBackspacePressed{
            if ssnTextField.count == 3{
                self.socialSecurityTextField.text = ssnTextField + "-"
            }else if ssnTextField.count == 6{
                self.socialSecurityTextField.text = ssnTextField + "-"
            }
        }
    }
    
    @objc func didChangeTextInPhoneNumber(textField:UITextField) {
        let phoneTextField = textField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        
        if !ifBackspacePressed{
            if phoneTextField.count == 1{
                self.phoneNumberTextField.text =  "(" + phoneTextField
            }else if phoneTextField.count == 4{
                self.phoneNumberTextField.text = phoneTextField + ")-"
            }else if phoneTextField.count == 9{
                self.phoneNumberTextField.text = phoneTextField + "-"
            }
        }
    }
    
    @IBAction func updateButtonAction(_ sender: UIButton) {
        let socialSecurityText = self.socialSecurityTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        let phoneText = self.phoneNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        
        var isError = false
        
        if !ValidationHelper.isEmptyAny(field: socialSecurityText) {
            socialSecurityErrorButton.isHidden = false
            isError = true
        }else if socialSecurityText.count < 11{
            socialSecurityErrorButton.isHidden = false
            isError = true
        } else {
            socialSecurityErrorButton.isHidden = true
        }
        if !ValidationHelper.isEmptyAny(field: phoneText) {
            phoneNumberErrorButton.isHidden = false
            isError = true
        }else if phoneText.count < 14{
            phoneNumberErrorButton.isHidden = false
            isError = true
        }else {
            phoneNumberErrorButton.isHidden = true
        }
        
        hideToolTip()
        if isError {
            return
        }
        
        self.buttonActivityIndicatorStart()
        sender.isEnabled = false
        self.updateUserInfo()
        
    }
    
    func buttonActivityIndicatorStart(){
        self.buttonActivityIndicator.isHidden = false
        self.buttonActivityIndicator.startAnimating()
        self.buttonActivityIndicator.hidesWhenStopped = true
    }
    
    @IBAction func socialSecurityErrorButtonAction(_ sender: UIButton) {
        let socialSecurityText = self.socialSecurityTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        
        if socialSecurityText.count > 0{
            self.showErrorTooltip(errorMessage: AppConstants.Strings.SOCIAL_SECURITY_NUMBER_NOT_CORRECT, view: sender)
        }else{
            self.showErrorTooltip(errorMessage: AppConstants.Strings.SOCIAL_SECURITY_NUMBER_EMPTY, view: sender)
        }
        
    }
    
    @IBAction func phoneNumberErrorButtonAction(_ sender: UIButton) {
        let phoneText = self.phoneNumberTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        
        if phoneText.count > 0{
            self.showErrorTooltip(errorMessage: AppConstants.Strings.PHONE_NUMBER_NOT_CORRECT, view: sender)
        }else{
            self.showErrorTooltip(errorMessage: AppConstants.Strings.PHONE_NUMBER_EMPTY, view: sender)
        }
    }
    
    @objc private func textFieldDidChange(textField: UITextField){
        switch textField {
        case socialSecurityTextField:
            socialSecurityErrorButton.isHidden = true
        case phoneNumberTextField:
            phoneNumberErrorButton.isHidden = true
        default:
            textField.isHidden = true
        }
        hideToolTip()
    }
    
    private func updateUserInfo() {
        var data : AccountVerify? = AccountVerify()
        
        let ssnString = self.socialSecurityTextField.text ?? ""
        if let number = Int(ssnString.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
            if number == 0{
                data?.ssn = "000000000"
            }else{
                data?.ssn = String(number)
            }
        }
        let phoneString = self.phoneNumberTextField.text ?? ""
        if let number = Int(phoneString.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()) {
            if number == 0{
                data?.phoneNumber = "0000000000"
            }else{
                data?.phoneNumber = String(number)
            }
        }
        self.uploadUserData(data:data!)
        
    }
    
    func uploadUserData(data:AccountVerify){
        profileViewModel.updateUserInfo(accessToken: getAccessToken(),data: data ,{(response) in
            if response.response?.statusCode == 201 {
                self.updateButton.isEnabled = true
                self.buttonActivityIndicator.stopAnimating()
                self.navigationController?.popViewController(animated: false)
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.buttonActivityIndicator.stopAnimating()
                self.updateButton.isEnabled = true
            }
        } , { (error) in
            self.updateButton.isEnabled = true
            self.buttonActivityIndicator.stopAnimating()
            self.scrollView.isHidden = false
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
}
