import UIKit

class VerifyAccountDocumentsViewController: BaseViewController {
    
    @IBOutlet var documentTypeLabel: UILabel!
    @IBOutlet var frontPhotoImageView: UIImageView!
    @IBOutlet var backPhotoImageView: UIImageView!
    @IBOutlet var documentTypeView: UIView!
    @IBOutlet var updateButton: UIButton!
    @IBOutlet var buttonActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var topConstraint: NSLayoutConstraint!
    @IBOutlet var noteLabel: UILabel!
    @IBOutlet var frontPhotoRemoveButton: UIButton!
    @IBOutlet var backPhotoRemoveButton: UIButton!
    
    let imagePickerManager = ImagePickerManager()
    
    let TYPE_DOCUMENT: Int = 1
    var documentTypeSelect = [DocumentType]()
    var selectedDocumentType = 0
    let profileViewModel = ProfileViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        documentTypeLabel.text = AppConstants.Strings.SELECT_DOCUMENT_TYPE_TEXT
        navigationItem.title = AppConstants.Strings.VERIFY_ACCOUNT_TITLE
        buttonActivityIndicator.isHidden = true
        hideKeyboardWhenTappedAround()
        keyboardRequired(topConstraint: topConstraint)
        self.frontPhotoImageView.image = UIImage(named: AppConstants.Images.FRONT_PHOTO_ID_ICON)
        self.backPhotoImageView.image = UIImage(named: AppConstants.Images.BACK_PHOTO_ID_ICON)
        setNoteText()
        getDocumentType()
    }
    
    func setNoteText(){
        let text = AppConstants.Strings.PLEASE_NOTE
        let textBold = AppConstants.Strings.PLEASE_NOTE_TEXT
        let range = (text as NSString).range(of: textBold)
        let attributedText = NSMutableAttributedString(string: text)
        attributedText.addAttribute(NSAttributedString.Key.font,value:UIFont.fontBold(size: 12) ,range: range)
        noteLabel.attributedText = attributedText
    }
    
    func showScreen(){
        self.screenActivityIndicator.stopAnimating()
        self.scrollView.isHidden = false
    }
    
    @IBAction func frontPhotoTapAction(_ sender: UITapGestureRecognizer) {
        self.getImageFrontPhotoId()
    }
    
    @IBAction func backPhotoTapAction(_ sender: UITapGestureRecognizer) {
        self.getImageBackPhotoId()
    }
    
    @IBAction func removeFrontPhotoAction(_ sender: UIButton) {
        self.frontPhotoRemoveButton.isHidden = true
        self.frontPhotoImageView.image = UIImage(named: AppConstants.Images.FRONT_PHOTO_ID_ICON)
    }
    
    @IBAction func removeBackPhotoAction(_ sender: UIButton) {
        self.backPhotoRemoveButton.isHidden = true
        self.backPhotoImageView.image = UIImage(named: AppConstants.Images.BACK_PHOTO_ID_ICON)
    }
    
    @IBAction func dropDownTapAction(_ sender: UITapGestureRecognizer) {
        showDropDown(tag: TYPE_DOCUMENT, title: AppConstants.Strings.SELECT_DOCUMENT_TYPE_TEXT, dataSourse: self, delegate: self, {(row) in
            self.documentTypeLabel.textColor = UIColor.black
            self.documentTypeLabel.text = self.documentTypeSelect[row].documentType
            self.selectedDocumentType = self.documentTypeSelect[row].id ?? 0
        })
    }
    
    func getImageFrontPhotoId(){
        self.imagePickerManager.pickImage(self,cameraTitle: AppConstants.CAMERA,galleryTitle: AppConstants.GALLERY){ image in
            self.frontPhotoImageView.image = image
            self.frontPhotoRemoveButton.isHidden = false
        }
    }
    
    func getImageBackPhotoId(){
        self.imagePickerManager.pickImage(self,cameraTitle: AppConstants.CAMERA,galleryTitle: AppConstants.GALLERY){ image in
            self.backPhotoImageView.image = image
            self.backPhotoRemoveButton.isHidden = false
        }
    }
    
    @IBAction func updateButtonAction(_ sender: UIButton) {
        
        if selectedDocumentType == 0 {
            self.view.makeToast(AppConstants.Strings.SELECT_DOCUMENT_TYPE)
        }else if frontPhotoImageView.image == UIImage(named: AppConstants.Images.FRONT_PHOTO_ID_ICON){
            self.view.makeToast(AppConstants.Strings.SELECT_FRONT_PHOTO_ID)
        }else if backPhotoImageView.image == UIImage(named: AppConstants.Images.BACK_PHOTO_ID_ICON){
            self.view.makeToast(AppConstants.Strings.SELECT_BACK_PHOTO_ID)
        }else{
            self.buttonActivityIndicatorStart()
            sender.isEnabled = false
            self.uploadDocuments()
        }
        
    }
    
    func uploadDocuments(){
        var data : AccountVerify = AccountVerify()
        data.documentType = self.selectedDocumentType
        data.documentSide = 1
        
        self.uploadDocumentOneApi(data:data)
    }
    
    
    private func uploadDocumentOneApi(data:AccountVerify) {
        let frontImage:UIImage = self.frontPhotoImageView.image!
        
        profileViewModel.uploadImageWithDocument(accessToken: getAccessToken(),image:frontImage,data: data , {(response) in
            if response.response?.statusCode == 201 {
                
                var data : AccountVerify = AccountVerify()
                data.documentType = self.selectedDocumentType
                data.documentSide = 2
                self.uploadDocumentTwoApi(data:data)
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.stopButtonActivityIndicatorAndEnableButton()
                //self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.stopButtonActivityIndicatorAndEnableButton()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    private func uploadDocumentTwoApi(data:AccountVerify) {
        let backImage:UIImage = self.backPhotoImageView.image!
        profileViewModel.uploadImageWithDocument(accessToken: getAccessToken(),image: backImage,data: data ,{(response) in
            if response.response?.statusCode == 201 {
                self.stopButtonActivityIndicatorAndEnableButton()
                self.navigationController?.popViewController(animated: false)
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.stopButtonActivityIndicatorAndEnableButton()
                //self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.stopButtonActivityIndicatorAndEnableButton()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func stopButtonActivityIndicatorAndEnableButton(){
        self.updateButton.isEnabled = true
        self.buttonActivityIndicator.stopAnimating()
    }
    
    func buttonActivityIndicatorStart(){
        self.buttonActivityIndicator.isHidden = false
        self.buttonActivityIndicator.startAnimating()
        self.buttonActivityIndicator.hidesWhenStopped = true
    }
    
}

extension VerifyAccountDocumentsViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return documentTypeSelect.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return documentTypeSelect[row].documentType
    }
    
    private func getDocumentType() {
        profileViewModel.getDocumentType(accessToken: getAccessToken(), {(response) in
            if response.response?.statusCode == 201 {
                let data: ApiResponse<[DocumentType]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    var documentList = data.data!
                    //documentList.removeFirst()
                    self.documentTypeSelect = documentList
                    self.showScreen()
                }
            } else {
                self.screenActivityIndicator.stopAnimating()
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
}
