import UIKit

class WithdrawViewController: BaseViewController,WithdrawClickProtocol {
    func withdrawClick(type: Int,amount:String,selectedBankAccount:Int) {
        if type == AppConstants.TYPE_WITHDRAW{
            let amountConvert:Double = Double(amount)!
            self.withdrawMoneyFromAccount(id:selectedBankAccount,amount:amountConvert)
        }
    }
    
    @IBOutlet var currentBalanceLabel: UILabel!
    @IBOutlet var savedBankAccountLabel: UILabel!
    @IBOutlet var enterAmountTextField: UITextField!
    @IBOutlet var amountErrorButton: UIButton!
    @IBOutlet var buttonActivityActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var withdrawButton: UIButton!
    @IBOutlet var payoutInProgressView: UIView!
    @IBOutlet var warningLabel: UILabel!
    @IBOutlet var warningImageView:UIImageView!
    @IBOutlet var topConstraint: NSLayoutConstraint!
    
    var currentBalance:Int?
    var accountInfo: [AccountInfo] = []
    var statesSelect = [states]()
    let profileViewModel = ProfileViewModel()
    var savedBankAccounts = [String]()
    var TYPE_SAVED_BANK_ACCOUNT:Int = 1
    var selectedBankAccount = 0
    var sales:Sales?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.Strings.WITHDRAW_FUNDS_TITLE
        self.currentBalanceLabel.text = String(format: AppConstants.FORMAT_TWO_DECIMAL, Double(self.currentBalance ?? 0))
        self.buttonActivityActivityIndicator.isHidden = true
        
        for i in 0..<accountInfo.count{
            savedBankAccounts.append(accountInfo[i].bankNickName! + " " + AppConstants.Strings.BRACKET_ON + accountInfo[i].lastFourNumbers! + AppConstants.Strings.BRACKET_OFF)
        }
        if (sales?.hasPayoutInProgress!)!{
            payoutInProgressView.isHidden = false
            setWarningLabelText(sales: sales!)
        }else{
            payoutInProgressView.isHidden = true
        }
        warningImageView.contentMode = .scaleAspectFit
        keyboardRequired(topConstraint: topConstraint)
    }
    
    func setWarningLabelText(sales:Sales){
        let text:String = sales.payoutProgressMessage!
        let textColor:String = AppConstants.Strings.NOTE_TEXT
        let range = (text as NSString).range(of: textColor)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 10), range: range)
        warningLabel.attributedText = attributedString
    }

    override func viewDidAppear(_ animated: Bool) {
        self.view.hideToastActivity()
    }
    
    @IBAction func selectBankAccountAction(_ sender: UITapGestureRecognizer) {
        showDropDown(tag: TYPE_SAVED_BANK_ACCOUNT, title:  AppConstants.Strings.SAVED_BANK_ACCOUNTS, dataSourse: self, delegate: self, {(row) in
            self.savedBankAccountLabel.textColor = UIColor.black
            self.savedBankAccountLabel.text = self.savedBankAccounts[row]
            self.selectedBankAccount = self.accountInfo[row].id!
        })
    }
    
    @IBAction func newBankAccountAction(_ sender: UIButton) {
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.BANK_DETAIL_BOARD, bundle:nil)
        let bankDetailsViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.BANK_DETAILS_SCREEN) as! BankDetailViewController
        bankDetailsViewController.currentBalance = self.currentBalance
        bankDetailsViewController.sales = self.sales
        self.navigationController?.pushViewController(bankDetailsViewController, animated:true)
    }
    
    @IBAction func withdrawButtonAction(_ sender: UIButton) {
        let amount = enterAmountTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let existingAccount = savedBankAccountLabel.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let amountInteger = Int(amount ?? "") ?? 0
        
        var isError = false
        
        if existingAccount == AppConstants.Strings.SAVED_BANK_ACCOUNTS {
            self.view.makeToast(AppConstants.Strings.FIRST_SELECT_BANK)
            isError = true
        }
        if !ValidationHelper.isEmptyAny(field: amount!) {
            amountErrorButton.isHidden = false
            isError = true
        }else if amountInteger > self.currentBalance ?? 0{
            amountErrorButton.isHidden = false
            isError = true
        }else if amountInteger > 1000{
            amountErrorButton.isHidden = false
            isError = true
        } else {
            amountErrorButton.isHidden = true
        }
        hideToolTip()
        if isError {
            return
        }
        let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.WITHDRAW_DIALOG_BOARD, bundle:nil)
        let withdrawDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.WITHDRAW_DIALOG) as! WithdrawDialogViewController
        withdrawDialogViewController.amount = amount
        withdrawDialogViewController.TYPE = AppConstants.TYPE_WITHDRAW
        withdrawDialogViewController.selectedBankAccount = self.selectedBankAccount
        withdrawDialogViewController.modalPresentationStyle = .overCurrentContext
        withdrawDialogViewController.withdrawClickDelegate = self
        self.navigationController?.present(withdrawDialogViewController, animated:false,completion: nil)
    }
    
    @IBAction func amountErrorButtonAction(_ sender: UIButton) {
        let amount = enterAmountTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        let amountInteger = Int(amount ?? "") ?? 0
        
        if !ValidationHelper.isEmptyAny(field: amount!) {
            showErrorTooltip(errorMessage: AppConstants.Strings.ENTER_WITHDRAW_AMOUNT, view: sender)
        }else if amountInteger > self.currentBalance ?? 0{
            showErrorTooltip(errorMessage: AppConstants.Strings.AMOUNT_MUST_BE_LESS_THAN_BALANCE, view: sender)
        }else if amountInteger > 1000{
            showErrorTooltip(errorMessage: AppConstants.Strings.AMOUNT_NOT_MORE_THAN_THOUSAND_DOLLAR, view: sender)
        } else {
            
        }
    }
    
    func withdrawMoneyFromAccount(id:Int,amount:Double) {
        self.dismiss(animated: false, completion: nil)
        self.view.makeToastActivity(.center)
        self.withdrawButton.isEnabled = false
        let amount = AccountInfo(withdrawAmount: amount)
        profileViewModel.withdrawMoneyFromAccount(accessToken: getAccessToken(), id: id, data: amount,{(response) in
            self.withdrawButton.isEnabled = true
            self.view.hideToastActivity()
            if response.response?.statusCode == 200 || response.response?.statusCode == 201 {
                let data: ApiResponse<AccountInfo> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    let accountInfo = data.data!
                    
                    var currentVCStack = self.navigationController?.viewControllers
                    currentVCStack?.removeLast()

                    let balanceHistoryViewControler = UIStoryboard(name: AppConstants.Storyboard.BALANCE_HISTORY_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.BALANCE_HISTORY_SCREEN) as! BalanceHistoryViewController
                    balanceHistoryViewControler.accountInfo = accountInfo
                    currentVCStack?.append(balanceHistoryViewControler)
                    self.navigationController?.setViewControllers(currentVCStack!, animated: true)
                    
                }
            }else if response.response?.statusCode == 400{
                    let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.WITHDRAW_FAIL_DIALOG_BOARD, bundle:nil)
                    let withdrawDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.WITHDRAW_FAIL_DIALOG) as! WithdrawFailDialogViewController
                    withdrawDialogViewController.TYPE = AppConstants.TYPE_WITHDRAW_FAIL
                    withdrawDialogViewController.modalPresentationStyle = .overCurrentContext
                    withdrawDialogViewController.modalTransitionStyle = .crossDissolve
                    self.navigationController?.present(withdrawDialogViewController, animated:false,completion: nil)
                
            } else {
                let data: ApiResponse<AccountInfo> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    let storyBoard : UIStoryboard = UIStoryboard(name: AppConstants.Storyboard.FAILS_MESSAGE_DIALOG_BOARD, bundle:nil)
                    let withdrawDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.FAIL_MESSAGE_DIALOG) as! FailesMessageViewController
                    withdrawDialogViewController.message = error
                    withdrawDialogViewController.modalPresentationStyle = .overCurrentContext
                    withdrawDialogViewController.modalTransitionStyle = .crossDissolve
                    self.navigationController?.present(withdrawDialogViewController, animated:false,completion: nil)
                }
            }
            
        } , { (error) in
            self.withdrawButton.isEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
}

extension WithdrawViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return accountInfo.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return savedBankAccounts[row]
    }
    
}
