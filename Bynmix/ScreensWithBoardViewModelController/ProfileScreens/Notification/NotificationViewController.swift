import UIKit
import Toast_Swift

class NotificationViewController: BaseViewController {

    @IBOutlet var tableView: UITableView!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var updateButon: UIButton!
    @IBOutlet var topView: UIView!
    @IBOutlet var footerView: UIView!
    
    var notificationHandler = NotificationHandler()
    var profileViewModel = ProfileViewModel()
    var notificationsUpdatedDelegate:NotificationsUpdatedProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.Strings.NOTIFICATION_SETTINGS
        tableView.contentInset = UIEdgeInsets(top: 60, left: 0, bottom: 0, right: 0)
        tableView.delegate = notificationHandler
        tableView.dataSource = notificationHandler
        getNotifications()
    }
    
    func getNotifications() {
        profileViewModel.getNotifications(accessToken: getAccessToken(), { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[NotificationSettings]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.notificationHandler.notificationSettings = data.data!
                    self.tableView.reloadData()
                    self.screenActivityIndicator.stopAnimating()
                    self.topView.isHidden = false
                    self.tableView.isHidden = false
                    self.footerView.isHidden = false
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.screenActivityIndicator.stopAnimating()
                self.view.hideToastActivity()
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    

    func updateNotifications(){
        self.view.makeToastActivity(.center)
        let settings = notificationHandler.notificationSettings as [NotificationSettings]
        let settingsRequest = NotificationSettingRequest(settings: settings)
        profileViewModel.updateNotifications(accessToken: getAccessToken(),data: settingsRequest ,{ (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[NotificationSettings]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    self.view.hideToastActivity()
                    self.updateButon.isEnabled = true
                    self.notificationsUpdatedDelegate?.notificationsUpdated()
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.view.hideToastActivity()
                self.updateButon.isEnabled = true
            }
        } , { (error) in
            self.view.hideToastActivity()
            self.updateButon.isEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    @IBAction func onUpdateButtonClick(_ sender: UIButton) {
        self.updateButon.isEnabled = false
        updateNotifications()
    }
    
}
