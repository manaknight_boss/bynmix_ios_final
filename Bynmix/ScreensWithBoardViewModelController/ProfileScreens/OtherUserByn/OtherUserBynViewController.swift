import UIKit
import XLPagerTabStrip
import Alamofire

class OtherUserBynViewController: BaseViewController,LoadMoreProtocol,RefreshOtherUserProfileProtocol {
    
    func refreshOtherUserProfile() {
        self.refreshList()
    }
    
    func onLoadMore() {
        if !isApiCalled {
            isApiCalled = true
            getBynListing(otherUserId: otherUserId!)
        }
    }
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var noDataShadowView: UIView!
    @IBOutlet var noDataView: UIView!
    @IBOutlet var noDataImageView: UIImageView!
    @IBOutlet var noDataLabel: UILabel!
    
    let otherUserBynHandler = OtherUserBynHandler()
    var currentPage = 1
    var isApiCalled = false
    var profileViewModel = ProfileViewModel()
    var feedViewModel = FeedViewModel()
    var otherUserId:Int?
    var feed:Feed?
    var USERNAME:String?
    static var otherUserBynFilterData:FilterData = FilterData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.isHidden = true
        collectionView.contentInset = UIEdgeInsets(top: 40, left: 5, bottom: 5, right: 3)
        collectionView.delegate = otherUserBynHandler
        collectionView.dataSource = otherUserBynHandler
        getItemsClick()
        if otherUserId != nil {
            getBynListing(otherUserId:otherUserId!)
        }
        addRefreshControl()
        otherUserBynHandler.loadMoreDelegate = self
        otherUserBynHandler.refreshOtherUserProfileDelegate = self
        setNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        noDataShadowView.layer.cornerRadius = 2
        noDataShadowView.layer.shadowColor = UIColor.black.cgColor
        noDataShadowView.alpha = CGFloat(0.26)
        noDataShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        noDataShadowView.layer.shadowOpacity = 1
        noDataShadowView.layer.zPosition = -1
    }
    
    func setNotifications(){
        NotificationCenter.default.addObserver(self,selector: #selector(listingFilterApply),name:Notification.Name(AppConstants.Notifications.OTHER_USER_FILTER_LISTING_BAR_BUTTON),object: nil)
        NotificationCenter.default.addObserver(self,selector: #selector(clearFilters),name:Notification.Name(AppConstants.Notifications.OTHER_USER_CLEAR_ALL_BAR_BUTTON),object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: Notification.Name(AppConstants.Notifications.OTHER_USER_FILTER_LISTING_BAR_BUTTON), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(AppConstants.Notifications.OTHER_USER_CLEAR_ALL_BAR_BUTTON), object: nil)
    }
    
    @objc func listingFilterApply(_ notification: Notification) {
        let barbuttonEnable:[SelectedFilter] = notification.userInfo?[AppConstants.Notifications.OTHER_USER_SELECTED_FILTER_LIST] as! [SelectedFilter]
        
        let selectedFilterList = barbuttonEnable
        
        if selectedFilterList.count > 0{
            self.view.makeToastActivity(.center)
            self.setFilterData(selectedFilterList: selectedFilterList)
        }
        
    }
    
    @objc func clearFilters(_ notification: Notification) {
        let barbuttonEnable = notification.userInfo?[AppConstants.Notifications.OTHER_USER_CLEAR_ALL_LIST] as! Bool
        
        if barbuttonEnable{
            self.view.makeToastActivity(.center)
            currentPage = 1
            OtherUserBynViewController.otherUserBynFilterData = FilterData()
            getBynListing(otherUserId: otherUserId!)
        }
    }
    
    func setFilterData(selectedFilterList :[SelectedFilter]) {
        var filterData = FilterData()
        var conditionIds:[Int] = []
        var categoryTypeIds:[Int] = []
        var sizeIds :[Int] = []
        var colorIds:[Int] = []
        var categoriesIds:[Int] = []
        var brandsIds:[Int] = []
        
        var conditionNames:[String] = []
        var sizeNames:[String] = []
        var colorNames:[String] = []
        var categoryNames:[String] = []
        var priceNames:[String] = []
        var brandNames:[String] = []
        
        for i in 0..<selectedFilterList.count {
            if (selectedFilterList[i].type == AppConstants.CONDITION_FILTER) {
                if (selectedFilterList[i].id != 0) {
                    conditionIds.append(selectedFilterList[i].id!)
                    filterData.conditionIds = conditionIds
                    
                    conditionNames.append(selectedFilterList[i].name!)
                    trackSearchDictionary.updateValue(conditionNames, forKey: "condition")
                }
            } else if (selectedFilterList[i].type == AppConstants.SIZE_FILTER) {
                if (selectedFilterList[i].id != 0) {
                    sizeIds.append(selectedFilterList[i].subType!)
                    filterData.sizeIds = sizeIds
                    
                    sizeNames.append(selectedFilterList[i].name!)
                    trackSearchDictionary.updateValue(sizeNames, forKey: "size")
                }
            } else if (selectedFilterList[i].type == AppConstants.COLOR_FILTER) {
                colorIds.append(selectedFilterList[i].id!)
                filterData.colorIds = colorIds
                
                colorNames.append(selectedFilterList[i].name!)
                trackSearchDictionary.updateValue(colorNames, forKey: "color")
            } else if (selectedFilterList[i].type == AppConstants.CATEGORY_FILTER) {
                if (selectedFilterList[i].subType != 0) {
                    categoriesIds.append(selectedFilterList[i].subType!)
                    categoryNames.append(selectedFilterList[i].name!)
                    trackSearchDictionary.updateValue(categoryNames, forKey: "category")
                }else{
                    categoryTypeIds.append(selectedFilterList[i].id!)
                }
                filterData.categoryIds = categoriesIds
                filterData.categoryTypeIds = categoryTypeIds
            } else if (selectedFilterList[i].type == AppConstants.BRAND_FILTER) {
                if (selectedFilterList[i].id != 0) {
                    brandsIds.append(selectedFilterList[i].id!)
                    filterData.brandIds = brandsIds
                    
                    brandNames.append(selectedFilterList[i].name!)
                    trackSearchDictionary.updateValue(brandNames, forKey: "brand")
                }
            } else if (selectedFilterList[i].type == AppConstants.PRICE_FILTER) {
                if selectedFilterList[i].id == 1 {
                    filterData.customMinPrice = 0
                    filterData.customMaxPrice = 20
                } else if selectedFilterList[i].id == 2 {
                    filterData.customMinPrice = 21
                    filterData.customMaxPrice = 50
                } else if selectedFilterList[i].id == 3 {
                    filterData.customMinPrice = 51
                    filterData.customMaxPrice = 100
                } else if selectedFilterList[i].id == 4 {
                    filterData.customMinPrice = 100
                    filterData.customMaxPrice = 200
                } else if selectedFilterList[i].id == 5{
                    filterData.customMinPrice = 200
                    filterData.customMaxPrice = 500
                }
                priceNames.append(selectedFilterList[i].name!)
                trackSearchDictionary.updateValue(priceNames, forKey: "price")
            }
        }
        
        OtherUserBynViewController.otherUserBynFilterData = filterData
        BaseViewController.mSelectedOtherUserProfileFilterList = selectedFilterList
        
        self.currentPage = 1
        self.getBynListing(otherUserId: otherUserId!,filterData: filterData)
        
    }
    
    func getItemsClick(){
        otherUserBynHandler.itemClickHandler = { feed in
            let storyboard = UIStoryboard(name: AppConstants.Storyboard.FOLLOW_FOLLOWING_BOARD, bundle: nil)
            let likeViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.FOLLOWERS_FOLLOWING) as! FollowersFollowingViewController
            if(feed.type == AppConstants.POST) {
                likeViewController.TYPE = AppConstants.TYPE_LIKE_POST
            } else {
                likeViewController.TYPE = AppConstants.TYPE_LIKE_LISTING
            }
            
            likeViewController.ID = feed.id
            self.navigationController?.pushViewController(likeViewController, animated: true)
        }
        
        otherUserBynHandler.onItemImageViewClick = { feed in
            let storyboard = UIStoryboard(name: AppConstants.Storyboard.LISTING_DETAIL_BOARD, bundle: nil)
            let listingDetailViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.Listing_Detail_Screen) as! ListDetailViewController
            
            listingDetailViewController.itemId = feed.id
            self.navigationController?.pushViewController(listingDetailViewController, animated: true)
        }
        
        otherUserBynHandler.onHeartButtonClick = { feed in
            if feed.type == AppConstants.LISTING {
                self.onHeartClickButton(id: feed.id!)
            }else{
                self.onHeartPostClickButton(id: feed.id!)
            }
        }
    }
    
    func getBynListing(otherUserId:Int,filterData:FilterData? = nil) {
        var filters: FilterData
        if filterData == nil {
            filters = FilterData()
        } else {
            filters = filterData!
        }
        profileViewModel.getOtherUserByn(accessToken: getAccessToken(), page: currentPage, id: otherUserId, data: filters,{(response) in
            self.view.hideToastActivity()
            self.handleResponse(response: response,filterData:filterData)
        } , { (error) in
            self.view.hideToastActivity()
            self.otherUserBynHandler.refreshControl.endRefreshing()
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
        
    }
    
    func handleResponse(response: AFDataResponse<Any>,filterData:FilterData? = nil) {
        self.isApiCalled = false
        self.otherUserBynHandler.refreshControl.endRefreshing()
        if response.response?.statusCode == 200 {
            self.screenActivityIndicator.stopAnimating()
            let data: ApiResponse<Feed> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            } else {
                let feed = data.data
                self.feed = feed
                if let feeds = feed?.items {
                    
                    if self.currentPage == 1 {
                        self.otherUserBynHandler.feed = feeds
                    } else {
                        self.otherUserBynHandler.feed.append(contentsOf: feeds)
                    }
                    if feed?.totalPages == self.currentPage {
                        self.isApiCalled = true
                        self.otherUserBynHandler.isLoadMoreRequired = false
                        let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
                        layout.footerReferenceSize = CGSize(width: 0, height: 0)
                    }
                    self.collectionView.reloadData()
                    self.screenActivityIndicator.stopAnimating()
                    self.collectionView.isHidden = false
                }
            }
            self.currentPage = self.currentPage + 1
        }else if response.response?.statusCode == 404 {
            self.noDataShadowView.isHidden = false
            self.noDataView.isHidden = false
            
            let username = self.USERNAME!
            let usernameTwo = " " + self.USERNAME!
            let text:String = username + " " + AppConstants.Strings.NO_LISTINGS_STATE + " " + usernameTwo + " " + AppConstants.Strings.LATEST_UPDATES
            let textColor:String = username
            let textColorUsername:String = usernameTwo
            let range = (text as NSString).range(of: textColor)
            let rangeUsername = (text as NSString).range(of: textColorUsername)
            let attributedString = NSMutableAttributedString(string: text)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cEA9DDC, range: range)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cEA9DDC, range: rangeUsername)
            
            self.noDataLabel.attributedText = attributedString
            self.noDataImageView.image = UIImage(named: AppConstants.Images.LOGO_BLACK)
            self.otherUserBynHandler.feed.removeAll()
            self.collectionView.reloadData()
            self.collectionView.isHidden = true
            self.screenActivityIndicator.stopAnimating()
            
        } else {
            self.noDataView.isHidden = false
            self.noDataShadowView.isHidden = false
            self.noDataLabel.text = AppConstants.Strings.SOMETHING_WRONG_HERE
            self.noDataImageView.image = UIImage(named: AppConstants.Images.LOGO_SAD_CLOUD)
            let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            }
            self.otherUserBynHandler.feed.removeAll()
            self.collectionView.reloadData()
            self.collectionView.isHidden = true
            self.screenActivityIndicator.stopAnimating()
        }
        if filterData?.brandIds?.count ?? 0 > 0 || filterData?.categoryIds?.count ?? 0 > 0 || filterData?.colorIds?.count ?? 0 > 0 || filterData?.conditionIds?.count ?? 0 > 0 || filterData?.sizeIds?.count ?? 0 > 0 || filterData?.customMinPrice != nil || filterData?.customMaxPrice != nil{
            self.trackSearchCalculate(userFilterData: filterData)
        }
    }
    
    func trackSearchCalculate(userFilterData:FilterData? = nil){
        var trackSearchOfUser = TrackSearchOfUser()
        var jsonString = ""
        
        if trackSearchDictionary.count > 0{
            jsonString = convertIntoJSONString(arrayObject: trackSearchDictionary)!
            print("jsonString - \(jsonString)")
        }
        
        trackSearchOfUser.searchBarLocation = AppConstants.TYPE_SEARCH_BAR_LOCATION_UNKNOWN
        
        if userFilterData != nil{
            if jsonString.count > 0{
                trackSearchOfUser.searchFilters = jsonString
            }
            self.trackSearch(trackSearchOfUser: trackSearchOfUser)
        }
    }
    
    
    func addRefreshControl() {
        otherUserBynHandler.refreshControl.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        collectionView.addSubview(otherUserBynHandler.refreshControl)
    }
    @objc func refreshList(){
        currentPage = 1
        isApiCalled = false
        self.otherUserBynHandler.isLoadMoreRequired = true
        
        let otherUserBynFilterData = OtherUserBynViewController.otherUserBynFilterData
        if (otherUserBynFilterData.brandIds?.count ?? 0) > 0 || (otherUserBynFilterData.categoryIds?.count ?? 0) > 0 || (otherUserBynFilterData.colorIds?.count ?? 0 > 0) || (otherUserBynFilterData.conditionIds?.count ?? 0 > 0) || (otherUserBynFilterData.sizeIds?.count ?? 0 > 0) || (otherUserBynFilterData.customMinPrice ?? 0 > 0) || (otherUserBynFilterData.customMaxPrice ?? 0 > 0){
            getBynListing(otherUserId:otherUserId!,filterData: otherUserBynFilterData)
        }else{
            getBynListing(otherUserId:otherUserId!)
        }
    }
    
    func onHeartClickButton(id:Int){
        feedViewModel.updateLikeStatus(accessToken: AppDefaults.getAccessToken(), id: id, {(response) in
            if response.response?.statusCode == 200 {
                
            }else if response.response?.statusCode == 500 {
                //self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG_TRY_AGAIN_LATER)
            }else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    //self.showMessageDialog(message: error)
                    print(error)
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func onHeartPostClickButton(id:Int){
        feedViewModel.updatePostLikeStatus(accessToken: AppDefaults.getAccessToken(), id: id, {(response) in
            if response.response?.statusCode == 200 {
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func setDelegate(feedScroll: FeedScrollProtocol) {
        otherUserBynHandler.scrollDelegate = feedScroll
    }
}

extension OtherUserBynViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: AppConstants.Strings.MY_BYN)
    }
}
