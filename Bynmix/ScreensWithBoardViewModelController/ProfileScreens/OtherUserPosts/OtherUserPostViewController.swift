import UIKit
import XLPagerTabStrip
import Alamofire

class OtherUserPostViewController: BaseViewController,LoadMoreProtocol,RefreshOtherUserProfileProtocol {
    
    func refreshOtherUserProfile() {
        self.refreshList()
    }
    
    func onLoadMore() {
        if !isApiCalled {
            isApiCalled = true
            getOtherUserPosts(otherUserId:otherUserId!)
        }
    }
    
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var tableViewLayout: UITableView!
    @IBOutlet var noDataShadowView: UIView!
    @IBOutlet var noDataView: UIView!
    @IBOutlet var noDataImageView: UIImageView!
    @IBOutlet var noDataLabel: UILabel!
    
    let otherUserPostHandler = OtherUserPostHandler()
    var feed:Feed? = nil
    var currentPage = 1
    var isApiCalled = false
    var viewControllerType:Int? = nil
    var feedViewModel = FeedViewModel()
    var profileViewModel = ProfileViewModel()
    var otherUserId:Int?
    var USERNAME:String?
    var dots: [Dots] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewLayout.estimatedRowHeight = 455
        tableViewLayout.rowHeight = UITableView.automaticDimension
        tableViewLayout.isHidden = true
        tableViewLayout.delegate = otherUserPostHandler
        tableViewLayout.dataSource = otherUserPostHandler
        getItemsClick()
        if otherUserId != nil {
            getOtherUserPosts(otherUserId:otherUserId!)
        }
        
        self.tableViewLayout.contentInset = UIEdgeInsets(top: 40, left: 0, bottom: 0, right: 0)
        addRefreshControl()
        otherUserPostHandler.loadMoreDelegate = self
        otherUserPostHandler.refreshOtherUserProfileDelegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        noDataShadowView.layer.cornerRadius = 2
        noDataShadowView.layer.shadowColor = UIColor.black.cgColor
        noDataShadowView.alpha = CGFloat(0.26)
        noDataShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        noDataShadowView.layer.shadowOpacity = 1
        noDataShadowView.layer.zPosition = -1
    }
    
    func getItemsClick(){
        otherUserPostHandler.itemClickHandler = { feed in
            let storyboard = UIStoryboard(name: AppConstants.Storyboard.FOLLOW_FOLLOWING_BOARD, bundle: nil)
            let likeViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.FOLLOWERS_FOLLOWING) as! FollowersFollowingViewController
            if(feed.type == AppConstants.POST) {
                likeViewController.TYPE = AppConstants.TYPE_LIKE_POST
            } else {
                likeViewController.TYPE = AppConstants.TYPE_LIKE_LISTING
            }
            
            likeViewController.ID = feed.id
            self.navigationController?.pushViewController(likeViewController, animated: true)
        }
        otherUserPostHandler.onItemImageViewClick = { feed in
            let storyboard = UIStoryboard(name: AppConstants.Storyboard.POST_DETAIL_BOARD, bundle: nil)
            let likeViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.POST_DETAIL_SCREEN) as! PostDetailViewController
            likeViewController.feed = feed
            if feed.type == AppConstants.POST {
                self.navigationController?.pushViewController(likeViewController, animated: true)
            }
        }
        
        otherUserPostHandler.onHeartButtonClick = { feed in
            if feed.type == AppConstants.LISTING {
                self.onHeartClickButton(id: feed.id!)
            }
        }
        
        otherUserPostHandler.onTagsViewClick = { index, feed in
            self.onTagsButtonClick(index: index, feed: feed)
        }
        
        otherUserPostHandler.onDotClick = { dot in
            self.postDotType(dot: dot)
        }
        
    }
    
    func postDotType(dot:Dots){
        if(dot.postDotTypeId! == AppConstants.TYPE_ID_MY_LISTING){
            self.listingScreen(listingId: dot.listingId!)
        }else{
            let link = dot.link ?? ""
            let title = dot.title ?? ""
            self.inAppWebBrowserScreen(link:link,title:title)
        }
    }
    
    func listingScreen(listingId:Int){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.LISTING_DETAIL_BOARD, bundle: nil)
        let listingDetailViewController = storyboard.instantiateViewController(withIdentifier:AppConstants.Controllers.Listing_Detail_Screen) as! ListDetailViewController
        listingDetailViewController.itemId = listingId
        self.navigationController?.pushViewController(listingDetailViewController, animated: true)
    }
    
    func getOtherUserPosts(otherUserId:Int) {
        profileViewModel.getOtherUserPost(accessToken: getAccessToken(), page: currentPage, id: otherUserId,{(response) in
            self.handleResponse(response:response)
        } , { (error) in
            self.otherUserPostHandler.refreshControl.endRefreshing()
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            } 
        })
        
    }
    
    func handleResponse(response:AFDataResponse<Any>){
        self.isApiCalled = false
        self.otherUserPostHandler.refreshControl.endRefreshing()
        if response.response?.statusCode == 200 {
            self.screenActivityIndicator.stopAnimating()
            let data: ApiResponse<Feed> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            } else {
                let feed = data.data
                self.feed = feed
                if self.otherUserPostHandler.feed.count > 0 && self.currentPage == 1 || self.currentPage == 2{
                    self.otherUserPostHandler.feed.removeAll()
                }
                if var feeds = feed?.items {
                    for i in 0..<feeds.count {
                        feeds[i].expandableLabelCollapsed = false
                        feeds[i].isActive = true
                        feeds[i].containDots = false
                    }
                    if self.currentPage == 1 {
                        self.otherUserPostHandler.feed = feeds
                    } else {
                        self.otherUserPostHandler.feed.append(contentsOf: feeds)
                    }
                    if feed?.totalPages == self.currentPage {
                        self.isApiCalled = true
                        self.otherUserPostHandler.isLoadMoreRequired = false
                    }
                    self.tableViewLayout.reloadData()
                    self.screenActivityIndicator.stopAnimating()
                    self.tableViewLayout.isHidden = false
                }
            }
            self.currentPage = self.currentPage + 1
        }else if response.response?.statusCode == 404 {
            self.noDataShadowView.isHidden = false
            self.noDataView.isHidden = false
            
            let username = self.USERNAME!
            let usernameTwo = " " + self.USERNAME!
            let text:String = username + " " + AppConstants.Strings.NO_POSTS_STATE + " " + usernameTwo + " "  + AppConstants.Strings.LATEST_UPDATES
            let textColor:String = username
            let textColorUsername = usernameTwo
            let range = (text as NSString).range(of: textColor)
            let rangeUsername = (text as NSString).range(of: textColorUsername)
            let attributedString = NSMutableAttributedString(string: text)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cEA9DDC, range: range)
            attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cEA9DDC, range: rangeUsername)
            
            self.noDataLabel.attributedText = attributedString
            self.screenActivityIndicator.stopAnimating()
            self.noDataImageView.image = UIImage(named: AppConstants.Images.LOGO_BLACK)
            
        } else {
            self.noDataView.isHidden = false
            self.noDataShadowView.isHidden = false
            self.noDataLabel.text = AppConstants.Strings.SOMETHING_WRONG_HERE
            self.noDataImageView.image = UIImage(named: AppConstants.Images.LOGO_SAD_CLOUD)
            let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            }
            self.screenActivityIndicator.stopAnimating()
        }
    }
    
    
    func addRefreshControl() {
        otherUserPostHandler.refreshControl.addTarget(self, action: #selector(refreshListEmpty), for: .valueChanged)
        tableViewLayout.addSubview(otherUserPostHandler.refreshControl)
    }
    @objc func refreshList(){
        currentPage = 1
        isApiCalled = false
        self.otherUserPostHandler.isLoadMoreRequired = true
        getOtherUserPosts(otherUserId:otherUserId!)
    }
    
    @objc func refreshListEmpty(){
    
    }
    
    func onHeartClickButton(id:Int){
        feedViewModel.updateLikeStatus(accessToken: getTemporaryToken(), id: id, {(response) in
            if response.response?.statusCode == 200 {
                
            }else if response.response?.statusCode == 500 {
                //self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG_TRY_AGAIN_LATER)
            }else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    print(error)
                    //self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func setDelegate(feedScroll: FeedScrollProtocol) {
        otherUserPostHandler.scrollDelegate = feedScroll
    }
    
    func onTagsButtonClick(index: Int, feed:Feed){
           if feed.postDots != nil && feed.postDots!.count > 0 {
               var feedTemp = feed
               feedTemp.containDots = true
               var feeds = self.otherUserPostHandler.feed
               feeds[index] = feedTemp
               
               self.otherUserPostHandler.feed = feeds
               let position = IndexPath(row: index, section: 0)
               self.tableViewLayout.reloadRows(at: [position], with: .none)
           } else {
               self.view.makeToastActivity(.center)
               feedViewModel.getDots(accessToken: getTemporaryToken(), id: feed.id!, {(response) in
                   self.view.hideToastActivity()
                   if response.response?.statusCode == 200 {
                       let data: ApiResponse<[Dots]> = Utils.convertResponseToData(data: response.data!)
                       if data.isError {
                           let error = Utils.getErrorMessage(errors: data.errors)
                           self.showMessageDialog(message: error)
                       }else{
                           self.dots = data.data!
                           var feedTemp = feed
                           feedTemp.postDots = self.dots
                           feedTemp.containDots = true
                           var feeds = self.otherUserPostHandler.feed
                           feeds.remove(at: index)
                           feeds.insert(feedTemp, at: index)
                           self.otherUserPostHandler.feed = feeds
                           let position = IndexPath(row: index, section: 0)
                           self.tableViewLayout.reloadRows(at: [position], with: .none)
                           
                           
                       }
                   }else {
                       let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                       if data.isError {
                           let error = Utils.getErrorMessage(errors: data.errors)
                           self.showMessageDialog(message: error)
                       }
                   }
               } , { (error) in
                   self.view.hideToastActivity()
                   if error.statusCode == AppConstants.NO_INTERNET {
                       self.showInternetError()
                   }
               })
           }
       }
    
}

extension OtherUserPostViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: AppConstants.Strings.MY_POSTS)
    }
}
