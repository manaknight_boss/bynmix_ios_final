import UIKit
import Toast_Swift
import ExpandableLabel

class OtherUserProfileViewController:BaseViewController,FeedScrollProtocol,UserFilterClearAllProtocol,UserFilterApplyProtocol,ReportAnIssueUserProtocol{
    
    func reportAnIssueUser() {
        self.navigationController?.popViewController(animated: false)
        self.reportAnIssueSuccessMessageDialog()
    }
    
    func userFilterClearAll() {
        AppConstants.clearFilterList = true
        NotificationCenter.default.post(name: Notification.Name(AppConstants.Notifications.OTHER_USER_CLEAR_ALL_BAR_BUTTON),object: nil,userInfo:[AppConstants.Notifications.OTHER_USER_CLEAR_ALL_LIST: AppConstants.clearFilterList])
        self.navigationController?.popViewController(animated: false)
    }
    
    func userFilterApply(selectedFilter: [SelectedFilter], heightModal: HeightModel) {
         NotificationCenter.default.post(name: Notification.Name(AppConstants.Notifications.OTHER_USER_FILTER_LISTING_BAR_BUTTON),object: nil,userInfo:[AppConstants.Notifications.OTHER_USER_SELECTED_FILTER_LIST: selectedFilter])
        self.navigationController?.popViewController(animated: false)
    }
    
    func onScroll(scrollValue: CGFloat) {
        let currentScroll = scrollValue + 40
        if currentScroll < 0{
            topConstraint.constant = 0
        }else if currentScroll <= self.profileView.frame.height {
            topConstraint.constant = -currentScroll
        }else {
            topConstraint.constant = -self.profileView.frame.height
        }
    }
    
    @IBOutlet var userImageView: UIImageView!
    @IBOutlet var loaderActivity: UIActivityIndicatorView!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var userPhotoImageView: UIImageView!
    @IBOutlet var userShadowView: UIView!
    @IBOutlet var stateLabel: UILabel!
    @IBOutlet var followersLabel: UILabel!
    @IBOutlet var followingLabel: UILabel!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var locationView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var blogLinkLabel: UILabel!
    @IBOutlet var collectionViewSocialLinks: UICollectionView!
    @IBOutlet var shortDescLabel: ExpandableLabel!
    @IBOutlet var coverImageView: UIImageView!
    @IBOutlet var profileBarButtonItem: UIBarButtonItem!
    @IBOutlet var followImageView: UIImageView!
    @IBOutlet var filterBarButton: UIBarButtonItem!
    @IBOutlet var topConstraint: NSLayoutConstraint!
    @IBOutlet var profileView: UIView!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var ratingStarOne: UIImageView!
    @IBOutlet weak var ratingStarTwo: UIImageView!
    @IBOutlet weak var ratingStarThree: UIImageView!
    @IBOutlet weak var ratingStarFour: UIImageView!
    @IBOutlet weak var ratingStarFive: UIImageView!
    @IBOutlet weak var viewReviewsLabel: UILabel!
    
    var user:User? = nil
    
    let profileButtonsHandler = ProfileButtonsHandler()
    let socialLinksHandler = SocialIconsHandler()
    let profileViewModel = ProfileViewModel()
    
    var imageSelectionType = 0
    let SELECTION_TYPE_PROFILE = 0
    let SELECTION_TYPE_COVER = 1
    var otherUserId:Int?
    var USERNAME:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self,selector: #selector(onTabChange),name:Notification.Name(AppConstants.Notifications.BAR_BUTTON),object: nil)
        navigationItem.rightBarButtonItem = filterBarButton
        scrollView.isHidden = true
        if user != nil {
            setUser(user:self.user!)
        }
        setSocialLinksLayout()
        profileBarButtonItem.imageInsets = UIEdgeInsets(top: 0, left: -35, bottom: 0, right: 0)
        if otherUserId != nil{
            getUser(otherUserId: otherUserId!)
        }
        followImageView.contentMode = .scaleAspectFill
        setUserImageUI()
    }
    
    @objc func onTabChange(_ notification: Notification) {
        let barbuttonEnable:NSNumber = notification.userInfo?[AppConstants.Notifications.BAR_BUTTON_ENABLE] as! NSNumber
        if barbuttonEnable == 0 {
           navigationItem.rightBarButtonItem = filterBarButton
        } else if barbuttonEnable == 1 {
            navigationItem.rightBarButtonItem = nil
        }
        
    }
    
    func setSocialLinksLayout() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 30, height: 30)
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 0
        self.collectionViewSocialLinks!.collectionViewLayout = layout
        self.collectionViewSocialLinks.dataSource = socialLinksHandler
        self.collectionViewSocialLinks.delegate = socialLinksHandler
        
        socialLinksHandler.itemClickHandler = { socialLink in
            var title = ""
            switch socialLink.type {
            case AppConstants.Social.FACEBOOK:
                title = "Facebook"
                break
            case AppConstants.Social.YOUTUBE:
                title = "YouTube"
                break
            case AppConstants.Social.TIKTOK:
                title = "TikTok"
                break
            case AppConstants.Social.PINTEREST:
                title = "Pinterest"
                break
            case AppConstants.Social.INSTAGRAM:
                title = "Instagram"
                break
            default:
                break
            }
            self.inAppWebBrowserScreen(link: socialLink.value, title: title)
        }
    }
    
    func setUserImageUI(){
        userImageView.layer.borderWidth = 2.0
        userImageView.layer.masksToBounds = false
        userImageView.layer.borderColor = UIColor.white.cgColor
        userImageView.clipsToBounds = true
        userImageView.layer.cornerRadius = userImageView.frame.size.width/2
        
        userShadowView.layer.cornerRadius = userShadowView.frame.size.width/2
        userShadowView.layer.shadowColor = UIColor.white.cgColor
        userShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        userShadowView.layer.shadowOpacity = 1
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let childViewController = segue.destination as? OtherUserTabsViewController {
            childViewController.otherUserId = self.otherUserId
            childViewController.USERNAME = self.USERNAME
            childViewController.view.translatesAutoresizingMaskIntoConstraints = true
            childViewController.setDelegateListing(feedScroll: self)
            childViewController.setDelegatePosts(feedScroll: self)
        }
    }
    
    @IBAction func onFollowersClick(_ sender: UITapGestureRecognizer) {
        if user?.followersCount == 0{
            followersLabel.isUserInteractionEnabled = false
        }else{
            followersLabel.isUserInteractionEnabled = true
            openFollowFollowingScreen(type: AppConstants.TYPE_OTHER_USER_FOLLOWER)
        }
    }
    
    @IBAction func onFollowingClick(_ sender: UITapGestureRecognizer) {
        if user?.followingCount == 0{
            followingLabel.isUserInteractionEnabled = false
        }else{
            followingLabel.isUserInteractionEnabled = true
            openFollowFollowingScreen(type: AppConstants.TYPE_OTHER_USER_FOLLOWING)
        }
    }
    
    func openFollowFollowingScreen(type: Int) {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.FOLLOW_FOLLOWING_BOARD, bundle: nil)
        let followFollowingViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.FOLLOWERS_FOLLOWING) as! FollowersFollowingViewController
        followFollowingViewController.TYPE = type
        followFollowingViewController.ID = self.otherUserId
        followFollowingViewController.USERNAME = user?.username
        navigationController?.pushViewController(followFollowingViewController, animated: true)
    }
    
    func getUser(otherUserId:Int) {
        profileViewModel.getUser(accessToken: getAccessToken(),userId: otherUserId ,{ (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<User> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let userData = data.data!
                    self.user = userData
                    self.setUser(user:userData)
                    
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            } else if error.statusCode == 401 {
                
            }
        })
    }
    
    func updateFollowStatus(userId: Int) {
        profileViewModel.updateFollowStatus(accessToken: getAccessToken(), userId: userId,{(response) in
            self.followImageView.isUserInteractionEnabled = true
            if response.response?.statusCode == 200 {
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.followImageView.isUserInteractionEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func setUser(user:User) {
        let text:String = AppConstants.Strings.MY_WEBSITE
        let textRegular:String = AppConstants.Strings.MY_WEBSITE
        let range = (textRegular as NSString).range(of: text)
        let attributedString = NSMutableAttributedString(string: textRegular)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 14), range: range)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range)
        blogLinkLabel.attributedText = attributedString
        
        let image = UIImage(named: AppConstants.Images.DEFAULT_USER)
        self.usernameLabel.text = user.username
        
        if let imageUrl = user.photoUrl{
            userPhotoImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        } else {
            userPhotoImageView.image =  image
        }
        self.coverImageView.kf.setImage(with: URL(string: (self.user?.backgroundPhoto)!)!)
        
        if ((user.stateAbbreviated) == nil) && user.city == nil {
            locationView.isHidden = true
            let cityState = ""
            self.stateLabel.text  = cityState
        }else if user.stateAbbreviated == nil {
            let city = user.city!
            self.stateLabel.text  = city
        }else if user.city == nil {
            let state = user.stateAbbreviated!
            self.stateLabel.text  = state
        }else {
            locationView.isHidden = false
            let cityState = user.city! + "," + user.stateAbbreviated!
            self.stateLabel.text  = cityState
        }
        
        self.followersLabel.text = "\(user.followersCount ??  0)"
        self.followingLabel.text = "\(user.followingCount ??  0)"
        
        if user.title == nil {
            self.titleLabel.isHidden = true
        } else {
            self.titleLabel.isHidden = false
            self.titleLabel.text = user.title
        }
        
        if user.blogLink == nil {
            self.blogLinkLabel.isHidden = true
        } else {
            self.blogLinkLabel.isHidden = false
        }
        
        if user.shortBio == nil {
            self.shortDescLabel.isHidden = true
            self.shortDescLabel.text = ""
            shortDescLabel.numberOfLines = 1
        } else {
            if (user.shortBio?.count ?? 0) > 140 {
                shortDescLabel.numberOfLines = 3
                shortDescLabel.collapsedAttributedLink = NSAttributedString(string: AppConstants.Strings.SHOW_MORE, attributes: [NSAttributedString.Key.foregroundColor:UIColor.c32afff, NSAttributedString.Key.font: UIFont.fontSemiBold(size: 12)])
                shortDescLabel.expandedAttributedLink = NSAttributedString(string:AppConstants.Strings.SHOW_LESS , attributes: [NSAttributedString.Key.foregroundColor:UIColor.c32afff, NSAttributedString.Key.font: UIFont.fontSemiBold(size: 12)])
                shortDescLabel.shouldCollapse = true
                shortDescLabel.collapsed = true
                shortDescLabel.textReplacementType = .character
                self.shortDescLabel.text = user.shortBio
            }else{
                self.shortDescLabel.text = user.shortBio
            }
            self.shortDescLabel.isHidden = false
            self.shortDescLabel.textAlignment = .center
        }
        
        if user.facebookLink == nil && user.youtubeLink == nil && user.tikTokLink == nil && user.pinterestLink == nil  && user.instagramLink == nil{
            self.collectionViewSocialLinks.isHidden = true
        } else {
            self.collectionViewSocialLinks.isHidden = false
            
            var socialOptions = [SocialLink]()
            
            if user.facebookLink != nil {
                socialOptions.append(SocialLink(type: AppConstants.Social.FACEBOOK, value: (user.facebookLink)!))
            }
            if user.youtubeLink != nil {
                socialOptions.append(SocialLink(type: AppConstants.Social.YOUTUBE, value: (user.youtubeLink)!))
            }
            if user.tikTokLink != nil {
                socialOptions.append(SocialLink(type: AppConstants.Social.TIKTOK, value: (user.tikTokLink)!))
            }
            if user.pinterestLink != nil {
                socialOptions.append(SocialLink(type: AppConstants.Social.PINTEREST, value: (user.pinterestLink)!))
            }
            if user.instagramLink != nil {
                socialOptions.append(SocialLink(type: AppConstants.Social.INSTAGRAM, value: (user.instagramLink)!))
            }
            socialLinksHandler.socialOptions = socialOptions
            self.collectionViewSocialLinks.reloadData()
        }
        
        if (user.isLoggedInUserFollowing)! {
            followImageView.image = UIImage( named: AppConstants.Images.PROFILE_FOLLOWING_ICON)
        } else {
            followImageView.image = UIImage( named: AppConstants.Images.FOLLOW_USER_ICON)
        }
         
        self.setRating(user:user)
        
        self.loaderActivity.stopAnimating()
        self.scrollView.isHidden = false
        
    }
    
    func setRating(user:User){
        let rating = Int(user.rating!.rounded())
        
        if rating == 0{
            self.ratingView.isHidden = true
        }else{
            self.ratingView.isHidden = false
            
            print("currentRating:",rating)
            
            if rating == 1{
                self.ratingStarOne.isHidden = false
                self.ratingStarTwo.isHidden = true
                self.ratingStarThree.isHidden = true
                self.ratingStarFour.isHidden = true
                self.ratingStarFive.isHidden = true
            }else if rating == 2{
                self.ratingStarOne.isHidden = false
                self.ratingStarTwo.isHidden = false
                self.ratingStarThree.isHidden = true
                self.ratingStarFour.isHidden = true
                self.ratingStarFive.isHidden = true
            }else if rating == 3{
                self.ratingStarOne.isHidden = false
                self.ratingStarTwo.isHidden = false
                self.ratingStarThree.isHidden = false
                self.ratingStarFour.isHidden = true
                self.ratingStarFive.isHidden = true
            }else if rating == 4{
                self.ratingStarOne.isHidden = false
                self.ratingStarTwo.isHidden = false
                self.ratingStarThree.isHidden = false
                self.ratingStarFour.isHidden = false
                self.ratingStarFive.isHidden = true
            }else{
                self.ratingStarOne.isHidden = false
                self.ratingStarTwo.isHidden = false
                self.ratingStarThree.isHidden = false
                self.ratingStarFour.isHidden = false
                self.ratingStarFive.isHidden = false
            }
        }
        
    }
    
    @IBAction func myWebsiteClick(_ sender: UITapGestureRecognizer) {
        let link = (self.user?.blogLink ?? "")
        self.inAppWebBrowserScreen(link: link, title: "Website")
    }
    
    @IBAction func onFollowIconClick(_ sender: UITapGestureRecognizer) {
        updateFollowStatus(userId: (user?.userId)!)
        var followersCount:Int = Int(self.followersLabel!.text!)!
        
        if followImageView.image == UIImage( named: AppConstants.Images.FOLLOW_USER_ICON){
            followImageView.image = UIImage( named: AppConstants.Images.PROFILE_FOLLOWING_ICON)
            followersCount = followersCount + 1
            followersLabel.text = "\(followersCount)"
        }else{
            followersCount = followersCount - 1
            followersLabel.text = "\(followersCount)"
            followImageView.image = UIImage( named: AppConstants.Images.FOLLOW_USER_ICON)
        }
        followImageView.isUserInteractionEnabled = false
    }
    
    @IBAction func onFilterButtonAction(_ sender: UIBarButtonItem) {
        filterListing()
    }
    
    @IBAction func reviewTapAction(_ sender: UITapGestureRecognizer) {
        viewReviewScreen()
    }
    
    func viewReviewScreen(){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.VIEW_REVIEW_TYPE_STORYBOARD, bundle: nil)
        let reviewViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.VIEW_REVIEW_TYPE_SCREEN) as! ReviewTypeViewController
        reviewViewController.user = self.user!
        self.navigationController?.pushViewController(reviewViewController, animated: false)
    }
    
    func filterListing(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.LISTING_FILTER_STORYBOARD, bundle: nil)
        let filterListingViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.LISTING_FILTER_SCREEN) as! FilterListingsViewController
        filterListingViewController.filterApplyDelegate = self
        filterListingViewController.filterClearAllDelegate = self
        self.navigationController?.pushViewController(filterListingViewController, animated: false)
    }
    
    @IBAction func onBackBarButtonAction(_ sender: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func usersBarButtonAction(_ sender: UIBarButtonItem) {
        self.searchUsersScreen()
    }
    
    @IBAction func reportAnIssueAction(_ sender: UITapGestureRecognizer) {
        reportAnIssueScreen()
    }
    
    func reportAnIssueScreen(){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.REPORT_AN_ISSUE_FEED_STORYBOARD, bundle: nil)
        let reportAnIssueViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.REPORT_AN_ISSUE_FEED_SCREEN) as! ReportAnIssueFeedViewController
        reportAnIssueViewController.TYPE = AppConstants.TYPE_REPORT_AN_ISSUE_USER
        reportAnIssueViewController.user = self.user!
        reportAnIssueViewController.reportAnIssueUserDelegate = self
        self.navigationController?.pushViewController(reportAnIssueViewController, animated: false)
    }
    
}
