import UIKit
import XLPagerTabStrip

class OtherUserTabsViewController: ButtonBarPagerTabStripViewController {
    var otherUserId:Int?
    var USERNAME:String?

    override func viewDidLoad() {
        loadDesign()
        super.viewDidLoad()

    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let tabBynViewController = UIStoryboard(name: AppConstants.Storyboard.OTHER_USER_BYN_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.OTHER_USER_BYN_SCREEN) as! OtherUserBynViewController
            tabBynViewController.otherUserId = self.otherUserId
            tabBynViewController.USERNAME = self.USERNAME
        
        let tabPostViewController = UIStoryboard(name: AppConstants.Storyboard.OTHER_USER_POSTS_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.OTHER_USER_POST_SCREEN) as! OtherUserPostViewController
            tabPostViewController.otherUserId = self.otherUserId
            tabPostViewController.USERNAME = self.USERNAME
        
        return [tabBynViewController,tabPostViewController]
    }
    
    override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool) {
        super.updateIndicator(for: viewController, fromIndex: fromIndex, toIndex: toIndex, withProgressPercentage: progressPercentage, indexWasChanged: indexWasChanged)
        if indexWasChanged {
            NotificationCenter.default.post(name: Notification.Name(AppConstants.Notifications.BAR_BUTTON),object: nil,userInfo:[AppConstants.Notifications.BAR_BUTTON_ENABLE: currentIndex, AppConstants.Notifications.IS_PROFILE: true])
        }
    }
    
    func loadDesign() {
        self.settings.style.buttonBarBackgroundColor = UIColor.white
        self.settings.style.buttonBarItemBackgroundColor = UIColor.white
        self.settings.style.selectedBarBackgroundColor = UIColor.cFB9D52
        self.settings.style.buttonBarItemFont = UIFont.fontSemiBold(size: 12)
        self.settings.style.selectedBarHeight = 2.0
        self.settings.style.buttonBarMinimumLineSpacing = 0
        self.settings.style.buttonBarItemTitleColor = UIColor.black
        self.settings.style.buttonBarItemsShouldFillAvailableWidth = true
        self.settings.style.buttonBarLeftContentInset = 10
        self.settings.style.buttonBarRightContentInset = 10
        
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            
            oldCell?.label.textColor = UIColor.c2A2A2A
            oldCell?.label.font = UIFont.fontSemiBold(size: 12)
            newCell?.label.textColor = UIColor.cBB189C
            newCell?.label.font = UIFont.fontSemiBold(size: 12)
            
        }
    }
    
    func setDelegateListing(feedScroll: FeedScrollProtocol) {
        let allTabs = self.viewControllers
        let tabListingsFeedViewController = allTabs[0] as! OtherUserBynViewController
        tabListingsFeedViewController.setDelegate(feedScroll: feedScroll)
    }
    
    func setDelegatePosts(feedScroll: FeedScrollProtocol) {
        let allTabs = self.viewControllers
        let tabPostsFeedViewController = allTabs[1] as! OtherUserPostViewController
        tabPostsFeedViewController.setDelegate(feedScroll: feedScroll)
    }

}
