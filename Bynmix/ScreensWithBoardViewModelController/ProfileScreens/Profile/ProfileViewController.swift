import UIKit
import ExpandableLabel
import Toast_Swift
import Mantis

class ProfileViewController: BaseViewController ,NotificationsUpdatedProtocol,LogoutProtocol {
    
    func logout(){
        self.logoutUser()
    }
    
    func notificationsUpdated() {
        self.navigationController?.popViewController(animated: false)
        self.view.makeToast("Updated Successfully")
    }
    
    @IBOutlet var gridHeightConstraint: NSLayoutConstraint!
    @IBOutlet var collectionViewLayout: UICollectionView!
    @IBOutlet var userImageView: UIImageView!
    @IBOutlet var loaderActivity: UIActivityIndicatorView!
    @IBOutlet var usernameLabel: UILabel!
    @IBOutlet var userPhotoImageView: UIImageView!
    @IBOutlet var userShadowView: UIView!
    @IBOutlet var stateLabel: UILabel!
    @IBOutlet var followersLabel: UILabel!
    @IBOutlet var followingLabel: UILabel!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var locationView: UIView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var blogLinkLabel: UILabel!
    @IBOutlet var collectionViewSocialLinks: UICollectionView!
    @IBOutlet var shortDescLabel: ExpandableLabel!
    @IBOutlet var coverImageView: UIImageView!
    @IBOutlet var profileBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var ratingStarOne: UIImageView!
    @IBOutlet weak var ratingStarTwo: UIImageView!
    @IBOutlet weak var ratingStarThree: UIImageView!
    @IBOutlet weak var ratingStarFour: UIImageView!
    @IBOutlet weak var ratingStarFive: UIImageView!
    @IBOutlet weak var viewReviewsLabel: UILabel!
    
    @IBOutlet var promotionsTableView: UITableView!
    @IBOutlet var promotiontableViewHeight: NSLayoutConstraint!
    
    var user:User? = nil
    
    let profileButtonsHandler = ProfileButtonsHandler()
    let socialLinksHandler = SocialIconsHandler()
    let profileViewModel = ProfileViewModel()
    
    var imageSelectionType = 0
    let SELECTION_TYPE_PROFILE = 0
    let SELECTION_TYPE_COVER = 1
    var VIEWCONTROLLER_TYPE:Int?
    let milliSeconds = 0.25
    
    var promotionsHandler = PromotionsHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrollView.isHidden = true
        setProfileButtonsLayout()
        if user != nil {
            setUser(user: self.user!)
        }
        setSocialLinksLayout()
        profileBarButtonItem.imageInsets = UIEdgeInsets(top: 0, left: -35, bottom: 0, right: 0)
        collectionViewLayout.contentInset = UIEdgeInsets(top: 0, left: 7, bottom: 7, right: 7)
        setUserImageUI()
        ratingView.isUserInteractionEnabled = true
        promotionsTableView.delegate = promotionsHandler
        promotionsTableView.dataSource = promotionsHandler
        getPromotionsClick()
        promotionsTableView.rowHeight = UITableView.automaticDimension
        promotionsTableView.estimatedRowHeight = 60
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.getUser()
        self.promotionsHandler.promotions.removeAll()
        self.promotionsTableView.reloadData()
        if AppConstants.isPromotionsAllListLoaded{
            getPromotions()
        }
    }
    
    func getPromotionsClick(){
        promotionsHandler.onCancelButtonClick = { promotions,index in
            self.promotionsHandler.promotions.remove(at: index)
            self.promotionsTableView.reloadData()
            let promotionId = promotions.promotionId!
            
            AppDefaults.setPromotionId(promotionId: promotionId)
            print(AppDefaults.getPromotionIds())
            self.resizePromotionsTableView(promotions: self.promotionsHandler.promotions)
        }
    }
    
    func setProfileButtonsLayout() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.width * 0.87
        let height: CGFloat = screenHeight / 3
        let width: CGFloat = screenWidth / 3
        layout.itemSize = CGSize(width: width, height: height)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        self.collectionViewLayout!.collectionViewLayout = layout
        self.gridHeightConstraint.constant = height * 5
        self.collectionViewLayout.dataSource = profileButtonsHandler
        self.collectionViewLayout.delegate = profileButtonsHandler
        
        profileButtonsHandler.itemClickHandler = { position in
            if position == 0  {
                self.openAccountSettingsScreen()
            }
            if position == 1  {
                self.openMyBynScreen()
            }
            if position == 2  {
                self.openUserMyPostsScreen()
            }
            if position == 3{
                self.openMyLikesScreen()
            }
            if position == 4{
                self.openMySaleAndMyPurchasesScreen(type:AppConstants.TYPE_PURCHASE)
            }
            if position == 5{
               self.openMySaleAndMyPurchasesScreen(type:AppConstants.TYPE_SALE)
            }
            if position == 6{
                self.openMyWalletScreen()
            }
            if position == 7{
                self.openAddressScreen()
            }
            if position == 9{
                self.openNotificationScreen()
            }
            if position == 12  {
                self.showLogoutDialog()
            }
        }
    }
    
    func setSocialLinksLayout() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 30, height: 30)
        layout.minimumInteritemSpacing = 10
        layout.minimumLineSpacing = 0
        self.collectionViewSocialLinks!.collectionViewLayout = layout
        self.collectionViewSocialLinks.dataSource = socialLinksHandler
        self.collectionViewSocialLinks.delegate = socialLinksHandler
        
        socialLinksHandler.itemClickHandler = { socialLink in
            var title = ""
            switch socialLink.type {
            case AppConstants.Social.FACEBOOK:
                title = "Facebook"
                break
            case AppConstants.Social.YOUTUBE:
                title = "YouTube"
                break
            case AppConstants.Social.TIKTOK:
                title = "TikTok"
                break
            case AppConstants.Social.PINTEREST:
                title = "Pinterest"
                break
            case AppConstants.Social.INSTAGRAM:
                title = "Instagram"
                break
            default:
                break
            }
            self.inAppWebBrowserScreen(link: socialLink.value, title: title)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if AppConstants.IS_JUST_CREATED == AppConstants.CREATE_TYPE_POST {
            AppConstants.IS_JUST_CREATED = AppConstants.CREATE_TYPE_NONE
            self.openUserMyPostsScreen()
        } else if AppConstants.IS_JUST_CREATED == AppConstants.CREATE_TYPE_LISTING {
            AppConstants.IS_JUST_CREATED = AppConstants.CREATE_TYPE_NONE
            self.openMyBynScreen()
        }
    }
    
    func setUserImageUI(){
        userImageView.layer.borderWidth = 2.0
        userImageView.layer.masksToBounds = false
        userImageView.layer.borderColor = UIColor.white.cgColor
        userImageView.clipsToBounds = true
        userImageView.layer.cornerRadius = userImageView.frame.size.width/2
        
        userShadowView.layer.cornerRadius = userShadowView.frame.size.width/2
        userShadowView.layer.shadowColor = UIColor.white.cgColor
        userShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        userShadowView.layer.shadowOpacity = 1
    }
    
    @IBAction func onEditIconClick(_ sender: UITapGestureRecognizer) {
        openAccountSettingsScreen()
    }
    
    @IBAction func onFollowersClick(_ sender: UITapGestureRecognizer) {
        if user?.followersCount == 0{
            followersLabel.isUserInteractionEnabled = false
        }else{
            followersLabel.isUserInteractionEnabled = true
            openFollowFollowingScreen(type: AppConstants.TYPE_FOLLOWER)
        }
    }
    
    @IBAction func onFollowingClick(_ sender: UITapGestureRecognizer) {
        if user?.followingCount == 0{
            followingLabel.isUserInteractionEnabled = false
        }else{
            followingLabel.isUserInteractionEnabled = true
            openFollowFollowingScreen(type: AppConstants.TYPE_FOLLOWING)
        }
    }
    
    func openMyBynScreen() {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.USER_BYN_BOARD, bundle: nil)
        let  userBynViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.USER_BYN_SCREEN) as! UserBynViewController
        navigationController?.pushViewController(userBynViewController, animated: true)
    }
    
    func openMyLikesScreen() {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.MY_LIKES_BOARD, bundle: nil)
        let  myLikesViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.MY_LIKES_SCREEN) as! MyLikesViewController
        navigationController?.pushViewController(myLikesViewController, animated: true)
    }
    
    func openAddressScreen(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.ADDRESSES_BOARD, bundle: nil)
        let  addressViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.ADDRESSES_SCREEN) as! AddressesViewController
        navigationController?.pushViewController(addressViewController, animated: true)
    }
    
    func openNotificationScreen() {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.NOTIFICATION_BOARD, bundle: nil)
        let notificationViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.NOTIFICATIONS_SCREEN) as! NotificationViewController
        notificationViewController.notificationsUpdatedDelegate = self
        navigationController?.pushViewController(notificationViewController, animated: true)
    }
    
    func openAccountSettingsScreen() {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.ACCOUNT_SETTINGS_STORYBOARD, bundle: nil)
        let accountSettingsViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.ACCOUNT_SETTINGS_SCREEN) as! AccountSettingsViewController
        accountSettingsViewController.user = user
        navigationController?.pushViewController(accountSettingsViewController, animated: true)
    }
    
    func openUserMyPostsScreen() {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.USER_POSTS_BOARD, bundle: nil)
        let editProfileViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.USER_POSTS_SCREEN) as! UserPostsViewController
        navigationController?.pushViewController(editProfileViewController, animated: true)
    }
    func openMyWalletScreen() {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.MY_WALLET_BOARD, bundle: nil)
        let myWalletViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.MY_WALLET_SCREEN) as! MyWalletViewController
        navigationController?.pushViewController(myWalletViewController, animated: true)
    }
    
    func openFollowFollowingScreen(type: Int) {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.FOLLOW_FOLLOWING_BOARD, bundle: nil)
        let followFollowingViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.FOLLOWERS_FOLLOWING) as! FollowersFollowingViewController
        followFollowingViewController.TYPE = type
        followFollowingViewController.ID = user?.userId
        followFollowingViewController.USERNAME = user?.username
        navigationController?.pushViewController(followFollowingViewController, animated: true)
    }
    
    func showLogoutDialog() {
        logoutScreen()
    }
    
    func logoutScreen(){
        let storyboard = UIStoryboard(name:AppConstants.Storyboard.LOGOUT_DIALOG_BOARD,bundle: nil)
        let logoutViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.LOGOUT_DIALOG) as! LogoutDialogViewController
        logoutViewController.modalPresentationStyle = .overFullScreen
        logoutViewController.logoutDelegate = self
        self.navigationController?.present(logoutViewController, animated: false, completion: nil)
    }
    
    func getUser() {
        profileViewModel.getUser(accessToken: getAccessToken(), { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<User> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let userData = data.data!
                    self.user = userData
                    self.setUser(user:userData)
                    
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func logoutUser() {
        self.view.makeToastActivity(.center)
        self.dismiss(animated: false, completion: nil)
        profileViewModel.logoutUser(accessToken: getAccessToken(), { (response) in
            self.view.hideToastActivity()
            self.clearAllLists()
            self.clearAllToken()
            self.logoutScreenBase()
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func clearAllToken(){
        AppDefaults.setAccessToken(token: "")
        AppDefaults.setRefreshToken(token: "")
        AppDefaults.setToken(token: "")
        AppDefaults.setPromotionId(promotionId: 0)
    }
    
    func clearAllLists(){
        ListingsFeedViewController.feedOfAll.removeAll()
        ListingsFeedViewController.feedOfUser.removeAll()
        ListingsFeedViewController.feedOfMyLikes.removeAll()
    }
    
    func setUser(user:User) {
        let text:String = AppConstants.Strings.MY_WEBSITE
        let textRegular:String = AppConstants.Strings.MY_WEBSITE
        let range = (textRegular as NSString).range(of: text)
        let attributedString = NSMutableAttributedString(string: textRegular)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 14), range: range)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: range)
        blogLinkLabel.attributedText = attributedString
        let image = UIImage(named: AppConstants.Images.DEFAULT_USER)
        self.usernameLabel.text = self.user!.username
        
        if let imageUrl = user.photoUrl{
            userPhotoImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        } else {
            userPhotoImageView.image =  image
        }
        self.coverImageView.contentMode = .scaleToFill
        self.coverImageView.kf.setImage(with: URL(string: (user.backgroundPhoto)!)!)
        
        if user.city != nil && user.stateAbbreviated != nil{
            locationView.isHidden = false
            let cityState = user.city! + "," + user.stateAbbreviated!
            self.stateLabel.text  = cityState
        }else if user.stateAbbreviated != nil{
            locationView.isHidden = false
            let state = self.user!.stateAbbreviated!
            self.stateLabel.text  = state
        }else if user.city != nil{
            locationView.isHidden = false
            let city = user.city!
            self.stateLabel.text  = city
        }else{
            locationView.isHidden = true
        }
        
        self.followersLabel.text = "\(user.followersCount ??  0)"
        self.followingLabel.text = "\(user.followingCount ??  0)"
        
        if user.title == nil {
            self.titleLabel.isHidden = true
        } else {
            self.titleLabel.isHidden = false
            self.titleLabel.text = self.user?.title
        }
        
        if user.blogLink == nil {
            self.blogLinkLabel.isHidden = true
        } else {
            self.blogLinkLabel.isHidden = false
        }
        
        if user.shortBio == nil {
            self.shortDescLabel.isHidden = true
            self.shortDescLabel.text = ""
            shortDescLabel.numberOfLines = 1
        } else {
            if (user.shortBio?.count ?? 0) > 140 {
                shortDescLabel.numberOfLines = 3
                shortDescLabel.collapsedAttributedLink = NSAttributedString(string: AppConstants.Strings.SHOW_MORE, attributes: [NSAttributedString.Key.foregroundColor:UIColor.c32afff, NSAttributedString.Key.font: UIFont.fontSemiBold(size: 12)])
                //shortDescLabel.expandedAttributedLink = NSAttributedString(string:AppConstants.Strings.SHOW_LESS , attributes: [NSAttributedString.Key.foregroundColor:UIColor.c32afff, NSAttributedString.Key.font: UIFont.fontSemiBold(size: 12)])
                shortDescLabel.setLessLinkWith(lessLink: AppConstants.Strings.SHOW_LESS, attributes: [NSAttributedString.Key.foregroundColor:UIColor.c32afff, NSAttributedString.Key.font: UIFont.fontSemiBold(size: 12)], position: nil)
                shortDescLabel.shouldCollapse = true
                shortDescLabel.collapsed = true
                shortDescLabel.textReplacementType = .character
                self.shortDescLabel.text = user.shortBio
            }else{
                self.shortDescLabel.text = user.shortBio
            }
            self.shortDescLabel.isHidden = false
            self.shortDescLabel.textAlignment = .center
        }
        
        if user.facebookLink == nil && user.youtubeLink == nil && user.tikTokLink == nil && user.pinterestLink == nil  && user.instagramLink == nil{
            self.collectionViewSocialLinks.isHidden = true
        } else {
            self.collectionViewSocialLinks.isHidden = false
            
            var socialOptions = [SocialLink]()
            
            if user.facebookLink != nil {
                socialOptions.append(SocialLink(type: AppConstants.Social.FACEBOOK, value: (user.facebookLink)!))
            }
            if user.youtubeLink != nil {
                socialOptions.append(SocialLink(type: AppConstants.Social.YOUTUBE, value: (user.youtubeLink)!))
            }
            if user.tikTokLink != nil {
                socialOptions.append(SocialLink(type: AppConstants.Social.TIKTOK, value: (user.tikTokLink)!))
            }
            if user.pinterestLink != nil {
                socialOptions.append(SocialLink(type: AppConstants.Social.PINTEREST, value: (user.pinterestLink)!))
            }
            if user.instagramLink != nil {
                socialOptions.append(SocialLink(type: AppConstants.Social.INSTAGRAM, value: (user.instagramLink)!))
            }
            socialLinksHandler.socialOptions = socialOptions
            self.collectionViewSocialLinks.reloadData()
        }
        
        self.setRating(user:user)
        
        self.loaderActivity.stopAnimating()
        self.scrollView.isHidden = false
    }
    
    func setRating(user:User){
        let rating = user.rating ?? 0
        
        if rating == 0{
            self.ratingView.isHidden = true
        }else{
            self.ratingView.isHidden = false
            
            print("currentRating:",rating)
            
            if  0...1 ~= rating{
                self.ratingStarOne.isHidden = false
                self.ratingStarTwo.isHidden = true
                self.ratingStarThree.isHidden = true
                self.ratingStarFour.isHidden = true
                self.ratingStarFive.isHidden = true
            }else if 1...2 ~= rating{
                self.ratingStarOne.isHidden = false
                self.ratingStarTwo.isHidden = true
                self.ratingStarThree.isHidden = true
                self.ratingStarFour.isHidden = true
                self.ratingStarFive.isHidden = true
            }else if 2...3 ~= rating{
                self.ratingStarOne.isHidden = false
                self.ratingStarTwo.isHidden = false
                self.ratingStarThree.isHidden = true
                self.ratingStarFour.isHidden = true
                self.ratingStarFive.isHidden = true
            }else if 3...4 ~= rating{
                self.ratingStarOne.isHidden = false
                self.ratingStarTwo.isHidden = false
                self.ratingStarThree.isHidden = false
                self.ratingStarFour.isHidden = true
                self.ratingStarFive.isHidden = true
            }else if 4...5 ~= rating{
                self.ratingStarOne.isHidden = false
                self.ratingStarTwo.isHidden = false
                self.ratingStarThree.isHidden = false
                self.ratingStarFour.isHidden = false
                self.ratingStarFive.isHidden = true
            }else{
                self.ratingStarOne.isHidden = false
                self.ratingStarTwo.isHidden = false
                self.ratingStarThree.isHidden = false
                self.ratingStarFour.isHidden = false
                self.ratingStarFive.isHidden = false
            }
        }
        
    }
    
    @IBAction func myWebsiteClick(_ sender: UITapGestureRecognizer) {
        self.inAppWebBrowserScreen(link: (self.user?.blogLink ?? ""), title: "Website")
    }
    
    @IBAction func onCoverEditClick(_ sender: UIButton) {
        pickImage()
        imageSelectionType = SELECTION_TYPE_COVER
    }
    
    @IBAction func onUserPhotoEditClick(_ sender: UITapGestureRecognizer) {
        pickImage()
        imageSelectionType = SELECTION_TYPE_PROFILE
    }
    
    @IBAction func onMyLikesAction(_ sender: UIBarButtonItem) {
    pushViewController(storyboardName: AppConstants.Storyboard.MY_LIKES_BOARD, viewControllerIdentifier: AppConstants.Controllers.MY_LIKES_SCREEN)
    }
    
    @IBAction func backBarButtonAction(_ sender: UIBarButtonItem) {
        print(self.navigationController?.viewControllers.count ?? 0)
        if self.navigationController?.viewControllers.count ?? 0 == 1{
            self.tabBarController?.selectedIndex = 0
        }else{
            self.navigationController?.popViewController(animated: false)
        }
    }
    
    @IBAction func usersBarButtonAction(_ sender: UIBarButtonItem) {
        self.searchUsersScreen()
    }
    
    @IBAction func reviewTapAction(_ sender: UITapGestureRecognizer) {
        viewReviewScreen()
    }
    
    func viewReviewScreen(){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.VIEW_REVIEW_TYPE_STORYBOARD, bundle: nil)
        let reviewViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.VIEW_REVIEW_TYPE_SCREEN) as! ReviewTypeViewController
        reviewViewController.user = self.user!
        self.navigationController?.pushViewController(reviewViewController, animated: false)
    }
    
    func getPromotions() {
        let promotions = AppConstants.PROMOTIONS_ALL_LIST
        
        var promotionsNew : [Promotions] = []
        if AppDefaults.getPromotionIds().count > 0{
            print("promotions:",AppDefaults.getPromotionIds())
            for i in 0..<promotions.count{
                if  !AppDefaults.getPromotionIds().contains(promotions[i].promotionId!){
                    promotionsNew.append(promotions[i])
                }
            }
            self.promotionsHandler.promotions = promotionsNew
        }else{
            self.promotionsHandler.promotions = promotions
        }
        self.promotionsTableView.reloadData()
        self.promotionsTableView.isHidden = false
        self.resizePromotionsTableView(promotions: promotions)
        
    }

    func resizePromotionsTableView(promotions:[Promotions]){
        if promotions.count == 0{
            promotiontableViewHeight.constant = 0
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                var tableViewHeight: CGFloat {
                    self.promotionsTableView.layoutIfNeeded()
                    return self.promotionsTableView.contentSize.height
                }
                self.promotiontableViewHeight?.constant = tableViewHeight
            }
        }
    }
    
}

extension ProfileViewController:UIImagePickerControllerDelegate, UINavigationControllerDelegate,CropViewControllerDelegate {
    
    func cropViewControllerDidCrop(_ cropViewController: CropViewController, cropped: UIImage, transformation: Transformation) {
        if imageSelectionType == SELECTION_TYPE_COVER {
            self.coverImageView.image = cropped
        } else {
            self.userImageView.image = cropped
        }
        cropViewController.dismiss(animated: false)
        if imageSelectionType == SELECTION_TYPE_COVER {
            profileViewModel.uploadCoverImage(accessToken: getAccessToken(), userId: (user?.userId)!, image: cropped,  { (response) in
                if response.response?.statusCode == 201 {
                    self.coverImageView.image = cropped
                }
            } , { (error) in
                if error.statusCode == AppConstants.NO_INTERNET {
                    self.showInternetError()
                }
            })
        } else {
            profileViewModel.uploadProfileImage(accessToken: getAccessToken(), userId: (user?.userId)!, image: cropped,  { (response) in
                if response.response?.statusCode == 201 {
                    self.userImageView.image = cropped
                }
            } , { (error) in
                if error.statusCode == AppConstants.NO_INTERNET {
                    self.showInternetError()
                }
            })
        }
        self.coverImageView.contentMode = .scaleToFill
    }
    
    func cropViewControllerDidCancel(_ cropViewController: CropViewController, original: UIImage) {
        self.dismiss(animated: false, completion: nil)
    }
    
    
    @objc public func pickImage() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let defaultAction = UIAlertAction(title: AppConstants.GALLERY, style: .default) { (action) in
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        let camera = UIAlertAction(title: AppConstants.CAMERA, style: .default) { (action) in
            let imagePicker = UIImagePickerController()
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }
        
        let cancel = UIAlertAction(title: AppConstants.CANCEL, style: .destructive)
        
        alertController.addAction(defaultAction)
        alertController.addAction(camera)
        alertController.addAction(cancel)
        alertController.modalPresentationStyle = .popover
        
        present(alertController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = (info[UIImagePickerController.InfoKey.originalImage] as? UIImage) else { return }

        let config = Mantis.Config()
        
        let cropViewController = Mantis.cropViewController(image: image, config: config)
        cropViewController.modalPresentationStyle = .fullScreen
        cropViewController.delegate = self
        cropViewController.config.showRotationDial = false
        cropViewController.config.cropToolbarConfig.toolbarButtonOptions = .reset
        cropViewController.config.cropVisualEffectType = .light
        
        if imageSelectionType == SELECTION_TYPE_COVER {
            cropViewController.config.presetFixedRatioType = .alwaysUsingOnePresetFixedRatio(ratio: 16.0 / 9.0)
        } else {
            
            cropViewController.config.presetFixedRatioType = .alwaysUsingOnePresetFixedRatio(ratio: 16.0 / 16.0)
        }
    
        picker.dismiss(animated: true, completion: {
            self.present(cropViewController, animated: true,completion: nil)
        })
        
    }
    
}
