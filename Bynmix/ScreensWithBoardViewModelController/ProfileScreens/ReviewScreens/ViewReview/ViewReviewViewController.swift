import UIKit
import XLPagerTabStrip
import Alamofire

class ViewReviewViewController: BaseViewController,LoadMoreProtocol {
    
    func onLoadMore() {
        if !isApiCalled {
            isApiCalled = true
            if TYPE == AppConstants.TYPE_BUYER{
                self.fetchBuyerReviews(id:self.userId, index: AppConstants.TYPE_BUYER)
            }else{
                self.fetchSellerReviews(id: self.userId, index: AppConstants.TYPE_SELLER)
            }
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var noDataView: UIView!
    @IBOutlet weak var noDataShadowView: UIView!
    @IBOutlet weak var noDataLabel: UILabel!
    
    var TYPE:Int?
    var viewReviewHandler = ViewReviewHandler()
    var profileViewModel = ProfileViewModel()
    var user:User?
    var userId = 0
    let milliSeconds = 0.25
    var refreshControl:UIRefreshControl?
    var currentPage = 1
    var isApiCalled = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let username = self.user?.username ?? ""
        noDataLabel.text = (username) + " does not have any reviews yet."
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        let id = userId
        if id != 0{
            currentPage = 1
            if TYPE == AppConstants.TYPE_BUYER{
                tableView.delegate = viewReviewHandler
                tableView.dataSource = viewReviewHandler
                self.fetchBuyerReviews(id: id, index: AppConstants.TYPE_BUYER)
            }else{
                tableView.delegate = viewReviewHandler
                tableView.dataSource = viewReviewHandler
                self.fetchSellerReviews(id: self.userId, index: AppConstants.TYPE_SELLER)
            }
        }
        noDataViewUI()
        addRefreshControl()
        onProfileClick()
    }
    
    func onProfileClick(){
        viewReviewHandler.onUserProfileClick = { review in
            if self.TYPE == AppConstants.TYPE_BUYER{
                if review.sellerId == AppDefaults.getUserId(){
                    self.openUserProfile()
                }else{
                    self.openProfile(id:review.sellerId ?? 0,username:review.sellerUsername ?? "")
                }
            }else{
                if review.buyerId == AppDefaults.getUserId(){
                    self.openUserProfile()
                }else{
                    self.openProfile(id:review.buyerId ?? 0,username:review.buyerUsername ?? "")
                }
            }
        }
        
        viewReviewHandler.onListingClick = { review in
            let id = review.listingId ?? 0
            self.openListingScreen(listingId:id)
        }
    }
    
    func openListingScreen(listingId:Int){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.LISTING_DETAIL_BOARD, bundle: nil)
        let listingDetailViewController = storyboard.instantiateViewController(withIdentifier:AppConstants.Controllers.Listing_Detail_Screen) as! ListDetailViewController
        listingDetailViewController.itemId = listingId
        self.navigationController?.pushViewController(listingDetailViewController, animated: true)
    }
    
    func noDataViewUI(){
        noDataShadowView.layer.cornerRadius = 2
        noDataShadowView.layer.shadowColor = UIColor.black.cgColor
        noDataShadowView.alpha = CGFloat(0.26)
        noDataShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        noDataShadowView.layer.shadowOpacity = 1
        noDataShadowView.layer.zPosition = -1
    }
    
    func fetchBuyerReviews(id:Int,index:Int) {
        profileViewModel.fetchBuyerReviews(accessToken: getAccessToken(), id: id, page: currentPage,{(response) in
            self.handleResponse(response:response, index: index)
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func fetchSellerReviews(id:Int,index:Int) {
        profileViewModel.fetchSellerReviews(accessToken: getAccessToken(), id: id, page: currentPage,{(response) in
            self.handleResponse(response:response, index: index)
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func handleResponse(response: AFDataResponse<Any>,index:Int) {
        self.isApiCalled = false
        self.refreshControl?.endRefreshing()
        if response.response?.statusCode == 200 {
            let data: ApiResponse<Reviews> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            } else {
                self.viewReviewHandler.TYPE = self.TYPE
                let reviewData = data.data
                
                if (reviewData?.items!.count)! > 0{
                    if let reviews = reviewData?.items {
                        if self.currentPage == 1 {
                            self.viewReviewHandler.review = reviews
                        } else {
                            self.viewReviewHandler.review.append(contentsOf: reviews)
                        }
                        if reviewData?.totalPages == currentPage {
                            self.isApiCalled = true
                            self.viewReviewHandler.isLoadMoreRequired = false
                        }
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                            self.screenActivityIndicator.stopAnimating()
                            self.tableView.isHidden = false
                        }
                        
                    }
                }else{
                    self.viewReviewHandler.review.removeAll()
                    self.tableView.reloadData()
                    noDataShadowView.isHidden = false
                    noDataView.isHidden = false
                }
                
            }
            self.currentPage = self.currentPage + 1
        } else if response.response?.statusCode == 404{
            self.viewReviewHandler.review.removeAll()
            self.tableView.reloadData()
            noDataShadowView.isHidden = false
            noDataView.isHidden = false
            
        }else{
            
        }
    }
    
    func addRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        tableView.addSubview(refreshControl!)
    }
    @objc func refreshList(){
        currentPage = 1
        isApiCalled = false
        self.viewReviewHandler.isLoadMoreRequired = true
        if TYPE == AppConstants.TYPE_BUYER{
            self.fetchBuyerReviews(id:self.userId, index: AppConstants.TYPE_BUYER)
        }else{
            self.fetchSellerReviews(id: self.userId, index: AppConstants.TYPE_SELLER)
        }
    }
    
}

extension ViewReviewViewController: IndicatorInfoProvider {
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        var title = ""
        if TYPE == AppConstants.TYPE_BUYER{
            title = AppConstants.Strings.AS_BUYER
        }else{
            title = AppConstants.Strings.AS_SELLER
        }
        return IndicatorInfo(title: title)
    }
    
}
