import UIKit
import XLPagerTabStrip

class ReviewTypeViewController: ButtonBarPagerTabStripViewController {
    
    var user:User?
    var isReload = false

    override func viewDidLoad() {
        loadDesign()
        super.viewDidLoad()
        let username = (user?.username)!
        navigationItem.title = "\(username) Reviews"
        buttonBarView.translatesAutoresizingMaskIntoConstraints = true
        buttonBarView.delegate = self
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.reloadPagerTabStripView()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        buttonBarView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let tabBuyerViewController = UIStoryboard(name: AppConstants.Storyboard.VIEW_REVIEWS_STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.VIEW_REVIEW_SCREEN) as! ViewReviewViewController
        tabBuyerViewController.TYPE = AppConstants.TYPE_BUYER
        tabBuyerViewController.user = user!
        tabBuyerViewController.userId = (user?.userId!)!
        
        let tabSellerViewController = UIStoryboard(name: AppConstants.Storyboard.VIEW_REVIEWS_STORYBOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.VIEW_REVIEW_SCREEN) as! ViewReviewViewController
        tabSellerViewController.TYPE = AppConstants.TYPE_SELLER
        tabSellerViewController.user = user!
        tabSellerViewController.userId = (user?.userId!)!
        return [tabBuyerViewController,tabSellerViewController]
    }

    func loadDesign() {
        self.settings.style.buttonBarBackgroundColor = UIColor.white
        self.settings.style.buttonBarItemBackgroundColor = UIColor.white
        self.settings.style.selectedBarBackgroundColor = UIColor.cFB9D52
        self.settings.style.buttonBarItemFont = UIFont.fontSemiBold(size: 12)
        self.settings.style.selectedBarHeight = 2.0
        self.settings.style.buttonBarMinimumLineSpacing = 0
        self.settings.style.buttonBarItemTitleColor = UIColor.black
        self.settings.style.buttonBarItemsShouldFillAvailableWidth = true
        self.settings.style.buttonBarLeftContentInset = 10
        self.settings.style.buttonBarRightContentInset = 10
        
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            
            oldCell?.label.textColor = UIColor.c2A2A2A
            oldCell?.label.font = UIFont.fontSemiBold(size: 12)
            newCell?.label.textColor = UIColor.cBB189C
            newCell?.label.font = UIFont.fontSemiBold(size: 12)
            
        }
    }
    
}
