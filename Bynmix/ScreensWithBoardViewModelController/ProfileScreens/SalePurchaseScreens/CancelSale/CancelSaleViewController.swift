import UIKit

class CancelSaleViewController: BaseViewController {
    
    @IBOutlet var itemTitleLabel:UILabel!
    @IBOutlet var itemImageView:UIImageView!
    @IBOutlet var itemShippedView:UIView!
    @IBOutlet var submitButton:UIButton!
    @IBOutlet var issueTextView:UITextView!
    @IBOutlet var mainScrollView: UIScrollView!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var dropDownLabel: UILabel!
    @IBOutlet var onSubmitActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var topConstraint:NSLayoutConstraint!
    
    var saleCancelReasonSelect = [SaleCancelReason]()
    var selectedIssueTypes = 0
    let profileViewModel = ProfileViewModel()
    let TYPE_SaleCancelReason: Int = 1
    var saleDetail:SalesDetail?
    var cancelOrderSaleDelegate:CancelOrderSaleProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getSaleCancelReason()
        hideKeyboardWhenTappedAround()
        let backBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.BACK_BUTTON), style: .plain, target: self, action: #selector(onBackBarButtonItem))
        self.navigationItem.leftBarButtonItem  = backBarButton
        setData()
        onSubmitActivityIndicator.isHidden = true
        navigationItem.title = AppConstants.Strings.CANCEL_SALE_TITLE
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        itemImageView.layer.masksToBounds = true
        itemImageView.layer.borderWidth = 1
        itemImageView.layer.borderColor = UIColor.black.cgColor
        itemImageView.layer.cornerRadius = 1
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        UIView.animate(withDuration: 0.5) {
            self.topConstraint.constant = -150
            self.view.layoutIfNeeded()
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        UIView.animate(withDuration: 0.5) {
            self.topConstraint.constant = 10
            self.view.layoutIfNeeded()
        }
    }
    
    func setData(){
        let text = "\"" + (self.saleDetail?.listingTitle!)! + "\""
        let title =  self.saleDetail?.listingTitle!
        self.setTitleText(text:text,title:title!)
        let imagePlaceholder = UIImage(named: AppConstants.Images.ITEM_PLACEHOLDER)
        if let imageUrl = saleDetail?.listingImageUrl{
            itemImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: imagePlaceholder)
        }else{
            itemImageView.image = imagePlaceholder
        }
        itemImageView.contentMode = .scaleToFill
    }
    
    func setTitleText(text:String,title:String){
        let text:String = text
        let title:String = title
        let rangePink = (text as NSString).range(of: title)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cBB189C, range: rangePink)
        itemTitleLabel.attributedText = attributedString
    }
    
    @objc func onBackBarButtonItem(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func dropDownAction(_ sender: UITapGestureRecognizer) {
        showDropDown(tag: TYPE_SaleCancelReason, title:  AppConstants.Strings.SELECT_CANCEL_REASON, dataSourse: self, delegate: self, {(row) in
            self.dropDownLabel.textColor = UIColor.black
            self.dropDownLabel.text = self.saleCancelReasonSelect[row].cancelReason
            self.selectedIssueTypes = self.saleCancelReasonSelect[row].saleCancelReasonId!
        })
    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        cancelOrder()
    }
    
    func cancelOrder() {
        if selectedIssueTypes == 0{
            self.view.makeToast(AppConstants.Strings.SELECT_REASON_TO_CANCEL_ORDER)
        }else{
            self.view.makeToastActivity(.center)
            let comment = self.issueTextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
            let saleCancelReasonId = self.selectedIssueTypes
            
            var saleCancelReason = SaleCancelReason()
            saleCancelReason.saleCancelReasonId = saleCancelReasonId
            saleCancelReason.comments = comment
            self.cancelOrder(saleId:(self.saleDetail?.saleId!)!,saleCancelReason:saleCancelReason,listingTitle:self.saleDetail?.listingTitle! ?? "")
        }
    }
    
    func cancelOrder(saleId:Int,saleCancelReason:SaleCancelReason,listingTitle:String){
        profileViewModel.cancelOrderSale(accessToken: getAccessToken(),id:saleId ,data: saleCancelReason, { (response) in
            if response.response?.statusCode == 201 || response.response?.statusCode == 200{
                let data: ApiResponse<Bool> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    self.cancelOrderSaleDelegate?.cancelOrderSale(listingTitle: listingTitle)
                    self.navigationController?.popViewController(animated: false)
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
}

extension CancelSaleViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return saleCancelReasonSelect.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return saleCancelReasonSelect[row].cancelReason
    }
    
    func getSaleCancelReason() {
        profileViewModel.getSaleCancelReason(accessToken: getAccessToken(), {(response) in
            self.screenActivityIndicator.stopAnimating()
            self.mainScrollView.isHidden = false
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[SaleCancelReason]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.saleCancelReasonSelect = data.data!
                }
            } else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            self.mainScrollView.isHidden = false
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
}

