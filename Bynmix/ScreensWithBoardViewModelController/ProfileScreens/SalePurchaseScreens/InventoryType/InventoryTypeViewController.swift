import UIKit
import XLPagerTabStrip

class InventoryTypeViewController: ButtonBarPagerTabStripViewController {

    var TYPE:Int?

    override func viewDidLoad() {
        loadDesign()
        super.viewDidLoad()
        if TYPE == AppConstants.TYPE_PURCHASE{
            navigationItem.title = AppConstants.Strings.PURCHASES_TITLE
        }else{
            navigationItem.title = AppConstants.Strings.SALES_TITLE
        }
        NotificationCenter.default.addObserver(self,selector: #selector(onSelectMyStatsChange),name:Notification.Name(AppConstants.Notifications.BAR_BUTTON),object: nil)
        
        navigationItem.rightBarButtonItem = self.setButtons()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.06) {
            self.reloadPagerTabStripView()
        }
    }
    
    @objc func onSelectMyStatsChange(_ notification: Notification) {

        let barbuttonEnable:NSNumber = notification.userInfo?[AppConstants.Notifications.BAR_BUTTON_ENABLE] as! NSNumber
        if Int(truncating: barbuttonEnable) == AppConstants.MY_STATS_INDEX {
            navigationItem.rightBarButtonItem = nil
        }else{
            navigationItem.rightBarButtonItem = self.setButtons()
        }
    }
    
    func setButtons() -> UIBarButtonItem{
        let gridView = UIButton.init(type: .custom)
        gridView.setImage(UIImage(named: AppConstants.Images.GRID_VIEW_BUTTON_ICON), for: .normal)
        gridView.addTarget(self, action: #selector(gridViewClick), for: .touchUpInside)

        let listView = UIButton.init(type: .custom)
        listView.setImage(UIImage(named: AppConstants.Images.LIST_VIEW_BUTTON_ICON), for: .normal)
        listView.addTarget(self, action: #selector(listViewClick), for: .touchUpInside)
        
        let stackview = UIStackView.init(arrangedSubviews: [gridView, listView])
        stackview.distribution = .equalSpacing
        stackview.axis = .horizontal
        stackview.alignment = .center
        stackview.spacing = 10

        let rightBarButton = UIBarButtonItem(customView: stackview)
        return rightBarButton
    }
    
    @objc func gridViewClick(){
       NotificationCenter.default.post(name: Notification.Name(AppConstants.Notifications.BAR_BUTTON),object: nil,userInfo:[AppConstants.Notifications.BAR_BUTTON_ENABLE: AppConstants.TYPE_GRID])
    }
    
    @objc func listViewClick(){
       NotificationCenter.default.post(name: Notification.Name(AppConstants.Notifications.BAR_BUTTON),object: nil,userInfo:[AppConstants.Notifications.BAR_BUTTON_ENABLE: AppConstants.TYPE_LIST])
    }
    
    override func updateIndicator(for viewController: PagerTabStripViewController, fromIndex: Int, toIndex: Int, withProgressPercentage progressPercentage: CGFloat, indexWasChanged: Bool) {
        super.updateIndicator(for: viewController, fromIndex: fromIndex, toIndex: toIndex, withProgressPercentage: progressPercentage, indexWasChanged: indexWasChanged)
        
        if indexWasChanged {
            if progressPercentage != 1.0 {
                NotificationCenter.default.post(name: Notification.Name(AppConstants.Notifications.BAR_BUTTON),object: nil,userInfo:[AppConstants.Notifications.BAR_BUTTON_ENABLE: currentIndex])
                print("currentIndex",currentIndex)
            }
        }
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        let tabSalePurchaseViewController = UIStoryboard(name: AppConstants.Storyboard.SALE_PURCHASE_LIST_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.SALE_PURCHASE_SCREEN) as! SalePurchaseViewController
        tabSalePurchaseViewController.TYPE = self.TYPE
        
        let tabStatsViewController = UIStoryboard(name: AppConstants.Storyboard.STATS_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.Controllers.MY_STATS_SCREEN) as! StatsViewController
        tabStatsViewController.TYPE = self.TYPE
        
        return [tabSalePurchaseViewController,tabStatsViewController]
    }

    func loadDesign() {
        self.settings.style.buttonBarBackgroundColor = UIColor.white
        self.settings.style.buttonBarItemBackgroundColor = UIColor.white
        self.settings.style.selectedBarBackgroundColor = UIColor.cFB9D52
        self.settings.style.buttonBarItemFont = UIFont.fontSemiBold(size: 12)
        self.settings.style.selectedBarHeight = 2.0
        self.settings.style.buttonBarMinimumLineSpacing = 0
        self.settings.style.buttonBarItemTitleColor = UIColor.black
        self.settings.style.buttonBarItemsShouldFillAvailableWidth = true
        self.settings.style.buttonBarLeftContentInset = 10
        self.settings.style.buttonBarRightContentInset = 10
        
        changeCurrentIndexProgressive = { (oldCell: ButtonBarViewCell?, newCell: ButtonBarViewCell?, progressPercentage: CGFloat, changeCurrentIndex: Bool, animated: Bool) -> Void in
            guard changeCurrentIndex == true else { return }
            
            oldCell?.label.textColor = UIColor.c2A2A2A
            oldCell?.label.font = UIFont.fontSemiBold(size: 12)
            newCell?.label.textColor = UIColor.cBB189C
            newCell?.label.font = UIFont.fontSemiBold(size: 12)
            
        }
    }
    
}
