import UIKit

class ReportAnIssueViewController: BaseViewController,CancelPurchaseProtocol {
    
    func cancelPurchase(purchase: PurchaseDetail) {
        self.dismiss(animated: false, completion: nil)
        self.view.makeToastActivity(.center)
        let id = purchase.purchaseId!
        self.cancelpurchase(id: id)
    }
    
    @IBOutlet var itemTitleLabel:UILabel!
    @IBOutlet var itemImageView:UIImageView!
    @IBOutlet var itemStatusLabel:UILabel!
    @IBOutlet var statusImageView: UIImageView!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var itemNotShippedView:UIView!
    @IBOutlet var itemShippedView:UIView!
    @IBOutlet var submitButton:UIButton!
    @IBOutlet var issueTextView:UITextView!
    
    @IBOutlet  var evidenceOneRemoveButton: UIButton!
    @IBOutlet  var evidenceTwoRemoveButton: UIButton!
    @IBOutlet  var evidenceThreeRemoveButton: UIButton!
    @IBOutlet  var evidenceFourRemoveButton: UIButton!
    @IBOutlet  var mainScrollView: UIScrollView!
    @IBOutlet  var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet  var dropDownLabel: UILabel!
    
    @IBOutlet  var photoOneImageView: UIImageView!
    @IBOutlet  var photoTwoImageView: UIImageView!
    @IBOutlet  var photoThreeImageView: UIImageView!
    @IBOutlet  var photoFourImageView: UIImageView!

    @IBOutlet weak var onSubmitActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet var textViewBottomConstraint: NSLayoutConstraint!
    
    
    var imageList:[UIImage] = []
    //var TYPE:Int?
    
    var disputeTypeSelect = [DisputeReasons]()
    var selectedIssueTypes = 0
    let profileViewModel = ProfileViewModel()
    let imagePickerManager = ImagePickerManager()
    var image:UIImage?
    
    let TYPE_REPORT_AN_ISSUE: Int = 1
    
    var saleDetail:SalesDetail?
    var purchaseDetail:PurchaseDetail?
    var reportAnIssueDelegate:ReportAnIssueProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.Strings.REPORT_AN_ISSUE_TITLE
        getReportAnIssueType()
        getDisputeType()
        setData()
        onSubmitActivityIndicator.isHidden = true
        //hideKeyboardWhenTappedAround()
        let backBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.BACK_BUTTON), style: .plain, target: self, action: #selector(onBackBarButtonItem))
        self.navigationItem.leftBarButtonItem  = backBarButton
        photoOneImageView.image = UIImage(named: AppConstants.Images.ADD_PHOTO_ICON)
        photoTwoImageView.image = UIImage(named: AppConstants.Images.ADD_PHOTO_ICON)
        photoThreeImageView.image = UIImage(named: AppConstants.Images.ADD_PHOTO_ICON)
        photoFourImageView.image = UIImage(named: AppConstants.Images.ADD_PHOTO_ICON)
        keyboardRequiredForTextView(topConstraint: topConstraint)
        NotificationCenter.default.addObserver(self, selector: #selector(ReportAnIssueViewController.keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ReportAnIssueViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    
    }
    
    @objc func onBackBarButtonItem(){
        self.navigationController?.popViewController(animated: true)
    }
    
   @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0{
                self.view.frame.origin.y -= keyboardSize.height * 0.70
                self.view.layoutIfNeeded()
            }
        }

    }

    @objc func keyboardWillHide(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y += keyboardSize.height * 0.70
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func setData(){
        if saleDetail == nil{
            let text = "\"" + (self.purchaseDetail?.listingTitle!)! + "\""
            let title =  self.purchaseDetail?.listingTitle!
            self.setTitleText(text:text,title:title!)
            let imagePlaceholder = UIImage(named: AppConstants.Images.ITEM_PLACEHOLDER)
            if let imageUrl = purchaseDetail?.listingImageUrl{
                itemImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: imagePlaceholder)
            }else{
                itemImageView.image = imagePlaceholder
            }
            self.dateLabel.text = "on " + (self.purchaseDetail?.purchaseDateFormatted!)!
            self.itemStatusLabel.text = self.purchaseDetail?.purchaseStatus!
            self.setIcons(purchaseStatusId: (self.purchaseDetail?.purchaseStatusId!)!)
        }else{
            let text = "\"" + (self.saleDetail?.listingTitle!)! + "\""
            let title =  self.saleDetail?.listingTitle!
            self.setTitleText(text:text,title:title!)
            let imagePlaceholder = UIImage(named: AppConstants.Images.ITEM_PLACEHOLDER)
            if let imageUrl = saleDetail?.listingImageUrl{
                itemImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: imagePlaceholder)
            }else{
                itemImageView.image = imagePlaceholder
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        itemImageView.layer.masksToBounds = true
        itemImageView.layer.borderWidth = 1
        itemImageView.layer.borderColor = UIColor.black.cgColor
        itemImageView.layer.cornerRadius = 1

    }
    
    func getReportAnIssueType(){
        if purchaseDetail!.canCancelPurchase! && purchaseDetail!.purchaseStatusId == AppConstants.PURCHASE_STATUS_ID{
            self.itemNotShippedView.isHidden = false
            self.itemShippedView.isHidden = true
        }else{
            self.itemShippedView.isHidden = false
            self.itemNotShippedView.isHidden = true
            self.submitButton.isHidden = false
        }
        self.viewDidLayoutSubviews()
    }
    
    func setTitleText(text:String,title:String){
        let text:String = text
        let title:String = title
        let rangePink = (text as NSString).range(of: title)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cBB189C, range: rangePink)
        itemTitleLabel.attributedText = attributedString
    }
    
    func setIcons(purchaseStatusId:Int) {
        switch (purchaseStatusId) {
            case 1:
                statusImageView.image = UIImage(named: AppConstants.Images.HOUR_GLASS_ICON)
                break
            case 2:
                statusImageView.image = UIImage(named: AppConstants.Images.HOUR_GLASS_ICON)
                break
            case 3:
                statusImageView.image = UIImage(named: AppConstants.Images.HOUR_GLASS_ICON)
                break
            case 4:
                statusImageView.image = UIImage(named: AppConstants.Images.LABEL_BLACK_ICON)
                break
            case 5:
                statusImageView.image = UIImage(named: AppConstants.Images.TRUCK_ICON)
                break
            case 6:
                statusImageView.image = UIImage(named: AppConstants.Images.HOUR_GLASS_ICON)
                break
            case 7:
                statusImageView.image = UIImage(named: AppConstants.Images.DELIVERY_ICON)
                break
            case 8:
                statusImageView.image = UIImage(named: AppConstants.Images.HOUR_GLASS_ICON)
                break
            case 9:
                statusImageView.image = UIImage(named: AppConstants.Images.HOUR_GLASS_ICON)
                break
            case 10:
                statusImageView.image = UIImage(named: AppConstants.Images.HOUR_GLASS_ICON)
                break
            case 11:
                statusImageView.image = UIImage(named: AppConstants.Images.HOUR_GLASS_ICON)
                break
            case 12:
                statusImageView.image = UIImage(named: AppConstants.Images.HOUR_GLASS_ICON)
                break
        default:
            break
        }

    }
    
    @IBAction func cancelPurchaseAction(_ sender: UIButton) {
        reportAnIssueDialog()
    }
    
    func reportAnIssueDialog(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.WITHDRAW_DIALOG_BOARD, bundle: nil)
        let reportAnIssueDialogViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.WITHDRAW_DIALOG) as! WithdrawDialogViewController
        reportAnIssueDialogViewController.modalPresentationStyle = .overFullScreen
        reportAnIssueDialogViewController.TYPE = AppConstants.TYPE_REPORT_AN_ISSUE
        reportAnIssueDialogViewController.purchaseDetail = self.purchaseDetail!
        reportAnIssueDialogViewController.itemTitle = self.purchaseDetail?.listingTitle!
        reportAnIssueDialogViewController.cancelPurchaseDelegate = self
        self.navigationController?.present(reportAnIssueDialogViewController, animated: false, completion: nil)
    }
    
    @IBAction func submitButtonAction(_ sender: UIButton) {
        sendData()
    }
    
    func sendData() {
        if self.selectedIssueTypes == 0{
            self.view.makeToast(AppConstants.Strings.SELECT_ISSUE_TO_ADD_DISPUTE, duration: 3.0, position: .center)
        }else{
            self.view.makeToastActivity(.center)
            let comment = self.issueTextView.text.trimmingCharacters(in: .whitespacesAndNewlines)
            var dispute = Dispute()
            var disputeIssueId = 0
            
            disputeIssueId = self.selectedIssueTypes
            dispute.comments = comment
            dispute.disputeTypeId = disputeIssueId
            dispute.purchaseId =  self.purchaseDetail?.purchaseId!
            
            if photoOneImageView.image != UIImage(named: AppConstants.Images.ADD_PHOTO_ICON){
                self.imageList.append(photoOneImageView!.image!)
            }
            if photoTwoImageView.image != UIImage(named: AppConstants.Images.ADD_PHOTO_ICON){
                self.imageList.append(photoTwoImageView!.image!)
            }
            if photoThreeImageView.image != UIImage(named: AppConstants.Images.ADD_PHOTO_ICON){
                self.imageList.append(photoThreeImageView!.image!)
            }
            if photoFourImageView.image != UIImage(named: AppConstants.Images.ADD_PHOTO_ICON){
                self.imageList.append(photoFourImageView!.image!)
            }
            sendApi(dispute: dispute)
        }
    }
    
    func uploadImages(id:Int,imageList:[UIImage]){
        profileViewModel.uploadReportAnIssueImages(accessToken: getAccessToken(), id: id,images:self.imageList, { (response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 201 || response.response?.statusCode == 200{
                self.reportAnIssueDelegate?.reportAnIssue()
                self.navigationController?.popViewController(animated: false)
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func sendApi(dispute:Dispute){
        profileViewModel.reportAnIssue(accessToken: getAccessToken(), data: dispute, { (response) in
            if response.response?.statusCode == 201 || response.response?.statusCode == 200{
                let data: ApiResponse<Int> = Utils.convertResponseToData(data: response.data!)
                if self.imageList.count > 0{
                    if data.isError {
                        let error = Utils.getErrorMessage(errors: data.errors)
                        self.showMessageDialog(message: error)
                    }else{
                        let id = data.data!
                        self.uploadImages(id:id,imageList: self.imageList)
                    }
                }else{
                    self.view.hideToastActivity()
                    self.reportAnIssueDelegate?.reportAnIssue()
                    self.navigationController?.popViewController(animated: false)
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    @IBAction func issueTypeDropDownAction(_ sender: UITapGestureRecognizer) {
        showDropDown(tag: TYPE_REPORT_AN_ISSUE, title:  AppConstants.Strings.SELECT_AN_ISSUE_TYPE, dataSourse: self, delegate: self, {(row) in
            self.dropDownLabel.textColor = UIColor.black
            self.dropDownLabel.text = self.disputeTypeSelect[row].disputeTypeName!
            self.selectedIssueTypes = self.disputeTypeSelect[row].id!
        })
    }
    
    @IBAction func evidencePhotoOneAction(_ sender: UITapGestureRecognizer) {
        if photoOneImageView.image == UIImage(named: AppConstants.Images.ADD_PHOTO_ICON){
            imagePickerManager.pickImage(self,cameraTitle: AppConstants.CAMERA,galleryTitle: AppConstants.GALLERY){ image in
                self.photoOneImageView.image = image
                self.evidenceOneRemoveButton.isHidden = false
                self.photoOneImageView.layer.masksToBounds = true
                self.photoOneImageView.layer.borderWidth = 1
                self.photoOneImageView.layer.borderColor = UIColor.black.cgColor
                self.photoOneImageView.layer.cornerRadius = 5
            }
        }else{
            print("Hello world")
        }
    }
    
    @IBAction func evidencePhotoTwoAction(_ sender: UITapGestureRecognizer) {
        if photoTwoImageView.image == UIImage(named: AppConstants.Images.ADD_PHOTO_ICON){
            imagePickerManager.pickImage(self,cameraTitle: AppConstants.CAMERA,galleryTitle: AppConstants.GALLERY){ image in
                self.photoTwoImageView.image = image
                self.evidenceTwoRemoveButton.isHidden = false
                self.photoTwoImageView.layer.masksToBounds = true
                self.photoTwoImageView.layer.borderWidth = 1
                self.photoTwoImageView.layer.borderColor = UIColor.black.cgColor
                self.photoTwoImageView.layer.cornerRadius = 5
            }
        }
    }
    
    @IBAction func evidencePhotoThreeAction(_ sender: UITapGestureRecognizer) {
        if photoThreeImageView.image == UIImage(named: AppConstants.Images.ADD_PHOTO_ICON){
            imagePickerManager.pickImage(self,cameraTitle: AppConstants.CAMERA,galleryTitle: AppConstants.GALLERY){ image in
                self.photoThreeImageView.image = image
                self.evidenceThreeRemoveButton.isHidden = false
                self.photoThreeImageView.layer.masksToBounds = true
                self.photoThreeImageView.layer.borderWidth = 1
                self.photoThreeImageView.layer.borderColor = UIColor.black.cgColor
                self.photoThreeImageView.layer.cornerRadius = 5
            }
        }
    }
    
    @IBAction func evidencePhotoFourAction(_ sender: UITapGestureRecognizer){
        if photoFourImageView.image == UIImage(named: AppConstants.Images.ADD_PHOTO_ICON){
            imagePickerManager.pickImage(self,cameraTitle: AppConstants.CAMERA,galleryTitle: AppConstants.GALLERY){ image in
                self.photoFourImageView.image = image
                self.evidenceFourRemoveButton.isHidden = false
                self.photoFourImageView.layer.masksToBounds = true
                self.photoFourImageView.layer.borderWidth = 1
                self.photoFourImageView.layer.borderColor = UIColor.black.cgColor
                self.photoFourImageView.layer.cornerRadius = 5
            }
        }
    }
    
    @IBAction func removeEvidenceOne(_ sender: UIButton) {
        photoOneImageView.image = UIImage(named: AppConstants.Images.ADD_PHOTO_ICON)
        self.evidenceOneRemoveButton.isHidden = true
        self.photoOneImageView.layer.cornerRadius = 0
        self.photoOneImageView.layer.masksToBounds = false
        self.photoOneImageView.layer.borderWidth = 0
        self.photoOneImageView.layer.borderColor = UIColor.clear.cgColor
        
    }
    
    @IBAction func removeEvidenceTwoAction(_ sender: UIButton) {
        photoTwoImageView.image = UIImage(named: AppConstants.Images.ADD_PHOTO_ICON)
        self.evidenceTwoRemoveButton.isHidden = true
        self.photoTwoImageView.layer.masksToBounds = false
        self.photoTwoImageView.layer.borderWidth = 0
        self.photoTwoImageView.layer.borderColor = UIColor.clear.cgColor
        self.photoTwoImageView.layer.cornerRadius = 0
    }
    
    @IBAction func removeEvidenceThreeAction(_ sender: UIButton) {
        photoThreeImageView.image = UIImage(named: AppConstants.Images.ADD_PHOTO_ICON)
        self.evidenceThreeRemoveButton.isHidden = true
        self.photoThreeImageView.layer.masksToBounds = false
        self.photoThreeImageView.layer.borderWidth = 0
        self.photoThreeImageView.layer.borderColor = UIColor.clear.cgColor
        self.photoThreeImageView.layer.cornerRadius = 0
    }
    
    @IBAction func removeEvidenceFourAction(_ sender: UIButton) {
        photoFourImageView.image = UIImage(named: AppConstants.Images.ADD_PHOTO_ICON)
        self.evidenceFourRemoveButton.isHidden = true
        self.photoFourImageView.layer.masksToBounds = false
        self.photoFourImageView.layer.borderWidth = 0
        self.photoFourImageView.layer.borderColor = UIColor.clear.cgColor
        self.photoFourImageView.layer.cornerRadius = 0
    }
    
    
    @IBAction func cancelPurchaseButtonWithOrLabel(_ sender: UIButton) {
        reportAnIssueDialog()
    }
    
    func cancelpurchase(id:Int) {
        profileViewModel.cancelPurchase(accessToken: getAccessToken(),id:id ,{(response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Bool> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.view.hideToastActivity()
                    self.reportAnIssueDelegate?.reportAnIssue()
                    self.navigationController?.popViewController(animated: false)
                }
            } else {
                self.view.hideToastActivity()
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
}

extension ReportAnIssueViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return disputeTypeSelect.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return disputeTypeSelect[row].disputeTypeName
    }
    
    func getDisputeType() {
        profileViewModel.getDisputes(accessToken: getAccessToken(), {(response) in
            self.screenActivityIndicator.stopAnimating()
            self.mainScrollView.isHidden = false
            if response.response?.statusCode == 200 {
                let data: ApiResponse<[DisputeReasons]> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.disputeTypeSelect = data.data!
                }
            } else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            self.mainScrollView.isHidden = false
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
}
