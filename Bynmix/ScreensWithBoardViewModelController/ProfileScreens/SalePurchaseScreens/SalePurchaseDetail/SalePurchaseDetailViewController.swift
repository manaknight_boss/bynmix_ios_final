import UIKit

class SalePurchaseDetailViewController: BaseViewController,OnFeedbackSubmitClickProtocol,PrintShippingLabelProtocol,ReportAnIssueProtocol,CancelOrderSaleProtocol,CancelDisputeRefreshProtocol,CancelPurchaseProtocol,PrintReturnLabelProtocol,RefreshSalePurchaseProtocol{
    
    func refreshSalePurchase() {
        self.view.makeToastActivity(.center)
        if TYPE == AppConstants.TYPE_PURCHASE{
            self.getPurchaseDetail(id:self.id!)
        }else{
            self.getSaleDetail(id:self.id!)
        }
    }
    
    func printReturnLabel(type:Int,id:Int){
        if type == AppConstants.TYPE_PRINT_RETURN_LABEL{
            self.emailReturnLabel(id:id)
        }else if type == AppConstants.TYPE_RETURN_LABEL_DONE{
            self.pageRedirectToSaleDelegate?.pageRedirectToSale()
        }
    }
    
    func cancelPurchase(purchase: PurchaseDetail) {
        self.dismiss(animated: false, completion: nil)
        self.cancelpurchase(id:purchase.purchaseId!)
    }
    
    func cancelDisputeRefresh(){
        self.successMessageDialog()
        if TYPE == AppConstants.TYPE_PURCHASE{
            self.getPurchaseDetail(id:self.id!)
        }else{
            self.getSaleDetail(id:self.id!)
        }
    }
    
    func cancelOrderSale(listingTitle: String) {
        self.openCancelOrderSaleDialog(title:listingTitle)
        if TYPE == AppConstants.TYPE_PURCHASE{
            self.getPurchaseDetail(id:self.id!)
        }else{
            self.getSaleDetail(id:self.id!)
        }
    }
    
    func reportAnIssue() {
        if TYPE == AppConstants.TYPE_PURCHASE{
            self.getPurchaseDetail(id:self.id!)
        }else{
            self.getSaleDetail(id:self.id!)
        }
    }
    
    func PrintShippingLabel(type: Int, id: Int) {
        if type == AppConstants.TYPE_PRINT_SHIPPING_LABEL{
            self.emailShippingLabel(id:id)
        }else if type == AppConstants.TYPE_SHIPPING_LABEL_DONE{
//            self.view.makeToastActivity(.center)
//            self.getSaleDetail(id: self.id! )
            self.pageRedirectToSaleDelegate?.pageRedirectToSale()
        }
    }
    
    func onFeedbackSubmitClick(type: Int, id: Int, rating: Int, comment: String) {
        print(id,rating,comment)
        if type == AppConstants.TYPE_PURCHASE{
            purchaseFeedback(id:id,rating:rating,comment:comment)
        }else{
            sellerFeedback(id:id,rating:rating,comment:comment)
        }
    }
    
    @IBOutlet var buyerImageView: UIImageView!
    @IBOutlet var buyerShadowView: UIView!
    @IBOutlet var soldPurchasedTextLabel: UILabel!
    @IBOutlet var buyerUsernameLabel: UILabel!
    @IBOutlet var heartButton: UIButton!
    @IBOutlet var listingTitleLabel: UILabel!
    @IBOutlet var dateFormattedLabel: UILabel!
    @IBOutlet var listingImageView: UIImageView!
    @IBOutlet var originalPriceLabel: UILabel!
    @IBOutlet var shippingPriceLabel: UILabel!
    @IBOutlet var totalPricelabel: UILabel!
    @IBOutlet var cancelOrderButton: UIButton!
    @IBOutlet var leaveFeedBackButton: UIButton!
    @IBOutlet var ratingView: UIView!
    @IBOutlet var starOneImageView: UIImageView!
    @IBOutlet var starTwoImageView: UIImageView!
    @IBOutlet var starThreeImageView: UIImageView!
    @IBOutlet var starFourImageView: UIImageView!
    @IBOutlet var starFiveImageView: UIImageView!
    @IBOutlet var reportAnIssueButton: UIButton!
    @IBOutlet var disputeDetailButton: UIButton!
    
    @IBOutlet var shippingDetailsTopView: UIView!
    @IBOutlet var saleDetailTopView: UIView!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var deliveredDateLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var stateLabel: UILabel!
    @IBOutlet var defaultAddressLabel: UILabel!
    @IBOutlet var viewShippingHistoryButton: UIButton!
    @IBOutlet var detailTypeLabel: UILabel!
    @IBOutlet var cardImageView: UIImageView!
    @IBOutlet var brandLabel: UILabel!
    @IBOutlet var lastFourLabel: UILabel!
    @IBOutlet var paidPriceLabel: UILabel!
    @IBOutlet var paidShippingPriceLabel: UILabel!
    @IBOutlet var discountLabel: UILabel!
    @IBOutlet var discountPriceLabel: UILabel!
    @IBOutlet var totalPaidPriceLabel: UILabel!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var paidWithView: UIView!
    @IBOutlet var statusView: UIView!
    
    @IBOutlet var salesTaxLabel: UILabel!
    @IBOutlet var pendingPointsLabel: UILabel!
    @IBOutlet var saleTaxView: UIView!
    
    @IBOutlet var discountImageView: UIImageView!
    @IBOutlet var freeShippingImageView: UIImageView!
    @IBOutlet var promotionImageView: UIImageView!
    @IBOutlet weak var returnView: UIView!
    @IBOutlet weak var returnStatusLabel: UILabel!
    @IBOutlet weak var returnAddressFullLabel: UILabel!
    @IBOutlet weak var returnNameLabel: UILabel!
    @IBOutlet weak var returnAddressLabel: UILabel!
    @IBOutlet weak var returnStateLabel: UILabel!
    @IBOutlet weak var defaultReturnAdressLabel: UILabel!
    @IBOutlet weak var printReturnButton: UIButton!
    @IBOutlet weak var returnStatusView: UIView!
    @IBOutlet weak var returnDetailsTopView: UIView!
    @IBOutlet weak var paidWithLabel: UILabel!
    
    @IBOutlet var salesTaxView: UIView!
    @IBOutlet var salesTaxBottomLabel: UILabel!
    
    let profileViewModel = ProfileViewModel()
    
    var id:Int?
    var TYPE:Int?
    var isEnabled = false
    var sale:SalesDetail?
    var purchase:PurchaseDetail?
    
    var userId: Int = 0
    var userName: String = ""
    var indexOfItem:Int? = 0
    var pageRedirectToSaleDelegate:PageRedirectToSaleProtocol?
    var pageRedirectToSaleListDelegate:PageRedirectToSaleListProtocol?
    var pageRedirectToPurchaseListDelegate:PageRedirectToPurchaseListProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        onTypeChange()
        let usernameTap = UITapGestureRecognizer(target: self, action: #selector(userClicked))
        let buyerShadowTap = UITapGestureRecognizer(target: self, action: #selector(userClicked))
        let buyerImageViewTap = UITapGestureRecognizer(target: self, action: #selector(userClicked))
        buyerUsernameLabel.addGestureRecognizer(usernameTap)
        buyerShadowView.addGestureRecognizer(buyerShadowTap)
        buyerImageView.addGestureRecognizer(buyerImageViewTap)
        buyerImageView.isUserInteractionEnabled = true
        buyerUsernameLabel.isUserInteractionEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let backBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.BACK_BUTTON), style: .plain, target: self, action: #selector(onBackBarButtonItem))
        self.navigationItem.leftBarButtonItem  = backBarButton
    }
    
    @objc func onBackBarButtonItem(){
        if TYPE == AppConstants.TYPE_PURCHASE{
            if purchase != nil{
                self.pageRedirectToPurchaseListDelegate?.pageRedirectToPurchaseList(purchase: purchase!)
            }
        }else{
            if sale != nil{
                self.pageRedirectToSaleListDelegate?.pageRedirectToSaleList(sale: sale!)
            }
        }
        self.navigationController?.popViewController(animated: false)
    }
    
    func onTypeChange(){
        self.buyerShadowView.layer.shadowColor = UIColor.black.cgColor
        self.buyerShadowView.alpha = CGFloat(0.26)
        self.buyerShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        self.buyerShadowView.layer.shadowOpacity = 1
        self.buyerShadowView.layer.cornerRadius = self.buyerShadowView.frame.size.width/2
        
        self.buyerImageView.layer.borderWidth = 2.0
        self.buyerImageView.layer.masksToBounds = false
        self.buyerImageView.layer.borderColor = UIColor.white.cgColor
        self.buyerImageView.clipsToBounds = true
        self.buyerImageView.layer.cornerRadius = self.buyerImageView.frame.size.width/2
        
        self.shippingDetailsTopView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        self.saleDetailTopView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        
        if TYPE == AppConstants.TYPE_PURCHASE{
            navigationItem.title = AppConstants.Strings.PURCHASE_DETAIL
            saleTaxView.isHidden = false
            pendingPointsLabel.isHidden = false
            getPurchaseDetail(id:self.id!)
        }else{
            navigationItem.title = AppConstants.Strings.SALES_DETIAL
            saleTaxView.isHidden = true
            pendingPointsLabel.isHidden = false
            getSaleDetail(id:self.id!)
        }
    }
    
    @IBAction func heartButtonAction(_ sender: UIButton) {
        if self.heartButton.currentImage == UIImage(named: AppConstants.Images.LISTING_DETAIL_HEART){
            self.heartButton.setImage(UIImage(named: AppConstants.Images.LISTING_DETAIL_HEART_SELECTED), for: .normal)
        }else{
            self.heartButton.setImage(UIImage(named: AppConstants.Images.LISTING_DETAIL_HEART), for: .normal)
        }
        
        if TYPE == AppConstants.TYPE_PURCHASE{
            let sellerId = self.purchase!.buyerAndSellerInfo?.sellerId!
            self.likeStatus(id: sellerId!)
        }else{
            let buyerId = self.sale!.buyerAndSellerInfo?.buyerId!
            self.likeStatus(id: buyerId!)
        }
    }
    
    @IBAction func returnShippingButton(_ sender: UIButton) {
        var id = 0
        var title = ""
        if TYPE == AppConstants.TYPE_PURCHASE{
            id = (purchase?.purchaseId!)!
            title = (purchase?.listingTitle!)!
            self.trackingInfoDialog(id:id,title: title)
            
        }else{
            title = (sale?.listingTitle!)!
            id = (sale?.saleId!)!
            if (sale?.canPrintShippingLabel!)!{
                self.getShippingRate(id:id,title:title, salesId: sale!.saleId!)
            }else{
                self.trackingInfoDialog(id:id,title:title)
            }
        }
        
    }
    
    func getShippingRate(id:Int,title:String, salesId: Int) {
        self.view.makeToastActivity(.center)
        profileViewModel.shippingRate(accessToken: AppDefaults.getAccessToken(), id: id, { (response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<ShippingRate> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    let shipping = data.data!
                    let shippingRate:Double = shipping.shippingRate!
                    self.emailShippingLabelDialog(id:salesId, title:title,shippingRate:shippingRate)
                }
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.view.hideToastActivity()
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func trackingInfoDialog(id:Int,title:String){
        
//       var canReturnInfoForReturnLabel = false
//
//       if self.TYPE == AppConstants.TYPE_PURCHASE{
//           if  self.purchase?.purchaseStatusId == AppConstants.RETURN_LABEL_PRINTED || self.purchase?.purchaseStatusId == AppConstants.RETURN_ORDER_SHIPPED || self.purchase?.purchaseStatusId == AppConstants.RETURN_ORDER_IN_TRANSIT || self.purchase?.purchaseStatusId == AppConstants.RETURN_ORDER_DELIVERED{
//               canReturnInfoForReturnLabel = true
//           }else{
//               canReturnInfoForReturnLabel = false
//           }
//       }else{
//           if self.sale?.saleStatusId == AppConstants.RETURN_LABEL_PRINTED || self.sale?.saleStatusId == AppConstants.RETURN_ORDER_SHIPPED || self.sale?.saleStatusId == AppConstants.RETURN_ORDER_IN_TRANSIT || self.sale?.saleStatusId == AppConstants.RETURN_ORDER_DELIVERED{
//               canReturnInfoForReturnLabel = true
//           }else{
//               canReturnInfoForReturnLabel = false
//           }
//
//       }
        
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.DELIVERY_STATUS_DIALOG_BOARD, bundle:nil)
        let deliveryStatusDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.DELIVERY_STATUS_DIALOG_SCREEN) as! DeliveryStatusDialogViewController
        deliveryStatusDialogViewController.modalPresentationStyle = .overFullScreen
        deliveryStatusDialogViewController.id = id
        deliveryStatusDialogViewController.TYPE = self.TYPE
        deliveryStatusDialogViewController.listingTitle = title
        //deliveryStatusDialogViewController.canReturnInfoForReturnLabel = canReturnInfoForReturnLabel
        self.navigationController?.present(deliveryStatusDialogViewController, animated:false ,completion: nil)
    }
    
    func emailShippingLabelDialog(id:Int,title:String,shippingRate:Double){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.PRINT_SHIPPING_LABEL_DIALOG_BOARD, bundle:nil)
        let printShippingLabelDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.PRINT_SHIPPING_LABEL_DIALOG_SCREEN) as! PrintShippingLabelDialogViewController
        printShippingLabelDialogViewController.modalPresentationStyle = .overFullScreen
        printShippingLabelDialogViewController.id = id
        printShippingLabelDialogViewController.TYPE = AppConstants.TYPE_PRINT_SHIPPING_LABEL
        printShippingLabelDialogViewController.listingTitle = title
        printShippingLabelDialogViewController.shippingRate = shippingRate
        printShippingLabelDialogViewController.printShippingLabelDelegate = self
        self.navigationController?.present(printShippingLabelDialogViewController, animated:false,completion: nil)
    }
    
    func emailReturnLabelDialog(id:Int,title:String){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.PRINT_SHIPPING_LABEL_DIALOG_BOARD, bundle:nil)
        let printShippingLabelDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.PRINT_SHIPPING_LABEL_DIALOG_SCREEN) as! PrintShippingLabelDialogViewController
        printShippingLabelDialogViewController.modalPresentationStyle = .overFullScreen
        printShippingLabelDialogViewController.id = id
        printShippingLabelDialogViewController.TYPE = AppConstants.TYPE_PRINT_RETURN_LABEL
        printShippingLabelDialogViewController.listingTitle = title
        printShippingLabelDialogViewController.printReturnLabelDelegate = self
        self.navigationController?.present(printShippingLabelDialogViewController, animated:false,completion: nil)
    }
    
    func setSalesDetail(sale:SalesDetail){
        
        let text:String = AppConstants.Strings.PAID_WITH
        let textBold:String = AppConstants.Strings.PAID_TEXT
        let textMedium:String = "WITH"
        let range = (text as NSString).range(of: textBold)
        let rangeMedium = (text as NSString).range(of: textMedium)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 14), range: range)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontMedium(size: 13), range: rangeMedium)
        self.paidWithLabel.attributedText = attributedString
        
        self.listingTitleLabel.text = sale.listingTitle
        self.dateFormattedLabel.text = AppConstants.SOLD_ON + sale.saleDateFormatted!
        let image = UIImage(named: AppConstants.Images.DEFAULT_USER)
        let defaultListingImage =  UIImage(named: AppConstants.Images.ITEM_PLACEHOLDER)!
        if let imageUrl = sale.buyerAndSellerInfo?.buyerImageUrl{
            self.buyerImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        } else {
            self.buyerImageView.image =  image
        }
        if let imageUrl = sale.listingImageUrl{
            self.listingImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: defaultListingImage)
        } else {
            self.listingImageView.image =  image
        }
        if (sale.buyerAndSellerInfo?.loggedInUserFollowingBuyer)!{
            self.heartButton.setImage(UIImage(named: AppConstants.Images.LISTING_DETAIL_HEART_SELECTED), for: .normal)
        }else{
            self.heartButton.setImage(UIImage(named: AppConstants.Images.LISTING_DETAIL_HEART), for: .normal)
        }
        
        self.soldPurchasedTextLabel.text = AppConstants.Strings.PURCHASED_BY
        self.buyerUsernameLabel.text = sale.buyerAndSellerInfo?.buyerUsername!
        self.userId = (sale.buyerAndSellerInfo?.buyerId)!
        self.userName = (sale.buyerAndSellerInfo?.buyerUsername)!
        self.detailTypeLabel.text = AppConstants.Strings.SALE_DETAILS
        
        self.originalPriceLabel.text = AppConstants.DOLLAR + String(format: AppConstants.FORMAT_ZERO_DECIMAL ,sale.saleOriginalPrice ?? 0)
        self.shippingPriceLabel.text = AppConstants.DOLLAR + String(format: AppConstants.FORMAT_ZERO_DECIMAL ,sale.saleShippingPrice ?? 0)
        self.totalPricelabel.text = AppConstants.DOLLAR + String(format: AppConstants.FORMAT_ZERO_DECIMAL ,sale.saleTotalPrice!)
        
        self.paidPriceLabel.text = AppConstants.DOLLAR + String(format: AppConstants.FORMAT_ZERO_DECIMAL ,sale.saleOriginalPrice ?? 0)
        self.paidShippingPriceLabel.text = AppConstants.DOLLAR + String(format: AppConstants.FORMAT_ZERO_DECIMAL, sale.saleShippingPrice ?? 0)
        self.discountLabel.text = AppConstants.Strings.BYNMIX_COMMISSION
        self.discountPriceLabel.text = AppConstants.MINUS_SIGN + AppConstants.DOLLAR + String(format: AppConstants.FORMAT_ZERO_DECIMAL ,sale.saleBynmixCommission ?? 0)
        self.totalPaidPriceLabel.text = AppConstants.DOLLAR + String(format: AppConstants.FORMAT_ZERO_DECIMAL ,sale.saleProfit ?? 0)
        self.paidWithView.isHidden = true
        
        if (sale.canOpenDispute! || sale.hasOpenDispute! || sale.hasViewableDispute!){
            self.reportAnIssueButton.backgroundColor = UIColor.black
            self.reportAnIssueButton.isEnabled = true
        }else{
            self.reportAnIssueButton.backgroundColor = UIColor.cCBCBCB
            self.reportAnIssueButton.isEnabled = false
        }
        
        if (sale.hasOpenDispute!) {
            self.disputeDetailButton.isHidden = false
            self.reportAnIssueButton.isHidden = true
        } else {
            if(sale.hasViewableDispute!) {
                self.disputeDetailButton.isHidden = false
                self.reportAnIssueButton.isHidden = true
            } else {
                self.reportAnIssueButton.isHidden = false
                self.disputeDetailButton.isHidden = true
            }
        }
        
        if sale.canLeaveFeedback!{
            self.leaveFeedBackButton.backgroundColor = UIColor.cBB189C
            self.leaveFeedBackButton.isEnabled = true
        }else{
            self.leaveFeedBackButton.backgroundColor = UIColor.cCBCBCB
            self.leaveFeedBackButton.isEnabled = false
        }
        
        if (sale.canCancelSale!) {
            self.cancelOrderButton.isHidden = false
            self.leaveFeedBackButton.isHidden = true
            self.ratingView.isHidden = true
        } else {
            self.cancelOrderButton.isHidden = true
            self.leaveFeedBackButton.isHidden = false
            self.ratingView.isHidden = true
            
            if (sale.rating ?? 0 > 0) {
                self.cancelOrderButton.isHidden = true
                self.leaveFeedBackButton.isHidden = true
                self.ratingView.isHidden = false
                let rating = sale.rating!
                
                if rating == 1{
                    self.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starTwoImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    self.starThreeImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    self.starFourImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    self.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                }else if rating == 2{
                    self.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starThreeImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    self.starFourImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    self.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                }else if rating == 3{
                    self.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starThreeImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starFourImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    self.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                }else if rating == 4{
                    self.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starThreeImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starFourImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                }else{
                    self.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starThreeImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starFourImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starFiveImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                }
                
                
            } else {
                self.cancelOrderButton.isHidden = true
                self.leaveFeedBackButton.isHidden = false
                self.cancelOrderButton.isOpaque = true
                self.ratingView.isOpaque = true
                self.ratingView.isHidden = true
            }
            
        }
        
        let status  = sale.shippedTo?.shippingStatus?.uppercased()
        var statusDetail = ""
        
        if  !(status ?? "").isEmpty{
            self.statusView.isHidden = false
            self.statusLabel.text = status
            
            if (status!.caseInsensitiveCompare(AppConstants.DELIVERED) == ComparisonResult.orderedSame){
                isEnabled = true
                statusDetail = setSaleDate(sale:sale)
            }else{
                statusDetail = ifShippingStatusDetailsEmptySale(sale: sale)
                isEnabled = false
            }
            
            if !statusDetail.isEmpty{
                self.deliveredDateLabel.isHidden = false
                self.deliveredDateLabel.text = statusDetail
            }else{
                self.deliveredDateLabel.isHidden = true
            }
            
        }else{
            self.statusView.isHidden = true
            isEnabled = false
        }
        
        self.nameLabel.text = sale.shippedTo?.fullName
        
        let addressOne = sale.shippedTo?.addressOne!
        let addressTwo = sale.shippedTo?.addressTwo!
        
        if  addressTwo!.isEmpty{
            self.addressLabel.text = addressOne
        }else{
            self.addressLabel.text = addressOne! + " " + addressTwo!
        }
        let defaultAddress = sale.shippedTo?.isDefaultAddress!
        
        if defaultAddress!{
            self.defaultAddressLabel.isHidden = false
        }else{
            self.defaultAddressLabel.isHidden = true
        }
        
        let city = sale.shippedTo?.city
        let state = sale.shippedTo?.state
        let zipcode = sale.shippedTo?.zipCode
        self.stateLabel.text = city! + " " + state! + " " + zipcode!
        
        if sale.canPrintShippingLabel!{
            self.viewShippingHistoryButton.setTitle(AppConstants.Strings.PRINT_SHIPPING_LABEL, for: .normal)
            self.statusView.isHidden = false
            self.deliveredDateLabel.isHidden = true
            self.statusLabel.text = AppConstants.Strings.NOT_YET_SHIPPED
        }else{
            self.viewShippingHistoryButton.setTitle(AppConstants.Strings.VIEW_SHIPPING_HISTORY, for: .normal)
        }
        
        if sale.purchasedWithPromotions?.count ?? 0 > 0{
            let promotions = sale.purchasedWithPromotions!
            for i in 0..<promotions.count{
                if promotions[i].promotionTypeId == AppConstants.FivePercentOff || promotions[i].promotionTypeId == AppConstants.TenPercentOff{
                    if let imageUrl = promotions[i].displayImageShort{
                        discountImageView.kf.setImage(with: URL(string: AppConstants.IMAGES_URL + imageUrl + AppConstants.IMAGES_EXTENSION))
                    }
                    discountImageView.isHidden = false
                }else{
                    discountImageView.isHidden = true
                }
                if promotions[i].promotionTypeId == AppConstants.FreeShipping || promotions[i].promotionTypeId == AppConstants.ThreeDollarShipping{
                    if let imageUrl = promotions[i].displayImageShort{
                        freeShippingImageView.kf.setImage(with: URL(string: AppConstants.IMAGES_URL + imageUrl + AppConstants.IMAGES_EXTENSION))
                    }
                    freeShippingImageView.isHidden = false
                }else{
                    freeShippingImageView.isHidden = true
                }
                if promotions[i].promotionTypeId == AppConstants.DoublePoints || promotions[i].promotionTypeId == AppConstants.TriplePoints ||  promotions[i].promotionTypeId == AppConstants.QuadruplePoints{
                    if let imageUrl = promotions[i].displayImageShort{
                        promotionImageView.kf.setImage(with: URL(string: AppConstants.IMAGES_URL + imageUrl + AppConstants.IMAGES_EXTENSION))
                    }
                    promotionImageView.isHidden = false
                }else{
                    promotionImageView.isHidden = true
                }
                
            }
            
        }else{
            discountImageView.isHidden = true
            freeShippingImageView.isHidden = true
            promotionImageView.isHidden = true
        }
        
        if sale.returnedTo != nil{
            returnView.isHidden = false
            self.returnDetailsTopView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
            
            let returnStatus  = sale.returnedTo?.returnStatus?.uppercased() ?? ""
            var statusDetail = ""
            
            if !(returnStatus).isEmpty{
                self.returnStatusView.isHidden = false
                self.returnStatusLabel.text = returnStatus
                
                if (returnStatus.caseInsensitiveCompare(AppConstants.DELIVERED) == ComparisonResult.orderedSame){
                    isEnabled = true
                    statusDetail = setSaleReturnDate(sale:sale)
                }else{
                    statusDetail = ifReturnStatusDetailsEmptySale(sale:sale)
                    isEnabled = false
                }
                
                if !(statusDetail.isEmpty){
                    self.returnAddressFullLabel.isHidden = false
                    self.returnAddressFullLabel.text = statusDetail
                }else{
                    self.returnAddressFullLabel.isHidden = true
                }
                
            }else{
                self.returnStatusView.isHidden = true
                isEnabled = false
            }
            
            self.returnNameLabel.text = sale.returnedTo?.fullName
            self.returnAddressLabel.text = sale.returnedTo?.addressOne
            let city = sale.returnedTo?.city ?? ""
            let state = sale.returnedTo?.state ?? ""
            let zipcode = sale.returnedTo?.zipCode ?? ""
            
            self.returnStateLabel.text = city + " " + state + " " + zipcode
            if sale.returnedTo?.isDefaultReturnAddress ?? false{
                self.defaultReturnAdressLabel.isHidden = false
            }else{
                self.defaultReturnAdressLabel.isHidden = true
            }
            
            self.printReturnButton.setTitle(AppConstants.Strings.VIEW_RETURN_HISTORY, for: .normal)
        }else{
            returnView.isHidden = true
        }
        
        if (sale.pointsStatusId ?? 0 > 0) {
            pendingPointsLabel.isHidden = false
            if (sale.pointsStatusId == 1) {
                pendingPointsLabel.textColor = UIColor.c00BCF0
                pendingPointsLabel.text = ("Points Earned: \(sale.points ?? 0) Points (Pending Feedback left)")
            } else if (sale.pointsStatusId == 2) {
                pendingPointsLabel.textColor = UIColor.c00A02A
                pendingPointsLabel.text = ("Points Earned: \(sale.points ?? 0) Points")
            } else if (sale.pointsStatusId == 3) {
                pendingPointsLabel.textColor = UIColor.black
                pendingPointsLabel.text = ("Points Retracted: \(sale.points ?? 0) Points")
            } else if (sale.pointsStatusId == 4) {
                pendingPointsLabel.textColor = UIColor.black
                pendingPointsLabel.text = ("Points Pending: \(sale.points ?? 0) Points (Pending Dispute)")
            } else {
                pendingPointsLabel.isHidden = true
            }
        } else {
            pendingPointsLabel.isHidden = true
        }
        
        self.salesTaxView.isHidden = true
        
    }
    
    func setPurchaseDetail(purchase:PurchaseDetail){
        let text:String = AppConstants.Strings.PAID_WITH
        let textBold:String = AppConstants.Strings.PAID_TEXT
        let textMedium:String = "WITH"
        let range = (text as NSString).range(of: textBold)
        let rangeMedium = (text as NSString).range(of: textMedium)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 14), range: range)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontMedium(size: 13), range: rangeMedium)
        self.paidWithLabel.attributedText = attributedString
        
        self.salesTaxLabel.text = AppConstants.DOLLAR + String(format:AppConstants.FORMAT_TWO_DECIMAL,purchase.salesTax ?? 0)
        if (purchase.pointsStatusId ?? 0 > 0) {
            pendingPointsLabel.isHidden = false
            if (purchase.pointsStatusId == 1) {
                pendingPointsLabel.textColor = UIColor.c00BCF0
                pendingPointsLabel.text = ("Points Earned: \(purchase.points ?? 0) Points (Pending Feedback left)")
            } else if (purchase.pointsStatusId == 2) {
                pendingPointsLabel.textColor = UIColor.c00A02A
                pendingPointsLabel.text = ("Points Earned: \(purchase.points ?? 0) Points")
            } else if (purchase.pointsStatusId == 3) {
                pendingPointsLabel.textColor = UIColor.black
                pendingPointsLabel.text = ("Points Retracted: \(purchase.points ?? 0) Points")
            } else if (purchase.pointsStatusId == 4) {
                pendingPointsLabel.textColor = UIColor.black
                pendingPointsLabel.text = ("Points Pending: \(purchase.points ?? 0) Points (Pending Dispute)")
            } else {
                pendingPointsLabel.isHidden = true
            }
        } else {
            pendingPointsLabel.isHidden = true
        }
        self.listingTitleLabel.text = purchase.listingTitle
        self.dateFormattedLabel.text = AppConstants.PURCHASED + purchase.purchaseDateFormatted!
        let image = UIImage(named: AppConstants.Images.DEFAULT_USER)
        let defaultListingImage =  UIImage(named: AppConstants.Images.ITEM_PLACEHOLDER)!
        if let imageUrl = purchase.buyerAndSellerInfo?.sellerImageUrl{
            self.buyerImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        } else {
            self.buyerImageView.image =  image
        }
        if let imageUrl = purchase.listingImageUrl{
            self.listingImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: defaultListingImage)
        } else {
            self.listingImageView.image =  image
        }
        
        if (purchase.buyerAndSellerInfo?.loggedInUserFollowingSeller)!{
            self.heartButton.setImage(UIImage(named: AppConstants.Images.LISTING_DETAIL_HEART_SELECTED), for: .normal)
        }else{
            self.heartButton.setImage(UIImage(named: AppConstants.Images.LISTING_DETAIL_HEART), for: .normal)
        }
        
        self.soldPurchasedTextLabel.text = AppConstants.Strings.SOLD_BY
        self.buyerUsernameLabel.text = purchase.buyerAndSellerInfo?.sellerUsername!
        self.userId = (purchase.buyerAndSellerInfo?.sellerId)!
        self.userName = (purchase.buyerAndSellerInfo?.sellerUsername)!
        
        self.detailTypeLabel.text = AppConstants.Strings.PURCHASE_DETAILS
        self.originalPriceLabel.text = AppConstants.DOLLAR + String(format : AppConstants.FORMAT_ZERO_DECIMAL ,purchase.purchaseOriginalBeforeDiscountPrice ?? 0)
        self.shippingPriceLabel.text = AppConstants.DOLLAR + String(format: AppConstants.FORMAT_ZERO_DECIMAL,purchase.purchaseShippingPrice ?? 0)
        self.totalPricelabel.text = AppConstants.DOLLAR + String(format: AppConstants.FORMAT_TWO_DECIMAL ,purchase.purchaseTotalPrice!)
        
        self.paidPriceLabel.text = AppConstants.DOLLAR + String(format: AppConstants.FORMAT_ZERO_DECIMAL ,purchase.purchaseOriginalAfterDiscountPrice!)
        self.paidShippingPriceLabel.text = AppConstants.DOLLAR + String(format: AppConstants.FORMAT_ZERO_DECIMAL, purchase.purchaseShippingPrice ?? 0)
        self.discountLabel.text = AppConstants.Strings.DISCOUNT_OR_COUPON
        self.discountPriceLabel.text = AppConstants.DOLLAR + String(format: AppConstants.FORMAT_ZERO_DECIMAL ,purchase.discount ?? 0)
        self.totalPaidPriceLabel.text = AppConstants.DOLLAR + String(format: AppConstants.FORMAT_TWO_DECIMAL ,purchase.purchaseTotalPrice ?? 0)
        self.paidWithView.isHidden = false
        let defaultCardImage = UIImage(named: AppConstants.Images.DEFAULT_CARD_ICON)
        let brandUrl = purchase.paidWith?.brandImageUrl
        let newBrandUrl = brandUrl?.replacingOccurrences(of: ".svg", with: "")
        
        if let imageUrl = newBrandUrl{
            self.cardImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: defaultCardImage)
        }
        
        self.brandLabel.text = purchase.paidWith!.brand
        self.lastFourLabel.text = purchase.paidWith!.lastFour
        
        if (purchase.canOpenDispute! || purchase.hasOpenDispute! || purchase.hasViewableDispute!){
            self.reportAnIssueButton.backgroundColor = UIColor.black
            self.reportAnIssueButton.isEnabled = true
        }else{
            self.reportAnIssueButton.backgroundColor = UIColor.cCBCBCB
            self.reportAnIssueButton.isEnabled = false
        }
        if purchase.canLeaveFeedback!{
            self.leaveFeedBackButton.backgroundColor = UIColor.cBB189C
            self.leaveFeedBackButton.isEnabled = true
        }else{
            self.leaveFeedBackButton.backgroundColor = UIColor.cCBCBCB
            self.leaveFeedBackButton.isEnabled = false
        }
        
        if (purchase.hasOpenDispute!) {
            self.disputeDetailButton.isHidden = false
            self.reportAnIssueButton.isHidden = true
        } else {
            if(purchase.hasViewableDispute!) {
                self.disputeDetailButton.isHidden = false
                self.reportAnIssueButton.isHidden = true
            } else {
                self.reportAnIssueButton.isHidden = false
                self.disputeDetailButton.isHidden = true
            }
        }
        
        
        if (purchase.canCancelPurchase!) {
            self.cancelOrderButton.isHidden = false
            self.leaveFeedBackButton.isHidden = true
            self.ratingView.isHidden = true
        } else {
            self.cancelOrderButton.isHidden = true
            self.leaveFeedBackButton.isHidden = false
            self.ratingView.isHidden = true
            if (purchase.rating ?? 0 > 0) {
                self.cancelOrderButton.isHidden = true
                self.leaveFeedBackButton.isHidden = true
                self.cancelOrderButton.isOpaque = true
                self.leaveFeedBackButton.isOpaque = true
                self.ratingView.isHidden = false
                
                let rating = purchase.rating!
                if rating == 1{
                    self.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starTwoImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    self.starThreeImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    self.starFourImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    self.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                }else if rating == 2{
                    self.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starThreeImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    self.starFourImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    self.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                }else if rating == 3{
                    self.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starThreeImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starFourImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                    self.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                }else if rating == 4{
                    self.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starThreeImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starFourImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starFiveImageView.image = UIImage(named: AppConstants.Images.EMPTY_RATING_STAR_ICON)
                }else{
                    self.starOneImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starTwoImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starThreeImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starFourImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                    self.starFiveImageView.image = UIImage(named: AppConstants.Images.STAR_ICON)
                }
                
            } else {
                self.cancelOrderButton.isHidden = true
                self.leaveFeedBackButton.isHidden = false
                self.cancelOrderButton.isOpaque = true
                self.ratingView.isOpaque = true
                self.ratingView.isHidden = true
            }
            
        }
        
        
        let status  = purchase.shippedTo?.shippingStatus?.uppercased()
        var statusDetail = ""
        
        if !(status ?? "").isEmpty{
            self.statusView.isHidden = false
            self.statusLabel.text = status
            
            if (status!.caseInsensitiveCompare(AppConstants.DELIVERED) == ComparisonResult.orderedSame){
                isEnabled = true
                statusDetail = setpurchaseDate(purchase: purchase)
            }else{
                statusDetail = ifShippingStatusDetailsEmptyPurchase(purchase: purchase)
                isEnabled = false
            }
        
            if !(statusDetail.isEmpty){
                self.deliveredDateLabel.isHidden = false
                self.deliveredDateLabel.text = statusDetail
            }else{
                self.deliveredDateLabel.isHidden = true
            }
            
        }else{
            self.statusView.isHidden = true
            isEnabled = false
        }
        
        self.nameLabel.text = purchase.shippedTo?.fullName
        
        let addressOne = purchase.shippedTo?.addressOne!
        let addressTwo = purchase.shippedTo?.addressTwo!
        
        if addressTwo!.isEmpty{
            self.addressLabel.text = addressOne
        }else{
            self.addressLabel.text = addressOne! + " " + addressTwo!
        }
        let defaultAddress = purchase.shippedTo?.isDefaultAddress!
        
        if defaultAddress!{
            self.defaultAddressLabel.isHidden = false
        }else{
            self.defaultAddressLabel.isHidden = true
        }
        
        let city = purchase.shippedTo?.city
        let state = purchase.shippedTo?.state
        let zipcode = purchase.shippedTo?.zipCode
        self.stateLabel.text = city! + " " + state! + " " + zipcode!
        
        if purchase.canPrintShippingLabel ?? false{
            self.viewShippingHistoryButton.setTitle(AppConstants.Strings.PRINT_SHIPPING_LABEL, for: .normal)
            self.statusView.isHidden = false
            self.deliveredDateLabel.isHidden = true
            self.statusLabel.text = AppConstants.Strings.NOT_YET_SHIPPED
        }else{
            self.viewShippingHistoryButton.setTitle(AppConstants.Strings.VIEW_SHIPPING_HISTORY, for: .normal)
        }
        
        if purchase.canPrintReturnLabel ?? false{
            self.viewShippingHistoryButton.setTitle(AppConstants.Strings.PRINT_SHIPPING_LABEL, for: .normal)
            self.statusView.isHidden = false
            self.deliveredDateLabel.isHidden = true
            self.statusLabel.text = AppConstants.Strings.NOT_YET_SHIPPED
        }else{
            self.viewShippingHistoryButton.setTitle(AppConstants.Strings.VIEW_SHIPPING_HISTORY, for: .normal)
        }
        
        if purchase.purchasedWithPromotions?.count ?? 0 > 0{
            let promotions = purchase.purchasedWithPromotions!
            for i in 0..<promotions.count{
                if promotions[i].promotionTypeId == AppConstants.FivePercentOff || promotions[i].promotionTypeId == AppConstants.TenPercentOff{
                    if let imageUrl = promotions[i].displayImageShort{
                        discountImageView.kf.setImage(with: URL(string: AppConstants.IMAGES_URL + imageUrl + AppConstants.IMAGES_EXTENSION))
                    }
                    discountImageView.isHidden = false
                }else if promotions[i].promotionTypeId == AppConstants.FreeShipping || promotions[i].promotionTypeId == AppConstants.ThreeDollarShipping{
                    if let imageUrl = promotions[i].displayImageShort{
                        freeShippingImageView.kf.setImage(with: URL(string: AppConstants.IMAGES_URL + imageUrl + AppConstants.IMAGES_EXTENSION))
                    }
                    freeShippingImageView.isHidden = false
                }else{
                    if let imageUrl = promotions[i].displayImageShort{
                        promotionImageView.kf.setImage(with: URL(string: AppConstants.IMAGES_URL + imageUrl + AppConstants.IMAGES_EXTENSION))
                    }
                    promotionImageView.isHidden = false
                }
            }
            
        }else{
            discountImageView.isHidden = true
            freeShippingImageView.isHidden = true
            promotionImageView.isHidden = true
        }
        
        if purchase.returnedTo != nil{
            returnView.isHidden = false
            self.returnDetailsTopView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
            self.returnNameLabel.text = purchase.returnedTo?.fullName
            self.defaultReturnAdressLabel.isHidden = true
            self.returnAddressLabel.isHidden = true
            self.returnStateLabel.isHidden = true
            
            let returnStatus  = purchase.returnedTo?.returnStatus?.uppercased() ?? ""
            var statusDetail = ""
            
            if !(returnStatus).isEmpty{
                self.returnStatusView.isHidden = false
                self.returnStatusLabel.text = returnStatus
                
                if (returnStatus.caseInsensitiveCompare(AppConstants.DELIVERED) == ComparisonResult.orderedSame){
                    isEnabled = true
                    statusDetail = setpurchaseReturnDate(purchase:purchase)
                }else{
                    statusDetail = ifReturnStatusDetailsEmptyPurchase(purchase:purchase)
                    isEnabled = false
                }
                
                if !(statusDetail.isEmpty){
                    self.returnAddressFullLabel.isHidden = false
                    self.returnAddressFullLabel.text = statusDetail
                }else{
                    self.returnAddressFullLabel.isHidden = true
                }
                
            }else{
                self.returnStatusView.isHidden = true
                isEnabled = false
            }
            
            if purchase.canPrintReturnLabel ?? false{
               self.printReturnButton.setTitle(AppConstants.Strings.PRINT_RETURN_LABEL, for: .normal)
            }else{
               self.printReturnButton.setTitle(AppConstants.Strings.VIEW_RETURN_HISTORY, for: .normal)
            }
        }else{
            returnView.isHidden = true
        }
        
        self.salesTaxView.isHidden = false
        self.salesTaxBottomLabel.text =  AppConstants.DOLLAR + String(format: AppConstants.FORMAT_TWO_DECIMAL, purchase.salesTax ?? 0)
        
    }
    
    func setpurchaseDate(purchase:PurchaseDetail) -> String{
        var statusDetail = ""
        if purchase.shippedTo?.deliveryDateFormatted == nil || (purchase.shippedTo?.deliveryDateFormatted) == ""{
            if purchase.updatedDateFormatted == nil || purchase.updatedDateFormatted == ""{
                statusDetail = AppConstants.on + (purchase.purchaseDateFormatted ?? "")
            }else{
                statusDetail = AppConstants.on + (purchase.updatedDateFormatted ?? "")
            }
        }else{
            statusDetail = AppConstants.on + (purchase.shippedTo?.deliveryDateFormatted ?? "")
        }
        return statusDetail
    }
    
    func ifShippingStatusDetailsEmptyPurchase(purchase:PurchaseDetail) -> String{
        var statusDetail = ""
        if purchase.shippedTo?.shippingStatusDetails == nil || (purchase.shippedTo?.shippingStatusDetails) == ""{
            if purchase.updatedDateFormatted == nil || purchase.updatedDateFormatted == ""{
                statusDetail = AppConstants.on + (purchase.purchaseDateFormatted ?? "")
            }else{
                statusDetail = AppConstants.on + (purchase.updatedDateFormatted ?? "")
            }
        }else{
            statusDetail = AppConstants.on + (purchase.shippedTo?.shippingStatusDetails ?? "")
        }
        return statusDetail
    }
    
    func ifReturnStatusDetailsEmptyPurchase(purchase:PurchaseDetail) -> String{
        var statusDetail = ""
        if purchase.returnedTo?.returnStatusDetails == nil || (purchase.returnedTo?.returnStatusDetails) == ""{
            if purchase.updatedDateFormatted == nil || purchase.updatedDateFormatted == ""{
                statusDetail = AppConstants.on + (purchase.purchaseDateFormatted ?? "")
            }else{
                statusDetail = AppConstants.on + (purchase.updatedDateFormatted ?? "")
            }
        }else{
            statusDetail = AppConstants.on + (purchase.returnedTo?.returnStatusDetails ?? "")
        }
        return statusDetail
    }
    
    func setpurchaseReturnDate(purchase:PurchaseDetail) -> String{
        var statusDetail = ""
        if purchase.returnedTo?.deliveryDateFormatted == nil || (purchase.returnedTo?.deliveryDateFormatted) == ""{
            if purchase.updatedDateFormatted == nil || purchase.updatedDateFormatted == ""{
                statusDetail = AppConstants.on + (purchase.purchaseDateFormatted ?? "")
            }else{
                statusDetail = AppConstants.on + (purchase.updatedDateFormatted ?? "")
            }
        }else{
            statusDetail = AppConstants.on + (purchase.returnedTo?.deliveryDateFormatted ?? "")
        }
        return statusDetail
    }
    
    func setSaleDate(sale:SalesDetail) -> String{
        var statusDetail = ""
        if sale.shippedTo?.deliveryDateFormatted == nil || (sale.shippedTo?.deliveryDateFormatted) == ""{
            if sale.updatedDateFormatted == nil || sale.updatedDateFormatted == ""{
                statusDetail = AppConstants.on + (sale.saleDateFormatted ?? "")
            }else{
                statusDetail = AppConstants.on + (sale.updatedDateFormatted ?? "")
            }
        }else{
            statusDetail = AppConstants.on + (sale.shippedTo?.deliveryDateFormatted ?? "")
        }
        return statusDetail
    }
    
    func ifShippingStatusDetailsEmptySale(sale:SalesDetail) -> String{
        var statusDetail = ""
        if sale.shippedTo?.shippingStatusDetails == nil || (sale.shippedTo?.shippingStatusDetails) == ""{
            if sale.updatedDateFormatted == nil || sale.updatedDateFormatted == ""{
                statusDetail = AppConstants.on + (sale.saleDateFormatted ?? "")
            }else{
                statusDetail = AppConstants.on + (sale.updatedDateFormatted ?? "")
            }
        }else{
            statusDetail = AppConstants.on + (sale.shippedTo?.shippingStatusDetails ?? "")
        }
        return statusDetail
    }
    
    func setSaleReturnDate(sale:SalesDetail) -> String{
        var statusDetail = ""
        if sale.returnedTo?.deliveryDateFormatted == nil || (sale.returnedTo?.deliveryDateFormatted) == ""{
            if sale.updatedDateFormatted == nil || sale.updatedDateFormatted == ""{
                statusDetail = AppConstants.on + (sale.saleDateFormatted ?? "")
            }else{
                statusDetail = AppConstants.on + (sale.updatedDateFormatted ?? "")
            }
        }else{
            statusDetail = AppConstants.on + (sale.returnedTo?.deliveryDateFormatted ?? "")
        }
        return statusDetail
    }
    
    func ifReturnStatusDetailsEmptySale(sale:SalesDetail) -> String{
        var statusDetail = ""
        if sale.returnedTo?.returnStatusDetails == nil || (sale.returnedTo?.returnStatusDetails) == ""{
            if sale.updatedDateFormatted == nil || sale.updatedDateFormatted == ""{
                statusDetail = AppConstants.on + (sale.saleDateFormatted ?? "")
            }else{
                statusDetail = AppConstants.on + (sale.updatedDateFormatted ?? "")
            }
        }else{
            statusDetail = AppConstants.on + (sale.returnedTo?.returnStatusDetails ?? "")
        }
        return statusDetail
    }
    
    func getSaleDetail(id:Int) {
        profileViewModel.getSalesDetail(accessToken: AppDefaults.getAccessToken(), id: id, { (response) in
            self.screenActivityIndicator.stopAnimating()
            self.view.hideToastActivity()
            let data: ApiResponse<SalesDetail> = Utils.convertResponseToData(data: response.data!)
            if response.response?.statusCode == 200 {
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let sale = data.data!
                    self.sale = sale
                    self.sale?.indexOfItem = self.indexOfItem!
                    print("saleList",self.sale!)
                    self.setSalesDetail(sale:sale)
                    self.scrollView.isHidden = false
                }
            }else if response.response?.statusCode == 400 {
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getPurchaseDetail(id:Int) {
        profileViewModel.getPurchasesDetail(accessToken: AppDefaults.getAccessToken(), id: id, { (response) in
            self.screenActivityIndicator.stopAnimating()
            self.view.hideToastActivity()
            let data: ApiResponse<PurchaseDetail> = Utils.convertResponseToData(data: response.data!)
            if response.response?.statusCode == 200 {
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let purchase = data.data!
                    self.purchase = purchase
                    self.purchase?.indexOfItem = self.indexOfItem!
                    self.setPurchaseDetail(purchase:purchase)
                    self.scrollView.isHidden = false
                }
            }else if response.response?.statusCode == 400 {
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func likeStatus(id:Int) {
        profileViewModel.updateSalePurchaseLikeStatus(accessToken: AppDefaults.getAccessToken(), id: id, { (response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    @objc func userClicked(sender: UITapGestureRecognizer) {
        if userId == AppDefaults.getUserId(){
            self.tabBarController?.selectedIndex = 4
        }else{
            self.openProfile(id: userId, username: userName)
        }
        
    }
    
    func purchaseFeedback(id:Int,rating:Int,comment:String) {
        let purchaseData = PurchaseDetail(purchaseId: id, rating: rating, feedback: comment)
        profileViewModel.buyerFeedback(accessToken: AppDefaults.getAccessToken(), id: id, data: purchaseData, { (response) in
            if response.response?.statusCode == 201 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    self.navigationController?.dismiss(animated: false, completion: nil)
                    self.view.makeToastActivity(.center)
                    self.getPurchaseDetail(id: self.id!)
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func sellerFeedback(id:Int,rating:Int,comment:String) {
        let sellerData = SalesDetail(saleId: id, rating: rating, feedback: comment)
        profileViewModel.sellerFeedback(accessToken: AppDefaults.getAccessToken(), id: id, data: sellerData, { (response) in
            if response.response?.statusCode == 201 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    self.navigationController?.dismiss(animated: false, completion: nil)
                    self.view.makeToastActivity(.center)
                    self.getSaleDetail(id:self.id!)
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    @IBAction func leaveFeedbackAction(_ sender: UIButton) {
        feedbackDialog()
    }
    
    func feedbackDialog(){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.FEEDBACK_DIALOG_BOARD, bundle:nil)
        let feedbackDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.FEEDBACK_DIALOG) as! FeedbackDialogViewController
        feedbackDialogViewController.modalPresentationStyle = .overFullScreen
        feedbackDialogViewController.sale = self.sale
        feedbackDialogViewController.purchase = self.purchase
        feedbackDialogViewController.TYPE = self.TYPE
        feedbackDialogViewController.onFeedbackSubmitClickDelegate = self
        self.navigationController?.present(feedbackDialogViewController, animated:false,completion: nil)
    }
    
    @IBAction func cancelOrderAction(_ sender: UIButton) {
        if self.TYPE == AppConstants.TYPE_PURCHASE{
//            let id = (self.purchase?.purchaseId!)!
//            self.checkReportAnIssue(id:id)
            self.cancelPurchaseDialog()
        }else{
            self.cancelSaleScreen(saleDetail:self.sale!)
        }
    }
    
    func cancelPurchaseDialog(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.WITHDRAW_DIALOG_BOARD, bundle: nil)
        let reportAnIssueDialogViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.WITHDRAW_DIALOG) as! WithdrawDialogViewController
        reportAnIssueDialogViewController.modalPresentationStyle = .overFullScreen
        reportAnIssueDialogViewController.TYPE = AppConstants.TYPE_REPORT_AN_ISSUE
        reportAnIssueDialogViewController.purchaseDetail = self.purchase!
        reportAnIssueDialogViewController.itemTitle = self.purchase?.listingTitle!
        reportAnIssueDialogViewController.cancelPurchaseDelegate = self
        self.navigationController?.present(reportAnIssueDialogViewController, animated: false, completion: nil)
    }
    
    func cancelSaleScreen(saleDetail:SalesDetail){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.CANCEL_SALE_STOEYBOARD, bundle: nil)
        let cancelSaleViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.CANCEL_SALE_SCREEN) as! CancelSaleViewController
        cancelSaleViewController.saleDetail = saleDetail
        cancelSaleViewController.cancelOrderSaleDelegate = self
        self.navigationController?.pushViewController(cancelSaleViewController, animated: false)
    }
    
    @IBAction func reportAnIssueButtonAction(_ sender: UIButton) {
        var id = 0
        if self.sale == nil{
            id = (self.purchase?.purchaseId!)!
        }else{
            id = (self.sale?.saleId!)!
        }
        self.checkReportAnIssue(id:id)
//        if self.TYPE == AppConstants.TYPE_PURCHASE{
//            self.reportAnIssuePurchaseScreen(purchaseDetail:self.purchase!)
//        }else{
//            self.reportAnIssueSaleScreen(saleDetail:self.sale!)
//        }
    }
    
    @IBAction func disputeDetailButtonAction(_ sender: UIButton) {
        var id = 0
        if self.sale == nil{
            id = (self.purchase?.disputeId!)!
        }else{
            id = (self.sale?.disputeId!)!
        }
        self.disputeDetailScreen(id: id)
    }
    
    func disputeDetailScreen(id:Int){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.DISPUTE_STORYBOARD, bundle: nil)
        let disputeViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.DISPUTE_DETAILS_SCREEN) as! DisputeDetailsViewController
        disputeViewController.disputeId = id
        disputeViewController.TYPE = self.TYPE
        disputeViewController.cancelDisputeRefreshDelegate = self
        disputeViewController.refreshSalePurchaseDelegate = self
        self.navigationController?.pushViewController(disputeViewController, animated: false)
    }
    
    func successMessageDialog(){
        let storyBoard = UIStoryboard.init(name: AppConstants.Storyboard.SUCCESS_MESSAGE_DIALOG_BOARD, bundle: nil)
        let successViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.SUCCESS_MESSAGE_DIALOG) as! SuccessMessageViewController
        successViewController.modalPresentationStyle = .overFullScreen
        successViewController.TYPE = AppConstants.TYPE_CANCEL_DISPUTE
        self.navigationController?.present(successViewController, animated: false, completion: nil)
    }
    
    func checkReportAnIssue(id:Int) {
          self.view.makeToastActivity(.center)
          profileViewModel.checkReportAnIssue(accessToken: AppDefaults.getAccessToken(), id: id, { (response) in
              self.view.hideToastActivity()
              if response.response?.statusCode == 200 {
                  let data: ApiResponse<ListingCheck> = Utils.convertResponseToData(data: response.data!)
                  
                  let status = data.data!
                  if status.isError ?? false{
                      self.showMessageDialog(message: status.errorMessage ?? "")
                  }else{
                    if self.TYPE == AppConstants.TYPE_PURCHASE{
                        self.reportAnIssuePurchaseScreen(purchaseDetail:self.purchase!)
                    }else{
                        self.reportAnIssueSaleScreen(saleDetail:self.sale!)
                    }
                  }
                  
              }else{
                  let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                  if data.isError {
                      let error = Utils.getErrorMessage(errors: data.errors)
                      self.showMessageDialog(message: error)
                  }
              }
          } , { (error) in
              self.view.hideToastActivity()
              if error.statusCode == AppConstants.NO_INTERNET {
                  self.showInternetError()
              }
          })
      }
    
    func reportAnIssuePurchaseScreen(purchaseDetail:PurchaseDetail){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.REPORT_AN_ISSUE_STORYBOARD, bundle: nil)
        let reportAnIssueViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.REPORT_AN_ISSUE_SCREEN) as! ReportAnIssueViewController
        //reportAnIssueViewController.TYPE = AppConstants.TYPE_ITEM_DELIVERED
        reportAnIssueViewController.purchaseDetail = purchaseDetail
        reportAnIssueViewController.reportAnIssueDelegate = self
        self.navigationController?.pushViewController(reportAnIssueViewController, animated: false)
    }
    
    func reportAnIssueSaleScreen(saleDetail:SalesDetail){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.REPORT_AN_ISSUE_STORYBOARD, bundle: nil)
        let reportAnIssueViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.REPORT_AN_ISSUE_SCREEN) as! ReportAnIssueViewController
        //reportAnIssueViewController.TYPE = AppConstants.TYPE_ITEM_DELIVERED
        reportAnIssueViewController.saleDetail = saleDetail
        reportAnIssueViewController.reportAnIssueDelegate = self
        self.navigationController?.pushViewController(reportAnIssueViewController, animated: false)
    }
    
    func emailShippingLabel(id:Int) {
        self.view.makeToastActivity(.center)
        self.dismiss(animated: false, completion: nil)
        profileViewModel.emailShippingLabel(accessToken: AppDefaults.getAccessToken(), id: id, { (response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 201 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    let storyBoard = UIStoryboard(name: AppConstants.Storyboard.PRINT_SHIPPING_LABEL_DIALOG_BOARD, bundle:nil)
                    let printShippingLabelDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.PRINT_SHIPPING_LABEL_DIALOG_SCREEN) as! PrintShippingLabelDialogViewController
                    printShippingLabelDialogViewController.modalPresentationStyle = .overFullScreen
                    printShippingLabelDialogViewController.TYPE = AppConstants.TYPE_SHIPPING_LABEL_DONE
                    printShippingLabelDialogViewController.printShippingLabelDelegate = self
                    printShippingLabelDialogViewController.listingTitle = self.sale?.listingTitle!
                    self.navigationController?.present(printShippingLabelDialogViewController, animated:false,completion: nil)
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func emailReturnLabel(id:Int) {
          self.view.makeToastActivity(.center)
          self.dismiss(animated: false, completion: nil)
          profileViewModel.returnLabel(accessToken: AppDefaults.getAccessToken(), id: id, { (response) in
              if response.response?.statusCode == 201 || response.response?.statusCode == 200{
                  let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                  if data.isError {
                      let error = Utils.getErrorMessage(errors: data.errors)
                      self.showMessageDialog(message: error)
                  }else{
                    self.view.hideToastActivity()
                      let storyBoard = UIStoryboard(name: AppConstants.Storyboard.PRINT_SHIPPING_LABEL_DIALOG_BOARD, bundle:nil)
                      let printShippingLabelDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.PRINT_SHIPPING_LABEL_DIALOG_SCREEN) as! PrintShippingLabelDialogViewController
                      printShippingLabelDialogViewController.modalPresentationStyle = .overFullScreen
                      printShippingLabelDialogViewController.TYPE = AppConstants.TYPE_RETURN_LABEL_DONE
                      printShippingLabelDialogViewController.printReturnLabelDelegate = self
                      printShippingLabelDialogViewController.listingTitle = self.purchase?.listingTitle!
                      self.navigationController?.present(printShippingLabelDialogViewController, animated:false,completion: nil)
                  }
              }else{
                  let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                  if data.isError {
                      let error = Utils.getErrorMessage(errors: data.errors)
                      self.showMessageDialog(message: error)
                  }
                self.view.hideToastActivity()
              }
          } , { (error) in
              self.view.hideToastActivity()
              if error.statusCode == AppConstants.NO_INTERNET {
                  self.showInternetError()
              }
          })
      }
    
    func openCancelOrderSaleDialog(title:String){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.CANCEL_ORDER_SALE_DIALOG_STORYBOARD, bundle: nil)
        let dialogViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.CANCEL_ORDER_SALE_DIALOG) as! CancelOrderSaleDialogViewController
        dialogViewController.modalPresentationStyle = .overFullScreen
        dialogViewController.listingTitle = title
        self.navigationController?.present(dialogViewController, animated: false, completion: nil)
    }
    
    func cancelpurchase(id:Int) {
        self.view.makeToastActivity(.center)
        profileViewModel.cancelPurchase(accessToken: getAccessToken(),id:id ,{(response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Bool> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    if self.TYPE == AppConstants.TYPE_PURCHASE{
                        self.getPurchaseDetail(id:self.id!)
                    }else{
                        self.getSaleDetail(id:self.id!)
                    }
                    self.view.hideToastActivity()
                }
            } else {
                self.view.hideToastActivity()
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    @IBAction func printReturnButtonAction(_ sender: UIButton) {
        var title = ""
        var id = 0
        
        if TYPE == AppConstants.TYPE_PURCHASE{
            title = (purchase?.listingTitle!)!
            id = (purchase?.purchaseId!)!
            
            if self.purchase?.canPrintReturnLabel ?? false{
                self.emailReturnLabelDialog(id:id,title:title)
            }else{
                self.trackingInfoDialogForReturn(id:id, title:title)
            }
        }else{
            title = (sale?.listingTitle!)!
            id = (sale?.saleId!)!
            self.trackingInfoDialogForReturn(id:id,title:title)
        }
    }
    
    func trackingInfoDialogForReturn(id:Int,title:String){
        let canReturnInfoForReturnLabel = true
        
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.DELIVERY_STATUS_DIALOG_BOARD, bundle:nil)
        let deliveryStatusDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.DELIVERY_STATUS_DIALOG_SCREEN) as! DeliveryStatusDialogViewController
        deliveryStatusDialogViewController.modalPresentationStyle = .overFullScreen
        deliveryStatusDialogViewController.id = id
        deliveryStatusDialogViewController.TYPE = self.TYPE
        deliveryStatusDialogViewController.listingTitle = title
        deliveryStatusDialogViewController.canReturnInfoForReturnLabel = canReturnInfoForReturnLabel
        self.navigationController?.present(deliveryStatusDialogViewController, animated:false ,completion: nil)
    }
    
}
