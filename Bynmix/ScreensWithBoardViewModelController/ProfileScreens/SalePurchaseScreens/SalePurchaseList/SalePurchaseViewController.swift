import UIKit
import Alamofire
import XLPagerTabStrip

class SalePurchaseViewController: BaseViewController ,LoadMoreProtocol,onRelistItemClickProtocol,OnFeedbackSubmitClickProtocol,PrintShippingLabelProtocol,ReportAnIssueProtocol,CancelOrderSaleProtocol,CancelDisputeRefreshProtocol,CancelPurchaseProtocol,CancelOrderPurchasePopUpProtocol,CancelOrderSalePopUpProtocol,DisputeDetailPurchaseMenuPopUpProtocol,DisputeDetailSaleMenuPopUpProtocol,ReportAnIssueSalePopUpProtocol,ReportAnIssuePurchasePopUpProtocol,PageRedirectToSaleProtocol,PageRedirectToPurchaseListProtocol,PageRedirectToSaleListProtocol,PrintReturnLabelProtocol,RefreshSalePurchaseProtocol,ConfirmDeliveryProtocol,ReturnDeliveredProtocol, SelectShipRateProtocol{
    
    func selectShipRate(shipRate: ShipRates,saleId:Int) {
        self.navigationController?.popViewController(animated: false)
        let rateId = shipRate.shippingRateObjectId ?? ""
        let shiprate = ShipRates(shippingRateObjectId: rateId)
        self.emailShippingLabelFreeShipping(id:saleId,data:shiprate)
        
    }
    
    
    func returnDelivered(){
        self.returnDeliveredDialog(title:(self.sale?.listingTitle!)!)
    }
    
    func confirmDelivery(type: Int) {
        self.dismiss(animated: false, completion: nil)
        if type == AppConstants.TYPE_CONFIRM_DELEVERY_CLICK{
            self.confirmDeliveryStatus(id:(self.sale?.saleId!)!)
        }else if type == AppConstants.TYPE_UPLOAD_IMAGES{
            self.disputeDetailScreenForAddingImages(id:(self.sale?.disputeId!)!)
        }
    }
    
    func refreshSalePurchase() {
        self.view.makeToastActivity(.center)
       if TYPE == AppConstants.TYPE_PURCHASE{
            self.currentPage = 1
            self.getPurchase()
        }else{
            self.currentPage = 1
            self.getSale()
        }
    }
    
    func printReturnLabel(type: Int, id: Int) {
        if type == AppConstants.TYPE_PRINT_RETURN_LABEL{
            self.emailReturnLabel(id:id)
        }else if type == AppConstants.TYPE_RETURN_LABEL_DONE{
            self.view.makeToastActivity(.center)
            self.currentPage = 1
            self.getPurchase()
            self.dismiss(animated: false , completion: nil)
        }
    }
    
    func pageRedirectToSaleList(sale: SalesDetail) {
        let index = sale.indexOfItem!
        
        let saleList = self.salePurchaseHandler.saleDetail
        let saleHistoryList = self.salePurchaseHistoryHandler.saleDetail
        if saleList.count > 0 || saleHistoryList.count > 0{
            
            var saleDetail = saleList[index]
            saleDetail.hasSubStatus = sale.hasSubStatus
            saleDetail.canCancelSale = sale.canCancelSale
            saleDetail.canOpenDispute = sale.canOpenDispute
            saleDetail.canLeaveFeedback = sale.canLeaveFeedback
            saleDetail.hasOpenDispute = sale.hasOpenDispute
            saleDetail.hasViewableDispute = sale.hasViewableDispute
            saleDetail.canPrintShippingLabel = sale.canPrintShippingLabel
            saleDetail.saleStatus = sale.saleStatus
            
            self.salePurchaseHandler.saleDetail[index] = saleDetail
            
            var saleHistoryDetail = saleHistoryList[index]
            saleHistoryDetail.hasSubStatus = sale.hasSubStatus
            saleHistoryDetail.canCancelSale = sale.canCancelSale
            saleHistoryDetail.canOpenDispute = sale.canOpenDispute
            saleHistoryDetail.canLeaveFeedback = sale.canLeaveFeedback
            saleHistoryDetail.hasOpenDispute = sale.hasOpenDispute
            saleHistoryDetail.hasViewableDispute = sale.hasViewableDispute
            saleHistoryDetail.canPrintShippingLabel = sale.canPrintShippingLabel
            saleHistoryDetail.saleStatus = sale.saleStatus
            
            self.salePurchaseHistoryHandler.saleDetail[index] = saleHistoryDetail
            
            tableView.reloadData()
            collectionView.reloadData()
            
        }
    }
    
    func pageRedirectToPurchaseList(purchase: PurchaseDetail) {
        let index = purchase.indexOfItem!
        let purchaseList = self.salePurchaseHandler.purchaseDetail
        let purchaseListInListView = self.salePurchaseHistoryHandler.purchaseDetail
        
        if purchaseList.count > 0 || purchaseListInListView.count > 0{
            
            var purchaseDetail = purchaseList[index]
            purchaseDetail.hasSubStatus = purchase.hasSubStatus
            purchaseDetail.canCancelPurchase = purchase.canCancelPurchase
            purchaseDetail.canOpenDispute = purchase.canOpenDispute
            purchaseDetail.canLeaveFeedback = purchase.canLeaveFeedback
            purchaseDetail.hasOpenDispute = purchase.hasOpenDispute
            purchaseDetail.hasViewableDispute = purchase.hasViewableDispute
            purchaseDetail.canPrintShippingLabel = purchase.canPrintShippingLabel
            purchaseDetail.purchaseStatus = purchase.purchaseStatus
            
            self.salePurchaseHandler.purchaseDetail[index] = purchaseDetail
            
            var list = purchaseListInListView[index]
            list.hasSubStatus = purchase.hasSubStatus
            list.canCancelPurchase = purchase.canCancelPurchase
            list.canOpenDispute = purchase.canOpenDispute
            list.canLeaveFeedback = purchase.canLeaveFeedback
            list.hasOpenDispute = purchase.hasOpenDispute
            list.hasViewableDispute = purchase.hasViewableDispute
            list.canPrintShippingLabel = purchase.canPrintShippingLabel
            list.purchaseStatus = purchase.purchaseStatus
            
            self.salePurchaseHistoryHandler.purchaseDetail[index] = list
            
            tableView.reloadData()
            collectionView.reloadData()
            
        }
        
    }
    
    func pageRedirectToSale() {
        self.dismiss(animated: false , completion: nil)
        self.navigationController?.popViewController(animated: false)
        self.view.makeToastActivity(.center)
        if TYPE == AppConstants.TYPE_PURCHASE{
            self.currentPage = 1
            self.getPurchase()
        }else{
            self.currentPage = 1
            self.getSale()
        }
    }
    
    func reportAnIssuePurchasePopUp(purchase: PurchaseDetail) {
        self.purchase = purchase
        if purchase.hasOpenDispute ?? false {
            //mCallback.openDisputeDetailFragment(detail.getDisputeId(), detail);
        } else {
            if purchase.hasViewableDispute ?? false {
                //mCallback.openDisputeDetailFragment(detail.getDisputeId(), detail);
            } else {
                self.purchase = purchase
                //self.reportAnIssuePurchaseScreen(purchaseDetail:self.purchase!)
                self.checkReportAnIssue(id:purchase.purchaseId!)
            }
        }
    }
    
    func reportAnIssueSalePopUp(sale:SalesDetail){
        self.sale = sale
        if sale.hasOpenDispute ?? false {
            //mCallback.openDisputeDetailFragment(detail.getDisputeId(), detail);
        } else {
            if sale.hasViewableDispute ?? false {
                //mCallback.openDisputeDetailFragment(detail.getDisputeId(), detail);
            } else {
                self.sale = sale
                //self.reportAnIssueSaleScreen(saleDetail:self.sale!)
                self.checkReportAnIssue(id:sale.saleId!)
            }
        }
    }
    
    func disputeDetailPurchaseMenuPop(purchase: PurchaseDetail) {
        self.purchase = purchase
        self.disputeDetailScreen(id:purchase.disputeId!)
    }
    
    func disputeDetailSaleMenuPopUp(sale:SalesDetail){
        self.sale = sale
        self.disputeDetailScreen(id:sale.disputeId!)
    }
    
    func cancelOrderSalePopUp(sale:SalesDetail){
        self.sale = sale
        self.cancelSaleScreen(saleDetail: self.sale!)
    }
    
    func cancelOrderPurchasePopUp(purchase: PurchaseDetail) {
        self.purchase = purchase
        self.cancelPurchaseDialog()
    }
    
    func cancelPurchase(purchase: PurchaseDetail) {
        self.cancelpurchase(id:purchase.purchaseId!)
        self.dismiss(animated: false, completion: nil)
    }
    
    func cancelDisputeRefresh(){
        self.successMessageDialog()
        if TYPE == AppConstants.TYPE_PURCHASE{
            self.currentPage = 1
            self.getPurchase()
        }else{
            self.currentPage = 1
            self.getSale()
        }
    }
    
    func cancelOrderSale(listingTitle: String) {
        self.openCancelOrderSaleDialog(title: listingTitle)
        if TYPE == AppConstants.TYPE_PURCHASE{
            self.currentPage = 1
            self.getPurchase()
        }else{
            self.currentPage = 1
            self.getSale()
        }
    }
    
    func reportAnIssue() {
        if TYPE == AppConstants.TYPE_PURCHASE{
            self.currentPage = 1
            self.getPurchase()
        }else{
            self.currentPage = 1
            self.getSale()
        }
    }
    
    func PrintShippingLabel(type: Int, id: Int) {
        if type == AppConstants.TYPE_PRINT_SHIPPING_LABEL{
            self.emailShippingLabel(id:id)
        }else if type == AppConstants.TYPE_SHIPPING_LABEL_DONE{
            self.currentPage = 1
            self.view.makeToastActivity(.center)
            self.getSale()
            self.dismiss(animated: false , completion: nil)
        }else if type == AppConstants.TYPE_PRINT_SHIPPING_LABEL_FREE_SHIPPING{
            self.dismiss(animated: false , completion: nil)
            let shipRates = (self.shippingData?.shipRatesForFreeShipping ?? [])
            self.editShippingScreen(shipRates: shipRates, buyerId: 0, addressId: 0, listingId: 0, saleId: id)
        }
    }
    
    func onFeedbackSubmitClick(type: Int, id: Int, rating: Int, comment: String) {
        if type == AppConstants.TYPE_PURCHASE{
            purchaseFeedback(id:id,rating:rating,comment:comment)
        }else{
            sellerFeedback(id:id,rating:rating,comment:comment)
        }
    }
    
    func onRelistItemClick(sale: SalesDetail) {
        relistItem(sale:sale)
    }
    
    func onLoadMore() {
        if !isApiCalled {
            isApiCalled = true
            self.typeSalePurchase()
        }
    }
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var noDataView: UIView!
    @IBOutlet var noDataShadowView: UIView!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var noDataImageView: UIImageView!
    @IBOutlet var noDataLabel: UILabel!
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var purchaseView: UIView!
    @IBOutlet var pointsEarnedView: UIView!
    @IBOutlet var profitEarnedView: UIView!
    
    @IBOutlet var purchasePointsLabel: UILabel!
    @IBOutlet var purchasePointsPendingLabel: UILabel!
    @IBOutlet var salePointsLabel: UILabel!
    @IBOutlet var salePointsPendingLabel: UILabel!
    @IBOutlet var saleProfitLabel: UILabel!
    @IBOutlet var saleProfitPendingLabel: UILabel!
    @IBOutlet var headerView: UIView!
    
    var TYPE:Int?
    var refreshControlOnGrid:UIRefreshControl?
    var refreshControlOnList:UIRefreshControl?
    var currentPage = 1
    var isApiCalled = false
    var profileViewModel = ProfileViewModel()
    var salePurchaseHandler = SalePurchaseHandler()
    var salePurchaseHistoryHandler = SalePurchaseHistoryHandler()
    var sale:SalesDetail?
    var purchase:PurchaseDetail?
    var x:Float?
    var y:Float?
    var type_Grid = true
    var type_List = false
    var shippingRate:Double?
    var shippingData:ShippingRate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        salePurchaseHistoryHandler.viewController = self
        salePurchaseHistoryHandler.loadMoreDelegate = self
        salePurchaseHandler.loadMoreDelegate = self
        collectionView.delegate = salePurchaseHandler
        collectionView.dataSource = salePurchaseHandler
        tableView.delegate = salePurchaseHistoryHandler
        tableView.dataSource = salePurchaseHistoryHandler
        typeSalePurchase()
        collectionView.contentInset = UIEdgeInsets(top: 40, left: 5, bottom: 5, right: 5)
        tableView.contentInset = UIEdgeInsets(top: 40, left: 0, bottom: 10, right: 0)
        NotificationCenter.default.addObserver(self,selector: #selector(onListChange),name:Notification.Name(AppConstants.Notifications.BAR_BUTTON),object: nil)
        if type_Grid{
            addRefreshControlOnGrid()
        }else if type_List{
            addRefreshControlOnList()
        }
        
    }
    
    @objc func onListChange(_ notification: Notification) {
        let barButtonEnable:NSNumber = notification.userInfo?[AppConstants.Notifications.BAR_BUTTON_ENABLE] as! NSNumber
        if Int(truncating: barButtonEnable) == AppConstants.TYPE_GRID {
            if self.salePurchaseHandler.purchaseDetail.count > 0 || self.salePurchaseHandler.saleDetail.count > 0{
                collectionView.isHidden = false
                noDataView.isHidden = true
                noDataShadowView.isHidden = true
                addRefreshControlOnGrid()
                self.type_Grid = true
            } else {
                collectionView.isHidden = true
                noDataView.isHidden = false
                noDataShadowView.isHidden = false
            }
            tableView.isHidden = true
            self.type_List = false
            
        } else if Int(truncating: barButtonEnable) == AppConstants.TYPE_LIST {
            if self.salePurchaseHandler.purchaseDetail.count > 0 || self.salePurchaseHandler.saleDetail.count > 0 {
                tableView.isHidden = false
                noDataView.isHidden = true
                noDataShadowView.isHidden = true
                self.type_List = true
                addRefreshControlOnList()
            } else {
                tableView.isHidden = true
                noDataView.isHidden = false
                noDataShadowView.isHidden = false
            }
            collectionView.isHidden = true
            self.type_Grid = false
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        noDataShadowView.layer.cornerRadius = 2
        noDataShadowView.layer.shadowColor = UIColor.black.cgColor
        noDataShadowView.alpha = CGFloat(0.26)
        noDataShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        noDataShadowView.layer.shadowOpacity = 1
        noDataShadowView.layer.zPosition = -1
    }
    
    func typeSalePurchase(){
        if TYPE == AppConstants.TYPE_PURCHASE{
            getPurchase()
            self.noDataLabel.text = AppConstants.Strings.NO_PURCHASE_TEXT
            purchaseView.isHidden = false
            pointsEarnedView.isHidden = true
            profitEarnedView.isHidden = true
            salePurchaseHandler.onPurchaseDetailClick = { purchase,index in
                self.onDetailPageClick(id: purchase.purchaseId! ,type: AppConstants.TYPE_PURCHASE, indexOfItem: index)
            }
            salePurchaseHistoryHandler.onPurchaseDetailClick = { purchase in
                self.onDetailPageClick(id: purchase.purchaseId! ,type: AppConstants.TYPE_PURCHASE, indexOfItem: 0)
            }
            salePurchaseHandler.onFeedbackPurchaseClick = { purchase in
                self.purchase = purchase
                self.feedbackDialog()
            }
            
            salePurchaseHandler.onButtonTwoPurchaseClick = { purchase in
                self.purchase = purchase
                var id = 0
                var title = ""
                id = (purchase.purchaseId!)
                title = (purchase.listingTitle!)
                if purchase.canPrintReturnLabel ?? false{
                    self.emailReturnLabelDialog(id:id,title:title)
                }else{
                    self.trackingInfoDialog(id:id, title:title)
                }
            }
            
            salePurchaseHandler.onReportAnIssuePurchaseClick = { purchase in
                if purchase.hasOpenDispute ?? false {
                    //mCallback.openDisputeDetailFragment(detail.getDisputeId(), detail);
                } else {
                    if purchase.hasViewableDispute ?? false {
                        //mCallback.openDisputeDetailFragment(detail.getDisputeId(), detail);
                    } else {
                        self.purchase = purchase
                        //self.reportAnIssuePurchaseScreen(purchaseDetail:self.purchase!)
                        self.checkReportAnIssue(id:purchase.purchaseId!)
                    }
                }
            }
            
            salePurchaseHandler.onCancelOrderPurchaseClick = { purchase in
                self.purchase = purchase
                //self.checkReportAnIssue(id:purchase.purchaseId!)
                self.cancelPurchaseDialog()
            }
            
            salePurchaseHandler.onDisputePurchaseClick = { purchase in
                self.purchase = purchase
                self.disputeDetailScreen(id:purchase.disputeId!)
            }
            
            salePurchaseHistoryHandler.onPurchaseMenuClick = { purchase, frame in
                self.purchase = purchase
                self.onMenuClick(type:AppConstants.TYPE_PURCHASE, frame: frame)
                
            }
        }else{
            getSale()
            self.noDataLabel.text = AppConstants.Strings.NO_SALE_TEXT
            purchaseView.isHidden = true
            pointsEarnedView.isHidden = false
            profitEarnedView.isHidden = false
            
            salePurchaseHandler.onSaleDetailClick = { sale , index in
                self.onDetailPageClick(id: sale.saleId! ,type: AppConstants.TYPE_SALE, indexOfItem: index)
            }
            salePurchaseHistoryHandler.onSaleDetailClick = { sale in
                self.onDetailPageClick(id: sale.saleId! ,type: AppConstants.TYPE_SALE, indexOfItem: 0)
            }
            salePurchaseHandler.onRelistItemSaleClick = { sale in
                self.relistItemClick(type: AppConstants.TYPE_RELIST_ITEM,sale:sale)
            }
            salePurchaseHandler.onFeedbackSaleClick = { sale in
                self.sale = sale
                self.feedbackDialog()
            }
            salePurchaseHandler.onButtonTwoSaleClick = { sale in
                self.sale = sale
                var id = 0
                let title = (sale.listingTitle!)
                id = (sale.saleId!)
                if (sale.canPrintShippingLabel!){
                    self.getShippingRate(listingId: sale.listingId!, title: title, id:id)
                }else{
                    let statusId = self.sale?.saleStatusId
                    if statusId == AppConstants.RETURN_ORDER_DELIVERED{
                        self.returnDeliveredDialog(title:title)
                    }else{
                        self.trackingInfoDialog(id:id, title:title)
                    }
                }
            }
            
            salePurchaseHandler.onReportAnIssueSaleClick = { sale in
                if sale.hasOpenDispute ?? false {
                    //mCallback.openDisputeDetailFragment(detail.getDisputeId(), detail);
                } else {
                    if sale.hasViewableDispute ?? false {
                        //mCallback.openDisputeDetailFragment(detail.getDisputeId(), detail);
                    } else {
                        self.sale = sale
                        //self.reportAnIssueSaleScreen(saleDetail:self.sale!)
                        self.checkReportAnIssue(id:sale.saleId!)
                    }
                }
            }
            
            salePurchaseHandler.onCancelOrderSaleClick = { sale in
                self.sale = sale
                self.cancelSaleScreen(saleDetail: sale)
            }
            
            salePurchaseHandler.onDisputeSaleClick = { sale in
                self.sale = sale
                self.disputeDetailScreen(id:sale.disputeId!)
            }
            
            salePurchaseHistoryHandler.onSaleMenuClick = { sale, frame in
                self.sale = sale
                self.onMenuClick(type:AppConstants.TYPE_SALE, frame:frame)
                
            }
        }
    }
    
    func returnDeliveredDialog(title:String){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.DELETE_IMAGE_DIALOG_BOARD, bundle: nil)
        let returnDeliveredViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.DELETE_IMAGE_DIALOG) as! DeleteImageDialogViewController
        returnDeliveredViewController.modalPresentationStyle = .overFullScreen
        returnDeliveredViewController.listingTitle = title
        returnDeliveredViewController.TYPE = AppConstants.TYPE_CONFIRM_DELEVERY
        returnDeliveredViewController.confirmDeliveryDelegate = self
        self.navigationController?.present(returnDeliveredViewController, animated: false, completion: nil)
    }
    
    func onMenuClick(type:Int, frame: CGRect){
        
        let rightMenuVC = UIStoryboard(name: AppConstants.Storyboard.MENU_POP_UP_BOARD, bundle: nil).instantiateViewController(withIdentifier: AppConstants.DialogController.MENU_POP_UP_DIALOG) as! MenuPopUpViewController
        
        rightMenuVC.top = frame.origin.y
        rightMenuVC.TYPE = self.TYPE
        if type == AppConstants.TYPE_PURCHASE{
            rightMenuVC.purchase = self.purchase
        }else{
            rightMenuVC.sale = self.sale
        }
        rightMenuVC.cancelOrderSalePopUpDelegate = self
        rightMenuVC.cancelOrderPurchasePopUpDelegate = self
        rightMenuVC.disputeDetailSaleMenuPopUpDelegate = self
        rightMenuVC.disputeDetailPurchaseMenuPopUpDelegate = self
        rightMenuVC.reportAnIssueSalePopUpDelegate = self
        rightMenuVC.reportAnIssuePurchasePopUpDelegate = self
        rightMenuVC.returnDeliveredDelegate = self
        
        if self.children.count > 0{
            let viewControllers:[UIViewController] = self.children
            for viewController in viewControllers{
                viewController.willMove(toParent: nil)
                viewController.view.removeFromSuperview()
                viewController.removeFromParent()
            }
        }
        else {
            self.addChild(rightMenuVC)
            rightMenuVC.view.frame = self.view.frame
            self.view.addSubview(rightMenuVC.view)
            rightMenuVC.didMove(toParent: self)
            
        }
        
    }
    
    func trackingInfoDialog(id:Int,title:String){
        var canReturnInfoForReturnLabel = false
        
        if self.TYPE == AppConstants.TYPE_PURCHASE{
            if  self.purchase?.purchaseStatusId == AppConstants.RETURN_LABEL_PRINTED || self.purchase?.purchaseStatusId == AppConstants.RETURN_ORDER_SHIPPED || self.purchase?.purchaseStatusId == AppConstants.RETURN_ORDER_IN_TRANSIT || self.purchase?.purchaseStatusId == AppConstants.RETURN_ORDER_DELIVERED{
                canReturnInfoForReturnLabel = true
            }else{
                canReturnInfoForReturnLabel = false
            }
        }else{
            if self.sale?.saleStatusId == AppConstants.RETURN_LABEL_PRINTED || self.sale?.saleStatusId == AppConstants.RETURN_ORDER_SHIPPED || self.sale?.saleStatusId == AppConstants.RETURN_ORDER_IN_TRANSIT || self.sale?.saleStatusId == AppConstants.RETURN_ORDER_DELIVERED{
                canReturnInfoForReturnLabel = true
            }else{
                canReturnInfoForReturnLabel = false
            }
            
        }
        
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.DELIVERY_STATUS_DIALOG_BOARD, bundle:nil)
        let feedbackDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.DELIVERY_STATUS_DIALOG_SCREEN) as! DeliveryStatusDialogViewController
        feedbackDialogViewController.modalPresentationStyle = .overFullScreen
        feedbackDialogViewController.id = id
        feedbackDialogViewController.TYPE = self.TYPE
        feedbackDialogViewController.listingTitle = title
        feedbackDialogViewController.canReturnInfoForReturnLabel = canReturnInfoForReturnLabel
        self.navigationController?.present(feedbackDialogViewController, animated:false,completion: nil)
    }
    
    func emailShippingLabelDialog(id:Int,title:String,shippingRate:Double,data:ShippingRate){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.PRINT_SHIPPING_LABEL_DIALOG_BOARD, bundle:nil)
        let printShippingLabelDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.PRINT_SHIPPING_LABEL_DIALOG_SCREEN) as! PrintShippingLabelDialogViewController
        printShippingLabelDialogViewController.modalPresentationStyle = .overFullScreen
        printShippingLabelDialogViewController.id = id
        printShippingLabelDialogViewController.TYPE = AppConstants.TYPE_PRINT_SHIPPING_LABEL
        printShippingLabelDialogViewController.listingTitle = title
        printShippingLabelDialogViewController.shippingRate = shippingRate
        printShippingLabelDialogViewController.shippingData = data
        printShippingLabelDialogViewController.printShippingLabelDelegate = self
        self.navigationController?.present(printShippingLabelDialogViewController, animated:false,completion: nil)
    }
    
    func feedbackDialog(){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.FEEDBACK_DIALOG_BOARD, bundle:nil)
        let feedbackDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.FEEDBACK_DIALOG) as! FeedbackDialogViewController
        feedbackDialogViewController.modalPresentationStyle = .overFullScreen
        feedbackDialogViewController.sale = self.sale
        feedbackDialogViewController.purchase = self.purchase
        feedbackDialogViewController.TYPE = self.TYPE
        feedbackDialogViewController.onFeedbackSubmitClickDelegate = self
        self.navigationController?.present(feedbackDialogViewController, animated:false,completion: nil)
    }
    
    func relistItemClick(type:Int,sale:SalesDetail){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.DELETE_IMAGE_DIALOG_BOARD, bundle:nil)
        let relistItemDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.DELETE_IMAGE_DIALOG) as! DeleteImageDialogViewController
        relistItemDialogViewController.TYPE = type
        relistItemDialogViewController.sale = sale
        relistItemDialogViewController.onRelistItemClickDelegate = self
        relistItemDialogViewController.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(relistItemDialogViewController, animated:false,completion: nil)
    }
    
    func onDetailPageClick(id:Int,type:Int,indexOfItem:Int){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.SALE_PURCHASE_DETAIL_BOARD, bundle:nil)
        let detailViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.SALE_PURCHASE_DETAIL_SCREEN) as! SalePurchaseDetailViewController
        detailViewController.id = id
        detailViewController.TYPE = type
        detailViewController.indexOfItem = indexOfItem
        detailViewController.pageRedirectToSaleListDelegate = self
        detailViewController.pageRedirectToPurchaseListDelegate = self
        detailViewController.pageRedirectToSaleDelegate = self
        self.navigationController?.pushViewController(detailViewController, animated:false)
    }
    
    func getSale() {
        self.salePurchaseHandler.TYPE = AppConstants.TYPE_SALE
        self.salePurchaseHistoryHandler.TYPE = AppConstants.TYPE_SALE
        profileViewModel.getSale(accessToken: getAccessToken(),page:currentPage, { (response) in
            self.view.hideToastActivity()
            self.handleResponse(response: response)
        } , { (error) in
            if self.type_List{
                self.refreshControlOnList?.endRefreshing()
            }else{
                self.refreshControlOnGrid?.endRefreshing()
            }
            self.screenActivityIndicator.stopAnimating()
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func handleResponse(response: AFDataResponse<Any>) {
        self.isApiCalled = false
        if self.type_List{
            self.refreshControlOnList?.endRefreshing()
        }else{
            self.refreshControlOnGrid?.endRefreshing()
        }
        if response.response?.statusCode == 200 {
            let data: ApiResponse<SalesDetail> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            } else {
                let saleData = data.data
                if (saleData?.items?.count)! > 0{
                    if let sale = saleData?.items {
                        if self.currentPage == 1 {
                            self.salePurchaseHandler.saleDetail = sale
                            self.salePurchaseHistoryHandler.saleDetail = sale
                        } else {
                            self.salePurchaseHandler.saleDetail.append(contentsOf: sale)
                            self.salePurchaseHistoryHandler.saleDetail.append(contentsOf: sale)
                        }
                        if saleData?.totalPages == currentPage {
                            self.isApiCalled = true
                            self.salePurchaseHandler.isLoadMoreRequired = false
                            self.salePurchaseHistoryHandler.isLoadMoreRequired = false
                            let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
                            layout.footerReferenceSize = CGSize(width: 0, height: 0)
                        }
                        self.collectionView.reloadData()
                        self.tableView.reloadData()
                        if self.type_Grid{
                            self.collectionView.isHidden = false
                            self.tableView.isHidden = true
                        }else{
                            self.collectionView.isHidden = true
                            self.tableView.isHidden = false
                        }
                        getSaleStats()
                    }
                    
                }else{
                    self.noDataView.isHidden = false
                    noDataShadowView.isHidden = false
                    self.collectionView.isHidden = true
                    self.tableView.isHidden = true
                    self.headerView.isHidden = true
                    self.screenActivityIndicator.stopAnimating()
                }
            }
            self.currentPage = self.currentPage + 1
        } else {
            self.screenActivityIndicator.stopAnimating()
            self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
        }
    }
    
    func getPurchase() {
        self.salePurchaseHandler.TYPE = AppConstants.TYPE_PURCHASE
        self.salePurchaseHistoryHandler.TYPE = AppConstants.TYPE_PURCHASE
        profileViewModel.getPurchase(accessToken: getAccessToken(),page:currentPage, { (response) in
            self.view.hideToastActivity()
            self.handleResponsePurchase(response: response)
        } , { (error) in
            if self.type_List{
                self.refreshControlOnList?.endRefreshing()
            }else{
                self.refreshControlOnGrid?.endRefreshing()
            }
            self.screenActivityIndicator.stopAnimating()
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func handleResponsePurchase(response: AFDataResponse<Any>) {
        self.isApiCalled = false
        if self.type_List{
            self.refreshControlOnList?.endRefreshing()
        }else{
            self.refreshControlOnGrid?.endRefreshing()
        }
        if response.response?.statusCode == 200 {
            let data: ApiResponse<PurchaseDetail> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            } else {
                let purchaseData = data.data
                if (purchaseData?.items?.count)! > 0{
                    if let purchase = purchaseData?.items {
                        if self.currentPage == 1 {
                            self.salePurchaseHandler.purchaseDetail = purchase
                            self.salePurchaseHistoryHandler.purchaseDetail = purchase
                        } else {
                            self.salePurchaseHandler.purchaseDetail.append(contentsOf: purchase)
                            self.salePurchaseHistoryHandler.purchaseDetail.append(contentsOf: purchase)
                        }
                        print("ListCount:",self.salePurchaseHandler.purchaseDetail.count)
                        if purchaseData?.totalPages == currentPage {
                            self.isApiCalled = true
                            self.salePurchaseHandler.isLoadMoreRequired = false
                            self.salePurchaseHistoryHandler.isLoadMoreRequired = false
                            let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
                            layout.footerReferenceSize = CGSize(width: 0, height: 0)
                        }
                        self.collectionView.reloadData()
                        self.tableView.reloadData()
                        getPurchaseStats()
                    }
                    
                }else{
                    self.noDataView.isHidden = false
                    noDataShadowView.isHidden = false
                    self.collectionView.isHidden = true
                    self.tableView.isHidden = true
                    self.headerView.isHidden = true
                    self.screenActivityIndicator.stopAnimating()
                }
                
            }
            self.currentPage = self.currentPage + 1
        } else {
            self.screenActivityIndicator.stopAnimating()
            self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
        }
    }
    
    func addRefreshControlOnList() {
        if refreshControlOnList == nil{
            refreshControlOnList = UIRefreshControl()
        }
        
        refreshControlOnList?.addTarget(self, action: #selector(refreshListOnList), for: .valueChanged)
        tableView.addSubview(refreshControlOnList!)
    }
    
    func addRefreshControlOnGrid() {
        if refreshControlOnGrid == nil{
            refreshControlOnGrid = UIRefreshControl()
        }
        refreshControlOnGrid?.addTarget(self, action: #selector(refreshListOnGrid), for: .valueChanged)
        collectionView.addSubview(refreshControlOnGrid!)
    }
    
    @objc func refreshListOnGrid(){
        currentPage = 1
        isApiCalled = false
        salePurchaseHandler.isLoadMoreRequired = true
        typeSalePurchase()
    }
    
    @objc func refreshListOnList(){
        self.salePurchaseHandler.isLoadMoreRequired = true
        currentPage = 1
        isApiCalled = false
        typeSalePurchase()
    }
    
    func getSaleStats() {
        profileViewModel.getSaleStats(accessToken: AppDefaults.getAccessToken(), { (response) in
            self.screenActivityIndicator.stopAnimating()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<MySalesStats> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let sale = data.data!
                    self.salePointsLabel.text = String(sale.totalSalePoints!)
                    self.salePointsPendingLabel.text = String(sale.totalPendingSalePoints!) + AppConstants.Strings.POINTS_PENDING_TEXT
                    self.saleProfitLabel.text = AppConstants.DOLLAR + " " + String(sale.totalSales!)
                    self.saleProfitPendingLabel.text = AppConstants.DOLLAR + " " + String(Int(sale.totalPendingSales!.rounded())) + " " + AppConstants.Strings.PENDING_TEXT
                    if self.type_Grid{
                        self.collectionView.isHidden = false
                        self.tableView.isHidden = true
                    }else{
                        self.collectionView.isHidden = true
                        self.tableView.isHidden = false
                    }
                }
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getPurchaseStats() {
        profileViewModel.getPurchaseStats(accessToken: AppDefaults.getAccessToken(), { (response) in
            self.screenActivityIndicator.stopAnimating()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<MyPurchasesStats> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let purchase = data.data!
                    self.purchasePointsLabel.text = String(purchase.totalPurchasePoints!)
                    self.purchasePointsPendingLabel.text = String(purchase.totalPendingPurchasePoints!) + AppConstants.Strings.POINTS_PENDING_TEXT
                    if self.type_Grid{
                        self.collectionView.isHidden = false
                        self.tableView.isHidden = true
                    }else{
                        self.collectionView.isHidden = true
                        self.tableView.isHidden = false
                    }
                    
                }
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getShippingRate(listingId:Int,title:String,id:Int) {
        self.view.makeToastActivity(.center)
        profileViewModel.shippingRate(accessToken: AppDefaults.getAccessToken(), id: listingId, { (response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<ShippingRate> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    let shipping = data.data!
                    let shippingRate:Double = shipping.shippingRate!
                    self.shippingData = shipping
                    self.emailShippingLabelDialog(id:id, title:title,shippingRate:shippingRate,data:shipping)
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func relistItem(sale:SalesDetail) {
        self.view.makeToastActivity(.center)
        profileViewModel.relistItem(accessToken: AppDefaults.getAccessToken(), listingId: sale.listingId!, { (response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 201 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    self.openMyBynScreen()
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func openMyBynScreen() {
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.USER_BYN_BOARD, bundle: nil)
        let userBynViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.USER_BYN_SCREEN) as! UserBynViewController
        
        var vcArray = self.navigationController?.viewControllers
        vcArray!.removeLast()
        vcArray!.append(userBynViewController)
        self.navigationController?.setViewControllers(vcArray!, animated: false)
    }
    
    func purchaseFeedback(id:Int,rating:Int,comment:String) {
        let purchaseData = PurchaseDetail(purchaseId: id, rating: rating, feedback: comment)
        profileViewModel.buyerFeedback(accessToken: AppDefaults.getAccessToken(), id: id, data: purchaseData, { (response) in
            if response.response?.statusCode == 201 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    self.navigationController?.dismiss(animated: false, completion: nil)
                    self.view.makeToastActivity(.center)
                    self.currentPage = 1
                    self.getPurchase()
                    
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func sellerFeedback(id:Int,rating:Int,comment:String) {
        let sellerData = SalesDetail(saleId: id, rating: rating, feedback: comment)
        profileViewModel.sellerFeedback(accessToken: AppDefaults.getAccessToken(), id: id, data: sellerData, { (response) in
            if response.response?.statusCode == 201 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    self.navigationController?.dismiss(animated: false, completion: nil)
                    self.view.makeToastActivity(.center)
                    self.currentPage = 1
                    self.getSale()
                }
            }
        } , { (error) in
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func emailShippingLabel(id:Int) {
        self.view.makeToastActivity(.center)
        self.dismiss(animated: false, completion: nil)
        profileViewModel.emailShippingLabel(accessToken: AppDefaults.getAccessToken(), id: id, { (response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 201 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    let storyBoard = UIStoryboard(name: AppConstants.Storyboard.PRINT_SHIPPING_LABEL_DIALOG_BOARD, bundle:nil)
                    let printShippingLabelDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.PRINT_SHIPPING_LABEL_DIALOG_SCREEN) as! PrintShippingLabelDialogViewController
                    printShippingLabelDialogViewController.modalPresentationStyle = .overFullScreen
                    printShippingLabelDialogViewController.TYPE = AppConstants.TYPE_SHIPPING_LABEL_DONE
                    printShippingLabelDialogViewController.printShippingLabelDelegate = self
                    printShippingLabelDialogViewController.listingTitle = self.sale?.listingTitle!
                    self.navigationController?.present(printShippingLabelDialogViewController, animated:false,completion: nil)
                    
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func emailShippingLabelFreeShipping(id:Int,data:ShipRates) {
        self.view.makeToastActivity(.center)
        self.dismiss(animated: false, completion: nil)
        profileViewModel.emailShippingLabelFreeShipping(accessToken: AppDefaults.getAccessToken(), id: id,data: data, { (response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 201 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    let storyBoard = UIStoryboard(name: AppConstants.Storyboard.PRINT_SHIPPING_LABEL_DIALOG_BOARD, bundle:nil)
                    let printShippingLabelDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.PRINT_SHIPPING_LABEL_DIALOG_SCREEN) as! PrintShippingLabelDialogViewController
                    printShippingLabelDialogViewController.modalPresentationStyle = .overFullScreen
                    printShippingLabelDialogViewController.TYPE = AppConstants.TYPE_SHIPPING_LABEL_DONE
                    printShippingLabelDialogViewController.printShippingLabelDelegate = self
                    printShippingLabelDialogViewController.listingTitle = self.sale?.listingTitle!
                    self.navigationController?.present(printShippingLabelDialogViewController, animated:false,completion: nil)
                    
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    
    func checkReportAnIssue(id:Int) {
        self.view.makeToastActivity(.center)
        profileViewModel.checkReportAnIssue(accessToken: AppDefaults.getAccessToken(), id: id, { (response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<ListingCheck> = Utils.convertResponseToData(data: response.data!)
                
                let status = data.data!
                if status.isError ?? false{
                    self.showMessageDialog(message: status.errorMessage ?? "")
                }else{
                    if self.TYPE == AppConstants.TYPE_PURCHASE{
                        self.reportAnIssuePurchaseScreen(purchaseDetail:self.purchase!)
                    }else{
                        self.reportAnIssueSaleScreen(saleDetail:self.sale!)
                    }
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func reportAnIssuePurchaseScreen(purchaseDetail:PurchaseDetail){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.REPORT_AN_ISSUE_STORYBOARD, bundle: nil)
        let reportAnIssueViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.REPORT_AN_ISSUE_SCREEN) as! ReportAnIssueViewController
        //reportAnIssueViewController.TYPE = AppConstants.TYPE_ITEM_DELIVERED
        reportAnIssueViewController.purchaseDetail = purchaseDetail
        reportAnIssueViewController.reportAnIssueDelegate = self
        self.navigationController?.pushViewController(reportAnIssueViewController, animated: false)
    }
    
    func reportAnIssueSaleScreen(saleDetail:SalesDetail){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.REPORT_AN_ISSUE_STORYBOARD, bundle: nil)
        let reportAnIssueViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.REPORT_AN_ISSUE_SCREEN) as! ReportAnIssueViewController
        //reportAnIssueViewController.TYPE = AppConstants.TYPE_ITEM_DELIVERED
        reportAnIssueViewController.saleDetail = saleDetail
        reportAnIssueViewController.reportAnIssueDelegate = self
        self.navigationController?.pushViewController(reportAnIssueViewController, animated: false)
    }
    
    func cancelSaleScreen(saleDetail:SalesDetail){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.CANCEL_SALE_STOEYBOARD, bundle: nil)
        let cancelSaleViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.CANCEL_SALE_SCREEN) as! CancelSaleViewController
        cancelSaleViewController.saleDetail = saleDetail
        cancelSaleViewController.cancelOrderSaleDelegate = self
        self.navigationController?.pushViewController(cancelSaleViewController, animated: false)
    }
    
    func openCancelOrderSaleDialog(title:String){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.CANCEL_ORDER_SALE_DIALOG_STORYBOARD, bundle: nil)
        let dialogViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.CANCEL_ORDER_SALE_DIALOG) as! CancelOrderSaleDialogViewController
        dialogViewController.modalPresentationStyle = .overFullScreen
        dialogViewController.listingTitle = title
        self.navigationController?.present(dialogViewController, animated: false, completion: nil)
    }
    
    func disputeDetailScreen(id:Int){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.DISPUTE_STORYBOARD, bundle: nil)
        let disputeViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.DISPUTE_DETAILS_SCREEN) as! DisputeDetailsViewController
        disputeViewController.disputeId = id
        disputeViewController.TYPE = self.TYPE
        disputeViewController.cancelDisputeRefreshDelegate = self
        disputeViewController.refreshSalePurchaseDelegate = self
        self.navigationController?.pushViewController(disputeViewController, animated: false)
    }
    
    func successMessageDialog(){
        let storyBoard = UIStoryboard.init(name: AppConstants.Storyboard.SUCCESS_MESSAGE_DIALOG_BOARD, bundle: nil)
        let successViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.SUCCESS_MESSAGE_DIALOG) as! SuccessMessageViewController
        successViewController.modalPresentationStyle = .overFullScreen
        successViewController.TYPE = AppConstants.TYPE_CANCEL_DISPUTE
        self.navigationController?.present(successViewController, animated: false, completion: nil)
    }
    
    func cancelPurchaseDialog(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.WITHDRAW_DIALOG_BOARD, bundle: nil)
        let reportAnIssueDialogViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.WITHDRAW_DIALOG) as! WithdrawDialogViewController
        reportAnIssueDialogViewController.modalPresentationStyle = .overFullScreen
        reportAnIssueDialogViewController.TYPE = AppConstants.TYPE_REPORT_AN_ISSUE
        reportAnIssueDialogViewController.purchaseDetail = self.purchase!
        reportAnIssueDialogViewController.itemTitle = self.purchase?.listingTitle!
        reportAnIssueDialogViewController.cancelPurchaseDelegate = self
        self.navigationController?.present(reportAnIssueDialogViewController, animated: false, completion: nil)
    }
    
    func cancelpurchase(id:Int) {
        self.view.makeToastActivity(.center)
        profileViewModel.cancelPurchase(accessToken: getAccessToken(),id:id ,{(response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<Bool> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    if self.TYPE == AppConstants.TYPE_PURCHASE{
                        self.currentPage = 1
                        self.getPurchase()
                    }else{
                        self.currentPage = 1
                        self.getSale()
                    }
                    self.view.hideToastActivity()
                }
            } else {
                self.view.hideToastActivity()
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func emailReturnLabelDialog(id:Int,title:String){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.PRINT_SHIPPING_LABEL_DIALOG_BOARD, bundle:nil)
        let printShippingLabelDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.PRINT_SHIPPING_LABEL_DIALOG_SCREEN) as! PrintShippingLabelDialogViewController
        printShippingLabelDialogViewController.modalPresentationStyle = .overFullScreen
        printShippingLabelDialogViewController.id = id
        printShippingLabelDialogViewController.TYPE = AppConstants.TYPE_PRINT_RETURN_LABEL
        printShippingLabelDialogViewController.listingTitle = title
        printShippingLabelDialogViewController.printReturnLabelDelegate = self
        self.navigationController?.present(printShippingLabelDialogViewController, animated:false,completion: nil)
    }
    
    func emailReturnLabel(id:Int) {
        self.view.makeToastActivity(.center)
        self.dismiss(animated: false, completion: nil)
        profileViewModel.returnLabel(accessToken: AppDefaults.getAccessToken(), id: id, { (response) in
            if response.response?.statusCode == 201 || response.response?.statusCode == 200{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                  self.view.hideToastActivity()
                    let storyBoard = UIStoryboard(name: AppConstants.Storyboard.PRINT_SHIPPING_LABEL_DIALOG_BOARD, bundle:nil)
                    let printShippingLabelDialogViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.DialogController.PRINT_SHIPPING_LABEL_DIALOG_SCREEN) as! PrintShippingLabelDialogViewController
                    printShippingLabelDialogViewController.modalPresentationStyle = .overFullScreen
                    printShippingLabelDialogViewController.TYPE = AppConstants.TYPE_RETURN_LABEL_DONE
                    printShippingLabelDialogViewController.printReturnLabelDelegate = self
                    printShippingLabelDialogViewController.listingTitle = self.purchase?.listingTitle!
                    self.navigationController?.present(printShippingLabelDialogViewController, animated:false,completion: nil)
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
              self.view.hideToastActivity()
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func confirmDeliveryStatus(id:Int) {
        self.view.makeToastActivity(.center)
        self.dismiss(animated: false, completion: nil)
        profileViewModel.confirmDeliveryStatus(accessToken: AppDefaults.getAccessToken(), saleId: id, { (response) in
            if response.response?.statusCode == 201 || response.response?.statusCode == 200{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }else{
                    self.currentPage = 1
                    self.getSale()
                    self.view.hideToastActivity()
                }
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.view.hideToastActivity()
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func disputeDetailScreenForAddingImages(id:Int){
        let storyBoard = UIStoryboard(name: AppConstants.Storyboard.DISPUTE_STORYBOARD, bundle: nil)
        let disputeViewController = storyBoard.instantiateViewController(withIdentifier: AppConstants.Controllers.DISPUTE_DETAILS_SCREEN) as! DisputeDetailsViewController
        disputeViewController.disputeId = id
        disputeViewController.TYPE = self.TYPE
        disputeViewController.isThereProblemWithReturn = true
        disputeViewController.cancelDisputeRefreshDelegate = self
        disputeViewController.refreshSalePurchaseDelegate = self
        self.navigationController?.pushViewController(disputeViewController, animated: false)
    }
    
    func editShippingScreen(shipRates:[ShipRates],buyerId:Int,addressId:Int,listingId:Int,saleId:Int){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.SELECT_SHIPPING_METHOD_STORYBOARD, bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.SELECT_SHIPPING_METHOD_SCREEN) as! SelectShippingMethodViewController
        viewController.shipRates = shipRates
        viewController.buyerId = buyerId
        viewController.addressId = addressId
        viewController.listingId = listingId
        viewController.TYPE = AppConstants.TYPE_SELECT_SHIPPING_METHOD
        viewController.saleId = saleId
        viewController.selectShipRateDelegate = self
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}

extension SalePurchaseViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        
        var title:String = ""
        if TYPE == AppConstants.TYPE_PURCHASE{
            title = AppConstants.Strings.MY_PURCHASES
        }else{
            title = AppConstants.Strings.MY_SALES
        }
        return IndicatorInfo(title: title)
    }
}
