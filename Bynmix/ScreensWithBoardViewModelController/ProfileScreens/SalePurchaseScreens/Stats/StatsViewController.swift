import UIKit
import XLPagerTabStrip

class StatsViewController: BaseViewController {
    
    var TYPE:Int?
    
    @IBOutlet var feedbackTextLabel: UILabel!
    @IBOutlet var feedbackLabel: UILabel!
    @IBOutlet var totalsalePurchaseTextLabel: UILabel!
    @IBOutlet var totalsalePurchaseLabel: UILabel!
    @IBOutlet var totalSpendEarnedTextLabel: UILabel!
    @IBOutlet var totalSpendEarnedLabel: UILabel!
    @IBOutlet var pointsEarnedLabel: UILabel!
    @IBOutlet var pendingPointsLabel: UILabel!
    @IBOutlet var availabelCreditsLabel: UILabel!
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var feedbackBottomView: UIView!
    @IBOutlet var totalPurchaseBottomView: UIView!
    @IBOutlet var spendBottomView: UIView!
    @IBOutlet var earnedView: UIView!
    @IBOutlet var pendingView: UIView!
    @IBOutlet var creditView: UIView!
    
    let profileViewModel = ProfileViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        typeStats()
//        feedbackBottomView.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMinXMaxYCorner]
//        totalPurchaseBottomView.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMinXMaxYCorner]
//        spendBottomView.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMinXMaxYCorner]
//        earnedView.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMinXMaxYCorner]
//        pendingView.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMinXMaxYCorner]
//        creditView.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMinXMaxYCorner]
    }
    
    func typeStats(){
        if TYPE == AppConstants.TYPE_PURCHASE{
            feedbackTextLabel.text = AppConstants.Strings.PURCHASES_NEED_FEEDBACK
            totalsalePurchaseTextLabel.text = AppConstants.Strings.TOTAL_PURCHASES
            totalSpendEarnedTextLabel.text = AppConstants.Strings.SPEND_THIS_MONTH
            getPurchaseStats()
        }else{
            feedbackTextLabel.text = AppConstants.Strings.SELLER_FEEDBACK
            totalsalePurchaseTextLabel.text = AppConstants.Strings.TOTAL_SALES
            totalSpendEarnedTextLabel.text = AppConstants.Strings.TOTAL_EARNED
            getSaleStats()
        }
    }
    
    func getSaleStats() {
        profileViewModel.getSaleStats(accessToken: AppDefaults.getAccessToken(), { (response) in
            self.screenActivityIndicator.stopAnimating()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<MySalesStats> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let sale = data.data!
                    
                    self.feedbackLabel.text = String(format: AppConstants.FORMAT_TWO_DECIMAL,sale.totalSellerFeedbackScore!) + AppConstants.PERCENT_SIGN
                    if sale.totalSaleItems! == 0 && sale.totalPendingSaleItems! == 0{
                        self.totalsalePurchaseLabel.text = AppConstants.ZERO
                    }else{
                        self.totalsalePurchaseLabel.text = String(sale.totalSales ?? 0)
                    }
                    self.totalSpendEarnedLabel.text = AppConstants.DOLLAR + String(format: AppConstants.FORMAT_TWO_DECIMAL,sale.totalSales!)
                    self.pointsEarnedLabel.text = String(sale.totalSalePoints!)
                    self.pendingPointsLabel.text = String(sale.totalPendingSalePoints!)
                    self.availabelCreditsLabel.text = String(sale.totalAvailableForWithdrawal!)
                    self.scrollView.isHidden = false
                }
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getPurchaseStats() {
        profileViewModel.getPurchaseStats(accessToken: AppDefaults.getAccessToken(), { (response) in
            self.screenActivityIndicator.stopAnimating()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<MyPurchasesStats> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let purchase = data.data!
                    self.feedbackLabel.text = String(purchase.purchasesNeedingFeedback!)
                    self.totalsalePurchaseLabel.text = String(purchase.totalPurchaseItems!)
                    self.totalSpendEarnedLabel.text = AppConstants.DOLLAR + String(format: AppConstants.FORMAT_TWO_DECIMAL,purchase.totalSpentThisMonth!)
                    self.pointsEarnedLabel.text = String(purchase.totalPurchasePoints!)
                    self.pendingPointsLabel.text = String(purchase.totalPendingPurchasePoints!)
                    self.availabelCreditsLabel.text = String(purchase.creditTotal!)
                    self.scrollView.isHidden = false
                }
            }
        } , { (error) in
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }

}
extension StatsViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: AppConstants.Strings.MY_STATS)
    }
}
