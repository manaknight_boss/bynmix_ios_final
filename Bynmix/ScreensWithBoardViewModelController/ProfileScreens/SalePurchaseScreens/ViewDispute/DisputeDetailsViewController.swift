import UIKit

class DisputeDetailsViewController: BaseViewController,CancelDisputeProtocol,NoCommentProtocol,AddCommentInDisputeProtocol,EscalateDisputeProtocol,RefundBuyerWithoutReturnProtocol,SellerSideEscalateDisputeProtocol,SellerSideBackToOptionsProtocol {
    
    func sellerSideBackToOptions(type: Int, disputeDetails: DisputeDetails) {
        self.dismiss(animated: false, completion: nil)
        if type == AppConstants.TYPE_SELLER_SIDE_REFUND_BUYER_WITHOUT_RETURN || type == AppConstants.TYPE_SELLER_SIDE_ESCLATE_DISPUTE{
            self.disputeDialog(type:AppConstants.TYPE_SELLER_SIDE_DISPUTE_OPTIONS_EXPANDED,disputeDetails:disputeDetails)
        }
    }
    
    func sellerSideEscalateDispute(type:Int,disputeDetails:DisputeDetails){
        self.dismiss(animated: false, completion: nil)
        self.disputeDialog(type:AppConstants.TYPE_SELLER_SIDE_ESCLATE_DISPUTE,disputeDetails:disputeDetails)
    }
    
    func refundBuyerWithoutReturn(type: Int, disputeDetails: DisputeDetails) {
        self.dismiss(animated: false, completion: nil)
        self.disputeDialog(type:AppConstants.TYPE_SELLER_SIDE_REFUND_BUYER_WITHOUT_RETURN,disputeDetails:disputeDetails)
    }
    
    func escalateDispute(type:Int,disputeDetails: DisputeDetails) {
        self.dismiss(animated: false, completion: nil)
        if type == AppConstants.TYPE_BUYER_SIDE_ESCLATE_DISPUTE{
            self.escalateDisputeOption(id: disputeDetails.id!)
        }else if type == AppConstants.TYPE_SELLER_SIDE_DISPUTE_OPTIONS_EXPANDED{
            self.disputeDialog(type:AppConstants.TYPE_SELLER_SIDE_ESCLATE_DISPUTE,disputeDetails:disputeDetails)
        }else if type == AppConstants.TYPE_SELLER_SIDE_REFUND_BUYER_WITHOUT_RETURN{
            self.refundBuyerWithoutReturnOption(id:disputeDetails.id!)
        }else if type == AppConstants.TYPE_SELLER_SIDE_ESCLATE_DISPUTE{
            self.escalateDisputeOption(id: disputeDetails.id!)
        }
        
    }
    
    func noComment() {
        self.view.makeToast(AppConstants.Strings.ADD_COMMENT_FIRST)
        self.dismiss(animated: false, completion: nil)
    }
    
    func addCommentInDispute(dispute:DisputeDetails,comment:String,images:[UIImage]){
        self.dismiss(animated: false, completion: nil)
        self.view.makeToastActivity(.center)
        let id = dispute.id!
        self.addDisputeCommment(id:id,comment:comment,images:images)
    }
    
    func cancelDispute(disputeDetails:DisputeDetails){
        self.dismiss(animated: false, completion: nil)
        let id  = disputeDetails.id!
        self.cancelDispute(id: id)
    }
    
    @IBOutlet weak var cancelDisputeView: UIView!
    @IBOutlet weak var disputeStatusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var disputeReasonLabel: UILabel!
    
    @IBOutlet weak var commentsView: UIView!
    @IBOutlet weak var imagesView: UIView!
    @IBOutlet weak var additionalCommentsLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    
    @IBOutlet weak var imageOne: UIImageView!
    @IBOutlet weak var imageTwo: UIImageView!
    @IBOutlet weak var imageThree: UIImageView!
    @IBOutlet weak var imageFour: UIImageView!
    
    @IBOutlet weak var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var leaveCommentButton: UIButton!
    @IBOutlet var tableViewHeight: NSLayoutConstraint!
    @IBOutlet var headerViewHeight: NSLayoutConstraint!
    @IBOutlet var headerView: UIView!
    
    @IBOutlet var commentsStackView: UIStackView!
    
    @IBOutlet var commentsTextLabel: UILabel!
    @IBOutlet var attachedImagesTextLabel: UILabel!
    @IBOutlet var imagesInnerView: UIView!
    
    @IBOutlet var leaveCommentView: UIView!
    @IBOutlet var leaveCommentViewHeight: NSLayoutConstraint!
    
    @IBOutlet var contentView: UIView!
    @IBOutlet var leaveCommentViewTopConstraint: NSLayoutConstraint!
    
    @IBOutlet var refundLabel: UILabel!
    @IBOutlet var escalateDisputeButton: UIButton!
    @IBOutlet var escalateDisputeView: UIView!
    
    var refreshControl = UIRefreshControl()
    
    var disputeId:Int?
    let profileViewModel = ProfileViewModel()
    var disputeDetails:DisputeDetails?
    var cancelDisputeRefreshDelegate:CancelDisputeRefreshProtocol?
    var disputeCommentHandler = DisputeCommentHandler()
    var TYPE:Int?
    var refreshSalePurchaseDelegate:RefreshSalePurchaseProtocol?
    var isThereProblemWithReturn = false
    var additionalComments :[Comments] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.Strings.DISPUTE_DETAILS_TITLE
        let backBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.BACK_BUTTON), style: .plain, target: self, action: #selector(onBackBarButtonItem))
        self.navigationItem.leftBarButtonItem  = backBarButton
        tableView.delegate = disputeCommentHandler
        tableView.dataSource = disputeCommentHandler
//        tableView.rowHeight = UITableView.automaticDimension
//        tableView.estimatedRowHeight = 50
        getDisputeDetails(id:disputeId!)
        addRefreshControl()
        onCommentImageClick()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        if self.additionalComments.count > 0{
            self.resizeTableView(comments: additionalComments)
        }
    }
    
    func onCommentImageClick(){
        disputeCommentHandler.onImageOneClick = { comments in
            let image = comments.images?[0]
            self.imageFullScreenDialog(image:image!)
        }
        disputeCommentHandler.onImageTwoClick = { comments in
            let image = comments.images?[1]
            self.imageFullScreenDialog(image:image!)
        }
        disputeCommentHandler.onImageThreeClick = { comments in
            let image = comments.images?[2]
            self.imageFullScreenDialog(image:image!)
        }
    }
    
    @objc func onBackBarButtonItem(){
        self.navigationController?.popViewController(animated: false)
        //self.refreshSalePurchaseDelegate?.refreshSalePurchase()
    }
    
    @IBAction func cancelDisputeAction(_ sender: UIButton) {
        cancelDisputeDialog()
    }
    
    func cancelDisputeDialog(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.WITHDRAW_DIALOG_BOARD, bundle: nil)
        let reportAnIssueDialogViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.WITHDRAW_DIALOG) as! WithdrawDialogViewController
        reportAnIssueDialogViewController.modalPresentationStyle = .overFullScreen
        reportAnIssueDialogViewController.TYPE = AppConstants.TYPE_CANCEL_DISPUTE
        reportAnIssueDialogViewController.disputeDetails = self.disputeDetails!
        reportAnIssueDialogViewController.cancelDisputeDelegate = self
        self.navigationController?.present(reportAnIssueDialogViewController, animated: false, completion: nil)
    }
    
    func getDisputeDetails(id:Int) {
        profileViewModel.getDisputeDetails(accessToken: getAccessToken(),id:id ,{(response) in
            self.screenActivityIndicator.stopAnimating()
            self.refreshControl.endRefreshing()
            if response.response?.statusCode == 200 {
                let data: ApiResponse<DisputeDetails> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let disputeDetails = data.data!
                    
                    self.setData(disputeDetails:disputeDetails)
                }
            } else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.refreshControl.endRefreshing()
            self.screenActivityIndicator.stopAnimating()
            self.mainScrollView.isHidden = false
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func cancelDispute(id:Int) {
        self.view.makeToastActivity(.center)
        profileViewModel.cancelDispute(accessToken: getAccessToken(),id:id ,{(response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 201 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.cancelDisputeRefreshDelegate?.cancelDisputeRefresh()
                    self.navigationController?.popViewController(animated: false)
                }
            } else {
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func setData(disputeDetails:DisputeDetails){
        self.disputeDetails = disputeDetails
        let text:String = "Dispute Status: " + disputeDetails.disputeStatus!
        let textFont:String = "Dispute Status: "
        let range = (text as NSString).range(of: textFont)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 14), range: range)
        self.disputeStatusLabel.attributedText = attributedString
        self.dateLabel.text = "Submitted on " + disputeDetails.createdDateFormatted!
        
        let textDisputeReason:String = "Dispute Reason: " + disputeDetails.disputeType!
        let textDisputeReasonFont:String = "Dispute Reason: "
        let disputeReasonRange = (textDisputeReason as NSString).range(of: textDisputeReasonFont)
        let disputeReasonAttributedString = NSMutableAttributedString(string: textDisputeReason)
        disputeReasonAttributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 14), range: disputeReasonRange)
        self.disputeReasonLabel.attributedText = disputeReasonAttributedString
        
        if (disputeDetails.canCancelDispute ?? false) {
            cancelDisputeView.isHidden = false
            //headerViewHeight.constant = 227.5
        } else {
            cancelDisputeView.isHidden = true
            //headerViewHeight.constant = 167.5
        }
        
        if disputeDetails.comments == ""{
            self.commentsView.isHidden = true
            //self.commentsViewHeight.constant = 0
            self.commentsTextLabel.isHidden = true
            self.commentsTextLabel.isHidden = true
        }else{
            //self.commentsViewHeight.constant = 117
            self.commentsView.isHidden = false
            self.commentsTextLabel.isHidden = false
            self.commentsTextLabel.isHidden = false
            self.commentLabel.text = disputeDetails.comments!
        }
        
        if (disputeDetails.images != nil && disputeDetails.images!.count > 0) {
            self.imagesView.isHidden = false
            //imagesViewHeight.constant = 119
            self.attachedImagesTextLabel.isHidden = false
            self.imagesInnerView.isHidden = false
            self.addImages(disputeDetails: disputeDetails)
        } else {
            self.imagesView.isHidden = true
            self.attachedImagesTextLabel.isHidden = true
            //imagesViewHeight.constant = 0
            self.imagesInnerView.isHidden = true
            self.imageOne.isHidden = true
            self.imageTwo.isHidden = true
            self.imageThree.isHidden = true
            self.imageFour.isHidden = true
        }
        
        if disputeDetails.additionalComments != nil && disputeDetails.additionalComments?.count ?? 0 > 0{
            self.additionalCommentsLabel.isHidden = false
        }else{
            self.additionalCommentsLabel.isHidden = true
        }
        
        if disputeDetails.canLeaveComment ?? false{
            self.leaveCommentButton.isHidden = false
            self.leaveCommentViewHeight.constant = 75
        }else{
            self.leaveCommentButton.isHidden = true
            self.leaveCommentViewHeight.constant = 0
        }
        
//        if self.TYPE == AppConstants.TYPE_PURCHASE{
//            if disputeDetails.canEscalateDispute ?? false{
//                self.escalateDisputeView.isHidden = false
//                self.escalateDisputeButton.setTitle(AppConstants.Strings.ESCALATE_DISPUTE, for: .normal)
//            }else{
//                self.escalateDisputeView.isHidden = true
//            }
//            if disputeDetails.escalateRequested ?? false{
//                self.disableEscalateDisputeButton()
//            }else{
//                escalateDisputeButton.backgroundColor = UIColor.cDC4293
//                escalateDisputeButton.isEnabled = true
//                escalateDisputeButton.setTitle(AppConstants.Strings.ESCALATE_DISPUTE, for: .normal)
//            }
//
//        }else{
//            if disputeDetails.canEscalateDispute ?? false{
//                self.escalateDisputeView.isHidden = false
//                self.escalateDisputeButton.setTitle(AppConstants.Strings.OTHER_OPTIONS, for: .normal)
//            }else{
//                self.escalateDisputeView.isHidden = true
//            }
//        }
        
        if self.TYPE == AppConstants.TYPE_SALE{
            if (!(disputeDetails.isResolved ?? false)){
                self.escalateDisputeView.isHidden = false
                UIView.performWithoutAnimation {
                    self.escalateDisputeButton.setTitle(AppConstants.Strings.OTHER_OPTIONS, for: .normal)
                    self.escalateDisputeButton.layoutIfNeeded()
                }
            } else {
                self.escalateDisputeView.isHidden = true
            }

        } else {

            if (disputeDetails.disputeEscalated ?? false){
                self.disableEscalateDisputeButton()
                self.escalateDisputeView.isHidden = false
            } else {
                if (disputeDetails.canEscalateDispute ?? false){
                    self.escalateDisputeView.isHidden = false
                    UIView.performWithoutAnimation {
                        self.escalateDisputeButton.setTitle(AppConstants.Strings.ESCALATE_DISPUTE, for: .normal)
                        self.escalateDisputeButton.layoutIfNeeded()
                    }
                    
                } else {
                    self.escalateDisputeView.isHidden = true
                }
            }
        }
        
        if disputeDetails.resolutionDescription != nil{
            refundLabel.isHidden = false
            refundLabel.text = disputeDetails.resolutionDescription
        }else{
            refundLabel.isHidden = true
        }
        
        if disputeDetails.additionalComments?.count != 0{
            disputeCommentHandler.comments = disputeDetails.additionalComments!
            tableView.reloadData()
            self.additionalComments = disputeDetails.additionalComments!
            self.resizeTableView(comments: disputeDetails.additionalComments!)
        }else{
            tableView.isHidden = true
        }
        self.showScreenAndHideLoader()
    }
    
    func showScreenAndHideLoader(){
        self.mainScrollView.isHidden = false
        self.view.hideToastActivity()
    }
    
    func resizeTableView(comments:[Comments]){
        if comments.count == 0{
            tableViewHeight.constant = 0
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.25) {
                var tableViewHeight: CGFloat {
                    self.tableView.layoutIfNeeded()
                    return self.tableView.contentSize.height
                }
                self.tableViewHeight?.constant = tableViewHeight + 20
            }
        }
        tableView.isHidden = false
    }
    
    func addImages(disputeDetails:DisputeDetails){
        let imagesCount = disputeDetails.images?.count
        let images = disputeDetails.images!
        let image = UIImage(named: AppConstants.Images.DEFAULT_OFFER_PLACEHOLDER)
        
        if imagesCount == 4{
            
            self.imageOne.kf.setImage(with: URL(string: images[0])!,placeholder: image)
            self.imageTwo.kf.setImage(with: URL(string: images[1])!,placeholder: image)
            self.imageThree.kf.setImage(with: URL(string: images[2])!,placeholder: image)
            self.imageFour.kf.setImage(with: URL(string: images[3])!,placeholder: image)
            
            self.imageOneAddBorder()
            self.imageTwoAddBorder()
            self.imageThreeAddBorder()
            self.imageFourAddBorder()
            
        }else if imagesCount == 3{
            self.imageOne.kf.setImage(with: URL(string: images[0])!,placeholder: image)
            self.imageTwo.kf.setImage(with: URL(string: images[1])!,placeholder: image)
            self.imageThree.kf.setImage(with: URL(string: images[2])!,placeholder: image)
            
            self.imageOneAddBorder()
            self.imageTwoAddBorder()
            self.imageThreeAddBorder()
            
        }else if imagesCount == 2{
            self.imageOne.kf.setImage(with: URL(string: images[0])!,placeholder: image)
            self.imageTwo.kf.setImage(with: URL(string: images[1])!,placeholder: image)
            self.imageOneAddBorder()
            self.imageTwoAddBorder()
            
        }else if imagesCount == 1{
            self.imageOne.kf.setImage(with: URL(string: images[0])!,placeholder: image)
            self.imageOneAddBorder()
        }
        
    }
    
    func imageOneAddBorder(){
        self.imageOne.layer.masksToBounds = true
        self.imageOne.layer.borderWidth = 1
        self.imageOne.layer.borderColor = UIColor.black.cgColor
    }
    
    func imageTwoAddBorder(){
        self.imageTwo.layer.masksToBounds = true
        self.imageTwo.layer.borderWidth = 1
        self.imageTwo.layer.borderColor = UIColor.black.cgColor
    }
    
    func imageThreeAddBorder(){
        self.imageThree.layer.masksToBounds = true
        self.imageThree.layer.borderWidth = 1
        self.imageThree.layer.borderColor = UIColor.black.cgColor
    }
    
    func imageFourAddBorder(){
        self.imageFour.layer.masksToBounds = true
        self.imageFour.layer.borderWidth = 1
        self.imageFour.layer.borderColor = UIColor.black.cgColor
    }
    
    @IBAction func imageOneTapAction(_ sender: UITapGestureRecognizer) {
        if (self.disputeDetails?.images!.count)! > 0{
            let image = self.disputeDetails?.images![0]
            self.imageFullScreenDialog(image:image!)
        }
    }
    
    @IBAction func imageTwoTapAction(_ sender: UITapGestureRecognizer) {
        if (self.disputeDetails?.images!.count)! > 1{
            let image = self.disputeDetails?.images![1]
            self.imageFullScreenDialog(image:image!)
        }
    }
    
    @IBAction func imageThreeTapAction(_ sender: UITapGestureRecognizer) {
        if (self.disputeDetails?.images!.count)! > 2{
            let image = self.disputeDetails?.images![2]
            self.imageFullScreenDialog(image:image!)
        }
    }
    
    @IBAction func imageFourTapAction(_ sender: UITapGestureRecognizer) {
        if (self.disputeDetails?.images!.count)! > 3{
            let image = self.disputeDetails?.images![3]
            self.imageFullScreenDialog(image:image!)
        }
    }
    
    func imageFullScreenDialog(image:String){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.IMAGE_FULL_SCREEN_STORYBOARD, bundle: nil)
        let dialogViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.IMAGE_FULL_SCREEN_DIALOG) as! ImageFullScreenDialogViewController
        dialogViewController.itemImage = image
        dialogViewController.modalPresentationStyle = .overFullScreen
        self.present(dialogViewController, animated: true, completion: nil)
    }
    
    @IBAction func leaveCommentAction(_ sender: UIButton) {
       leaveCommentDialog()
    }
    
    func leaveCommentDialog(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.ADD_COMMENT_DIALOG_STORYBOARD, bundle: nil)
        let commentViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.ADD_COMMENT_DIALOG) as! AddCommentDialogViewController
        commentViewController.isThereProblemWithReturn = self.isThereProblemWithReturn
        commentViewController.disputeDetails = self.disputeDetails
        commentViewController.noCommentDelegate = self
        commentViewController.addCommentInDisputeDelegate = self
        self.navigationController?.present(commentViewController, animated: false, completion: nil)
    }
    
    func addDisputeCommment(id:Int,comment:String,images:[UIImage]) {
        self.view.makeToastActivity(.center)
        let comments = Comments(additionalComment: comment)
        profileViewModel.addDisputeCommment(accessToken: getAccessToken(),id:id, data: comments ,{(response) in
            if response.response?.statusCode == 201 {
                let data: ApiResponse<Int> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    let commentId = data.data!
                    
                    if images.count > 0{
                        self.uploadImages(disputeId: id, commentId: commentId, imageList: images)
                    }else{
                        self.getDisputeDetails(id:self.disputeId!)
                    }
                }
            } else {
                self.view.hideToastActivity()
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func uploadImages(disputeId:Int,commentId:Int,imageList:[UIImage]){
        profileViewModel.uploadCommentImages(accessToken: getAccessToken(), disputeId: disputeId, commentId: commentId, images: imageList, { (response) in
            self.view.hideToastActivity()
            if response.response?.statusCode == 201 || response.response?.statusCode == 200{
                
                self.getDisputeDetails(id:self.disputeId!)
                
            }else{
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
                self.view.hideToastActivity()
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func escalateDisputeOption(id:Int) {
        self.view.makeToastActivity(.center)
        profileViewModel.escalateDispute(accessToken: getAccessToken(),id:id ,{(response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.view.hideToastActivity()
                    self.disableEscalateDisputeButton()
                }
            } else {
                self.view.hideToastActivity()
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func refundBuyerWithoutReturnOption(id:Int) {
        self.view.makeToastActivity(.center)
        profileViewModel.refundBuyerWithoutReturn(accessToken: getAccessToken(),id:id ,{(response) in
            if response.response?.statusCode == 200 {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                } else {
                    self.getDisputeDetails(id:id)
                }
            } else {
                self.view.hideToastActivity()
                self.showMessageDialog(message: AppConstants.Strings.SOMETHING_WENT_WRONG)
            }
        } , { (error) in
            self.view.hideToastActivity()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func disableEscalateDisputeButton(){
        escalateDisputeButton.backgroundColor = UIColor.cCBCBCB
        escalateDisputeButton.isEnabled = false
        UIView.performWithoutAnimation {
            escalateDisputeButton.setTitle(AppConstants.Strings.DISPUTE_ESCALATED, for: .normal)
            self.escalateDisputeButton.layoutIfNeeded()
        }
        
    }
    
    func addRefreshControl(){
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        mainScrollView.addSubview(refreshControl)
    }
    
    @objc func refresh(_ sender: AnyObject) {
        self.getDisputeDetails(id:self.disputeId!)
    }
    
    @IBAction func escalateDisputeButtonAction(_ sender: UIButton) {
        if self.TYPE == AppConstants.TYPE_PURCHASE{
            self.disputeDialog(type:AppConstants.TYPE_BUYER_SIDE_ESCLATE_DISPUTE, disputeDetails: self.disputeDetails!)
        }else{
            self.disputeDialog(type:AppConstants.TYPE_SELLER_SIDE_DISPUTE_OPTIONS_EXPANDED, disputeDetails: self.disputeDetails!)
        }
    }
    
    func disputeDialog(type:Int,disputeDetails:DisputeDetails){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.DISPUTE_DIALOG_STORYBOARD, bundle: nil)
        let disputeDialog = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.DISPUTE_DIALOG) as! DisputeDialogViewController
        disputeDialog.modalPresentationStyle = .overCurrentContext
        disputeDialog.TYPE = type
        disputeDialog.disputeDetails = disputeDetails
        disputeDialog.TYPE_BUYER_OR_SELLER = self.TYPE
        disputeDialog.escalateDisputeDelegate = self
        disputeDialog.refundBuyerWithoutReturnDelegate = self
        disputeDialog.sellerSideEscalateDisputeDelegate = self
        disputeDialog.sellerSideBackToOptionsDelegate = self
        self.navigationController?.present(disputeDialog, animated: false, completion: nil)
    }
    
}
