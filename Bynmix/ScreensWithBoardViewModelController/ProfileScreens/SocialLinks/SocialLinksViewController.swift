import UIKit
import XLPagerTabStrip

class SocialLinksViewController: BaseViewController,AccountUnlinkProtocol,URLDialogProtocol {
    
    func URLDialog(type:Int,url: String) {
        switch type {
        case AppConstants.TYPE_ACCOUNT_TIKTOK:
            var newURL = ""
            if url.isEmpty{
                newURL = ""
            }else if !url.starts(with: "https") || !url.starts(with: "http"){
                if url.lowercased().contains("tiktok"){
                    newURL = "https://\(url)"
                }else{
                    newURL = "https://www.tiktok.com/" + (url.starts(with: "@") ? url : "@" + url)
                }
            }
            self.user?.tikTokLink = newURL
            self.tikTokLoginView.isHidden = true
            self.tikTokUsernameView.isHidden = false
            self.tikTokUsernameLabel.text = AppConstants.Strings.CONNECTED_TEXT
        case AppConstants.TYPE_ACCOUNT_FB:
            var newURL = ""
            if url.isEmpty{
                newURL = ""
            }else if !url.starts(with: "https") || !url.starts(with: "http"){
                if url.lowercased().contains("facebook"){
                    newURL = "https://\(url)"
                }else{
                    newURL = "https://www.facebook.com/\(url)"
                }
            }
            self.user?.facebookLink = newURL
            self.fbLoginView.isHidden = true
            self.fbUsernameView.isHidden = false
            self.fbUserNameLabel.text = AppConstants.Strings.CONNECTED_TEXT
        case AppConstants.TYPE_ACCOUNT_YOUTUBE:
            var newURL = ""
            
            if url.isEmpty{
                newURL = ""
            }else if !url.starts(with: "https") || !url.starts(with: "http"){
                if url.lowercased().contains("youtube"){
                    newURL = "https://\(url)"
                }else{
                    newURL = "https://www.youtube.com/\(url)"
                }
            }
            self.user?.youtubeLink = newURL
            self.youtubeLoginView.isHidden = true
            self.youtubeUsernameView.isHidden = false
            self.youtubeUsernameLabel.text = AppConstants.Strings.CONNECTED_TEXT
        case AppConstants.TYPE_ACCOUNT_INSTAGRAM:
            var newURL = ""
            
            if url.isEmpty{
                newURL = ""
            }else if !url.starts(with: "https") || !url.starts(with: "http"){
                if url.lowercased().contains("instagram"){
                    newURL = "https://\(url)"
                }else{
                    newURL = "https://www.instagram.com/\(url)"
                }
            }
            self.user?.instagramLink = newURL
            self.instagramLoginView.isHidden = true
            self.instagramUsernameView.isHidden = false
            self.instagramUsernameLabel.text = AppConstants.Strings.CONNECTED_TEXT
        case AppConstants.TYPE_ACCOUNT_PINTEREST:
            var newURL = ""
            
            if url.isEmpty{
                newURL = ""
            }else if !url.starts(with: "https") || !url.starts(with: "http"){
                if url.lowercased().contains("pinterest"){
                    newURL = "https://\(url)"
                }else{
                    newURL = "https://www.pinterest.com/\(url)"
                }
            }
            self.user?.pinterestLink = newURL
            self.pinterestLoginView.isHidden = true
            self.pinterestUsernameView.isHidden = false
            self.pinterestUsernameLabel.text = AppConstants.Strings.CONNECTED_TEXT
        default:
            break
        }
        self.dismiss(animated: false, completion: nil)
    }
    
    func accountUnlink(type: Int) {
        if type == AppConstants.TYPE_ACCOUNT_FB{
            self.fbLoginView.isHidden = false
            self.fbUsernameView.isHidden = true
            self.user?.facebookLink = ""
        }else if type == AppConstants.TYPE_ACCOUNT_TIKTOK{
            self.tikTokLoginView.isHidden = false
            self.tikTokUsernameView.isHidden = true
            self.user?.tikTokLink = ""
        }else if type == AppConstants.TYPE_ACCOUNT_INSTAGRAM{
            self.instagramLoginView.isHidden = false
            self.instagramUsernameView.isHidden = true
            self.user?.instagramLink = ""
        }else if type == AppConstants.TYPE_ACCOUNT_YOUTUBE{
            self.youtubeLoginView.isHidden = false
            self.youtubeUsernameView.isHidden = true
            self.user?.youtubeLink = ""
        }else{
            self.pinterestLoginView.isHidden = false
            self.pinterestUsernameView.isHidden = true
            self.user?.pinterestLink = ""
        }
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBOutlet var blogWebLinkTextField: UITextField!
    @IBOutlet var buttonLoader: UIActivityIndicatorView!
    @IBOutlet var topConstraint: NSLayoutConstraint!
    @IBOutlet var saveButton: UIButton!
    
    @IBOutlet var fbLoginView: UIView!
    @IBOutlet var fbUsernameView: UIView!
    @IBOutlet var fbUserNameLabel: UILabel!
    
    @IBOutlet var tikTokLoginView: UIView!
    @IBOutlet var tikTokUsernameView: UIView!
    @IBOutlet var tikTokUsernameLabel: UILabel!
    
    @IBOutlet var instagramLoginView: UIView!
    @IBOutlet var instagramUsernameView: UIView!
    @IBOutlet var instagramUsernameLabel: UILabel!
    
    @IBOutlet var youtubeLoginView: UIView!
    @IBOutlet var youtubeUsernameView: UIView!
    @IBOutlet var youtubeUsernameLabel: UILabel!
    
    @IBOutlet var pinterestLoginView: UIView!
    @IBOutlet var pinterestUsernameView: UIView!
    @IBOutlet var pinterestUsernameLabel: UILabel!
    
    var user:User? = nil
    let profileViewModel = ProfileViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buttonLoader.stopAnimating()
        blogWebLinkTextField.text = user?.blogLink
        hideKeyboardWhenTappedAround()
        setInstagramBorder()
        checkIfUserAlreadyLogined()
    }
    
    func setInstagramBorder(){
        let colorStart = UIColor.cAA27C1
        let endColor = UIColor.cFA4E49
        let colors = [colorStart,endColor]
        instagramLoginView.setGradientBorder(width: 4, colors: colors)
        instagramUsernameView.setGradientBorder(width: 4, colors: colors)
    }
    
    func checkIfUserAlreadyLogined(){
        if user?.facebookLink != nil{
            fbUsernameView.isHidden = false
            fbLoginView.isHidden = true
            fbUserNameLabel.text = self.user?.facebookUsername ?? AppConstants.Strings.CONNECTED_TEXT
        }else{
            fbUsernameView.isHidden = true
            fbLoginView.isHidden = false
        }
        if user?.tikTokLink != nil{
            tikTokLoginView.isHidden = true
            tikTokUsernameView.isHidden = false
            tikTokUsernameLabel.text = AppConstants.Strings.CONNECTED_TEXT
        }else{
            tikTokLoginView.isHidden = false
            tikTokUsernameView.isHidden = true
        }
        if user?.instagramLink != nil{
            instagramLoginView.isHidden = true
            instagramUsernameView.isHidden = false
            instagramUsernameLabel.text = self.user?.instagramUsername ?? AppConstants.Strings.CONNECTED_TEXT
        }else{
            instagramLoginView.isHidden = false
            instagramUsernameView.isHidden = true
        }
        if user?.youtubeLink != nil{
            youtubeLoginView.isHidden = true
            youtubeUsernameView.isHidden = false
            youtubeUsernameLabel.text = self.user?.youtubeUsername ?? AppConstants.Strings.CONNECTED_TEXT
        }else{
            youtubeLoginView.isHidden = false
            youtubeUsernameView.isHidden = true
        }
        if user?.pinterestLink != nil{
            pinterestLoginView.isHidden = true
            pinterestUsernameView.isHidden = false
            pinterestUsernameLabel.text = AppConstants.Strings.CONNECTED_TEXT
        }else{
            pinterestLoginView.isHidden = false
            pinterestUsernameView.isHidden = true
        }
    }
    
    @IBAction func fbLoginTapAction(_ sender: UITapGestureRecognizer) {
        self.openURLDialog(type:AppConstants.TYPE_ACCOUNT_FB)
    }
    
    @IBAction func tikTokLoginTapAction(_ sender: UITapGestureRecognizer) {
        self.openURLDialog(type:AppConstants.TYPE_ACCOUNT_TIKTOK)
    }
    
    @IBAction func instagramLoginTapAction(_ sender: UITapGestureRecognizer) {
        self.openURLDialog(type:AppConstants.TYPE_ACCOUNT_INSTAGRAM)
    }
    
    @IBAction func youtubeLoginTapAction(_ sender: UITapGestureRecognizer) {
        self.openURLDialog(type:AppConstants.TYPE_ACCOUNT_YOUTUBE)
    }
    
    @IBAction func pinterestLoginTapAction(_ sender: UITapGestureRecognizer) {
        self.openURLDialog(type:AppConstants.TYPE_ACCOUNT_PINTEREST)
    }
    
    @IBAction func fbUnlinkButtonAction(_ sender: UIButton) {
        self.openUnlinkDialog(typeAccount: AppConstants.TYPE_ACCOUNT_FB)
    }
    
    @IBAction func tiktokUnlinkButtonAction(_ sender: UIButton) {
        self.openUnlinkDialog(typeAccount: AppConstants.TYPE_ACCOUNT_TIKTOK)
    }
    
    @IBAction func instagramUnlinkButtonAction(_ sender: UIButton) {
        self.openUnlinkDialog(typeAccount: AppConstants.TYPE_ACCOUNT_INSTAGRAM)
    }
    
    @IBAction func youtubeUnlinkButtonAction(_ sender: UIButton) {
        self.openUnlinkDialog(typeAccount: AppConstants.TYPE_ACCOUNT_YOUTUBE)
    }
    
    @IBAction func pinterestUnlinkButtonAction(_ sender: UIButton) {
        self.openUnlinkDialog(typeAccount: AppConstants.TYPE_ACCOUNT_PINTEREST)
    }
    
    func openURLDialog(type:Int){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.CONNECT_URL_DIALOG_STORYBOARD, bundle: nil)
        let urlDialogViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.ADD_URL_DIALOG) as! ConnectURLDialogViewController
        urlDialogViewController.modalPresentationStyle = .overFullScreen
        urlDialogViewController.TYPE = type
        urlDialogViewController.urlDialogDelegate = self
        self.navigationController?.present(urlDialogViewController, animated: false, completion: nil)
    }
    
    func openUnlinkDialog(typeAccount:Int){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.LOGOUT_DIALOG_BOARD, bundle: nil)
        let unlinkDialogViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.LOGOUT_DIALOG) as! LogoutDialogViewController
        unlinkDialogViewController.modalPresentationStyle = .overFullScreen
        unlinkDialogViewController.TYPE = AppConstants.TYPE_UNLINK
        unlinkDialogViewController.TYPE_ACCOUNT = typeAccount
        unlinkDialogViewController.accountUnlinkDelegate = self
        self.navigationController?.present(unlinkDialogViewController, animated: false, completion: nil)
    }
    
    @IBAction func onSaveButtonClick(_ sender: UIButton) {
        let blogLink = blogWebLinkTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) ?? ""
        
        let fbLink = self.user?.facebookLink ?? ""
        
        let tikTokLink = self.user?.tikTokLink ?? ""
        let instaLink = self.user?.instagramLink ?? ""
        let youtubelink = self.user?.youtubeLink ?? ""
        let pinterestLink =  self.user?.pinterestLink ?? ""

      updateProfileInfo(blogLink: blogLink, facebookLink: fbLink, tikTokLink: tikTokLink, instagramLink: instaLink, youtubeLink: youtubelink, pinterestLink: pinterestLink)
    }
    
    func updateProfileInfo(blogLink: String,facebookLink: String,tikTokLink:String,instagramLink: String, youtubeLink: String,pinterestLink: String) {
        buttonLoader.startAnimating()
        saveButton.isEnabled = false
        
        if blogLink.hasPrefix(AppConstants.HTTP) || blogLink.hasPrefix(AppConstants.HTTPS){
            user?.blogLink = blogLink
        } else {
            if blogLink.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                user?.blogLink = ""
            } else {
                user?.blogLink = AppConstants.HTTP + blogLink
            }
        }
        
        
        if facebookLink == "" {
            user?.facebookLink = ""
        } else {
            user?.facebookLink = facebookLink
        }
        
        if tikTokLink.hasPrefix(AppConstants.HTTP) || tikTokLink.hasPrefix(AppConstants.HTTPS){
            user?.tikTokLink = tikTokLink
        }else{
            if tikTokLink.isEmpty {
                user?.tikTokLink = ""
            } else {
                user?.tikTokLink = AppConstants.HTTP + tikTokLink
            }
        }
        
        if instagramLink == "" {
            user?.instagramLink = ""
        } else {
            user?.instagramLink = instagramLink
        }
        
        if youtubeLink == ""{
            user?.youtubeLink = ""
        }else{
            user?.youtubeLink = youtubeLink
        }
        
        if pinterestLink.hasPrefix(AppConstants.HTTP) || pinterestLink.hasPrefix(AppConstants.HTTPS){
            user?.pinterestLink = pinterestLink
        } else {
            if pinterestLink.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty {
                user?.pinterestLink = ""
            } else {
                user?.pinterestLink = AppConstants.HTTP + pinterestLink
            }
        }
        let title = user?.title ?? ""
        
        user?.shortTitle = title
        
        profileViewModel.updateProfileInfo(accessToken: getAccessToken(), data: user!,{(response) in
            self.buttonLoader.stopAnimating()
            self.saveButton.isEnabled = true
            if response.response?.statusCode == 200 {
                self.navigationController?.popViewController(animated: false)
            } else {
                let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
                if data.isError {
                    let error = Utils.getErrorMessage(errors: data.errors)
                    self.showMessageDialog(message: error)
                }
            }
        } , { (error) in
            self.buttonLoader.stopAnimating()
            self.saveButton.isEnabled = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            } 
        })
    }
    
}

extension SocialLinksViewController: IndicatorInfoProvider {
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return IndicatorInfo(title: AppConstants.Strings.SOCIAL)
    }
}
