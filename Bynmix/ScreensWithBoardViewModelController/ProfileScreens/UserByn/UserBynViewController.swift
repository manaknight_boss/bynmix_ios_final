import UIKit
import Alamofire

class UserBynViewController: BaseViewController,LoadMoreProtocol, OnDisableClickProtocol,OnDeleteClickProtocol,UserFilterApplyProtocol,UserFilterClearAllProtocol {
    
    func userFilterApply(selectedFilter: [SelectedFilter], heightModal: HeightModel) {
        self.setFilterData(selectedFilterList: selectedFilter)
        self.navigationController?.popViewController(animated: false)
        self.view.makeToastActivity(.center)
    }
    
    func userFilterClearAll() {
        self.view.makeToastActivity(.center)
        self.currentPage = 1
        UserBynViewController.userBynFilterData = FilterData()
        getBynListing()
        self.navigationController?.popViewController(animated: false)
    }
    
    func onDeleteClick(id: Int) {
        deleteListing(id: id)
    }
    
    func onDisableClick(feed: Feed) {
        if feed.isActive == true{
            enableListing(id: feed.id!)
        }else{
            disableListing(id:feed.id!)
        }
        
    }
    
    func onLoadMore() {
        if !isApiCalled {
            isApiCalled = true
            getBynListing()
        }
    }
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var noDataShadowView: UIView!
    @IBOutlet var noDataView: UIView!
    @IBOutlet var noDataImageView: UIImageView!
    @IBOutlet var noDataLabel: UILabel!
    @IBOutlet var loadingDialogView: UIView!
    @IBOutlet var loadingDialogLabel: UILabel!
    
    let userBynHandler = UserBynHandler()
    var refreshControl:UIRefreshControl?
    var currentPage = 1
    var isApiCalled = false
    var profileViewModel = ProfileViewModel()
    var feed:Feed?
    var filterBarButton: UIBarButtonItem?
    var listNeedsRefresh = false
    
    static var userBynFilterData:FilterData = FilterData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = AppConstants.MY_BYN_TITLE
        collectionView.isHidden = true
        collectionView.contentInset = UIEdgeInsets(top: 5, left: 5, bottom: 10, right: 3)
        collectionView.delegate = userBynHandler
        collectionView.dataSource = userBynHandler
        getItemsClick()
        addRefreshControl()
        userBynHandler.loadMoreDelegate = self
        let filterBarButton = UIBarButtonItem(image: UIImage(named: AppConstants.Images.FILTER_ICON), style: .plain, target: self, action: #selector(onFilterBarButtonClick))
        self.navigationItem.rightBarButtonItem  = filterBarButton
        getBynListing()
    }
    
    @objc func onFilterBarButtonClick(){
        filterListing()
    }
    
    func filterListing(){
        let storyboard = UIStoryboard(name: AppConstants.Storyboard.LISTING_FILTER_STORYBOARD, bundle: nil)
        let filterListingViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.LISTING_FILTER_SCREEN) as! FilterListingsViewController
        filterListingViewController.filterApplyDelegate = self
        filterListingViewController.filterClearAllDelegate = self
        self.navigationController?.pushViewController(filterListingViewController, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        noDataShadowView.layer.cornerRadius = 2
        noDataShadowView.layer.shadowColor = UIColor.black.cgColor
        noDataShadowView.alpha = CGFloat(0.26)
        noDataShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        noDataShadowView.layer.shadowOpacity = 1
        noDataShadowView.layer.zPosition = -1
        
        if listNeedsRefresh{
            self.view.makeToastActivity(.center)
            currentPage = 1
            getBynListing()
            listNeedsRefresh = false
        }
    }
    
    func getItemsClick(){
        userBynHandler.onEnableDisableButtonClick = { feed in
            
            if feed.isActive == true{
                let storyboard = UIStoryboard(name: AppConstants.Storyboard.ENABLE_DISABLE_DIALOG_BOARD, bundle: nil)
                let enableDisableDialogViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.ENABLE_DISABLE_LISTING_DIALOG) as! EnableDisableDialogViewController
                enableDisableDialogViewController.feed = feed
                enableDisableDialogViewController.type = AppConstants.TYPE_DISABLE
                enableDisableDialogViewController.onDisableClickDelegate = self
                self.navigationController?.present(enableDisableDialogViewController, animated: false,completion: nil)
            }else{
                self.enableListing(id:feed.id!)
            }
            
        }
        userBynHandler.onDeleteButtonClick = { feed in
            let storyboard = UIStoryboard(name: AppConstants.Storyboard.ENABLE_DISABLE_DIALOG_BOARD, bundle: nil)
            let deleteDialogViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.ENABLE_DISABLE_LISTING_DIALOG) as! EnableDisableDialogViewController
            deleteDialogViewController.feed = feed
            deleteDialogViewController.type = AppConstants.TYPE_DELETE
            deleteDialogViewController.onDeleteClickDelegate = self
            deleteDialogViewController.onDisableClickDelegate = self
            self.navigationController?.present(deleteDialogViewController, animated: false,completion:  nil)
        }
        
        userBynHandler.onItemImageViewClick = { feed in
            if feed.isActive == false {
                let storyboard = UIStoryboard(name: AppConstants.Storyboard.ENABLE_DISABLE_DIALOG_BOARD, bundle: nil)
                let reEnableListingViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.ENABLE_DISABLE_LISTING_DIALOG) as! EnableDisableDialogViewController
                reEnableListingViewController.feed = feed
                reEnableListingViewController.type = AppConstants.TYPE_RE_ENABLE
                reEnableListingViewController.onDisableClickDelegate = self
                
                self.navigationController?.present(reEnableListingViewController, animated: false,completion:  nil)
            }else{
                
                let storyboard = UIStoryboard(name: AppConstants.Storyboard.LISTING_DETAIL_BOARD, bundle: nil)
                let listingDetailViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.Listing_Detail_Screen) as! ListDetailViewController
                
                listingDetailViewController.itemId = feed.id
                self.navigationController?.pushViewController(listingDetailViewController, animated: true)
            }
        }
        
        userBynHandler.onEditButtonClick = { feed in
            //call api and pass data
            self.getListingDetailEdit(id:feed.id!)
        }
        
    }
    
    func enableListing(id:Int) {
        self.loadingDialogLabel.text = AppConstants.Strings.ENABLING_LISTING_TEXT
        self.loadingDialogView.isHidden = false
        profileViewModel.enableDisableByn(accessToken: getAccessToken(), id: id, page: currentPage,{(response) in
            self.loadingDialogView.isHidden = true
            if response.response?.statusCode == 200 {
                self.userBynHandler.updateStatus(id: id)
                self.collectionView.reloadData()
            }
        } , { (error) in
            self.loadingDialogView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func disableListing(id:Int) {
        self.loadingDialogLabel.text = AppConstants.Strings.DISABLING_LISTING_TEXT
        self.loadingDialogView.isHidden = false
        profileViewModel.enableDisableByn(accessToken: getAccessToken(), id: id, page: currentPage,{(response) in
            self.loadingDialogView.isHidden = true
            if response.response?.statusCode == 200 {
                self.userBynHandler.updateStatus(id: id)
                self.collectionView.reloadData()
            }
        } , { (error) in
            self.loadingDialogView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    
    func deleteListing(id:Int) {
        self.loadingDialogLabel.text = AppConstants.Strings.DELETE_LISTING_TEXT
        self.loadingDialogView.isHidden = false
        profileViewModel.deleteByn(accessToken: getAccessToken(), id: id,{(response) in
            self.loadingDialogView.isHidden = true
            if response.response?.statusCode == 200 {
                self.userBynHandler.deleteStatus(id: id)
                self.collectionView.reloadData()
            }
        } , { (error) in
            self.loadingDialogView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getBynListing(filterData:FilterData? = nil) {
        var filters: FilterData
        if filterData == nil {
            filters = FilterData()
        } else {
            filters = filterData!
        }
        if self.userBynHandler.feed.count > 0 && filterData != nil{
            self.userBynHandler.feed.removeAll()
            self.collectionView.reloadData()
        }
        profileViewModel.getUserByn(accessToken: getAccessToken(), page: currentPage, data: filters,{(response) in
            self.view.hideToastActivity()
            self.handleResponse(response: response,filterData: filterData)
        } , { (error) in
            self.refreshControl?.endRefreshing()
            self.view.hideToastActivity()
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            } 
        })
    }
    
    func handleResponse(response: AFDataResponse<Any>,filterData:FilterData? = nil) {
        self.refreshControl?.endRefreshing()
        self.isApiCalled = false
        if response.response?.statusCode == 200 {
            self.screenActivityIndicator.stopAnimating()
            let data: ApiResponse<Feed> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            } else {
                let feed = data.data!
                self.feed = feed
                
                if feed.items!.count > 0{
                    
                    if let feeds = feed.items {
                        
                        if self.currentPage == 1 {
                            self.userBynHandler.feed = feeds
                        } else {
                            self.userBynHandler.feed.append(contentsOf: feeds)
                        }
                        if feed.totalPages == self.currentPage {
                            self.isApiCalled = true
                            self.userBynHandler.isLoadMoreRequired = false
                            let layout = self.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
                            layout.footerReferenceSize = CGSize(width: 0, height: 0)
                        }
                        
                        self.collectionView.reloadData()
                        self.screenActivityIndicator.stopAnimating()
                        self.collectionView.isHidden = false
                        self.noDataShadowView.isHidden = true
                        self.noDataView.isHidden = true
                        
                    }
                }else{
                    self.screenActivityIndicator.stopAnimating()
                    self.noDataShadowView.isHidden = false
                    self.noDataView.isHidden = false
                    self.collectionView.isHidden = true
                    self.noDataLabel.text = AppConstants.Strings.NO_USER_BYN
                    self.noDataImageView.image = UIImage(named: AppConstants.Images.LOGO_BLACK)
                }
                
            }
            self.currentPage = self.currentPage + 1
        }else if response.response?.statusCode == 404 {
            self.noDataShadowView.isHidden = false
            self.noDataView.isHidden = false
            self.collectionView.isHidden = true
            self.noDataLabel.text = AppConstants.Strings.NO_USER_BYN
            self.noDataImageView.image = UIImage(named: AppConstants.Images.LOGO_BLACK)
            self.screenActivityIndicator.stopAnimating()
            
        } else {
            self.noDataView.isHidden = false
            self.noDataShadowView.isHidden = false
            self.collectionView.isHidden = true
            self.noDataLabel.text = AppConstants.Strings.SOMETHING_WRONG_HERE
            self.noDataImageView.image = UIImage(named: AppConstants.Images.LOGO_SAD_CLOUD)
            let data: ApiResponse<EmptyResponse> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            }
            self.screenActivityIndicator.stopAnimating()
        }
        if filterData?.brandIds?.count ?? 0 > 0 || filterData?.categoryIds?.count ?? 0 > 0 || filterData?.colorIds?.count ?? 0 > 0 || filterData?.conditionIds?.count ?? 0 > 0 || filterData?.sizeIds?.count ?? 0 > 0 || filterData?.customMinPrice != nil || filterData?.customMaxPrice != nil{
            self.trackSearchCalculate(userFilterData: filterData)
        }
    }
    
    func addRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        collectionView.addSubview(refreshControl!)
    }
    @objc func refreshList(){
        currentPage = 1
        isApiCalled = false
        self.userBynHandler.isLoadMoreRequired = true
        
        let userBynFilterData = UserBynViewController.userBynFilterData
        if (userBynFilterData.brandIds?.count ?? 0) > 0 || (userBynFilterData.categoryIds?.count ?? 0) > 0 || (userBynFilterData.colorIds?.count ?? 0 > 0) || (userBynFilterData.conditionIds?.count ?? 0 > 0) || (userBynFilterData.sizeIds?.count ?? 0 > 0) || (userBynFilterData.customMinPrice ?? 0 > 0) || (userBynFilterData.customMaxPrice ?? 0 > 0){
            getBynListing(filterData: userBynFilterData)
        }else{
            getBynListing()
        }
    }
    
    func setFilterData(selectedFilterList :[SelectedFilter]) {
        var filterData = FilterData()
        var conditionIds:[Int] = []
        var categoryTypeIds:[Int] = []
        var sizeIds :[Int] = []
        var colorIds:[Int] = []
        var categoriesIds:[Int] = []
        var brandsIds:[Int] = []
        
        var conditionNames:[String] = []
        var sizeNames:[String] = []
        var colorNames:[String] = []
        var categoryNames:[String] = []
        var priceNames:[String] = []
        var brandNames:[String] = []
        
        for i in 0..<selectedFilterList.count {
            if (selectedFilterList[i].type == AppConstants.CONDITION_FILTER) {
                if (selectedFilterList[i].id != 0) {
                    conditionIds.append(selectedFilterList[i].id!)
                    filterData.conditionIds = conditionIds
                    
                    conditionNames.append(selectedFilterList[i].name!)
                    trackSearchDictionary.updateValue(conditionNames, forKey: "condition")
                }
            } else if (selectedFilterList[i].type == AppConstants.SIZE_FILTER) {
                if (selectedFilterList[i].id != 0) {
                    sizeIds.append(selectedFilterList[i].subType!)
                    filterData.sizeIds = sizeIds
                    
                    sizeNames.append(selectedFilterList[i].name!)
                    trackSearchDictionary.updateValue(sizeNames, forKey: "size")
                }
            } else if (selectedFilterList[i].type == AppConstants.COLOR_FILTER) {
                colorIds.append(selectedFilterList[i].id!)
                filterData.colorIds = colorIds
                
                colorNames.append(selectedFilterList[i].name!)
                trackSearchDictionary.updateValue(colorNames, forKey: "color")
            } else if (selectedFilterList[i].type == AppConstants.CATEGORY_FILTER) {
                if (selectedFilterList[i].subType != 0) {
                    categoriesIds.append(selectedFilterList[i].subType!)
                    categoryNames.append(selectedFilterList[i].name!)
                    trackSearchDictionary.updateValue(categoryNames, forKey: "category")
                }else{
                    categoryTypeIds.append(selectedFilterList[i].id!)
                }
                filterData.categoryIds = categoriesIds
                filterData.categoryTypeIds = categoryTypeIds
            } else if (selectedFilterList[i].type == AppConstants.BRAND_FILTER) {
                if (selectedFilterList[i].id != 0) {
                    brandsIds.append(selectedFilterList[i].id!)
                    filterData.brandIds = brandsIds
                    
                    brandNames.append(selectedFilterList[i].name!)
                    trackSearchDictionary.updateValue(brandNames, forKey: "brand")
                }
            } else if (selectedFilterList[i].type == AppConstants.PRICE_FILTER) {
                if selectedFilterList[i].id == 1 {
                    filterData.customMinPrice = 0
                    filterData.customMaxPrice = 20
                } else if selectedFilterList[i].id == 2 {
                    filterData.customMinPrice = 21
                    filterData.customMaxPrice = 50
                } else if selectedFilterList[i].id == 3 {
                    filterData.customMinPrice = 51
                    filterData.customMaxPrice = 100
                } else if selectedFilterList[i].id == 4 {
                    filterData.customMinPrice = 100
                    filterData.customMaxPrice = 200
                } else if selectedFilterList[i].id == 5{
                    filterData.customMinPrice = 200
                    filterData.customMaxPrice = 500
                }
                priceNames.append(selectedFilterList[i].name!)
                trackSearchDictionary.updateValue(priceNames, forKey: "price")
            }
        }
        
        UserBynViewController.userBynFilterData = filterData
        BaseViewController.mSelectedUserBynFilterList = selectedFilterList
        
        self.currentPage = 1
        getBynListing(filterData: filterData)
        
    }
    
    func trackSearchCalculate(userFilterData:FilterData? = nil){
        var trackSearchOfUser = TrackSearchOfUser()
        var jsonString = ""
        
        if trackSearchDictionary.count > 0{
            jsonString = convertIntoJSONString(arrayObject: trackSearchDictionary)!
            print("jsonString - \(jsonString)")
        }
        
        trackSearchOfUser.searchBarLocation = AppConstants.TYPE_SEARCH_BAR_LOCATION_UNKNOWN
        
        if userFilterData != nil{
            if jsonString.count > 0{
                trackSearchOfUser.searchFilters = jsonString
            }
            self.trackSearch(trackSearchOfUser: trackSearchOfUser)
        }
    }
    
}

