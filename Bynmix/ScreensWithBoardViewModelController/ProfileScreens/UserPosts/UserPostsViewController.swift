import UIKit
import Alamofire

class UserPostsViewController: BaseViewController,LoadMoreProtocol,OnDisableClickProtocol,OnDeleteClickProtocol {
    func onDisableClick(feed: Feed) {
        if feed.isActive == true{
            enablePosts(id: feed.id!)
        }else{
            disablePosts(id: feed.id!)
        }
    }
    
    func onDeleteClick(id: Int) {
        deletePosts(id: id)
    }
    
    func onLoadMore() {
        if !isApiCalled {
            isApiCalled = true
            getUserPosts()
        }
    }
    
    @IBOutlet var screenActivityIndicator: UIActivityIndicatorView!
    @IBOutlet var tableViewLayout: UITableView!
    @IBOutlet var noDataShadowView: UIView!
    @IBOutlet var noDataView: UIView!
    @IBOutlet var noDataImageView: UIImageView!
    @IBOutlet var noDataLabel: UILabel!
    @IBOutlet var loadingDialogView: UIView!
    @IBOutlet var loadingDialogLabel: UILabel!
    
    let userPostHandler = UserPostsHandler()
    var feed:Feed? = nil
    var refreshControl:UIRefreshControl?
    var currentPage = 1
    var isApiCalled = false
    var viewControllerType:Int? = nil
    var feedViewModel = FeedViewModel()
    var profileViewModel = ProfileViewModel()
    var otherUserId:Int?
    var USERNAME:String?
    var VIEWCONTROLLER_TYPE:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewLayout.isHidden = true
        tableViewLayout.delegate = userPostHandler
        tableViewLayout.dataSource = userPostHandler
        getItemsClick()
        getUserPosts()
        self.tableViewLayout.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 10, right: 0)
        addRefreshControl()
        userPostHandler.loadMoreDelegate = self
        navigationItem.title = AppConstants.MY_POSTS_TITLE
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        noDataShadowView.layer.cornerRadius = 2
        noDataShadowView.layer.shadowColor = UIColor.black.cgColor
        noDataShadowView.alpha = CGFloat(0.26)
        noDataShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        noDataShadowView.layer.shadowOpacity = 1
        noDataShadowView.layer.zPosition = -1
    }
    
    func getItemsClick(){
        userPostHandler.onEnableDisableButtonClick = { feed in
            
            if feed.isActive == true{
                let storyboard = UIStoryboard(name: AppConstants.Storyboard.ENABLE_DISABLE_DIALOG_BOARD, bundle: nil)
                let enableDisableDialogViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.ENABLE_DISABLE_LISTING_DIALOG) as! EnableDisableDialogViewController
                enableDisableDialogViewController.feed = feed
                enableDisableDialogViewController.type = AppConstants.TYPE_DISABLE_POST
                enableDisableDialogViewController.onDisableClickDelegate = self
                self.navigationController?.present(enableDisableDialogViewController, animated: false,completion: nil)
            }else{
                self.enablePosts(id:feed.id!)
            }
        }
        userPostHandler.onDeleteButtonClick = { feed in
            let storyboard = UIStoryboard(name: AppConstants.Storyboard.ENABLE_DISABLE_DIALOG_BOARD, bundle: nil)
            let deleteDialogViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.ENABLE_DISABLE_LISTING_DIALOG) as! EnableDisableDialogViewController
            deleteDialogViewController.feed = feed
            deleteDialogViewController.type = AppConstants.TYPE_DELETE_POST
            deleteDialogViewController.onDeleteClickDelegate = self
            deleteDialogViewController.onDisableClickDelegate = self
            self.navigationController?.present(deleteDialogViewController, animated: false,completion:  nil)
        }
        
        userPostHandler.onItemImageViewClick = { feed in
            if feed.isActive == false {
                let storyboard = UIStoryboard(name: AppConstants.Storyboard.ENABLE_DISABLE_DIALOG_BOARD, bundle: nil)
                let reEnableListingViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.DialogController.ENABLE_DISABLE_LISTING_DIALOG) as! EnableDisableDialogViewController
                reEnableListingViewController.feed = feed
                reEnableListingViewController.type = AppConstants.TYPE_RE_ENABLE_POST
                reEnableListingViewController.onDisableClickDelegate = self
                self.navigationController?.present(reEnableListingViewController, animated: false,completion:  nil)
            }else{
                
                let storyboard = UIStoryboard(name: AppConstants.Storyboard.POST_DETAIL_BOARD, bundle: nil)
                let listingDetailViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.POST_DETAIL_SCREEN) as! PostDetailViewController
                
                listingDetailViewController.feed = feed
                self.navigationController?.pushViewController(listingDetailViewController, animated: true)
            }
        }
        
        userPostHandler.onEditButtonClick = {feed in
            self.view.makeToastActivity(.center)
            let storyboard = UIStoryboard(name: AppConstants.Storyboard.EDIT_POST_BOARD, bundle: nil)
            let listingDetailViewController = storyboard.instantiateViewController(withIdentifier: AppConstants.Controllers.EDIT_POST_SCREEN) as! EditPostViewController
            
            listingDetailViewController.feed = feed
            self.view.hideToastActivity()
            self.navigationController?.pushViewController(listingDetailViewController, animated: true)
        }
        
    }
    
    func enablePosts(id:Int) {
        self.loadingDialogLabel.text = AppConstants.Strings.ENABLING_POSTS_TEXT
        self.loadingDialogView.isHidden = false
        profileViewModel.enableDisablePosts(accessToken: getAccessToken(), id: id, page: currentPage,{(response) in
            self.loadingDialogView.isHidden = true
            if response.response?.statusCode == 200 {
                self.userPostHandler.updateStatus(id: id)
                self.tableViewLayout.reloadData()
            }
        } , { (error) in
            self.loadingDialogView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func disablePosts(id:Int) {
        self.loadingDialogLabel.text = AppConstants.Strings.DISABLING_POSTS_TEXT
        self.loadingDialogView.isHidden = false
        profileViewModel.enableDisablePosts(accessToken: getAccessToken(), id: id, page: currentPage,{(response) in
            self.loadingDialogView.isHidden = true
            if response.response?.statusCode == 200 {
                self.userPostHandler.updateStatus(id: id)
                self.tableViewLayout.reloadData()
            }
        } , { (error) in
            self.loadingDialogView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            } 
        })
    }
    
    func deletePosts(id:Int) {
        self.loadingDialogLabel.text = AppConstants.Strings.DELETE_POSTS_TEXT
        self.loadingDialogView.isHidden = false
        profileViewModel.deletePosts(accessToken: getAccessToken(), id: id,{(response) in
            self.loadingDialogView.isHidden = true
            if response.response?.statusCode == 200 {
                self.userPostHandler.deleteStatus(id: id)
                self.tableViewLayout.reloadData()
            }
        } , { (error) in
            self.loadingDialogView.isHidden = true
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            }
        })
    }
    
    func getUserPosts() {
        profileViewModel.getUserPost(accessToken: getAccessToken(), page: currentPage,{(response) in
            self.handleResponse(response:response)
        } , { (error) in
            self.refreshControl?.endRefreshing()
            self.screenActivityIndicator.stopAnimating()
            if error.statusCode == AppConstants.NO_INTERNET {
                self.showInternetError()
            } 
        })
    }
    
    func handleResponse(response:AFDataResponse<Any>){
        self.isApiCalled = false
        self.refreshControl?.endRefreshing()
        if response.response?.statusCode == 200 {
            self.screenActivityIndicator.stopAnimating()
            let data: ApiResponse<Feed> = Utils.convertResponseToData(data: response.data!)
            if data.isError {
                let error = Utils.getErrorMessage(errors: data.errors)
                self.showMessageDialog(message: error)
            } else {
                let feed = data.data
                self.feed = feed
                if let feeds = feed?.items {
                    
                    if self.currentPage == 1 {
                        self.userPostHandler.feed = feeds
                    } else {
                        self.userPostHandler.feed.append(contentsOf: feeds)
                    }
                    if feed?.totalPages == self.currentPage {
                        self.isApiCalled = true
                        self.userPostHandler.isLoadMoreRequired = false
                    }
                    self.tableViewLayout.reloadData()
                    self.screenActivityIndicator.stopAnimating()
                    self.tableViewLayout.isHidden = false
                }
            }
            self.currentPage = self.currentPage + 1
        }else if response.response?.statusCode == 404 {
            self.noDataShadowView.isHidden = false
            self.noDataView.isHidden = false
            self.noDataLabel.text = AppConstants.Strings.NO_USER_POST
            self.noDataImageView.image = UIImage(named: AppConstants.Images.LOGO_BLACK)
            self.screenActivityIndicator.stopAnimating()
            
        } else {
            self.noDataView.isHidden = false
            self.noDataShadowView.isHidden = false
            self.noDataLabel.text = AppConstants.Strings.SOMETHING_WRONG_HERE
            self.noDataImageView.image = UIImage(named: AppConstants.Images.LOGO_SAD_CLOUD)
            self.screenActivityIndicator.stopAnimating()
        }
    }
    
    func addRefreshControl() {
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshList), for: .valueChanged)
        tableViewLayout.addSubview(refreshControl!)
    }
    @objc func refreshList(){
        currentPage = 1
        isApiCalled = false
        self.userPostHandler.isLoadMoreRequired = true
        getUserPosts()
    }
    
}
