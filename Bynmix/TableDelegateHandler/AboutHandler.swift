import UIKit

class AboutHandler: NSObject,UITableViewDelegate,UITableViewDataSource {

    var about = [Terms]()
    let aboutcell = "about_cell"
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return about.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: aboutcell, for: indexPath)
            as! AboutTableViewCell
        let data = self.about[indexPath.row]
        
        cell.titleLabel?.text = data.title
        if let description = data.subSections {
            cell.descriptionTwoLabel.isHidden = false
            cell.descriptionThreeLabel.isHidden = true
            cell.descriptionFourLabel.isHidden = true
            cell.descriptionFiveLabel.isHidden = true
            
            let subData = description[0]
            if let innerDescription = subData.description {
                cell.descriptionLabel?.text = innerDescription
            }
            let subData2 = description[1]
            
            if let innerDescription = subData2.description {
                cell.descriptionTwoLabel?.text = innerDescription
            }
            
            if (description.count) > 2{
                cell.descriptionThreeLabel.isHidden = false
                cell.descriptionFourLabel.isHidden = false
                cell.descriptionFiveLabel.isHidden = false
                let subData3 = description[2]
                let subData4 = description[3]
                let subData5 = description[4]
                if let innerDescription = subData3.description {
                    cell.descriptionThreeLabel?.text = innerDescription
                }
                if let innerDescription = subData4.description {
                    cell.descriptionFourLabel?.text = innerDescription
                }
                if let innerDescription = subData5.description {
                    cell.descriptionFiveLabel?.text = innerDescription
                }

            }
        } else {
            if let desc = data.description {
                cell.descriptionLabel?.text = desc
            }
            cell.descriptionTwoLabel.text = ""
           
        }
        
        cell.selectionStyle = .none
        return cell
    }
    
}
