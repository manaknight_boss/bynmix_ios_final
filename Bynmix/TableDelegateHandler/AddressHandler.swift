import UIKit

class AddressHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    var shippingAddress: Address? = nil
    var returnAddress: Address? = nil
    var otherAddresses = [Address]()
    let returnCell = "returnAddressCell"
    let setAddressCell = "setAddressCell"
    var returnAddressTableViewCell : ReturnAddressTableViewCell?
    var onEditButtonClick: ((Address) -> Void)!
    var onDeleteButtonClick: ((Address) -> Void)!
    var onShippingAddNewAddressButtonClick: (() -> Void)!
    var onReturnAddNewAddressButtonClick: (() -> Void)!
    var onSelectExistingAddressButtonClick: ((Int) -> Void)!
    var allAddresses: [Address] = []
    
    func deleteStatus(id: Int) {
        var index = -1
        
        for i in 0..<self.otherAddresses.count {
            if self.otherAddresses[i].addressId == id {
                index = i
                break
            }
        }
        self.otherAddresses.remove(at: index)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else if section == 1 {
            return 1
        } else {
            return otherAddresses.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if otherAddresses.count > 0 {
            return 3
        } else {
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return AppConstants.Strings.PREFFERED_SHIPPING_ADDRESS_TILE
        } else if section == 1 {
            return AppConstants.Strings.PREFFERED_RETURN_ADDRESS_TILE
        } else {
            return AppConstants.Strings.OTHER_ADDRESS_TILE
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor.cE8E8E8
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.c414141
        header.textLabel?.font = UIFont.fontMedium(size: 18)
        header.backgroundColor = UIColor.cE8E8E8
        header.tintColor = UIColor.cE8E8E8
        header.textLabel?.text = header.textLabel!.text!.capitalized
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: returnCell, for: indexPath) as! ReturnAddressTableViewCell
        
        returnAddressTableViewCell = cell
        cell.footerView.frame = CGRect(x: 0, y: 0, width: cell.footerView.frame.width, height: 10)
        var address: Address? = nil
        cell.footerView.backgroundColor = UIColor.cE8E8E8
        if indexPath.section == 0 {
            if shippingAddress != nil {
                address = shippingAddress!
                cell.deleteButton.isHidden = true
                cell.editButton.tag = -1
                
                cell.addressesView.isHidden = false
                cell.buttonsView.isHidden = true
                cell.newAddressView.isHidden = true
                cell.nameLabel.text = address!.fullName
                cell.addressOneLabel.text = address!.addressOne
                cell.addressTwoLabel.text = address!.addressTwo
                cell.addressThreeLabel.text = (address!.city! + " " + address!.stateAbbreviation! + ", " + address!.zipCode!)
                cell.editButton.addTarget(self, action: #selector(onEditButtonTap), for: .touchUpInside)
                cell.deleteButton.isHidden = true
            } else {
                cell.selectExistingAddress.tag = AppConstants.TYPE_SHIPPING_ADD_ADDRESS
                cell.selectExistingAddress.addTarget(self, action: #selector(onExistingAddressButtonTap), for: .touchUpInside)
                cell.addNewAddressButton.tag = AppConstants.TYPE_SHIPPING_ADD_ADDRESS
                cell.addNewAddressButton.addTarget(self, action: #selector(onNewAddressButtonTap), for: .touchUpInside)
                cell.addNewFullButton.tag = AppConstants.TYPE_SHIPPING_ADD_ADDRESS
                cell.addNewFullButton.addTarget(self, action: #selector(onNewAddressFullButtonTap), for: .touchUpInside)
                
                if allAddresses.count > 0{
                    cell.addressesView.isHidden = true
                    cell.buttonsView.isHidden = false
                    cell.newAddressView.isHidden = true
                }else{
                    cell.addressesView.isHidden = true
                    cell.buttonsView.isHidden = true
                    cell.newAddressView.isHidden = false
                    
                }
                
            }
        } else if indexPath.section == 1 {
            if returnAddress != nil {
                address = returnAddress!
                cell.deleteButton.isHidden = true
                cell.editButton.tag = -2
                cell.addressesView.isHidden = false
                cell.buttonsView.isHidden = true
                cell.nameLabel.text = address!.fullName
                cell.addressOneLabel.text = address!.addressOne
                cell.addressTwoLabel.text = address!.addressTwo
                cell.addressThreeLabel.text = (address!.city! + " " + address!.stateAbbreviation! + ", " + address!.zipCode!)
                cell.editButton.addTarget(self, action: #selector(onEditButtonTap), for: .touchUpInside)
                cell.deleteButton.isHidden = true
                cell.newAddressView.isHidden = true
            } else {
                cell.selectExistingAddress.tag = AppConstants.TYPE_RETURN_ADD_ADDRESS
                cell.selectExistingAddress.addTarget(self, action: #selector(onExistingAddressButtonTap), for: .touchUpInside)
                cell.addNewAddressButton.tag = AppConstants.TYPE_RETURN_ADD_ADDRESS
                cell.addNewAddressButton.addTarget(self, action: #selector(onNewAddressButtonTap), for: .touchUpInside)
                cell.addNewFullButton.tag = AppConstants.TYPE_RETURN_ADD_ADDRESS
                cell.addNewFullButton.addTarget(self, action: #selector(onNewAddressFullButtonTap), for: .touchUpInside)
                if allAddresses.count > 0{
                    cell.addressesView.isHidden = true
                    cell.buttonsView.isHidden = false
                    cell.newAddressView.isHidden = true
                }else{
                    cell.addressesView.isHidden = true
                    cell.buttonsView.isHidden = true
                    cell.newAddressView.isHidden = false
                }
                
            }
        } else {
            address = self.otherAddresses[indexPath.row]
            cell.deleteButton.isHidden = false
            cell.editButton.tag = indexPath.row
            
            cell.nameLabel.text = address!.fullName
            cell.addressOneLabel.text = address!.addressOne
            cell.addressTwoLabel.text = address!.addressTwo
            cell.addressThreeLabel.text = (address!.city! + " " + address!.stateAbbreviation! + ", " + address!.zipCode!)
            cell.editButton.addTarget(self, action: #selector(onEditButtonTap), for: .touchUpInside)
            cell.deleteButton.tag = indexPath.row
            cell.deleteButton.addTarget(self, action: #selector(onDeleteButtonTap), for: .touchUpInside)
            cell.deleteButton.isHidden = false
            cell.addressesView.isHidden = false
            cell.buttonsView.isHidden = true
            cell.footerView.isHidden = true
        }
        cell.addNewAddressButton.titleEdgeInsets = UIEdgeInsets(top: 8,left: 8,bottom: 8,right: 8)
        cell.selectExistingAddress.titleEdgeInsets = UIEdgeInsets(top: 8,left: 8,bottom: 8,right: 8)
        cell.selectExistingAddress.titleLabel?.textAlignment = NSTextAlignment.center
        
        
        return cell
    }
    
    @objc func onEditButtonTap(sender:UIButton){
        let index = sender.tag
        if index == -1 {
            onEditButtonClick(shippingAddress!)
        } else if index == -2 {
            onEditButtonClick(returnAddress!)
        } else {
            onEditButtonClick(otherAddresses[index])
        }
    }
    
    @objc func onDeleteButtonTap(sender:UIButton){
        let index = sender.tag
        onDeleteButtonClick(otherAddresses[index])
    }
    
    @objc func onNewAddressButtonTap(sender:UIButton){
        let index = sender.tag
        if index == AppConstants.TYPE_SHIPPING_ADD_ADDRESS{
            onShippingAddNewAddressButtonClick()
        }else{
            onReturnAddNewAddressButtonClick()
        }
    }
    
    @objc func onNewAddressFullButtonTap(sender:UIButton){
        let index = sender.tag
        if index == AppConstants.TYPE_SHIPPING_ADD_ADDRESS{
            onShippingAddNewAddressButtonClick()
        }else{
            onReturnAddNewAddressButtonClick()
        }
    }
    
    @objc func onExistingAddressButtonTap(sender:UIButton){
        onSelectExistingAddressButtonClick(sender.tag)
    }
    
}
