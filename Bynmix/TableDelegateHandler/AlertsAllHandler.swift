import UIKit
import ActiveLabel

class AlertsAllHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    var alerts = [Setting]()
    let alertsCell = "alertsAllCell"
    var isLoadMoreRequired = true
    var loadMoreDelegate: LoadMoreProtocol?
    var tableView: UITableView?
    var onUserImageClickHandler: ((Setting) -> Void)!
    var onUserImagePromotionClickHandler: (() -> Void)!
    var onFormattedTextClicked:((Setting) -> Void)!
    var count = 1
    var heightAtIndexPath = NSMutableDictionary()
    var onNotificationClick: ((Setting) -> Void)!
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let height = NSNumber(value: Float(cell.frame.size.height))
        heightAtIndexPath.setObject(height, forKey: indexPath as NSCopying)
        
        if isLoadMoreRequired {
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                self.tableView?.tableFooterView = spinner
                self.tableView?.tableFooterView?.isHidden = false
            }
        } else {
            self.tableView?.tableFooterView = nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return alerts.count
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView?.tableFooterView != nil {
            let height = scrollView.frame.size.height
            let contentYoffset = scrollView.contentOffset.y
            let distanceFromBottom = scrollView.contentSize.height - contentYoffset
            if distanceFromBottom < height {
                loadMoreDelegate?.onLoadMore()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let alert = self.alerts[indexPath.row] as Setting
        let image = UIImage(named: AppConstants.Images.DEFAULT_OFFER_PLACEHOLDER)
        
        let cell = tableView.dequeueReusableCell(withIdentifier: alertsCell, for: indexPath) as! AlertsAllTableViewCell
        
        cell.timeLabel.text = alert.notificationDateFormatted
        cell.timeLabel.textColor = UIColor.c434343
        cell.userImageView.layer.borderWidth = 2.0
        cell.userImageView.layer.masksToBounds = false
        cell.userImageView.layer.borderColor = UIColor.white.cgColor
        cell.userImageView.clipsToBounds = true
        cell.userImageView.layer.cornerRadius = 2
        
        cell.userShadowView.layer.cornerRadius = 2
        cell.userShadowView.layer.shadowColor = UIColor.black.cgColor
        cell.userShadowView.alpha = CGFloat(0.50)
        cell.userShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        cell.userShadowView.layer.shadowOpacity = 1
        
        let htmlCSSString = "<style>" +
            "html *" +
            "{" +
            "font-size: \(11)pt !important;" +
            "font-family: Raleway-Regular !important;" +
        "}</style> \(self)"
        
        let text = htmlCSSString + alert.formattedText!
        cell.descriptionLabel.attributedText = text.html2Attributed
        
        if alert.notificationTypeId == AppConstants.PROMOTION{
            
            cell.descriptionLabel.textColor  = UIColor.white
            cell.timeLabel.textColor = UIColor.white
            
            if alert.promotionTypeId == AppConstants.TEN_PERCENT_OFF || alert.promotionTypeId == AppConstants.FIVE_PERCENT_OFF{
                cell.mainView.backgroundColor  = UIColor.c84A666
                cell.userShadowView.backgroundColor = UIColor.c6B8652
                cell.userImageView.backgroundColor = UIColor.c6B8652
                
            }else if alert.promotionTypeId == AppConstants.FREE_SHIPPING || alert.promotionTypeId == AppConstants.THREE_DOLLAR_SHIPPING{
                cell.mainView.backgroundColor  = UIColor.cE26E00
                cell.userShadowView.backgroundColor = UIColor.cB05600
                cell.userImageView.backgroundColor = UIColor.cB05600
                
            }else{
                cell.mainView.backgroundColor  = UIColor.c0089B3
                cell.userShadowView.backgroundColor = UIColor.c006483
                cell.userImageView.backgroundColor = UIColor.c006483
            }
            
            if let imageUrl = alert.iconUrl{
                cell.userImageView.kf.setImage(with: URL(string: imageUrl), placeholder: image)
            }else{
                cell.userImageView.image = image
            }
            
            cell.statusImageView.isHidden = true
        } else {
            cell.mainView.backgroundColor  = UIColor.white
            cell.userShadowView.backgroundColor = UIColor.white
            
            var imageUrl = ""
            if (alert.notificationTypeId == AppConstants.POST_LIKED || alert.notificationTypeId == AppConstants.COMMENT_RECEIVED || alert.notificationTypeId == AppConstants.USER_FOLLOWING) {
                imageUrl = alert.userImage ?? ""
            } else {
                imageUrl = alert.listingImage ?? ""
            }
            cell.userImageView.kf.setImage(with: URL(string: imageUrl),placeholder: image)
            cell.statusImageView.isHidden = false
            cell.statusImageView.kf.setImage(with: URL(string: alert.iconUrl!)!)
        }
        
        if alert.isUnread ?? false{
            cell.addBorderView.backgroundColor = UIColor.cBB189C
        }else{
            cell.addBorderView.backgroundColor = UIColor.white
        }
        
//        if alert.promotionTypeId == nil{
//            cell.userImageView.tag = indexPath.row
//            let usertap = UITapGestureRecognizer(target: self, action: #selector(userImageClicked))
//            cell.userImageView.addGestureRecognizer(usertap)
//
//            cell.descriptionLabel.tag = indexPath.row
//            let formattedTextTap = UITapGestureRecognizer(target: self, action: #selector(formattedTextClickAction))
//            cell.descriptionLabel.addGestureRecognizer(formattedTextTap)
//        }else{
//            cell.userImageView.tag = indexPath.row
//            let usertap = UITapGestureRecognizer(target: self, action: #selector(promotionClicked))
//            cell.userImageView.addGestureRecognizer(usertap)
//
//            cell.descriptionLabel.tag = indexPath.row
//            let formattedTextTap = UITapGestureRecognizer(target: self, action: #selector(promotionClicked))
//            cell.descriptionLabel.addGestureRecognizer(formattedTextTap)
//        }
//
//        cell.userImageView.isUserInteractionEnabled = true
        
        cell.tag = indexPath.row
        let tap = UITapGestureRecognizer(target: self, action: #selector(notificationClicked))
        cell.addGestureRecognizer(tap)
        
        return cell
    }
    
    @objc func notificationClicked(sender: UITapGestureRecognizer) {
        let index = (sender.view?.tag)!
        onNotificationClick(alerts[index])
    }
    
//    @objc func promotionClicked(sender: UITapGestureRecognizer) {
//        onUserImagePromotionClickHandler()
//    }
//    
//    @objc func userImageClicked(sender: UITapGestureRecognizer) {
//        let index = (sender.view?.tag)!
//        onUserImageClickHandler(alerts[index])
//    }
//    
//    @objc func formattedTextClickAction(sender: UITapGestureRecognizer) {
//        let index = (sender.view?.tag)!
//        onFormattedTextClicked(alerts[index])
//    }
    
}
