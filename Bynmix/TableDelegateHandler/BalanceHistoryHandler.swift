import UIKit

class BalanceHistoryHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    var balanceHistory = [BalanceHistory]()
    var balanceHistoryCell = "withdrawlHistoryCell"
    var loadMoreDelegate: LoadMoreProtocol?
    var isLoadMoreRequired = true
    var tableView: UITableView?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return balanceHistory.count
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView?.tableFooterView != nil {
            let height = scrollView.frame.size.height
            let contentYoffset = scrollView.contentOffset.y
            let distanceFromBottom = scrollView.contentSize.height - contentYoffset
            if distanceFromBottom < height {
                loadMoreDelegate?.onLoadMore()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: balanceHistoryCell, for: indexPath) as! BalanceHistoryTableViewCell
        
        let balance = balanceHistory[indexPath.row]
        
        cell.confirmationCodeLabel.text =  balance.confirmationCode
        
        let htmlCSSString = "<style>" +
            "html *" +
            "{" +
            "font-size: \(10)pt !important;" +
            "font-family: Raleway-Regular !important;" +
        "}</style> \(self)"
        
        let text = htmlCSSString + balance.formattedDisplayText!
        cell.formatedDisplayLabel.attributedText = text.html2Attributed
        if balance.typeId == AppConstants.TYPE_STATUS_PAID{
            cell.statusImageView.image = UIImage(named: AppConstants.Images.STATUS_PAID_ICON)
            cell.typeLabel.text = AppConstants.STATUS_PAID
        }else if balance.typeId == AppConstants.TYPE_STATUS_FAILED{
            cell.statusImageView.image = UIImage(named: AppConstants.Images.STATUS_FAILED_ICON)
            cell.typeLabel.text = AppConstants.STATUS_FAILED
        }else if balance.typeId == AppConstants.TYPE_STATUS_CREATED{
            cell.statusImageView.image = UIImage(named: AppConstants.Images.STATUS_CREATED_ICON)
            cell.typeLabel.text = AppConstants.STATUS_CREATED
        }else if balance.typeId == AppConstants.TYPE_STATUS_CANCELED{
            cell.statusImageView.image = UIImage(named: AppConstants.Images.STATUS_CANCELLED_ICON)
            cell.typeLabel.text = AppConstants.STATUS_CANCELED
        }else{
            cell.statusImageView.image = UIImage(named: AppConstants.Images.STATUS_UPDATED_ICON)
            cell.typeLabel.text = AppConstants.STATUS_UPDATED
        }
        if balance.failureMessage == "" || balance.failureMessage == nil{
            cell.failedCanceledView.isHidden = true
        }else{
            cell.failedCanceledView.isHidden = false
        }
        
        let amountText:String = AppConstants.DOLLAR + String(format: "%.2f", Double(balance.amount!)) + " " + AppConstants.WITHDRAWAL
        let textDollar:String = AppConstants.DOLLAR + String(format: "%.2f", Double(balance.amount!))
        let range = (amountText as NSString).range(of: textDollar)
        let attributedString = NSMutableAttributedString(string: amountText)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontBold(size: 16), range: range)
        cell.amountLabel.attributedText = attributedString
        cell.statusImageView.contentMode = .scaleAspectFit
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if isLoadMoreRequired {
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                self.tableView?.tableFooterView = spinner
                self.tableView?.tableFooterView?.isHidden = false
            }
        } else {
            self.tableView?.tableFooterView = nil
        }
        
    }
    
}
