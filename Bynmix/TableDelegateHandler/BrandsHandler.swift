import UIKit

class BrandsHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    let brandsCell = "BrandsCell"
    var brands = [Brand]()
    var tableView: UITableView?
    var onBrandsClick: ((Brand) -> Void)!
    var brandsDictionary = [String: [Brand]]()
    var brandsSectionTitles = [String]()
    var heightAtIndexPath = NSMutableDictionary()
    var isLoadMoreRequired = true
    var loadMoreDelegate: LoadMoreProtocol?
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let height = NSNumber(value: Float(cell.frame.size.height))
        heightAtIndexPath.setObject(height, forKey: indexPath as NSCopying)
        
        if isLoadMoreRequired {
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                self.tableView?.tableFooterView = spinner
                self.tableView?.tableFooterView?.isHidden = false
            }
        } else {
            self.tableView?.tableFooterView = nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return brands.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: brandsCell, for: indexPath) as! BrandsTableViewCell
        
        let brand = brands[indexPath.row] as Brand
        cell.brandNameLabel?.text = brand.brandName
        if brand.isSelected ?? false {
            cell.brandNameLabel.font = UIFont.fontBold(size: 16)
            cell.brandNameLabel.textColor = UIColor.cBB189C
        } else {
            cell.brandNameLabel.font = UIFont.fontRegular(size: 16)
            cell.brandNameLabel.textColor = UIColor.c424242
        }
        cell.tag = indexPath.row
        let cellTap = UITapGestureRecognizer(target: self, action: #selector(onBrandsTap))
        cell.addGestureRecognizer(cellTap)
        
        return cell
    }
    
    @objc func onBrandsTap(sender:UITapGestureRecognizer){
        let index = sender.view!.tag
        onBrandsClick(brands[index])
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView?.tableFooterView != nil {
            let height = scrollView.frame.size.height
            let contentYoffset = scrollView.contentOffset.y
            let distanceFromBottom = scrollView.contentSize.height - contentYoffset
            if distanceFromBottom < height {
                loadMoreDelegate?.onLoadMore()
            }
        }
    }
    
}
