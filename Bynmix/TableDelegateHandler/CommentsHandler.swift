import UIKit
import Foundation

class CommentsHandler: NSObject,UITableViewDelegate,UITableViewDataSource,UITextViewDelegate {
   
    var comments = [Comments]()
    let commentsCell = "commentsCell"
    var onUserImageViewClick:((Comments) -> Void)!
    var onreplyClick:((Comments) -> Void)!
    var usersList = [Feed]()
    var onMentionClick:((Int,String) -> Void)!
    var tableView:UITableView?
    var listingUserId:Int = 0
    var commentsIdDelegate:CommentsIdProtocol?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.comments.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let commentLabel = UILabel(frame: CGRect.zero)
        commentLabel.font = UIFont.fontRegular(size: 12)
        commentLabel.text = comments[indexPath.item].comment
        commentLabel.numberOfLines = 0
        commentLabel.sizeToFit()
        
        let timeLabel = UILabel(frame: CGRect.zero)
        timeLabel.font = UIFont.fontMediumItalic(size: 9)
        timeLabel.text = comments[indexPath.item].comment
        timeLabel.numberOfLines = 1
        timeLabel.sizeToFit()
        return (87.5 + commentLabel.frame.height + timeLabel.frame.height)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: commentsCell, for: indexPath) as! CommentsTableViewCell
        
        let comment = self.comments[indexPath.row] as Comments
        
        let image = UIImage(named: AppConstants.Images.DEFAULT_USER)
        cell.usernameLabel.text = comment.username
        
        cell.commentLabel.enabledTypes = [.mention]
        cell.commentLabel.mentionColor = UIColor.cFF80EE
        cell.commentLabel.highlightFontName = "Raleway-Bold"
        cell.commentLabel.highlightFontSize = 12
        cell.commentLabel.text = comment.comment
        
        cell.commentLabel.handleMentionTap { mention in
            let mentionString = "@" + mention
            if comment.userMentions != nil{
                for i in 0..<comment.userMentions!.count{
                    let mystring = comment.comment!
                    if let range = mystring.range(of: mentionString) {
                        let startPos = mystring.distance(from: mystring.startIndex, to: range.lowerBound)
                
                        if startPos == comment.userMentions![i].startPosition!{
                            let userId = comment.userMentions![i].userId!
                            self.commentsIdDelegate?.commentsId(id:userId)
                            break
                        }
                        
                    }
                }
            }
        }
        
        cell.timeLabel.text = comment.createdDateFormatted
        if let imageUrl = comment.userImage{
            cell.userImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        }
        
        cell.userImageView.layer.borderWidth = 2.0
        cell.userImageView.layer.masksToBounds = false
        cell.userImageView.layer.borderColor = UIColor.white.cgColor
        cell.userImageView.clipsToBounds = true
        cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.width/2
        cell.userImageView.isUserInteractionEnabled = true
        cell.usernameLabel.isUserInteractionEnabled = true
        
        cell.userImageView.tag = indexPath.row
        let userImageTap = UITapGestureRecognizer(target: self, action: #selector(userImageViewClick))
        cell.userImageView.addGestureRecognizer(userImageTap)
        
        cell.usernameLabel.tag = indexPath.row
        let userNameTap = UITapGestureRecognizer(target: self, action: #selector(userImageViewClick))
        cell.usernameLabel.addGestureRecognizer(userNameTap)
        
        cell.replyButton.tag = indexPath.row
        cell.replyButton.addTarget(self, action: #selector(onReplyClickAction), for: .touchUpInside)
        
        if comment.userId == self.listingUserId{
            cell.hilightedView.backgroundColor = UIColor.cC7C7C7
        }else{
            cell.hilightedView.backgroundColor = UIColor.white
        }
        
        cell.userShadowView.layer.cornerRadius = cell.userShadowView.frame.size.width/2
        cell.userShadowView.layer.shadowColor = UIColor.black.cgColor
        cell.userShadowView.alpha = CGFloat(0.80)
        cell.userShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        cell.userShadowView.layer.shadowOpacity = 1
        
        return cell
    }
    
    @objc func userImageViewClick(sender: UITapGestureRecognizer){
        let index = sender.view!.tag
        onUserImageViewClick(comments[index])
    }
    
    @objc func onReplyClickAction(sender: UIButton){
        let index = sender.tag
        onreplyClick(comments[index])
    }
}
