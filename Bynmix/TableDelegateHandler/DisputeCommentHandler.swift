import UIKit

class DisputeCommentHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    var comments = [Comments]()
    var disputeCell = "disputeCell"
    var onImageOneClick:((Comments) -> Void)!
    var onImageTwoClick:((Comments) -> Void)!
    var onImageThreeClick:((Comments) -> Void)!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: disputeCell, for: indexPath) as! DisputeCommentsTableViewCell
        
        let comment = comments[indexPath.row] as Comments
        
        cell.buyerNameView.layer.cornerRadius = cell.buyerNameView.frame.size.width / 2
        cell.sellerNameView.layer.cornerRadius = cell.sellerNameView.frame.size.width / 2
        
        if(comment.isChargeDisputeEvidenceComment ?? false) {
            cell.cancelDisputeView.isHidden = false
            cell.commentView.isHidden = true
            
            let htmlCSSString = "<style>" +
                "html *" +
                "{" +
                "font-size: \(11)pt !important;" +
                "font-family: Raleway-Regular !important;" +
            "}</style> \(self)"
            
            let text = htmlCSSString + comment.comment!
            cell.chargeDisputeCommentLabel.attributedText = text.html2Attributed
        } else {
            cell.cancelDisputeView.isHidden = true
            cell.commentView.isHidden = false
            cell.commentLabel.text = comment.comment
            if (comment.isBuyerComment ?? false) {
                cell.buyerNameView.isHidden = false
                cell.buyerNameLabel.isHidden = false
                
                cell.sellerNameView.isHidden = true
                cell.sellerNameLabel.isHidden = true
                
                cell.sellerWidthConstraint.constant = 0
                cell.sellerHeightConstraint.constant = 0
                
                cell.triangleLeftImageView.isHidden = false
                cell.triangleRightImageView.isHidden = true
            } else {
                cell.buyerNameView.isHidden = true
                cell.buyerNameLabel.isHidden = true
                
                cell.sellerNameView.isHidden = false
                cell.sellerNameLabel.isHidden = false
                
                cell.buyerWidthConstraint.constant = 0
                cell.buyerHeightConstraint.constant = 0
                
                cell.triangleLeftImageView.isHidden = true
                cell.triangleRightImageView.isHidden = false
            }
        }
        
        if comment.images != nil && comment.images!.count > 0{
            cell.disputeImagesView.isHidden = false
            let image = UIImage(named: AppConstants.Images.ITEM_PLACEHOLDER)
            
            if comment.images?.count == 1{
                if let imageUrl = comment.images?[0]{
                    cell.disputeImageOne.kf.setImage(with: URL(string: imageUrl), placeholder: image)
                }else{
                    cell.disputeImageOne.image = image
                }
                
                cell.disputeImageOne.isHidden = false
                cell.disputeImageTwo.isHidden = true
                cell.disputeImageThree.isHidden = true
                self.applyBorderImageOne(cell:cell)
            }else if comment.images?.count == 2{
                
                if let imageUrl = comment.images?[0]{
                    cell.disputeImageOne.kf.setImage(with: URL(string: imageUrl), placeholder: image)
                }else{
                    cell.disputeImageOne.image = image
                }
                
                if let imageUrl = comment.images?[1]{
                    cell.disputeImageTwo.kf.setImage(with: URL(string: imageUrl), placeholder: image)
                }else{
                    cell.disputeImageTwo.image = image
                }
                cell.disputeImageOne.isHidden = false
                cell.disputeImageTwo.isHidden = false
                cell.disputeImageThree.isHidden = true
                self.applyBorderImageOne(cell:cell)
                self.applyBorderImageTwo(cell:cell)
            }else if comment.images?.count == 3{
                
                if let imageUrl = comment.images?[0]{
                    cell.disputeImageOne.kf.setImage(with: URL(string: imageUrl), placeholder: image)
                }else{
                    cell.disputeImageOne.image = image
                }
                
                if let imageUrl = comment.images?[1]{
                    cell.disputeImageTwo.kf.setImage(with: URL(string: imageUrl), placeholder: image)
                }else{
                    cell.disputeImageTwo.image = image
                }
                
                if let imageUrl = comment.images?[2]{
                    cell.disputeImageThree.kf.setImage(with: URL(string: imageUrl), placeholder: image)
                }else{
                    cell.disputeImageThree.image = image
                }
                cell.disputeImageOne.isHidden = false
                cell.disputeImageTwo.isHidden = false
                cell.disputeImageThree.isHidden = false
                self.applyBorderImageOne(cell:cell)
                self.applyBorderImageTwo(cell:cell)
                self.applyBorderImageThree(cell:cell)
            }else{
                cell.disputeImagesView.isHidden = true
            }
            cell.disputeImageOne.contentMode = .scaleAspectFill
            cell.disputeImageTwo.contentMode = .scaleAspectFill
            cell.disputeImageThree.contentMode = .scaleAspectFill
        }else{
            cell.disputeImagesView.isHidden = true
        }
        
        cell.disputeImageOne.tag = indexPath.row
        let imageOneTap = UITapGestureRecognizer(target: self, action: #selector(onImageOneClickAction))
        cell.disputeImageOne.addGestureRecognizer(imageOneTap)
        
        cell.disputeImageTwo.tag = indexPath.row
        let imageTwoTap = UITapGestureRecognizer(target: self, action: #selector(onImageTwoClickAction))
        cell.disputeImageTwo.addGestureRecognizer(imageTwoTap)
        
        cell.disputeImageThree.tag = indexPath.row
        let imageThreeTap = UITapGestureRecognizer(target: self, action: #selector(onImageThreeClickAction))
        cell.disputeImageThree.addGestureRecognizer(imageThreeTap)
        
        return cell
    }
    
    func applyBorderImageOne(cell:DisputeCommentsTableViewCell){
        cell.disputeImageOne.layer.masksToBounds = true
        cell.disputeImageOne.layer.borderWidth = 1
        cell.disputeImageOne.layer.borderColor = UIColor.black.cgColor
        cell.disputeImageOne.layer.cornerRadius = 2
    }
    
    func applyBorderImageTwo(cell:DisputeCommentsTableViewCell){
        cell.disputeImageTwo.layer.masksToBounds = true
        cell.disputeImageTwo.layer.borderWidth = 1
        cell.disputeImageTwo.layer.borderColor = UIColor.black.cgColor
        cell.disputeImageTwo.layer.cornerRadius = 2
    }
    
    func applyBorderImageThree(cell:DisputeCommentsTableViewCell){
        cell.disputeImageThree.layer.masksToBounds = true
        cell.disputeImageThree.layer.borderWidth = 1
        cell.disputeImageThree.layer.borderColor = UIColor.black.cgColor
        cell.disputeImageThree.layer.cornerRadius = 2
    }
    
    @objc func onImageOneClickAction(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        onImageOneClick(comments[index])
    }
    
    @objc func onImageTwoClickAction(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        onImageTwoClick(comments[index])
    }
    
    @objc func onImageThreeClickAction(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        onImageThreeClick(comments[index])
    }
    
}
