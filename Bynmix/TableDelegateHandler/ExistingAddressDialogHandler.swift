import UIKit

class ExistingAddressDialogHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    var address = [Address]()
    let existingAddressCell = "addressesDialogCell"
    var addressesDialogTableViewCell :AddressesDialogTableViewCell?
    var onDefaultButtonClick: ((Address) -> Void)!
    var tableView:UITableView?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return address.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: existingAddressCell, for: indexPath) as! AddressesDialogTableViewCell
        let addresses = address[indexPath.row]
        addressesDialogTableViewCell = cell
        cell.nameLabel.text = addresses.fullName
        cell.addressOneLabel.text = addresses.addressOne
        cell.addressTwoLabel.text = addresses.addressTwo
        let city = addresses.city ?? ""
        let state = addresses.state ?? ""
        let zipcode = addresses.zipCode ?? ""
        cell.addressThreeLabel.text = (city + " " + state + ", " + zipcode)
        
        return cell
    }
    
    @objc func onSelectDefaultShippingAction(sender:UIButton){
        let index = sender.tag
        let myIndexPath = NSIndexPath(row: sender.tag, section: 0)
        let cell = tableView!.cellForRow(at: myIndexPath as IndexPath) as! AddressesDialogTableViewCell
        if cell.defaultShippingAddressButton.currentImage == UIImage(named: AppConstants.Images.CHECKBOX_SELECTED){
            cell.defaultShippingAddressButton.setImage(UIImage(named: AppConstants.Images.CHECKBOX), for: .normal)
        }else{
            cell.defaultShippingAddressButton.setImage(UIImage(named: AppConstants.Images.CHECKBOX_SELECTED), for: .normal)
            onDefaultButtonClick(address[index])
        }
    }

}
