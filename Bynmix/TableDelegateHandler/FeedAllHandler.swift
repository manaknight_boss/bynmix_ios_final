import UIKit

class FeedAllHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    var feed = [Feed]()
    var feedAllCell = "feedAllCell"
    var itemClickHandler: ((Feed) -> Void)!
    var onItemImageViewClick:((Feed) -> Void)!
    var tableView: UITableView?
    var onHeartButtonClick:((Feed) -> Void)!
    var scrollDelegate: FeedScrollProtocol?
    var heightAtIndexPath = NSMutableDictionary()
    var loadMoreDelegate: LoadMoreProtocol?
    var isLoadMoreRequired = true
    var onOtherUserClick:((Feed) -> Void)!
    var onTagsButtonClick:((Int, Feed) -> Void)!
    var onDotClick:((Dots) -> Void)!
    var feedAllTableViewCell:FeedAllTableViewCell?
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if let height = heightAtIndexPath.object(forKey: indexPath) as? NSNumber {
            return CGFloat(height.floatValue)
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let height = NSNumber(value: Float(cell.frame.size.height))
        heightAtIndexPath.setObject(height, forKey: indexPath as NSCopying)
        
        if isLoadMoreRequired {
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                self.tableView?.tableFooterView = spinner
                self.tableView?.tableFooterView?.isHidden = false
            }
        } else {
            self.tableView?.tableFooterView = nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return feed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: feedAllCell, for: indexPath) as! FeedAllTableViewCell
        
        feedAllTableViewCell = cell
        
        let feed = self.feed[indexPath.row] as Feed
        
        cell.topView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        let type = feed.type
        var image : UIImage = UIImage(named: AppConstants.Images.DEFAULT_USER)!
        if type == AppConstants.LISTING {
            image =  UIImage(named: AppConstants.Images.ITEM_PLACEHOLDER)!
        } else {
            if feed.postType == AppConstants.VIDEO_POST {
                image =  UIImage(named: AppConstants.Images.ITEM_VIDEO_PLACEHOLDER)!
                
            }else {
                image =  UIImage(named: AppConstants.Images.ITEM_PLACEHOLDER)!
            }
        }
        
        let text:String = AppConstants.Strings.SIZE + (feed.size ?? "")
        let textRegular:String = AppConstants.Strings.SIZE
        let range = (text as NSString).range(of: textRegular)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 11), range: range)
        
        let priceText:String = AppConstants.Strings.PRICE + AppConstants.Strings.DOLLAR_SIGN + String(UInt(feed.price ?? 0))
        let textPriceRegular:String = AppConstants.Strings.PRICE
        let priceRange = (priceText as NSString).range(of: textPriceRegular)
        let attributedPriceString = NSMutableAttributedString(string: priceText)
        attributedPriceString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontRegular(size: 11), range: priceRange)
        
        cell.middleView.frame = CGRect(x: 0, y: 0, width: cell.middleView.frame.width, height: cell.middleView.frame.width)
        cell.itemImageView.layer.zPosition = -1
        if let imageUrl = feed.photoUrl{
            cell.itemImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        }else{
            cell.itemImageView.image = image
        }
        cell.postTitleLabel.isHidden = true
        cell.titleLabel.text = feed.title
        cell.titleLabel.isHidden = false
        
        if type == AppConstants.LISTING {
            cell.sizePriceView.isHidden = false
            //cell.typeButton.setImage(UIImage(named: AppConstants.Images.BADGE_ICON), for: UIControl.State.normal)
            cell.topView.backgroundColor = UIColor.cD53A95
            cell.sizePriceView.backgroundColor = UIColor.cD53A95
            cell.sizeLabel.attributedText = attributedString
            cell.priceLabel.attributedText = attributedPriceString
            //cell.headerHeight.constant = 60
//            cell.postTitleLabel.isHidden = true
        }else {
//            cell.postTitleLabel.text = feed.title
//            cell.postTitleLabel.isHidden = false
            cell.sizePriceView.isHidden = true
            //cell.titleLabel.isHidden = true
            //cell.typeButton.setImage(UIImage(named: AppConstants.Images.BOOKMARK_ICON), for: UIControl.State.normal)
            cell.topView.backgroundColor = UIColor.cF3608C
            cell.sizePriceView.backgroundColor = UIColor.cF3608C
            //cell.headerHeight.constant = 60
        }
        
        if type == AppConstants.POST && feed.postType == AppConstants.VIDEO_POST {
            cell.videoImageView.isHidden = false
            cell.videoImageView.image = UIImage(named: AppConstants.Images.VIDEO_ICON)
        }else{
            cell.videoImageView.isHidden = true
        }
        
        let likesCount =  feed.likesCount
        cell.likesShadowView.layer.shadowColor = UIColor.black.cgColor
        cell.likesShadowView.alpha = CGFloat(0.26)
        cell.likesShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        cell.likesShadowView.layer.shadowOpacity = 1
        
        cell.likesView.tag = indexPath.row
        
        let likesTap = UITapGestureRecognizer(target: self, action: #selector(likesClick))
        cell.likesView.addGestureRecognizer(likesTap)
        
        cell.itemImageView.tag = indexPath.row
        let itemImageTap = UITapGestureRecognizer(target: self, action: #selector(itemImageViewClick))
        cell.itemImageView.addGestureRecognizer(itemImageTap)
        
        cell.heartButton.tag = indexPath.row
        cell.heartButton.addTarget(self, action: #selector(heartClick), for: .touchDown)
        
        if likesCount == 0 {
            cell.likesView.isHidden = true
            cell.likesShadowView.isHidden = true
            
        }else if likesCount == 1{
            cell.likesView.isHidden = false
            cell.likesShadowView.isHidden = false
            cell.likesLabel.text = "\(likesCount ?? 0)"
            cell.likesStringLabel.text = AppConstants.Strings.LIKE
        }else{
            cell.likesView.isHidden = false
            cell.likesShadowView.isHidden = false
            cell.likesLabel.text = "\(likesCount ?? 0)"
            cell.likesStringLabel.text = AppConstants.Strings.LIKES
        }
        
        cell.likesLabel.text = "\(feed.likesCount ?? 0)"
        
        cell.userImageView.layer.borderWidth = 2.0
        cell.userImageView.layer.masksToBounds = false
        cell.userImageView.layer.borderColor = UIColor.white.cgColor
        cell.userImageView.clipsToBounds = true
        cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.width/2
        
        cell.userShadowView.layer.cornerRadius = cell.userShadowView.frame.size.width/2
        cell.userShadowView.layer.shadowColor = UIColor.black.cgColor
        cell.userShadowView.alpha = CGFloat(0.26)
        cell.userShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        cell.userShadowView.layer.shadowOpacity = 1
        cell.userShadowView.layer.zPosition = -1
        
        if let imageUrl = feed.userPhotoUrl{
            cell.userImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        } else {
            cell.userImageView.image =  image
        }
        
        cell.usernameLabel.text = feed.username
        cell.postTimeLabel.text = feed.createdDateFormatted
        
        if feed.userLiked == false {
            cell.heartButton.setImage(UIImage(named: AppConstants.Images.LISTING_DETAIL_HEART), for: .normal)
        } else {
            cell.heartButton.setImage(UIImage(named: AppConstants.Images.LISTING_DETAIL_HEART_SELECTED), for: .normal)
        }
        
        if feed.hasPostDots == true {
            cell.dotsButton.isHidden = false
        }else {
            cell.dotsButton.isHidden = true
        }
        
        cell.dotsButton.tag = indexPath.row
        cell.dotsButton.addTarget(self, action: #selector(itemDotsButtonClick), for: .touchUpInside)
        
        cell.userImageView.tag = indexPath.row
        let userImageTap = UITapGestureRecognizer(target: self, action: #selector(onUserImageViewClick))
        cell.userImageView.addGestureRecognizer(userImageTap)
        
        cell.usernameLabel.tag = indexPath.row
        let userNameTap = UITapGestureRecognizer(target: self, action: #selector (onUserNameClick))
        cell.usernameLabel.addGestureRecognizer(userNameTap)
        
        if feed.canUserLikeItem!{
            cell.heartButton.isHidden = false
        }else{
            cell.heartButton.isHidden = true
        }
        
        if feed.containDots!{
            for view in (cell.itemImageView.subviews) {
                view.removeFromSuperview()
            }
            for i in 0..<feed.postDots!.count{
                self.setDot(index: i, dots: feed.postDots!)
            }
            cell.dotsButton.setTitle(AppConstants.Strings.HIDE_TAG_PRODUCTS, for: .normal)
        }else{
            for view in (cell.itemImageView.subviews) {
                view.removeFromSuperview()
            }
            cell.dotsButton.setTitle(AppConstants.Strings.TAG_PRODUCTS, for: .normal)
            self.feed[indexPath.row].containDots = false
        }
        
        return cell
    }
    
    @objc func onUserImageViewClick(sender:UITapGestureRecognizer){
        let index = sender.view!.tag
        onOtherUserClick(feed[index])
    }
    
    @objc func onUserNameClick(sender:UITapGestureRecognizer){
        let index = sender.view!.tag
        onOtherUserClick(feed[index])
    }
    
    @objc func itemDotsLabelClick(sender:UITapGestureRecognizer) {
        let index = sender.view!.tag
        let position = IndexPath(row: index, section: 0)
        self.tableView?.reloadRows(at: [position], with: UITableView.RowAnimation.none)
    }
    
    @objc func heartClick(sender: UIButton){
        let index = sender.tag
        var likesCount =  feed[index].likesCount
        var userLiked = feed[index].userLiked
        userLiked = !userLiked!
        if userLiked == false {
            likesCount! -= 1
        }else{
            likesCount! += 1
        }
        feed[index].likesCount = likesCount
        feed[index].userLiked = userLiked
        onHeartButtonClick(feed[index])
        let position = IndexPath(row: index, section: 0)
        self.tableView?.reloadRows(at: [position], with: UITableView.RowAnimation.none)
        
    }
    
    @objc func likesClick(sender: UITapGestureRecognizer){
        let index = sender.view!.tag
        itemClickHandler(feed[index])
    }
    
    @objc func itemImageViewClick(sender: UITapGestureRecognizer){
        let index = sender.view!.tag
        onItemImageViewClick(feed[index])
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom >= height {
            scrollDelegate?.onScroll(scrollValue: scrollView.contentOffset.y)
        }
        if self.tableView?.tableFooterView != nil {
            if distanceFromBottom < height {
                loadMoreDelegate?.onLoadMore()
            }
        }
    }
    
    @objc func itemDotsButtonClick(sender:UIButton) {
        let index = sender.tag
        let position = IndexPath(row: index, section: 0)
        if !feed[index].containDots!{
            print("feed index",index,feed[index])
            onTagsButtonClick(index, feed[index])
        }
        else{
            feed[index].containDots = false
            tableView?.reloadRows(at: [position], with: .none)
        }
    }
    
    func setDot(index: Int, dots: [Dots]) {
        var dotImage: UIImage
        if dots[index].hexColor == AppConstants.DOT_COLOR_RED{
            dotImage = UIImage(named: AppConstants.Images.VIEW_RED_ICON)!
        }else if dots[index].hexColor == AppConstants.DOT_COLOR_YELLOW{
            dotImage = UIImage(named: AppConstants.Images.VIEW_YELLOW_ICON)!
        }else if dots[index].hexColor == AppConstants.DOT_COLOR_BLUE{
            dotImage = UIImage(named: AppConstants.Images.VIEW_BLUE_ICON)!
        }else if dots[index].hexColor == AppConstants.DOT_COLOR_GREEN{
            dotImage = UIImage(named: AppConstants.Images.VIEW_GREEN_ICON)!
        }else {
            dotImage = UIImage(named: AppConstants.Images.VIEW_PURPLE_ICON)!
        }
        
        let imageWidth = Double((self.feedAllTableViewCell?.itemImageView.frame.width)!)
        let imageHeight = Double((self.feedAllTableViewCell?.itemImageView.frame.height)!)
        let x = dots[index].xCoordinate! * imageWidth
        let y = dots[index].yCoordinate! * imageHeight
        
        let dotImageWidth = Double((dotImage.size.width))
        let dotImageHeight = Double((dotImage.size.height))
        let dot = UIImageView(frame: CGRect(x: x - (dotImageWidth/2), y: y - (dotImageHeight), width: dotImageWidth, height: dotImageHeight))
        dot.image = dotImage
        dot.contentMode = .scaleAspectFit
        self.feedAllTableViewCell?.itemImageView.addSubview(dot)
        
        let dotTap = UITapGestureRecognizer(target: self, action: #selector(self.onDotTapDetected))
        dot.tag = dots[index].postDotId!
        dot.isUserInteractionEnabled = true
        dot.addGestureRecognizer(dotTap)
    }
    
    @objc func onDotTapDetected(sender: UITapGestureRecognizer){
        let id = (sender.view?.tag)!
        self.onDotClick(self.getDotById(id: id))
    }
    
    func getDotById(id: Int) -> Dots {
        for post in feed {
            if post.containDots! {
                let dots = post.postDots
                for dot in dots! {
                    if dot.postDotId == id {
                        return dot
                    }
                }
            }
        }
        return Dots()
    }
    
}
