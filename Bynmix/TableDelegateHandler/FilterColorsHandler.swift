import UIKit

class FilterColorsHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    var colorsList = [Colors]()
    let colorsCell = "FilterColorsCell"
    var onColorsClick: ((Colors) -> Void)!
    var tableView:UITableView?
    var filterSelectedDelegate: FilterSelectedProtocol?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return colorsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: colorsCell, for: indexPath) as! FilterColorsTableViewCell
        
        let colors = self.colorsList[indexPath.row] as Colors
        
        cell.colorView.layer.borderWidth = 1.0
        cell.colorView.layer.masksToBounds = false
        cell.colorView.layer.borderColor = UIColor.c4C4C4C.cgColor
        cell.colorView.clipsToBounds = true
        cell.colorView.layer.cornerRadius = cell.colorView.frame.size.width/2
        
        cell.colorNameButton.setTitle(colors.colorName, for: .normal)
        cell.colorView.backgroundColor = UIColor(hex: colors.hexCode!)
        
        cell.colorNameButton.tag = indexPath.row
        cell.colorNameButton.addTarget(self, action: #selector(onColorButtonAction), for: .touchUpInside)
        
        if filterSelectedDelegate?.isFilterSelected(type: AppConstants.COLOR_FILTER, id: colors.colorId!, subType: 0, isParentType: false) ?? false {
            cell.colorNameButton.titleLabel?.font = UIFont.fontSemiBold(size: 16)
            cell.colorNameButton.setTitleColor(UIColor.cBB189C, for: .normal)
        } else {
            cell.colorNameButton.setTitleColor(UIColor.c424242, for: .normal)
            cell.colorNameButton.titleLabel?.font = UIFont.fontRegular(size: 16)
        }
        
        cell.tag = indexPath.row
        let colorTap = UITapGestureRecognizer(target: self, action: #selector(onColorTapAction))
        cell.addGestureRecognizer(colorTap)
        
        return cell
    }
    
    @objc func onColorTapAction(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        onColorsClick(colorsList[index])
        reloadTableView()
    }
    
    @objc func onColorButtonAction(sender:UIButton){
        let index = sender.tag
        onColorsClick(colorsList[index])
        reloadTableView()
    }
    
    func reloadTableView(){
        self.tableView?.reloadData()
    }
    
}
