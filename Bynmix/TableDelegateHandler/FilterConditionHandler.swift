import UIKit

class FilterConditionHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    let conditionCell = "ConditionCell"
    var condition = [Condition]()
    var onConditionClick: ((Condition) -> Void)!
    var tableView:UITableView?
    var filterSelectedDelegate: FilterSelectedProtocol?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return condition.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 42
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: conditionCell, for: indexPath) as! FilterConditionTableViewCell
        
        let conditions = condition[indexPath.row] as Condition
        
        if filterSelectedDelegate?.isFilterSelected(type: AppConstants.CONDITION_FILTER, id: conditions.conditionId!, subType: 0, isParentType: false) ?? false {
            cell.conditionNameLabel.font = UIFont.fontSemiBold(size: 16)
            cell.conditionNameLabel.textColor = UIColor.cBB189C
        } else {
            if conditions.conditionId == 0{
                cell.conditionNameLabel.font = UIFont.fontSemiBold(size: 16)
                cell.conditionNameLabel.textColor = UIColor.c424242
            }else{
                cell.conditionNameLabel.font = UIFont.fontRegular(size: 16)
                cell.conditionNameLabel.textColor = UIColor.c424242
            }
        }
        
        var filterName = conditions.conditionName
        if(filterName?.caseInsensitiveCompare("Show all") == ComparisonResult.orderedSame){
            filterName = filterName?.uppercased()
            cell.conditionNameLabel.text = filterName
             cell.conditionNameLabel.font = UIFont.fontSemiBold(size: 16)
        }else{
            cell.conditionNameLabel.text = filterName
        }
        
        cell.tag = indexPath.row
        let conditionTap = UITapGestureRecognizer(target: self, action: #selector(onConditionTapAction))
        cell.addGestureRecognizer(conditionTap)
        
        return cell
    }
    
    @objc func onConditionTapAction(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        onConditionClick(condition[index])
        tableView?.reloadData()
    }
      
}
