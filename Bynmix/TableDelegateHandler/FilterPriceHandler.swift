import UIKit

class FilterPriceHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    let priceCell = "priceCell"
    var price = [Price]()
    var onPriceClick: ((Price) -> Void)!
    var filterSelectedDelegate: FilterSelectedProtocol?
    var tableView :UITableView?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return price.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 42
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: priceCell, for: indexPath) as! FilterPriceTableViewCell
        
        let prices = price[indexPath.row] as Price
        
        cell.priceButton.setTitle(prices.price, for: .normal)
        
        cell.tag = indexPath.row
        let priceTap = UITapGestureRecognizer(target: self, action: #selector(onPriceTapAction))
        cell.addGestureRecognizer(priceTap)
        
        cell.priceButton.tag = indexPath.row
        cell.priceButton.addTarget(self, action: #selector(onPriceButtonAction), for: .touchUpInside)
        
        if filterSelectedDelegate?.isFilterSelected(type: AppConstants.PRICE_FILTER, id: prices.id!, subType: 0, isParentType: false) ?? false {
            cell.priceButton.titleLabel?.font = UIFont.fontSemiBold(size: 16)
            cell.priceButton.setTitleColor(UIColor.cBB189C, for: .normal)
        } else {
            if prices.id == 0{
                cell.priceButton.setTitleColor(UIColor.c424242, for: .normal)
                cell.priceButton.titleLabel?.font = UIFont.fontSemiBold(size: 16)
            }else{
                cell.priceButton.setTitleColor(UIColor.c424242, for: .normal)
                cell.priceButton.titleLabel?.font = UIFont.fontRegular(size: 16)
            }
        }
        
        return cell
    }
    
    @objc func onPriceTapAction(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        onPriceClick(price[index])
        reloadTableView()
    }
    
    @objc func onPriceButtonAction(sender:UIButton){
        let index = sender.tag
        onPriceClick(price[index])
        reloadTableView()
    }
    
    func reloadTableView(){
        self.tableView?.reloadData()
    }
      
}
