import UIKit

class FilterSelectCategoryHandler: NSObject,UITableViewDelegate,UITableViewDataSource {

    let selectCategoryCell = "SelectCategoryCell"
    var categoryType = [CategoryType]()
    var onCategoryTypeClick:((CategoryType) -> Void)!
    var tableView:UITableView?
    var filterSelectedDelegate: FilterSelectedProtocol?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return categoryType.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 42
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: selectCategoryCell, for: indexPath) as! FilterSelectCategoryTableViewCell
        
        let categoryTypes = categoryType[indexPath.row] as CategoryType
        
        cell.categoryNameButton.setTitle(categoryTypes.categoryTypeName, for: .normal)
        
        if filterSelectedDelegate?.isFilterSelected(type: AppConstants.CATEGORY_FILTER, id: categoryTypes.categoryTypeId!, subType: 0, isParentType: true) ?? false{
            cell.categoryNameButton.setTitleColor(UIColor.cBB189C, for: .normal)
            cell.categoryNameButton.titleLabel?.font = UIFont.fontSemiBold(size: 16)
        }else{
            if categoryTypes.categoryTypeId == 0{
                cell.arrowImageView.isHidden = true
                cell.categoryNameButton.titleLabel?.font = UIFont.fontSemiBold(size: 16)
                cell.categoryNameButton.setTitleColor(UIColor.c424242, for: .normal)
            }else{
                cell.arrowImageView.isHidden = false
                cell.categoryNameButton.titleLabel?.font = UIFont.fontRegular(size: 16)
                cell.categoryNameButton.setTitleColor(UIColor.c424242, for: .normal)
            }
        }
        
        cell.tag = indexPath.row
        let tap = UITapGestureRecognizer(target: self, action: #selector(onCategoryTypeClickAction))
        cell.addGestureRecognizer(tap)
        
        cell.categoryNameButton.tag = indexPath.row
        cell.categoryNameButton.addTarget(self, action: #selector(onCategoryTypeButtonAction), for: .touchUpInside)
        
        return cell
    }
    
    @objc func onCategoryTypeClickAction(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        onCategoryTypeClick(categoryType[index])
        reloadTableView()
    }
    
    @objc func onCategoryTypeButtonAction(sender:UIButton){
        let index = sender.tag
        onCategoryTypeClick(categoryType[index])
        reloadTableView()
    }
    
    func reloadTableView(){
        self.tableView?.reloadData()
    }
    
}
