import UIKit

class FilterSelectSubCategoryHandler:NSObject,UITableViewDelegate,UITableViewDataSource {
    
    let selectSubCategoryCell = "SelectSubCategoryCell"
    var category = [Category]()
    var onCategoryClick:((Category,Int,String) -> Void)!
    var subTypeId:Int?
    var subTypeName:String?
    var tableView:UITableView?
    var filterSelectedDelegate: FilterSelectedProtocol?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return category.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 42
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: selectSubCategoryCell, for: indexPath) as! FilterSelectSubCategoryTableViewCell
        
        let categories = category[indexPath.row] as Category
        
        cell.subCategoryButton.setTitle(categories.categoryName, for: .normal)
        
        if filterSelectedDelegate?.isFilterSelected(type: AppConstants.CATEGORY_FILTER, id: subTypeId!, subType:  categories.categoryId!, isParentType: false) ?? false{
            cell.subCategoryButton.setTitleColor(UIColor.cBB189C, for: .normal)
            cell.subCategoryButton.titleLabel?.font = UIFont.fontSemiBold(size: 16)
        }else{
            if categories.categoryId == 0{
                cell.subCategoryButton.titleLabel?.font = UIFont.fontSemiBold(size: 16)
                cell.subCategoryButton.setTitleColor(UIColor.c424242, for: .normal)
            }else{
                cell.subCategoryButton.titleLabel?.font = UIFont.fontRegular(size: 16)
                cell.subCategoryButton.setTitleColor(UIColor.c424242, for: .normal)
            }
        }
        
        cell.tag = indexPath.row
        let tap = UITapGestureRecognizer(target: self, action: #selector(onCategoryClickAction))
        cell.addGestureRecognizer(tap)
        
        cell.subCategoryButton.tag = indexPath.row
        cell.subCategoryButton.addTarget(self, action: #selector(onCategoryButtonAction), for: .touchUpInside)
        
        return cell
    }
    
    @objc func onCategoryClickAction(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        onCategoryClick(category[index],self.subTypeId!, subTypeName ?? "")
        reloadTableView()
    }
    
    @objc func onCategoryButtonAction(sender:UIButton){
        let index = sender.tag
        onCategoryClick(category[index],self.subTypeId!, subTypeName ?? "")
        reloadTableView()
    }
    
    func reloadTableView(){
        self.tableView?.reloadData()
    }
    
}
