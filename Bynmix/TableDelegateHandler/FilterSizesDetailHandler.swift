import UIKit

class FilterSizesDetailHandler: NSObject,UITableViewDelegate,UITableViewDataSource, FilterSelectedProtocol,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let index = collectionView.tag
        if sizeList[index].sizes!.count == 1{
            let size = Sizes(sizeId: 0, sizeName: "")
            sizeList[index].sizes!.insert(size, at: 1)
        }
        return sizeList[index].sizes!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: sizeCell, for: indexPath) as! FilterSizeDetailCollectionViewCell
        let index = collectionView.tag
        let size = sizeList[index].sizes![indexPath.item] as Sizes
        
        cell.sizeNameLabel.text = size.sizeName
        
        if size.sizeId == 0{
            cell.seperatorView.isHidden = true
        }else{
            cell.seperatorView.isHidden = false
        }
        
        if filterSelectedDelegate?.isFilterSelected(type: AppConstants.SIZE_FILTER, id: subTypeId!, subType:  size.sizeId!, isParentType: false) ?? false{
            self.selectedSize(cell:cell)
        }else{
            self.unSelectedSize(cell:cell)
        }
        
        cell.tag = size.sizeId!
        let tap = UITapGestureRecognizer(target: self, action: #selector(onSizeDetailClickAction))
        cell.addGestureRecognizer(tap)
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionViewSize = collectionView.frame.size.width * 0.499
        return CGSize(width: collectionViewSize, height: 42)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func isFilterSelected(type: Int, id: Int, subType: Int, isParentType: Bool) -> Bool {
        self.filterSelectedDelegate?.isFilterSelected(type: type, id: id, subType: subType, isParentType: isParentType) ?? false
    }
    
    let selectSizesDetailCell = "SelectDetailSizesCell"
    var sizeList = [FilterSizes]()
    var tableView:UITableView?
    var subTypeId:Int?
    var subTypeName:String?
    var filterSelectedDelegate: FilterSelectedProtocol?
    let sizeCell = "FilterSizesCell"
    var onSizeDetailClick:((Sizes,Int,String) -> Void)!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sizeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: selectSizesDetailCell, for: indexPath) as! FilterSizesDetailTableViewCell
        self.passDataToGrid(cell:cell, indexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sizeList[section].fit?.uppercased()
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor.cBB189C
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.white
        header.textLabel?.font = UIFont.fontSemiBold(size: 16)
    }
    
    func passDataToGrid(cell:FilterSizesDetailTableViewCell,indexPath:IndexPath){
        cell.collectionView.delegate = self
        cell.collectionView.dataSource = self
        cell.collectionView.tag = indexPath.section
        cell.collectionView.reloadData()
        let height : CGFloat = cell.collectionView.collectionViewLayout.collectionViewContentSize.height
        cell.collectionViewHeight.constant = height
        cell.layoutIfNeeded()
    }
    
    func selectedSize(cell:FilterSizeDetailCollectionViewCell){
        cell.sizeNameLabel.font = UIFont.fontSemiBold(size: 16)
        cell.sizeNameLabel.textColor = UIColor.cBB189C
    }
    
    func unSelectedSize(cell:FilterSizeDetailCollectionViewCell){
        cell.sizeNameLabel.font = UIFont.fontRegular(size: 16)
        cell.sizeNameLabel.textColor = UIColor.c424242
    }
    
    @objc func onSizeDetailClickAction(sender:UITapGestureRecognizer){
        let sizeId = (sender.view?.tag)!

        for i in 0..<self.sizeList.count {
            let size = sizeList[i].sizes
            for j in 0..<size!.count {
                if size![j].sizeId == sizeId {
                    if sizeId != 0{
                        self.onSizeDetailClick(size![j],self.subTypeId ?? 0,self.subTypeName ?? "")
                    }
                }
            }
        }
    }
    
}
