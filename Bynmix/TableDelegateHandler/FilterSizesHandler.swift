import UIKit

class FilterSizesHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    let sizeCell = "SelectFilterSizeCell"
    var sizeType = [FilterSizeType]()
    var onSizeTypeClick:((FilterSizeType) -> Void)!
    var tableView:UITableView?
    var filterSelectedDelegate: FilterSelectedProtocol?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return sizeType.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 42
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: sizeCell, for: indexPath) as! SelectFilterSizeTableViewCell
        
        let sizeTypes = sizeType[indexPath.row] as FilterSizeType
        
        cell.sizeNameButton.setTitle(sizeTypes.sizeTypeName, for: .normal)
        
        if filterSelectedDelegate?.isFilterSelected(type: AppConstants.SIZE_FILTER, id: sizeTypes.sizeTypeId!, subType: 0, isParentType: true) ?? false{
            cell.sizeNameButton.setTitleColor(UIColor.cBB189C, for: .normal)
            cell.sizeNameButton.titleLabel?.font = UIFont.fontSemiBold(size: 16)
        }else{
            if sizeTypes.sizeTypeId == 0{
                cell.sizeNameButton.titleLabel?.font = UIFont.fontSemiBold(size: 16)
                cell.sizeNameButton.setTitleColor(UIColor.c424242, for: .normal)
                cell.arrowImageView.isHidden = true
            }else{
                cell.sizeNameButton.titleLabel?.font = UIFont.fontRegular(size: 16)
                cell.sizeNameButton.setTitleColor(UIColor.c424242, for: .normal)
                cell.arrowImageView.isHidden = false
            }
        }
        
        cell.tag = indexPath.row
        let tap = UITapGestureRecognizer(target: self, action: #selector(onSizeTypeClickAction))
        cell.addGestureRecognizer(tap)
        
        cell.sizeNameButton.tag = indexPath.row
        cell.sizeNameButton.addTarget(self, action: #selector(onSizeTypeButtonAction), for: .touchUpInside)
        
        cell.layoutIfNeeded()
        
        return cell
    }
    
    @objc func onSizeTypeClickAction(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        onSizeTypeClick(sizeType[index])
        reloadTableView()
    }
    
    @objc func onSizeTypeButtonAction(sender:UIButton){
        let index = sender.tag
        onSizeTypeClick(sizeType[index])
        reloadTableView()
    }
    
    func reloadTableView(){
        self.tableView?.reloadData()
    }
    
}
