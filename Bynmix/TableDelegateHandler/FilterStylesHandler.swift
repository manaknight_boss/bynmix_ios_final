import UIKit

class FilterStylesHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    let styleCell = "filterStylesCell"
    var styles = [Styles]()
    var onStyleClick:((Styles,Int) -> Void)!
    var stylesListTableViewCell:StylesListTableViewCell?
    var tableview:UITableView?
    var selectedStyles : [Styles] = []
    var filterSelectedDelegate: FilterSelectedProtocol?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableview = tableView
        return styles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: styleCell, for: indexPath) as! StylesListTableViewCell
        stylesListTableViewCell = cell
        
        let style = styles[indexPath.row] as Styles
        
        cell.styleNameLabel.text = style.descriptorName
        
        if filterSelectedDelegate?.isFilterSelected(type: AppConstants.USER_STYLE_FILTER, id: style.descriptorId!, subType: 0, isParentType: false) ?? false {
            cell.styleNameLabel.textColor = UIColor.cBB189C
            cell.styleNameLabel.font = UIFont.fontSemiBold(size: 16)
        }else{
            if style.descriptorId == 0{
                cell.styleNameLabel.font = UIFont.fontSemiBold(size: 16)
                cell.styleNameLabel.textColor = UIColor.c424242
            }else{
                cell.styleNameLabel.font = UIFont.fontRegular(size: 16)
                cell.styleNameLabel.textColor = UIColor.c424242
            }
        }
        
        cell.tag = indexPath.row
        let tap = UITapGestureRecognizer(target: self, action: #selector(onStyleClickAction))
        cell.addGestureRecognizer(tap)
        
        return cell
    }
    
    @objc func onStyleClickAction(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        onStyleClick(styles[index],index)
        tableview?.reloadData()
    }

}
