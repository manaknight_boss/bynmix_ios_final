import UIKit

class FilterUserBodyTypeHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    let bodyTypeCell = "filterBodyTypeCell"
    var bodyType = [BodyType]()
    var onBodyTypeClick:((BodyType) -> Void)!
    var tableview:UITableView?
    var filterSelectedDelegate: FilterSelectedProtocol?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableview = tableView
        return bodyType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: bodyTypeCell, for: indexPath) as! FilterUserBodyTypeTableViewCell
        
        let bodyTypes = bodyType[indexPath.row] as BodyType
        
        cell.bodyTypeLabel.text = bodyTypes.bodyTypeName
        
        if filterSelectedDelegate?.isFilterSelected(type: AppConstants.USER_BODY_TYPE_FILTER, id: bodyTypes.bodyTypeId!, subType: 0, isParentType: false) ?? false{
            cell.bodyTypeLabel.textColor = UIColor.cBB189C
            cell.bodyTypeLabel.font = UIFont.fontSemiBold(size: 16)
        }else{
            if bodyTypes.bodyTypeId == 0{
                cell.bodyTypeLabel.font = UIFont.fontSemiBold(size: 16)
                cell.bodyTypeLabel.textColor = UIColor.c424242
            }else{
                cell.bodyTypeLabel.font = UIFont.fontRegular(size: 16)
                cell.bodyTypeLabel.textColor = UIColor.c424242
            }
        }
        
        cell.tag = indexPath.row
        let tap = UITapGestureRecognizer(target: self, action: #selector(onBodyTypeClickAction))
        cell.addGestureRecognizer(tap)
        
        return cell
    }
    
    @objc func onBodyTypeClickAction(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        onBodyTypeClick(bodyType[index])
        tableview?.reloadData()
    }
    
}
