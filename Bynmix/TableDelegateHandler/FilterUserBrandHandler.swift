import UIKit

class FilterUserBrandHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    let brandCell = "filterBrandsCell"
    var brands = [Brand]()
    var onBrandClick:((Brand) -> Void)!
    var heightAtIndexPath = NSMutableDictionary()
    var isLoadMoreRequired = true
    var loadMoreDelegate: LoadMoreProtocol?
    var tableView:UITableView?
    var filterSelectedDelegate: FilterSelectedProtocol?
    var TYPE:Int?
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let height = NSNumber(value: Float(cell.frame.size.height))
        heightAtIndexPath.setObject(height, forKey: indexPath as NSCopying)
        
        if isLoadMoreRequired {
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                self.tableView?.tableFooterView = spinner
                self.tableView?.tableFooterView?.isHidden = false
            }
        } else {
            self.tableView?.tableFooterView = nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return brands.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: brandCell, for: indexPath) as! FilterUserBrandsTableViewCell
        
        let brand = brands[indexPath.row] as Brand
        
        cell.brandNameLabel.text = brand.brandName
        
        if TYPE == AppConstants.BRAND_FILTER{
            if filterSelectedDelegate?.isFilterSelected(type: AppConstants.BRAND_FILTER, id: brand.brandId!, subType: 0, isParentType: false) ?? false {
                self.selectedFilter(cell:cell)
            }else{
                self.unSelectedFilter(cell:cell,brand:brand)
            }
        }else{
            if filterSelectedDelegate?.isFilterSelected(type: AppConstants.USER_BRAND_FILTER, id: brand.brandId!, subType: 0, isParentType: false) ?? false {
                self.selectedFilter(cell:cell)
            }else{
                self.unSelectedFilter(cell:cell,brand:brand)
            }
        }
        
        cell.tag = indexPath.row
        let tap = UITapGestureRecognizer(target: self, action: #selector(onBrandClickAction))
        cell.addGestureRecognizer(tap)
        
        return cell
    }
    
    func unSelectedFilter(cell:FilterUserBrandsTableViewCell,brand:Brand){
        if brand.brandId == 0{
            cell.brandNameLabel.font = UIFont.fontSemiBold(size: 16)
            cell.brandNameLabel.textColor = UIColor.c424242
        }else{
            cell.brandNameLabel.font = UIFont.fontRegular(size: 16)
            cell.brandNameLabel.textColor = UIColor.c424242
        }
    }
    
    func selectedFilter(cell:FilterUserBrandsTableViewCell){
        cell.brandNameLabel.textColor = UIColor.cBB189C
        cell.brandNameLabel.font = UIFont.fontSemiBold(size: 16)
    }
    
    @objc func onBrandClickAction(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        onBrandClick(brands[index])
        tableView?.reloadData()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView?.tableFooterView != nil {
            let height = scrollView.frame.size.height
            let contentYoffset = scrollView.contentOffset.y
            let distanceFromBottom = scrollView.contentSize.height - contentYoffset
            if distanceFromBottom < height {
                loadMoreDelegate?.onLoadMore()
            }
        }
    }
    
}
