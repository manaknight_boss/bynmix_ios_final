import UIKit

class FollowersFollowingHandler: NSObject,UITableViewDelegate,UITableViewDataSource {

    var users = [User]()
    let userCell:String = "follow_following_cell"
    var itemClickHandler: ((User) -> Void)!
    var tableView: UITableView?
    var loadMoreDelegate: LoadMoreProtocol?
    var isLoadMoreRequired = true
    var otherUserClickHandler: ((User) -> Void)!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return self.users.count
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView?.tableFooterView != nil {
            let height = scrollView.frame.size.height
            let contentYoffset = scrollView.contentOffset.y
            let distanceFromBottom = scrollView.contentSize.height - contentYoffset
            if distanceFromBottom < height {
                loadMoreDelegate?.onLoadMore()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: userCell, for: indexPath) as! FollowFollowingTableViewCell
        
        let users = self.users[indexPath.row] as User
        
        cell.topView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        cell.bottomView.layer.maskedCorners = [.layerMaxXMaxYCorner,.layerMinXMaxYCorner]
        
        let image = UIImage(named: AppConstants.Images.DEFAULT_USER)
        cell.usernameLabel.text = users.username
        if users.title == nil{
            cell.titleLabel.text = AppConstants.EMPTY
        }else{
           cell.titleLabel.text = users.title
        }
        
        if users.userDescriptorsFormatted == "" {
            cell.styleLabel.text = AppConstants.EMPTY
        }else{
            cell.styleLabel.text = users.userDescriptorsFormatted
        }
        if users.userBrandsFormatted == "" {
            cell.brandsLabel.text = AppConstants.EMPTY
        }else{
            cell.brandsLabel.text = users.userBrandsFormatted
        }
        
        cell.userImageView.layer.borderWidth = 2.0
        cell.userImageView.layer.masksToBounds = false
        cell.userImageView.layer.borderColor = UIColor.white.cgColor
        cell.userImageView.clipsToBounds = true
        cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.width/2
        
        cell.userShadowView.layer.cornerRadius = cell.userShadowView.frame.size.width/2
        cell.userShadowView.layer.shadowColor = UIColor.black.cgColor
        cell.userShadowView.alpha = CGFloat(0.26)
        cell.userShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        cell.userShadowView.layer.shadowOpacity = 1
        
        cell.bottomView.layer.zPosition = -1
        
        if let imageUrl = users.photoUrl{
            cell.userImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        } else {
            cell.userImageView.image =  image
        }
        
        if users.isUserSameAsLoggedInUser == true {
            cell.followFolllowingImageView.isHidden = true
        }else{
            cell.followFolllowingImageView.isHidden = false
        }
            
        if users.isLoggedInUserFollowing! {
            cell.followFolllowingImageView.image = UIImage(named: AppConstants.Images.FOLLOWING_ICON)
            
        } else {
            cell.followFolllowingImageView.image = UIImage(named: AppConstants.Images.FOLLOW_ICON)
        }
        cell.followFolllowingImageView.tag = indexPath.row
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(followFollowingButtonClicked))
        cell.followFolllowingImageView.addGestureRecognizer(tap)
        
        cell.usernameLabel.tag = indexPath.row
        let usernameTap = UITapGestureRecognizer(target: self, action: #selector(usernameClicked))
        cell.usernameLabel.addGestureRecognizer(usernameTap)
        
        cell.userImageView.tag = indexPath.row
        let usertap = UITapGestureRecognizer(target: self, action: #selector(userImageClicked))
        cell.userImageView.addGestureRecognizer(usertap)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if isLoadMoreRequired {
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                self.tableView?.tableFooterView = spinner
                self.tableView?.tableFooterView?.isHidden = false
            }
        } else {
            self.tableView?.tableFooterView = nil
        }
        
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    @objc func followFollowingButtonClicked(sender: UITapGestureRecognizer) {
        let index = (sender.view?.tag)!
        users[index].isLoggedInUserFollowing = !users[index].isLoggedInUserFollowing!
        self.tableView?.reloadData()
         users[index].viewControllerType = false
         itemClickHandler(users[index])
       
    }
    
    @objc func usernameClicked(sender: UITapGestureRecognizer) {
        let index = (sender.view?.tag)!
        otherUserClickHandler(users[index])
       
    }
    
    @objc func userImageClicked(sender: UITapGestureRecognizer) {
        let index = (sender.view?.tag)!
        otherUserClickHandler(users[index])
        
    }
    
}
