import UIKit

class IntroductionHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    let introductionCell = "introductionPointsCell"
    var introduction = [IntroPoints]()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return introduction.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: introductionCell, for: indexPath) as! IntroductionTableViewCell
        let intro = introduction[indexPath.row] as IntroPoints
        
        cell.pointLabel.text = intro.point
        
        return cell
    }
    
}
