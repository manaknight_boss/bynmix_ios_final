import UIKit

class MentionsHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    var mentionItems = [MentionItem]()
    var mentionCell = "mentionCell"
    var onUserClick:((MentionItem) -> Void)!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mentionItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: mentionCell, for: indexPath) as! MentionsTableViewCell
        
        let mentionItem = mentionItems[indexPath.row] as MentionItem
        
        let image = UIImage(named: AppConstants.Images.DEFAULT_USER)
        
        if let imageUrl = mentionItem.imageUrl{
            cell.userImageView.kf.setImage(with: URL(string: imageUrl), placeholder: image)
        }else{
            cell.userImageView.image = image
        }
        cell.userNameLabel.text = mentionItem.username
        cell.fullNameLabel.text = mentionItem.fullName
        
        cell.userImageView.clipsToBounds = true
        cell.userImageView.layer.cornerRadius = cell.userImageView.frame.size.width/2
        
        cell.tag = indexPath.row
        let tap = UITapGestureRecognizer(target: self, action: #selector(onUserClickAction))
        cell.addGestureRecognizer(tap)
        
        return cell
    }
    
    @objc func onUserClickAction(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        onUserClick(mentionItems[index])
    }
    
}
