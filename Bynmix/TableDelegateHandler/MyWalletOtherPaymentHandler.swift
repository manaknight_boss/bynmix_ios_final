import UIKit

class MyWalletOtherPaymentHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    var card = [Card]()
    let myWalletCell = "myWalletCell"
    var onEditButtonClick: ((Card) -> Void)!
    var onDeleteButtonClick: ((Card) -> Void)!
    
    func deleteStatus(id: Int) {
        var index = -1
        for i in 0..<self.card.count {
            if self.card[i].cardId == id {
                index = i
                break
            }
        }
        self.card.remove(at: index)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return card.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))
        
        let label = UILabel()
        label.frame = CGRect.init(x: 15, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.text = "Other Payment Types"
        label.font = UIFont.fontMedium(size: 18)
        label.textColor = UIColor.c414141 
        
        headerView.addSubview(label)
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: myWalletCell, for: indexPath) as! MyWalletDefaultPaymentTableViewCell
        
        let cards = card[indexPath.row]
        
        let image = UIImage(named: AppConstants.Images.DEFAULT_CARD_ICON)
        
        if let imageUrl = cards.brandImageUrl{
            cell.brandImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        }
        
        cell.brandNameWithCardNumberLabel.text = cards.brand! + " " + AppConstants.Strings.ENDING_IN + " " +  cards.lastFour!
        cell.cardHolderNameLabel.text = cards.nameOnCard
        cell.expirationDateLabel.text = AppConstants.EXPIRES + " " + String(cards.expirationMonth!) + AppConstants.FORWARD_SLASH + String(cards.expirationYear!)
        
        let text:String = cards.brand! + " " + AppConstants.Strings.ENDING_IN + " " +  cards.lastFour!
        let textBrand:String = cards.brand!
        let textLastFour:String = cards.lastFour!
        let range = (text as NSString).range(of: textLastFour)
        let rangeLastFour = (text as NSString).range(of: textBrand)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: range)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontBold(size: 16), range: rangeLastFour)
        
        cell.brandNameWithCardNumberLabel.attributedText = attributedString
        cell.editButton.tag = indexPath.row
        cell.editButton.addTarget(self, action: #selector(onEditButtonAction), for: .touchUpInside)
        cell.deleteButton.tag = indexPath.row
        cell.deleteButton.addTarget(self, action: #selector(onDeleteButtonAction), for: .touchUpInside)
        
        return cell
    }
    
    @objc func onEditButtonAction(sender:UIButton){
        let index = sender.tag
        onEditButtonClick(card[index])
    }
    
    @objc func onDeleteButtonAction(sender:UIButton){
        let index = sender.tag
        onDeleteButtonClick(card[index])
    }
    
}
