import UIKit

class NotificationHandler: NSObject,UITableViewDataSource,UITableViewDelegate {
    
    var notificationSettings = [NotificationSettings]()
    let notificationCell = "notificationCell"

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationSettings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: notificationCell, for: indexPath) as! NotificationTableViewCell
        
        let notifications = notificationSettings[indexPath.row] as NotificationSettings
        let settingType = notifications.settingType
        cell.notificationNameLabel.text = settingType!.typeTitle
        cell.notificationDescLabel.text = settingType!.description
        cell.notificationSwitch.isOn = notifications.isEnabled!
        
        cell.notificationSwitch.tag = indexPath.row
        
        
        cell.notificationSwitch.layer.borderColor = UIColor.c696969.cgColor
        cell.notificationSwitch.layer.borderWidth = 2
        cell.notificationSwitch.layer.cornerRadius = 16
        cell.notificationSwitch.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
        return cell
    }
    
    @objc func switchChanged(sender: UISwitch) {
        let value = sender.isOn
        let index = sender.tag
        notificationSettings[index].isEnabled = value
    }

}
