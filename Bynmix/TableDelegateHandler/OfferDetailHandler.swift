import UIKit

class OfferDetailHandler:  NSObject,UITableViewDelegate,UITableViewDataSource  {
    
    let offerDetailCell = "offerDetailCell"
    
    var subOfferHistory = [SubOfferHistory]()
    var onCancelButtonClick:((SubOfferHistory,Int) -> Void)!
    var isOfferCancelable:Bool?
    var offerDetailTableViewCell:OfferDetailTableViewCell?
    var onLeftViewClick:((SubOfferHistory) -> Void)!
    var onRightViewClick:((SubOfferHistory) -> Void)!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subOfferHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: offerDetailCell, for: indexPath) as! OfferDetailTableViewCell
        
        offerDetailTableViewCell = cell
        
        let subOffersHistory = subOfferHistory[indexPath.row] as SubOfferHistory
        
        if(subOffersHistory.offerType == AppConstants.TYPE_OFFER) {
            if (subOffersHistory.isBuyerOffer ?? false) {
                self.setContainer(isLeft: false, subOffersHistory: subOffersHistory, cell: cell, isCancelButtonRequired: indexPath.row == subOfferHistory.count - 1)
                self.setOfferUI(isLeft: false, subOffersHistory: subOffersHistory, cell: cell)
            } else {
                self.setContainer(isLeft: true, subOffersHistory: subOffersHistory, cell: cell, isCancelButtonRequired: indexPath.row == subOfferHistory.count - 1)
                self.setOfferUI(isLeft: true, subOffersHistory: subOffersHistory, cell: cell)
            }
        } else if(subOffersHistory.offerType == AppConstants.TYPE_DECLINED) {
            if (subOffersHistory.isDeclinedBySeller ?? false) {
                self.setContainer(isLeft: true, subOffersHistory: subOffersHistory, cell: cell, isCancelButtonRequired: indexPath.row == subOfferHistory.count - 1)
                self.setDeclinedUI(isLeft: true, subOffersHistory: subOffersHistory, cell: cell)
            } else {
                self.setContainer(isLeft: false, subOffersHistory: subOffersHistory, cell: cell, isCancelButtonRequired: indexPath.row == subOfferHistory.count - 1)
                self.setDeclinedUI(isLeft: false, subOffersHistory: subOffersHistory, cell: cell)
            }
        } else {
            if (subOffersHistory.isAcceptedByBuyer ?? false) {
                self.setContainer(isLeft: false, subOffersHistory: subOffersHistory, cell: cell, isCancelButtonRequired: indexPath.row == subOfferHistory.count - 1)
                self.setAcceptedUI(isLeft: false, subOffersHistory: subOffersHistory, cell: cell)
            } else {
                self.setContainer(isLeft: true, subOffersHistory: subOffersHistory, cell: cell, isCancelButtonRequired: indexPath.row == subOfferHistory.count - 1)
                self.setAcceptedUI(isLeft: true, subOffersHistory: subOffersHistory, cell: cell)
            }
        }
        
        cell.itemImageView.layer.borderWidth = 2.0
        cell.itemImageView.layer.masksToBounds = false
        cell.itemImageView.layer.borderColor = UIColor.white.cgColor
        cell.itemImageView.clipsToBounds = true
        cell.itemImageView.layer.cornerRadius = cell.itemImageView.frame.size.width/2
        
        cell.shadowView.layer.cornerRadius = cell.shadowView.frame.size.width/2
        cell.shadowView.layer.shadowColor = UIColor.black.cgColor
        cell.shadowView.alpha = CGFloat(0.50)
        cell.shadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        cell.shadowView.layer.shadowOpacity = 1
        
        cell.leftItemImageView.layer.borderWidth = 2.0
        cell.leftItemImageView.layer.masksToBounds = false
        cell.leftItemImageView.layer.borderColor = UIColor.white.cgColor
        cell.leftItemImageView.clipsToBounds = true
        cell.leftItemImageView.layer.cornerRadius = cell.leftItemImageView.frame.size.width/2
        
        cell.leftShadowView.layer.cornerRadius = cell.leftShadowView.frame.size.width/2
        cell.leftShadowView.layer.shadowColor = UIColor.black.cgColor
        cell.leftShadowView.alpha = CGFloat(0.50)
        cell.leftShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        cell.leftShadowView.layer.shadowOpacity = 1
        
        cell.cancelButton.tag = indexPath.row
        cell.cancelButton.addTarget(self, action: #selector(onCancelButtonAction(sender:)), for: .touchUpInside)
        
        cell.itemImageView.tag = indexPath.row
        let rightImageViewTap = UITapGestureRecognizer(target: self, action: #selector(onRightViewClickAction))
        cell.itemImageView.addGestureRecognizer(rightImageViewTap)
        cell.itemImageView.isUserInteractionEnabled = true
        
        cell.leftItemImageView.tag = indexPath.row
        let leftImageViewTap = UITapGestureRecognizer(target: self, action: #selector(onLeftViewClickAction))
        cell.leftItemImageView.addGestureRecognizer(leftImageViewTap)
        cell.leftItemImageView.isUserInteractionEnabled = true
        
        cell.usernameLabel.tag = indexPath.row
        let rightUserNameLabelTap = UITapGestureRecognizer(target: self, action: #selector(onRightViewClickAction))
        cell.usernameLabel.addGestureRecognizer(rightUserNameLabelTap)
        cell.usernameLabel.isUserInteractionEnabled = true
        
        cell.leftUsernameLabel.tag = indexPath.row
        let leftUserNameLabelTap = UITapGestureRecognizer(target: self, action: #selector(onLeftViewClickAction))
        cell.leftUsernameLabel.addGestureRecognizer(leftUserNameLabelTap)
        cell.leftUsernameLabel.isUserInteractionEnabled = true
        
        return cell
    }
    
    func setContainer(isLeft:Bool,subOffersHistory:SubOfferHistory,cell:OfferDetailTableViewCell,isCancelButtonRequired:Bool){
        let image : UIImage = UIImage(named: AppConstants.Images.DEFAULT_USER)!
        
        if isLeft{
            
            cell.leftMainView.isHidden = false
            cell.rightMainView.isHidden = true
            cell.leftUsernameLabel.text = subOffersHistory.sellerUsername
            
            if let imageUrl = subOffersHistory.sellerUserImage{
                cell.leftItemImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
            } else {
                cell.leftItemImageView.image =  image
            }
            
            self.showCancelButton(subOffersHistory:subOffersHistory,cell:cell, isCancelButtonRequired: isCancelButtonRequired)
            
        }else{
            
            cell.leftMainView.isHidden = true
            cell.rightMainView.isHidden = false
            
            cell.usernameLabel.text = subOffersHistory.buyerUsername
            
            if let imageUrl = subOffersHistory.buyerUserImage{
                cell.itemImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
            } else {
                cell.itemImageView.image =  image
            }
            
            self.showCancelButton(subOffersHistory:subOffersHistory,cell:cell, isCancelButtonRequired: isCancelButtonRequired)
            
        }
        
    }
    
    func showCancelButton(subOffersHistory:SubOfferHistory,cell:OfferDetailTableViewCell,isCancelButtonRequired:Bool){
        if isCancelButtonRequired{
            if self.isOfferCancelable ?? false{
                cell.cancelButton.isHidden = false
            }else{
                cell.leftCancelButton.isHidden = true
            }
        }else{
            cell.leftCancelButton.isHidden = true
        }
    }
    
    func setDeclinedUI(isLeft:Bool,subOffersHistory:SubOfferHistory,cell:OfferDetailTableViewCell){
        let offerTypeName:String = subOffersHistory.offerTypeName!
        let price = String(format:AppConstants.FORMAT_TWO_DECIMAL,subOffersHistory.declinedPrice!)
        if isLeft{
            cell.leftOfferedLabel.text = offerTypeName
            cell.leftPriceLabel.text = AppConstants.DOLLAR +  price
            cell.leftDateLabel.text = subOffersHistory.declinedDateFormatted
            cell.leftDeclinedView.isHidden = false
            cell.rightDeclinedView.isHidden = true
            cell.leftDeclinedLabel.text = subOffersHistory.declinedReason
            cell.leftMainView.backgroundColor = UIColor.cFFD1D1
            cell.leftHeaderView.backgroundColor = UIColor.cFFD1D1
            
        }else{
            cell.offeredLabel.text = offerTypeName
            cell.priceLabel.text = AppConstants.DOLLAR + price
            cell.dateLabel.text = subOffersHistory.declinedDateFormatted
            cell.leftDeclinedView.isHidden = true
            cell.rightDeclinedView.isHidden = false
            cell.rightDeclinedLabel.text = subOffersHistory.declinedReason
            cell.headerView.backgroundColor = UIColor.cFFD1D1
        }
        
    }
    
    func setAcceptedUI(isLeft:Bool,subOffersHistory:SubOfferHistory,cell:OfferDetailTableViewCell){
        let offerTypeName:String = subOffersHistory.offerTypeName!
        let price = String(format:AppConstants.FORMAT_TWO_DECIMAL,subOffersHistory.acceptedPrice!)
        
        if isLeft{
            cell.leftOfferedLabel.text = offerTypeName
            cell.leftPriceLabel.text = AppConstants.DOLLAR + price
            cell.leftDateLabel.text = subOffersHistory.acceptedDateFormatted
            
            cell.leftMainView.backgroundColor = UIColor.cDF6CE
            cell.leftHeaderView.backgroundColor = UIColor.cDF6CE
            
        }else{
            cell.offeredLabel.text = offerTypeName
            cell.priceLabel.text = AppConstants.DOLLAR + price
            cell.dateLabel.text = subOffersHistory.acceptedDateFormatted
            
            cell.leftMainView.backgroundColor = UIColor.cDF6CE
            cell.leftHeaderView.backgroundColor = UIColor.cDF6CE
        }
    }
    
    func setOfferUI(isLeft:Bool,subOffersHistory:SubOfferHistory,cell:OfferDetailTableViewCell){
        
        var offerString:String?
        if(isLeft) {
            offerString = AppConstants.COUNTER_OFFERED
        } else {
            offerString = AppConstants.OFFERED
        }
        
        let offerTypeName:String = offerString!
        let price = String(subOffersHistory.offerPrice!)
        
        if isLeft{
            cell.leftOfferedLabel.text = offerTypeName
            cell.leftPriceLabel.text = AppConstants.DOLLAR + price
            cell.leftDateLabel.text = subOffersHistory.offerDateFormatted
            
            cell.leftMainView.backgroundColor = UIColor.D0E8FF
            cell.leftHeaderView.backgroundColor = UIColor.D0E8FF
            
        }else{
            cell.offeredLabel.text = offerTypeName
            cell.priceLabel.text = AppConstants.DOLLAR + price
            cell.dateLabel.text = subOffersHistory.offerDateFormatted
            
            cell.headerView.backgroundColor = UIColor.FFDAF8
    
        }
    
    }
    
    @objc func onCancelButtonAction(sender:UIButton){
        let index = sender.tag
        onCancelButtonClick(subOfferHistory[index],index)
    }
    
    @objc func onLeftViewClickAction(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        onLeftViewClick(subOfferHistory[index])
    }
    
    @objc func onRightViewClickAction(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        onRightViewClick(subOfferHistory[index])
    }
    
}
