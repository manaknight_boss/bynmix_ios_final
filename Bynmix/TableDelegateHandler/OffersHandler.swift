import UIKit

class OffersHandler: NSObject,UITableViewDelegate,UITableViewDataSource  {
    
    let offersCell = "offersCell"
    var setting = [Setting]()
    var loadMoreDelegate: LoadMoreProtocol?
    var tableView:UITableView?
    var isLoadMoreRequired = true
    var TYPE:Int?
    var onViewButtonClick:((Setting) -> Void)!
    var onCounterButtonClick:((Setting) -> Void)!
    var onAcceptButtonClick:((Setting) -> Void)!
    var onDeclineButtonClick:((Setting) -> Void)!
    var onUserImageViewClick:((Setting) -> Void)!
    var onFormattedTextClick:((Setting) -> Void)!
    var heightAtIndexPath = NSMutableDictionary()
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let height = NSNumber(value: Float(cell.frame.size.height))
        heightAtIndexPath.setObject(height, forKey: indexPath as NSCopying)
        
        if isLoadMoreRequired {
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                self.tableView?.tableFooterView = spinner
                self.tableView?.tableFooterView?.isHidden = false
            }
        } else {
            self.tableView?.tableFooterView = nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return setting.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: offersCell, for: indexPath) as! OffersTableViewCell
        
        let settings = self.setting[indexPath.row] as Setting
        
        cell.timeLabel.textColor = UIColor.c434343
        cell.userImageView.layer.borderWidth = 2.0
        cell.userImageView.layer.masksToBounds = false
        cell.userImageView.layer.borderColor = UIColor.white.cgColor
        cell.userImageView.clipsToBounds = true
        cell.userImageView.layer.cornerRadius = 2
        
        cell.shadowView.layer.cornerRadius = 2
        cell.shadowView.layer.shadowColor = UIColor.black.cgColor
        cell.shadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        cell.shadowView.layer.shadowOpacity = 1
        cell.shadowView.alpha = CGFloat(0.50)
        
        cell.timeLabel.text = settings.notificationDateFormatted
        
        let image = UIImage(named:  AppConstants.Images.DEFAULT_OFFER_PLACEHOLDER)
        
        if let imageUrl = settings.listingImage{
            cell.userImageView.kf.setImage(with: URL(string: imageUrl), placeholder: image)
        }else{
            cell.userImageView.image = image
        }
        
        let htmlCSSString = "<style>" +
            "html *" +
            "{" +
            "font-size: \(12)pt !important;" +
            "font-family: Raleway-Regular !important;" +
        "}</style> \(self)"
        
        cell.highlightView.backgroundColor = UIColor.white
        let text = htmlCSSString + settings.formattedText!
        cell.descLabel.attributedText = text.html2Attributed
        
        if let imageUrl = settings.iconUrl{
            cell.statusImageView.kf.setImage(with: URL(string: imageUrl), placeholder: image)
        }else{
            cell.statusImageView.image = image
        }
        
        if (TYPE == AppConstants.TYPE_MY_OFFERS) {
            let sellerOffer = settings.isSellerOffer ?? false
            if (sellerOffer && settings.isActiveOffer!) {
                cell.viewButton.isHidden = true
                cell.buttonsView.isHidden = false
            } else {
                cell.viewButton.isHidden = false
                cell.buttonsView.isHidden = true
            }
        } else {
            if (settings.isActiveOffer!) {
                cell.viewButton.isHidden = true
                cell.buttonsView.isHidden = false
            } else {
                cell.viewButton.isHidden = false
                cell.buttonsView.isHidden = true
            }
        }
        
        cell.viewButton.tag = indexPath.row
        cell.viewButton.addTarget(self, action: #selector(onViewButtonAction), for: .touchUpInside)
        
        cell.counterButton.tag = indexPath.row
        cell.counterButton.addTarget(self, action: #selector(onCounterButtonAction), for: .touchUpInside)
        
        cell.acceptButton.tag = indexPath.row
        cell.acceptButton.addTarget(self, action: #selector(onAcceptButtonAction), for: .touchUpInside)
        
        cell.declineButton.tag = indexPath.row
        cell.declineButton.addTarget(self, action: #selector(onDeclineButtonAction), for: .touchUpInside)
        
        cell.userImageView.tag = indexPath.row
        let imageViewTap = UITapGestureRecognizer(target: self, action: #selector(onUserImageViewTap))
        cell.userImageView.addGestureRecognizer(imageViewTap)
        
        cell.descLabel.tag = indexPath.row
        let formattedTextTap = UITapGestureRecognizer(target: self, action: #selector(formattedTextClickAction))
        cell.descLabel.addGestureRecognizer(formattedTextTap)
        
        return cell
    }
    
    @objc func formattedTextClickAction(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        onFormattedTextClick(setting[index])
    }
    
    @objc func onUserImageViewTap(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        onUserImageViewClick(setting[index])
    }
    
    @objc func onViewButtonAction(sender:UIButton){
        let index = sender.tag
        onViewButtonClick(setting[index])
    }
    
    @objc func onCounterButtonAction(sender:UIButton){
        let index = sender.tag
        onCounterButtonClick(setting[index])
    }
    
    @objc func onAcceptButtonAction(sender:UIButton){
        let index = sender.tag
        onAcceptButtonClick(setting[index])
    }
    
    @objc func onDeclineButtonAction(sender:UIButton){
        let index = sender.tag
        onDeclineButtonClick(setting[index])
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView?.tableFooterView != nil {
            let height = scrollView.frame.size.height
            let contentYoffset = scrollView.contentOffset.y
            let distanceFromBottom = scrollView.contentSize.height - contentYoffset
            if distanceFromBottom < height {
                loadMoreDelegate?.onLoadMore()
            }
        }
    }
    
}
