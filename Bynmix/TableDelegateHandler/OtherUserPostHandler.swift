import UIKit
import ExpandableLabel

class OtherUserPostHandler: NSObject,UITableViewDataSource,UITableViewDelegate, ExpandableLabelDelegate {
    
    func willExpandLabel(_ label: ExpandableLabel) {
        tableView!.beginUpdates()
    }
    
    func didExpandLabel(_ label: ExpandableLabel) {
        
        let point = label.convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView!.indexPathForRow(at: point) as IndexPath? {
            var feed = self.feed[indexPath.row] as Feed
            feed.expandableLabelCollapsed! = false
        }
        tableView!.endUpdates()
    }
    
    func willCollapseLabel(_ label: ExpandableLabel) {
        tableView!.beginUpdates()
    }
    
    func didCollapseLabel(_ label: ExpandableLabel) {
        let point = label.convert(CGPoint.zero, to: tableView)
        if let indexPath = tableView!.indexPathForRow(at: point) as IndexPath? {
            var feed = self.feed[indexPath.row] as Feed
            feed.expandableLabelCollapsed! = true
        }
        tableView!.endUpdates()
    }
    
    var feed = [Feed]()
    var otherUserPostsCell = "OtherUserPostsCell"
    var itemClickHandler: ((Feed) -> Void)!
    var onItemImageViewClick:((Feed) -> Void)!
    var tableView: UITableView?
    var onHeartButtonClick:((Feed) -> Void)!
    var scrollDelegate: FeedScrollProtocol?
    var heightAtIndexPath = NSMutableDictionary()
    var loadMoreDelegate: LoadMoreProtocol?
    var isLoadMoreRequired = true
    var onTagsViewClick:((Int, Feed) -> Void)!
    var otherUserPostTableViewCell:OtherUserPostTableViewCell?
    var onDotClick:((Dots) -> Void)!
    var canRefresh = true
    var refreshOtherUserProfileDelegate:RefreshOtherUserProfileProtocol?
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        return refreshControl
    }()
    
//    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
//        
//        if let height = heightAtIndexPath.object(forKey: indexPath) as? NSNumber {
//            return CGFloat(height.floatValue)
//        } else {
//            return UITableView.automaticDimension
//        }
//    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        let label = UILabel(frame: CGRect.zero)
//        label.numberOfLines = 2
//        label.font = UIFont.fontSemiBold(size: 20)
//        label.textColor = UIColor.white
//        label.text = feed[indexPath.item].title
//        label.sizeToFit()
//
//        let expandableLabel = ExpandableLabel(frame: CGRect.zero)
//        expandableLabel.numberOfLines = 3
//        expandableLabel.font = UIFont.fontRegular(size: 11)
//        expandableLabel.textColor = UIColor.white
//        expandableLabel.text = feed[indexPath.item].description
//        expandableLabel.sizeToFit()
//
//        return 370 + label.frame.height + expandableLabel.frame.height
//    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let height = NSNumber(value: Float(cell.frame.size.height))
        heightAtIndexPath.setObject(height, forKey: indexPath as NSCopying)
        
        if isLoadMoreRequired {
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                self.tableView?.tableFooterView = spinner
                self.tableView?.tableFooterView?.isHidden = false
            }
        } else {
            self.tableView?.tableFooterView = nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return feed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: otherUserPostsCell, for: indexPath) as! OtherUserPostTableViewCell
        
        otherUserPostTableViewCell = cell
        
        cell.titleDescLabel.delegate = self
        let feed = self.feed[indexPath.row] as Feed
        
        cell.topView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        let type = feed.type
        let image : UIImage
        
        if feed.postType == AppConstants.VIDEO_POST {
            image =  UIImage(named: AppConstants.Images.ITEM_VIDEO_PLACEHOLDER)!
            
        }else {
            image =  UIImage(named: AppConstants.Images.ITEM_PLACEHOLDER)!
        }
        
        cell.titleLabel.text = feed.title
        cell.titleLabel.textAlignment = .left
        
        cell.middleView.frame = CGRect(x: 0, y: 0, width: cell.middleView.frame.width, height: cell.middleView.frame.width)
        cell.itemImageView.layer.zPosition = -1
        
        if let imageUrl = feed.photoUrl{
            cell.itemImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        }
        
        if type == AppConstants.POST && feed.postType == AppConstants.VIDEO_POST {
            cell.videoImageView.isHidden = false
            cell.videoImageView.image = UIImage(named: AppConstants.Images.VIDEO_ICON)
        }else{
            cell.videoImageView.isHidden = true
        }
        
        let likesCount =  feed.likesCount
        cell.likesShadowView.layer.shadowColor = UIColor.black.cgColor
        cell.likesShadowView.alpha = CGFloat(0.26)
        cell.likesShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        cell.likesShadowView.layer.shadowOpacity = 1
        
        cell.likesView.tag = indexPath.row
        
        let likesTap = UITapGestureRecognizer(target: self, action: #selector(likesClick))
        cell.likesView.addGestureRecognizer(likesTap)
        
        cell.itemImageView.tag = indexPath.row
        let itemImageTap = UITapGestureRecognizer(target: self, action: #selector(itemImageViewClick))
        cell.itemImageView.addGestureRecognizer(itemImageTap)
        
        cell.heartButton.tag = indexPath.row
        cell.heartButton.addTarget(self, action: #selector(heartClick), for: .touchDown)
        
        if likesCount == 0 {
            cell.likesView.isHidden = true
            cell.likesShadowView.isHidden = true
            
        }else if likesCount == 1{
            cell.likesView.isHidden = false
            cell.likesShadowView.isHidden = false
            cell.likesLabel.text = "\(likesCount ?? 0)"
            cell.likesStringLabel.text = AppConstants.Strings.LIKE
        }else{
            cell.likesView.isHidden = false
            cell.likesShadowView.isHidden = false
            cell.likesLabel.text = "\(likesCount ?? 0)"
            cell.likesStringLabel.text = AppConstants.Strings.LIKES
        }
        
        cell.likesLabel.text = "\(feed.likesCount ?? 0)"
        
        if feed.userLiked == false {
            cell.heartButton.setImage(UIImage(named: AppConstants.Images.PROFILE_HEART), for: .normal)
        } else {
            cell.heartButton.setImage(UIImage(named: AppConstants.Images.PROFILE_HEART_SELECTED), for: .normal)
        }
        
        if feed.hasPostDots == true {
            cell.dotsButton.isHidden = false
        }else {
            cell.dotsButton.isHidden = true
        }
        
        cell.dotsButton.tag = indexPath.row
        cell.dotsButton.addTarget(self, action: #selector(itemDotsButtonClick), for: .touchUpInside)
        
        if (feed.description?.count) ?? 0 > 140 {
            //cell.titleDescLabel.setLessLinkWith(lessLink: AppConstants.Strings.READ_LESS, attributes: [NSAttributedString.Key.foregroundColor:UIColor.white, NSAttributedString.Key.font: UIFont.fontSemiBold(size: 11)], position: nil)
            cell.titleDescLabel.collapsedAttributedLink = NSAttributedString(string: AppConstants.Strings.READ_MORE, attributes: [NSAttributedString.Key.foregroundColor:UIColor.white, NSAttributedString.Key.font: UIFont.fontSemiBold(size: 11)])
            //cell.titleDescLabel.ellipsis = NSAttributedString(string: "...")
            cell.titleDescLabel.expandedAttributedLink = NSAttributedString(string:AppConstants.Strings.READ_LESS , attributes: [NSAttributedString.Key.foregroundColor:UIColor.white, NSAttributedString.Key.font: UIFont.fontSemiBold(size: 11)])
            cell.titleDescLabel.shouldCollapse = true
            cell.titleDescLabel.textReplacementType = .character
            cell.titleDescLabel.numberOfLines = 3
            //cell.titleDescLabel.collapsed = true
            cell.titleDescLabel.collapsed = !feed.expandableLabelCollapsed!
        }else{
            cell.titleDescLabel.numberOfLines = 0
        }
        cell.titleDescLabel.text = feed.description
    
        if feed.containDots!{
            for view in (cell.itemImageView.subviews) {
                view.removeFromSuperview()
            }
            for i in 0..<feed.postDots!.count{
                self.setDot(index: i, dots: feed.postDots!)
            }
            cell.dotsButton.setTitle(AppConstants.Strings.HIDE_TAG_PRODUCTS, for: .normal)
        }else{
            for view in (cell.itemImageView.subviews) {
                view.removeFromSuperview()
            }
            cell.dotsButton.setTitle(AppConstants.Strings.TAG_PRODUCTS, for: .normal)
            self.feed[indexPath.row].containDots = false
        }
        cell.layoutIfNeeded()
        
        return cell
    }
    
    @objc func itemDotsLabelClick(sender:UITapGestureRecognizer) {
        let index = sender.view!.tag
        let position = IndexPath(row: index, section: 0)
        self.tableView?.reloadRows(at: [position], with: UITableView.RowAnimation.none)
    }
    
    @objc func heartClick(sender: UIButton){
        let index = sender.tag
        var likesCount =  feed[index].likesCount
        var userLiked = feed[index].userLiked
        userLiked = !userLiked!
        if userLiked == false {
            likesCount! -= 1
        }else{
            likesCount! += 1
        }
        feed[index].likesCount = likesCount
        feed[index].userLiked = userLiked
        onHeartButtonClick(feed[index])
        let position = IndexPath(row: index, section: 0)
        self.tableView?.reloadRows(at: [position], with: UITableView.RowAnimation.none)
    }
    
    @objc func likesClick(sender: UITapGestureRecognizer){
        let index = sender.view!.tag
        itemClickHandler(feed[index])
    }
    
    @objc func itemImageViewClick(sender: UITapGestureRecognizer){
        let index = sender.view!.tag
        onItemImageViewClick(feed[index])
    }
    
    @objc func itemDotsButtonClick(sender:UIButton) {
        let index = sender.tag
        let position = IndexPath(row: index, section: 0)
        if !feed[index].containDots!{
            onTagsViewClick(index, feed[index])
        }
        else{
            feed[index].containDots = false
            tableView?.reloadRows(at: [position], with: .none)
        }
    }
    
    func setDot(index: Int, dots: [Dots]) {
        var dotImage: UIImage
        if dots[index].hexColor == AppConstants.DOT_COLOR_RED{
            dotImage = UIImage(named: AppConstants.Images.VIEW_RED_ICON)!
        }else if dots[index].hexColor == AppConstants.DOT_COLOR_YELLOW{
            dotImage = UIImage(named: AppConstants.Images.VIEW_YELLOW_ICON)!
        }else if dots[index].hexColor == AppConstants.DOT_COLOR_BLUE{
            dotImage = UIImage(named: AppConstants.Images.VIEW_BLUE_ICON)!
        }else if dots[index].hexColor == AppConstants.DOT_COLOR_GREEN{
            dotImage = UIImage(named: AppConstants.Images.VIEW_GREEN_ICON)!
        }else {
            dotImage = UIImage(named: AppConstants.Images.VIEW_PURPLE_ICON)!
        }
        
        let imageWidth = Double((self.otherUserPostTableViewCell?.itemImageView.frame.width)!)
        let imageHeight = Double((self.otherUserPostTableViewCell?.itemImageView.frame.height)!)
        let x = dots[index].xCoordinate! * imageWidth
        let y = dots[index].yCoordinate! * imageHeight
        
        let dotImageWidth = Double((dotImage.size.width))
        let dotImageHeight = Double((dotImage.size.height))
        let dot = UIImageView(frame: CGRect(x: x - (dotImageWidth/2), y: y - (dotImageHeight), width: dotImageWidth, height: dotImageHeight))
        dot.image = dotImage
        dot.contentMode = .scaleAspectFit
        self.otherUserPostTableViewCell?.itemImageView.addSubview(dot)
        
        let dotTap = UITapGestureRecognizer(target: self, action: #selector(self.onDotTapDetected))
        dot.tag = dots[index].postDotId!
        dot.isUserInteractionEnabled = true
        dot.addGestureRecognizer(dotTap)
    }
    
    @objc func onDotTapDetected(sender: UITapGestureRecognizer){
        let id = (sender.view?.tag)!
        self.onDotClick(self.getDotById(id: id))
    }
    
    func getDotById(id: Int) -> Dots {
        for post in feed {
            if post.containDots! {
                let dots = post.postDots
                for dot in dots! {
                    if dot.postDotId == id {
                        return dot
                    }
                }
            }
        }
        return Dots()
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        if distanceFromBottom >= height {
            scrollDelegate?.onScroll(scrollValue: scrollView.contentOffset.y)
        }
        if self.tableView?.tableFooterView != nil {
            if distanceFromBottom < height {
                loadMoreDelegate?.onLoadMore()
            }
        }
        
        if scrollView.contentOffset.y < -55 { //change 100 to whatever you want
            
            if canRefresh && !(self.tableView?.refreshControl?.isRefreshing ?? false) {
                
                self.canRefresh = false
                self.refreshControl.beginRefreshing()
                self.refreshOtherUserProfileDelegate?.refreshOtherUserProfile()
            }
        }else if scrollView.contentOffset.y <= -40 {
            
            self.canRefresh = true
        }
    }
    
}
