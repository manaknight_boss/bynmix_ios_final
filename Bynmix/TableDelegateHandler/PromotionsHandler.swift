import UIKit

class PromotionsHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    let promotionsCell = "inAppNotificationCell"
    var promotions = [Promotions]()
    var onCancelButtonClick:((Promotions,Int) -> Void)!
    var tableView:UITableView?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return promotions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: promotionsCell, for: indexPath) as! InAppNotificationTableViewCell
        let promotion = promotions[indexPath.row] as Promotions
        
        let image = UIImage(named: AppConstants.Images.TWO_X_STAR)
        
        let htmlCSSString = "<style>" +
            "html *" +
            "{" +
            "font-size: \(9)pt !important;" +
            "font-family: Raleway-Regular !important;" +
        "}</style> \(self)"
        
        let text = htmlCSSString + promotion.bannerText!
        cell.notificationLabel.attributedText = text.html2Attributed
        cell.notificationLabel.textColor  = UIColor.white
        
        if let imageUrl = promotion.iconUrl{
            cell.notificationImageView.kf.setImage(with: URL(string: imageUrl), placeholder: image)
        }else{
            cell.notificationImageView.image = image
        }
        
        if (promotion.promotionTypeId! > 0 && promotion.promotionTypeId! < 4) {
            cell.closeButton.backgroundColor = UIColor.c006483
            cell.notificationBackgroundView.backgroundColor = UIColor.c0089B3
            
        } else if (promotion.promotionTypeId! > 3 && promotion.promotionTypeId! < 6) {
            cell.closeButton.backgroundColor = UIColor.cB05600
            cell.notificationBackgroundView.backgroundColor = UIColor.cE26E00
        } else if (promotion.promotionTypeId! > 5 && promotion.promotionTypeId! < 8) {
            cell.closeButton.backgroundColor = UIColor.c6B8652
            cell.notificationBackgroundView.backgroundColor = UIColor.c84A666
        }
        cell.closeButton.tag = indexPath.row
        cell.closeButton.addTarget(self, action: #selector(onCancelButtonClickAction), for: .touchUpInside)
        cell.layoutIfNeeded()
        
        return cell
    }
    
    @objc func onCancelButtonClickAction(sender:UIButton){
        let index = sender.tag
        onCancelButtonClick(promotions[index],index)
    }
    

}
