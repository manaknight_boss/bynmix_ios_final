import UIKit

class ReplaceCardHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    var card = [Card]()
    let replaceCell = "replaceCardCell"
    var onSelectButtonClick: ((Card) -> Void)!
    var tableView:UITableView?
    var selectedIndex = -1
    var replaceCardTableViewCell: ReplaceCardTableViewCell?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return card.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: replaceCell, for: indexPath) as! ReplaceCardTableViewCell
        replaceCardTableViewCell = cell
        let cards = card[indexPath.row]
        
        let image = UIImage(named: AppConstants.Images.DEFAULT_CARD_ICON)
        
        if let imageUrl = cards.brandImageUrl{
            cell.brandImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        }
        
        cell.brandNameWithCardNumberLabel.text = cards.brand! + " " + AppConstants.Strings.ENDING_IN + " " +  cards.lastFour!
        cell.cardHolderNameLabel.text = cards.nameOnCard
        cell.expirationDateLabel.text = AppConstants.EXPIRES + " " + String(cards.expirationMonth!) + AppConstants.FORWARD_SLASH + String(cards.expirationYear!)
        
        let text:String = cards.brand! + " " + AppConstants.Strings.ENDING_IN + " " +  cards.lastFour!
        let textBrand:String = cards.brand!
        let textLastFour:String = cards.lastFour!
        let range = (text as NSString).range(of: textLastFour)
        let rangeLastFour = (text as NSString).range(of: textBrand)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: range)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontBold(size: 16), range: rangeLastFour)
        
        cell.brandNameWithCardNumberLabel.attributedText = attributedString
        cell.SelectButton.tag = indexPath.row
        cell.SelectButton.addTarget(self, action: #selector(onSelectButtonAction), for: .touchUpInside)
        
        return cell
    }
    
    @objc func onSelectButtonAction(sender:UIButton){
        let index = sender.tag
        var indexPath = IndexPath(row: index, section: 0)
        var cell = self.tableView!.cellForRow(at: indexPath) as! ReplaceCardTableViewCell
        if self.selectedIndex == -1 {
            self.select(index: index, cell: cell)
        }
        else
        {
            indexPath = IndexPath(row: self.selectedIndex, section: 0)
            cell = self.tableView!.cellForRow(at: indexPath) as! ReplaceCardTableViewCell
            self.unselect(index: index, cell: cell)
            indexPath = IndexPath(row: index, section: 0)
            cell = self.tableView!.cellForRow(at: indexPath) as! ReplaceCardTableViewCell
            self.select(index: index, cell: cell)
        }
    }
    func select(index : Int,cell: ReplaceCardTableViewCell)
    {
        cell.SelectButton.setImage( UIImage(named: AppConstants.Images.TICK_ICON), for: .normal)
        self.selectedIndex = index
        onSelectButtonClick(card[index])
    }
    
    func unselect(index : Int,cell: ReplaceCardTableViewCell)
    {
        cell.SelectButton.setImage( UIImage(named: AppConstants.Images.LARGE_CHECKBOX), for: .normal)
    }
    
}
