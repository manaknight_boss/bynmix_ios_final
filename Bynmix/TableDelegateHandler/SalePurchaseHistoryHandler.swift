import UIKit

class SalePurchaseHistoryHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    var saleDetail = [SalesDetail]()
    var purchaseDetail = [PurchaseDetail]()
    var TYPE:Int?
    var salePurchaseHistoryCell = "salePurchaseHistoryCell"
    var isLoadMoreRequired = true
    var scrollDelegate: FeedScrollProtocol?
    var loadMoreDelegate: LoadMoreProtocol?
    var onSaleDetailClick:((SalesDetail) -> Void)!
    var onPurchaseDetailClick:((PurchaseDetail) -> Void)!
    var onSaleMenuClick:((SalesDetail, CGRect) -> Void)!
    var onPurchaseMenuClick:((PurchaseDetail, CGRect) -> Void)!
    var tableView: UITableView?
    var salePurchaseHistoryTableViewCell:SalePurchaseHistoryTableViewCell?
    var viewController: UIViewController?
    var heightAtIndexPath = NSMutableDictionary()
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
           
           let height = NSNumber(value: Float(cell.frame.size.height))
           heightAtIndexPath.setObject(height, forKey: indexPath as NSCopying)
           
           if isLoadMoreRequired {
               let lastSectionIndex = tableView.numberOfSections - 1
               let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
               if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
                   let spinner = UIActivityIndicatorView(style: .gray)
                   spinner.startAnimating()
                   spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                   
                   self.tableView?.tableFooterView = spinner
                   self.tableView?.tableFooterView?.isHidden = false
               }
           } else {
               self.tableView?.tableFooterView = nil
           }
       }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        if TYPE == AppConstants.TYPE_PURCHASE{
            return purchaseDetail.count
        }else{
            return saleDetail.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: salePurchaseHistoryCell, for: indexPath) as! SalePurchaseHistoryTableViewCell
        
        salePurchaseHistoryTableViewCell = cell
        
        cell.shadowImageView.layer.cornerRadius = 2
        cell.shadowImageView.layer.shadowColor = UIColor.black.cgColor
        cell.shadowImageView.alpha = CGFloat(0.26)
        cell.shadowImageView.layer.shadowOffset = CGSize(width: 0,height: 1)
        cell.shadowImageView.layer.shadowOpacity = 1
        cell.shadowImageView.layer.zPosition = -1
        
        cell.userImageView.layer.masksToBounds = true
        cell.userImageView.layer.borderWidth = 1.5
        cell.userImageView.layer.borderColor = UIColor.white.cgColor
        cell.userImageView.layer.cornerRadius = 2
        
        cell.topView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        cell.topView.backgroundColor = UIColor.cD53A95
        let image =  UIImage(named: AppConstants.Images.ITEM_PLACEHOLDER)!
        var purchase:PurchaseDetail?
        var sale:SalesDetail?
        
        cell.promotionPriceLabel.isHidden = true
        cell.promotionPointsLabel.isHidden = true
        var newDate = "", previousDate = ""
        if TYPE == AppConstants.TYPE_PURCHASE{
            purchase = purchaseDetail[indexPath.row] as PurchaseDetail
            cell.userImageView.tag = indexPath.row
            let tap = UITapGestureRecognizer(target: self, action: #selector(onPurchaseDetailClickPage))
            cell.userImageView.addGestureRecognizer(tap)
            cell.userImageView.isUserInteractionEnabled = true
            
            cell.detailView.tag = indexPath.row
            let detailTap = UITapGestureRecognizer(target: self, action: #selector(onPurchaseDetailClickPage))
            cell.detailView.addGestureRecognizer(detailTap)
            
            cell.menuButton.tag = indexPath.row
            cell.menuButton.addTarget(self, action: #selector(onPurchaseMenuClickButton), for: .touchUpInside)
            cell.titleLabel.text = purchase!.listingTitle
            cell.soldDateLabel.text = AppConstants.PURCHASED + purchase!.purchaseDateFormatted!
            if let imageUrl = purchase!.listingImageUrl{
                cell.userImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
            }
            cell.originalPriceLabel.text = AppConstants.DOLLAR + String(Int(purchase!.purchasePrice?.rounded() ?? 0))
            cell.shippingPriceLabel.text = AppConstants.DOLLAR + String(Int(purchase!.shippingPrice?.rounded() ?? 0))
            
            if ((purchase!.promotions?.count) ?? 0 > 0) {
                let count = purchase!.promotions?.count
                if (count! > 0) {
                    let promotion:[String] = purchase!.promotionsFormatted!
                    for i in 0..<count! {
                        if (purchase!.promotionsFormatted![i].lowercased().contains(String(AppConstants.Strings.PROMOTION_PRICE).lowercased())){
                            cell.promotionPriceLabel.isHidden = false
                            cell.promotionPriceLabel.text = promotion[i]
                        } else {
                            cell.promotionPointsLabel.isHidden = false
                            cell.promotionPointsLabel.text = promotion[i]
                        }
                    }
                } else {
                    cell.promotionPriceLabel.isHidden = true
                    cell.promotionPointsLabel.isHidden = true
                }
            }
            
            if (purchase!.pointsStatusId! > 0) {
                cell.pointsEarnedLabel.isHidden = false
                if (purchase!.pointsStatusId! == 1) {
                    cell.pointsEarnedLabel.textColor = UIColor.c007EE2
                    cell.pointsEarnedLabel.text = AppConstants.Strings.POINTS_EARNED + String(purchase?.points ?? 0) + AppConstants.Strings.PENDING_FEEDBACK_LEFT
                } else if (purchase!.pointsStatusId! == 2) {
                    cell.pointsEarnedLabel.textColor = UIColor.c0AA90E
                    cell.pointsEarnedLabel.text = AppConstants.Strings.POINTS_EARNED + String(purchase?.points ?? 0) + AppConstants.Strings.POINTS_TEXT
                } else if (purchase!.pointsStatusId! == 3) {
                    cell.pointsEarnedLabel.textColor = UIColor.black
                    cell.pointsEarnedLabel.text = AppConstants.Strings.POINTS_RETRACTED + String(purchase?.points ?? 0) + AppConstants.Strings.POINTS_TEXT
                } else if (purchase!.pointsStatusId! == 4) {
                    cell.pointsEarnedLabel.textColor = UIColor.black
                    cell.pointsEarnedLabel.text = AppConstants.Strings.POINTS_PENDING + String(purchase?.points ?? 0) + AppConstants.Strings.PENDING_DISPUTE
                } else {
                    cell.pointsEarnedLabel.isHidden = true
                }
            } else {
                cell.pointsEarnedLabel.isHidden = true
            }
            
            let purchaseYear = purchase!.purchaseYear!
            let purchaseMonth = purchase!.purchaseMonth!
            
            newDate = purchaseMonth + " " + purchaseYear
            if indexPath.row != 0 {
                let previousPurchase = purchaseDetail[indexPath.row - 1] as PurchaseDetail
                previousDate = previousPurchase.purchaseMonth! + " " + previousPurchase.purchaseYear!
                
            }
            print(newDate)
            let totalPrice = String(Int(purchase!.purchasePrice!.rounded() + purchase!.shippingPrice!.rounded()))
            cell.totalPriceLabel.text = AppConstants.DOLLAR + totalPrice
        }else{
            sale = saleDetail[indexPath.row] as SalesDetail
            cell.userImageView.tag = indexPath.row
            let tap = UITapGestureRecognizer(target: self, action: #selector(onSaleDetailClickPage))
            cell.userImageView.addGestureRecognizer(tap)
            cell.userImageView.isUserInteractionEnabled = true
            
            cell.detailView.tag = indexPath.row
            let detailTap = UITapGestureRecognizer(target: self, action: #selector(onSaleDetailClickPage))
            cell.detailView.addGestureRecognizer(detailTap)
            
            cell.menuButton.tag = indexPath.row
            cell.menuButton.addTarget(self, action: #selector(onSaleMenuClickButton), for: .touchUpInside)
            
            cell.titleLabel.text = sale!.listingTitle
            cell.soldDateLabel.text = AppConstants.SOLD_ON + sale!.saleDateFormatted!
            
            if let imageUrl = sale!.listingImageUrl{
                cell.userImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
            }
            cell.originalPriceLabel.text = AppConstants.DOLLAR + String(Int(sale!.salePrice?.rounded() ?? 0))
            cell.shippingPriceLabel.text = AppConstants.DOLLAR + String(Int(sale!.shippingPrice?.rounded() ?? 0))
            
            let totalPrice = String(Int(sale!.salePrice!.rounded() + sale!.shippingPrice!.rounded()))
            cell.totalPriceLabel.text = AppConstants.DOLLAR + totalPrice
            
            if ((sale!.promotions?.count) ?? 0 > 0) {
                let count = sale!.promotions?.count
                if (count! > 0) {
                    let promotion:[String] = sale!.promotionsFormatted!
                    for i in 0..<count! {
                        if (sale!.promotionsFormatted![i].lowercased().contains(String(AppConstants.Strings.PROMOTION_PRICE).lowercased())){
                            cell.promotionPriceLabel.isHidden = false
                            cell.promotionPriceLabel.text = promotion[i]
                        } else {
                            cell.promotionPointsLabel.isHidden = false
                            cell.promotionPointsLabel.text = promotion[i]
                        }
                    }
                } else {
                    cell.promotionPriceLabel.isHidden = true
                    cell.promotionPointsLabel.isHidden = true
                }
            }
            
            if (sale!.pointsStatusId! > 0) {
                cell.pointsEarnedLabel.isHidden = false
                if (sale!.pointsStatusId! == 1) {
                    cell.pointsEarnedLabel.textColor = UIColor.c007EE2
                    cell.pointsEarnedLabel.text = AppConstants.Strings.POINTS_EARNED + String(sale?.points ?? 0) + AppConstants.Strings.PENDING_FEEDBACK_LEFT
                } else if (sale!.pointsStatusId! == 2) {
                    cell.pointsEarnedLabel.textColor = UIColor.c0AA90E
                    cell.pointsEarnedLabel.text = AppConstants.Strings.POINTS_EARNED + String(sale?.points ?? 0) + AppConstants.Strings.POINTS_TEXT
                } else if (sale!.pointsStatusId! == 3) {
                    cell.pointsEarnedLabel.textColor = UIColor.black
                    cell.pointsEarnedLabel.text = AppConstants.Strings.POINTS_RETRACTED + String(sale?.points ?? 0) + AppConstants.Strings.POINTS_TEXT
                } else if (sale!.pointsStatusId! == 4) {
                    cell.pointsEarnedLabel.textColor = UIColor.black
                    cell.pointsEarnedLabel.text = AppConstants.Strings.POINTS_PENDING + String(sale?.points ?? 0) + AppConstants.Strings.PENDING_DISPUTE
                } else {
                    cell.pointsEarnedLabel.isHidden = true
                }
            } else {
                cell.pointsEarnedLabel.isHidden = true
            }
            
            let saleYear = sale!.saleYear!
            let saleMonth = sale!.saleMonth!
            newDate = saleMonth + " " + saleYear
            if indexPath.row != 0 {
                let previousSale = saleDetail[indexPath.row - 1] as SalesDetail
                previousDate = previousSale.saleMonth! + " " + previousSale.saleYear!
                
            }
        }
        
        if indexPath.row == 0{
            cell.postDateLabel.text = newDate
            cell.postDateView.isHidden = false
        } else {
            if newDate == previousDate {
                cell.postDateView.isHidden = true
            } else {
                cell.postDateLabel.text = newDate
                cell.postDateView.isHidden = false
            }
            
        }
        
        return cell
    }
    
    @objc func onPurchaseDetailClickPage(sender:UITapGestureRecognizer){
        let index = sender.view!.tag
        onPurchaseDetailClick(purchaseDetail[index])
    }
    
    @objc func onSaleDetailClickPage(sender:UITapGestureRecognizer){
        let index = sender.view!.tag
        onSaleDetailClick(saleDetail[index])
    }
    
    @objc func onSaleMenuClickButton(sender:UIButton, forEvent event: UIEvent){
        let index = sender.tag
        let frame = sender.superview!.convert(sender.frame, to: viewController?.view) as CGRect
        onSaleMenuClick(saleDetail[index], frame)
        
    }
    
    @objc func onPurchaseMenuClickButton(sender:UIButton, forEvent event: UIEvent){
        let index = sender.tag
        let frame = sender.superview!.convert(sender.frame, to: viewController?.view)
        onPurchaseMenuClick(purchaseDetail[index], frame)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView?.tableFooterView != nil {
            let height = scrollView.frame.size.height
            let contentYoffset = scrollView.contentOffset.y
            let distanceFromBottom = scrollView.contentSize.height - contentYoffset
            if distanceFromBottom < height {
                loadMoreDelegate?.onLoadMore()
            }
        }
    }
    
    
}
