import UIKit

class SelectExistingAddressHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    var shippingAddress: Address? = nil
    var returnAddress: Address? = nil
    var otherAddresses = [Address]()
    let existingAddressCell = "ExistingAddressCell"
    var onAddressAddButtonClick: ((Address) -> Void)!
    var selectExistingAddressTableViewCell : SelectExistingAddressTableViewCell?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if shippingAddress != nil && returnAddress != nil {
            if section == 0 {
                return 1
            } else if section == 1 {
                return 1
            } else {
                return otherAddresses.count
            }
        } else {
            if shippingAddress == nil && returnAddress == nil{
                return otherAddresses.count
            } else {
                if section == 0 {
                    return 1
                } else {
                    return otherAddresses.count
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if otherAddresses.count > 0 {
            return getSectionCount() + 1
        } else {
            return getSectionCount()
        }
    }
    
    func getSectionCount() -> Int {
        if shippingAddress != nil && returnAddress != nil {
            return 2
        } else {
            if shippingAddress == nil && returnAddress == nil {
                return 0
            }
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            if shippingAddress != nil {
                return AppConstants.Strings.PREFFERED_SHIPPING_ADDRESS_TILE
            } else if shippingAddress == nil && returnAddress == nil {
                return AppConstants.Strings.OTHER_ADDRESS_TILE
            } else {
                return AppConstants.Strings.PREFFERED_RETURN_ADDRESS_TILE
            }
        } else if section == 1 {
            if shippingAddress != nil && returnAddress != nil {
                return AppConstants.Strings.PREFFERED_RETURN_ADDRESS_TILE
            } else {
                return AppConstants.Strings.OTHER_ADDRESS_TILE
            }
            
        } else {
            return AppConstants.Strings.OTHER_ADDRESS_TILE
        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.c414141
        header.textLabel?.font = UIFont.fontMedium(size: 18)
        header.backgroundColor = UIColor.cE8E8E8
        header.tintColor = UIColor.cE8E8E8
        header.textLabel?.text = header.textLabel!.text!.capitalized
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: existingAddressCell, for: indexPath) as! SelectExistingAddressTableViewCell
        
        selectExistingAddressTableViewCell = cell
        cell.footerView.backgroundColor = UIColor.cE8E8E8
        if indexPath.section == 0 {
            if shippingAddress != nil {
                setAddressToCell(address: shippingAddress!, type: -1, cell: cell)
                cell.selectButton.addTarget(self, action: #selector(onExistingAddressButtonTap), for: .touchUpInside)
            } else if shippingAddress == nil && returnAddress == nil {
                setAddressToCell(address: self.otherAddresses[indexPath.row], type: indexPath.row, cell: cell)
            } else {
                setAddressToCell(address: returnAddress!, type: -2, cell: cell)
                cell.selectButton.addTarget(self, action: #selector(onExistingAddressButtonTap), for: .touchUpInside)
            }
            cell.footerViewHeight.constant = 0
        } else if indexPath.section == 1 {
            if shippingAddress != nil && returnAddress != nil {
                setAddressToCell(address: returnAddress!, type: -2, cell: cell)
                cell.selectButton.addTarget(self, action: #selector(onExistingAddressButtonTap), for: .touchUpInside)
            } else {
                setAddressToCell(address: self.otherAddresses[indexPath.row], type: indexPath.row, cell: cell)
            }
            cell.footerViewHeight.constant = 0
        } else {
            setAddressToCell(address: self.otherAddresses[indexPath.row], type: indexPath.row, cell: cell)
            cell.footerViewHeight.constant = 12
        }

        return cell
    }
    
    func setAddressToCell(address: Address,type: Int, cell: SelectExistingAddressTableViewCell) {
        cell.selectButton.tag = type
        cell.nameLabel.text = address.fullName
        cell.addressOneLabel.text = address.addressOne
        cell.addressTwoLabel.text = address.addressTwo
        cell.addressThreeLabel.text = (address.city! + " " + address.stateAbbreviation! + ", " + address.zipCode!)
        cell.selectButton.addTarget(self, action: #selector(onExistingAddressButtonTap), for: .touchUpInside)
        cell.addressesView.isHidden = false
    }
    
    @objc func onExistingAddressButtonTap(sender:UIButton){
        let index = sender.tag
        if index == -1 {
            onAddressAddButtonClick(shippingAddress!)
        } else if index == -2 {
            onAddressAddButtonClick(returnAddress!)
        } else {
            onAddressAddButtonClick(otherAddresses[index])
        }
    }

}
