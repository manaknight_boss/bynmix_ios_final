import UIKit

class SelectExistingCardHandler: NSObject,UITableViewDelegate,UITableViewDataSource {

    var card = [Card]()
    let selectExistingCardCell = "selectExistingCardCell"
    var onAddressClick: ((Card) -> Void)!
    var selectedIndex = -1
    var defaultCard:Card?
    
   func numberOfSections(in tableView: UITableView) -> Int {
       return 1
   }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor.cE8E8E8
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.c414141
        header.textLabel?.font = UIFont.fontMedium(size: 18)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return AppConstants.Strings.OTHER_PAYMENT_TYPES
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if card.count > 0 && defaultCard != nil{
            return 45
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return card.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: selectExistingCardCell, for: indexPath) as! SelectExistingCardTableViewCell
        let cards = card[indexPath.row]
        
        let image = UIImage(named: AppConstants.Images.DEFAULT_CARD_ICON)
        
        if let imageUrl = cards.brandImageUrl{
            cell.brandImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        }
        
        cell.brandNameWithCardNumberLabel.text = cards.brand! + " " + AppConstants.Strings.ENDING_IN + " " +  cards.lastFour!
        cell.cardHolderNameLabel.text = cards.nameOnCard
        cell.expirationDateLabel.text = AppConstants.EXPIRES + " " + String(cards.expirationMonth!) + AppConstants.FORWARD_SLASH + String(cards.expirationYear!)
        
        let text:String = cards.brand! + " " + AppConstants.Strings.ENDING_IN + " " +  cards.lastFour!
        let textBrand:String = cards.brand!
        let textLastFour:String = cards.lastFour!
        let range = (text as NSString).range(of: textLastFour)
        let rangeLastFour = (text as NSString).range(of: textBrand)
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 16), range: range)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontBold(size: 16), range: rangeLastFour)
        
        cell.brandNameWithCardNumberLabel.attributedText = attributedString
        cell.SelectButton.tag = indexPath.row
        let addresstap = UITapGestureRecognizer(target: self, action: #selector(onAddressAction))
        cell.SelectButton.addGestureRecognizer(addresstap)
        
        return cell
    }
    
    @objc func onAddressAction(sender:UITapGestureRecognizer){
        let index = sender.view!.tag
        var selectedCard = self.card[index]
        selectedCard.sourceToken = selectedCard.stripeCardId
        selectedCard.isNewAddress = false
        selectedCard.settingAddressId = selectedCard.addressId
        onAddressClick(selectedCard)
    }
    
    
}
