import UIKit

class SelectListingHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    var listingTitles = [ListingTitles]()
    let listingCell = "selectListing"
    var isLoadMoreRequired = true
    var tableView: UITableView?
    var loadMoreDelegate: LoadMoreProtocol?
    var onListingClick: ((ListingTitles) -> Void)!
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView?.tableFooterView != nil {
            let height = scrollView.frame.size.height
            let contentYoffset = scrollView.contentOffset.y
            let distanceFromBottom = scrollView.contentSize.height - contentYoffset
            if distanceFromBottom < height {
                loadMoreDelegate?.onLoadMore()
            }
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
          if isLoadMoreRequired {
              let lastSectionIndex = tableView.numberOfSections - 1
              let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
              if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
                  let spinner = UIActivityIndicatorView(style: .gray)
                  spinner.startAnimating()
                  spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                  
                  self.tableView?.tableFooterView = spinner
                  self.tableView?.tableFooterView?.isHidden = false
              }
          } else {
              self.tableView?.tableFooterView = nil
          }
          
      }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return listingTitles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: listingCell, for: indexPath) as! SelectListingTableViewCell
        
        let listingTitle = self.listingTitles[indexPath.row] as ListingTitles
        
        let image = UIImage(named: AppConstants.Images.DEFAULT_USER)
        if let imageUrl = listingTitle.imageUrl{
            cell.listingImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        }
        
        cell.listingLabel.text = listingTitle.title
        
        cell.listingImageView.layer.borderWidth = 2.0
        cell.listingImageView.layer.masksToBounds = false
        cell.listingImageView.layer.borderColor = UIColor.white.cgColor
        cell.listingImageView.clipsToBounds = true
        cell.listingImageView.layer.cornerRadius = 2
        
        cell.shadowView.layer.cornerRadius = 2
        cell.shadowView.layer.shadowColor = UIColor.black.cgColor
        cell.shadowView.alpha = CGFloat(0.26)
        cell.shadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        cell.shadowView.layer.shadowOpacity = 1
        
        cell.tag = indexPath.row
        let listingTap = UITapGestureRecognizer(target: self, action: #selector(selectListing))
        cell.addGestureRecognizer(listingTap)
        
        return cell
    }
    
    @objc func selectListing(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        onListingClick(listingTitles[index])
    }
     
}
