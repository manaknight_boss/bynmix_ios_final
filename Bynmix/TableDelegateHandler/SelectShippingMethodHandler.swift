import UIKit

class SelectShippingMethodHandler: NSObject,UITableViewDelegate,UITableViewDataSource  {
    
    let shippingMethodCell = "shippingMethodCell"
    var shipRates = [ShipRates]()
    var onRateClick:((ShipRates) -> Void)!
    var TYPE:Int?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shipRates.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: shippingMethodCell, for: indexPath) as! SelectShippingMethodTableViewCell
        let shipRate = shipRates[indexPath.row] as ShipRates
        
        if TYPE == AppConstants.TYPE_SELECT_SHIPPING_METHOD{
            cell.rateLabel.text =  "$\(String(shipRate.rate ?? 0))"
            cell.mediumLabel.text = shipRate.serviceLevel ?? ""
            cell.deliveryLabel.text = shipRate.durationDescription ?? ""
        }else{
            cell.rateLabel.text =  "$\(String(shipRate.amount ?? 0))"
            cell.mediumLabel.text = shipRate.serviceLevelName ?? ""
            cell.deliveryLabel.text = shipRate.duration ?? ""
        }
        cell.editImageView.tag = indexPath.row
        let rateTap = UITapGestureRecognizer(target: self, action: #selector(onRateClickAction(sender:)))
        cell.editImageView.addGestureRecognizer(rateTap)
        
        return cell
    }
    
    @objc func onRateClickAction(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        onRateClick(shipRates[index])
    }
    
}
