import UIKit

class SelectShippingRateHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    let shippingCell = "shippingRateCell"
    var shippingInfo = [ShippingInfo]()
    var shippingWeightInDetail = ShippingWeightInDetail()
    var onRateClick:((ShippingInfo) -> Void)!
    var tableView:UITableView?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return shippingInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: shippingCell, for: indexPath) as! SelectShippingRateTableViewCell
        let shipping = shippingInfo[indexPath.row] as ShippingInfo
        
        cell.rateLabel.text = "$\(shipping.rate ?? 0)"
        cell.classLabel.text = (shipping.shippingType?.type ?? "")
        cell.weightLabel.text = "(\(Int(shipping.shippingWeightOunceMin ?? 0)) - \(Int(shipping.shippingWeightOunceMax ?? 0)) lbs)"
        cell.dateLabel.text = (shipping.shippingType?.description ?? "")
        
        let pounds = (shippingWeightInDetail.pounds ?? 0)
        let ounces = (shippingWeightInDetail.ounces ?? 0 * 0.0625)
        
        let weightTotal = pounds + ounces
        let length = (shippingWeightInDetail.length ?? 0)
        let width = (shippingWeightInDetail.width ?? 0)
        let height = (shippingWeightInDetail.height ?? 0)
        let shippingWeight = Int(shipping.shippingWeightOunceMax ?? 0 * 0.0625) 
        self.addShadow(shadowView: cell.shadowView)
        
        if weightTotal > Double(shippingWeight) || length > shipping.maxLengthDimension ?? 0 || width > shipping.maxWidthDimension ?? 0 || height > shipping.maxHeightDimension ?? 0{
            self.disabledView(mainView: cell.innerView, innerView: cell.innerView, borderView: cell.borderView, cell: cell)
        }else if shipping.isSelected ?? false{
            self.addBorderPink(mainView: cell.innerView, innerView: cell.innerView, borderView: cell.borderView, cell: cell)
        }else{
            self.addBorderBlack(mainView: cell.innerView, innerView: cell.innerView, borderView: cell.borderView, cell: cell)
        }
        
        cell.tag = indexPath.row
        let rateTap = UITapGestureRecognizer(target: self, action: #selector(onRateClickAction(sender:)))
        cell.addGestureRecognizer(rateTap)
        
        return cell
    }
    
    func addShadow(shadowView:UIView){
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.alpha = CGFloat(0.90)
        shadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        shadowView.layer.shadowOpacity = 1
    }
    
    func addBorderPink(mainView:UIView,innerView:UIView,borderView:UIView,cell:SelectShippingRateTableViewCell){
        mainView.layer.borderWidth = 3.0
        mainView.layer.borderColor = UIColor.cBB189C.cgColor
        mainView.layer.masksToBounds = true
        innerView.backgroundColor = UIColor.cFFE9FB
        borderView.backgroundColor = UIColor.cE8E8E8
        cell.isUserInteractionEnabled = true
    }
    
    func addBorderBlack(mainView:UIView,innerView:UIView,borderView:UIView,cell:SelectShippingRateTableViewCell){
        mainView.layer.borderWidth = 2.0
        mainView.layer.borderColor = UIColor.c272727.cgColor
        mainView.layer.masksToBounds = true
        innerView.backgroundColor = UIColor.white
        borderView.backgroundColor = UIColor.cE8E8E8
        cell.isUserInteractionEnabled = true
    }
    
    func disabledView(mainView:UIView,innerView:UIView,borderView:UIView,cell:SelectShippingRateTableViewCell){
        mainView.layer.borderWidth = 2.0
        cell.rateLabel.textColor = UIColor.c848484 
        cell.classLabel.textColor = UIColor.c848484
        cell.weightLabel.textColor = UIColor.c848484
        cell.dateLabel.textColor = UIColor.c848484
        mainView.layer.borderColor = UIColor.c797979.cgColor
        mainView.layer.masksToBounds = true
        innerView.backgroundColor = UIColor.cE2E2E2
        borderView.backgroundColor = UIColor.cE8E8E8
        cell.isUserInteractionEnabled = false
    }
    
    @objc func onRateClickAction(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        onRateClick(shippingInfo[index])
    }
    
}
