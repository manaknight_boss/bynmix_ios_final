import UIKit

class SelectTagsTypeHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    let selectTagCell = "SelectTagTypeCell"
    var tags = [Dots]()
    var onSelectTagTypeClick: ((Int,Dots) -> Void)!
    var selectTagTypeTableViewCell = SelectTagTypeTableViewCell()
    var tableView:UITableView?
    var onListingButtonClick: ((Int) -> Void)!
    var TYPE:Int?
    var onLinkErrorButtonClick: ((UIButton) -> Void)!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return tags.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: selectTagCell, for: indexPath) as! SelectTagTypeTableViewCell
        selectTagTypeTableViewCell = cell
        
        let dots = tags[indexPath.row]
        
        self.setUI(cell:cell)
        
        cell.listingButton.tag = indexPath.row
        cell.listingButton.addTarget(self, action: #selector(onListingButtonClicked), for: .touchUpInside)
        
        cell.dropDownView.tag = indexPath.row
        let dropDownTap = UITapGestureRecognizer(target: self, action: #selector(selectTagTypeViewClicked))
        cell.dropDownView.addGestureRecognizer(dropDownTap)
        
        cell.dropDownButton.tag = indexPath.row
        cell.dropDownButton.addTarget(self, action: #selector(selectTagTypButtonClicked), for: .touchUpInside)
        
        if TYPE == AppConstants.TYPE_EDIT_TAGS{
            if(dots.postDotTypeId == nil || dots.postDotTypeId == 0) {
                self.hideAllViews(cell:cell)
                self.setSelectTagTypeLabel(cell:cell)
            } else {
                self.checkIfListingOrRetailer(cell:cell,dots:dots)
            }
            
            self.setDotsColor(cell:cell,dots:dots)
        }else{
            
            self.setDotsColor(cell:cell,dots:dots)
            
            if(dots.postDotTypeId == nil || dots.postDotTypeId == 0) {
                self.hideAllViews(cell:cell)
                self.setSelectTagTypeLabel(cell:cell)
            } else {
                self.checkIfListingOrRetailer(cell:cell,dots:dots)
            }
        }
        
        cell.titletextField.tag = indexPath.row
        cell.linkTextField.tag = indexPath.row
        
        cell.linkErrorButton.tag = indexPath.row
        
        if cell.linkTextField.text == nil{
            cell.linkTextField.text = ""
        }
        if cell.titletextField.text == nil{
            cell.titletextField.text = ""
        }
        
        return cell
    }
    
    func setSelectTagTypeLabel(cell:SelectTagTypeTableViewCell){
        cell.selectTagTypeLabel.text = AppConstants.Strings.SELECT_TAG_TYPE
        cell.selectTagTypeLabel.font = UIFont.fontSemiBold(size: 16)
        cell.selectTagTypeLabel.textColor = UIColor.cA6A6A6
    }
    
    func checkIfListingOrRetailer(cell:SelectTagTypeTableViewCell,dots:Dots){
        if(dots.postDotTypeId == AppConstants.TYPE_ID_ONLINE_RETAILER){
            cell.selectTagTypeLabel.text = AppConstants.Strings.ONLINE_RETAILER
            cell.selectTagTypeLabel.font = UIFont.fontSemiBold(size: 16)
            cell.selectTagTypeLabel.textColor = UIColor.black
            
            cell.titletextField.text = dots.title
            cell.linkTextField.text = dots.link
            
            self.typeOnlineRetailer(cell: cell)
            
        }else if (dots.postDotTypeId == AppConstants.TYPE_ID_MY_LISTING){
            cell.selectTagTypeLabel.text = AppConstants.Strings.MY_LISTING
            cell.selectTagTypeLabel.font = UIFont.fontSemiBold(size: 16)
            cell.selectTagTypeLabel.textColor = UIColor.black
            
            self.typeListing(cell: cell)
            
            if dots.listingTitle != nil{
                cell.listingButton.setTitle(dots.listingTitle, for: .normal)
            }else{
                cell.listingButton.setTitle(AppConstants.Strings.SELECT_LISTING_FROM_YOUR_BYN, for: .normal)
            }
            
        }else{
            self.hideAllViews(cell:cell)
        }
    }
    
    func setDotsColor(cell:SelectTagTypeTableViewCell,dots:Dots){
        if dots.hexColor == AppConstants.DOT_COLOR_RED{
            cell.tagView.backgroundColor = UIColor.cE58585
        }else if dots.hexColor == AppConstants.DOT_COLOR_GREEN{
            cell.tagView.backgroundColor = UIColor.c85E590
        }else if dots.hexColor == AppConstants.DOT_COLOR_PURPLE{
            cell.tagView.backgroundColor = UIColor.c858DE5
        }else if dots.hexColor == AppConstants.DOT_COLOR_BLUE{
            cell.tagView.backgroundColor = UIColor.c22CFFF
        }else if dots.hexColor == AppConstants.DOT_COLOR_YELLOW{
            cell.tagView.backgroundColor = UIColor.cFDD12A
        }
    }
    
    func typeListing(cell:SelectTagTypeTableViewCell){
        cell.titleView.isHidden = true
        cell.linkView.isHidden = true
        cell.listingButton.isHidden = false
    }
    
    func typeOnlineRetailer(cell:SelectTagTypeTableViewCell){
        cell.titleView.isHidden = false
        cell.linkView.isHidden = false
        cell.listingButton.isHidden = true
    }
    
    func setUI(cell:SelectTagTypeTableViewCell){
        cell.dropDownView.layer.borderWidth = 1
        cell.dropDownView.layer.borderColor = UIColor.cDDDDDD.cgColor
        cell.titleView.layer.borderWidth = 1
        cell.titleView.layer.borderColor = UIColor.cDDDDDD.cgColor
        cell.linkView.layer.borderWidth = 1
        cell.linkView.layer.borderColor = UIColor.cDDDDDD.cgColor
        cell.tagView.layer.cornerRadius = cell.tagView.frame.width / 2
    }
    
    func hideAllViews(cell:SelectTagTypeTableViewCell){
        cell.titleView.isHidden = true
        cell.linkView.isHidden = true
        cell.listingButton.isHidden = true
    }
    
    @objc func linkErrorButton(sender: UIButton) {
        onLinkErrorButtonClick(sender)
    }
    
    @objc func onListingButtonClicked(sender: UIButton) {
        let index = sender.tag
        onListingButtonClick(index)
    }
    
    @objc func selectTagTypeViewClicked(sender: UITapGestureRecognizer) {
        let index = (sender.view?.tag)!
        onSelectTagTypeClick(index,tags[index])
    }
    
    @objc func selectTagTypButtonClicked(sender: UIButton) {
        let index = sender.tag
        onSelectTagTypeClick(index,tags[index])
    }
    
}


