import UIKit

class SellerPolicyHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    let sellerPolicyCell = "SellerPolicyCell"
    var sellerData = [Terms]()
    var onEmailClick:((Terms) -> Void)!
    var TYPE:Int?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sellerData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: sellerPolicyCell, for: indexPath) as! SellerPolicyTableViewCell
        
        let sellerPolicy = self.sellerData[indexPath.row] as Terms
        
        cell.topView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        
        cell.titleLabel.text = sellerPolicy.title
        
        let htmlCSSString = "<style>" +
            "html *" +
            "{" +
            "font-size: \(12)pt !important;" +
            "font-family: Raleway-Regular !important;" +
        "}</style> \(self)"
        
        let text = htmlCSSString + sellerPolicy.description!
        print(text)
        cell.descriptionLabel.attributedText = text.html2Attributed
        cell.descriptionLabel.isUserInteractionEnabled = true
        
        cell.descriptionLabel.tag = indexPath.row
        let emailTap = UITapGestureRecognizer(target: self, action: #selector(onEmailClickAction))
        cell.descriptionLabel.addGestureRecognizer(emailTap)
        
        return cell
    }
    
    @objc func onEmailClickAction(sender: UITapGestureRecognizer){
        let index = sender.view!.tag
        if TYPE == AppConstants.TYPE_SALES_TAX{
            if index == sellerData.count - 1 {
               onEmailClick(sellerData[index])
            }
        }
    }

}
