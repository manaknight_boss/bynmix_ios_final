import UIKit

class ShippingHandler: NSObject,UITableViewDelegate,UITableViewDataSource {

    let shippingCell = "shippingCell"
    var shippingInfo = [ShippingInfo]()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shippingInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: shippingCell, for: indexPath) as! ShippingTableViewCell
        let shipping = shippingInfo[indexPath.row] as ShippingInfo
        
        cell.rateLabel.text = "$\(shipping.rate ?? 0)"
        cell.classLabel.text = "\(shipping.shippingType?.type ?? "")"
        cell.weightLabel.text = "(\(Int(shipping.shippingWeightMin ?? 0)) - \(Int(shipping.shippingWeightMax ?? 0)) lbs)"
        
        return cell
    }
    
    func addBorder(mainView:UIView,shadowView:UIView){
        mainView.layer.borderWidth = 4.0
        mainView.layer.borderColor = UIColor.cBB189C.cgColor
        mainView.layer.masksToBounds = true
        
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.alpha = CGFloat(0.26)
        shadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        shadowView.layer.shadowOpacity = 1
    }
    
    func addBorderBlack(mainView:UIView,shadowView:UIView){
        mainView.layer.borderWidth = 2.0
        mainView.layer.borderColor = UIColor.c272727.cgColor
        mainView.layer.masksToBounds = true
        
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.alpha = CGFloat(0.26)
        shadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        shadowView.layer.shadowOpacity = 1
    }
    
}
