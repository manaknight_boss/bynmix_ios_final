import UIKit

class SizeTabHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    let sizeTabCell = "sizeTabCell"
    var sizeData = [SizeType]()
    let sizeTypeHandler = SizeInfoHandler()
    var onSizesClick: ((Int) -> Void)!
    var tableView:UITableView?
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return sizeData[section].displayGroups!.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sizeData.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sizeData[section].sizeTypeName
    }
    
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor.cBB189C
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.white
        header.textLabel?.font = UIFont.fontSemiBold(size: 14)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:SizeCellTableViewCell = tableView.dequeueReusableCell(withIdentifier: "dataTableCellId") as! SizeCellTableViewCell
        
        let sizes = sizeData[indexPath.section].sizes!
        
        
        var requiredSizes: [Sizes] = []
        let displayGroup = String(indexPath.row + 1)
        for i in 0..<sizes.count {
            if sizes[i].displayGroup == displayGroup {
                requiredSizes.append(sizes[i])
            }
        }
        
        sizeTypeHandler.sizeInfoData = requiredSizes
        sizeTypeHandler.onSizesClick = { id in
            self.onSizesClick(id)
        }
        
        cell.dataCollectionView.collectionViewLayout = FlowLayoutHelper()
        
        cell.dataCollectionView.delegate = sizeTypeHandler
        cell.dataCollectionView.dataSource = sizeTypeHandler
        
        cell.dataCollectionView.reloadData()
        
        let height : CGFloat = cell.dataCollectionView.collectionViewLayout.collectionViewContentSize.height
        
        cell.dataCollectionViewHeightConstraint.constant = height
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 10
    }
    
    func tableView(_ tableView: UITableView, willDisplayFooterView view: UIView, forSection section: Int) {
        view.tintColor = UIColor.cE8E8E8
    }
    
}



