import UIKit

class SizeTypeHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    let sizeTypeCell = "SizeTypeCell"
    var sizeTypeData = [[Sizes]]()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sizeTypeData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: sizeTypeCell, for: indexPath) as! SizeTypeTableViewCell
        let sizeInfoHandler = SizeInfoHandler()
        
        cell.sizeCollectionView!.collectionViewLayout = FlowLayoutHelper()
        sizeInfoHandler.sizeInfoData = sizeTypeData[indexPath.item]
        
        
        cell.sizeCollectionView.delegate = sizeInfoHandler
        cell.sizeCollectionView.dataSource = sizeInfoHandler
        cell.sizeCollectionView.reloadData()
        return cell
    }
    
}
