import UIKit

class SubCategoryHandler: NSObject,UITableViewDelegate,UITableViewDataSource {

    let subCategoryCell = "SubCategoryCell"
    var categoryType = [Category]()
    var onSubCategoryClick: ((Category) -> Void)!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: subCategoryCell, for: indexPath) as! SubCategoryTableViewCell
        cell.subCategoryLabel.text = categoryType[indexPath.row].categoryName
        if categoryType[indexPath.row].isSelected! {
            cell.subCategoryLabel.textColor = UIColor.cBB189C
            cell.subCategoryLabel.font = UIFont.fontBold(size: 16)
        } else {
            cell.subCategoryLabel.textColor = UIColor.black
        }
        cell.tag = indexPath.row
        let cellTap = UITapGestureRecognizer(target: self, action: #selector(onSubCategoryTap))
        cell.addGestureRecognizer(cellTap)
        
        return cell
    }
    
    @objc func onSubCategoryTap(sender:UITapGestureRecognizer){
        let index = sender.view!.tag
        onSubCategoryClick(categoryType[index])
    }
    
}
