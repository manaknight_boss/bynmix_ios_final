import UIKit

class TrackingInfoHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    var shippingHistory = [ShippingHistory]()
    var shippingNil = [String]()
    let trackingInfoCell = "trackingInformationCell"
    
    var hasShipping:Bool = false
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if hasShipping{
            return shippingHistory.count
        }else{
            return shippingNil.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: trackingInfoCell, for: indexPath) as! TrackingInformationTableViewCell
        
        if hasShipping{
            let shippingData = shippingHistory[indexPath.row]
            cell.trackingInformationLabel.text = shippingData.displayStatus
        }else{
            let shippingNilMessage = shippingNil[indexPath.row]
            cell.trackingInformationLabel.text = shippingNilMessage
        }
        
        return cell
    }
    

}
