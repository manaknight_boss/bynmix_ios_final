import UIKit

class UserOffersHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    let offersCell = "offersCell"
    var subOfferHistory = [SubOfferHistory]()
    var loadMoreDelegate: LoadMoreProtocol?
    var tableView:UITableView?
    var isLoadMoreRequired = true
    var TYPE:Int?
    var onViewButtonClick:((SubOfferHistory) -> Void)!
    var onCounterButtonClick:((SubOfferHistory) -> Void)!
    var onAcceptButtonClick:((SubOfferHistory) -> Void)!
    var onDeclineButtonClick:((SubOfferHistory) -> Void)!
    var onUserImageViewClick:((SubOfferHistory) -> Void)!
    var onFormattedTextClick:((SubOfferHistory) -> Void)!
    var heightAtIndexPath = NSMutableDictionary()
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let height = NSNumber(value: Float(cell.frame.size.height))
        heightAtIndexPath.setObject(height, forKey: indexPath as NSCopying)
        
        if isLoadMoreRequired {
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                self.tableView?.tableFooterView = spinner
                self.tableView?.tableFooterView?.isHidden = false
            }
        } else {
            self.tableView?.tableFooterView = nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return subOfferHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: offersCell, for: indexPath) as! OffersTableViewCell
        
        let subOfferHistory = self.subOfferHistory[indexPath.row] as SubOfferHistory
        
        cell.timeLabel.textColor = UIColor.c434343
        cell.userImageView.layer.borderWidth = 2.0
        cell.userImageView.layer.masksToBounds = false
        cell.userImageView.layer.borderColor = UIColor.white.cgColor
        cell.userImageView.clipsToBounds = true
        cell.userImageView.layer.cornerRadius = 2
        
        cell.shadowView.layer.cornerRadius = 2
        cell.shadowView.layer.shadowColor = UIColor.black.cgColor
        cell.shadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        cell.shadowView.layer.shadowOpacity = 1
        cell.shadowView.alpha = CGFloat(0.50)
        
        cell.timeLabel.text = subOfferHistory.offerDateFormatted
        
        var username:String?
        if (subOfferHistory.isBuyerOffer ?? false) {
            username = subOfferHistory.buyerUsername
        } else {
            username = subOfferHistory.sellerUsername
        }
        
        if (subOfferHistory.isActiveOffer ?? false) {
            if (subOfferHistory.isBuyerOffer ?? false) {
                cell.viewButton.isHidden = true
                cell.buttonsView.isHidden = false
            } else {
                cell.viewButton.isHidden = false
                cell.buttonsView.isHidden = true
            }

        } else {
            cell.viewButton.isHidden = false
            cell.buttonsView.isHidden = true
        }
        
        let image = UIImage(named:  AppConstants.Images.DEFAULT_OFFER_PLACEHOLDER)
        
        if let imageUrl = subOfferHistory.listingImage{
            cell.userImageView.kf.setImage(with: URL(string: imageUrl), placeholder: image)
        }else{
            cell.userImageView.image = image
        }
        
        cell.highlightView.backgroundColor = UIColor.cBB189C
        
        let descText = "\(username ?? "") counteroffered $\(subOfferHistory.offerPrice ?? 0) for \"\(subOfferHistory.listingTitle ?? "")\"!"
        
        let textPink:String = "\(username ?? "")"
        let textBold:String = "counteroffered $\(subOfferHistory.offerPrice ?? 0)"
        let textUnderline:String = (subOfferHistory.listingTitle ?? "")
        let rangePink = (descText as NSString).range(of: textPink)
        let rangeBold = (descText as NSString).range(of: textBold)
        let rangeUnderline = (descText as NSString).range(of: textUnderline)
        let attributedString = NSMutableAttributedString(string: descText)
        attributedString.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.cBB189C, range: rangePink)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontSemiBold(size: 14), range: rangePink)
        attributedString.addAttribute(NSAttributedString.Key.font, value: UIFont.fontBold(size: 14), range: rangeBold)
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: rangeUnderline)
        cell.descLabel.attributedText = attributedString
                
        cell.viewButton.tag = indexPath.row
        cell.viewButton.addTarget(self, action: #selector(onViewButtonAction), for: .touchUpInside)
        
        cell.counterButton.tag = indexPath.row
        cell.counterButton.addTarget(self, action: #selector(onCounterButtonAction), for: .touchUpInside)
        
        cell.acceptButton.tag = indexPath.row
        cell.acceptButton.addTarget(self, action: #selector(onAcceptButtonAction), for: .touchUpInside)
        
        cell.declineButton.tag = indexPath.row
        cell.declineButton.addTarget(self, action: #selector(onDeclineButtonAction), for: .touchUpInside)
        
        cell.userImageView.tag = indexPath.row
        let imageViewTap = UITapGestureRecognizer(target: self, action: #selector(onUserImageViewTap))
        cell.userImageView.addGestureRecognizer(imageViewTap)
        
        cell.descLabel.tag = indexPath.row
        let formattedTextTap = UITapGestureRecognizer(target: self, action: #selector(formattedTextClickAction))
        cell.descLabel.addGestureRecognizer(formattedTextTap)
        
        return cell
    }
    
    @objc func formattedTextClickAction(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        onFormattedTextClick(subOfferHistory[index])
    }
    
    @objc func onUserImageViewTap(sender:UITapGestureRecognizer){
        let index = (sender.view?.tag)!
        onUserImageViewClick(subOfferHistory[index])
    }
    
    @objc func onViewButtonAction(sender:UIButton){
        let index = sender.tag
        onViewButtonClick(subOfferHistory[index])
    }
    
    @objc func onCounterButtonAction(sender:UIButton){
        let index = sender.tag
        onCounterButtonClick(subOfferHistory[index])
    }
    
    @objc func onAcceptButtonAction(sender:UIButton){
        let index = sender.tag
        onAcceptButtonClick(subOfferHistory[index])
    }
    
    @objc func onDeclineButtonAction(sender:UIButton){
        let index = sender.tag
        onDeclineButtonClick(subOfferHistory[index])
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView?.tableFooterView != nil {
            let height = scrollView.frame.size.height
            let contentYoffset = scrollView.contentOffset.y
            let distanceFromBottom = scrollView.contentSize.height - contentYoffset
            if distanceFromBottom < height {
                loadMoreDelegate?.onLoadMore()
            }
        }
    }
    
}
