import UIKit

class UserPostsHandler: NSObject,UITableViewDelegate,UITableViewDataSource {

    var feed = [Feed]()
    var userPostsCell = "UserPostsCell"
    var tableView: UITableView?
    var heightAtIndexPath = NSMutableDictionary()
    var loadMoreDelegate: LoadMoreProtocol?
    var isLoadMoreRequired = true
    var onEnableDisableButtonClick: ((Feed) -> Void)!
    var onDeleteButtonClick:((Feed) -> Void)!
    var onItemImageViewClick:((Feed) -> Void)!
    var onEditButtonClick:((Feed) -> Void)!
    
    func updateStatus(id: Int) {
        for i in 0..<self.feed.count {
            if self.feed[i].id == id {
                self.feed[i].isActive = !feed[i].isActive!
            }
        }
    }
    
    func deleteStatus(id: Int) {
        var index = -1
        for i in 0..<self.feed.count {
            if self.feed[i].id == id {
                index = i
                break
            }
        }
        self.feed.remove(at: index)
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if let height = heightAtIndexPath.object(forKey: indexPath) as? NSNumber {
            return CGFloat(height.floatValue)
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let height = NSNumber(value: Float(cell.frame.size.height))
        heightAtIndexPath.setObject(height, forKey: indexPath as NSCopying)
        
        if isLoadMoreRequired {
            let lastSectionIndex = tableView.numberOfSections - 1
            let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1
            if indexPath.section ==  lastSectionIndex && indexPath.row == lastRowIndex {
                let spinner = UIActivityIndicatorView(style: .gray)
                spinner.startAnimating()
                spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
                
                self.tableView?.tableFooterView = spinner
                self.tableView?.tableFooterView?.isHidden = false
            }
        } else {
            self.tableView?.tableFooterView = nil
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return feed.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: userPostsCell, for: indexPath) as! UserPostsTableViewCell
        
        let feed = self.feed[indexPath.row] as Feed
        
        cell.topView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        let type = feed.type
        let image : UIImage
        
        if feed.postType == AppConstants.VIDEO_POST {
            image =  UIImage(named: AppConstants.Images.ITEM_VIDEO_PLACEHOLDER)!
            
        }else {
            image =  UIImage(named: AppConstants.Images.ITEM_PLACEHOLDER)!
        }
        
        cell.titleLabel.text = feed.title
        cell.timeLabel.text = feed.createdDateFormatted
        
        cell.middleView.frame = CGRect(x: 0, y: 0, width: cell.middleView.frame.width, height: cell.middleView.frame.width)
        cell.itemImageView.layer.zPosition = -1
        if let imageUrl = feed.photoUrl{
            if imageUrl == ""{
                cell.itemImageView.image = image
            }else{
                cell.itemImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
            }
        }
        
        if type == AppConstants.POST && feed.postType == AppConstants.VIDEO_POST{
            cell.videoImageView.isHidden = false
            cell.videoImageView.image = UIImage(named: AppConstants.Images.VIDEO_ICON)
        }else{
            cell.videoImageView.isHidden = true
        }
        
        if feed.isActive == true{
            cell.enableDisableButton.setTitle(AppConstants.DISABLE, for: .normal)
            cell.enableDisableButton.backgroundColor = UIColor.cC80000.withAlphaComponent(0.80)
        }else{
            cell.enableDisableButton.setTitle(AppConstants.ENABLE, for: .normal)
            cell.enableDisableButton.backgroundColor = UIColor.cE17500.withAlphaComponent(0.80)
        }
        
        cell.deleteButton.tag = indexPath.row
        cell.deleteButton.addTarget(self, action: #selector(deleteButtonClick), for: .touchUpInside)
        
        cell.enableDisableButton.tag = indexPath.row
        cell.enableDisableButton.addTarget(self, action: #selector(enableDisableButtonClick), for: .touchUpInside)
        
        cell.editButton.tag = indexPath.row
        cell.editButton.addTarget(self, action: #selector(editButtonClick), for: .touchUpInside)
        
        cell.middleView.tag = indexPath.row
        let itemImageTap = UITapGestureRecognizer(target: self, action: #selector(itemImageViewClick))
        cell.middleView.addGestureRecognizer(itemImageTap)
        
        return cell
    }
    
    @objc func itemImageViewClick(sender: UITapGestureRecognizer){
        let index = sender.view!.tag
        onItemImageViewClick(feed[index])
    }
    
    @objc func deleteButtonClick(sender: UIButton){
        let index = sender.tag
        onDeleteButtonClick(feed[index])
    }
    
    @objc func enableDisableButtonClick(sender: UIButton){
        let index = sender.tag
        onEnableDisableButtonClick(feed[index])
    }
    
    @objc func editButtonClick(sender: UIButton){
        let index = sender.tag
        onEditButtonClick(feed[index])
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
           if self.tableView?.tableFooterView != nil {
               let height = scrollView.frame.size.height
               let contentYoffset = scrollView.contentOffset.y
               let distanceFromBottom = scrollView.contentSize.height - contentYoffset
               if distanceFromBottom < height {
                   loadMoreDelegate?.onLoadMore()
               }
           }
       }
    
}
