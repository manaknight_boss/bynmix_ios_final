import UIKit

class ViewReviewHandler: NSObject,UITableViewDelegate,UITableViewDataSource {
    
    var reviewCell = "reviewCell"
    var review = [Reviews]()
    var TYPE:Int?
    var loadMoreDelegate: LoadMoreProtocol?
    var isLoadMoreRequired = true
    var tableView:UITableView?
    var onUserProfileClick:((Reviews) -> Void)!
    var onListingClick:((Reviews) -> Void)!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.tableView = tableView
        return review.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reviewCell, for: indexPath) as! ViewReviewTableViewCell
        
        let reviews = review[indexPath.row] as Reviews
        
        cell.userImageView.layer.borderWidth = 2.0
        cell.userImageView.layer.masksToBounds = false
        cell.userImageView.layer.borderColor = UIColor.white.cgColor
        cell.userImageView.clipsToBounds = true
        cell.userImageView.layer.cornerRadius = 2
        
        cell.userShadowView.layer.cornerRadius = 2
        cell.userShadowView.layer.shadowColor = UIColor.black.cgColor
        cell.userShadowView.alpha = CGFloat(0.50)
        cell.userShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        cell.userShadowView.layer.shadowOpacity = 1
        
        cell.otherUserImageView.layer.borderWidth = 2.0
        cell.otherUserImageView.layer.masksToBounds = false
        cell.otherUserImageView.layer.borderColor = UIColor.white.cgColor
        cell.otherUserImageView.clipsToBounds = true
        cell.otherUserImageView.layer.cornerRadius = cell.otherUserImageView.frame.size.width/2
        
        cell.otherUserShadowView.layer.cornerRadius = cell.otherUserShadowView.frame.size.width/2
        cell.otherUserShadowView.layer.shadowColor = UIColor.black.cgColor
        cell.otherUserShadowView.alpha = CGFloat(0.26)
        cell.otherUserShadowView.layer.shadowOffset = CGSize(width: 0,height: 1)
        cell.otherUserShadowView.layer.shadowOpacity = 1
        cell.otherUserShadowView.layer.zPosition = -1
        
        let lisingTitle = reviews.listingTitle
        var userImage: String?
        if (TYPE == AppConstants.TYPE_BUYER) {
            userImage = reviews.buyerPhotoUrl!
            cell.otherUserTitleLabel.text = reviews.sellerUsername
        } else {
            userImage = reviews.sellerPhotoUrl!
            cell.otherUserTitleLabel.text = reviews.buyerUsername
        }
        cell.titleLabel.text = lisingTitle
        if reviews.feedback != "" && reviews.feedback != nil{
            cell.commentView.isHidden = false
            cell.commentLabel.text = "\"" + (reviews.feedback ?? "") + "\""
        }else{
            cell.commentView.isHidden = true
        }
        cell.commentDateLabel.text = reviews.createdDateFormatted
        
        let photoUrl = reviews.listingPhotoUrl
        let image = UIImage(named: AppConstants.Images.DEFAULT_OFFER_PLACEHOLDER)
        
        if let imageUrl = photoUrl{
            cell.userImageView.kf.setImage(with: URL(string: imageUrl)!,placeholder: image)
        } else {
            cell.userImageView.image =  image
        }
        
        let userDefaultImage : UIImage = UIImage(named: AppConstants.Images.DEFAULT_USER)!
        
        if let userImageUrl = userImage{
            cell.otherUserImageView.kf.setImage(with: URL(string: userImageUrl)!,placeholder: userDefaultImage)
        } else {
            cell.otherUserImageView.image =  userDefaultImage
        }
        
        let rating:Int = Int(reviews.rating?.rounded() ?? 0)
        
        if rating == 0{
            cell.starOneImageView.image = UIImage(named: AppConstants.Images.UNRATED_STAR_ICON)
            cell.starTwoImageView.image = UIImage(named: AppConstants.Images.UNRATED_STAR_ICON)
            cell.starThreeImageView.image = UIImage(named: AppConstants.Images.UNRATED_STAR_ICON)
            cell.starFourImageView.image = UIImage(named: AppConstants.Images.UNRATED_STAR_ICON)
            cell.starFiveImageView.image = UIImage(named: AppConstants.Images.UNRATED_STAR_ICON)
        }else if rating == 1{
            cell.starOneImageView.image = UIImage(named: AppConstants.Images.RATING_STAR_ICON)
            cell.starTwoImageView.image = UIImage(named: AppConstants.Images.UNRATED_STAR_ICON)
            cell.starThreeImageView.image = UIImage(named: AppConstants.Images.UNRATED_STAR_ICON)
            cell.starFourImageView.image = UIImage(named: AppConstants.Images.UNRATED_STAR_ICON)
            cell.starFiveImageView.image = UIImage(named: AppConstants.Images.UNRATED_STAR_ICON)
        }else if rating == 2{
            cell.starOneImageView.image = UIImage(named: AppConstants.Images.RATING_STAR_ICON)
            cell.starTwoImageView.image = UIImage(named: AppConstants.Images.RATING_STAR_ICON)
            cell.starThreeImageView.image = UIImage(named: AppConstants.Images.UNRATED_STAR_ICON)
            cell.starFourImageView.image = UIImage(named: AppConstants.Images.UNRATED_STAR_ICON)
            cell.starFiveImageView.image = UIImage(named: AppConstants.Images.UNRATED_STAR_ICON)
            
        }else if rating == 3{
            cell.starOneImageView.image = UIImage(named: AppConstants.Images.RATING_STAR_ICON)
            cell.starTwoImageView.image = UIImage(named: AppConstants.Images.RATING_STAR_ICON)
            cell.starThreeImageView.image = UIImage(named: AppConstants.Images.RATING_STAR_ICON)
            cell.starFourImageView.image = UIImage(named: AppConstants.Images.UNRATED_STAR_ICON)
            cell.starFiveImageView.image = UIImage(named: AppConstants.Images.UNRATED_STAR_ICON)
            
        }else if rating == 4{
            cell.starOneImageView.image = UIImage(named: AppConstants.Images.RATING_STAR_ICON)
            cell.starTwoImageView.image = UIImage(named: AppConstants.Images.RATING_STAR_ICON)
            cell.starThreeImageView.image = UIImage(named: AppConstants.Images.RATING_STAR_ICON)
            cell.starFourImageView.image = UIImage(named: AppConstants.Images.RATING_STAR_ICON)
            cell.starFiveImageView.image = UIImage(named: AppConstants.Images.UNRATED_STAR_ICON)
        }else{
            cell.starOneImageView.image = UIImage(named: AppConstants.Images.RATING_STAR_ICON)
            cell.starTwoImageView.image = UIImage(named: AppConstants.Images.RATING_STAR_ICON)
            cell.starThreeImageView.image = UIImage(named: AppConstants.Images.RATING_STAR_ICON)
            cell.starFourImageView.image = UIImage(named: AppConstants.Images.RATING_STAR_ICON)
            cell.starFiveImageView.image = UIImage(named: AppConstants.Images.RATING_STAR_ICON)
        }
        
        cell.otherUserImageView.isUserInteractionEnabled = true
        cell.otherUserImageView.tag = indexPath.row
        let imageTap = UITapGestureRecognizer(target: self, action: #selector(onUserProfileClickAction))
        cell.otherUserImageView.addGestureRecognizer(imageTap)
        
        cell.otherUserTitleLabel.isUserInteractionEnabled = true
        cell.otherUserTitleLabel.tag = indexPath.row
        let titleTap = UITapGestureRecognizer(target: self, action: #selector(onUserProfileClickAction))
        cell.otherUserTitleLabel.addGestureRecognizer(titleTap)
        
        cell.userImageView.isUserInteractionEnabled = true
        cell.userImageView.tag = indexPath.row
        let userImageViewTap = UITapGestureRecognizer(target: self, action: #selector(onListingClickAction))
        cell.userImageView.addGestureRecognizer(userImageViewTap)
        
        cell.userShadowView.isUserInteractionEnabled = true
        cell.userShadowView.tag = indexPath.row
        let userShadowViewTap = UITapGestureRecognizer(target: self, action: #selector(onListingClickAction))
        cell.userShadowView.addGestureRecognizer(userShadowViewTap)
        
        return cell
    }
    
    @objc func onUserProfileClickAction(sender:UITapGestureRecognizer){
        let index = (sender.view!.tag)
        onUserProfileClick(review[index])
    }
    
    @objc func onListingClickAction(sender:UITapGestureRecognizer){
        let index = (sender.view!.tag)
        onListingClick(review[index])
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if self.tableView?.tableFooterView != nil {
            let height = scrollView.frame.size.height
            let contentYoffset = scrollView.contentOffset.y
            let distanceFromBottom = scrollView.contentSize.height - contentYoffset
            if distanceFromBottom < height {
                loadMoreDelegate?.onLoadMore()
            }
        }
    }
    
    
}
