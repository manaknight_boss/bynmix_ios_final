import Foundation

class AppDefaults {
    
    static let APP = "App"
    
    static let TOKEN : String = "token"
    static let APP_TOKEN : String = "app_token"
    static let WELCOME_VIEW : String = "welcome_view"
    static let ON_BOARDING_STAGE : String = "on_boarding_stage"
    static let LAST_ONBOARDING_SCREEN : String = "last_onboarding_screen_completed"
    static let Registration_Completed : String = "registration_completed"
    static let DEVICE_ID : String = "device_id"
    static let REFRESH_TOKEN : String = "refresh_token"
    static let USERNAME : String = "username"
    static let SELLER_SETUP : String = "seller_setup"
    static let LISTING_TITLE : String = "listing_title"
    static let PROFILE_SCREEN : String = "profile_screen"
    static let USER_ID :String = "user_id"
    static let TAGS :String = "tags"
    static let LAST_INDEX_OF_TAB = "last_index_of_tab"
    static let PROMOTION_ID = "promotion_id"
    static let TOKEN_EXPIRED = "token_expired"
    static let SOCIAL_LINK_FB_ID = "fb_id"
    static let SOCIAL_LINK_FB_USERNAME = "fb_username"
    
    public static func getToken() -> String {
        return KeychainWrapper.standard.string(forKey: TOKEN) ?? ""
        //return UserDefaults.standard.string(forKey: TOKEN) ?? ""
    }
    
    public static func setToken(token: String) {
        KeychainWrapper.standard.set(token, forKey: TOKEN)
        //UserDefaults.standard.set(token, forKey: TOKEN)
    }
    
    public static func getAccessToken() -> String {
        return KeychainWrapper.standard.string(forKey: APP_TOKEN) ?? ""
        //return UserDefaults.standard.string(forKey: APP_TOKEN) ?? ""
    }
    
    public static func setAccessToken(token: String) {
        KeychainWrapper.standard.set(token, forKey: APP_TOKEN)
        //UserDefaults.standard.set(token, forKey: APP_TOKEN)
    }
    
    public static func getWelcomeView() -> Bool {
        return UserDefaults.standard.bool(forKey: WELCOME_VIEW)
    }
    
    public static func setWelcomeView(welocmeView: Bool) {
        UserDefaults.standard.set(welocmeView, forKey: WELCOME_VIEW)
    }
    
    public static func isSellerSetupDone() -> Bool {
        return UserDefaults.standard.bool(forKey: SELLER_SETUP)
    }
    
    public static func setSellerSetup(isSetupDone: Bool) {
        UserDefaults.standard.set(isSetupDone, forKey: SELLER_SETUP)
    }
    
    public static func openProfileScreen() -> Bool {
        return UserDefaults.standard.bool(forKey: PROFILE_SCREEN)
    }
    
    public static func setProfileScreen(openProfile: Bool) {
        UserDefaults.standard.set(openProfile, forKey: PROFILE_SCREEN)
    }
    
    public static func getOnBoardingStage() -> Bool {
        return UserDefaults.standard.bool(forKey: ON_BOARDING_STAGE)
    }
    
    public static func setOnBoardingStage(onboardingComplete: Bool) {
        UserDefaults.standard.set(onboardingComplete, forKey: ON_BOARDING_STAGE)
    }
    
    public static func getLastOnBoardingScreen() -> Int {
        return UserDefaults.standard.integer(forKey: LAST_ONBOARDING_SCREEN)
    }
    
    public static func setLastOnBoardingScreen(lastOnBoardingScreen: Int) {
        UserDefaults.standard.set(lastOnBoardingScreen, forKey: LAST_ONBOARDING_SCREEN)
    }
    
    public static func getUserId() -> Int {
        return UserDefaults.standard.integer(forKey: USER_ID)
    }
    
    public static func setUserId(userId: Int) {
        UserDefaults.standard.set(userId, forKey: USER_ID)
    }
    
    public static func getRegistrationCompleted() -> Bool {
        return UserDefaults.standard.bool(forKey: Registration_Completed)
    }
    
    public static func setRegistrationCompleted(registrationCompleted: Bool) {
        UserDefaults.standard.set(registrationCompleted, forKey: Registration_Completed)
    }
    
    public static func getDeviceId() -> String {
        return UserDefaults.standard.string(forKey: DEVICE_ID) ?? ""
    }
    
    public static func setDeviceId(deviceId: String) {
        UserDefaults.standard.set(deviceId, forKey: DEVICE_ID)
    }
    
    public static func getRefreshToken() -> String {
        return KeychainWrapper.standard.string(forKey: REFRESH_TOKEN) ?? ""
        //return UserDefaults.standard.string(forKey: REFRESH_TOKEN) ?? ""
    }
    
    public static func setRefreshToken(token: String) {
        KeychainWrapper.standard.set(token, forKey: REFRESH_TOKEN)
        //UserDefaults.standard.set(token, forKey: REFRESH_TOKEN)
    }
    
    public static func getUsername() -> String {
        return UserDefaults.standard.string(forKey: USERNAME) ?? ""
    }
    
    public static func setUsername(username: String) {
        UserDefaults.standard.set(username, forKey: USERNAME)
    }
    
    public static func getTitle() -> String {
        return UserDefaults.standard.string(forKey: LISTING_TITLE) ?? ""
    }
    
    public static func setTitle(title: String) {
        UserDefaults.standard.set(title, forKey: LISTING_TITLE)
    }
    
    public static func getTags() -> Int {
        return UserDefaults.standard.integer(forKey: TAGS)
    }
    
    public static func setTags(tagValue: Int) {
        UserDefaults.standard.set(tagValue, forKey: TAGS)
    }
    
    public static func getChangedTab() -> Bool {
        return UserDefaults.standard.bool(forKey: LAST_INDEX_OF_TAB)
    }
    
    public static func setChangedTab(value: Bool) {
        UserDefaults.standard.set(value, forKey: LAST_INDEX_OF_TAB)
    }
    
    public static func getPromotionIds() -> [Int] {
        var promotionsArray: [Int] = []
        if let promotionArray : AnyObject = UserDefaults.standard.object(forKey: PROMOTION_ID) as AnyObject? {
            let readArray : [Int] = promotionArray as! [Int]
            promotionsArray = readArray
        }
        return promotionsArray
    }
    
    public static func setPromotionId(promotionId: Int) {
        let defaults = UserDefaults.standard
        var array = defaults.object(forKey: PROMOTION_ID) as? [Int] ?? [Int]()
        if promotionId == 0{
            array.removeAll()
        }else{
            array.append(promotionId)
        }
        defaults.set(array, forKey: PROMOTION_ID)
        defaults.synchronize()
        print("Array:\(array)")
    }
    
    public static func getTokenExpired() -> Bool {
        return UserDefaults.standard.bool(forKey: TOKEN_EXPIRED)
    }
    
    public static func setTokenExpired(isTokenExpired: Bool) {
        UserDefaults.standard.set(isTokenExpired, forKey: TOKEN_EXPIRED)
    }
    
    public static func getFBID() -> String {
        return UserDefaults.standard.string(forKey: SOCIAL_LINK_FB_ID) ?? ""
    }
    
    public static func setFBID(fbId: String) {
        UserDefaults.standard.set(fbId, forKey: SOCIAL_LINK_FB_ID)
    }
    
    public static func getFBUsername() -> String {
        return UserDefaults.standard.string(forKey: SOCIAL_LINK_FB_USERNAME) ?? ""
    }
    
    public static func setFBUsername(fbUsername: String) {
        UserDefaults.standard.set(fbUsername, forKey: SOCIAL_LINK_FB_USERNAME)
    }
    
}
