import Foundation
import Alamofire

class AlertsViewModel {

    let alertsRepository : AlertsRepository
    
    init() {
        alertsRepository = AlertsRepository()
    }
    
    public func getMyOffers(accessToken: String,page:Int ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        alertsRepository.getMyOffers(accessToken: accessToken, page: page, response, error)
    }
    
    public func getAlertsAll(accessToken: String,page:Int ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        alertsRepository.getAlertsAll(accessToken: accessToken, page: page, response, error)
    }
    
    public func getReceivedOffers(accessToken: String,page:Int ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        alertsRepository.getReceivedOffers(accessToken: accessToken, page: page, response, error)
    }
    
    public func offerAccepted(accessToken: String,listingId:Int ,offerId:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        alertsRepository.offerAccepted(accessToken: accessToken, listingId: listingId, offerId: offerId, response, error)
    }
    
    public func offerDeclined(accessToken: String,listingId:Int ,offerId:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        alertsRepository.offerDeclined(accessToken: accessToken, listingId: listingId, offerId: offerId, response, error)
    }
    
    public func getMyOffersDetail(accessToken: String,listingId:Int ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        alertsRepository.getMyOffersDetail(accessToken: accessToken, listingId: listingId, response, error)
    }
    
    public func getReceivedOffersDetail(accessToken: String,listingId:Int ,initiatorUserId:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        alertsRepository.getReceivedOffersDetail(accessToken: accessToken, listingId: listingId, initiatorUserId: initiatorUserId, response, error)
    }
    
    public func cancelOffer(accessToken: String,listingId:Int ,offerId:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        alertsRepository.cancelOffer(accessToken: accessToken, listingId: listingId, offerId: offerId, response, error)
    }
    
    public func getBuyerBids(accessToken: String ,id:Int,page:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        alertsRepository.getBuyerBids(accessToken: accessToken, id:id,page:page ,response, error)
    }
    
    public func submitOffer(accessToken: String ,id:Int,data:SubOfferHistory,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        alertsRepository.submitOffer(accessToken: accessToken, id:id,data:data ,response, error)
    }
    
    public func counterOffer(accessToken: String ,listingId:Int,offerId:Int,data:SubOfferHistory,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        alertsRepository.counterOffer(accessToken: accessToken, listingId:listingId, offerId: offerId,data:data ,response, error)
    }
    
    public func updateAlertsReadNotifications(accessToken: String ,data:Setting,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
           alertsRepository.updateAlertsReadNotifications(accessToken: accessToken, data:data,response, error)
       }
    
}
