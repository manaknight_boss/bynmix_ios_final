import Foundation
import Alamofire

class AppInfoViewModel {
    
    let appInfoRepository: AppInfoRepository
    
    init() {
        appInfoRepository = AppInfoRepository()
    }
    
    public func getTerms(_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        appInfoRepository.getTerms(onResponse, onError)
    }
    
    public func getPrivacyPolicy(_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        appInfoRepository.getPrivacyPolicy(onResponse, onError)
    }
    
}
