import Alamofire
class AuthViewModel {
    
    let authRepository: AuthRepository
    
    init() {
        authRepository = AuthRepository()
    }
    
    public func login(request: UserAuth, _ onResponse: @escaping (UserAuth) -> Void,_ onError: @escaping (ApiError) -> Void) {
        authRepository.login(data: request, onResponse, onError)
    }
    
    public func loginWithSocial(request: UserAuth, _ onResponse: @escaping (AFDataResponse<Any>) -> Void,_ onError: @escaping (ApiError) -> Void) {
        authRepository.loginWithSocial(data: request, onResponse, onError)
    }
    
    public func register(request: UserAuth, _ onResponse: @escaping (AFDataResponse<Any>) -> Void,_ onError: @escaping (ApiError) -> Void) {
        authRepository.register(data: request, onResponse, onError)
    }
    
    public func updateDeviceId(accessToken: String,data:DeviceIdRequest ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        authRepository.updateDeviceId(accessToken: accessToken,data: data ,response, error)
    }
    
    public func trackUsers(accessToken: String,data:UserAuth ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        authRepository.trackUsers(accessToken: accessToken,data: data ,response, error)
    }
    
}
