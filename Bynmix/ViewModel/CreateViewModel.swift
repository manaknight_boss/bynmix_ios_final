import Foundation
import Alamofire

class CreateViewModel{
    let createRepository:CreateRepository
    
    init(){
        createRepository = CreateRepository()
    }
    
    public func createPost(accessToken: String, data:CreatePostRequest,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.createPost(accessToken: accessToken, data: data, response, error)
    }
    
    public func uploadPostImage(accessToken: String, id :Int ,image:UIImage,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.uploadPostImage(accessToken: accessToken, id: id, image: image, response, error)
    }
    
    public func updatePost(accessToken: String, data:CreatePostRequest,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.updatePost(accessToken: accessToken, data: data, id: id, response, error)
    }
    
    public func deleteImage(accessToken: String, data:DeleteImagesRequest, id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.deleteImage(accessToken: accessToken, data:data, id: id, response, error)
    }
    
    public func getImageIdPost(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.getImageIdPost(accessToken: accessToken, id: id, response, error)
    }
    
    public func getDotsInfo(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.getDotsInfo(accessToken: accessToken, id: id, response, error)
    }
    
    public func getSellerPolicy(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.getSellerPolicy(accessToken: accessToken, response, error)
    }
    
    public func getSizeTabName(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.getSizeTabName(accessToken: accessToken, response, error)
    }
    
    public func getSizes(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.getSizes(accessToken: accessToken, response, error)
    }
    
    public func getSizesWithFilterView(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.getSizesWithFilterView(accessToken: accessToken, response, error)
    }
    
    public func getCategory(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.getCategory(accessToken: accessToken, response, error)
    }
    
    public func getColors(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.getColors(accessToken: accessToken, response, error)
    }
    
    public func getBrands(accessToken: String,search:String,page:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.getBrands(accessToken: accessToken,search: search,page:page ,response, error)
    }
    
    public func getBrandsForFilters(accessToken: String,search:String,page:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.getBrandsForFilters(accessToken: accessToken, search: search,page:page ,response, error)
       }
    
    public func createListing(accessToken: String,data:ListingDetail,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.createListing(accessToken: accessToken, data: data, response, error)
    }
    
    public func uploadListingImage(accessToken: String, id :Int ,images:[UIImage],_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.uploadListingImages(accessToken: accessToken, id: id, images: images, response, error)
    }
    
    public func getShippingSlab(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.getShippingSlab(accessToken: accessToken, response, error)
    }
    
    public func getAbout(_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.getAbout(response, error)
    }
    
    public func checkSellerStatus(accessToken: String, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.checkSellerStatus(accessToken: accessToken, response, error)
    }
    
    public func sendSellerDetail(accessToken: String,data:SellerPersonelInfo ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.sendSellerDetail(accessToken: accessToken,data:data ,response, error)
    }
    
    public func getListingTitles(accessToken: String,searchQuery: String? = nil,page:Int ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.getListingTitles(accessToken: accessToken,searchQuery: searchQuery ,page:page ,response, error)
    }
    
    public func deleteDots(accessToken: String, id:Int ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.deleteDots(accessToken: accessToken, id: id,response,error)
    }
    
    public func getCondition(accessToken: String ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.getCondition(accessToken: accessToken,response,error)
    }
    
    public func getSalesTax(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.getSalesTax(accessToken: accessToken, response, error)
    }
    
    public func trackListings(accessToken: String,listingId:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.trackListings(accessToken: accessToken, listingId: listingId, response, error)
    }
    
    public func trackPosts(accessToken: String,postId:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.trackPosts(accessToken: accessToken, postId: postId, response, error)
    }
    
    public func getPostType(accessToken: String,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.getPostType(accessToken: accessToken, response, error)
    }
    
    public func uploadListingVideo(accessToken: String,id:Int,videoURL:URL,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.uploadListingVideo(accessToken: accessToken, id: id, videoURL: videoURL,response, error)
    }
    
    public func deleteListingVideo(accessToken: String,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.deleteListingVideo(accessToken: accessToken, id: id,response, error)
    }
    
    public func updateImagesOrder(accessToken: String,data:ListingImagesOrder,id:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        createRepository.updateImagesOrder(accessToken: accessToken, data: data, id: id,response, error)
    }
    
}
