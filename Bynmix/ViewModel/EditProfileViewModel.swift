import Foundation
import Alamofire

class EditProfileViewModel {
    
    let editProfileRepository : EditProfileRepository
    
    init () {
        editProfileRepository = EditProfileRepository()
    }
    
    public func getBodyType(accessToken: String, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        editProfileRepository.getBodyType(accessToken: accessToken, onResponse, onError)
    }
    
    public func getStates(accessToken: String, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        editProfileRepository.getStates(accessToken: accessToken, onResponse, onError)
    }
    
}
