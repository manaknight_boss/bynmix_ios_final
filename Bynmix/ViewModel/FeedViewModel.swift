import Foundation
import Alamofire

class FeedViewModel {
    let feedRepository : FeedRepository
    
    init() {
        feedRepository = FeedRepository()
    }
    
    public func getFeedAll(accessToken: String,data:FilterData?,page:Int ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        feedRepository.getFeedAll(accessToken: accessToken, data: data, page: page, response, error)
    }
    
    public func getFeedListings(accessToken: String,data:FilterData? ,page:Int,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        feedRepository.getFeedListings(accessToken: accessToken, data: data,page:page ,response, error)
    }
    
    public func getFeedPosts(accessToken: String,data:FilterData?,page:Int ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        feedRepository.getFeedPosts(accessToken: accessToken, data: data,page:page ,response, error)
    }
    
    public func getLikeUsers(accessToken: String,page:Int,id: Int, likesType:String, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        feedRepository.getLikeUsers(accessToken: accessToken, page: page,id: id, likesType: likesType, onResponse, onError)
    }
    
    public func updateLikeStatus(accessToken: String,id: Int, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
            feedRepository.updateLikeStatus(accessToken: accessToken,id: id, onResponse, onError)
    }
    
    public func updatePostLikeStatus(accessToken: String,id: Int, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        feedRepository.updatePostLikeStatus(accessToken: accessToken,id: id, onResponse, onError)
    }
    
    public func getFeedUsers(accessToken: String, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        feedRepository.getFeedUsers(accessToken: accessToken, onResponse, onError)
    }
    
    public func getFollowFollowingUsers(accessToken: String, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        feedRepository.getFollowFollowingUsers(accessToken: accessToken, onResponse, onError)
    }
    
    public func getListingDetail(accessToken: String ,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        feedRepository.getListingDetail(accessToken: accessToken,id:id ,onResponse, onError)
    }
    
    public func getPostDetail(accessToken: String ,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        feedRepository.getPostDetail(accessToken: accessToken,id:id ,onResponse, onError)
    }
    
    public func getListingDetailImages(accessToken: String ,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        feedRepository.getListingDetailImages(accessToken: accessToken,id:id ,onResponse, onError)
    }
    
    public func getAllUsers(accessToken: String ,page:Int,data:UserFilterData?,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        feedRepository.getAllUsers(accessToken: accessToken,page:page, data: data ,onResponse, onError)
    }
    
    public func getDots(accessToken: String ,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        feedRepository.getDots(accessToken: accessToken, id: id ,onResponse, onError)
    }
    
    public func getDefaultCard(accessToken: String ,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        feedRepository.getDefaultCard(accessToken: accessToken ,onResponse, onError)
    }
    
    public func getDefaultAddress(accessToken: String ,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        feedRepository.getDefaultAddress(accessToken: accessToken ,onResponse, onError)
    }
    
    public func getDefaultReturnAddress(accessToken: String ,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        feedRepository.getDefaultReturnAddress(accessToken: accessToken ,onResponse, onError)
    }
    
    public func purchaseListing(accessToken: String ,data:SubOfferHistory,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        feedRepository.purchaseListing(accessToken: accessToken, data: data, id: id ,onResponse, onError)
    }
    
    public func addComment(accessToken: String ,data:Comments,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        feedRepository.addComment(accessToken: accessToken, id: id, data: data, onResponse, onError)
    }
    
    public func getIssues(accessToken: String ,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        feedRepository.getIssues(accessToken: accessToken, onResponse, onError)
    }
    
    public func reportAnIssue(accessToken: String ,data:Dispute,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        feedRepository.reportAnIssue(accessToken: accessToken,data:data ,onResponse, onError)
    }
    
    public func getPromotions(accessToken: String ,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        feedRepository.getPromotions(accessToken: accessToken ,onResponse, onError)
    }
    
    public func getPromotionsBuyerEnd(accessToken: String ,listingId:Int,data:OffersCheck,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        feedRepository.getPromotionsBuyerEnd(accessToken: accessToken, listingId: listingId, data: data ,onResponse, onError)
    }
    
    public func getPromotionsSellerEnd(accessToken: String ,listingId:Int,data:OffersCheck,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        feedRepository.getPromotionsSellerEnd(accessToken: accessToken, listingId: listingId, data: data ,onResponse, onError)
    }
    
    public func getUsersList(accessToken: String ,search:String,page:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        feedRepository.getUsersList(accessToken: accessToken,search: search,page: page ,onResponse, onError)
    }
    
    public func getListingComments(accessToken: String ,listingId:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        feedRepository.getListingComments(accessToken: accessToken, listingId: listingId ,onResponse, onError)
    }
    
    public func getShippingRates(accessToken: String ,listingId:Int,data:ShipRates,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        feedRepository.getShippingRates(accessToken: accessToken, listingId: listingId ,data: data,onResponse, onError)
    }
    
}
