import Foundation
import Alamofire

class ForgotPasswordViewModel {
   
    let forgotPasswordRepository : ForgotPasswordRepository
    
    init() {
        forgotPasswordRepository = ForgotPasswordRepository()
    }
    
    public func sendCode(accessToken: String,data: ForgotPasswordRequest, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        forgotPasswordRepository.sendCode(accessToken: accessToken, data: data, response, error)
    }
    
    public func verifyCode(accessToken: String,data: VerifyCodeRequest, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        forgotPasswordRepository.verifyCode(accessToken: accessToken, data: data, response, error)
    }
    
    public func resetPassword(accessToken: String,data: ResetPasswordRequest, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        forgotPasswordRepository.resetPassword(accessToken: accessToken, data: data, response, error)
    }
    
}
