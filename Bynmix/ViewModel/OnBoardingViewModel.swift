import Foundation
import Alamofire

class OnBoardingViewModel {
    
    let onBoardingRepository : OnBoardingRepository
    
    init() {
        onBoardingRepository = OnBoardingRepository()
    }
    
    public func getBrands(accessToken: String,page:Int ,searchQuery: String? = nil, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        onBoardingRepository.getBrands(accessToken: accessToken,page:page ,searchQuery: searchQuery, onResponse, onError)
    }
    
    public func getTags(accessToken: String,page:Int ,searchQuery: String? = nil, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        onBoardingRepository.getTags(accessToken: accessToken,page:page ,searchQuery: searchQuery, onResponse, onError)
    }
    
    public func getBloggers(accessToken: String,page:Int ,searchQuery: String? = nil, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        onBoardingRepository.getBloggers(accessToken: accessToken, page: page, searchQuery: searchQuery, onResponse, onError)
    }
    
    public func saveBrands(accessToken: String, data: BrandRequest, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        onBoardingRepository.saveBrands(accessToken: accessToken, data: data, onResponse, onError)
    }
    
    public func saveTags(accessToken: String, data: TagRequest, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        onBoardingRepository.saveTags(accessToken: accessToken, data: data, onResponse, onError)
    }
    
    public func saveBloggers(accessToken: String, data: BloggerRequest, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        onBoardingRepository.saveBloggers(accessToken: accessToken, data: data, onResponse, onError)
    }
    
    public func getSelectedBrandsCount(brands: [Brand]) -> Int {
        var count : Int = 0
        brands.forEach{(item) in
            if item.isSelected! {
                count += 1
            }
        }
        return count
    }
    
    func indexOfBrand(brand: Brand, brands: [Brand]) -> Int {
        for i in 0..<brands.count {
            if brands[i].brandId == brand.brandId {
                return i
            }
        }
        return -1
    }
    
    func isBrandAlreadyExist(brand: Brand, brands: [Brand]) -> Bool{
        for i in 0..<brands.count {
            if brands[i].brandId == brand.brandId {
                return true
            }
        }
        
        return false
    }
    
    func indexOfTag(tag: Tags, tags: [Tags]) -> Int {
        for i in 0..<tags.count {
            if tags[i].descriptorId == tag.descriptorId {
                return i
            }
        }
        return -1
    }
    
    func isTagAlreadyExist(tag: Tags, tags: [Tags]) -> Bool{
        for i in 0..<tags.count {
            if tags[i].descriptorId == tag.descriptorId {
                return true
            }
        }
        
        return false
    }
    
    func getTruncatedText(text: String) -> String {
        if text.count > 10 {
            let index = text.index(text.endIndex, offsetBy: (8 - text.count))
            return text[..<index] + ".."
        } else {
            return text
        }
    }
    
    func indexOfBlogger(blogger: User, bloggers: [User]) -> Int {
        for i in 0..<bloggers.count {
            if bloggers[i].userId == blogger.userId {
                return i
            }
        }
        return -1
    }
    
    func indexOfSelectedBlogger(blogger: User, selectedBloggers: [User]) -> Int {
        for i in 0..<selectedBloggers.count {
            if selectedBloggers[i].userId == blogger.userId {
                return i
            }
        }
        return -1
    }
    
    func getSelectedBloggersCount(selectedBloggers: [User]) -> Int {
        var count : Int = 0
        selectedBloggers.forEach{(item) in
            if item.isSelected! {
                count += 1
            }
        }
        return count
    }
    
    public func introduction(accessToken: String, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        onBoardingRepository.introduction(accessToken: accessToken, onResponse, onError)
    }
    
}
