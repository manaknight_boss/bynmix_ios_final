import Foundation
import Alamofire

class ProfileViewModel {
    
    let profileRepository : ProfileRepository
    
    init() {
        profileRepository = ProfileRepository()
    }
    
    public func getUser(accessToken: String,userId: Int? = 0, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        
        if let id = userId {
            profileRepository.getUser(accessToken: accessToken,userId: id, onResponse, onError)
        } else {
            profileRepository.getUser(accessToken: accessToken,userId: 0, onResponse, onError)
        }
    }
    
    public func updateProfileInfo(accessToken: String,data: User, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        profileRepository.updateProfileInfo(accessToken: accessToken, data: data, response, error)
    }
    
    public func getFollowers(accessToken: String, page:Int, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getfollowers(accessToken: accessToken,page: page ,onResponse, onError)
    }
    
    public func getFollowing(accessToken: String, page: Int, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getfollowing(accessToken: accessToken, page: page, onResponse, onError)
    }
    
    public func updateFollowStatus(accessToken: String,userId: Int, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        profileRepository.updateFollowStatus(accessToken: accessToken, userId: userId, response, error)
    }
    
    public func updateUserPhoto(accessToken: String,userId: Int, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        profileRepository.updateUserPhoto(accessToken: accessToken, id: userId, response, error)
    }
    
    public func updateUserCoverPhoto(accessToken: String,userId: Int, _ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        profileRepository.updateUserCoverPhoto(accessToken: accessToken, id: userId, response, error)
    }
    
    public func uploadCoverImage(accessToken: String,userId: Int,image: UIImage, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.uploadCoverImage(accessToken: accessToken, userId: userId, image: image, onResponse, onError)
    }
    
    public func uploadProfileImage(accessToken: String,userId: Int,image: UIImage, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.uploadProfileImage(accessToken: accessToken, userId: userId, image: image, onResponse, onError)
    }
    
    public func getLikesListing(accessToken: String,sort :String,page:Int ,data:FilterData?,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        profileRepository.getLikesListing(accessToken: accessToken,sort: sort ,page: page, data: data , response, error)
    }
    
    public func getLikesPost(accessToken: String,page:Int ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        profileRepository.getLikesPost(accessToken: accessToken ,page: page , response, error)
    }
    
    public func getNotifications(accessToken: String ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        profileRepository.getNotifications(accessToken: accessToken , response, error)
    }
    
    public func updateNotifications(accessToken: String,data:NotificationSettingRequest ,_ response: @escaping (AFDataResponse<Any>) -> Void, _ error: @escaping (ApiError) -> Void) {
        profileRepository.updateNotifications(accessToken: accessToken ,data: data ,response, error)
    }
    
    public func getOtherUserByn(accessToken: String,page:Int,id: Int, data:FilterData?,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getOtherUserByn(accessToken: accessToken, page: page,id: id, data: data, onResponse, onError)
    }
    
    public func getOtherUserPost(accessToken: String,page:Int,id: Int, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getOtherUserPost(accessToken: accessToken, page: page,id: id, onResponse, onError)
    }
    
    public func getOtherUserFollowers(accessToken: String,page:Int,userId: Int, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getOtherUserFollowers(accessToken: accessToken, userId: userId, page: page, onResponse, onError)
    }
    
    public func getOtherUserFollowing(accessToken: String,page:Int,userId: Int, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getOtherUserFollowing(accessToken: accessToken, userId: userId, page: page, onResponse, onError)
    }
    
    public func getUserPost(accessToken: String,page:Int, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getUserPost(accessToken: accessToken, page: page, onResponse, onError)
    }
    
    public func getUserByn(accessToken: String,page:Int, data:FilterData?,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getUserByn(accessToken: accessToken, page: page, data: data, onResponse, onError)
    }
    
    public func enableDisableByn(accessToken: String,id:Int,page:Int ,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.enableDisableByn(accessToken: accessToken, id: id, page: page, onResponse, onError)
    }
    
    public func deleteByn(accessToken: String,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.deleteByn(accessToken: accessToken, id: id, onResponse, onError)
    }
    
    public func enableDisablePosts(accessToken: String,id:Int,page:Int ,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.enableDisablePosts(accessToken: accessToken, id: id, page: page, onResponse, onError)
    }
    
    public func deletePosts(accessToken: String,id:Int ,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.deletePosts(accessToken: accessToken, id: id, onResponse, onError)
    }
    
    public func getAddresses(accessToken: String ,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getAddresses(accessToken: accessToken, onResponse, onError)
    }
    
    public func addAddresses(accessToken: String ,data:Address,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.addAddresses(accessToken: accessToken, data: data, onResponse, onError)
    }
    
    public func updateAddresses(accessToken: String ,id:Int,data:Address,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.updateAddresses(accessToken: accessToken, id: id, data: data, onResponse, onError)
    }
    
    public func deleteAddresses(accessToken: String ,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.deleteAddresses(accessToken: accessToken, id: id, onResponse, onError)
    }
    
    public func getMyWalletSales(accessToken: String ,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getMyWalletSales(accessToken: accessToken, onResponse, onError)
    }
    
    public func getMyWalletCard(accessToken: String ,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getMyWalletCard(accessToken: accessToken, onResponse, onError)
    }
    
    public func getBalanceHistory(accessToken: String ,page:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getBalanceHistory(accessToken: accessToken, page: page, onResponse, onError)
    }
    
    public func addPayment(accessToken: String ,data:Card,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.addPayment(accessToken: accessToken, data: data, onResponse, onError)
    }
    
    public func getCardInfo(accessToken: String ,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getCardInfo(accessToken: accessToken,id:id ,onResponse, onError)
    }
    
    public func editPayment(accessToken: String ,id:Int,data:Card,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.editPayment(accessToken: accessToken,id:id, data: data ,onResponse, onError)
    }
    
    public func getAccountInfo(accessToken: String ,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getAccountInfo(accessToken: accessToken ,onResponse, onError)
    }
    
    public func withdrawMoneyFromAccount(accessToken: String,id:Int ,data:AccountInfo,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.withdrawMoneyFromAccount(accessToken: accessToken,id:id, data: data ,onResponse, onError)
    }
    
    public func uploadBankDetails(accessToken: String ,data:AccountInfo,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.uploadBankDetails(accessToken: accessToken, data: data ,onResponse, onError)
    }
    
    public func checkIfOffersAccepted(accessToken: String ,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.checkIfOffersAccepted(accessToken: accessToken, id: id ,onResponse, onError)
    }
    
    public func deleteCard(accessToken: String ,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.deleteCard(accessToken: accessToken, id: id ,onResponse, onError)
    }
    
    public func replaceCard(accessToken: String ,oldCardId:Int,newCardId:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.replaceCard(accessToken: accessToken, oldCardId: oldCardId, newCardId:newCardId,onResponse, onError)
    }
    
    public func getSale(accessToken: String ,page:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getSales(accessToken: accessToken, page: page,onResponse, onError)
    }
    
    public func getPurchase(accessToken: String ,page:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getPurchase(accessToken: accessToken, page: page,onResponse, onError)
    }
    
    public func getPurchaseStats(accessToken: String ,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getPurchaseStats(accessToken: accessToken,onResponse, onError)
    }
    
    public func getSaleStats(accessToken: String ,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getSaleStats(accessToken: accessToken,onResponse, onError)
    }
    
    public func getSalesDetail(accessToken: String ,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getSalesDetail(accessToken: accessToken, id: id,onResponse, onError)
    }
    
    public func getPurchasesDetail(accessToken: String ,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getPurchasesDetail(accessToken: accessToken, id: id,onResponse, onError)
    }
    
    public func relistItem(accessToken: String ,listingId:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.relistItem(accessToken: accessToken, listingId: listingId,onResponse, onError)
    }
    
    public func updateSalePurchaseLikeStatus(accessToken: String ,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.updateSalePurchaseLikeStatus(accessToken: accessToken, id: id,onResponse, onError)
    }
    
    public func buyerFeedback(accessToken: String ,id:Int,data: PurchaseDetail,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.buyerFeedback(accessToken: accessToken, id: id, data: data,onResponse, onError)
    }
    
    public func sellerFeedback(accessToken: String ,id:Int,data: SalesDetail,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.sellerFeedback(accessToken: accessToken, id: id, data: data,onResponse, onError)
    }
    
    public func getTrackInformation(accessToken: String ,id:Int,canReturnInfoForReturnLabel:Bool,type:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getTrackInformation(accessToken: accessToken, id: id, canReturnInfoForReturnLabel: canReturnInfoForReturnLabel, type: type,onResponse, onError)
    }
    
    public func emailShippingLabel(accessToken: String ,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.emailShippingLabel(accessToken: accessToken, id: id,onResponse, onError)
    }
    
    public func emailShippingLabelFreeShipping(accessToken: String ,id:Int,data:ShipRates,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.emailShippingLabelFreeShipping(accessToken: accessToken, id: id, data: data,onResponse, onError)
    }
    
    public func shippingRate(accessToken: String ,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.shippingRate(accessToken: accessToken, id: id,onResponse, onError)
    }
    
    public func getListingImages(accessToken: String ,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getListingImages(accessToken: accessToken, id: id,onResponse, onError)
    }
    
    public func getListingStatuses(accessToken: String ,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getListingStatuses(accessToken: accessToken,onResponse, onError)
    }
    
    public func getListingDetail(accessToken: String ,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getListingDetail(accessToken: accessToken,id:id,onResponse, onError)
    }
    
    public func updateEditListing(accessToken: String ,id:Int,data:ListingDetail,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.updateEditListing(accessToken: accessToken,id:id, data: data,onResponse, onError)
    }
    
    public func updateEditListingImages(accessToken: String ,id:Int,images:[UIImage],_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.updateEditListingImages(accessToken: accessToken,id:id, images: images,onResponse, onError)
    }
    
    public func deleteEditListingImages(accessToken: String ,id:Int,data:DeleteImagesRequest,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.deleteEditListingImages(accessToken: accessToken,id:id, data: data,onResponse, onError)
    }
    
    public func getStyles(accessToken: String ,searchQuery: String? = nil,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getStyles(accessToken: accessToken,searchQuery: searchQuery,onResponse, onError)
    }
    
    public func getReportIssueType(accessToken: String,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getReportIssueType(accessToken: accessToken,onResponse, onError)
    }
    
    public func checkReportAnIssue(accessToken: String,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.checkReportAnIssue(accessToken: accessToken,id:id,onResponse, onError)
    }
    
    public func getSaleCancelReason(accessToken: String,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getSaleCancelReason(accessToken: accessToken,onResponse, onError)
    }
    
    public func getDisputes(accessToken: String,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getDisputes(accessToken: accessToken,onResponse, onError)
    }
    
    public func uploadReportAnIssueImages(accessToken: String,id:Int,images:[UIImage],_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.uploadReportAnIssueImages(accessToken: accessToken, id: id, images: images,onResponse, onError)
    }
    
    public func reportAnIssue(accessToken: String,data:Dispute,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.reportAnIssue(accessToken: accessToken,data: data,onResponse, onError)
    }
    
    public func cancelOrderSale(accessToken: String,id:Int,data:SaleCancelReason,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.cancelOrderSale(accessToken: accessToken, id: id,data: data,onResponse, onError)
    }
    
    public func cancelPurchase(accessToken: String,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.cancelPurchase(accessToken: accessToken, id: id,onResponse, onError)
    }
    
    public func getDisputeDetails(accessToken: String,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getDisputeDetails(accessToken: accessToken, id: id,onResponse, onError)
    }
    
    public func cancelDispute(accessToken: String,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.cancelDispute(accessToken: accessToken, id: id,onResponse, onError)
    }
    
    public func fetchBuyerReviews(accessToken: String,id:Int,page:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.fetchBuyerReviews(accessToken: accessToken, id: id, page: page,onResponse, onError)
    }
    
    public func fetchSellerReviews(accessToken: String,id:Int,page:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.fetchSellerReviews(accessToken: accessToken, id: id, page: page,onResponse, onError)
    }
    
    public func addDisputeCommment(accessToken: String,id:Int,data:Comments,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.addDisputeCommment(accessToken: accessToken, id: id, data: data,onResponse, onError)
    }
    
    public func escalateDispute(accessToken: String,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.escalateDispute(accessToken: accessToken, id: id,onResponse, onError)
    }
    
    public func refundBuyerWithoutReturn(accessToken: String,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.refundBuyerWithoutReturn(accessToken: accessToken, id: id,onResponse, onError)
    }
    
    public func returnLabel(accessToken: String,id:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.returnLabel(accessToken: accessToken, id: id,onResponse, onError)
    }
    
    public func uploadCommentImages(accessToken: String,disputeId:Int,commentId:Int,images:[UIImage],_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.uploadCommentImages(accessToken: accessToken, disputeId: disputeId,commentId: commentId, images: images,onResponse, onError)
    }
    
    public func confirmDeliveryStatus(accessToken: String,saleId:Int,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.confirmDeliveryStatus(accessToken: accessToken, saleId: saleId,onResponse, onError)
    }
    
    public func trackSearch(accessToken: String,data:TrackSearchOfUser,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.trackSearch(accessToken: accessToken, data: data,onResponse, onError)
    }
    
    public func logoutUser(accessToken: String,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.logoutUser(accessToken: accessToken,onResponse, onError)
    }
    
    public func updateUserInfo(accessToken: String,data:AccountVerify,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.updateUserInfo(accessToken: accessToken, data: data,onResponse, onError)
    }
    
    public func checkIfAccountVerified(accessToken: String,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.checkIfAccountVerified(accessToken: accessToken,onResponse, onError)
    }
    
    public func getDocumentType(accessToken: String,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getDocumentType(accessToken: accessToken,onResponse, onError)
    }
    
    public func uploadImageWithDocument(accessToken: String,image:UIImage,data:AccountVerify,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.uploadImageWithDocument(accessToken: accessToken,image: image,data: data,onResponse, onError)
    }
    
    public func getSelectedBrands(accessToken: String,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getSelectedBrands(accessToken: accessToken,onResponse, onError)
    }
    
    public func getSelectedDescriptors(accessToken: String,_ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.getSelectedDescriptors(accessToken: accessToken,onResponse, onError)
    }
    
    public func saveEditedBrands(accessToken: String, data: User, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.saveEditedBrands(accessToken: accessToken, data: data, onResponse, onError)
    }
    
    public func saveEditedTags(accessToken: String, data: User, _ onResponse: @escaping (AFDataResponse<Any>) -> Void, _ onError: @escaping (ApiError) -> Void) {
        profileRepository.saveEditedTags(accessToken: accessToken, data: data, onResponse, onError)
    }
    
}
